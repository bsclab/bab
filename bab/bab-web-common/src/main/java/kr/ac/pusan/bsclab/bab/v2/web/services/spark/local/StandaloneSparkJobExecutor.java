package kr.ac.pusan.bsclab.bab.v2.web.services.spark.local;

import java.lang.reflect.Method;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.rest.RestService;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkJob;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkSubmission;
import kr.ac.pusan.bsclab.bab.v2.web.services.IJobExecutor;

@Service
@ConditionalOnProperty(name = "bab.serviceJobExecutor", havingValue = "standalone-spark")
public class StandaloneSparkJobExecutor implements IJobExecutor {

	@Autowired
	SparkConfiguration sparkConfig;
	
	@Autowired
	RestService restService;
	

	@Override
	public Job run(Job j) {
		SparkJob job = (SparkJob) j;
		try {
			job.setStarted(new Date().getTime());
			job.setStatus(SparkJob.STATUS_RUNNING);
			job.setRemarks("");
			String[] args = job.getAppArgs().toArray(new String[0]);
			args[2] = "local";
//			args[4] = args[1] + args[4];
			Class<?> x = Class.forName(job.getMainClass());
			Object o = x.newInstance();
			Method m = x.getMethod("run", String[].class);
			Object r = m.invoke(o, new Object[] {args});
			if (r != null) {
				job.setStatus(SparkJob.STATUS_COMPLETED);
			} else {
				job.setStatus(SparkJob.STATUS_FAILED);
			}
			x = null;
			o = null;
			m = null;
			r = null;
			System.gc();
//			String hdfs = args[1];
//			String spark = args[2];
//			String jobClass = args[3];
//			String jobPath = args[4];
//			String userId = args[5];
//			String runMode = args.length > 6 ? args[6] : "cluster";
//			ObjectMapper mapper = new ObjectMapper();
//			String json = mapper.writeValueAsString((SparkJob) job);
//			String uri = sparkConfig.getRestUrl() + "/v1/submissions/create";
//			HttpHeaders headers = new HttpHeaders();
//			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
//			HttpEntity<String> entity = new HttpEntity<String>(json, headers);
//			ResponseEntity<SparkSubmission> status = new ResponseEntity<SparkSubmission>(null);
//			if (status != null) {
//				job.setSparkSubmission(status.getBody());
//				return status(job);
//			}
			toString();
		} catch (Exception e) {
			job.setStatus(SparkJob.STATUS_ERROR);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Job status(Job j) {
		try {
			SparkJob job = (SparkJob) j;
//			String uri = sparkConfig.getRestUrl() + "/v1/submissions/status/" + job.getSparkSubmission().getSubmissionId();
//			HttpHeaders headers = new HttpHeaders();
//			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
//			HttpEntity<String> entity = new HttpEntity<String>("", headers);
//			ResponseEntity<SparkSubmission> status = restService.exchange(uri, HttpMethod.GET, entity, SparkSubmission.class);
//			if (status != null) {
//				job.setSparkSubmission(status.getBody());
//				job.setStatus(job.getSparkSubmission().getDriverState().trim());
//			}
			String[] args = job.getAppArgs().toArray(new String[0]);
			args[2] = "local";
			return job;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Job kill(Job j) {
		SparkJob job = (SparkJob) status(j);
		if (job.isRunning() && job.getSparkSubmission() != null) {
			if (!job.getSparkSubmission().getDriverState().equalsIgnoreCase(Job.STATUS_COMPLETED)
				|| !job.getSparkSubmission().getDriverState().equalsIgnoreCase(Job.STATUS_FINISHED)) {
				try {
					String uri = sparkConfig.getRestUrl() + "/v1/submissions/kill/" + job.getSparkSubmission().getSubmissionId();
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
					HttpEntity<String> entity = new HttpEntity<String>("", headers);
					ResponseEntity<SparkSubmission> status = restService.exchange(uri, HttpMethod.POST, entity, SparkSubmission.class);
					if (status != null) {
						job.setSparkSubmission(status.getBody());
						job = (SparkJob) status(job);
						job.setStatus(Job.STATUS_KILLED);
						return job;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return job; 
	}

	@Override
	public boolean isBusy() {
		return false;
	}

}
