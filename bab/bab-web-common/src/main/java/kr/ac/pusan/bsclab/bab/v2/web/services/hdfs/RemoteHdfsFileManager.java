package kr.ac.pusan.bsclab.bab.v2.web.services.hdfs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import kr.ac.pusan.bsclab.bab.v2.web.services.rest.RestService;

@Service
@ConditionalOnProperty(name = "bab.serviceFileManager", havingValue = "remote-hdfs")
public class RemoteHdfsFileManager implements IFileManager {

	@Autowired
	RestService restService;

	@Override
	public boolean isExists(String hdfsPath) {
		try {
			String uri = hdfsPath + "?user.name=default&op=GETFILESTATUS";
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK && result.getBody().indexOf("FileStatus") > -1) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public void saveAsTextFile(String hdfsPath, String data) {
		try {
			String uri = hdfsPath + "?user.name=default&op=CREATE";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.PUT, entity, String.class);
			String saveUri = result.getHeaders().get("Location").get(0);
			HttpEntity<String> entityFile = new HttpEntity<String>(data, headers);
			result = restService.exchange(saveUri, HttpMethod.PUT, entityFile, String.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String loadFromTextFile(String hdfsPath) {
		try {
			String uri = hdfsPath + "?user.name=default&op=OPEN";
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				return result.getBody();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
