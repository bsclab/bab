package kr.ac.pusan.bsclab.bab.v2.web.services.spark;

import java.util.Date;

import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;

public class SparkResponse<T> extends ServiceResponse {
  public static final String STATUS_UNKNOWN = "UNKNOWN";
  public static final String STATUS_QUEUED = "QUEUED";
  public static final String STATUS_RUNNING = "RUNNING";
  public static final String STATUS_FINISHED = "FINISHED";

  private String id;
  private Job request;
  private T response;
  private String status = STATUS_UNKNOWN;

  public SparkResponse() {
    Date now = new Date();
    id = String.valueOf(now.getTime());
  }

  public SparkResponse(Job request) {
    this.request = request;
    id = request.getId();
    status = request.getStatus();
  }
  
  public SparkResponse(Job request, T response) {
    this.request = request;
    this.response = response;
    id = request.getId();
    status = request.getStatus();
   }

  public Job getRequest() {
    return request;
  }

  public void setRequest(Job request) {
    this.request = request;
  }

  public T getResponse() {
    return response;
  }

  public void setResponse(T response) {
    this.response = response;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}

