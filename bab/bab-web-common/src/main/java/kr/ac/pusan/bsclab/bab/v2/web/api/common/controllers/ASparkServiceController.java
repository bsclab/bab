package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.api.controllers.JobController;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.HdfsConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkResponse;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkJob;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;

public abstract class ASparkServiceController extends AServiceController {

	@Autowired
	protected SparkConfiguration sparkConfig;

	@Autowired
	protected HdfsConfiguration hdfsConfig;

	public SparkConfiguration getSparkConfig() {
		return sparkConfig;
	}

	public HdfsConfiguration getHdfsConfig() {
		return hdfsConfig;
	}

	public IServiceResponse runService(ServiceEndpoint service, String workspaceId, String datasetId, String repositoryId,
	    IServiceRequest request) {
		if (request instanceof Configuration) {
			Configuration reqConfig = (Configuration) request;
			reqConfig.setWorkspaceId(workspaceId);
			reqConfig.setDatasetId(datasetId);
			reqConfig.setRepositoryId(repositoryId);
			reqConfig.setRepositoryURI("/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId);
		}
		SparkJob job = (SparkJob) getJobManager().getJobFactory().create(service, request);
		getJobManager().register(job);
		job.getAppArgs().add(0, getConfig().getUrl() + JobController.BASE_URL + "/report/" + job.getId());
		String configFileName = getHdfsConfig().getRestUrl() + "/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId
		    + "/" + job.getHash() + service.getService().legacyJobExtension();
		JobConfiguration config = null;
		if (!getFileManager().isExists(configFileName)) {
			config = new JobConfiguration();
			config.setJobId(service.toString() + "+" + job.getId());
			config.setJobClass(service.getService().name());
			config.setWorkspaceId(workspaceId);
			config.setDatasetId(datasetId);
			config.setRepositoryId(repositoryId);
			config.setPath("/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash());
			config.setExt(service.getService().legacyJobExtension());
			config.setJobPath("/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash()
			    + service.getService().legacyJobExtension());
			config.setResultPath(config.getPath() + service.getService().legacyJobExtension());
			String configJson = "";
			try {
				configJson = getMapper().writeValueAsString(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
			config.setConfiguration(configJson);
			String json = "";
			try {
				json = getMapper().writeValueAsString(config);
			} catch (Exception e) {
				e.printStackTrace();
			}
			getFileManager().saveAsTextFile(configFileName, json);
		}
		String configJson = getFileManager().loadFromTextFile(configFileName);
		try {
			config = getMapper().readValue(configJson, JobConfiguration.class);
		} catch (Exception e) {
			e.printStackTrace();
			config = null;
		}
		if (config != null) {
			request.setUri("/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash()
			    + service.getService().legacyJobExtension());
			job.getAppArgs().add(request.getUri());
			job.getAppArgs().add(getConfig().getUrl());
			job.getAppArgs().add(hdfsConfig.getMode());
			String resultFileName = getHdfsConfig().getRestUrl() + config.getResultPath();
			Class<? extends IServiceResponse> responseClass = service.getService().responseClass();
			if (getFileManager().isExists(resultFileName)) {
				String resultJson = getFileManager().loadFromTextFile(resultFileName);
				try {
					IServiceResponse response = getMapper().readValue(resultJson, responseClass);
					job.setStatus(Job.STATUS_COMPLETED);
					return new SparkResponse<IServiceResponse>(job, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			getJobManager().submit(job);
			return new SparkResponse<IServiceResponse>(job);
		}
		return null;
	}

}
