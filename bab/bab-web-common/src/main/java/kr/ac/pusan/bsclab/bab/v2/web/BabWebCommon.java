package kr.ac.pusan.bsclab.bab.v2.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import kr.ac.pusan.bsclab.bab.v2.web.BabWeb;

@ComponentScan(basePackages = "kr.ac.pusan.bsclab.bab.v2.web")
@EnableScheduling
@SpringBootApplication
@EnableAsync
public class BabWebCommon extends BabWeb {
	public static final String BASE_URL = "/bab";
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BabWebCommon.class, args);
		
//		SpringApplication.run(new Object[]{BabWeb.class, BabWebCommon.class}, args);
	}
}