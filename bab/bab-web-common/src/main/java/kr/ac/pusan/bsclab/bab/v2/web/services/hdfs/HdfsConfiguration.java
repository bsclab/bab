package kr.ac.pusan.bsclab.bab.v2.web.services.hdfs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import kr.ac.pusan.bsclab.bab.v2.web.models.AConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;

@Configuration
@ConfigurationProperties("hdfs")
public class HdfsConfiguration extends AConfiguration {

	public static final String MODE_CLUSTER = "cluster";
	public static final String MODE_LOCAL = "local";

	@Autowired
	protected BabConfiguration bab;

	protected String namenodeIp;
	protected String baseDir;
	protected String url;
	protected String restUrl;
	protected String version = "2.6.0";
	protected String mode = MODE_CLUSTER;

	public BabConfiguration getBab() {
		return bab;
	}

	public void setBab(BabConfiguration bab) {
		this.bab = bab;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRestUrl() {
		return restUrl;
	}

	public void setRestUrl(String restUrl) {
		this.restUrl = restUrl;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNamenodeIp() {
		return namenodeIp;
	}

	public void setNamenodeIp(String namenodeIp) {
		this.namenodeIp = namenodeIp;
	}

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

}