package kr.ac.pusan.bsclab.bab.v2.web.services.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface RestServiceCallback<T> {
	void done(ResponseEntity<T> response);
}
