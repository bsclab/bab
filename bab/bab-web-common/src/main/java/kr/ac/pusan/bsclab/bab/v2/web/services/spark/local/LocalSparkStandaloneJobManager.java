package kr.ac.pusan.bsclab.bab.v2.web.services.spark.local;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.StandaloneJobManager;

@Service
@ConditionalOnProperty(name = "bab.serviceJobManager", havingValue = "local-spark")
public class LocalSparkStandaloneJobManager extends StandaloneJobManager {
	@Override
	public Job submit(Job job) {
		return super.submit(job);
	}
}
