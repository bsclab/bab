package kr.ac.pusan.bsclab.bab.v2.web.services.spark;

public class SparkSubmission {
	protected String action;
	protected String serverSparkVersion;
	protected String submissionId;
	protected boolean success;
	protected String message;
	protected String driverState;
	protected String workerHostPort;
	protected String workerId;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getServerSparkVersion() {
		return serverSparkVersion;
	}

	public void setServerSparkVersion(String serverSparkVersion) {
		this.serverSparkVersion = serverSparkVersion;
	}

	public String getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDriverState() {
		return driverState;
	}

	public void setDriverState(String driverState) {
		this.driverState = driverState;
	}

	public String getWorkerHostPort() {
		return workerHostPort;
	}

	public void setWorkerHostPort(String workerHostPort) {
		this.workerHostPort = workerHostPort;
	}

	public String getWorkerId() {
		return workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}

}
