package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;

@Controller
public class SparkController extends ASparkServiceController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/spark";

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/submit/{packageId}/{serviceId}/{workspaceId}/{datasetId}/{repositoryId}")
	@ResponseBody
	public IServiceResponse postSubmit(
			@PathVariable(value = "packageId") String packageId,
			@PathVariable(value = "serviceId") String serviceId,
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId,
			@RequestBody(required = false) String json) {
		ServiceEndpoint service = getJobManager().getServices(0).get(packageId).get(serviceId);
		Class<? extends IServiceRequest> configClass = service.getService().requestClass();
		IServiceRequest config = null;
		try {
			config = getMapper().readValue(json, configClass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (config == null) {
			try {
				config = configClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		IServiceResponse response = runService(service, workspaceId, datasetId, repositoryId, config);
		return response;
	}

}
