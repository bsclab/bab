package kr.ac.pusan.bsclab.bab.v2.web.services.spark;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import kr.ac.pusan.bsclab.bab.v2.web.api.controllers.JobController;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;

public class SparkJob extends Job {
	public static final String STATUS_SUBMITTED = "SUBMITTED";
	public static final String ACTION_SUBMIT = "CreateSubmissionRequest";
	
	public static String getHash(String json) {
		return DigestUtils.md5Hex(json);
	}

	protected Map<String, String> environmentVariables = new LinkedHashMap<String, String>();

	protected String clientSparkVersion;
	protected String action;
	protected Map<String, String> sparkProperties = new LinkedHashMap<String, String>();

	protected String appResource;
	protected String mainClass;
	protected List<String> appArgs = new ArrayList<String>();

	protected SparkSubmission sparkSubmission;

	public SparkJob(SparkConfiguration conf) {
		getEnvironmentVariables().put("SPARK_ENV_LOADED", "1");
		setClientSparkVersion(conf.version);
		setAction(ACTION_SUBMIT);
		setName(conf.getBabName());

		getSparkProperties().put("spark.app.name", conf.getBabName());

		getSparkProperties().put("spark.master", conf.getUrl());
		getSparkProperties().put("spark.submit.deployMode", conf.getMode());
		getSparkProperties().put("spark.driver.supervise", conf.getDriverSupervise());
		if (!conf.getDriverCores().equalsIgnoreCase("0")) {
			getSparkProperties().put("spark.driver.cores", conf.getDriverCores());
		}
		if (!conf.getDriverMemory().equalsIgnoreCase("0")) {
			getSparkProperties().put("spark.driver.memory", conf.getDriverMemory());
		}
		getSparkProperties().put("driver.class.path", conf.getDriverClassPath());

		if (!conf.getExecutorCores().equalsIgnoreCase("0")) {
			getSparkProperties().put("spark.executor.cores", conf.getExecutorCores());
		}
		if (!conf.getExecutorMemory().equalsIgnoreCase("0")) {
			getSparkProperties().put("spark.executor.memory", conf.getExecutorMemory());
		}
		getSparkProperties().put("spark.eventLog.enabled", conf.getEventLogEnabled());
		getSparkProperties().put("spark.eventLog.dir", conf.getEventLogDir());
	}
	
	@Override
	public boolean isRunning() {
		if (getSparkSubmission() != null) {
			return getSparkSubmission().getDriverState().equalsIgnoreCase(STATUS_RUNNING) ||
					getSparkSubmission().getDriverState().equalsIgnoreCase(STATUS_WAITING) ||
					getSparkSubmission().getDriverState().equalsIgnoreCase(STATUS_SUBMITTED);
		}
		return super.isRunning();
	}

	public Map<String, String> getEnvironmentVariables() {
		return environmentVariables;
	}

	public void setEnvironmentVariables(Map<String, String> environmentVariables) {
		this.environmentVariables = environmentVariables;
	}

	public String getClientSparkVersion() {
		return clientSparkVersion;
	}

	public void setClientSparkVersion(String clientSparkVersion) {
		this.clientSparkVersion = clientSparkVersion;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Map<String, String> getSparkProperties() {
		return sparkProperties;
	}

	public void setSparkProperties(Map<String, String> sparkProperties) {
		this.sparkProperties = sparkProperties;
	}

	public String getAppResource() {
		return appResource;
	}

	public void setAppResource(String appResource) {
		this.appResource = appResource;
	}

	public String getMainClass() {
		return mainClass;
	}

	public void setMainClass(String mainClass) {
		this.mainClass = mainClass;
	}

	public List<String> getAppArgs() {
		return appArgs;
	}

	public void setAppArgs(List<String> appArgs) {
		this.appArgs = appArgs;
	}

	public SparkSubmission getSparkSubmission() {
		return sparkSubmission;
	}

	public void setSparkSubmission(SparkSubmission sparkSubmission) {
		this.sparkSubmission = sparkSubmission;
	}

	@Override
	public String getUri() {
		return JobController.BASE_URL + "/status/" + getId();
	}

}
