package kr.ac.pusan.bsclab.bab.v2.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component("CommandLineRunner_bab_web_common")
public class CommandLineRunnerTest implements CommandLineRunner {


  private static final Logger logger = LoggerFactory.getLogger(CommandLineRunnerTest.class);


  @Override
  public void run(String... args) throws Exception {
    logger.debug("#### bab-web-common command line runner start ####");



    logger.debug("#### bab-web-common command line runner finished ####");
  }


}
