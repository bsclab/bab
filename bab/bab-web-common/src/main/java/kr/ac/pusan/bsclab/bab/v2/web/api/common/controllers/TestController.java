package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

@Controller
public class TestController extends ASparkServiceController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/test";
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/submit")
	public @ResponseBody IServiceResponse getTestSubmit() {
		ServiceEndpoint se = getJobManager().getServices(0).get("kr.ac.pusan.bsclab.bab.v2.legacy").get("HelloWorldJob");
		IServiceRequest sr = null;
		try {
			sr = se.getService().requestClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		if (sr == null) {
			sr = new Configuration();
		}
		Configuration config = (Configuration) sr;
		config.setRepositoryURI("/default/default/1");
		config.setWorkspaceId("default");
		config.setDatasetId("default");
		config.setRepositoryId("1");
		IServiceResponse response = runService(se, "default", "default", "1", sr);
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/heuristic")
	public @ResponseBody IServiceResponse getTestHeuristic() {
		ServiceEndpoint se = getJobManager().getServices(0).get("kr.ac.pusan.bsclab.bab.v2.legacy").get("ModelHeuristicJob");
		IServiceRequest sr = null;
		try {
			sr = se.getService().requestClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		if (sr == null) {
			sr = new Configuration();
		}
		Configuration config = (Configuration) sr;
		config.setRepositoryURI("/default/default/1");
		config.setWorkspaceId("default");
		config.setDatasetId("default");
		config.setRepositoryId("1");
		IServiceResponse response = runService(se, "default", "default", "1", sr);
		return response;
	}

}
