/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.base.controller;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;

/**
 * Interface for BAB job
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public interface IJob extends Serializable {
  /**
   * Get job resource (result)
   * 
   * @param id job Id
   * @return job Id
   */
  public IResource getResource(String id);

  /**
   * Run BAB job
   * 
   * @param json configuration json from web service
   * @param res output resource
   * @param se spark executor instance
   * @return job result class
   * @throws Exception
   */
  public IJobResult run(String json, IResource res, IExecutor se) throws Exception;

  /**
   * Get request information
   * 
   * @return request information
   */
  public String getRequestInfo();
}
