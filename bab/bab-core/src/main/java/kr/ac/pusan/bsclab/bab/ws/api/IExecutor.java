/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api;

import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;
import kr.ac.pusan.bsclab.bab.ws.controller.FileUtil;

/**
 * Abstraction for spark executor
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public interface IExecutor {
  /**
   * Executor startup
   * 
   * @param startup
   */
  public void startup(String parameter);

  /**
   * Executor shutdown
   */
  public void shutdown();

  /**
   * Get exact path of output uri
   * 
   * @param rawUri Raw Output URI
   * @return Output URI
   */
  public String getContextUri(String rawUri);

  public DateUtil getDateUtil();

  public FileUtil getFileUtil();

}
