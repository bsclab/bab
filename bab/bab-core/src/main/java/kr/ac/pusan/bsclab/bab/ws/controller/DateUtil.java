/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public static final long TU_DEFAULT = 1000L;
  public static final long TU_YEAR = 31104000000L;
  public static final long TU_MONTH = 2592000000L;
  public static final long TU_WEEK = 604800000L;
  public static final long TU_DAY = 86400000L;
  public static final long TU_HOUR = 3600000L;
  public static final long TU_MINUTE = 60000L;
  public static final long TU_SECOND = 1000L;
  public static final long TU_MILLIS = 1L;

  private static DateFormat dateFormatter;

  protected DateUtil() {

  }

  protected static DateUtil instance;

  public static DateUtil getInstance() {
    if (instance == null) {
      instance = new DateUtil();
    }
    return instance;
  }

  public String toDateTimeString(long date) {
    if (dateFormatter == null)
      dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    return dateFormatter.format(new Date(date));
  }


  public long fromDateTimeString(String date) {
    if (dateFormatter == null)
      dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    try {
      return dateFormatter.parse(date).getTime();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return 0;
  }

  public String toDurationString(long duration) {
    long ms = duration % 1000;
    duration = duration / 1000;
    long oduration = duration;
    long y = duration / 31104000;
    duration %= 31104000;
    long m = duration / 2592000;
    duration %= 2592000;
    long d = duration / 86400;
    duration %= 86400;
    long h = duration / 3600;
    duration %= 3600;
    long i = duration / 60;
    duration %= 60;
    long s = duration % 60;
    // System.out.println(String.format("%dY %dM %dD %02d:%02d:%02d.%03d",
    // y, m, d, h, i, s, ms));
    if (oduration < 86400) {
      return String.format("%02d:%02d:%02d.%03d", h, i, s, ms);
    } else if (oduration < 2592000) {
      return String.format("%dD %02d:%02d:%02d.%03d", d, h, i, s, ms);
    } else if (oduration < 31104000) {
      return String.format("%dM %dD %02d:%02d:%02d.%03d", m, d, h, i, s, ms);
    }
    return String.format("%dY %dM %dD %02d:%02d:%02d.%03d", y, m, d, h, i, s, ms);
  }

}
