//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceResponse;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

/**
 * Basic implementation of BAB repository <br>
 * <br >
 * See {@link IRepository}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class BRepository extends ServiceResponse implements IRepository {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;


  private String repositoryURI;
  private String id;
  private String name;
  private String description;
  private String uri;
  private Map<String, Map<String, String>> mapping = new TreeMap<String, Map<String, String>>();

  private Map<String, Object> attributes;

  private String originalName;
  private long createdDate;

  private Map<String, Integer> cases;
  private Map<String, Integer> activities;
  private Map<String, Integer> activityTypes;
  private Map<String, Integer> originators;
  private Map<String, Integer> resources;
  private Map<String, Map<String, Integer>> caseAttributes;
  private Map<String, Map<String, Integer>> eventAttributes;

  private int noOfCases;
  private int noOfEvents;
  private int noOfActivities;
  private int noOfActivityTypes;
  private int noOfOriginators;
  private int noOfResourceClasses;
  private Map<String, Integer> noOfCaseAttributes;
  private Map<String, Integer> noOfEventAttributes;

  private long timestampStart;
  private long timestampEnd;

  public BRepository() {

  }

  public BRepository(String id, String uri, String originalName, long createdDate, int noOfCases,
      int noOfEvents, int noOfActivities, int noOfActivityTypes, int noOfOriginator,
      int noOfResourceClasses) {
    this.id = id;
    this.uri = uri;
    this.originalName = originalName;
    this.createdDate = createdDate;
    this.noOfCases = noOfCases;
    this.noOfEvents = noOfEvents;
    this.noOfActivities = noOfActivities;
    this.noOfActivityTypes = noOfActivityTypes;
    this.noOfOriginators = noOfOriginator;
    this.noOfResourceClasses = noOfResourceClasses;
  }

  @Override
  public Map<String, Integer> getCases() {
    if (cases == null) {
      cases = new LinkedHashMap<String, Integer>();
    }
    return cases;
  }

  @Override
  public Map<String, Integer> getActivities() {
    if (activities == null) {
      activities = new LinkedHashMap<String, Integer>();
    }
    return activities;
  }

  @Override
  public Map<String, Integer> getActivityTypes() {
    if (activityTypes == null) {
      activityTypes = new LinkedHashMap<String, Integer>();
    }
    return activityTypes;
  }

  @Override
  public Map<String, Integer> getOriginators() {
    if (originators == null) {
      originators = new LinkedHashMap<String, Integer>();
    }
    return originators;
  }

  @Override
  public Map<String, Integer> getResources() {
    if (resources == null) {
      resources = new LinkedHashMap<String, Integer>();
    }
    return resources;
  }

  @Override
  public Map<String, Object> getAttributes() {
    if (attributes == null) {
      attributes = new TreeMap<String, Object>();
    }
    return attributes;
  }

  @Override
  public Map<String, Integer> getNoOfCaseAttributes() {
    if (noOfCaseAttributes == null) {
      noOfCaseAttributes = new TreeMap<String, Integer>();
    }
    return noOfCaseAttributes;
  }

  @Override
  public Map<String, Integer> getNoOfEventAttributes() {
    if (noOfEventAttributes == null) {
      noOfEventAttributes = new TreeMap<String, Integer>();
    }
    return noOfEventAttributes;
  }

  @Override
  public Map<String, Map<String, Integer>> getCaseAttributes() {
    if (caseAttributes == null) {
      caseAttributes = new TreeMap<String, Map<String, Integer>>();
    }
    return caseAttributes;
  }

  @Override
  public Map<String, Map<String, Integer>> getEventAttributes() {
    if (eventAttributes == null) {
      eventAttributes = new TreeMap<String, Map<String, Integer>>();
    }
    return eventAttributes;
  }

  @Override
  public String getResourceClass() {
    return "repository.BRepository";
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getUri() {
    return uri;
  }

  @Override
  public String getOriginalName() {
    return originalName;
  }

  @Override
  public long getCreatedDate() {
    return createdDate;
  }

  @Override
  public int getNoOfCases() {
    return noOfCases;
  }

  @Override
  public int getNoOfEvents() {
    return noOfEvents;
  }

  @Override
  public int getNoOfActivities() {
    return noOfActivities;
  }

  @Override
  public int getNoOfActivityTypes() {
    return noOfActivityTypes;
  }

  @Override
  public int getNoOfOriginators() {
    return noOfOriginators;
  }

  @Override
  public int getNoOfResourceClasses() {
    return noOfResourceClasses;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Map<String, Map<String, String>> getMapping() {
    return mapping;
  }

  public void setMapping(Map<String, Map<String, String>> mapping) {
    this.mapping = mapping;
  }

  @Override
  public long getTimestampStart() {
    return timestampStart;
  }

  public void setTimestampStart(long timestampStart) {
    this.timestampStart = timestampStart;
  }

  @Override
  public long getTimestampEnd() {
    return timestampEnd;
  }

  public void setTimestampEnd(long timestampEnd) {
    this.timestampEnd = timestampEnd;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }
}
