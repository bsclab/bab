package kr.ac.pusan.bsclab.bab.ws.api.repository;

import java.util.Map;

public interface IRepositoryFilter<I, O> {
  public String getName();

  public Map<String, String> getOptions();

  public O apply(I i);
}
