/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.model;

import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

/**
 * Basic implementation of event <br>
 * <br >
 * See {@link IEvent}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */

public class BEvent implements IEvent {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  public String id;
  public String uri;
  public String label;
  public String type;
  public String originator;
  public long timestamp;
  public String resource;
  public Map<String, Object> attributes;
  public String resourceClass;

  public BEvent() {

  }

  public BEvent(String id, String uri, String label, String type, String originator, long timestamp,
      String resource) {
    this.id = id;
    this.uri = uri;
    this.label = label;
    this.type = type;
    this.originator = originator;
    this.timestamp = timestamp;
    this.resource = resource;
  }

  @Override
  public Map<String, Object> getAttributes() {
    if (attributes == null) {
      attributes = new TreeMap<String, Object>();
    }
    return attributes;
  }

  @Override
  public String getResourceClass() {
    return "repository.BEvent";
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getUri() {
    return uri;
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String getOriginator() {
    if (originator == null)
      originator = "null";
    return originator;
  }

  @Override
  public Long getTimestamp() {
    return timestamp;
  }

  @Override
  public String getResource() {
    if (resource == null)
      resource = "null";
    return resource;
  }

  @Override
  public String toString() {
    return label + " (" + getType() + ")";
  }


  public void setId(String id) {
    this.id = id;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setOriginator(String originator) {
    this.originator = originator;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public void setResource(String resource) {
    this.resource = resource;
  }

  public void setAttributes(Map<String, Object> attributes) {
    if (attributes == null) {
      attributes = new TreeMap<String, Object>();
    }
    this.attributes = attributes;
  }

  public void setResourceClass(String resourceClass) {
    this.resourceClass = resourceClass;
  }

}
