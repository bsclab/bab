package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.BabServiceInfo;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.hello.World;
import kr.ac.pusan.bsclab.bab.ws.api.hello.WorldParameter;

@Controller
public class DocsController extends AbstractServiceController {

	public static final String BASE_URL = BabWebService.BASE_URL + "/docs";

	@CrossOrigin
	@RequestMapping(path = BASE_URL)
	public @ResponseBody Map<String, BabServiceInfo> getIndex(
			ModelMap modelMap, 
			HttpServletRequest request, 
			HttpServletResponse response) {
		Map<String, BabServiceInfo> services = new TreeMap<String, BabServiceInfo>();
		
		return services;
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/world/{workspaceId}/{datasetId}/{repositoryId}")
	public @ResponseBody Response<World> getWorld(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, 
			WorldParameter config,
			ModelMap modelMap, 
			HttpServletRequest request, 
			HttpServletResponse response) {
//		try {
//			
//			if (config == null) {
//				config = new WorldParameter();
//			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPathOnly(workspaceId, datasetId, repositoryId));
//			}
//			
//			ObjectMapper mapper = new ObjectMapper();
//			String configJson = mapper.writeValueAsString(config);
//			Response<World> babResponse = this
//					.submitJob("HelloWorldJob", "ichsanjson", workspaceId,
//					datasetId, repositoryId, configJson, World.class);
//			
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return null;
	}

}
