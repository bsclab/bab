package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(LogId.class)
@Table(name="woo_sf_job_order")
public class JobOrder {
	
	@Id
	@Column(name="COIL_NO")
	String coilNo;

	@Column(name="HISCOI_NO")
	String hiscoiNo;
	
	@Id
	@Column(name="PRC_CD")
	String prcCd;
	
	@Column(name="PRC_CD1")
	String prcCd1;
	
	@Column(name="PRC_REPEAT")
	int prcRepeat;
	
	@Id
	@Column(name="CSEQ")
	int cseq;
	
	@Column(name="THK")
	String thk;
	
	@Column(name="WDT")
	String wdt;
	
	@Column(name="WGT")
	String wgt;
	
	@Column(name="SDT")
	String sdt;
	
	@Column(name="EDT")
	String edt;
	
	@Column(name="CHG_YN")
	String chgYn;
	
	@Column(name="FIRCRT_YMD")
	String fircrtYmd;
	
	@Column(name="PLNPRC_CD")
	String plnprcCd;
	
	@Id
	@Column(name="ORD_NO")
	String ordNo;
	
	@Column(name="ORD_SEQ")
	String ordSeq;
	
	@Column(name="DRTCOI_NO")
	String drtcoiNo;
	
	@Column(name="SPC_NO")
	String spcNo;
	
	@Column(name="PRC_CD_SHORT")
	String prcCdShort;
	
	@Column(name="EMG_NAM")
	String emgNam;
}