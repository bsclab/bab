package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import java.io.Serializable;

public class LogId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String coilNo;
	String prcCd;
	int cseq;
	String ordNo;
}
