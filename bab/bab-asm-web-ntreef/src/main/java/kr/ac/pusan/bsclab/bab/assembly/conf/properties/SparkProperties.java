package kr.ac.pusan.bsclab.bab.assembly.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-spark.properties")
@ConfigurationProperties(prefix="spark")
public class SparkProperties {
	
	private String masterIp;
	private String masterPort;
	private String masterUri;
	private String hiddenApiUri;
	private String hiddenApiPort;
	private String version;
	public String getMasterIp() {
		return masterIp;
	}
	public void setMasterIp(String masterIp) {
		this.masterIp = masterIp;
	}
	public String getMasterPort() {
		return masterPort;
	}
	public void setMasterPort(String masterPort) {
		this.masterPort = masterPort;
	}
	public String getMasterUri() {
		return masterUri;
	}
	public void setMasterUri(String masterUri) {
		this.masterUri = masterUri;
	}
	public String getHiddenApiUri() {
		return hiddenApiUri;
	}
	public void setHiddenApiUri(String hiddenApiUri) {
		this.hiddenApiUri = hiddenApiUri;
	}
	public String getHiddenApiPort() {
		return hiddenApiPort;
	}
	public void setHiddenApiPort(String hiddenApiPort) {
		this.hiddenApiPort = hiddenApiPort;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	

}
