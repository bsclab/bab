package kr.ac.pusan.bsclab.bab.assembly.utils.sqoop;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SqoopProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopConnector;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopElement;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.ConfigValue;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.ConfigValueName;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.DriverConfigInputs;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.SqoopJobFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.from.FromMysqlInputs;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to.ToHdfsCompression;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to.ToHdfsInputs;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to.ToHdfsOutputFormat;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.HdfsLinkFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.Link;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.LinkFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.MysqlLinkFactory;

@Component
public class SqoopElementFactory {
	
	@Autowired
	AppProperties ap;
	
	@Autowired
	SqoopProperties sp;
	
	@Autowired
	HdfsProperties hp;
	
	@Autowired
	SqoopJobFactory sjf;
	
	@Transactional
 	public Link createLink(SqoopConnector sqoopConnector, String linkName) {
 		
		Link link = null;
		LinkFactory lf = null;
		
		switch (sqoopConnector) {
			
		case GENERIC_JDBC_CONNECTOR:
			lf = new MysqlLinkFactory();
			link = lf.createLink(-1
							, linkName
							, sqoopConnector.ordinal()
							, true
							, ap.getName()
							, new Date()
							, ap.getName()
							, new Date()
							, lf.createLinkConfigValues("linkConfig"
									, -1
									, SqoopElement.LINK
									, lf.createInputs(ap.getJdbcDriver()
											, ap.getConnectionString()
											, ap.getDbUsername()
											, ap.getDbPassword()
											, "")));
			break;
		case HDFS_CONNECTOR:
			lf = new HdfsLinkFactory();
			link = lf.createLink(-1
							, linkName
							, sqoopConnector.ordinal()
							, true
							, ap.getName()
							, new Date()
							, ap.getName()
							, new Date()
							, lf.createLinkConfigValues("linkConfig"
									, -1
									, SqoopElement.LINK
									, lf.createInputs(hp.getApiUri())));
			break;
			default :
				break;
		}
		return link;
	}
	
	@Transactional
	public Job createJob(String name
			, String hdfsOutputDir
			, Link fromLink
			, Link toLink
			, String query) {
		
		Job job = null;
		
		InputsFactory fromInputsFactory = null;
		InputsFactory toInputsFactory = null;
		InputsFactory driverConfigInputs = null;
		
		int fromLinkConnectorId = fromLink.getConnectorId();
		int toLinkConnectorId = toLink.getConnectorId();
		if (fromLinkConnectorId == 1
				&& toLinkConnectorId == 3) {
			fromInputsFactory = new FromMysqlInputs();
			toInputsFactory = new ToHdfsInputs();
			driverConfigInputs = new DriverConfigInputs();
			
			
			ConfigValue[] fromCvs =  sjf.createConfigValues(2
					, ConfigValueName.fromJobConfig
					, SqoopElement.JOB
					, fromInputsFactory.createInputs(null
							, null
							, query
							, null
							, "COIL_NO"
							, null
							, null));
			
			
			ConfigValue[] toCvs = sjf.createConfigValues(9
					, ConfigValueName.toJobConfig
					, SqoopElement.JOB
					, toInputsFactory.createInputs(null
							, null
							, ToHdfsOutputFormat.TEXT_FILE
							, ToHdfsCompression.NONE
							, null
							, hdfsOutputDir));
			
			ConfigValue[] driverCvs = sjf.createConfigValues(12
					, ConfigValueName.throttlingConfig
					, SqoopElement.JOB
					, driverConfigInputs.createInputs(""
							, "1"));
			
			job = sjf.createJob(-1
					, name
					, fromLink
					, toLink
					, ap.getName()
					, new Date()
					, ap.getName()
					, new Date()
					, true
					, fromCvs
					, toCvs
					, driverCvs);
		}
		return job;
	}

	public String dbToHdfsQueryBuilder(String resourceTable, Timestamp sdt, Timestamp edt) {
		return sp.getDbToHdfsQuery()
						.replace("${SDT}", sdt.toString())
						.replace("${EDT}", edt.toString())
						.replace("${RESOURCETABLE}", ap.getLogTableName());
				
	}
}
