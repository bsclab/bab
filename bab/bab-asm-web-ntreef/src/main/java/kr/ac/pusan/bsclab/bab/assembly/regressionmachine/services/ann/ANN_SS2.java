package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SS2 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-0.34781963, 1.56024063, -0.38382617, -0.80904824, -0.31761226, -1.26266968
            				, 1.15666068, -3.04863906, -1.17503726, 0.85605055, 0.23736139, -1.56173027
            				, -0.6587255, -0.25498173, -0.09712796, 1.95473862, -1.89578009, 0.96504283
            				, -0.37595248, -0.79416561}, 
            			{0.73394334, 1.67558312, 0.80264276, -0.21646886, -0.28429538, -1.45538807
        					, 0.01486767, -2.53821445, 0.08241399, -0.50133306, 0.79674256, 0.66183132
        					, -0.2147982, -1.86819315, -0.95223767, -1.26910782, -1.28143227, -1.18270063
        					, -0.24091627, -0.25923526},
            			{-0.273092, 1.28844094, 0.28644857, -2.32868218, -1.63913107, 0.94517392
    						, 0.29794407, -0.0921907, 0.27042359, -0.57936943, -0.01316085, -0.46229088
    						, -1.80000961, -1.45778978, 0.05108613, 0.25993797, 0.11801901, 0.32483107
    						, -1.33338404, -0.07389431}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {1.19805002, 1.97923517, -1.36681163, 0.31116083, -0.30294451, -2.16945624
            			, 0.9543969, -0.34010845, 0.98925096, -0.05636287, -1.38923347, -0.09243418
            			, -0.58530539, -0.16487576, -0.81702268, -0.20148322, -1.30786943, -0.2239836
            			, -0.9998672, 1.51286483};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.13078368,
            			-0.97277349,
            			-1.0871141,
            			-0.28450325,
            			-0.39319554,
            			-0.85174656,
            			-0.92048699,
            			 0.85823071,
            			-0.36772323,
            			-0.27581412,
            			-1.11435497,
            			-0.98048246,
            			 0.23729065,
            			 1.36934817,
            			-0.16274385,
            			 1.0091244,
            			 0.00233098,
            			-0.04616751,
            			 0.17404348,
            			 0.69272};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.30680585};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 1.29;
	}

	@Override
	public double getMaxWidth() {
		
		return 443.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 3387.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.39;
	}

	@Override
	public double getMinWidth() {
		
		return 14.0;
	}

	@Override
	public double getMinWeight() {
		
		return 80.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
