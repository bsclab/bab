package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import kr.ac.pusan.bsclab.bab.assembly.domain.info.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueue;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;

@Controller
@EnableScheduling
public class BabWebServiceController extends AbstractServiceController {
	
	public static final String BASE_URL = BabWebService.BASE_URL + "/job";

//	@CrossOrigin
//	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
//	public @ResponseBody String getIndex() {
//		String result = "BAB Web Service v1.0";
//		return result;
//	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/services")
	public @ResponseBody Set<String> getServices() {
		Set<RequestMappingInfo> mappings = this.handlerMapping.getHandlerMethods().keySet();
		Set<String> result = new TreeSet<String>();
		for (RequestMappingInfo map : mappings) {
			StringBuilder reqMethod = new StringBuilder();
			Set<RequestMethod> methods = map.getMethodsCondition().getMethods();
			for (RequestMethod method : methods) {
				reqMethod.append("[");
				reqMethod.append(method);
				reqMethod.append("]");
			}
			reqMethod.append(" ");
			Set<String> patterns = map.getPatternsCondition().getPatterns();
			for (String pattern : patterns) {
				result.add(reqMethod.toString() + pattern);
			}
		}
		return result;
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/queue")
	public @ResponseBody Map<String, JobQueue> getQueue() {
//		logger.debug("jobManager.getManagedJobQueues()={}", jobManager.getManagedJobQueues());
		return jobManager.getManagedJobQueues();
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/queue/{jobQueueId}")
	public @ResponseBody JobQueue getJob(
			@PathVariable("jobQueueId") String jobQueueId) {
		
		return jobManager.getManagedJobQueues().get(jobQueueId);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public @ResponseBody Response<List<Job>> getIndex() {
		
		
		Response<List<Job>> response = new Response<List<Job>>();
		List<Job> jobs = jobDao.findAll();
		
		response.setResponse(jobs);
		
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/report/{jobQueueId}")
	public @ResponseBody JobQueue getReport(
			@PathVariable("jobQueueId") String jobQueueId) {
		
		return jobManager.getManagedJobQueues().get(jobQueueId);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/report/{jobQueueId}/{status}")
	public @ResponseBody JobQueue getReport(
			@PathVariable("jobQueueId") String jobQueueId
			, @PathVariable(value = "status") String status) {
		
		return jobManager.getManagedJobQueues().get(jobQueueId);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/debug/{jobQueueId}")
	public @ResponseBody String postDebug(
			@PathVariable("jobQueueId") String jobQueueId,
			String message,
			HttpServletRequest request
			) {
		String echo = DateUtil.getInstance().toDateTimeString(new Date().getTime()) + " DEBUG REMOTE --- [" + request.getRemoteAddr() + "] " + jobQueueId + " : " + message;
		System.out.println(echo);
		return echo;
	}
	
	/*
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/pull")
	public @ResponseBody JobSubmission getPull() {
		return getJobQueue().pull();
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/report/{jobId}")
	public @ResponseBody JobSubmission getReport(@PathVariable(value = "jobId") String jobId, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		return getReport(jobId, null, modelMap, request, response);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/report/{jobId}/{status}")
	public @ResponseBody JobSubmission getReport(@PathVariable(value = "jobId") String jobId, @PathVariable(value = "status") String status, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		return getJobQueue().report(jobId);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/kill/{jobId}")
	public @ResponseBody JobSubmission getKill(@PathVariable(value = "jobId") String jobId, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		return getJobQueue().kill(jobId);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/queue")
	public @ResponseBody JobQueue getJobs() {
		return getJobQueue();
	}
*/
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/webhdfs")
	public @ResponseBody String postFile(HdfsFileConfiguration hdfsFileConfiguration,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		return webHdfs.openAsTextFile(hdfsFileConfiguration.getPath());
	}

//	@Scheduled(initialDelay = 5000, fixedRate = 15000)
//	public void autoPull() {
//		getJobQueue().pull();
//	}
	
}
