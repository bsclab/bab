package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_CRC implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CRB0120")) return 50;
if(p.equals("CRC1263"))
 if(we <= 2334) return 25;
 if(we > 2334) return 20;
if(p.equals("PP12183")) return 50;
if(p.equals("PP12184"))
 if(t <= 0.91) return 70;
 if(t > 0.91)
  if(t <= 1.01) return 60;
  if(t > 1.01) return 50;
if(p.equals("PP12186"))
 if(t <= 1.1) return 60;
 if(t > 1.1) return 50;
if(p.equals("PP12188")) return 50;
if(p.equals("PP12192"))
 if(w <= 343) return 50;
 if(w > 343) return 40;
if(p.equals("PP12194"))
 if(t <= 1.2)
  if(t <= 0.9)
   if(we <= 4418) return 50;
   if(we > 4418) return 60;
  if(t > 0.9) return 50;
 if(t > 1.2) return 40;
if(p.equals("PP12195")) return 70;
if(p.equals("PP12196"))
 if(t <= 1.01) return 60;
 if(t > 1.01)
  if(t <= 1.22) return 50;
  if(t > 1.22) return 40;
if(p.equals("PP12197"))
 if(we <= 4115) return 50;
 if(we > 4115) return 60;
if(p.equals("PP12198"))
 if(t <= 1.27) return 60;
 if(t > 1.27)
  if(we <= 3976) return 40;
  if(we > 3976) return 50;
if(p.equals("PP12199"))
 if(we <= 4546) return 40;
 if(we > 4546)
  if(w <= 370) return 40;
  if(w > 370) return 50;
if(p.equals("PP12202")) return 40;
if(p.equals("PP12204")) return 40;
if(p.equals("PP12213"))
 if(t <= 0.95) return 60;
 if(t > 0.95) return 50;
if(p.equals("PP12221"))
 if(t <= 0.91) return 70;
 if(t > 0.91) return 50;
if(p.equals("PP12222"))
 if(t <= 0.63) return 70;
 if(t > 0.63) return 60;
if(p.equals("PP12228")) return 50;
if(p.equals("PP12229"))
 if(w <= 335) return 40;
 if(w > 335) return 50;
if(p.equals("PP12230")) return 60;
if(p.equals("PP12232"))
 if(w <= 323) return 40;
 if(w > 323) return 60;
if(p.equals("PP12234"))
 if(we <= 6732)
  if(t <= 0.8)
   if(w <= 504) return 60;
   if(w > 504) return 50;
  if(t > 0.8) return 70;
 if(we > 6732) return 90;
if(p.equals("PP12235")) return 60;
if(p.equals("PP12238"))
 if(w <= 343) return 40;
 if(w > 343) return 30;
if(p.equals("PP12245"))
 if(t <= 1.1) return 60;
 if(t > 1.1) return 50;
if(p.equals("PP12247")) return 50;
if(p.equals("PP12248"))
 if(t <= 1)
  if(we <= 4054) return 40;
  if(we > 4054) return 50;
 if(t > 1) return 50;
if(p.equals("PP12249")) return 50;
if(p.equals("PP12251"))
 if(t <= 1.1) return 60;
 if(t > 1.1) return 50;
if(p.equals("PP12252"))
 if(t <= 1)
  if(w <= 482) return 60;
  if(w > 482) return 70;
 if(t > 1) return 50;
if(p.equals("PP12253"))
 if(w <= 539)
  if(w <= 494) return 70;
  if(w > 494) return 60;
 if(w > 539) return 50;
if(p.equals("PP12256"))
 if(t <= 0.35)
  if(t <= 0.26)
   if(we <= 2334) return 60;
   if(we > 2334)
    if(t <= 0.205) return 110;
    if(t > 0.205) return 90;
  if(t > 0.26)
   if(w <= 313) return 80;
   if(w > 313) return 100;
 if(t > 0.35)
  if(t <= 0.95) return 70;
  if(t > 0.95) return 60;
if(p.equals("PP12257")) return 40;
if(p.equals("PP12262")) return 50;
if(p.equals("PP12263"))
 if(t <= 1.01)
  if(t <= 0.85) return 60;
  if(t > 0.85) return 40;
 if(t > 1.01) return 50;
if(p.equals("PP12267"))
 if(t <= 0.86) return 50;
 if(t > 0.86) return 40;
if(p.equals("PP12273")) return 40;
if(p.equals("PP12275"))
 if(t <= 1.54) return 50;
 if(t > 1.54)
  if(we <= 5750)
   if(we <= 5660) return 40;
   if(we > 5660) return 50;
  if(we > 5750) return 40;
if(p.equals("PP12282"))
 if(t <= 1)
  if(w <= 315) return 50;
  if(w > 315) return 40;
 if(t > 1)
  if(we <= 4185) return 50;
  if(we > 4185)
   if(t <= 1.055) return 60;
   if(t > 1.055) return 40;
if(p.equals("PP12283"))
 if(t <= 1.31) return 60;
 if(t > 1.31) return 50;
if(p.equals("PP12284"))
 if(w <= 440) return 45;
 if(w > 440) return 70;
if(p.equals("PP12285")) return 50;
if(p.equals("PP12286"))
 if(we <= 4542) return 40;
 if(we > 4542) return 50;
if(p.equals("PP12287"))
 if(we <= 7194) return 50;
 if(we > 7194) return 70;
if(p.equals("PP12296")) return 50;
if(p.equals("PP12297")) return 50;
if(p.equals("PP12306")) return 40;
if(p.equals("PP12308")) return 50;
if(p.equals("PP12324")) return 40;
if(p.equals("PP12328"))
 if(t <= 1.01)
  if(w <= 345) return 50;
  if(w > 345) return 60;
 if(t > 1.01)
  if(we <= 4232) return 40;
  if(we > 4232)
   if(w <= 329) return 50;
   if(w > 329)
    if(w <= 333)
     if(t <= 1.21) return 50;
     if(t > 1.21) return 40;
    if(w > 333)
     if(we <= 4678) return 50;
     if(we > 4678) return 40;
if(p.equals("PP12329"))
 if(t <= 1.1) return 60;
 if(t > 1.1) return 40;
if(p.equals("PP12330")) return 70;
if(p.equals("PP12332"))
 if(t <= 1.1)
  if(t <= 0.95) return 70;
  if(t > 0.95) return 60;
 if(t > 1.1)
  if(we <= 3434)
   if(we <= 3423) return 30;
   if(we > 3423) return 40;
  if(we > 3434) return 50;
if(p.equals("PP12336"))
 if(w <= 1829) return 40;
 if(w > 1829) return 50;
if(p.equals("PP12340")) return 50;
if(p.equals("PP12351")) return 60;
if(p.equals("PP12353"))
 if(t <= 0.75) return 50;
 if(t > 0.75) return 60;
if(p.equals("PP12354"))
 if(t <= 1.72) return 20;
 if(t > 1.72) return 40;
if(p.equals("PP12355"))
 if(w <= 1829) return 130;
 if(w > 1829) return 50;
if(p.equals("PP12356"))
 if(we <= 7035) return 80;
 if(we > 7035) return 90;
if(p.equals("PP12357"))
 if(t <= 1.675) return 50;
 if(t > 1.675) return 40;
if(p.equals("PP12358"))
 if(w <= 325)
  if(t <= 0.95) return 60;
  if(t > 0.95)
   if(t <= 1.055) return 60;
   if(t > 1.055) return 50;
 if(w > 325)
  if(w <= 1829) return 40;
  if(w > 1829) return 50;
if(p.equals("PP12360"))
 if(w <= 364) return 50;
 if(w > 364) return 60;
if(p.equals("PP12361")) return 50;
if(p.equals("PP12366")) return 50;
if(p.equals("PP12369"))
 if(we <= 3882) return 40;
 if(we > 3882) return 50;
if(p.equals("PP12377")) return 60;
if(p.equals("PP12378")) return 50;
if(p.equals("PP12383")) return 40;
if(p.equals("PP12384")) return 60;
if(p.equals("PP12388"))
 if(t <= 1.25) return 50;
 if(t > 1.25) return 30;
if(p.equals("PP12389")) return 40;
if(p.equals("PP12390"))
 if(w <= 1829) return 120;
 if(w > 1829) return 50;
if(p.equals("PP12392")) return 50;
if(p.equals("PP12393")) return 40;
if(p.equals("PP12395")) return 50;
if(p.equals("PP12410"))
 if(t <= 1.92) return 50;
 if(t > 1.92) return 40;
if(p.equals("PP12411")) return 50;
if(p.equals("PP12421"))
 if(w <= 515) return 30;
 if(w > 515) return 50;
if(p.equals("PP12422")) return 50;
if(p.equals("PP12423")) return 50;
if(p.equals("PP12431")) return 40;
if(p.equals("PP12432")) return 70;
if(p.equals("PP12435")) return 50;
if(p.equals("PP12436"))
 if(t <= 1.99)
  if(t <= 1.7) return 50;
  if(t > 1.7) return 40;
 if(t > 1.99)
  if(t <= 2.32) return 50;
  if(t > 2.32) return 60;
if(p.equals("PP12437"))
 if(w <= 357)
  if(we <= 3979)
   if(we <= 2794) return 40;
   if(we > 2794)
    if(w <= 341.5) return 30;
    if(w > 341.5) return 40;
  if(we > 3979) return 60;
 if(w > 357) return 50;
if(p.equals("PP12438")) return 50;
if(p.equals("PP12439"))
 if(t <= 1.69) return 50;
 if(t > 1.69) return 20;
if(p.equals("PP12440"))
 if(w <= 369) return 50;
 if(w > 369) return 40;
if(p.equals("PP12443"))
 if(we <= 4206) return 50;
 if(we > 4206) return 60;
if(p.equals("PP12444"))
 if(w <= 517) return 90;
 if(w > 517) return 70;
if(p.equals("PP12446"))
 if(w <= 350) return 60;
 if(w > 350) return 50;
if(p.equals("PP12450")) return 30;
if(p.equals("PP12457")) return 85;
if(p.equals("PP12459")) return 40;
if(p.equals("PP12460")) return 120;
if(p.equals("PP12462"))
 if(w <= 465)
  if(we <= 5592) return 60;
  if(we > 5592)
   if(w <= 407.5) return 50;
   if(w > 407.5)
    if(t <= 2.74) return 55;
    if(t > 2.74) return 50;
 if(w > 465)
  if(w <= 468)
   if(we <= 4494) return 40;
   if(we > 4494) return 50;
  if(w > 468)
   if(t <= 1.93)
    if(we <= 4812)
     if(t <= 1.67)
      if(we <= 4528) return 50;
      if(we > 4528) return 60;
     if(t > 1.67) return 50;
    if(we > 4812)
     if(w <= 480) return 30;
     if(w > 480) return 60;
   if(t > 1.93) return 50;
if(p.equals("PP12469"))
 if(t <= 2.35) return 40;
 if(t > 2.35) return 50;
if(p.equals("PP12470")) return 50;
if(p.equals("PP12479"))
 if(t <= 0.8)
  if(w <= 328)
   if(we <= 4461) return 60;
   if(we > 4461) return 50;
  if(w > 328) return 70;
 if(t > 0.8) return 60;
if(p.equals("PP12480")) return 50;
if(p.equals("PP12484")) return 60;
if(p.equals("PP12486")) return 40;
if(p.equals("PP12491")) return 50;
if(p.equals("PP12501")) return 40;
if(p.equals("PP12508")) return 60;
if(p.equals("PP12510")) return 40;
if(p.equals("PP12513")) return 50;
if(p.equals("PP12519")) return 70;
if(p.equals("PP12525")) return 40;
if(p.equals("PP12530")) return 50;
if(p.equals("PP12531")) return 50;
if(p.equals("PP12538"))
 if(t <= 1.62) return 20;
 if(t > 1.62)
  if(t <= 1.665) return 40;
  if(t > 1.665) return 50;
if(p.equals("PP12544"))
 if(t <= 0.87) return 70;
 if(t > 0.87) return 60;
if(p.equals("PP12545")) return 40;
if(p.equals("PP12546")) return 60;
if(p.equals("PP12550")) return 50;
if(p.equals("PP12554"))
 if(t <= 1.87) return 60;
 if(t > 1.87)
  if(t <= 2.185) return 40;
  if(t > 2.185) return 20;
if(p.equals("PP12558")) return 50;
if(p.equals("PP12565")) return 90;
if(p.equals("PP21894")) return 50;
if(p.equals("PP21895"))
 if(we <= 3477) return 30;
 if(we > 3477) return 40;
if(p.equals("PP21896"))
 if(t <= 2.7)
  if(w <= 413)
   if(w <= 388) return 40;
   if(w > 388) return 30;
  if(w > 413) return 40;
 if(t > 2.7)
  if(w <= 430)
   if(we <= 4404) return 40;
   if(we > 4404)
    if(we <= 4644) return 50;
    if(we > 4644) return 60;
  if(w > 430) return 50;
if(p.equals("PP21901")) return 70;
if(p.equals("PP21904")) return 30;
if(p.equals("PP21906"))
 if(t <= 1.85)
  if(we <= 5744) return 60;
  if(we > 5744) return 50;
 if(t > 1.85) return 40;
if(p.equals("PP21911")) return 30;
if(p.equals("PP21912"))
 if(t <= 1.67)
  if(t <= 1.36)
   if(w <= 482) return 45;
   if(w > 482) return 60;
  if(t > 1.36) return 60;
 if(t > 1.67)
  if(we <= 4875) return 50;
  if(we > 4875)
   if(we <= 6000) return 40;
   if(we > 6000) return 50;
if(p.equals("PP21913"))
 if(we <= 4629)
  if(w <= 359)
   if(t <= 2.15)
    if(we <= 3666) return 40;
    if(we > 3666)
     if(w <= 347) return 40;
     if(w > 347) return 50;
   if(t > 2.15)
    if(w <= 318) return 50;
    if(w > 318) return 40;
  if(w > 359)
   if(w <= 374) return 50;
   if(w > 374)
    if(t <= 1.57)
     if(w <= 393)
      if(we <= 3928)
       if(we <= 3860) return 40;
       if(we > 3860) return 45;
      if(we > 3928)
       if(we <= 4020) return 50;
       if(we > 4020) return 60;
     if(w > 393) return 40;
    if(t > 1.57)
     if(we <= 4376) return 50;
     if(we > 4376) return 40;
 if(we > 4629)
  if(w <= 360)
   if(t <= 1.32)
    if(t <= 1.055) return 60;
    if(t > 1.055) return 50;
   if(t > 1.32) return 20;
  if(w > 360)
   if(we <= 5465) return 50;
   if(we > 5465) return 60;
if(p.equals("PP21914"))
 if(we <= 4490)
  if(t <= 2.38)
   if(t <= 2.33)
    if(t <= 2.21)
     if(we <= 4114)
      if(t <= 2.18)
       if(we <= 3401) return 30;
       if(we > 3401) return 40;
      if(t > 2.18)
       if(we <= 3570) return 40;
       if(we > 3570) return 50;
     if(we > 4114) return 50;
    if(t > 2.21)
     if(we <= 3801)
      if(t <= 2.3) return 30;
      if(t > 2.3) return 40;
     if(we > 3801) return 40;
   if(t > 2.33)
    if(we <= 3552) return 40;
    if(we > 3552) return 30;
  if(t > 2.38)
   if(w <= 417)
    if(t <= 2.46)
     if(w <= 407)
      if(w <= 402)
       if(t <= 2.43) return 40;
       if(t > 2.43)
        if(w <= 385.5) return 40;
        if(w > 385.5)
         if(we <= 4079) return 30;
         if(we > 4079) return 40;
      if(w > 402)
       if(w <= 406)
        if(we <= 3818) return 30;
        if(we > 3818)
         if(we <= 3950)
          if(w <= 405) return 30;
          if(w > 405)
           if(we <= 3825) return 36.6666666666667;
           if(we > 3825) return 50;
         if(we > 3950)
          if(we <= 4098) return 45;
          if(we > 4098) return 30;
       if(w > 406) return 45;
     if(w > 407)
      if(w <= 410) return 50;
      if(w > 410) return 30;
    if(t > 2.46) return 40;
   if(w > 417)
    if(w <= 441)
     if(we <= 4088) return 50;
     if(we > 4088) return 40;
    if(w > 441) return 35;
 if(we > 4490)
  if(t <= 2.48)
   if(t <= 2.41)
    if(we <= 4837) return 60;
    if(we > 4837) return 70;
   if(t > 2.41) return 60;
  if(t > 2.48) return 50;
if(p.equals("PP21917"))
 if(w <= 373)
  if(we <= 3740) return 40;
  if(we > 3740) return 30;
 if(w > 373)
  if(we <= 4396) return 50;
  if(we > 4396)
   if(we <= 4647) return 40;
   if(we > 4647) return 50;
if(p.equals("PP21922"))
 if(t <= 2.56)
  if(t <= 2.555) return 50;
  if(t > 2.555) return 40;
 if(t > 2.56) return 30;
if(p.equals("PP21923"))
 if(t <= 2.52)
  if(t <= 2.45)
   if(t <= 1.57) return 40;
   if(t > 1.57)
    if(t <= 2.02) return 50;
    if(t > 2.02)
     if(we <= 5442) return 50;
     if(we > 5442) return 40;
  if(t > 2.45)
   if(we <= 4731) return 60;
   if(we > 4731) return 50;
 if(t > 2.52)
  if(we <= 4432)
   if(w <= 343)
    if(we <= 3918) return 30;
    if(we > 3918) return 40;
   if(w > 343) return 30;
  if(we > 4432) return 40;
if(p.equals("PP21927")) return 50;
if(p.equals("PP21931"))
 if(we <= 4434) return 120;
 if(we > 4434)
  if(t <= 1.25) return 60;
  if(t > 1.25) return 40;
if(p.equals("PP21932"))
 if(w <= 330)
  if(t <= 3.49) return 100;
  if(t > 3.49) return 30;
 if(w > 330) return 60;
if(p.equals("PP21933")) return 50;
if(p.equals("PP21935"))
 if(t <= 1.01) return 40;
 if(t > 1.01) return 50;
if(p.equals("PP21937"))
 if(t <= 1.82)
  if(w <= 338)
   if(w <= 337) return 50;
   if(w > 337) return 40;
  if(w > 338)
   if(t <= 1.75)
    if(w <= 339) return 60;
    if(w > 339)
     if(we <= 4607) return 50;
     if(we > 4607)
      if(we <= 4960) return 60;
      if(we > 4960) return 50;
   if(t > 1.75) return 50;
 if(t > 1.82)
  if(t <= 2.3)
   if(we <= 4477)
    if(w <= 372)
     if(t <= 1.85) return 30;
     if(t > 1.85)
      if(t <= 2.04) return 40;
      if(t > 2.04)
       if(w <= 369) return 30;
       if(w > 369) return 50;
    if(w > 372)
     if(t <= 2.2) return 50;
     if(t > 2.2) return 30;
   if(we > 4477)
    if(t <= 2.21)
     if(we <= 5042) return 40;
     if(we > 5042)
      if(t <= 2.14) return 50;
      if(t > 2.14) return 40;
    if(t > 2.21)
     if(w <= 359)
      if(we <= 4962) return 30;
      if(we > 4962) return 40;
     if(w > 359)
      if(we <= 4941) return 40;
      if(we > 4941)
       if(we <= 4999) return 50;
       if(we > 4999) return 30;
  if(t > 2.3)
   if(w <= 387) return 60;
   if(w > 387) return 70;
if(p.equals("PP21945")) return 55;
if(p.equals("PP21956"))
 if(w <= 1829) return 30;
 if(w > 1829) return 50;
if(p.equals("PP21957")) return 60;
if(p.equals("PP21958"))
 if(w <= 1829) return 60;
 if(w > 1829) return 50;
if(p.equals("PP21965")) return 50;
if(p.equals("PP21975"))
 if(w <= 454) return 30;
 if(w > 454) return 50;
if(p.equals("PP21994"))
 if(w <= 456)
  if(we <= 4955) return 50;
  if(we > 4955)
   if(w <= 455) return 60;
   if(w > 455)
    if(t <= 2.56)
     if(we <= 5174) return 50;
     if(we > 5174) return 60;
    if(t > 2.56) return 50;
 if(w > 456)
  if(t <= 2.83) return 40;
  if(t > 2.83)
   if(t <= 2.91)
    if(t <= 2.88)
     if(we <= 4445) return 20;
     if(we > 4445) return 50;
    if(t > 2.88) return 30;
   if(t > 2.91)
    if(w <= 536)
     if(w <= 511)
      if(we <= 5392) return 40;
      if(we > 5392) return 60;
     if(w > 511) return 80;
    if(w > 536) return 50;
if(p.equals("PP21996")) return 40;
if(p.equals("PP22001"))
 if(we <= 4497)
  if(t <= 2.1)
   if(t <= 1.93) return 30;
   if(t > 1.93)
    if(w <= 460) return 40;
    if(w > 460) return 50;
  if(t > 2.1)
   if(t <= 2.45) return 40;
   if(t > 2.45) return 30;
 if(we > 4497)
  if(t <= 1.41)
   if(t <= 1.1) return 60;
   if(t > 1.1) return 50;
  if(t > 1.41)
   if(we <= 6085)
    if(t <= 1.66) return 40;
    if(t > 1.66)
     if(t <= 2.41)
      if(we <= 4819) return 50;
      if(we > 4819)
       if(we <= 5162) return 60;
       if(we > 5162) return 50;
     if(t > 2.41)
      if(we <= 5620) return 40;
      if(we > 5620) return 50;
   if(we > 6085) return 55;
if(p.equals("PP22003"))
 if(t <= 1.41) return 60;
 if(t > 1.41)
  if(t <= 2.74)
   if(w <= 528) return 30;
   if(w > 528) return 50;
  if(t > 2.74)
   if(we <= 7045) return 40;
   if(we > 7045) return 50;
if(p.equals("PP22005")) return 50;
if(p.equals("PP22009"))
 if(t <= 1.6) return 50;
 if(t > 1.6) return 40;
if(p.equals("PP22014")) return 50;
if(p.equals("PP22015")) return 55;
if(p.equals("PP22017"))
 if(t <= 2.51) return 40;
 if(t > 2.51) return 50;
if(p.equals("PP22038")) return 50;
if(p.equals("PP22052"))
 if(w <= 346) return 40;
 if(w > 346)
  if(t <= 1.87) return 60;
  if(t > 1.87) return 50;
if(p.equals("PP22054")) return 50;
if(p.equals("PP22056")) return 60;
if(p.equals("PP22066")) return 40;
if(p.equals("PP22067")) return 40;
if(p.equals("PP22071"))
 if(t <= 0.86)
  if(w <= 358)
   if(we <= 4276) return 60;
   if(we > 4276)
    if(t <= 0.795) return 65;
    if(t > 0.795) return 50;
  if(w > 358)
   if(we <= 5302) return 70;
   if(we > 5302) return 130;
 if(t > 0.86) return 50;
if(p.equals("PP22074")) return 70;
if(p.equals("PP22080")) return 50;
if(p.equals("PP22083"))
 if(t <= 1.1) return 60;
 if(t > 1.1) return 40;
if(p.equals("PP22093")) return 50;
if(p.equals("PP22096")) return 50;
if(p.equals("PP22111")) return 80;
if(p.equals("PP22112"))
 if(t <= 0.95) return 70;
 if(t > 0.95) return 60;
if(p.equals("PP22132"))
 if(t <= 0.39) return 90;
 if(t > 0.39)
  if(t <= 0.75) return 65;
  if(t > 0.75) return 120;
if(p.equals("PP22136")) return 80;
if(p.equals("PP22137")) return 50;
if(p.equals("PP22157"))
 if(t <= 1.32) return 60;
 if(t > 1.32)
  if(t <= 1.355) return 60;
  if(t > 1.355) return 40;
if(p.equals("PP22174")) return 50;
if(p.equals("PP22175")) return 50;
if(p.equals("PP22183")) return 60;
if(p.equals("PP22186")) return 50;
if(p.equals("PP22195"))
 if(t <= 1.97) return 50;
 if(t > 1.97)
  if(t <= 2.4) return 60;
  if(t > 2.4) return 40;
if(p.equals("PP22202"))
 if(t <= 0.87)
  if(t <= 0.865) return 70;
  if(t > 0.865) return 40;
 if(t > 0.87) return 50;
if(p.equals("PP22209")) return 60;
if(p.equals("PP22210")) return 180;
if(p.equals("PP22213"))
 if(t <= 1.5) return 20;
 if(t > 1.5)
  if(t <= 1.72) return 30;
  if(t > 1.72) return 40;
if(p.equals("PP22225"))
 if(t <= 1.01) return 50;
 if(t > 1.01) return 40;
if(p.equals("PP22229")) return 60;
if(p.equals("PP22230")) return 60;
if(p.equals("PP22240")) return 80;
if(p.equals("PP22243"))
 if(we <= 7597) return 50;
 if(we > 7597)
  if(w <= 528) return 50;
  if(w > 528) return 60;
if(p.equals("PP22261")) return 110;
if(p.equals("PP22262")) return 70;
if(p.equals("PP22263"))
 if(t <= 0.31) return 110;
 if(t > 0.31)
  if(t <= 0.66) return 70;
  if(t > 0.66) return 60;
if(p.equals("PP22264"))
 if(w <= 1829) return 60;
 if(w > 1829) return 50;
if(p.equals("PP22267")) return 40;
if(p.equals("PP22268")) return 120;
if(p.equals("PP22276"))
 if(t <= 1)
  if(w <= 419) return 60;
  if(w > 419)
   if(t <= 0.71) return 50;
   if(t > 0.71) return 60;
 if(t > 1) return 50;
if(p.equals("PP22277"))
 if(w <= 328) return 50;
 if(w > 328)
  if(t <= 0.71) return 190;
  if(t > 0.71) return 70;
if(p.equals("PP22278")) return 60;
if(p.equals("PP22284")) return 50;
if(p.equals("PP22295")) return 40;
if(p.equals("PP22298")) return 40;
if(p.equals("PP22300")) return 60;
if(p.equals("PP22306")) return 50;
if(p.equals("PP22307")) return 40;
if(p.equals("PP22312"))
 if(t <= 0.53) return 80;
 if(t > 0.53)
  if(t <= 0.95) return 60;
  if(t > 0.95) return 50;
if(p.equals("PP22316")) return 50;
if(p.equals("PP22318")) return 50;
if(p.equals("PP22319")) return 50;
if(p.equals("PP22321"))
 if(t <= 2.45)
  if(we <= 3359) return 40;
  if(we > 3359) return 30;
 if(t > 2.45)
  if(we <= 3426) return 30;
  if(we > 3426) return 40;
if(p.equals("PP22322"))
 if(t <= 2.45) return 40;
 if(t > 2.45)
  if(we <= 5019)
   if(we <= 4544)
    if(t <= 3.455)
     if(w <= 310) return 40;
     if(w > 310) return 50;
    if(t > 3.455) return 30;
   if(we > 4544) return 40;
  if(we > 5019) return 50;
if(p.equals("PP22336"))
 if(we <= 3472)
  if(t <= 2.45) return 30;
  if(t > 2.45)
   if(we <= 3188) return 30;
   if(we > 3188)
    if(we <= 3290) return 40;
    if(we > 3290)
     if(we <= 3359) return 30;
     if(we > 3359) return 40;
 if(we > 3472) return 40;
if(p.equals("PP22339")) return 60;
if(p.equals("PP22340")) return 40;
if(p.equals("PP22342"))
 if(w <= 318) return 45;
 if(w > 318)
  if(t <= 2.02) return 50;
  if(t > 2.02)
   if(w <= 422)
    if(w <= 375)
     if(w <= 323) return 50;
     if(w > 323) return 40;
    if(w > 375) return 50;
   if(w > 422) return 40;
if(p.equals("PP22350")) return 50;
if(p.equals("PP22351"))
 if(w <= 1829) return 30;
 if(w > 1829) return 50;
if(p.equals("PP22352"))
 if(t <= 2.45) return 40;
 if(t > 2.45)
  if(w <= 414)
   if(we <= 5582.5) return 40;
   if(we > 5582.5) return 50;
  if(w > 414) return 50;
if(p.equals("PP22354"))
 if(w <= 485) return 45;
 if(w > 485) return 60;
if(p.equals("PP22355"))
 if(t <= 1.01) return 50;
 if(t > 1.01)
  if(w <= 326)
   if(w <= 315)
    if(we <= 4252) return 60;
    if(we > 4252) return 50;
   if(w > 315) return 40;
  if(w > 326) return 50;
if(p.equals("PP22356")) return 40;
if(p.equals("PP22357")) return 40;
if(p.equals("PP22358")) return 50;
if(p.equals("PP22361")) return 55;
if(p.equals("PP22362")) return 40;
if(p.equals("PP22366")) return 40;
if(p.equals("PP22367"))
 if(t <= 1.36)
  if(we <= 5316)
   if(t <= 1.185) return 70;
   if(t > 1.185) return 50;
  if(we > 5316) return 60;
 if(t > 1.36) return 30;
if(p.equals("PP22369")) return 40;
if(p.equals("PP22380"))
 if(t <= 0.31)
  if(we <= 3408) return 60;
  if(we > 3408) return 110;
 if(t > 0.31)
  if(w <= 1829) return 130;
  if(w > 1829) return 50;
if(p.equals("PP22387")) return 40;
if(p.equals("PP22406")) return 60;
if(p.equals("PP22408"))
 if(t <= 0.66) return 70;
 if(t > 0.66) return 50;
if(p.equals("PP22410")) return 70;
if(p.equals("PP22415"))
 if(t <= 2.56) return 60;
 if(t > 2.56) return 50;
if(p.equals("PP22416")) return 50;
if(p.equals("PP22417"))
 if(w <= 400) return 40;
 if(w > 400)
  if(t <= 2.02)
   if(t <= 1.61) return 60;
   if(t > 1.61)
    if(t <= 2.005) return 65;
    if(t > 2.005) return 40;
  if(t > 2.02)
   if(we <= 5712) return 60;
   if(we > 5712) return 50;
if(p.equals("PP22425")) return 60;
if(p.equals("PP22427")) return 60;
if(p.equals("PP22435")) return 50;
if(p.equals("PP22447")) return 50;
if(p.equals("PP22448"))
 if(w <= 356) return 60;
 if(w > 356)
  if(t <= 2.21) return 50;
  if(t > 2.21)
   if(t <= 2.74) return 60;
   if(t > 2.74) return 50;
if(p.equals("PP22456")) return 50;
if(p.equals("PP22457")) return 70;
if(p.equals("PP22460")) return 90;
if(p.equals("PP22461")) return 70;
if(p.equals("PP22464"))
 if(t <= 2.04) return 50;
 if(t > 2.04) return 40;
if(p.equals("PP22469")) return 40;
if(p.equals("PP22470")) return 70;
if(p.equals("PP22471")) return 70;
if(p.equals("PP22473")) return 40;
if(p.equals("PP22481"))
 if(we <= 4412)
  if(we <= 4398) return 70;
  if(we > 4398) return 40;
 if(we > 4412) return 50;
if(p.equals("PP22484")) return 50;
if(p.equals("PP22485")) return 45;
if(p.equals("PP22487")) return 50;
if(p.equals("PP22488")) return 70;
if(p.equals("PP22489")) return 50;
if(p.equals("PP22491")) return 30;
if(p.equals("PP22498"))
 if(t <= 2.08)
  if(t <= 1.99) return 50;
  if(t > 1.99) return 60;
 if(t > 2.08)
  if(we <= 4008) return 30;
  if(we > 4008)
   if(w <= 388) return 40;
   if(w > 388)
    if(w <= 395) return 50;
    if(w > 395)
     if(t <= 2.115) return 40;
     if(t > 2.115) return 30;
if(p.equals("PP22500")) return 50;
if(p.equals("PP22502")) return 40;
if(p.equals("PP22504")) return 30;
if(p.equals("PP22505"))
 if(t <= 1.42) return 60;
 if(t > 1.42)
  if(we <= 3870) return 40;
  if(we > 3870)
   if(w <= 364) return 50;
   if(w > 364)
    if(we <= 4530) return 40;
    if(we > 4530) return 50;
if(p.equals("PP22507"))
 if(t <= 1.65)
  if(we <= 3912)
   if(t <= 1.635) return 50;
   if(t > 1.635) return 40;
  if(we > 3912) return 60;
 if(t > 1.65)
  if(t <= 1.82)
   if(we <= 3148.5) return 40;
   if(we > 3148.5)
    if(t <= 1.815)
     if(w <= 359.5) return 50;
     if(w > 359.5) return 30;
    if(t > 1.815) return 50;
  if(t > 1.82) return 50;
if(p.equals("PP22508")) return 40;
if(p.equals("PP22509"))
 if(t <= 1.1) return 50;
 if(t > 1.1)
  if(t <= 1.2925) return 50;
  if(t > 1.2925) return 40;
if(p.equals("PP22510")) return 50;
if(p.equals("PP22512")) return 60;
if(p.equals("PP22514"))
 if(w <= 399) return 50;
 if(w > 399)
  if(we <= 3737) return 30;
  if(we > 3737) return 40;
if(p.equals("PP22515")) return 40;
if(p.equals("PP22520")) return 40;
if(p.equals("PP22521")) return 40;
if(p.equals("PP22522")) return 40;
if(p.equals("PP22530")) return 50;
if(p.equals("PP22531"))
 if(we <= 4307)
  if(w <= 457) return 80;
  if(w > 457) return 30;
 if(we > 4307)
  if(w <= 528)
   if(w <= 457) return 30;
   if(w > 457) return 40;
  if(w > 528) return 50;
if(p.equals("PP22540")) return 50;
if(p.equals("PP22541")) return 50;
if(p.equals("PP22553")) return 70;
if(p.equals("PP22558")) return 60;
if(p.equals("PP22570")) return 40;
if(p.equals("PP22571")) return 40;
if(p.equals("PP22574"))
 if(w <= 1829) return 30;
 if(w > 1829) return 50;
if(p.equals("PP22581"))
 if(we <= 6065) return 60;
 if(we > 6065) return 70;
if(p.equals("PP22586")) return 80;
if(p.equals("PP22595")) return 90;
if(p.equals("WSA1253")) return 50;
return 50.0;
}
}
