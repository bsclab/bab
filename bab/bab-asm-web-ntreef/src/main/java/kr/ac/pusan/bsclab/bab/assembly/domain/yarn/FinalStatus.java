package kr.ac.pusan.bsclab.bab.assembly.domain.yarn;

public enum FinalStatus {
	
	UNDEFINED
	, SUCCEEDED
	, FAILED
	, KILLED

}
