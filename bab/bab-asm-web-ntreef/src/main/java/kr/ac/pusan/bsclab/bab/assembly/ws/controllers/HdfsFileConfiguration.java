package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

public class HdfsFileConfiguration {
	private String path = null;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}