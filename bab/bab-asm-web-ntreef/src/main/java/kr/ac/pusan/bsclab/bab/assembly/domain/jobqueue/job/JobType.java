package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job;

public enum JobType {
	
	DATABASE, SQOOP, HDFS, SPARK

}
