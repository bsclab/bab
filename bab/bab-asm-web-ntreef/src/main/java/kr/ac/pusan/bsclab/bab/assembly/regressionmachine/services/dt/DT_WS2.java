package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_WS2 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("OW11009"))
 if(we <= 4166) return 50;
 if(we > 4166)
  if(t <= 1.5245)
   if(w <= 299.585) return 90;
   if(w > 299.585) return 50;
  if(t > 1.5245) return 50;
if(p.equals("PP11004")) return 50;
if(p.equals("PP11008"))
 if(we <= 4523) return 60;
 if(we > 4523)
  if(w <= 312)
   if(we <= 4796) return 40;
   if(we > 4796) return 55;
  if(w > 312)
   if(t <= 3.5)
    if(t <= 1.68) return 50;
    if(t > 1.68) return 40;
   if(t > 3.5)
    if(w <= 356) return 50;
    if(w > 356) return 60;
if(p.equals("PP11010")) return 60;
if(p.equals("PP11012")) return 60;
if(p.equals("PP11036"))
 if(t <= 2.45) return 50;
 if(t > 2.45) return 40;
if(p.equals("PP11037"))
 if(we <= 3260)
  if(w <= 237) return 50;
  if(w > 237) return 80;
 if(we > 3260)
  if(w <= 375) return 50;
  if(w > 375)
   if(t <= 3.85) return 30;
   if(t > 3.85) return 60;
if(p.equals("PP11038")) return 50;
if(p.equals("PP11039"))
 if(w <= 285) return 60;
 if(w > 285) return 40;
if(p.equals("PP11041")) return 40;
if(p.equals("PP11045"))
 if(we <= 3424) return 50;
 if(we > 3424) return 60;
if(p.equals("PP11046"))
 if(we <= 3764)
  if(t <= 2.1) return 50;
  if(t > 2.1) return 60;
 if(we > 3764)
  if(w <= 326.16) return 40;
  if(w > 326.16)
   if(we <= 5192) return 60;
   if(we > 5192)
    if(t <= 2.1) return 50;
    if(t > 2.1) return 40;
if(p.equals("PP11047")) return 40;
if(p.equals("PP11048"))
 if(we <= 3543) return 60;
 if(we > 3543)
  if(w <= 398) return 50;
  if(w > 398) return 60;
if(p.equals("PP11050")) return 50;
if(p.equals("PP11052")) return 60;
if(p.equals("PP11057")) return 40;
if(p.equals("PP11058"))
 if(t <= 1.8)
  if(w <= 300.11)
   if(w <= 299.16)
    if(w <= 210) return 50;
    if(w > 210) return 60;
   if(w > 299.16)
    if(w <= 299.17) return 40;
    if(w > 299.17) return 50;
  if(w > 300.11)
   if(w <= 314)
    if(w <= 300.13) return 70;
    if(w > 300.13)
     if(we <= 4809) return 50;
     if(we > 4809)
      if(we <= 4943) return 40;
      if(we > 4943) return 30;
   if(w > 314)
    if(w <= 332.17)
     if(w <= 331)
      if(w <= 326.16)
       if(w <= 314.08) return 60;
       if(w > 314.08)
        if(w <= 324)
         if(we <= 4501)
          if(w <= 317) return 60;
          if(w > 317) return 40;
         if(we > 4501) return 50;
        if(w > 324) return 60;
      if(w > 326.16)
       if(we <= 5025) return 50;
       if(we > 5025)
        if(we <= 5150) return 50;
        if(we > 5150)
         if(we <= 5179) return 60;
         if(we > 5179) return 30;
     if(w > 331)
      if(w <= 332)
       if(we <= 5086) return 50;
       if(we > 5086)
        if(we <= 5138)
         if(we <= 5100) return 60;
         if(we > 5100) return 40;
        if(we > 5138)
         if(w <= 331.21) return 60;
         if(w > 331.21) return 50;
      if(w > 332) return 70;
    if(w > 332.17)
     if(we <= 5498) return 50;
     if(we > 5498)
      if(w <= 371.15)
       if(w <= 356) return 30;
       if(w > 356) return 60;
      if(w > 371.15)
       if(w <= 374.28)
        if(w <= 372)
         if(w <= 371.23) return 60;
         if(w > 371.23) return 50;
        if(w > 372) return 60;
       if(w > 374.28)
        if(w <= 375.2) return 60;
        if(w > 375.2) return 50;
 if(t > 1.8)
  if(w <= 256)
   if(we <= 2930) return 40;
   if(we > 2930) return 50;
  if(w > 256)
   if(w <= 402)
    if(w <= 276) return 60;
    if(w > 276) return 40;
   if(w > 402) return 50;
if(p.equals("PP11059")) return 60;
if(p.equals("PP11060")) return 60;
if(p.equals("PP11081"))
 if(t <= 2.6)
  if(we <= 3063) return 50;
  if(we > 3063)
   if(w <= 390) return 60;
   if(w > 390) return 50;
 if(t > 2.6) return 40;
if(p.equals("PP11105")) return 50;
if(p.equals("PP11107"))
 if(t <= 1.75)
  if(w <= 1174)
   if(we <= 5261) return 60;
   if(we > 5261) return 40;
  if(w > 1174) return 50;
 if(t > 1.75)
  if(we <= 5308)
   if(t <= 2.002) return 50;
   if(t > 2.002) return 10;
  if(we > 5308) return 60;
if(p.equals("PP11137")) return 65;
if(p.equals("PP11261")) return 50;
if(p.equals("PP11264"))
 if(we <= 3678) return 50;
 if(we > 3678) return 70;
if(p.equals("PP11266"))
 if(we <= 2333)
  if(w <= 362)
   if(t <= 0.86)
    if(we <= 1008.5) return 90;
    if(we > 1008.5) return 120;
   if(t > 0.86)
    if(we <= 2009)
     if(we <= 1841) return 100;
     if(we > 1841) return 105;
    if(we > 2009)
     if(we <= 2052) return 70;
     if(we > 2052) return 80;
  if(w > 362)
   if(w <= 375.21)
    if(we <= 2233) return 150;
    if(we > 2233) return 100;
   if(w > 375.21) return 120;
 if(we > 2333)
  if(we <= 2704)
   if(t <= 0.86)
    if(w <= 358)
     if(we <= 2593) return 80;
     if(we > 2593) return 85;
    if(w > 358)
     if(w <= 375.21) return 87.15;
     if(w > 375.21) return 110.916666666667;
   if(t > 0.86)
    if(w <= 407)
     if(we <= 2473) return 70;
     if(we > 2473)
      if(we <= 2560)
       if(w <= 213.5) return 50;
       if(w > 213.5) return 114;
      if(we > 2560) return 80;
    if(w > 407) return 100;
  if(we > 2704)
   if(t <= 1.05)
    if(t <= 0.86)
     if(we <= 2772) return 120;
     if(we > 2772) return 100;
    if(t > 0.86)
     if(w <= 407) return 66;
     if(w > 407) return 113.333333333333;
   if(t > 1.05)
    if(t <= 1.25) return 60;
    if(t > 1.25) return 90;
if(p.equals("PP11275")) return 60;
if(p.equals("PP11287")) return 140;
if(p.equals("PP11312")) return 120;
if(p.equals("PP11443")) return 50;
if(p.equals("PP11539")) return 50;
if(p.equals("PP11659")) return 50;
if(p.equals("PP11683"))
 if(t <= 1.1)
  if(t <= 0.42)
   if(we <= 2465)
    if(w <= 318) return 140;
    if(w > 318)
     if(we <= 2407)
      if(w <= 335)
       if(we <= 1926) return 110;
       if(we > 1926) return 220;
      if(w > 335)
       if(w <= 351) return 160;
       if(w > 351)
        if(we <= 2124) return 220;
        if(we > 2124)
         if(w <= 369) return 160;
         if(w > 369) return 110;
     if(we > 2407) return 96.6666666666667;
   if(we > 2465)
    if(we <= 2951)
     if(w <= 358)
      if(we <= 2529)
       if(we <= 2506) return 180;
       if(we > 2506) return 60;
      if(we > 2529) return 180;
     if(w > 358) return 140;
    if(we > 2951)
     if(we <= 3942) return 120;
     if(we > 3942) return 220;
  if(t > 0.42)
   if(w <= 350)
    if(we <= 2375) return 110;
    if(we > 2375)
     if(we <= 3195) return 160;
     if(we > 3195) return 110;
   if(w > 350) return 150;
 if(t > 1.1)
  if(we <= 16700) return 60;
  if(we > 16700)
   if(we <= 21051)
    if(w <= 1231.5) return 40;
    if(w > 1231.5) return 50;
   if(we > 21051) return 50;
if(p.equals("PP11686")) return 40;
if(p.equals("PP11884")) return 40;
if(p.equals("PP11892")) return 60;
if(p.equals("PP11894"))
 if(w <= 285)
  if(we <= 1450) return 220;
  if(we > 1450)
   if(we <= 4277) return 100;
   if(we > 4277) return 50;
 if(w > 285)
  if(t <= 2.004) return 70;
  if(t > 2.004) return 50;
if(p.equals("PP11910")) return 40;
if(p.equals("PP11912")) return 120;
if(p.equals("PP11917")) return 100;
if(p.equals("PP11931"))
 if(we <= 4437)
  if(we <= 4102) return 60;
  if(we > 4102) return 35;
 if(we > 4437) return 50;
if(p.equals("PP11932"))
 if(t <= 1.05)
  if(t <= 0.81) return 50;
  if(t > 0.81)
   if(t <= 1) return 40;
   if(t > 1) return 80;
 if(t > 1.05)
  if(t <= 1.195)
   if(we <= 5561)
    if(we <= 4579) return 60;
    if(we > 4579) return 40;
   if(we > 5561) return 50;
  if(t > 1.195)
   if(we <= 4995)
    if(we <= 4354)
     if(w <= 337.4) return 60;
     if(w > 337.4) return 50;
    if(we > 4354) return 60;
   if(we > 4995)
    if(w <= 337.4) return 40;
    if(w > 337.4)
     if(w <= 363)
      if(we <= 5306) return 60;
      if(we > 5306) return 55;
     if(w > 363) return 40;
if(p.equals("PP11954"))
 if(w <= 231) return 100;
 if(w > 231)
  if(t <= 0.9)
   if(t <= 0.79)
    if(we <= 2381)
     if(we <= 2315) return 110;
     if(we > 2315) return 100;
    if(we > 2381)
     if(we <= 2443) return 80;
     if(we > 2443) return 110;
   if(t > 0.79) return 80;
  if(t > 0.9) return 70;
if(p.equals("PP11984")) return 60;
if(p.equals("PP11988")) return 50;
if(p.equals("PP11991"))
 if(t <= 1.1)
  if(we <= 2324) return 90;
  if(we > 2324) return 150;
 if(t > 1.1) return 50;
if(p.equals("PP12002")) return 40;
if(p.equals("PP12011")) return 60;
if(p.equals("PP12026"))
 if(t <= 3.1) return 50;
 if(t > 3.1) return 60;
if(p.equals("PP12035")) return 50;
if(p.equals("PP12037")) return 50;
if(p.equals("PP12047")) return 50;
if(p.equals("PP12095"))
 if(we <= 4979)
  if(we <= 4750)
   if(we <= 4405)
    if(w <= 332) return 30;
    if(w > 332) return 60;
   if(we > 4405)
    if(w <= 330.25)
     if(w <= 328.1)
      if(we <= 4593)
       if(we <= 4412.5) return 50;
       if(we > 4412.5) return 60;
      if(we > 4593) return 30;
     if(w > 328.1) return 60;
    if(w > 330.25)
     if(w <= 332)
      if(we <= 4598)
       if(we <= 4447) return 50;
       if(we > 4447) return 40;
      if(we > 4598)
       if(w <= 331.21)
        if(we <= 4675) return 50;
        if(we > 4675) return 30;
       if(w > 331.21)
        if(we <= 4723)
         if(we <= 4681) return 50;
         if(we > 4681) return 40;
        if(we > 4723) return 50;
     if(w > 332)
      if(w <= 332.17) return 60;
      if(w > 332.17)
       if(w <= 359) return 50;
       if(w > 359) return 60;
  if(we > 4750)
   if(w <= 325)
    if(w <= 320)
     if(w <= 315)
      if(we <= 4814) return 50;
      if(we > 4814)
       if(we <= 4840) return 80;
       if(we > 4840) return 70;
     if(w > 315)
      if(we <= 4809) return 40;
      if(we > 4809) return 50;
    if(w > 320) return 90;
   if(w > 325)
    if(we <= 4949)
     if(we <= 4796) return 40;
     if(we > 4796)
      if(w <= 330.06)
       if(we <= 4882) return 30;
       if(we > 4882) return 40;
      if(w > 330.06)
       if(we <= 4929)
        if(we <= 4833)
         if(w <= 332.17) return 37.5;
         if(w > 332.17) return 50;
        if(we > 4833)
         if(w <= 345.1)
          if(w <= 330.25)
           if(we <= 4888) return 50;
           if(we > 4888) return 60;
          if(w > 330.25)
           if(w <= 331.21) return 40;
           if(w > 331.21)
            if(we <= 4867) return 50;
            if(we > 4867)
             if(we <= 4874) return 40;
             if(we > 4874)
              if(we <= 4924) return 30;
              if(we > 4924) return 40;
         if(w > 345.1) return 60;
       if(we > 4929) return 30;
    if(we > 4949)
     if(we <= 4973)
      if(w <= 330.25) return 60;
      if(w > 330.25) return 40;
     if(we > 4973) return 45;
 if(we > 4979)
  if(w <= 345.1)
   if(we <= 5272)
    if(w <= 332.17)
     if(w <= 331.21)
      if(w <= 327)
       if(we <= 5096) return 35;
       if(we > 5096) return 50;
      if(w > 327)
       if(w <= 329) return 60;
       if(w > 329)
        if(we <= 5081)
         if(we <= 5021) return 20;
         if(we > 5021)
          if(we <= 5066) return 50;
          if(we > 5066) return 25;
        if(we > 5081)
         if(we <= 5138)
          if(we <= 5102) return 60;
          if(we > 5102) return 70;
         if(we > 5138) return 50;
     if(w > 331.21)
      if(we <= 5073)
       if(we <= 5036)
        if(we <= 5005) return 60;
        if(we > 5005)
         if(we <= 5029) return 50;
         if(we > 5029) return 40;
       if(we > 5036) return 50;
      if(we > 5073)
       if(we <= 5144)
        if(we <= 5117) return 80;
        if(we > 5117) return 70;
       if(we > 5144) return 50;
    if(w > 332.17) return 50;
   if(we > 5272)
    if(w <= 345)
     if(we <= 5298)
      if(we <= 5285) return 40;
      if(we > 5285) return 25;
     if(we > 5298)
      if(we <= 5319) return 60;
      if(we > 5319) return 40;
    if(w > 345) return 60;
  if(w > 345.1)
   if(w <= 397)
    if(w <= 373)
     if(we <= 5342)
      if(we <= 5201) return 50;
      if(we > 5201)
       if(we <= 5298) return 40;
       if(we > 5298) return 50;
     if(we > 5342)
      if(w <= 351) return 40;
      if(w > 351)
       if(w <= 372.13)
        if(we <= 5664)
         if(we <= 5396)
          if(we <= 5369) return 45;
          if(we > 5369) return 40;
         if(we > 5396) return 50;
        if(we > 5664) return 40;
       if(w > 372.13)
        if(we <= 5688) return 45;
        if(we > 5688) return 50;
    if(w > 373)
     if(we <= 5675)
      if(w <= 374.28)
       if(w <= 374.11) return 50;
       if(w > 374.11) return 60;
      if(w > 374.28)
       if(w <= 375.21)
        if(we <= 5292) return 50;
        if(we > 5292) return 30;
       if(w > 375.21) return 50;
     if(we > 5675)
      if(we <= 5851)
       if(w <= 374) return 50;
       if(w > 374) return 60;
      if(we > 5851)
       if(w <= 390) return 40;
       if(w > 390)
        if(we <= 6145) return 50;
        if(we > 6145) return 40;
   if(w > 397)
    if(w <= 484) return 40;
    if(w > 484)
     if(w <= 509)
      if(w <= 498)
       if(we <= 7044)
        if(we <= 7005) return 50;
        if(we > 7005) return 70;
       if(we > 7044)
        if(we <= 7080) return 30;
        if(we > 7080) return 40;
      if(w > 498) return 50;
     if(w > 509)
      if(we <= 7323) return 30;
      if(we > 7323) return 40;
if(p.equals("PP12129")) return 30;
if(p.equals("PP12130"))
 if(we <= 3621) return 80;
 if(we > 3621)
  if(we <= 4766)
   if(we <= 4474) return 40;
   if(we > 4474) return 50;
  if(we > 4766) return 40;
if(p.equals("PP12141")) return 50;
if(p.equals("PP12146"))
 if(w <= 612)
  if(t <= 2.1) return 50;
  if(t > 2.1)
   if(we <= 12027)
    if(we <= 11698) return 60;
    if(we > 11698) return 50;
   if(we > 12027) return 60;
 if(w > 612) return 50;
if(p.equals("PP12158"))
 if(w <= 322.5) return 40;
 if(w > 322.5)
  if(w <= 369) return 60;
  if(w > 369)
   if(we <= 3241) return 50;
   if(we > 3241) return 60;
if(p.equals("PP12164")) return 50;
if(p.equals("PP12167"))
 if(t <= 0.9)
  if(w <= 560) return 125;
  if(w > 560)
   if(we <= 3997)
    if(we <= 3025) return 20;
    if(we > 3025) return 5;
   if(we > 3997)
    if(we <= 9098) return 120;
    if(we > 9098) return 100;
 if(t > 0.9)
  if(we <= 24590) return 50;
  if(we > 24590) return 40;
if(p.equals("PP12178"))
 if(we <= 5278) return 50;
 if(we > 5278) return 40;
if(p.equals("PP12183")) return 40;
if(p.equals("PP12184"))
 if(w <= 343)
  if(w <= 337)
   if(w <= 328.1) return 50;
   if(w > 328.1) return 60;
  if(w > 337) return 50;
 if(w > 343)
  if(w <= 350) return 40;
  if(w > 350)
   if(w <= 358)
    if(t <= 2.65) return 45;
    if(t > 2.65) return 40;
   if(w > 358) return 60;
if(p.equals("PP12186"))
 if(w <= 340)
  if(w <= 215) return 40;
  if(w > 215) return 50;
 if(w > 340) return 40;
if(p.equals("PP12187")) return 60;
if(p.equals("PP12188"))
 if(w <= 322) return 60;
 if(w > 322) return 50;
if(p.equals("PP12190")) return 50;
if(p.equals("PP12192"))
 if(w <= 315) return 40;
 if(w > 315) return 50;
if(p.equals("PP12193"))
 if(w <= 230)
  if(w <= 220)
   if(w <= 215) return 40;
   if(w > 215) return 50;
  if(w > 220)
   if(w <= 223) return 40;
   if(w > 223)
    if(w <= 226) return 60;
    if(w > 226) return 45;
 if(w > 230) return 50;
if(p.equals("PP12194"))
 if(w <= 315) return 40;
 if(w > 315)
  if(we <= 3762) return 50;
  if(we > 3762) return 60;
if(p.equals("PP12195")) return 50;
if(p.equals("PP12196"))
 if(we <= 5050) return 50;
 if(we > 5050)
  if(we <= 5492) return 40;
  if(we > 5492) return 50;
if(p.equals("PP12197")) return 50;
if(p.equals("PP12198"))
 if(t <= 2.45)
  if(t <= 2.1)
   if(w <= 326.16) return 50;
   if(w > 326.16)
    if(we <= 4882) return 60;
    if(we > 4882) return 50;
  if(t > 2.1) return 50;
 if(t > 2.45)
  if(t <= 2.6)
   if(w <= 324)
    if(we <= 4224) return 40;
    if(we > 4224) return 50;
   if(w > 324)
    if(we <= 4458) return 40;
    if(we > 4458)
     if(we <= 4516) return 50;
     if(we > 4516)
      if(we <= 8876)
       if(w <= 566) return 40;
       if(w > 566) return 50;
      if(we > 8876) return 50;
  if(t > 2.6)
   if(t <= 3.1)
    if(t <= 2.8)
     if(w <= 378)
      if(w <= 350) return 30;
      if(w > 350)
       if(w <= 369)
        if(we <= 3553) return 40;
        if(we > 3553)
         if(we <= 3746)
          if(w <= 360) return 50;
          if(w > 360)
           if(w <= 361) return 40;
           if(w > 361)
            if(we <= 3650) return 50;
            if(we > 3650)
             if(we <= 3687) return 40;
             if(we > 3687) return 50;
         if(we > 3746) return 40;
       if(w > 369) return 40;
     if(w > 378)
      if(we <= 3573) return 30;
      if(we > 3573) return 50;
    if(t > 2.8) return 40;
   if(t > 3.1) return 50;
if(p.equals("PP12199"))
 if(t <= 2.45) return 50;
 if(t > 2.45)
  if(t <= 2.9) return 60;
  if(t > 2.9) return 50;
if(p.equals("PP12201")) return 45;
if(p.equals("PP12202"))
 if(t <= 2.9) return 40;
 if(t > 2.9)
  if(we <= 3236) return 60;
  if(we > 3236) return 30;
if(p.equals("PP12204")) return 50;
if(p.equals("PP12205")) return 50;
if(p.equals("PP12206"))
 if(w <= 289) return 50;
 if(w > 289) return 60;
if(p.equals("PP12213")) return 50;
if(p.equals("PP12218"))
 if(we <= 3293) return 61.1666666666667;
 if(we > 3293) return 58.75;
if(p.equals("PP12221")) return 40;
if(p.equals("PP12222")) return 60;
if(p.equals("PP12226")) return 60;
if(p.equals("PP12227")) return 50;
if(p.equals("PP12228")) return 40;
if(p.equals("PP12229"))
 if(we <= 4259) return 30;
 if(we > 4259) return 50;
if(p.equals("PP12230"))
 if(we <= 4501) return 40;
 if(we > 4501) return 50;
if(p.equals("PP12231"))
 if(w <= 436) return 60;
 if(w > 436) return 50;
if(p.equals("PP12232")) return 40;
if(p.equals("PP12233")) return 50;
if(p.equals("PP12234")) return 50;
if(p.equals("PP12235")) return 50;
if(p.equals("PP12237")) return 50;
if(p.equals("PP12238")) return 60;
if(p.equals("PP12239"))
 if(we <= 3593) return 50;
 if(we > 3593) return 30;
if(p.equals("PP12245"))
 if(w <= 315)
  if(we <= 4384) return 50;
  if(we > 4384) return 40;
 if(w > 315) return 60;
if(p.equals("PP12247")) return 60;
if(p.equals("PP12248"))
 if(w <= 314.19) return 50;
 if(w > 314.19)
  if(we <= 4115) return 30;
  if(we > 4115) return 40;
if(p.equals("PP12249")) return 50;
if(p.equals("PP12251"))
 if(t <= 3.45)
  if(w <= 515.5) return 40;
  if(w > 515.5) return 50;
 if(t > 3.45) return 60;
if(p.equals("PP12252")) return 50;
if(p.equals("PP12253"))
 if(we <= 7437)
  if(w <= 495) return 40;
  if(w > 495) return 30;
 if(we > 7437) return 50;
if(p.equals("PP12256"))
 if(w <= 325) return 50;
 if(w > 325) return 60;
if(p.equals("PP12257"))
 if(we <= 2480)
  if(w <= 214)
   if(w <= 209)
    if(w <= 208) return 40;
    if(w > 208) return 30;
   if(w > 209)
    if(we <= 2082)
     if(we <= 2072) return 40;
     if(we > 2072) return 25;
    if(we > 2082) return 50;
  if(w > 214) return 40;
 if(we > 2480)
  if(t <= 2.45) return 50;
  if(t > 2.45)
   if(we <= 2619) return 50;
   if(we > 2619)
    if(w <= 307)
     if(w <= 259) return 40;
     if(w > 259) return 60;
    if(w > 307) return 40;
if(p.equals("PP12262")) return 50;
if(p.equals("PP12263")) return 50;
if(p.equals("PP12266"))
 if(w <= 268)
  if(t <= 3.35) return 40;
  if(t > 3.35) return 60;
 if(w > 268) return 50;
if(p.equals("PP12267"))
 if(w <= 305) return 40;
 if(w > 305) return 50;
if(p.equals("PP12269")) return 50;
if(p.equals("PP12273")) return 60;
if(p.equals("PP12275"))
 if(w <= 478) return 50;
 if(w > 478)
  if(we <= 4776) return 40;
  if(we > 4776)
   if(we <= 4835) return 25;
   if(we > 4835) return 50;
if(p.equals("PP12280")) return 40;
if(p.equals("PP12282"))
 if(w <= 315) return 50;
 if(w > 315) return 40;
if(p.equals("PP12283"))
 if(we <= 4823)
  if(we <= 4221) return 40;
  if(we > 4221) return 50;
 if(we > 4823) return 40;
if(p.equals("PP12285")) return 40;
if(p.equals("PP12286"))
 if(w <= 331.21)
  if(w <= 263) return 40;
  if(w > 263) return 30;
 if(w > 331.21) return 40;
if(p.equals("PP12287")) return 40;
if(p.equals("PP12289")) return 50;
if(p.equals("PP12296")) return 50;
if(p.equals("PP12297")) return 40;
if(p.equals("PP12298")) return 50;
if(p.equals("PP12302"))
 if(t <= 2.9) return 40;
 if(t > 2.9) return 50;
if(p.equals("PP12306")) return 40;
if(p.equals("PP12307")) return 40;
if(p.equals("PP12308")) return 60;
if(p.equals("PP12313"))
 if(w <= 225)
  if(t <= 2.8) return 50;
  if(t > 2.8) return 40;
 if(w > 225)
  if(w <= 237)
   if(we <= 2438)
    if(t <= 2.8) return 40;
    if(t > 2.8)
     if(we <= 2287) return 50;
     if(we > 2287) return 60;
   if(we > 2438) return 60;
  if(w > 237) return 50;
if(p.equals("PP12315")) return 50;
if(p.equals("PP12319")) return 40;
if(p.equals("PP12324")) return 60;
if(p.equals("PP12325")) return 50;
if(p.equals("PP12326")) return 50;
if(p.equals("PP12327"))
 if(w <= 256.5)
  if(we <= 3127) return 50;
  if(we > 3127) return 40;
 if(w > 256.5) return 50;
if(p.equals("PP12328")) return 50;
if(p.equals("PP12329")) return 40;
if(p.equals("PP12330")) return 50;
if(p.equals("PP12332")) return 60;
if(p.equals("PP12336")) return 40;
if(p.equals("PP12340"))
 if(we <= 3408)
  if(w <= 207) return 45;
  if(w > 207)
   if(w <= 241) return 60;
   if(w > 241) return 50;
 if(we > 3408)
  if(w <= 317) return 50;
  if(w > 317) return 45;
if(p.equals("PP12342"))
 if(we <= 3284) return 40;
 if(we > 3284) return 60;
if(p.equals("PP12344")) return 50;
if(p.equals("PP12349")) return 50;
if(p.equals("PP12350")) return 50;
if(p.equals("PP12351")) return 40;
if(p.equals("PP12352")) return 50;
if(p.equals("PP12353"))
 if(w <= 385.18) return 45;
 if(w > 385.18) return 60;
if(p.equals("PP12354")) return 60;
if(p.equals("PP12355")) return 50;
if(p.equals("PP12356")) return 40;
if(p.equals("PP12357"))
 if(t <= 2.1) return 50;
 if(t > 2.1) return 40;
if(p.equals("PP12358")) return 60;
if(p.equals("PP12359"))
 if(we <= 3127) return 60;
 if(we > 3127)
  if(we <= 3464) return 70;
  if(we > 3464) return 50;
if(p.equals("PP12360")) return 40;
if(p.equals("PP12361")) return 50;
if(p.equals("PP12362")) return 50;
if(p.equals("PP12363")) return 40;
if(p.equals("PP12364")) return 60;
if(p.equals("PP12366")) return 50;
if(p.equals("PP12369"))
 if(w <= 371) return 50;
 if(w > 371)
  if(t <= 2.9) return 40;
  if(t > 2.9) return 50;
if(p.equals("PP12370")) return 40;
if(p.equals("PP12373")) return 40;
if(p.equals("PP12376")) return 50;
if(p.equals("PP12377")) return 50;
if(p.equals("PP12378")) return 50;
if(p.equals("PP12383")) return 60;
if(p.equals("PP12384")) return 50;
if(p.equals("PP12388")) return 60;
if(p.equals("PP12389")) return 60;
if(p.equals("PP12390")) return 60;
if(p.equals("PP12394")) return 60;
if(p.equals("PP12395")) return 50;
if(p.equals("PP12399")) return 50;
if(p.equals("PP12408")) return 40;
if(p.equals("PP12409")) return 50;
if(p.equals("PP12411")) return 60;
if(p.equals("PP12413")) return 60;
if(p.equals("PP12414")) return 60;
if(p.equals("PP12421")) return 40;
if(p.equals("PP12422"))
 if(we <= 4532) return 50;
 if(we > 4532) return 40;
if(p.equals("PP12423")) return 50;
if(p.equals("PP12424")) return 50;
if(p.equals("PP12428")) return 50;
if(p.equals("PP12429")) return 50;
if(p.equals("PP12430")) return 50;
if(p.equals("PP12431")) return 50;
if(p.equals("PP12432")) return 50;
if(p.equals("PP12433")) return 50;
if(p.equals("PP12435"))
 if(we <= 4673)
  if(we <= 4647) return 40;
  if(we > 4647) return 50;
 if(we > 4673) return 60;
if(p.equals("PP12437"))
 if(we <= 5416) return 40;
 if(we > 5416)
  if(we <= 5566) return 40;
  if(we > 5566) return 50;
if(p.equals("PP12438")) return 50;
if(p.equals("PP12439")) return 50;
if(p.equals("PP12440"))
 if(we <= 5378) return 50;
 if(we > 5378) return 40;
if(p.equals("PP12443")) return 50;
if(p.equals("PP12444")) return 40;
if(p.equals("PP12445"))
 if(we <= 4384) return 40;
 if(we > 4384) return 50;
if(p.equals("PP12455"))
 if(we <= 858) return 30;
 if(we > 858) return 90;
if(p.equals("PP12457")) return 40;
if(p.equals("PP12459")) return 50;
if(p.equals("PP12460")) return 50;
if(p.equals("PP12461")) return 60;
if(p.equals("PP12462")) return 60;
if(p.equals("PP12464")) return 80;
if(p.equals("PP12466")) return 50;
if(p.equals("PP12468")) return 40;
if(p.equals("PP12471")) return 40;
if(p.equals("PP12472"))
 if(t <= 1.2) return 70;
 if(t > 1.2) return 80;
if(p.equals("PP12479")) return 40;
if(p.equals("PP12480")) return 60;
if(p.equals("PP12484")) return 40;
if(p.equals("PP12485")) return 40;
if(p.equals("PP12488")) return 40;
if(p.equals("PP12491")) return 40;
if(p.equals("PP12499")) return 40;
if(p.equals("PP12500")) return 40;
if(p.equals("PP12506")) return 50;
if(p.equals("PP12508")) return 40;
if(p.equals("PP12510")) return 60;
if(p.equals("PP12519")) return 50;
if(p.equals("PP12521")) return 50;
if(p.equals("PP12524")) return 40;
if(p.equals("PP12525")) return 50;
if(p.equals("PP12526"))
 if(we <= 3543) return 40;
 if(we > 3543) return 50;
if(p.equals("PP12528")) return 40;
if(p.equals("PP12529")) return 40;
if(p.equals("PP12530")) return 30;
if(p.equals("PP12533")) return 50;
if(p.equals("PP12537")) return 50;
if(p.equals("PP12538")) return 50;
if(p.equals("PP12544"))
 if(w <= 273)
  if(t <= 3.35) return 45;
  if(t > 3.35) return 60;
 if(w > 273) return 50;
if(p.equals("PP12545")) return 50;
if(p.equals("PP12546")) return 60;
if(p.equals("PP12553")) return 60;
if(p.equals("PP12554")) return 50;
if(p.equals("PP12555")) return 50;
if(p.equals("PP12558")) return 35;
if(p.equals("PP12559")) return 50;
if(p.equals("PP12563"))
 if(t <= 2.8) return 50;
 if(t > 2.8)
  if(t <= 3.1)
   if(we <= 9293) return 60;
   if(we > 9293)
    if(w <= 541) return 50;
    if(w > 541)
     if(we <= 13630) return 60;
     if(we > 13630) return 50;
  if(t > 3.1) return 50;
if(p.equals("PP12565")) return 60;
if(p.equals("PP12575")) return 50;
if(p.equals("PP12578")) return 50;
if(p.equals("PP12598")) return 60;
if(p.equals("PP21028")) return 50;
if(p.equals("PP21039")) return 50;
if(p.equals("PP21045")) return 60;
if(p.equals("PP21089"))
 if(t <= 1.1)
  if(we <= 1000) return 90;
  if(we > 1000)
   if(we <= 1070) return 120;
   if(we > 1070) return 50;
 if(t > 1.1) return 40;
if(p.equals("PP21090")) return 70;
if(p.equals("PP21092"))
 if(t <= 1.9)
  if(t <= 1.519)
   if(w <= 84.9) return 80;
   if(w > 84.9)
    if(we <= 637) return 65;
    if(we > 637) return 56.6666666666667;
  if(t > 1.519)
   if(we <= 1357)
    if(we <= 1074) return 160;
    if(we > 1074) return 60;
   if(we > 1357) return 70;
 if(t > 1.9)
  if(t <= 3.3) return 90;
  if(t > 3.3)
   if(t <= 3.87) return 80;
   if(t > 3.87) return 50;
if(p.equals("PP21105")) return 100;
if(p.equals("PP21107"))
 if(we <= 12156) return 60;
 if(we > 12156) return 40;
if(p.equals("PP21116")) return 40;
if(p.equals("PP21187")) return 70;
if(p.equals("PP21190")) return 60;
if(p.equals("PP21208")) return 50;
if(p.equals("PP21292"))
 if(t <= 1.4) return 60;
 if(t > 1.4) return 50;
if(p.equals("PP21306"))
 if(w <= 1160.3) return 30;
 if(w > 1160.3) return 40;
if(p.equals("PP21312"))
 if(w <= 725)
  if(we <= 1913) return 140;
  if(we > 1913)
   if(t <= 3.3)
    if(t <= 1.15) return 70;
    if(t > 1.15) return 80;
   if(t > 3.3) return 50;
 if(w > 725) return 30;
if(p.equals("PP21331")) return 50;
if(p.equals("PP21336"))
 if(w <= 491.17)
  if(t <= 3.2) return 150;
  if(t > 3.2) return 110;
 if(w > 491.17) return 50;
if(p.equals("PP21345")) return 40;
if(p.equals("PP21346")) return 50;
if(p.equals("PP21364")) return 45;
if(p.equals("PP21391"))
 if(w <= 325)
  if(w <= 282)
   if(w <= 259)
    if(we <= 1624) return 100;
    if(we > 1624)
     if(we <= 2292)
      if(t <= 1) return 120;
      if(t > 1) return 80;
     if(we > 2292)
      if(we <= 2433) return 80;
      if(we > 2433)
       if(t <= 3.35) return 95;
       if(t > 3.35) return 60;
   if(w > 259)
    if(we <= 1795)
     if(we <= 1624) return 110;
     if(we > 1624) return 70;
    if(we > 1795) return 110;
  if(w > 282)
   if(t <= 0.9)
    if(we <= 1702)
     if(we <= 1643) return 82.85;
     if(we > 1643) return 75;
    if(we > 1702)
     if(w <= 287.3)
      if(we <= 2169) return 92;
      if(we > 2169) return 50;
     if(w > 287.3) return 100;
   if(t > 0.9)
    if(we <= 2739)
     if(t <= 1.05)
      if(we <= 2009) return 80;
      if(we > 2009) return 60;
     if(t > 1.05)
      if(t <= 3.9) return 90;
      if(t > 3.9) return 60;
    if(we > 2739)
     if(we <= 2839)
      if(we <= 2793) return 74.1166666666667;
      if(we > 2793) return 61.8166666666667;
     if(we > 2839)
      if(t <= 1.175) return 70;
      if(t > 1.175) return 50;
 if(w > 325)
  if(w <= 358)
   if(w <= 338)
    if(t <= 3.55)
     if(w <= 330.03) return 50;
     if(w > 330.03) return 140;
    if(t > 3.55) return 60;
   if(w > 338)
    if(we <= 2148) return 60;
    if(we > 2148)
     if(we <= 2208) return 120;
     if(we > 2208)
      if(t <= 3.45) return 110;
      if(t > 3.45) return 60;
  if(w > 358)
   if(w <= 375.21)
    if(we <= 2297)
     if(we <= 2170)
      if(we <= 2094) return 108.75;
      if(we > 2094) return 100;
     if(we > 2170) return 90;
    if(we > 2297)
     if(we <= 3208) return 75;
     if(we > 3208) return 100;
   if(w > 375.21)
    if(we <= 24965)
     if(t <= 3.85)
      if(w <= 376.06) return 50;
      if(w > 376.06)
       if(w <= 515.5) return 100;
       if(w > 515.5) return 50;
     if(t > 3.85) return 60;
    if(we > 24965) return 40;
if(p.equals("PP21394"))
 if(we <= 12063) return 50;
 if(we > 12063) return 60;
if(p.equals("PP21398"))
 if(t <= 2.21) return 50;
 if(t > 2.21)
  if(w <= 590) return 60;
  if(w > 590) return 50;
if(p.equals("PP21423"))
 if(t <= 1.45)
  if(t <= 0.71) return 70;
  if(t > 0.71)
   if(we <= 1795) return 70;
   if(we > 1795) return 60;
 if(t > 1.45)
  if(w <= 513)
   if(we <= 5913) return 30;
   if(we > 5913) return 40;
  if(w > 513) return 30;
if(p.equals("PP21432")) return 50;
if(p.equals("PP21456")) return 50;
if(p.equals("PP21473")) return 50;
if(p.equals("PP21476")) return 130;
if(p.equals("PP21493")) return 50;
if(p.equals("PP21526")) return 50;
if(p.equals("PP21529")) return 40;
if(p.equals("PP21577")) return 50;
if(p.equals("PP21597")) return 130;
if(p.equals("PP21620"))
 if(t <= 1.25)
  if(we <= 3735) return 50;
  if(we > 3735) return 80;
 if(t > 1.25)
  if(t <= 1.9)
   if(we <= 1420) return 90;
   if(we > 1420) return 100;
  if(t > 1.9) return 150;
if(p.equals("PP21684")) return 50;
if(p.equals("PP21702"))
 if(we <= 1728)
  if(w <= 189)
   if(w <= 179)
    if(w <= 170.5) return 90;
    if(w > 170.5) return 160;
   if(w > 179) return 120;
  if(w > 189)
   if(w <= 200)
    if(we <= 1590) return 200;
    if(we > 1590) return 120;
   if(w > 200)
    if(we <= 1258) return 160;
    if(we > 1258)
     if(w <= 235)
      if(we <= 1294) return 120;
      if(we > 1294) return 110;
     if(w > 235)
      if(we <= 1624)
       if(we <= 1523) return 160;
       if(we > 1523) return 100;
      if(we > 1624)
       if(we <= 1670) return 120;
       if(we > 1670) return 110;
 if(we > 1728)
  if(t <= 0.9)
   if(w <= 270)
    if(w <= 222) return 100;
    if(w > 222) return 70;
   if(w > 270)
    if(t <= 0.79) return 130;
    if(t > 0.79)
     if(we <= 1817) return 75;
     if(we > 1817) return 100;
  if(t > 0.9)
   if(we <= 2423) return 60;
   if(we > 2423)
    if(we <= 2478) return 71.3333333333333;
    if(we > 2478)
     if(t <= 1.5245) return 100;
     if(t > 1.5245) return 50;
if(p.equals("PP21730"))
 if(w <= 690) return 50;
 if(w > 690) return 60;
if(p.equals("PP21754"))
 if(w <= 603)
  if(we <= 12126) return 60;
  if(we > 12126) return 50;
 if(w > 603)
  if(we <= 11840) return 50;
  if(we > 11840) return 40;
if(p.equals("PP21762")) return 50;
if(p.equals("PP21802")) return 50;
if(p.equals("PP21805")) return 50;
if(p.equals("PP21833")) return 40;
if(p.equals("PP21880")) return 40;
if(p.equals("PP21884")) return 60;
if(p.equals("PP21890")) return 50;
if(p.equals("PP22085")) return 40;
if(p.equals("PP22171")) return 50;
if(p.equals("PP22178")) return 50;
if(p.equals("PP22259")) return 50;
if(p.equals("PP22271"))
 if(t <= 1.2)
  if(we <= 3798) return 50;
  if(we > 3798) return 70;
 if(t > 1.2)
  if(we <= 15130)
   if(we <= 14214) return 40;
   if(we > 14214) return 30;
  if(we > 15130) return 50;
if(p.equals("PP22323")) return 40;
if(p.equals("PP22325")) return 50;
if(p.equals("PP22326")) return 50;
if(p.equals("PP22367")) return 50;
if(p.equals("PP22369")) return 50;
if(p.equals("PP22390")) return 60;
if(p.equals("PP22416")) return 40;
if(p.equals("PP22417")) return 40;
if(p.equals("PP22423")) return 60;
if(p.equals("PP22425")) return 40;
if(p.equals("PP22426"))
 if(we <= 2910) return 40;
 if(we > 2910) return 50;
if(p.equals("PP22427"))
 if(we <= 4746) return 40;
 if(we > 4746) return 50;
if(p.equals("PP22459")) return 30;
if(p.equals("PP22478")) return 60;
if(p.equals("PP22482")) return 40;
if(p.equals("PP22483")) return 30;
if(p.equals("PP22484")) return 50;
if(p.equals("PP22485")) return 50;
if(p.equals("PP22491")) return 50;
if(p.equals("PP22492")) return 50;
if(p.equals("PP22493"))
 if(we <= 2411) return 80;
 if(we > 2411) return 120;
if(p.equals("PP22494")) return 130;
if(p.equals("PP22498")) return 50;
if(p.equals("PP22499")) return 40;
if(p.equals("PP22500")) return 40;
if(p.equals("PP22502")) return 40;
if(p.equals("PP22505"))
 if(t <= 2.9) return 40;
 if(t > 2.9) return 50;
if(p.equals("PP22509")) return 50;
if(p.equals("PP22512")) return 50;
if(p.equals("PP22514"))
 if(t <= 3.2) return 50;
 if(t > 3.2) return 40;
if(p.equals("PP22515")) return 50;
if(p.equals("PP22531")) return 40;
if(p.equals("PP22535")) return 50;
if(p.equals("PP22537")) return 100;
if(p.equals("PP22563")) return 50;
if(p.equals("PP22568")) return 60;
if(p.equals("PP22571")) return 45;
if(p.equals("PP22581")) return 60;
if(p.equals("PP22590")) return 40;
if(p.equals("PP22600")) return 60;
if(p.equals("PP22601")) return 60;
if(p.equals("PP22604")) return 60;
if(p.equals("WS21007"))
 if(w <= 276)
  if(t <= 3.35) return 100;
  if(t > 3.35) return 60;
 if(w > 276) return 60;
if(p.equals("WS21010"))
 if(t <= 2) return 60;
 if(t > 2) return 90;
if(p.equals("WS21012"))
 if(we <= 6172) return 40;
 if(we > 6172)
  if(we <= 9116) return 110;
  if(we > 9116) return 80;
if(p.equals("WS21130")) return 60;
if(p.equals("WS21143")) return 90;
if(p.equals("WS21238")) return 150;
if(p.equals("WS21265")) return 120;
if(p.equals("WS21267")) return 150;
if(p.equals("WS21271")) return 30;
if(p.equals("WS31014")) return 150;
return 150.0;
}
}
