package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SSD implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-9.34820592e-01, 3.95924151e-01, 7.18648970e-01, -3.74493051e+00
            				, 6.51041317e+00, 7.75143385e+00, 2.50504565e+00, -1.27169025e+00
            				, -1.03605759e+00, -1.87861168e+00, -5.68970025e-01, -1.30510700e+00
            				, 6.66319549e-01, 3.83094621e+00, -5.00446439e-01, -1.26943156e-01
            				, -1.51561108e+01, -1.47983682e+00, 3.35928559e+00, 3.51072240e+00}, 
            			{-2.87301493e+00, 2.97213107e-01, -1.85030535e-01, -1.88682318e+00
        					, 6.32920325e-01, 7.75862098e-01, 1.48115516e+00, 2.11933609e-02
        					, -8.38736832e-01, -3.84221017e-01, -2.71276450e+00, -7.96504855e-01
        					, 3.87641877e-01, -3.02151758e-02, -1.62787825e-01, -7.24046886e-01
        					, -1.05959356e+00, 1.31692600e+00, 1.07306004e+00, 9.87427771e-01},
            			{-1.70247912e+00, -1.28354609e-01, 5.64475954e-01, 7.58067787e-01
    						, 6.99083984e-01, 6.36623561e-01, 7.11823851e-02, 1.11058402e+00
    						, -1.05741033e-02, -8.57353151e-01, -4.64096844e-01, -9.36142921e-01
    						, 9.00128603e-01, -9.52884495e-01, 9.66191292e-01, -2.19322515e+00
    						, 2.32181907e+00, 1.16327834e+00, -7.34817326e-01, 1.42648566e+00}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.06009039, -0.17863937, -2.07512951, -0.24306367, 0.97703975, 0.87538558
            			, -0.46446589, -1.93093026, -0.03647349, -0.41533315, -0.01911471, -0.44719896
            			, -1.47029662, 1.54014671, -1.31046021, -1.97951758, 0.27596864, -0.19274561
            			, 1.51170278, 0.24812016};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.0470226 ,
            			1.55381644,
            			-1.68612099,
            			0.93764019,
            			0.91022521,
            			2.07316208,
            			0.72837853,
            			1.20782447,
            			-0.02860674,
            			-0.84757501,
            			-0.64067763,
            			0.49060169,
            			0.26513675,
            			0.63908482,
            			0.14714257,
            			2.34442711,
            			-1.77531147,
            			2.17060351,
            			0.2606774 ,
            			0.32198015};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.14105333};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 3.27;
	}

	@Override
	public double getMaxWidth() {
		
		return 560.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 7675.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.23;
	}

	@Override
	public double getMinWidth() {
		
		return 64.0;
	}

	@Override
	public double getMinWeight() {
		
		return 106.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
