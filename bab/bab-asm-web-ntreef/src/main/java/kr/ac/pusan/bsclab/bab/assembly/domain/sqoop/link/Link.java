package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;


@JsonRootName("link")
public class Link {
	
	@JsonProperty("link-config-values")
	LinkConfigValue[] linkConfigValues;
	
	@JsonProperty("creation-user")
	String creationUser;
	
	String name;
	
	@JsonProperty("creation-date")
	Date creationDate;
	
	int id;
	
	@JsonProperty("update-date")
	Date updateDate;
	
	boolean enabled;
	
	@JsonProperty("update-user")
	String updateUser;
	
	
	/**
	 * 1 : generic-jdbc-connector
	 * 2 : kite-connector
	 * 3 : hdfs-connector
	 * 4 : kafka-connector
	 */
	@JsonProperty("connector-id")
	int connectorId;

	

	/**
	 * @param linkConfigValues
	 * @param creationUser
	 * @param name
	 * @param creationDate
	 * @param id
	 * @param updateDate
	 * @param enabled
	 * @param updateUser
	 * @param connectorId
	 */
	public Link(LinkConfigValue[] linkConfigValues, String creationUser, String name, Date creationDate, int id,
			Date updateDate, boolean enabled, String updateUser, int connectorId) {
		this.linkConfigValues = linkConfigValues;
		this.creationUser = creationUser;
		this.name = name;
		this.creationDate = creationDate;
		this.id = id;
		this.updateDate = updateDate;
		this.enabled = enabled;
		this.updateUser = updateUser;
		this.connectorId = connectorId;
	}


	public LinkConfigValue[] getLinkConfigValues() {
		return linkConfigValues;
	}


	public void setLinkConfigValues(LinkConfigValue[] linkConfigValues) {
		this.linkConfigValues = linkConfigValues;
	}


	public String getCreationUser() {
		return creationUser;
	}


	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public boolean isEnabled() {
		return enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public int getConnectorId() {
		return connectorId;
	}


	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	
	
}