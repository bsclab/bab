package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class DashboardController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/dashboard/{workspaceId}/{datasetId}/{sdt}/{edt}";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("dashboard/dashboard");

		String message = this.getClass().getName();

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);

		view.addObject("jsonData", jsonData);
		// view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		view.addObject("message", message);

		return view;
	}
}

class StatisticData {
	public String label;
	public int value;
	public String format;

	public StatisticData(String label, String format) {
		this.label = label;
		this.value = 0;
		this.format = format;
	}
}

class SubTabData {
	public String label;
	public boolean isProcessing;
	public boolean isRendered;
	public String data;
	public Map<String, StatisticData> statistics = new HashMap<String, StatisticData>();

	public SubTabData(String label, String format) {
		this.label = label;
		this.isProcessing = false;
		this.isRendered = false;
		this.data = null;
		this.statistics.put("one", new StatisticData("Min", format));
		this.statistics.put("two", new StatisticData("Min", format));
		this.statistics.put("three", new StatisticData("Min", format));
		this.statistics.put("four", new StatisticData("Min", format));
	}
}

class TabData {
	public String label;
	public String id;
	public boolean isRendered;
	public boolean isProcessing;

	public Map<String, StatisticData> statistics;
	public Map<String, SubTabData> tabs;

	public TabData(String id, String label) {
		this.id = id;
		this.label = label;
		this.statistics = new HashMap<String, StatisticData>();
		this.tabs = new HashMap<String, SubTabData>();
	}

	public void addStatistic(String key, String label, String format) {
		this.statistics.put(key, new StatisticData(label, format));
	}

	public void addSubTab(String key, String label, String format) {
		this.tabs.put(key, new SubTabData(label, format));
	}
}
