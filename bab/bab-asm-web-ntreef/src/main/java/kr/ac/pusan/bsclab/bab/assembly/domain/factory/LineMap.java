package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import javax.persistence.Table;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

@Table(name="sf_line_map")
public class LineMap {
	
	@Id
	@Column(name="PLNPRC_ID")
	String plnprcId;
	
	@Column(name="PLNPRC_MAP")
	String plnprcMap;
	
	@Column(name="CRM_ODN")
	String crmOdn;

	@Column(name="ANN_ODN")
	String annOdn;

	@Column(name="TOPANN_CLS")
	String topannCls;

	@Column(name="PLNPRC_ODN")
	String plnprc_odn;

	@Column(name="MFGSTD_DUR")
	String mfgstdDur;

	@Column(name="USE_YN")
	String useYn;

	@Column(name="START_DT")
	String startDt;

	@Column(name="END_DT")
	String endDt;

	@Column(name="CRT_DT")
	Date crtDt;

	@Column(name="CRTCHR_NO")
	String crtchrNo;

	@Column(name="UPD_DT")
	Date updDt;

	@Column(name="UPDCHR_NO")
	String updchrNo;

	@Column(name="RMT_CD")
	String rmtCd;

	@Column(name="ZON_CD")
	String zonCd;

	@Column(name="AFT_CRM_ODN")
	String aftCrmOdn;

	@Column(name="AFT_ANN_ODN")
	String aftAnnOdn;
}
