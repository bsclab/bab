package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link;

import java.util.ArrayList;
import java.util.List;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

public class HdfsLinkFactory extends LinkFactory implements InputsFactory {

	
	/**
	 * args[0] : uri
	 */
	@Override
	public Input[] createInputs(Object... args) {
		List<Input> inputs = new ArrayList<Input>();
		inputs.add(new Input(255, "ANY", "linkConfig.uri", 23, false, "", "STRING", args[0], null));
		
		return inputs.toArray(new Input[0]);
	}

}
