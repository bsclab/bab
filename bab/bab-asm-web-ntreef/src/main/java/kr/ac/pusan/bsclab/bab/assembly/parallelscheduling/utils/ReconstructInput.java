package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling.utils;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReconstructInput {
	public static JSONArray constructToJSON(Iterator<String> iterator) throws JSONException {
		JSONObject parent_jsonob = null;
		JSONArray jsonar_graph_root = new JSONArray();
		
		HashMap<String, JSONArray> hashmap = new HashMap<String, JSONArray>();
		
		String prev_jobse = null;
		
		while(iterator.hasNext()) {
			String array_data = iterator.next();
			
			String[] split = array_data.split("_");
			String jobseq = split[0];
			String[] split_jobseq = jobseq.split("-");
			String jobse = split_jobseq[0]; 
			String machine = split[1];
			String parent = split[2];
			
			JSONObject jsonob = new JSONObject();
			jsonob.put("name", jobse+ " - "+machine);
			jsonob.put("jobseq", jobseq);
			
			if(prev_jobse == null || jobse.equals(prev_jobse)) {
				//System.out.println("Atas "+jobseq);
				if(prev_jobse == null) {
					jsonar_graph_root.put(jsonob);	
				}
				if(parent_jsonob != null) {
					JSONArray jsonar = new JSONArray();
					jsonar.put(jsonob);
					jsonob.put("parent", parent_jsonob.get("jobseq"));
					parent_jsonob.put("children", jsonar);
				}
				parent_jsonob = jsonob;
			}
			else {
				//System.out.println("Bawah "+jobseq);
				JSONArray jsonar = hashmap.get(parent);
				if(jsonar == null) {
					jsonar = new JSONArray();
					hashmap.put(parent, jsonar);
					jsonob.put("parent", parent_jsonob.get("jobseq"));
					parent_jsonob.put("children", jsonar);
					parent_jsonob = jsonob;
				}
				else
					parent_jsonob = jsonob;
				
				jsonar.put(jsonob);
			}
			
			prev_jobse = jobse;
		} 
		
		return jsonar_graph_root;
	}
}
