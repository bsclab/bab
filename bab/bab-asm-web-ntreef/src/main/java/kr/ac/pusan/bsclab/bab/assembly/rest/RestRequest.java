package kr.ac.pusan.bsclab.bab.assembly.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Component
public class RestRequest {
	
	private static final Logger logger
		= LoggerFactory.getLogger(RestRequest.class);
	
	@Transactional
	public ResponseEntity<String> exchange(String uri
										, HttpMethod httpMethod
										, HttpEntity<?> httpEntity
										, Class<String> responseType) {
											
		logger.debug(httpMethod.name() + " request to={}", uri);
		
		return new RestTemplate().exchange(uri
												, httpMethod
												, httpEntity
												, responseType);
	}

}
