package kr.ac.pusan.bsclab.bab.assembly.job;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.factory.dao.LogDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.JobStatus;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.Report;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.ReportType;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.dao.WarningDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueue;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueueStatus;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.JobType;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.DBJobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.SparkJobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.SqoopJobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkDriverStatus;
import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkSubmissionStatus;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopConnector;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.Link;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission.Submission;
import kr.ac.pusan.bsclab.bab.assembly.domain.yarn.App;
import kr.ac.pusan.bsclab.bab.assembly.domain.yarn.FinalStatus;
import kr.ac.pusan.bsclab.bab.assembly.utils.DateConvertor;
import kr.ac.pusan.bsclab.bab.assembly.utils.hdfs.WebHdfs;
import kr.ac.pusan.bsclab.bab.assembly.utils.spark.SparkSubmission;
import kr.ac.pusan.bsclab.bab.assembly.utils.spark.SparkSubmissionFactory;
import kr.ac.pusan.bsclab.bab.assembly.utils.spark.SparkWeb;
import kr.ac.pusan.bsclab.bab.assembly.utils.sqoop.SqoopCmdMaker;
import kr.ac.pusan.bsclab.bab.assembly.utils.sqoop.SqoopElementFactory;
import kr.ac.pusan.bsclab.bab.assembly.utils.sqoop.SqoopWeb;
import kr.ac.pusan.bsclab.bab.assembly.utils.yarn.YarnWeb;


@Component
public class JobRunner {
	
	@Autowired
	JobListener jobListener;
	
	@Autowired
	JobManager jobManager;
	
	@Autowired
	LogDao logDao;
	
	@Autowired
	WarningDao warningDao;
	
	@Autowired
	AppProperties ap;
	
	@Autowired
	HdfsProperties hp;
	
	@Autowired
	SqoopCmdMaker scm;
	
	@Autowired
	SqoopElementFactory sef;
	
	@Autowired
	SparkSubmissionFactory ssf;
	
	@Autowired
	SqoopWeb sqw;
	
	@Autowired
	YarnWeb yw;
	
	@Autowired
	SparkWeb spw;
	
	@Autowired
	WebHdfs hdfs;
	
	@Autowired
	DateConvertor dc;

	
	@Async
	public void run(JobQueue jobQueue) throws JsonParseException, JsonMappingException, IOException, InterruptedException {
		int reportNo = 0;
		String jobHashCode = "i" + Math.abs(jobQueue.hashCode());
//		String error=null;
//		ObjectMapper om = new ObjectMapper();
		int jobNo = jobListener.save(new kr.ac.pusan.bsclab.bab.assembly.domain.info.Job(
				0
				, jobQueue.getJobQueueId()
				, JobStatus.RUNNING
				, new ObjectMapper().writeValueAsString(jobQueue)
				, dc.getNow(), null, 0, 0));
		
		try {
			
		
			while (!jobQueue.isEmpty()) {
				
				
				Job<? extends JobConfiguration> job = jobQueue.poll();
				JobType jt = job.getJobType();
				if (jt.equals(JobType.DATABASE)) {
					DBJobConfiguration conf = (DBJobConfiguration) job.getJobConfiguration();
					
					List<Map<String, Object>> warningDataList
						= new ArrayList<Map<String, Object>>();
					logDao.createTemporaryTable(ap.getLogTableName(), conf.getTmpTableName());
					logDao.uploadCsvToTable(conf.getTmpTableName()
							, conf.getCsvFilePath());
					warningDataList.addAll(warningDao.showWarning());
					warningDataList.addAll(logDao.getOverlaps(ap.getLogTableName()
							, conf.getTmpTableName()));
					
					if (warningDataList.size() == 0)
						logDao.save(ap.getLogTableName(), conf.getTmpTableName());
					else {
						jobQueue.setJobQueueStatus(JobQueueStatus.FAILED);
						reportNo = jobListener.save(new Report(0, jobNo
								, ReportType.OVERLAP_DATA_OR_NULL_VALUE
								, new ObjectMapper().writeValueAsString(warningDataList)));
						jobListener.updateByJobId(new kr.ac.pusan.bsclab.bab.assembly.domain.info.Job(
								0
								, jobQueue.getJobQueueId()
								, JobStatus.FAILED
								, new ObjectMapper().writeValueAsString(jobQueue)
								, null, dc.getNow(), 100, reportNo));
					}
					
					logDao.dropTable(conf.getTmpTableName());
					
					
					
					continue;
				} else if (jt.equals(JobType.SQOOP)) {
					
					SqoopJobConfiguration conf = (SqoopJobConfiguration) job.getJobConfiguration();
					
					Link mLink = sef.createLink(SqoopConnector.GENERIC_JDBC_CONNECTOR
							, "mysql_" + jobHashCode);
					Link hLink = sef.createLink(SqoopConnector.HDFS_CONNECTOR
							, "hdfs_" + jobHashCode);
					mLink.setId(sqw.reqCreateLink(mLink));
					hLink.setId(sqw.reqCreateLink(hLink));
					kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.Job mToH
					= sef.createJob("job_" + jobHashCode
							, conf.getOutputDir()
							, mLink, hLink
							, scm.dbToHdfsQueryBuilder(
									conf.getResourceTable(), conf.getSdt(), conf.getEdt()));
					
					mToH.setId(sqw.reqCreateJob(mToH));
					
					Submission se = sqw.reqStartJob(mToH);
					
					while (true) {
						
						App app = yw.getApp(se.getExternalId());
						FinalStatus fs = app.getFinalStatus();
						
						
						if (fs.equals(FinalStatus.UNDEFINED))
							Thread.sleep(ap.getSqoopJobCheckerTime());
						else if (fs.equals(FinalStatus.FAILED)
								|| fs.equals(FinalStatus.KILLED)) {
							jobQueue.setJobQueueStatus(JobQueueStatus.FAILED);
						} else if (fs.equals(FinalStatus.SUCCEEDED)) {
							sqw.stopAndDeleteSqoopFlow(mToH);
							break;
						}
					}
					continue;
				} else if (jt.equals(JobType.SPARK)) {

					if (ap.getTestMode() == 1) {
						File f = new File(ap.getTestJarDir() + ap.getJar());
						if (f.exists()) {
							String jarPath = hp.getHome() + "/lib/" + ap.getJar();
							if (hdfs.isExists(jarPath)) {
								hdfs.delete(jarPath);
							}
							hdfs.copyFromLocal(f.getAbsolutePath(), jarPath);
							System.out.println("TESTMODE: REUPLOAD JAR " + jarPath + " FROM " + f.getAbsolutePath());
						}
					}
					
					SparkJobConfiguration conf =
							(SparkJobConfiguration) job.getJobConfiguration();
					
					SparkSubmission ss = ssf.createSparkSubmission(conf.getJobId()
							, conf.getJobClass()
							, conf.getJobPath()
							, conf.getUserId());
					
					SparkSubmissionStatus sss = spw.submit(ss);
					
					
					
					while (true) {
						
						SparkDriverStatus sds = spw.status(sss.getSubmissionId());
						
						String driverState = sds.driverState;
						
						if (driverState.equalsIgnoreCase("FINISHED")) {
							break;
						} else if (driverState.equalsIgnoreCase("FAILED")){
							jobQueue.setJobQueueStatus(JobQueueStatus.FAILED);
							break;
						} else {
							Thread.sleep(ap.getSqoopJobCheckerTime());
						}
					}
					continue;
				}
			}
		} catch (Exception e) {
			
			reportNo = jobListener.save(new Report(0, jobNo
					, ReportType.UNEXPECTED_EXCEPTION
					, e.getMessage()));
			jobListener.updateByJobId(new kr.ac.pusan.bsclab.bab.assembly.domain.info.Job(
					0
					, jobQueue.getJobQueueId()
					, JobStatus.FAILED
					, new ObjectMapper().writeValueAsString(jobQueue)
					, null, dc.getNow(), 100, reportNo));
			
			jobQueue.setJobQueueStatus(JobQueueStatus.FAILED);
		}
		
		if (jobQueue.getJobQueueStatus().equals(JobQueueStatus.RUNNING)) {
			jobQueue.setJobQueueStatus(JobQueueStatus.FINISHED);
			jobListener.updateByJobId(new kr.ac.pusan.bsclab.bab.assembly.domain.info.Job(
					0
					, jobQueue.getJobQueueId()
					, JobStatus.SUCCEEDED
					, new ObjectMapper().writeValueAsString(jobQueue)
					, null, dc.getNow(), 100, 0));
		} else {
			jobListener.updateByJobId(new kr.ac.pusan.bsclab.bab.assembly.domain.info.Job(
					0
					, jobQueue.getJobQueueId()
					, JobStatus.FAILED
					, new ObjectMapper().writeValueAsString(jobQueue)
					, null, dc.getNow(), 100, reportNo));
		}
			
		
		
	}
}
