package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_WSA implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP12410"))
 if(we <= 3314) return 20;
 if(we > 3314) return 40;
if(p.equals("PP21894"))
 if(w <= 234)
  if(w <= 215)
   if(we <= 2021)
    if(t <= 2.8) return 30;
    if(t > 2.8) return 40;
   if(we > 2021)
    if(w <= 209)
     if(we <= 2058) return 40;
     if(we > 2058)
      if(we <= 2348) return 30;
      if(we > 2348) return 20;
    if(w > 209)
     if(we <= 2152) return 20;
     if(we > 2152) return 30;
  if(w > 215)
   if(w <= 218)
    if(we <= 2194) return 30;
    if(we > 2194)
     if(we <= 2420) return 120;
     if(we > 2420) return 55;
   if(w > 218)
    if(we <= 2066) return 20;
    if(we > 2066)
     if(t <= 2.8) return 30;
     if(t > 2.8)
      if(w <= 232) return 20;
      if(w > 232) return 40;
 if(w > 234)
  if(t <= 2.3)
   if(t <= 2) return 50;
   if(t > 2) return 80;
  if(t > 2.3)
   if(w <= 235)
    if(we <= 2346) return 20;
    if(we > 2346) return 30;
   if(w > 235)
    if(w <= 241)
     if(we <= 3230) return 30;
     if(we > 3230) return 60;
    if(w > 241) return 20;
if(p.equals("PP21895"))
 if(w <= 313)
  if(w <= 312) return 20;
  if(w > 312) return 30;
 if(w > 313) return 20;
if(p.equals("PP21896"))
 if(t <= 4) return 60;
 if(t > 4)
  if(we <= 4705)
   if(we <= 4443) return 60;
   if(we > 4443) return 20;
  if(we > 4705) return 30;
if(p.equals("PP21901")) return 120;
if(p.equals("PP21904"))
 if(t <= 4.7)
  if(w <= 243)
   if(w <= 220)
    if(t <= 3.5) return 40;
    if(t > 3.5) return 30;
   if(w > 220)
    if(w <= 229) return 60;
    if(w > 229)
     if(w <= 237) return 20;
     if(w > 237) return 60;
  if(w > 243)
   if(t <= 4)
    if(w <= 251) return 60;
    if(w > 251) return 30;
   if(t > 4)
    if(w <= 290)
     if(w <= 265) return 30;
     if(w > 265)
      if(w <= 278) return 40;
      if(w > 278)
       if(w <= 288) return 30;
       if(w > 288)
        if(we <= 3005) return 30;
        if(we > 3005) return 60;
    if(w > 290) return 30;
 if(t > 4.7)
  if(w <= 237)
   if(w <= 232)
    if(we <= 2506)
     if(we <= 2396) return 50;
     if(we > 2396) return 60;
    if(we > 2506) return 30;
   if(w > 232) return 20;
  if(w > 237)
   if(w <= 245)
    if(we <= 2936) return 70;
    if(we > 2936) return 40;
   if(w > 245)
    if(w <= 288)
     if(w <= 268)
      if(w <= 263)
       if(w <= 256) return 30;
       if(w > 256) return 40;
      if(w > 263)
       if(we <= 3043) return 20;
       if(we > 3043) return 40;
     if(w > 268)
      if(we <= 2932)
       if(w <= 278.5) return 50;
       if(w > 278.5) return 30;
      if(we > 2932) return 30;
    if(w > 288)
     if(w <= 318)
      if(w <= 301)
       if(w <= 294) return 60;
       if(w > 294)
        if(we <= 3236) return 50;
        if(we > 3236)
         if(we <= 3700) return 30;
         if(we > 3700) return 40;
      if(w > 301)
       if(we <= 3604)
        if(we <= 3422)
         if(we <= 3299) return 60;
         if(we > 3299) return 40;
        if(we > 3422) return 60;
       if(we > 3604) return 70;
     if(w > 318)
      if(we <= 3893)
       if(we <= 3764)
        if(w <= 332) return 50;
        if(w > 332) return 20;
       if(we > 3764) return 50;
      if(we > 3893)
       if(w <= 358)
        if(w <= 336)
         if(we <= 4044) return 30;
         if(we > 4044) return 40;
        if(w > 336)
         if(we <= 4009) return 60;
         if(we > 4009) return 90;
       if(w > 358)
        if(we <= 4592)
         if(we <= 4291) return 60;
         if(we > 4291) return 30;
        if(we > 4592) return 40;
if(p.equals("PP21906")) return 30;
if(p.equals("PP21909")) return 50;
if(p.equals("PP21911"))
 if(t <= 5)
  if(we <= 2776)
   if(w <= 238) return 30;
   if(w > 238)
    if(we <= 2427) return 50;
    if(we > 2427) return 20;
  if(we > 2776)
   if(w <= 329)
    if(t <= 4.7)
     if(t <= 3.5)
      if(t <= 2.8) return 60;
      if(t > 2.8) return 30;
     if(t > 3.5)
      if(t <= 4) return 50;
      if(t > 4) return 30;
    if(t > 4.7)
     if(w <= 251) return 90;
     if(w > 251) return 30;
   if(w > 329) return 60;
 if(t > 5)
  if(we <= 5155) return 20;
  if(we > 5155)
   if(we <= 5263) return 40;
   if(we > 5263) return 30;
if(p.equals("PP21912"))
 if(t <= 2.8)
  if(w <= 482) return 20;
  if(w > 482) return 30;
 if(t > 2.8)
  if(t <= 3.5)
   if(w <= 482)
    if(w <= 472) return 30;
    if(w > 472) return 40;
   if(w > 482)
    if(we <= 4875) return 30;
    if(we > 4875) return 20;
  if(t > 3.5) return 120;
if(p.equals("PP21913"))
 if(t <= 3)
  if(w <= 392)
   if(we <= 3365)
    if(t <= 2.8) return 30;
    if(t > 2.8) return 60;
   if(we > 3365)
    if(we <= 4027)
     if(we <= 3492)
      if(w <= 350)
       if(we <= 3476) return 20;
       if(we > 3476) return 120;
      if(w > 350)
       if(we <= 3472) return 30;
       if(we > 3472) return 90;
     if(we > 3492)
      if(w <= 379)
       if(we <= 3756)
        if(t <= 2.8)
         if(w <= 360)
          if(w <= 358)
           if(w <= 350)
            if(we <= 3515) return 20;
            if(we > 3515) return 30;
           if(w > 350) return 20;
          if(w > 358)
           if(we <= 3545) return 40;
           if(we > 3545) return 30;
         if(w > 360)
          if(w <= 373) return 20;
          if(w > 373)
           if(w <= 377) return 40;
           if(w > 377) return 20;
        if(t > 2.8)
         if(we <= 3500) return 30;
         if(we > 3500) return 20;
       if(we > 3756)
        if(w <= 373) return 50;
        if(w > 373) return 40;
      if(w > 379)
       if(w <= 380) return 20;
       if(w > 380)
        if(we <= 3672)
         if(we <= 3581) return 10;
         if(we > 3581) return 60;
        if(we > 3672) return 20;
    if(we > 4027)
     if(t <= 2.8) return 30;
     if(t > 2.8) return 10;
  if(w > 392)
   if(we <= 3766)
    if(w <= 398)
     if(we <= 3612)
      if(w <= 395.5) return 30;
      if(w > 395.5) return 80;
     if(we > 3612) return 30;
    if(w > 398)
     if(w <= 403) return 20;
     if(w > 403) return 30;
   if(we > 3766)
    if(we <= 4330)
     if(we <= 4179)
      if(w <= 413)
       if(w <= 406)
        if(t <= 2.8)
         if(w <= 393)
          if(we <= 3918) return 40;
          if(we > 3918)
           if(we <= 4017) return 20;
           if(we > 4017) return 40;
         if(w > 393)
          if(w <= 398)
           if(w <= 395) return 30;
           if(w > 395)
            if(we <= 3952) return 40;
            if(we > 3952) return 50;
          if(w > 398)
           if(w <= 404) return 30;
           if(w > 404) return 50;
        if(t > 2.8) return 20;
       if(w > 406)
        if(w <= 410) return 30;
        if(w > 410) return 20;
      if(w > 413)
       if(w <= 416) return 90;
       if(w > 416)
        if(w <= 417)
         if(we <= 4044)
          if(we <= 3941) return 30;
          if(we > 3941) return 80;
         if(we > 4044) return 20;
        if(w > 417) return 40;
     if(we > 4179) return 30;
    if(we > 4330)
     if(w <= 416) return 90;
     if(w > 416) return 60;
 if(t > 3)
  if(t <= 3.3)
   if(w <= 332) return 110;
   if(w > 332)
    if(w <= 357) return 90;
    if(w > 357) return 120;
  if(t > 3.3)
   if(t <= 3.5)
    if(we <= 4943)
     if(w <= 435)
      if(w <= 417)
       if(w <= 370)
        if(w <= 306)
         if(we <= 3392) return 30;
         if(we > 3392) return 20;
        if(w > 306) return 30;
       if(w > 370)
        if(w <= 410)
         if(we <= 4083) return 30;
         if(we > 4083) return 60;
        if(w > 410)
         if(we <= 4837) return 20;
         if(we > 4837) return 30;
      if(w > 417)
       if(w <= 432) return 60;
       if(w > 432) return 30;
     if(w > 435)
      if(we <= 4492) return 20;
      if(we > 4492)
       if(we <= 4734) return 70;
       if(we > 4734) return 30;
    if(we > 4943) return 20;
   if(t > 3.5)
    if(w <= 317) return 80;
    if(w > 317)
     if(we <= 4968) return 30;
     if(we > 4968) return 80;
if(p.equals("PP21915"))
 if(t <= 2.8) return 30;
 if(t > 2.8)
  if(we <= 2478)
   if(w <= 238) return 60;
   if(w > 238) return 20;
  if(we > 2478)
   if(w <= 264) return 30;
   if(w > 264)
    if(we <= 3008)
     if(we <= 2842) return 40;
     if(we > 2842) return 20;
    if(we > 3008) return 30;
if(p.equals("PP21917"))
 if(w <= 424)
  if(t <= 2.7) return 20;
  if(t > 2.7)
   if(w <= 399)
    if(w <= 385)
     if(w <= 299) return 20;
     if(w > 299)
      if(w <= 320)
       if(w <= 306)
        if(we <= 3452)
         if(we <= 3237) return 30;
         if(we > 3237) return 60;
        if(we > 3452) return 30;
       if(w > 306) return 60;
      if(w > 320)
       if(we <= 3904)
        if(t <= 4) return 30;
        if(t > 4)
         if(we <= 3619) return 20;
         if(we > 3619) return 30;
       if(we > 3904) return 60;
    if(w > 385) return 30;
   if(w > 399)
    if(t <= 4) return 50;
    if(t > 4) return 20;
 if(w > 424) return 30;
if(p.equals("PP21918")) return 90;
if(p.equals("PP21920"))
 if(w <= 216) return 50;
 if(w > 216)
  if(t <= 4.7)
   if(w <= 229)
    if(t <= 4) return 30;
    if(t > 4)
     if(w <= 224) return 60;
     if(w > 224) return 10;
   if(w > 229)
    if(we <= 2902)
     if(w <= 237) return 90;
     if(w > 237) return 20;
    if(we > 2902)
     if(t <= 2.8)
      if(w <= 261) return 40;
      if(w > 261)
       if(w <= 288) return 80;
       if(w > 288) return 60;
     if(t > 2.8)
      if(w <= 236) return 80;
      if(w > 236)
       if(w <= 306)
        if(w <= 292)
         if(t <= 4) return 30;
         if(t > 4)
          if(we <= 3229) return 30;
          if(we > 3229) return 60;
        if(w > 292)
         if(we <= 3734) return 90;
         if(we > 3734) return 60;
       if(w > 306) return 30;
  if(t > 4.7) return 60;
if(p.equals("PP21922"))
 if(t <= 5)
  if(w <= 259) return 50;
  if(w > 259)
   if(w <= 310)
    if(w <= 269)
     if(we <= 2935) return 60;
     if(we > 2935) return 30;
    if(w > 269) return 60;
   if(w > 310) return 30;
 if(t > 5)
  if(we <= 4697) return 80;
  if(we > 4697) return 60;
if(p.equals("PP21923"))
 if(t <= 3)
  if(t <= 2.3) return 30;
  if(t > 2.3) return 70;
 if(t > 3)
  if(w <= 337)
   if(t <= 4)
    if(we <= 3829) return 120;
    if(we > 3829) return 30;
   if(t > 4)
    if(we <= 4044) return 90;
    if(we > 4044)
     if(w <= 320) return 60;
     if(w > 320)
      if(w <= 334) return 10;
      if(w > 334)
       if(we <= 4631) return 60;
       if(we > 4631) return 10;
  if(w > 337)
   if(t <= 4)
    if(w <= 408)
     if(we <= 4753) return 30;
     if(we > 4753) return 60;
    if(w > 408)
     if(w <= 423) return 20;
     if(w > 423) return 30;
   if(t > 4)
    if(w <= 350)
     if(we <= 4035) return 90;
     if(we > 4035) return 30;
    if(w > 350)
     if(w <= 371)
      if(w <= 353) return 90;
      if(w > 353) return 60;
     if(w > 371)
      if(we <= 4497) return 20;
      if(we > 4497) return 30;
if(p.equals("PP21926")) return 40;
if(p.equals("PP21927")) return 30;
if(p.equals("PP21931"))
 if(we <= 5211) return 40;
 if(we > 5211) return 50;
if(p.equals("PP21932")) return 30;
if(p.equals("PP21933"))
 if(we <= 4864) return 40;
 if(we > 4864) return 60;
if(p.equals("PP21935")) return 50;
if(p.equals("PP21936"))
 if(t <= 3.5) return 50;
 if(t > 3.5) return 30;
if(p.equals("PP21937"))
 if(we <= 4310) return 120;
 if(we > 4310)
  if(we <= 4941) return 40;
  if(we > 4941) return 70;
if(p.equals("PP21938")) return 110;
if(p.equals("PP21939")) return 40;
if(p.equals("PP21940"))
 if(t <= 3)
  if(we <= 4876)
   if(t <= 2.3) return 30;
   if(t > 2.3)
    if(w <= 337) return 60;
    if(w > 337)
     if(we <= 4731) return 50;
     if(we > 4731) return 60;
  if(we > 4876) return 120;
 if(t > 3)
  if(t <= 3.3)
   if(we <= 4958)
    if(we <= 4050)
     if(we <= 3542)
      if(t <= 3.25) return 20;
      if(t > 3.25) return 50;
     if(we > 3542) return 80;
    if(we > 4050)
     if(we <= 4845)
      if(w <= 356) return 60;
      if(w > 356)
       if(we <= 4307)
        if(we <= 4257) return 30;
        if(we > 4257) return 40;
       if(we > 4307) return 30;
     if(we > 4845)
      if(we <= 4908)
       if(we <= 4864) return 40;
       if(we > 4864) return 20;
      if(we > 4908)
       if(we <= 4937) return 30;
       if(we > 4937) return 40;
   if(we > 4958)
    if(w <= 360) return 50;
    if(w > 360) return 30;
  if(t > 3.3)
   if(we <= 4594) return 60;
   if(we > 4594)
    if(t <= 3.5) return 30;
    if(t > 3.5)
     if(w <= 394) return 50;
     if(w > 394) return 30;
if(p.equals("PP21942")) return 60;
if(p.equals("PP21943")) return 40;
if(p.equals("PP21945")) return 60;
if(p.equals("PP21956")) return 60;
if(p.equals("PP21957")) return 70;
if(p.equals("PP21958")) return 40;
if(p.equals("PP21965")) return 30;
if(p.equals("PP21972")) return 70;
if(p.equals("PP21973")) return 90;
if(p.equals("PP21975"))
 if(t <= 3.2)
  if(we <= 4550) return 40;
  if(we > 4550) return 30;
 if(t > 3.2)
  if(w <= 457)
   if(w <= 456) return 10;
   if(w > 456) return 30;
  if(w > 457)
   if(w <= 459)
    if(we <= 5199)
     if(we <= 5100) return 60;
     if(we > 5100) return 40;
    if(we > 5199) return 20;
   if(w > 459)
    if(we <= 5239)
     if(we <= 4422) return 50;
     if(we > 4422) return 20;
    if(we > 5239)
     if(we <= 5362) return 10;
     if(we > 5362) return 60;
if(p.equals("PP21976")) return 60;
if(p.equals("PP21994"))
 if(t <= 3.5)
  if(we <= 4799) return 20;
  if(we > 4799)
   if(w <= 459) return 30;
   if(w > 459)
    if(we <= 5568) return 60;
    if(we > 5568)
     if(we <= 5760) return 30;
     if(we > 5760) return 50;
 if(t > 3.5)
  if(we <= 5431) return 30;
  if(we > 5431)
   if(t <= 4) return 60;
   if(t > 4)
    if(we <= 5494) return 20;
    if(we > 5494)
     if(we <= 5567) return 90;
     if(we > 5567) return 30;
if(p.equals("PP21996")) return 70;
if(p.equals("PP22001"))
 if(t <= 2) return 40;
 if(t > 2)
  if(we <= 3781)
   if(t <= 4.5)
    if(t <= 3.5)
     if(t <= 3) return 20;
     if(t > 3) return 60;
    if(t > 3.5)
     if(w <= 296) return 180;
     if(w > 296) return 20;
   if(t > 4.5) return 30;
  if(we > 3781)
   if(t <= 3.5)
    if(t <= 2.8)
     if(w <= 377) return 90;
     if(w > 377) return 60;
    if(t > 2.8)
     if(w <= 378) return 60;
     if(w > 378) return 90;
   if(t > 3.5)
    if(t <= 4)
     if(w <= 386)
      if(w <= 350) return 60;
      if(w > 350)
       if(we <= 4366) return 80;
       if(we > 4366) return 50;
     if(w > 386)
      if(w <= 404) return 20;
      if(w > 404) return 60;
    if(t > 4) return 80;
if(p.equals("PP22002"))
 if(t <= 3.5) return 60;
 if(t > 3.5)
  if(we <= 2699)
   if(t <= 4.5)
    if(w <= 231) return 30;
    if(w > 231)
     if(we <= 2358) return 90;
     if(we > 2358) return 30;
   if(t > 4.5)
    if(we <= 2571)
     if(we <= 2563)
      if(we <= 2525)
       if(w <= 229) return 60;
       if(w > 229) return 30;
      if(we > 2525) return 30;
     if(we > 2563) return 10;
    if(we > 2571)
     if(w <= 261)
      if(w <= 220) return 40;
      if(w > 220) return 60;
     if(w > 261) return 30;
  if(we > 2699)
   if(w <= 245)
    if(we <= 2914) return 20;
    if(we > 2914) return 90;
   if(w > 245)
    if(we <= 3262)
     if(we <= 2863) return 40;
     if(we > 2863)
      if(w <= 255)
       if(w <= 247) return 20;
       if(w > 247)
        if(w <= 250) return 60;
        if(w > 250)
         if(we <= 2963) return 20;
         if(we > 2963)
          if(we <= 3014) return 60;
          if(we > 3014) return 40;
      if(w > 255)
       if(w <= 283) return 20;
       if(w > 283) return 30;
    if(we > 3262)
     if(w <= 313)
      if(we <= 3307) return 20;
      if(we > 3307)
       if(w <= 310) return 40;
       if(w > 310) return 90;
     if(w > 313)
      if(we <= 3915)
       if(we <= 3812)
        if(we <= 3738) return 30;
        if(we > 3738) return 20;
       if(we > 3812) return 30;
      if(we > 3915)
       if(w <= 375)
        if(w <= 356) return 40;
        if(w > 356)
         if(w <= 361) return 20;
         if(w > 361) return 40;
       if(w > 375) return 30;
if(p.equals("PP22003"))
 if(t <= 3.5)
  if(we <= 7041)
   if(t <= 2.8) return 30;
   if(t > 2.8) return 60;
  if(we > 7041) return 80;
 if(t > 3.5)
  if(w <= 490) return 20;
  if(w > 490)
   if(w <= 519) return 30;
   if(w > 519) return 20;
if(p.equals("PP22006"))
 if(we <= 1193)
  if(we <= 1099)
   if(we <= 1058) return 30;
   if(we > 1058) return 90;
  if(we > 1099)
   if(we <= 1165)
    if(we <= 1117) return 20;
    if(we > 1117) return 30;
   if(we > 1165)
    if(we <= 1173)
     if(we <= 1170) return 20;
     if(we > 1170) return 25.3833333333333;
    if(we > 1173) return 30;
 if(we > 1193)
  if(w <= 696)
   if(w <= 695)
    if(we <= 8209)
     if(we <= 7446)
      if(w <= 598)
       if(w <= 102)
        if(we <= 1209) return 40;
        if(we > 1209)
         if(we <= 1225) return 30;
         if(we > 1225) return 40;
       if(w > 102)
        if(we <= 7083)
         if(we <= 6729) return 60;
         if(we > 6729)
          if(we <= 7009) return 20;
          if(we > 7009) return 30;
        if(we > 7083) return 40;
      if(w > 598)
       if(w <= 638)
        if(we <= 7335) return 20;
        if(we > 7335) return 30;
       if(w > 638) return 30;
     if(we > 7446)
      if(w <= 686) return 40;
      if(w > 686) return 20;
    if(we > 8209)
     if(we <= 8318)
      if(we <= 8300) return 90;
      if(we > 8300) return 30;
     if(we > 8318) return 60;
   if(w > 695)
    if(we <= 8209) return 40;
    if(we > 8209)
     if(we <= 8300) return 20;
     if(we > 8300) return 30;
  if(w > 696) return 60;
if(p.equals("PP22007"))
 if(we <= 3639) return 30;
 if(we > 3639) return 60;
if(p.equals("PP22009"))
 if(w <= 359) return 30;
 if(w > 359)
  if(we <= 3392) return 30;
  if(we > 3392)
   if(w <= 399)
    if(t <= 2.8)
     if(we <= 3814) return 30;
     if(we > 3814) return 40;
    if(t > 2.8) return 20;
   if(w > 399)
    if(w <= 410)
     if(we <= 3953) return 60;
     if(we > 3953) return 20;
    if(w > 410)
     if(w <= 417)
      if(we <= 4079) return 40;
      if(we > 4079) return 20;
     if(w > 417) return 20;
if(p.equals("PP22014")) return 110;
if(p.equals("PP22015")) return 30;
if(p.equals("PP22017")) return 30;
if(p.equals("PP22035"))
 if(t <= 2) return 60;
 if(t > 2) return 30;
if(p.equals("PP22036"))
 if(w <= 294) return 60;
 if(w > 294) return 30;
if(p.equals("PP22037")) return 30;
if(p.equals("PP22038")) return 30;
if(p.equals("PP22041")) return 30;
if(p.equals("PP22052"))
 if(w <= 391) return 30;
 if(w > 391) return 50;
if(p.equals("PP22054")) return 60;
if(p.equals("PP22056")) return 60;
if(p.equals("PP22070")) return 60;
if(p.equals("PP22071"))
 if(we <= 4291) return 90;
 if(we > 4291)
  if(w <= 390)
   if(we <= 4731) return 60;
   if(we > 4731) return 100;
  if(w > 390) return 30;
if(p.equals("PP22072")) return 90;
if(p.equals("PP22074"))
 if(we <= 4971) return 30;
 if(we > 4971) return 60;
if(p.equals("PP22075"))
 if(w <= 404)
  if(w <= 305) return 180;
  if(w > 305) return 20;
 if(w > 404) return 30;
if(p.equals("PP22080")) return 50;
if(p.equals("PP22083")) return 40;
if(p.equals("PP22086"))
 if(we <= 3536) return 100;
 if(we > 3536) return 60;
if(p.equals("PP22093"))
 if(t <= 2)
  if(we <= 4323) return 40;
  if(we > 4323) return 60;
 if(t > 2) return 30;
if(p.equals("PP22094")) return 70;
if(p.equals("PP22111")) return 60;
if(p.equals("PP22112"))
 if(t <= 1.035) return 90;
 if(t > 1.035) return 30;
if(p.equals("PP22115"))
 if(we <= 2906) return 60;
 if(we > 2906) return 30;
if(p.equals("PP22132")) return 30;
if(p.equals("PP22135")) return 40;
if(p.equals("PP22136")) return 70;
if(p.equals("PP22137")) return 30;
if(p.equals("PP22159"))
 if(we <= 2486) return 30;
 if(we > 2486) return 40;
if(p.equals("PP22174")) return 30;
if(p.equals("PP22175")) return 90;
if(p.equals("PP22183")) return 30;
if(p.equals("PP22186")) return 120;
if(p.equals("PP22195"))
 if(w <= 312) return 40;
 if(w > 312) return 80;
if(p.equals("PP22202"))
 if(we <= 4131) return 30;
 if(we > 4131) return 120;
if(p.equals("PP22209"))
 if(w <= 422) return 50;
 if(w > 422) return 30;
if(p.equals("PP22210")) return 90;
if(p.equals("PP22213"))
 if(w <= 509) return 70;
 if(w > 509) return 60;
if(p.equals("PP22215")) return 100;
if(p.equals("PP22225")) return 55;
if(p.equals("PP22229")) return 30;
if(p.equals("PP22230")) return 20;
if(p.equals("PP22231")) return 70;
if(p.equals("PP22236"))
 if(t <= 1.17) return 80;
 if(t > 1.17) return 60;
if(p.equals("PP22237")) return 30;
if(p.equals("PP22240")) return 40;
if(p.equals("PP22243")) return 30;
if(p.equals("PP22246"))
 if(t <= 4.7) return 30;
 if(t > 4.7) return 90;
if(p.equals("PP22250")) return 70;
if(p.equals("PP22256")) return 80;
if(p.equals("PP22262")) return 40;
if(p.equals("PP22263"))
 if(we <= 4793) return 30;
 if(we > 4793) return 40;
if(p.equals("PP22264")) return 40;
if(p.equals("PP22267")) return 90;
if(p.equals("PP22268")) return 40;
if(p.equals("PP22276"))
 if(we <= 4064) return 60;
 if(we > 4064) return 120;
if(p.equals("PP22277")) return 60;
if(p.equals("PP22278"))
 if(we <= 7083) return 20;
 if(we > 7083) return 30;
if(p.equals("PP22284")) return 60;
if(p.equals("PP22292"))
 if(we <= 5440) return 40;
 if(we > 5440) return 90;
if(p.equals("PP22293")) return 40;
if(p.equals("PP22295"))
 if(w <= 417) return 30;
 if(w > 417) return 20;
if(p.equals("PP22297"))
 if(we <= 3096) return 40;
 if(we > 3096)
  if(w <= 233) return 30;
  if(w > 233) return 90;
if(p.equals("PP22298")) return 70;
if(p.equals("PP22299")) return 100;
if(p.equals("PP22300")) return 100;
if(p.equals("PP22303")) return 20;
if(p.equals("PP22306")) return 70;
if(p.equals("PP22307")) return 120;
if(p.equals("PP22312")) return 40;
if(p.equals("PP22316")) return 100;
if(p.equals("PP22317")) return 100;
if(p.equals("PP22318")) return 40;
if(p.equals("PP22319")) return 50;
if(p.equals("PP22345")) return 30;
if(p.equals("PP22350")) return 60;
if(p.equals("PP22351")) return 60;
if(p.equals("PP22354")) return 40;
if(p.equals("PP22355"))
 if(w <= 315) return 110;
 if(w > 315)
  if(w <= 326) return 80;
  if(w > 326) return 90;
if(p.equals("PP22361")) return 40;
if(p.equals("PP22362")) return 60;
if(p.equals("PP22363")) return 60;
if(p.equals("PP22365")) return 30;
if(p.equals("PP22380")) return 90;
if(p.equals("PP22406")) return 90;
if(p.equals("PP22408")) return 90;
if(p.equals("PP22431")) return 150;
if(p.equals("PP22437")) return 60;
if(p.equals("PP22448"))
 if(t <= 3.2) return 30;
 if(t > 3.2)
  if(we <= 5487) return 60;
  if(we > 5487) return 30;
if(p.equals("WSA1253")) return 60;
return 60.0;
}
}
