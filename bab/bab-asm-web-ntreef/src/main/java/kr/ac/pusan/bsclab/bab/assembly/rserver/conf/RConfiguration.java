package kr.ac.pusan.bsclab.bab.assembly.rserver.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Class for defining environment configuration to use R-Server with distributed
 * computing. set spark home, hadoop directory, yarn directory, hdfs directory,
 * spark version, portRserve is port that use to communicate with RStudio
 * Server, IP address of RStudio Server, scriptPath is path script location of R
 * script that will be used for R-analyzing, outputPath is path location of
 * output. in this case use hdfs path.
 * 
 * All of of these fields value refers to application.properties.
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */
@Configuration
@ConfigurationProperties(prefix = "rserver")
public class RConfiguration {

	private String sparkHome;
	private String hadoopDir;
	private String yarnDir;
	private String hdfsUrl;
	private String sparkVersion;
	private String graphUrl;

	private int portRserve;
	private String ipRserver;

	private String scriptPath;
	private String outputPath;
	private String outputHdfsPath;

	/**
	 * @return spark home directory
	 */
	public String getSparkHome() {
		return sparkHome;
	}

	/**
	 * @param sparkHome for setting spark home directory
	 */
	public void setSparkHome(String sparkHome) {
		this.sparkHome = sparkHome;
	}

	/**
	 * @return hadoop home directory
	 */
	public String getHadoopDir() {
		return hadoopDir;
	}

	/**
	 * @param hadoop
	 *            directory
	 */
	public void setHadoopDir(String hadoopDir) {
		this.hadoopDir = hadoopDir;
	}

	/**
	 * @return yarn home directory
	 */
	public String getYarnDir() {
		return yarnDir;
	}

	/**
	 * @param yarn directory
	 */
	public void setYarnDir(String yarnDir) {
		this.yarnDir = yarnDir;
	}

	/**
	 * @return HDFS or Hadoop File System directory
	 */
	public String getHdfsUrl() {
		return hdfsUrl;
	}

	/**
	 * @param HDFS url
	 */
	public void setHdfsUrl(String hdfsUrl) {
		this.hdfsUrl = hdfsUrl;
	}

	/**
	 * @return spark version
	 */
	public String getSparkVersion() {
		return sparkVersion;
	}

	/**
	 * @param spark version
	 */
	public void setSparkVersion(String sparkVersion) {
		this.sparkVersion = sparkVersion;
	}

	/**
	 * @return R-Script location
	 */
	public String getScriptPath() {
		return scriptPath;
	}

	/**
	 * @param r-script path
	 */
	public void setScriptPath(String scriptPath) {
		this.scriptPath = scriptPath;
	}

	/**
	 * @return output of graph, in HDFS
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * @param output path
	 */
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	/**
	 * @return IP address of RStudio Server
	 */
	public String getIpRserver() {
		return ipRserver;
	}

	/**
	 * @param IP Address RStudio Server
	 */
	public void setIpRserver(String ipRserver) {
		this.ipRserver = ipRserver;
	}

	/**
	 * @return Port number of RServe
	 */
	public int getPortRserve() {
		return portRserve;
	}

	/**
	 * @param port Rserve
	 */
	public void setPortRserve(int portRserve) {
		this.portRserve = portRserve;
	}

	/**
	 * @return graph url in HDFS
	 */
	public String getGraphUrl() {
		return graphUrl;
	}

	/**
	 * @param graphURL in HDFS
	 */
	public void setGraphUrl(String graphURL) {
		this.graphUrl = graphURL;
	}

	/**
	 * @return outputHdfsPath
	 */
	public String getOutputHdfsPath() {
		return outputHdfsPath;
	}

	/**
	 * @param outputHdfsPath
	 */
	public void setOutputHdfsPath(String outputHdfsPath) {
		this.outputHdfsPath = outputHdfsPath;
	}
	
}
