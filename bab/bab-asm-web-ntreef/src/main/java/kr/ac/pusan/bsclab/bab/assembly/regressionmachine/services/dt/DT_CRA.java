package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_CRA implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CRA1332")) return 45;
if(p.equals("CRB0112")) return 55;
if(p.equals("PP12184"))
 if(t <= 0.82)
  if(t <= 0.53)
   if(we <= 4386) return 70;
   if(we > 4386) return 115;
  if(t > 0.53)
   if(t <= 0.635) return 45;
   if(t > 0.635) return 50;
 if(t > 0.82)
  if(t <= 0.825)
   if(we <= 4286)
    if(we <= 3983) return 35;
    if(we > 3983) return 37;
   if(we > 4286) return 40;
  if(t > 0.825)
   if(w <= 284) return 65;
   if(w > 284)
    if(t <= 1.05) return 50;
    if(t > 1.05) return 30;
if(p.equals("PP12186"))
 if(w <= 263) return 65;
 if(w > 263) return 80;
if(p.equals("PP12187")) return 60;
if(p.equals("PP12188")) return 50;
if(p.equals("PP12190")) return 60;
if(p.equals("PP12192"))
 if(t <= 0.49) return 70;
 if(t > 0.49) return 60;
if(p.equals("PP12193"))
 if(w <= 233)
  if(we <= 3010)
   if(we <= 2824) return 50;
   if(we > 2824)
    if(we <= 2961) return 65;
    if(we > 2961) return 70;
  if(we > 3010) return 60;
 if(w > 233) return 85;
if(p.equals("PP12194"))
 if(t <= 0.33) return 135;
 if(t > 0.33)
  if(w <= 316)
   if(t <= 0.8)
    if(t <= 0.407) return 55;
    if(t > 0.407) return 60;
   if(t > 0.8) return 55;
  if(w > 316)
   if(we <= 4181)
    if(we <= 3934) return 40;
    if(we > 3934) return 70;
   if(we > 4181)
    if(t <= 0.7) return 80;
    if(t > 0.7) return 60;
if(p.equals("PP12195"))
 if(t <= 0.71) return 95;
 if(t > 0.71) return 85;
if(p.equals("PP12196")) return 40;
if(p.equals("PP12197")) return 42;
if(p.equals("PP12198"))
 if(t <= 1.62) return 70;
 if(t > 1.62) return 60;
if(p.equals("PP12201"))
 if(w <= 310) return 55;
 if(w > 310) return 60;
if(p.equals("PP12202")) return 35;
if(p.equals("PP12204")) return 25;
if(p.equals("PP12205")) return 35;
if(p.equals("PP12206"))
 if(w <= 289) return 35;
 if(w > 289) return 50;
if(p.equals("PP12211"))
 if(we <= 3776)
  if(we <= 3590) return 38.5;
  if(we > 3590) return 35;
 if(we > 3776)
  if(we <= 5211)
   if(we <= 4388)
    if(w <= 392.5) return 40;
    if(w > 392.5) return 60;
   if(we > 4388) return 45;
  if(we > 5211)
   if(we <= 5261) return 38;
   if(we > 5261) return 50;
if(p.equals("PP12213")) return 75;
if(p.equals("PP12221")) return 75;
if(p.equals("PP12227")) return 30;
if(p.equals("PP12232"))
 if(w <= 323) return 35;
 if(w > 323) return 37;
if(p.equals("PP12234"))
 if(t <= 0.33) return 72.5;
 if(t > 0.33) return 65;
if(p.equals("PP12235")) return 50;
if(p.equals("PP12237"))
 if(w <= 249) return 25;
 if(w > 249)
  if(t <= 2.12) return 22;
  if(t > 2.12) return 30;
if(p.equals("PP12238"))
 if(w <= 357)
  if(we <= 3780) return 18;
  if(we > 3780) return 25;
 if(w > 357) return 20;
if(p.equals("PP12245"))
 if(w <= 315)
  if(t <= 0.95)
   if(we <= 4243) return 55;
   if(we > 4243) return 65;
  if(t > 0.95) return 45;
 if(w > 315) return 20;
if(p.equals("PP12247")) return 60;
if(p.equals("PP12249")) return 45;
if(p.equals("PP12251")) return 60;
if(p.equals("PP12253")) return 50;
if(p.equals("PP12256")) return 80;
if(p.equals("PP12257"))
 if(t <= 1.62)
  if(t <= 1.36)
   if(t <= 1.15) return 65;
   if(t > 1.15)
    if(w <= 242) return 60;
    if(w > 242) return 55;
  if(t > 1.36)
   if(t <= 1.45) return 55;
   if(t > 1.45)
    if(we <= 2001) return 45;
    if(we > 2001)
     if(w <= 211) return 45;
     if(w > 211) return 50;
 if(t > 1.62)
  if(t <= 1.81) return 45;
  if(t > 1.81)
   if(w <= 241)
    if(we <= 2731)
     if(we <= 2464) return 40;
     if(we > 2464) return 35;
    if(we > 2731)
     if(we <= 2900) return 45;
     if(we > 2900) return 35;
   if(w > 241) return 40;
if(p.equals("PP12263")) return 60;
if(p.equals("PP12266"))
 if(w <= 282)
  if(we <= 3500) return 45;
  if(we > 3500) return 55;
 if(w > 282) return 47;
if(p.equals("PP12273")) return 55;
if(p.equals("PP12280")) return 50;
if(p.equals("PP12286")) return 50;
if(p.equals("PP12296")) return 60;
if(p.equals("PP12297")) return 80;
if(p.equals("PP12298")) return 25;
if(p.equals("PP12302"))
 if(we <= 2866) return 50;
 if(we > 2866) return 45;
if(p.equals("PP12306")) return 35;
if(p.equals("PP12307")) return 50;
if(p.equals("PP12313"))
 if(t <= 1.5) return 55;
 if(t > 1.5)
  if(w <= 232)
   if(t <= 1.695) return 45;
   if(t > 1.695)
    if(we <= 2123) return 45;
    if(we > 2123) return 50;
  if(w > 232) return 50;
if(p.equals("PP12319")) return 55;
if(p.equals("PP12324"))
 if(t <= 1.8) return 22.5;
 if(t > 1.8) return 22;
if(p.equals("PP12325"))
 if(t <= 0.73) return 40;
 if(t > 0.73)
  if(w <= 285) return 40;
  if(w > 285) return 60;
if(p.equals("PP12326"))
 if(w <= 228)
  if(t <= 0.53) return 30;
  if(t > 0.53) return 70;
 if(w > 228)
  if(t <= 0.81) return 40;
  if(t > 0.81)
   if(t <= 1.1)
    if(t <= 0.91) return 55;
    if(t > 0.91) return 70;
   if(t > 1.1) return 45;
if(p.equals("PP12327"))
 if(t <= 0.66)
  if(t <= 0.33)
   if(w <= 237) return 90;
   if(w > 237) return 80;
  if(t > 0.33)
   if(w <= 267) return 70;
   if(w > 267) return 65;
 if(t > 0.66)
  if(t <= 1.05)
   if(we <= 3315)
    if(w <= 243) return 37;
    if(w > 243) return 45;
   if(we > 3315)
    if(w <= 256) return 60;
    if(w > 256) return 50;
  if(t > 1.05) return 45;
if(p.equals("PP12328"))
 if(we <= 4516)
  if(t <= 0.45)
   if(w <= 321)
    if(w <= 309) return 75;
    if(w > 309) return 50;
   if(w > 321)
    if(w <= 323) return 70;
    if(w > 323) return 90;
  if(t > 0.45) return 70;
 if(we > 4516)
  if(w <= 347) return 100;
  if(w > 347) return 70;
if(p.equals("PP12329"))
 if(w <= 307) return 60;
 if(w > 307) return 65;
if(p.equals("PP12330")) return 75;
if(p.equals("PP12332")) return 60;
if(p.equals("PP12336")) return 22.5;
if(p.equals("PP12337"))
 if(t <= 2.58)
  if(w <= 310) return 40;
  if(w > 310)
   if(w <= 368) return 55;
   if(w > 368) return 50;
 if(t > 2.58)
  if(we <= 3578)
   if(w <= 323) return 27;
   if(w > 323) return 37;
  if(we > 3578)
   if(we <= 4224) return 37.5;
   if(we > 4224) return 25;
if(p.equals("PP12340"))
 if(we <= 3010)
  if(we <= 2780) return 40;
  if(we > 2780)
   if(w <= 207) return 45;
   if(w > 207) return 40;
 if(we > 3010) return 55;
if(p.equals("PP12342"))
 if(t <= 1.1) return 70;
 if(t > 1.1) return 50;
if(p.equals("PP12344")) return 50;
if(p.equals("PP12349")) return 55;
if(p.equals("PP12350")) return 55;
if(p.equals("PP12352"))
 if(w <= 212) return 40;
 if(w > 212)
  if(t <= 0.53) return 80;
  if(t > 0.53) return 65;
if(p.equals("PP12356")) return 40;
if(p.equals("PP12357"))
 if(w <= 350)
  if(w <= 307) return 40;
  if(w > 307) return 35;
 if(w > 350)
  if(t <= 0.95) return 30;
  if(t > 0.95) return 45;
if(p.equals("PP12359"))
 if(t <= 1.92)
  if(w <= 269)
   if(w <= 210.5) return 45;
   if(w > 210.5) return 12;
  if(w > 269) return 15;
 if(t > 1.92)
  if(we <= 3116) return 30;
  if(we > 3116)
   if(t <= 1.98) return 50;
   if(t > 1.98) return 33;
if(p.equals("PP12360"))
 if(w <= 315) return 60;
 if(w > 315)
  if(t <= 0.91) return 45;
  if(t > 0.91) return 47.5;
if(p.equals("PP12361")) return 70;
if(p.equals("PP12362"))
 if(w <= 261)
  if(t <= 0.75) return 90;
  if(t > 0.75) return 60;
 if(w > 261) return 110;
if(p.equals("PP12363"))
 if(t <= 1) return 55;
 if(t > 1) return 60;
if(p.equals("PP12364"))
 if(t <= 0.75)
  if(t <= 0.51) return 90;
  if(t > 0.51) return 40;
 if(t > 0.75)
  if(we <= 3856) return 70;
  if(we > 3856) return 80;
if(p.equals("PP12366")) return 50;
if(p.equals("PP12370")) return 70;
if(p.equals("PP12373")) return 35;
if(p.equals("PP12376")) return 60;
if(p.equals("PP12377"))
 if(t <= 0.725) return 55;
 if(t > 0.725) return 45;
if(p.equals("PP12383")) return 70;
if(p.equals("PP12384")) return 45;
if(p.equals("PP12389"))
 if(t <= 1.895) return 30;
 if(t > 1.895) return 26;
if(p.equals("PP12395")) return 22.5;
if(p.equals("PP12399"))
 if(t <= 0.81) return 55;
 if(t > 0.81) return 60;
if(p.equals("PP12408")) return 60;
if(p.equals("PP12409")) return 45;
if(p.equals("PP12414")) return 45;
if(p.equals("PP12421"))
 if(t <= 1.54) return 30;
 if(t > 1.54) return 15;
if(p.equals("PP12422"))
 if(t <= 1.36) return 20;
 if(t > 1.36) return 25;
if(p.equals("PP12424")) return 60;
if(p.equals("PP12428"))
 if(we <= 3308) return 65;
 if(we > 3308) return 55;
if(p.equals("PP12429"))
 if(we <= 3550) return 70;
 if(we > 3550) return 80;
if(p.equals("PP12430")) return 45;
if(p.equals("PP12432")) return 20;
if(p.equals("PP12433"))
 if(t <= 1.15)
  if(t <= 1.075) return 25;
  if(t > 1.075) return 50;
 if(t > 1.15)
  if(t <= 1.27) return 22;
  if(t > 1.27) return 40;
if(p.equals("PP12436")) return 40;
if(p.equals("PP12437"))
 if(w <= 306)
  if(we <= 3986)
   if(we <= 2906) return 35;
   if(we > 2906) return 70;
  if(we > 3986)
   if(we <= 4072) return 75;
   if(we > 4072) return 35;
 if(w > 306)
  if(t <= 0.61) return 75;
  if(t > 0.61)
   if(w <= 357)
    if(we <= 3949) return 60;
    if(we > 3949) return 70;
   if(w > 357)
    if(we <= 5468) return 50;
    if(we > 5468) return 60;
if(p.equals("PP12440"))
 if(t <= 1.32)
  if(we <= 5300) return 20;
  if(we > 5300) return 30;
 if(t > 1.32) return 15;
if(p.equals("PP12443"))
 if(t <= 1.1) return 45;
 if(t > 1.1) return 75;
if(p.equals("PP12444"))
 if(w <= 258) return 75;
 if(w > 258) return 65;
if(p.equals("PP12446"))
 if(w <= 265) return 37;
 if(w > 265) return 40;
if(p.equals("PP12447"))
 if(t <= 2.18) return 20;
 if(t > 2.18) return 40;
if(p.equals("PP12449"))
 if(t <= 1.82) return 50;
 if(t > 1.82) return 45;
if(p.equals("PP12450"))
 if(t <= 1.97) return 30;
 if(t > 1.97) return 20;
if(p.equals("PP12460")) return 92.5;
if(p.equals("PP12461"))
 if(we <= 2625)
  if(w <= 214)
   if(w <= 209) return 45;
   if(w > 209) return 60;
  if(w > 214)
   if(t <= 1.85) return 55;
   if(t > 1.85) return 45;
 if(we > 2625) return 60;
if(p.equals("PP12464")) return 50;
if(p.equals("PP12466")) return 60;
if(p.equals("PP12468")) return 60;
if(p.equals("PP12470"))
 if(w <= 271) return 12;
 if(w > 271) return 15;
if(p.equals("PP12471"))
 if(we <= 2058) return 20;
 if(we > 2058)
  if(we <= 2072.5) return 45;
  if(we > 2072.5) return 27.5;
if(p.equals("PP12477"))
 if(we <= 2066)
  if(t <= 1.74) return 45;
  if(t > 1.74)
   if(w <= 206) return 25;
   if(w > 206) return 45;
 if(we > 2066) return 30;
if(p.equals("PP12478"))
 if(t <= 2.48) return 40;
 if(t > 2.48) return 45;
if(p.equals("PP12480")) return 45;
if(p.equals("PP12481"))
 if(t <= 2.2) return 17;
 if(t > 2.2) return 43;
if(p.equals("PP12486"))
 if(w <= 261)
  if(we <= 1725) return 35;
  if(we > 1725) return 25;
 if(w > 261) return 30;
if(p.equals("PP12487")) return 35;
if(p.equals("PP12491")) return 45;
if(p.equals("PP12499")) return 40;
if(p.equals("PP12500")) return 45;
if(p.equals("PP12508")) return 42.5;
if(p.equals("PP12510")) return 15;
if(p.equals("PP12514")) return 45;
if(p.equals("PP12521")) return 60;
if(p.equals("PP12526"))
 if(we <= 3438) return 55;
 if(we > 3438)
  if(t <= 1.1) return 65;
  if(t > 1.1) return 45;
if(p.equals("PP12529")) return 55;
if(p.equals("PP12530")) return 70;
if(p.equals("PP12532")) return 40;
if(p.equals("PP12533")) return 60;
if(p.equals("PP12544")) return 65;
if(p.equals("PP12550")) return 20;
if(p.equals("PP12553")) return 35;
if(p.equals("PP12555")) return 20;
if(p.equals("PP12558"))
 if(t <= 1.1) return 50;
 if(t > 1.1) return 55;
if(p.equals("PP12578")) return 70;
if(p.equals("PP21894"))
 if(t <= 1.45)
  if(w <= 228)
   if(w <= 217) return 75;
   if(w > 217) return 60;
  if(w > 228) return 55;
 if(t > 1.45)
  if(t <= 1.73)
   if(we <= 2368) return 45;
   if(we > 2368)
    if(t <= 1.695)
     if(we <= 2486) return 47;
     if(we > 2486) return 45;
    if(t > 1.695) return 50;
  if(t > 1.73)
   if(w <= 218)
    if(we <= 2232) return 40;
    if(we > 2232) return 25;
   if(w > 218)
    if(we <= 2113)
     if(we <= 2042) return 35;
     if(we > 2042) return 45;
    if(we > 2113)
     if(t <= 2.03) return 43;
     if(t > 2.03) return 35;
if(p.equals("PP21895"))
 if(t <= 2.4) return 27;
 if(t > 2.4) return 35;
if(p.equals("PP21901")) return 30;
if(p.equals("PP21904"))
 if(w <= 283)
  if(t <= 2.84)
   if(w <= 226)
    if(t <= 2.4) return 35;
    if(t > 2.4) return 40;
   if(w > 226)
    if(t <= 2.83)
     if(w <= 232)
      if(t <= 2.7)
       if(we <= 2500) return 35;
       if(we > 2500) return 45;
      if(t > 2.7) return 30;
     if(w > 232)
      if(we <= 2882) return 50;
      if(we > 2882) return 35;
    if(t > 2.83) return 30;
  if(t > 2.84)
   if(t <= 2.91)
    if(we <= 2403) return 21;
    if(we > 2403)
     if(t <= 2.88) return 30;
     if(t > 2.88)
      if(w <= 278) return 40;
      if(w > 278) return 32;
   if(t > 2.91)
    if(t <= 3)
     if(t <= 2.93)
      if(w <= 257.5) return 30;
      if(w > 257.5)
       if(we <= 2847) return 25;
       if(we > 2847)
        if(we <= 2966) return 27;
        if(we > 2966) return 28;
     if(t > 2.93)
      if(w <= 241) return 25;
      if(w > 241)
       if(we <= 3044) return 40;
       if(we > 3044) return 50;
    if(t > 3)
     if(t <= 3.35) return 35;
     if(t > 3.35) return 40;
 if(w > 283)
  if(t <= 3.2)
   if(we <= 3168)
    if(t <= 3.14)
     if(t <= 3.05) return 35;
     if(t > 3.05) return 30;
    if(t > 3.14) return 20;
   if(we > 3168) return 25;
  if(t > 3.2)
   if(t <= 4.04)
    if(t <= 3.5)
     if(we <= 3292)
      if(w <= 295)
       if(we <= 3020) return 35;
       if(we > 3020) return 25;
      if(w > 295)
       if(we <= 3099) return 25;
       if(we > 3099) return 27;
     if(we > 3292)
      if(we <= 3667)
       if(we <= 3484) return 40;
       if(we > 3484) return 20;
      if(we > 3667) return 35;
    if(t > 3.5)
     if(w <= 358) return 25;
     if(w > 358)
      if(we <= 4016) return 33.5;
      if(we > 4016) return 30;
   if(t > 4.04)
    if(we <= 3728) return 29;
    if(we > 3728)
     if(we <= 3814) return 22;
     if(we > 3814) return 20;
if(p.equals("PP21906"))
 if(t <= 1.65) return 30;
 if(t > 1.65) return 22.5;
if(p.equals("PP21909")) return 50;
if(p.equals("PP21911"))
 if(we <= 3148)
  if(t <= 2.21)
   if(w <= 253) return 42.5;
   if(w > 253) return 50;
  if(t > 2.21)
   if(w <= 243)
    if(w <= 238) return 30;
    if(w > 238)
     if(w <= 239.5) return 30;
     if(w > 239.5) return 35;
   if(w > 243) return 30;
 if(we > 3148)
  if(t <= 3.69)
   if(w <= 266)
    if(w <= 245) return 37;
    if(w > 245) return 45;
   if(w > 266)
    if(t <= 2.97) return 35;
    if(t > 2.97)
     if(we <= 5300)
      if(we <= 5268)
       if(we <= 5232) return 46.6666666666667;
       if(we > 5232) return 40;
      if(we > 5268) return 43.3333333333333;
     if(we > 5300) return 45;
  if(t > 3.69) return 25;
if(p.equals("PP21913"))
 if(t <= 1.73)
  if(t <= 1.57) return 60;
  if(t > 1.57)
   if(w <= 341)
    if(we <= 1963) return 45;
    if(we > 1963)
     if(w <= 209.5) return 45;
     if(w > 209.5)
      if(t <= 1.71) return 55;
      if(t > 1.71) return 45;
   if(w > 341)
    if(w <= 350) return 60;
    if(w > 350) return 55;
 if(t > 1.73) return 40;
if(p.equals("PP21915"))
 if(t <= 2.52)
  if(t <= 2.4)
   if(we <= 2402)
    if(we <= 2345) return 25;
    if(we > 2345) return 23;
   if(we > 2402) return 35;
  if(t > 2.4) return 60;
 if(t > 2.52)
  if(w <= 262)
   if(w <= 251)
    if(w <= 238) return 40;
    if(w > 238)
     if(t <= 2.74)
      if(we <= 2306) return 30;
      if(we > 2306) return 35;
     if(t > 2.74) return 25;
   if(w > 251)
    if(we <= 2870)
     if(we <= 2478) return 38;
     if(we > 2478) return 35;
    if(we > 2870) return 45;
  if(w > 262)
   if(w <= 264) return 40;
   if(w > 264)
    if(t <= 2.93)
     if(we <= 2984) return 31.6666666666667;
     if(we > 2984) return 28;
    if(t > 2.93) return 25;
if(p.equals("PP21917")) return 40;
if(p.equals("PP21918")) return 75;
if(p.equals("PP21920"))
 if(we <= 3527)
  if(t <= 2.78)
   if(t <= 1.5) return 50;
   if(t > 1.5)
    if(w <= 249)
     if(t <= 2.4) return 35;
     if(t > 2.4)
      if(w <= 236) return 40;
      if(w > 236)
       if(we <= 2754) return 35;
       if(we > 2754) return 45;
    if(w > 249)
     if(w <= 293) return 35;
     if(w > 293) return 36;
  if(t > 2.78)
   if(t <= 3.54)
    if(we <= 3170) return 35;
    if(we > 3170) return 25;
   if(t > 3.54)
    if(t <= 4.04)
     if(w <= 245) return 30;
     if(w > 245) return 20;
    if(t > 4.04) return 20;
 if(we > 3527)
  if(t <= 2.56)
   if(t <= 1.5) return 60;
   if(t > 1.5) return 50;
  if(t > 2.56) return 40;
if(p.equals("PP21922"))
 if(t <= 2.12)
  if(w <= 293)
   if(we <= 2954)
    if(w <= 249)
     if(w <= 231) return 20;
     if(w > 231) return 30;
    if(w > 249)
     if(we <= 2726) return 24;
     if(we > 2726) return 22;
   if(we > 2954) return 25;
  if(w > 293) return 30;
 if(t > 2.12)
  if(w <= 249) return 40;
  if(w > 249)
   if(we <= 2939)
    if(we <= 2877) return 45;
    if(we > 2877) return 21;
   if(we > 2939) return 35;
if(p.equals("PP21923"))
 if(w <= 291) return 30;
 if(w > 291) return 42;
if(p.equals("PP21926"))
 if(w <= 246) return 75;
 if(w > 246)
  if(t <= 1.05) return 70;
  if(t > 1.05) return 45;
if(p.equals("PP21931"))
 if(t <= 1.25) return 55;
 if(t > 1.25) return 42.5;
if(p.equals("PP21936"))
 if(w <= 295) return 25;
 if(w > 295) return 23;
if(p.equals("PP21937"))
 if(we <= 4769)
  if(we <= 4531) return 55;
  if(we > 4531) return 60;
 if(we > 4769)
  if(t <= 1.1) return 70;
  if(t > 1.1) return 65;
if(p.equals("PP21938")) return 27;
if(p.equals("PP21939")) return 60;
if(p.equals("PP21940")) return 35;
if(p.equals("PP21943")) return 50;
if(p.equals("PP21945")) return 65;
if(p.equals("PP21956")) return 25;
if(p.equals("PP21957")) return 35;
if(p.equals("PP21958"))
 if(we <= 3272) return 85;
 if(we > 3272) return 47;
if(p.equals("PP21972")) return 55;
if(p.equals("PP21973"))
 if(t <= 0.66) return 40;
 if(t > 0.66) return 50;
if(p.equals("PP21976")) return 30;
if(p.equals("PP22001"))
 if(t <= 2.19)
  if(t <= 1.83) return 28.3333333333333;
  if(t > 1.83) return 27;
 if(t > 2.19)
  if(w <= 278) return 25;
  if(w > 278) return 35;
if(p.equals("PP22002"))
 if(w <= 289)
  if(t <= 2.93)
   if(we <= 2746)
    if(we <= 2585)
     if(we <= 2370)
      if(t <= 2.7)
       if(w <= 211) return 45;
       if(w > 211) return 30;
      if(t > 2.7) return 25;
     if(we > 2370)
      if(we <= 2570) return 35;
      if(we > 2570) return 32;
    if(we > 2585)
     if(w <= 249) return 30;
     if(w > 249) return 23;
   if(we > 2746)
    if(w <= 235) return 28;
    if(w > 235)
     if(w <= 252.5)
      if(w <= 251) return 40;
      if(w > 251) return 23;
     if(w > 252.5)
      if(w <= 279)
       if(t <= 2.56) return 35;
       if(t > 2.56)
        if(we <= 2984) return 30;
        if(we > 2984)
         if(we <= 3014) return 35;
         if(we > 3014) return 40;
      if(w > 279) return 40;
  if(t > 2.93)
   if(t <= 3.09)
    if(w <= 242)
     if(w <= 229) return 30;
     if(w > 229)
      if(w <= 237) return 20;
      if(w > 237)
       if(w <= 240) return 30;
       if(w > 240) return 20;
    if(w > 242)
     if(we <= 2902)
      if(w <= 253)
       if(w <= 245) return 35;
       if(w > 245)
        if(w <= 247) return 30;
        if(w > 247) return 45;
      if(w > 253)
       if(we <= 2746) return 30;
       if(we > 2746) return 32;
     if(we > 2902)
      if(w <= 278)
       if(w <= 277)
        if(w <= 246) return 30;
        if(w > 246)
         if(w <= 268) return 35;
         if(w > 268)
          if(we <= 3292) return 30;
          if(we > 3292) return 25;
       if(w > 277) return 23;
      if(w > 278)
       if(w <= 279) return 30;
       if(w > 279)
        if(we <= 3336) return 30;
        if(we > 3336) return 35;
   if(t > 3.09)
    if(t <= 3.59)
     if(we <= 2722) return 28;
     if(we > 2722) return 30;
    if(t > 3.59) return 20;
 if(w > 289)
  if(we <= 4142)
   if(w <= 322)
    if(t <= 4.15)
     if(we <= 3384) return 25;
     if(we > 3384)
      if(we <= 3645) return 35;
      if(we > 3645) return 30;
    if(t > 4.15) return 17;
   if(w > 322)
    if(t <= 3.48)
     if(w <= 335)
      if(we <= 3828) return 37;
      if(we > 3828) return 35;
     if(w > 335) return 37;
    if(t > 3.48) return 27;
  if(we > 4142)
   if(t <= 2.7) return 50;
   if(t > 2.7) return 40;
if(p.equals("PP22005"))
 if(we <= 4222) return 45;
 if(we > 4222) return 58;
if(p.equals("PP22007"))
 if(we <= 3640) return 30;
 if(we > 3640) return 32;
if(p.equals("PP22014")) return 35;
if(p.equals("PP22015")) return 40;
if(p.equals("PP22017")) return 30;
if(p.equals("PP22035"))
 if(w <= 245) return 50;
 if(w > 245) return 35;
if(p.equals("PP22036"))
 if(w <= 282)
  if(w <= 257.5) return 40;
  if(w > 257.5) return 50;
 if(w > 282)
  if(we <= 4268) return 45;
  if(we > 4268) return 55;
if(p.equals("PP22038"))
 if(w <= 265) return 40;
 if(w > 265) return 55;
if(p.equals("PP22041")) return 60;
if(p.equals("PP22054")) return 10;
if(p.equals("PP22070"))
 if(t <= 2.58) return 55;
 if(t > 2.58) return 35;
if(p.equals("PP22071")) return 45;
if(p.equals("PP22072")) return 40;
if(p.equals("PP22080"))
 if(w <= 344)
  if(t <= 0.45) return 70;
  if(t > 0.45) return 65;
 if(w > 344) return 95;
if(p.equals("PP22083"))
 if(t <= 0.46) return 55;
 if(t > 0.46) return 85;
if(p.equals("PP22086")) return 55;
if(p.equals("PP22093")) return 55;
if(p.equals("PP22094")) return 55;
if(p.equals("PP22102")) return 40;
if(p.equals("PP22112"))
 if(w <= 312)
  if(we <= 4348) return 60;
  if(we > 4348)
   if(we <= 4358) return 45;
   if(we > 4358) return 52.5;
 if(w > 312)
  if(w <= 343) return 42.5;
  if(w > 343)
   if(t <= 0.91) return 35;
   if(t > 0.91) return 65;
if(p.equals("PP22115"))
 if(we <= 2906) return 50;
 if(we > 2906) return 60;
if(p.equals("PP22135")) return 60;
if(p.equals("PP22157"))
 if(we <= 4492)
  if(w <= 392.5) return 100;
  if(w > 392.5) return 60;
 if(we > 4492) return 60;
if(p.equals("PP22159"))
 if(t <= 1.095)
  if(t <= 0.825) return 70;
  if(t > 0.825)
   if(w <= 218) return 70;
   if(w > 218) return 75;
 if(t > 1.095) return 40;
if(p.equals("PP22175")) return 42;
if(p.equals("PP22184")) return 40;
if(p.equals("PP22186")) return 35;
if(p.equals("PP22195")) return 45;
if(p.equals("PP22209")) return 15;
if(p.equals("PP22210")) return 17.5;
if(p.equals("PP22215")) return 60;
if(p.equals("PP22225")) return 65;
if(p.equals("PP22229")) return 45;
if(p.equals("PP22230")) return 85;
if(p.equals("PP22231")) return 60;
if(p.equals("PP22236"))
 if(t <= 0.825)
  if(w <= 255) return 60;
  if(w > 255) return 30;
 if(t > 0.825) return 40;
if(p.equals("PP22243"))
 if(w <= 282) return 50;
 if(w > 282)
  if(w <= 296) return 55;
  if(w > 296) return 50;
if(p.equals("PP22246"))
 if(t <= 2.98) return 10;
 if(t > 2.98)
  if(w <= 249) return 32;
  if(w > 249) return 30;
if(p.equals("PP22250")) return 35;
if(p.equals("PP22256")) return 45;
if(p.equals("PP22277"))
 if(t <= 0.53) return 60;
 if(t > 0.53) return 45;
if(p.equals("PP22293")) return 50;
if(p.equals("PP22297"))
 if(w <= 237) return 70;
 if(w > 237) return 37.5;
if(p.equals("PP22298")) return 65;
if(p.equals("PP22299")) return 90;
if(p.equals("PP22307"))
 if(we <= 3612) return 22;
 if(we > 3612) return 25;
if(p.equals("PP22316")) return 18;
if(p.equals("PP22317")) return 45;
if(p.equals("PP22319")) return 60;
if(p.equals("PP22336"))
 if(t <= 2.4) return 27;
 if(t > 2.4)
  if(we <= 3585)
   if(we <= 3480) return 45;
   if(we > 3480) return 35;
  if(we > 3585) return 33;
if(p.equals("PP22339")) return 35;
if(p.equals("PP22340")) return 20;
if(p.equals("PP22341"))
 if(w <= 250)
  if(t <= 1.78) return 15;
  if(t > 1.78)
   if(we <= 2898) return 40;
   if(we > 2898) return 30;
 if(w > 250)
  if(t <= 2.18) return 20;
  if(t > 2.18) return 35;
if(p.equals("PP22342")) return 40;
if(p.equals("PP22345")) return 35;
if(p.equals("PP22350")) return 45;
if(p.equals("PP22351")) return 40;
if(p.equals("PP22354"))
 if(w <= 248) return 65;
 if(w > 248) return 50;
if(p.equals("PP22355"))
 if(w <= 315)
  if(we <= 4243) return 45;
  if(we > 4243) return 40;
 if(w > 315)
  if(w <= 326) return 70;
  if(w > 326) return 50;
if(p.equals("PP22361")) return 80;
if(p.equals("PP22363")) return 60;
if(p.equals("PP22365")) return 45;
if(p.equals("PP22406")) return 55;
if(p.equals("PP22408")) return 70;
if(p.equals("PP22409")) return 55;
if(p.equals("PP22410")) return 60;
if(p.equals("PP22416"))
 if(t <= 1.81)
  if(t <= 1.7) return 47;
  if(t > 1.7) return 45;
 if(t > 1.81) return 40;
if(p.equals("PP22417")) return 40;
if(p.equals("PP22420")) return 35;
if(p.equals("PP22421")) return 45;
if(p.equals("PP22422")) return 35;
if(p.equals("PP22424"))
 if(we <= 3152) return 29.5;
 if(we > 3152) return 31;
if(p.equals("PP22425"))
 if(t <= 0.75) return 45;
 if(t > 0.75) return 35;
if(p.equals("PP22426")) return 55;
if(p.equals("PP22431"))
 if(t <= 0.7) return 65;
 if(t > 0.7) return 50;
if(p.equals("PP22435")) return 40;
if(p.equals("PP22457")) return 60;
if(p.equals("PP22468")) return 65;
if(p.equals("PP22473")) return 45;
if(p.equals("PP22474"))
 if(w <= 299) return 25;
 if(w > 299) return 20;
if(p.equals("PP22482"))
 if(t <= 0.825) return 75;
 if(t > 0.825)
  if(we <= 2994) return 25;
  if(we > 2994) return 45;
if(p.equals("PP22485"))
 if(we <= 3937) return 42.5;
 if(we > 3937) return 45;
if(p.equals("PP22486"))
 if(w <= 296) return 35;
 if(w > 296) return 40;
if(p.equals("PP22488"))
 if(we <= 3616) return 48.5;
 if(we > 3616) return 42.5;
if(p.equals("PP22490")) return 70;
if(p.equals("PP22491")) return 45;
if(p.equals("PP22492")) return 60;
if(p.equals("PP22498")) return 35;
if(p.equals("PP22499"))
 if(t <= 1) return 45;
 if(t > 1) return 25;
if(p.equals("PP22503"))
 if(w <= 246) return 26;
 if(w > 246) return 40;
if(p.equals("PP22504")) return 20;
if(p.equals("PP22505"))
 if(t <= 1.94) return 55;
 if(t > 1.94) return 40;
if(p.equals("PP22513"))
 if(w <= 235) return 40;
 if(w > 235)
  if(t <= 1.94) return 30;
  if(t > 1.94) return 40;
if(p.equals("PP22534"))
 if(t <= 3.47)
  if(t <= 2.73)
   if(t <= 2.63) return 32;
   if(t > 2.63)
    if(we <= 2512) return 25;
    if(we > 2512) return 20;
  if(t > 2.73)
   if(w <= 261)
    if(w <= 255) return 30;
    if(w > 255) return 27;
   if(w > 261)
    if(we <= 2987)
     if(we <= 2840) return 45;
     if(we > 2840) return 20;
    if(we > 2987)
     if(we <= 3080) return 30;
     if(we > 3080) return 35;
 if(t > 3.47)
  if(we <= 3168) return 27;
  if(we > 3168) return 28;
if(p.equals("PP22535"))
 if(we <= 3182)
  if(w <= 236)
   if(w <= 228)
    if(we <= 2746) return 35;
    if(we > 2746) return 15;
   if(w > 228)
    if(we <= 2486) return 22;
    if(we > 2486) return 23;
  if(w > 236)
   if(w <= 245) return 25;
   if(w > 245)
    if(we <= 2349) return 20;
    if(we > 2349) return 30;
 if(we > 3182)
  if(t <= 3.59)
   if(w <= 281) return 40;
   if(w > 281)
    if(w <= 341) return 45;
    if(w > 341) return 40;
  if(t > 3.59) return 20;
if(p.equals("PP22540")) return 70;
if(p.equals("PP22541")) return 170;
if(p.equals("PP22542")) return 70;
if(p.equals("PP22546"))
 if(t <= 2.84) return 45;
 if(t > 2.84) return 41;
if(p.equals("PP22550")) return 42.5;
if(p.equals("PP22553")) return 65;
if(p.equals("PP22563"))
 if(t <= 2.29) return 35;
 if(t > 2.29)
  if(t <= 2.58) return 22.5;
  if(t > 2.58) return 27;
if(p.equals("PP22566")) return 35;
if(p.equals("PP22568")) return 30;
if(p.equals("PP22590")) return 40;
if(p.equals("PP22600")) return 35;
if(p.equals("PP22601"))
 if(t <= 1.83)
  if(w <= 210.5) return 45;
  if(w > 210.5) return 40;
 if(t > 1.83) return 30;
if(p.equals("WS21267")) return 45;
return 45.0;
}
}
