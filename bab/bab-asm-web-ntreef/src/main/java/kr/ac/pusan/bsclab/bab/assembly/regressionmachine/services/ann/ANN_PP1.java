package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_PP1 implements ANN_base {
	
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-0.2886003, -0.78511858, 0.8418383, 0.57467264, 0.01768204, -1.53127348
            				, -1.67365515, -1.69826019, -0.55446488, 0.25428274, -0.26881036, 0.95718884
            				, 0.3713572, 1.43896151, 1.22454631, -0.66324824, -0.43539536, 0.91556489
            				, -0.08031289, 0.93031001}, 
            			{-0.75571644, -0.70443082, -1.26885056, 0.94891751, -0.1533692, -0.22509882
        					, 0.63901252, 1.04603362, -0.9462043, -0.85645324, -0.92050058, -0.10458641
        					, 0.20295861, 0.52836031, 0.39898026, -0.1952969, -0.79731542, 0.91599894
        					, -0.54014856, -0.98317832},
            			{-1.29473269, -0.1599531, -1.35068226, -1.05451286, -0.63732761, 0.00234067
    						, 1.21658301, 0.86617953, 1.04981124, 0.21634553, 0.04253871, -1.05206227
    						, -0.07556278, 0.86843848, -1.64264619, -0.7064563, 0.09545442, 1.61558735
    						, -0.67804277, 1.54180253}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {2.10042453, 0.57711822, 1.43174326, -1.18280554, -0.66789591, -0.29961732
            			, 0.14401989, 0.18172938, 0.41326275, -0.15729412, -0.3765581, -0.57493544
            			, -0.29200155, 0.86158365, 0.85211521, -0.23234914, 0.79756397, 0.18618275
            			, -0.12319, -0.67572588};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.32827568,
            			-0.69685692,
            			-0.97540861,
            			-0.2742103 ,
            			-1.36731422,
            			-1.48106551,
            			1.16770256,
            			1.0317837 ,
            			-1.13625276,
            			0.31616583,
            			-2.41135716,
            			1.14080584,
            			-1.24197173,
            			-1.42734551,
            			1.31022489,
            			1.32350135,
            			-1.67588341,
            			1.40430832,
            			0.63583362,
            			-1.23862863};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.63548261};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 6.3;
	}

	@Override
	public double getMaxWidth() {
		
		return 1284.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25500.00;
	}

	@Override
	public double getMinThick() {
		
		return 1.4;
	}

	@Override
	public double getMinWidth() {
		
		return 877.0;
	}

	@Override
	public double getMinWeight() {
		
		return 7212.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
