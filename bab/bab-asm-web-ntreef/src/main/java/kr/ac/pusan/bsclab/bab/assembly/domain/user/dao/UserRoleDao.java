package kr.ac.pusan.bsclab.bab.assembly.domain.user.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.assembly.domain.user.UserRole;

@Repository
public class UserRoleDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<UserRole> findAllByUserNo(int userNo) {
		String sql = "select no, role, userNo from user_roles where userNo=?";
		return jdbcTemplate.query(sql, new Object[]{userNo}, new UserRoleRowMapper());
	}
	
	public int save(UserRole userRole) {
		String sql = "INSERT INTO user_roles(userNo, role)"
									+ " VALUES(?, ?)";
		return jdbcTemplate.update(sql
									, new Object[]{userRole.getUserNo()
													, userRole.getRole()});
	}
}

class UserRoleRowMapper implements RowMapper<UserRole>{

	@Override
	public UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserRole userRole = new UserRole();
		userRole.setNo(rs.getInt("no"));
		userRole.setRole(rs.getString("role"));
		userRole.setUserNo(rs.getInt("userNo"));
		return userRole;
	} 
}
