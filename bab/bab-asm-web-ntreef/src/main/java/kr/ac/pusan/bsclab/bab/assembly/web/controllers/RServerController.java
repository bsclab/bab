package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

/**
 * RServer web controller
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

@Controller
public class RServerController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/rservices";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/correlation/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getRServiceData(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("rservices/rservices");
		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		view.addObject("jsonData", jsonData);
		view.addObject("apiURI", apiManager.getAPIURI());
		return view;
	}
}
