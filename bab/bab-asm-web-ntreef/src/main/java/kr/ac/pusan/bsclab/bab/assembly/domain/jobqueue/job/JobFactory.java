package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job;

import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfiguration;

@Component
public class JobFactory {
	
	
	public <T extends JobConfiguration> Job<T> createJob(
			JobType jobType, T jobConfiguration) {
		
		return new Job<T>(jobType, jobConfiguration);
	}

}
