package kr.ac.pusan.bsclab.bab.assembly.ws.models;

import java.util.Date;

import kr.ac.pusan.bsclab.bab.assembly.ws.models.JobSubmission;

public class Response<T>  {
	public static final String STATUS_UNKNOWN 	= "UNKNOWN";
	public static final String STATUS_QUEUED 	= "QUEUED";
	public static final String STATUS_RUNNING 	= "RUNNING";
	public static final String STATUS_FINISHED 	= "FINISHED";
	
	private String id;
	private JobSubmission request;
	private T response;
	private String status = STATUS_UNKNOWN;
	private String resourceDataHash;
	
	public Response() {
		Date now = new Date();
		id = String.valueOf(now.getTime());
	}
	
	public Response(JobSubmission request, T response) {

		Date now = new Date();
		id = String.valueOf(now.getTime());
		this.request = request;
		this.response = response;
	}

	
	public Response(JobSubmission request, String resourceDataHash, T response) {

		Date now = new Date();
		id = String.valueOf(now.getTime());
		this.resourceDataHash = resourceDataHash;
		this.request = request;
		this.response = response;
	}

	public String getResourceDataHash() {
		return resourceDataHash;
	}

	public void setResourceDataHash(String resourceDataHash) {
		this.resourceDataHash = resourceDataHash;
	}

	public JobSubmission getRequest() {
		return this.request;
	}

	public void setRequest(JobSubmission request) {
		this.request = request;
	}

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
