package kr.ac.pusan.bsclab.bab.assembly.utils.hdfs;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class HdfsFileStatus {
	public String pathSuffix;
	public String type;
	public long length;
	public String owner;
	public String group;
	public String permission;
	public long accessTime;
	public long modificationTime;
	public long blockSize;
	public int replication;
	
	@JsonIgnore
	private FileStatus hdfsFileStatus;
	
	public FileStatus toFileStatus() {
		if (hdfsFileStatus == null) {
			hdfsFileStatus = new FileStatus(length, !type.equalsIgnoreCase("FILE"), replication, blockSize, modificationTime, accessTime, new FsPermission(permission), owner, group, new Path(pathSuffix));
		}
		return hdfsFileStatus;
	}
}
