package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_WS2 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.10063381e-01, -9.40920067e+00, -9.68909931e+00, 2.36456972e-02
            				, -7.77265644e+00, -1.70551527e+00, 2.51401520e+01, 7.10374117e-02
            				, 1.13453157e-01, -1.06264925e+00, -2.66080022e-01, -1.17223797e+01
            				, -7.20581532e+00, 3.60874486e+00, -8.07651162e-01, -4.26307917e-01
            				, -6.46114826e-01, -2.24226952e+01, -5.93666315e-01, -3.23607653e-01}, 
            			{7.98677742e-01, 1.42148530e+00, 8.03584516e-01, -1.02106142e+00
        					, 2.79898262e+00, 1.02101147e+00, -2.92304206e+00, 6.81613386e-01
        					, -1.22662961e+00, 8.12412620e-01, 4.01342511e-01, 1.71761155e+00
        					, -5.26220202e-01, 6.99652255e-01, -7.53200829e-01, 2.95687348e-01
        					, 4.41997796e-01, 2.66872597e+00, -9.01557624e-01, -1.29376996e+00},
            			{8.43434334e-01, -1.61082125e+00, 1.20001328e+00, -1.05714321e-01
    						, -3.98076028e-01, 7.33959436e-01, 5.10547876e-01, 4.71669465e-01
    						, -5.77789307e-01, -1.65693235e+00, -5.02592087e-01, -5.84632158e-02
    						, -2.75783968e+00, -2.15708923e+00, 6.27696738e-02, -7.72041157e-02
    						, -2.51134872e-01, -3.14375520e+00, -9.32721019e-01, 9.29689467e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.56466353, 1.69888914, 0.65989172, -0.19946133, 1.70026112, -0.85826063
            			, 0.99711269, -2.30592489, -0.50162834, -1.14485896, -2.41456246, 0.9517765
            			, 0.38005948, 1.26822829, -0.67392379, -0.56072348, -1.21916401, 0.4158113
            			, -0.03110923, -1.39619315};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.68636501,
            			0.51539689,
            			1.07032883,
            			0.46065301,
            			0.52414864,
            			-0.39937603,
            			-1.72733283,
            			0.11861741,
            			0.90037638,
            			-2.7172966,
            			0.95510906,
            			1.32511425,
            			0.4359197,
            			-0.1154382,
            			-0.18088372,
            			-1.1413188,
            			-0.12222505,
            			2.12487364,
            			0.02332837,
            			0.21476126};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {1.97503746};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 4.5;
	}

	@Override
	public double getMaxWidth() {
		
		return 1249.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 21505.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.36;
	}

	@Override
	public double getMinWidth() {
		
		return 71.0;
	}

	@Override
	public double getMinWeight() {
		
		return 384.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
