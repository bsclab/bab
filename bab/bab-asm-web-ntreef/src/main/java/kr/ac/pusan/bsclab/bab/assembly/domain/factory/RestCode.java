package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name="sf_rest_code")
public class RestCode {
	
	@Id
	@Column(name="UID_NO")
	String uidNo;

	@Column(name="MAJOR_ID")
	String majorId;

	@Column(name="MAJOR_NAM")
	String majorNam;

	@Column(name="MINOR_ID")
	String minorId;

	@Column(name="MINOR_NAM")
	String minorNam;

	@Column(name="MAINCAT_CD")
	String maincatCd;

	@Column(name="MAINCAT_NAM")
	String maincatNam;

	@Column(name="SUBCAT_CD")
	String subcatCd;

	@Column(name="SUBCAT_NAM")
	String subcatNam;

	@Column(name="USE_YN")
	String useYn;

	@Column(name="DEFAULT_YN")
	String defaultYn;

	@Column(name="SORT_NO")
	int sortNo;

	@Column(name="REF1_CLS")
	String ref1Cls;

	@Column(name="REF2_CLS")
	String ref2Cls;

	@Column(name="REF3_CLS")
	String ref3Cls;

	@Column(name="REF4_CLS")
	String ref4Cls;

	@Column(name="REF5_CLS")
	String ref5Cls;

	@Column(name="REF6_CLS")
	String ref6Cls;

	@Column(name="REF7_CLS")
	String ref7Cls;

	@Column(name="REF8_CLS")
	String ref8Cls;

	@Column(name="REF9_CLS")
	String ref9Cls;

	@Column(name="REF10_CLS")
	String ref10Cls;

	@Column(name="REF11_CLS")
	String ref11Cls;

	@Column(name="REF12_CLS")
	String ref12Cls;

	@Column(name="REF13_CLS")
	String ref13Cls;

	@Column(name="REF14_CLS")
	String ref14Cls;

	@Column(name="REF15_CLS")
	String ref15Cls;

	@Column(name="REF1_TXT")
	String ref1Txt;

	@Column(name="REF2_TXT")
	String ref2Txt;

	@Column(name="REF3_TXT")
	String ref3Txt;

	@Column(name="REF4_TXT")
	String ref4Txt;

	@Column(name="REF5_TXT")
	String ref5Txt;

	@Column(name="REF6_TXT")
	String ref6Txt;

	@Column(name="REF7_TXT")
	String ref7Txt;

	@Column(name="REF8_TXT")
	String ref8Txt;

	@Column(name="REF9_TXT")
	String ref9Txt;

	@Column(name="REF10_TXT")
	String ref10Txt;

	@Column(name="REF11_TXT")
	String ref11Txt;

	@Column(name="REF12_TXT")
	String ref12Txt;

	@Column(name="REF13_TXT")
	String ref13Txt;

	@Column(name="REF14_TXT")
	String ref14Txt;

	@Column(name="REF15_TXT")
	String ref15Txt;

	@Column(name="REMARK_REM")
	String remarkRem;

	@Column(name="BYMAT_YN")
	String bymatYn;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DT")
	Date startDt;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DT")
	Date endDt;

	@Column(name="SYSTEM_YN")
	String systemYn;

	@Temporal(TemporalType.DATE)
	@Column(name="CRT_DT")
	Date crtDt;

	@Column(name="CRTCHR_NO")
	String crtchrNo;

	@Temporal(TemporalType.DATE)
	@Column(name="UPD_DT")
	Date updDt;

	@Column(name="UPDCHR_NO")
	String updchrNo;

	@Column(name="ZON_CD")
	String zonCd;

}
