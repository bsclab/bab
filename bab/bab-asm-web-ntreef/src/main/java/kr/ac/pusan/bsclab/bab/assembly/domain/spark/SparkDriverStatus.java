package kr.ac.pusan.bsclab.bab.assembly.domain.spark;

public class SparkDriverStatus extends SparkRestResult {
	public String driverState;
	public String workerHostPort;
	public String workerId;
	public String message;
}
