package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CRB implements ANN_base {
	
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{0.27979282, -0.38681793, 1.04559469, 0.00372323, -1.80650198, -0.34910768
            				, 0.86263752, -0.39863545, 0.7877447, -1.29578245, 0.04033021, -0.99124783
            				, 0.21749273, 1.64208496, 1.54193878, -0.71314591, -0.1350112, 0.68232101
            				, -0.27463186, 0.48792344}, 
            			{-0.83918703, 0.16782972, 1.92916942, -0.19658458, 0.84280652, 1.6092931
        					, 1.46962762, -0.45755306, -0.11768226, 0.06784746, -0.81234509, 0.62410963
        					, -0.60163605, 1.00384426, 0.17848152, -0.97389245, -0.8268131, -1.71174419
        					, -1.33320677, 2.21736693},
            			{1.23461449, 0.85921431, -0.37892282, -0.6091755, -2.37004328, 0.12853624
            				, 0.02657513, -0.86029005, 0.21777709, 1.38974226, -0.59346545, 0.08379268
            				, -1.32988346, 0.61960441, -0.76260066, 0.5420959, 0.49722171, 0.00928862
            				, 1.2581526, 0.83506757}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.23034811, 0.12082323, 0.1095202, 0.07422847, -0.15627822, -0.19494483
            			, 0.30054596, 0.31253222, -0.92017275, 1.1262275, 1.32638037, 2.08510351
            			, 1.27322197, -0.02251166, -1.07356822, 1.76267195, 0.84904933, -1.22074914
            			, 1.49170864, 0.1091487};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {1.3076117,
            			-0.53537482,
            			-1.15407741,
            			-0.83436131,
            			0.00767181,
            			1.29881155,
            			-1.20387459,
            			0.15213497,
            			0.76490963,
            			1.08054471,
            			-0.60384899,
            			0.30672476,
            			0.53217185,
            			0.04244415,
            			-0.80550456,
            			0.26813504,
            			-0.14817333,
            			0.32194763,
            			-1.9550705
            			-0.23784837};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.50949109};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 1.77;
	}

	@Override
	public double getMaxWidth() {
		
		return 247.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 3440.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.41;
	}

	@Override
	public double getMinWidth() {
		
		return 211.0;
	}

	@Override
	public double getMinWeight() {
		
		return 2091.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
