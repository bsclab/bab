package kr.ac.pusan.bsclab.bab.assembly.rserver.exceptions;

/**
 * Class for handling Exception while using service of RStudio Server
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

public class RServeException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	/**
	 * @return error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 */
	public RServeException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	/**
	 * call super class
	 */
	public RServeException() {
		super();
	}

}
