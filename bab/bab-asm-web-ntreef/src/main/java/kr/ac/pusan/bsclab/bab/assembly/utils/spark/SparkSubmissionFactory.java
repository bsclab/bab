package kr.ac.pusan.bsclab.bab.assembly.utils.spark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SparkProperties;

@Component
public class SparkSubmissionFactory {
	
	@Autowired
	AppProperties ap;
	
	@Autowired
	HdfsProperties hp;
	
	@Autowired
	SparkProperties sp;
	
	
	public SparkSubmission createSparkSubmission(String jobId
			, String jobClass
			, String jobPath
			, String userId) {
		
		return new SparkSubmission(ap.getReportUri()
				, ap.getName()
				, ap.getVersion()
				, hp.getApiUri()
				, sp.getMasterUri()
				, sp.getVersion()
				, ap.getMainClass()
				, ap.getJarUri()
				, jobId, jobClass, jobPath, userId);
		
	}

}
