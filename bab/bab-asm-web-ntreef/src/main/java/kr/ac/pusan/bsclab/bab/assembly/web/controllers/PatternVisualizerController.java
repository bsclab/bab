package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class PatternVisualizerController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/patternvisualizer";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/taskmatrix/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getTaskMatrix(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
//			@PathVariable(value = "repositoryId") String repositoryId
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/taskmatrix");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);

		view.addObject("jsonData", jsonData);
		view.addObject("apiURI", apiManager.getAPIURI());


		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/associationrule/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getAssociationRule(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
//			@PathVariable(value = "repositoryId") String repositoryId,
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/associationrule");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
//		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/timegap/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getTimeGap(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
//			@PathVariable(value = "repositoryId") String repositoryId,
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/timegap");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
//		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/timegap2/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getTimeGap2(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/timegap2");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
//		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/timegap-filtered")
	public ModelAndView getTimeGapFiltered(
			// @PathVariable(value = "workspaceId") String workspaceId,
			// @PathVariable(value = "datasetId") String datasetId,
			// @PathVariable(value = "repositoryId") String repositoryId,
			HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/timegap-filtered");

		// add jsondata here
//		Map<String, String> jsonData = new HashMap<String, String>();
		// jsonData.put("workspaceId",
		// session.getAttribute("workspaceId").toString());
		// jsonData.put("datasetId", datasetId);
		// jsonData.put("repositoryId", repositoryId);
		//
		// view.addObject("jsonData", jsonData);

		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/socialnetwork/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getSocialNetwork(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("patternvisualizer/socialnetwork");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
//		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());


		return view;
	}
}
