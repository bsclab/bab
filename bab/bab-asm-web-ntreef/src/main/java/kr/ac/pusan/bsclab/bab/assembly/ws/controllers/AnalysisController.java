package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;
import kr.ac.pusan.bsclab.bab.assembly.utils.yarn.YarnSubmission;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.AssociationRuleMiner;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config.DottedChartAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.DottedChartModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.DeltaAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.DeltaAnalysisResult;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.HeatMapModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.TaskMatrixConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models.Ymkpi;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models.YmkpiParameter;

@Controller
public class AnalysisController extends AbstractServiceController {

	public static final String BASE_URL = BabWebService.BASE_URL + "/analysis";
	
	@Autowired
	RestRequest restRequest;
	
	public String get(String uri) {
		//ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
		return "";
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/yarn")
	public @ResponseBody String testYarn() {
		String response = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
//			// String uri = BabServer.CONFIG.SPARK_GATEWAY +
//			// "/v1/submissions/status/" + submission.submissionId;
//			String uri = appConf.getSpark().get("hiddenApiUri") + "/v1/submissions/status/" + submission.submissionId;
			//HttpHeaders headers = new HttpHeaders();
				//		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			//HttpEntity<String> entity = new HttpEntity<String>("", headers);
			//ResponseEntity<String> result = restRequest.exchange("", HttpMethod.GET, entity, String.class);
//			SparkDriverStatus status = mapper.readValue(result.getBody(), SparkDriverStatus.class);
			YarnSubmission s = new YarnSubmission();
			response = mapper.writeValueAsString(s);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
			
			
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/associationrule/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<AssociationRuleMiner> postAssociationRule(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, AssociationRuleMinerConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new AssociationRuleMinerConfiguration();
			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));
//			}
			
			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<AssociationRuleMiner> babResponse = this.submitJob("AnalysisAssociationRuleJob", "arans",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), AssociationRuleMiner.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/socialnetwork/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<SocialNetworkAnalysis> postSocialNetwork(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			SocialNetworkAnalysisConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new SocialNetworkAnalysisConfiguration();
			}
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<SocialNetworkAnalysis> babResponse = this.submitJob(
					"AnalysisSocialNetworkJob", "snans",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), SocialNetworkAnalysis.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/timegap/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<TimeGapAnalysisJobModel> postTimeGap(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, TimeGapAnalysisJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new TimeGapAnalysisJobConfiguration();
			}
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<TimeGapAnalysisJobModel> babResponse = this.submitJob("AnalysisTimeGapJob", "tgans",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), TimeGapAnalysisJobModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

/*	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/mtga/{workspaceId}/{datasetId}/{repositoryId}")
	public @ResponseBody Response<MTGAModel> postMtga(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, MTGAJobConfiguration config, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			if (config == null) {
				config = new MTGAJobConfiguration();
			}
			config.setRepositoryURI(webHdfs.getRepositoryPathOnly(workspaceId, datasetId, repositoryId));

			ObjectMapper mapper = new ObjectMapper();
			String configJson = mapper.writeValueAsString(config);
			Response<MTGAModel> babResponse = this.submitJob(appConf, "AnalysisMultidimensionalTimeGapJob", "mtgans",
					workspaceId, datasetId, repositoryId, configJson, MTGAModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/delta/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<DeltaAnalysisResult> postDelta(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			DeltaAnalysisJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
//			String resourceDataHash = repositoryId;
//			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
//					, dc.getTimestamp(edt));
			String[] ruParts = config.getRepositoryURI().split("/");
			String resourceDataHash = ruParts[ruParts.length - 1];
			
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			//String vdt = sdf.format(new Date());
			
//			if (config == null) {
//				config = new DeltaAnalysisJobConfiguration();
//			}
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash) + "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<DeltaAnalysisResult> babResponse = this.submitJob("AnalysisDeltaJob", "dlans",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), DeltaAnalysisResult.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/taskmatrix/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<HeatMapModel> postTaskMatrix(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,TaskMatrixConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new TaskMatrixConfiguration();
			}
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<HeatMapModel> babResponse = this.submitJob("AnalysisTaskMatrixJob", "tmans", workspaceId,
					datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), HeatMapModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/dottedchart/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<DottedChartModel> postDottedChart(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			DottedChartAnalysisJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new DottedChartAnalysisJobConfiguration();
			}
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<DottedChartModel> babResponse = this.submitJob("AnalysisDottedChartJob", "dcans",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), DottedChartModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/ymkpi/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<Ymkpi> postYmkpi(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			YmkpiParameter param,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (param == null) {
				param = new YmkpiParameter();
			}
//			param.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));

			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(param));
			param.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(param);
			Response<Ymkpi> babResponse = this.submitJob("YmKPIJob", "ymkpi",
					workspaceId, datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), Ymkpi.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
