package kr.ac.pusan.bsclab.bab.assembly.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-rserver.properties")
@ConfigurationProperties(prefix="rserver")
public class RserverProperties {
	
	String ipRserver;
	String portRserver;
	String sparkHome;
	String hadoopDir;
	String yarnDir;
	String hdfsUrl;
	String sparkVersion;
	String scriptPath;
	String outputPath;
	public String getIpRserver() {
		return ipRserver;
	}
	public void setIpRserver(String ipRserver) {
		this.ipRserver = ipRserver;
	}
	public String getPortRserver() {
		return portRserver;
	}
	public void setPortRserver(String portRserver) {
		this.portRserver = portRserver;
	}
	public String getSparkHome() {
		return sparkHome;
	}
	public void setSparkHome(String sparkHome) {
		this.sparkHome = sparkHome;
	}
	public String getHadoopDir() {
		return hadoopDir;
	}
	public void setHadoopDir(String hadoopDir) {
		this.hadoopDir = hadoopDir;
	}
	public String getYarnDir() {
		return yarnDir;
	}
	public void setYarnDir(String yarnDir) {
		this.yarnDir = yarnDir;
	}
	public String getHdfsUrl() {
		return hdfsUrl;
	}
	public void setHdfsUrl(String hdfsUrl) {
		this.hdfsUrl = hdfsUrl;
	}
	public String getSparkVersion() {
		return sparkVersion;
	}
	public void setSparkVersion(String sparkVersion) {
		this.sparkVersion = sparkVersion;
	}
	public String getScriptPath() {
		return scriptPath;
	}
	public void setScriptPath(String scriptPath) {
		this.scriptPath = scriptPath;
	}
	public String getOutputPath() {
		return outputPath;
	}
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}
	
}
