package kr.ac.pusan.bsclab.bab.assembly.ws.tests;

import java.util.concurrent.CountDownLatch;

public class Receiver {

	private CountDownLatch latch = new CountDownLatch(1);

	public void receiveMessage(String message) {
		latch.countDown();
	}

	public CountDownLatch getLatch() {
		return latch;
	}

}
