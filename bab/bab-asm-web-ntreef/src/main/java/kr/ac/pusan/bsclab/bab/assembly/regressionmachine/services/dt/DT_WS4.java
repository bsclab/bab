package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_WS4 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10169")) return 60;
if(p.equals("AN10240")) return 60;
if(p.equals("AN10244")) return 50;
if(p.equals("AN10245"))
 if(we <= 9091)
  if(we <= 8239)
   if(we <= 8137) return 60;
   if(we > 8137) return 50;
  if(we > 8239) return 60;
 if(we > 9091) return 70;
if(p.equals("AN10252")) return 60;
if(p.equals("AN10258")) return 60;
if(p.equals("PP11539")) return 60;
if(p.equals("PP11880")) return 50;
if(p.equals("PP12231")) return 50;
if(p.equals("PP12483")) return 50;
if(p.equals("PP12536")) return 45;
if(p.equals("PP12563")) return 50;
if(p.equals("PP21292")) return 50;
if(p.equals("PP21306")) return 60;
if(p.equals("PP21317")) return 60;
if(p.equals("PP21320"))
 if(we <= 4660)
  if(t <= 2.8)
   if(we <= 4526) return 60;
   if(we > 4526) return 40;
  if(t > 2.8)
   if(w <= 412) return 40;
   if(w > 412) return 50;
 if(we > 4660)
  if(we <= 13550) return 50;
  if(we > 13550)
   if(we <= 13850) return 45;
   if(we > 13850) return 60;
if(p.equals("PP21323")) return 50;
if(p.equals("PP21328"))
 if(w <= 494)
  if(we <= 7657)
   if(t <= 5.2) return 55;
   if(t > 5.2)
    if(we <= 7209) return 40;
    if(we > 7209)
     if(we <= 7389) return 50;
     if(we > 7389) return 45;
  if(we > 7657)
   if(we <= 8149) return 60;
   if(we > 8149)
    if(we <= 8268) return 55;
    if(we > 8268) return 60;
 if(w > 494)
  if(w <= 622)
   if(t <= 5.5)
    if(t <= 4.7)
     if(w <= 568) return 50;
     if(w > 568)
      if(we <= 12129) return 45;
      if(we > 12129) return 50;
    if(t > 4.7)
     if(we <= 11565) return 50;
     if(we > 11565)
      if(we <= 11815) return 60;
      if(we > 11815) return 50;
   if(t > 5.5)
    if(w <= 521) return 45;
    if(w > 521) return 55;
  if(w > 622) return 50;
if(p.equals("PP21330")) return 60;
if(p.equals("PP21331")) return 50;
if(p.equals("PP21332"))
 if(we <= 10842) return 50;
 if(we > 10842) return 45;
if(p.equals("PP21334"))
 if(we <= 11851) return 60;
 if(we > 11851) return 50;
if(p.equals("PP21336"))
 if(t <= 6.1)
  if(t <= 4.7)
   if(w <= 522)
    if(w <= 457)
     if(w <= 402) return 50;
     if(w > 402)
      if(t <= 4.2) return 60;
      if(t > 4.2) return 50;
    if(w > 457) return 50;
   if(w > 522)
    if(w <= 553)
     if(we <= 10235) return 45;
     if(we > 10235) return 50;
    if(w > 553)
     if(w <= 555) return 45;
     if(w > 555)
      if(w <= 633)
       if(we <= 12865) return 50;
       if(we > 12865) return 45;
      if(w > 633) return 45;
  if(t > 4.7)
   if(w <= 543)
    if(w <= 518)
     if(t <= 5.7)
      if(t <= 5)
       if(we <= 9937)
        if(w <= 456) return 50;
        if(w > 456) return 55;
       if(we > 9937)
        if(w <= 483) return 45;
        if(w > 483) return 50;
      if(t > 5)
       if(t <= 5.45) return 60;
       if(t > 5.45)
        if(t <= 5.6) return 50;
        if(t > 5.6)
         if(we <= 7945)
          if(we <= 7636) return 60;
          if(we > 7636) return 50;
         if(we > 7945)
          if(w <= 365) return 45;
          if(w > 365) return 60;
     if(t > 5.7)
      if(we <= 9105)
       if(we <= 6295) return 50;
       if(we > 6295)
        if(w <= 390) return 55;
        if(w > 390) return 45;
      if(we > 9105) return 50;
    if(w > 518)
     if(t <= 5.2) return 45;
     if(t > 5.2) return 60;
   if(w > 543)
    if(we <= 11247)
     if(t <= 5.7)
      if(w <= 553)
       if(w <= 551) return 50;
       if(w > 551) return 60;
      if(w > 553) return 50;
     if(t > 5.7) return 60;
    if(we > 11247)
     if(t <= 5.45) return 50;
     if(t > 5.45)
      if(w <= 581)
       if(w <= 545) return 55;
       if(w > 545) return 50;
      if(w > 581) return 55;
 if(t > 6.1)
  if(t <= 8)
   if(we <= 7395) return 50;
   if(we > 7395)
    if(we <= 8155) return 60;
    if(we > 8155)
     if(we <= 10102) return 50;
     if(we > 10102) return 60;
  if(t > 8)
   if(w <= 575) return 80;
   if(w > 575)
    if(w <= 620)
     if(w <= 588) return 60;
     if(w > 588)
      if(we <= 12146) return 50;
      if(we > 12146)
       if(w <= 612) return 60;
       if(w > 612) return 50;
    if(w > 620) return 80;
if(p.equals("PP21341"))
 if(we <= 11990) return 50;
 if(we > 11990) return 60;
if(p.equals("PP21346")) return 50;
if(p.equals("PP21364"))
 if(w <= 467) return 40;
 if(w > 467) return 60;
if(p.equals("PP21368")) return 60;
if(p.equals("PP21389"))
 if(t <= 6.3)
  if(w <= 564)
   if(w <= 527) return 50;
   if(w > 527) return 45;
  if(w > 564)
   if(we <= 12705)
    if(w <= 588)
     if(we <= 10921) return 60;
     if(we > 10921) return 50;
    if(w > 588)
     if(we <= 12456)
      if(we <= 10852) return 50;
      if(we > 10852)
       if(w <= 591)
        if(we <= 11549) return 40;
        if(we > 11549) return 60;
       if(w > 591)
        if(we <= 12359)
         if(we <= 12020)
          if(w <= 594) return 50;
          if(w > 594) return 40;
         if(we > 12020) return 50;
        if(we > 12359) return 40;
     if(we > 12456)
      if(we <= 12498) return 60;
      if(we > 12498) return 50;
   if(we > 12705) return 45;
 if(t > 6.3) return 50;
if(p.equals("PP21393"))
 if(we <= 6614)
  if(t <= 5.45)
   if(w <= 354) return 60;
   if(w > 354) return 45;
  if(t > 5.45)
   if(t <= 6.1) return 40;
   if(t > 6.1)
    if(we <= 6239) return 60;
    if(we > 6239) return 50;
 if(we > 6614)
  if(we <= 7567) return 50;
  if(we > 7567)
   if(w <= 506)
    if(w <= 499)
     if(we <= 9703)
      if(w <= 462)
       if(we <= 8256)
        if(t <= 6.1)
         if(w <= 418)
          if(we <= 7666)
           if(t <= 4.35) return 50;
           if(t > 4.35) return 60;
          if(we > 7666)
           if(we <= 8192) return 50;
           if(we > 8192) return 60;
         if(w > 418) return 60;
        if(t > 6.1) return 60;
       if(we > 8256)
        if(w <= 413) return 50;
        if(w > 413)
         if(we <= 9272)
          if(w <= 419) return 55;
          if(w > 419) return 50;
         if(we > 9272) return 55;
      if(w > 462)
       if(we <= 9575)
        if(w <= 491) return 50;
        if(w > 491) return 45;
       if(we > 9575)
        if(we <= 9639) return 60;
        if(we > 9639) return 50;
     if(we > 9703)
      if(t <= 4.7)
       if(we <= 10022) return 55;
       if(we > 10022)
        if(we <= 10213) return 45;
        if(we > 10213) return 50;
      if(t > 4.7)
       if(we <= 10186)
        if(we <= 9830)
         if(w <= 495.5) return 40;
         if(w > 495.5) return 50;
        if(we > 9830) return 50;
       if(we > 10186) return 55;
    if(w > 499)
     if(we <= 9471) return 60;
     if(we > 9471) return 55;
   if(w > 506) return 50;
if(p.equals("PP21394"))
 if(we <= 6762)
  if(we <= 6043) return 40;
  if(we > 6043)
   if(w <= 342) return 60;
   if(w > 342) return 50;
 if(we > 6762) return 50;
if(p.equals("PP21398"))
 if(w <= 637)
  if(t <= 2.7)
   if(t <= 2.06)
    if(we <= 7948) return 40;
    if(we > 7948) return 50;
   if(t > 2.06)
    if(w <= 442)
     if(we <= 7546) return 50;
     if(we > 7546) return 60;
    if(w > 442) return 50;
  if(t > 2.7)
   if(t <= 2.8)
    if(w <= 617) return 60;
    if(w > 617)
     if(we <= 12038)
      if(we <= 11611) return 60;
      if(we > 11611) return 55;
     if(we > 12038) return 50;
   if(t > 2.8)
    if(t <= 3.1)
     if(we <= 12266) return 50;
     if(we > 12266) return 55;
    if(t > 3.1)
     if(w <= 511) return 60;
     if(w > 511) return 50;
 if(w > 637)
  if(t <= 3)
   if(we <= 11980)
    if(w <= 639)
     if(t <= 2.9) return 60;
     if(t > 2.9) return 50;
    if(w > 639) return 50;
   if(we > 11980)
    if(we <= 12218)
     if(we <= 12075) return 45;
     if(we > 12075)
      if(we <= 12170) return 60;
      if(we > 12170) return 45;
    if(we > 12218) return 60;
  if(t > 3) return 50;
if(p.equals("PP21404")) return 50;
if(p.equals("PP21408"))
 if(t <= 7) return 60;
 if(t > 7)
  if(we <= 10634) return 40;
  if(we > 10634) return 50;
if(p.equals("PP21419")) return 50;
if(p.equals("PP21420"))
 if(t <= 5.45)
  if(we <= 22093)
   if(we <= 21916) return 40;
   if(we > 21916) return 50;
  if(we > 22093) return 45;
 if(t > 5.45) return 45;
if(p.equals("PP21423"))
 if(we <= 7153)
  if(t <= 2.5)
   if(w <= 466) return 50;
   if(w > 466)
    if(w <= 619) return 45;
    if(w > 619) return 50;
  if(t > 2.5) return 45;
 if(we > 7153)
  if(w <= 575) return 50;
  if(w > 575)
   if(w <= 579)
    if(we <= 7638.5) return 50;
    if(we > 7638.5)
     if(w <= 578.5)
      if(t <= 4.25) return 60;
      if(t > 4.25) return 50;
     if(w > 578.5) return 50;
   if(w > 579)
    if(we <= 8518) return 60;
    if(we > 8518) return 50;
if(p.equals("PP21429")) return 50;
if(p.equals("PP21432"))
 if(t <= 3.8) return 50;
 if(t > 3.8) return 45;
if(p.equals("PP21456"))
 if(t <= 2.7)
  if(t <= 2.06)
   if(w <= 625) return 50;
   if(w > 625) return 45;
  if(t > 2.06) return 50;
 if(t > 2.7)
  if(t <= 3.6) return 55;
  if(t > 3.6) return 60;
if(p.equals("PP21469")) return 40;
if(p.equals("PP21473"))
 if(t <= 6)
  if(t <= 5.99)
   if(t <= 4.2)
    if(w <= 410) return 60;
    if(w > 410) return 55;
   if(t > 4.2) return 50;
  if(t > 5.99) return 45;
 if(t > 6)
  if(w <= 438) return 50;
  if(w > 438)
   if(t <= 6.5)
    if(w <= 591)
     if(we <= 8662) return 60;
     if(we > 8662)
      if(w <= 475) return 50;
      if(w > 475)
       if(w <= 554) return 40;
       if(w > 554) return 50;
    if(w > 591) return 60;
   if(t > 6.5) return 60;
if(p.equals("PP21475"))
 if(we <= 8494) return 45;
 if(we > 8494) return 50;
if(p.equals("PP21476"))
 if(t <= 5.7)
  if(t <= 4.2)
   if(we <= 12175)
    if(we <= 12016)
     if(w <= 328) return 50;
     if(w > 328)
      if(we <= 11637)
       if(we <= 10349) return 45;
       if(we > 10349)
        if(w <= 611) return 50;
        if(w > 611)
         if(we <= 11058) return 50;
         if(we > 11058) return 30;
      if(we > 11637) return 45;
    if(we > 12016) return 60;
   if(we > 12175) return 50;
  if(t > 4.2) return 50;
 if(t > 5.7)
  if(we <= 9278)
   if(w <= 468)
    if(we <= 8360)
     if(w <= 402) return 50;
     if(w > 402) return 60;
    if(we > 8360) return 50;
   if(w > 468)
    if(t <= 6.1)
     if(w <= 600) return 45;
     if(w > 600) return 50;
    if(t > 6.1) return 45;
  if(we > 9278) return 50;
if(p.equals("PP21487"))
 if(t <= 2.8) return 60;
 if(t > 2.8)
  if(t <= 3.3) return 50;
  if(t > 3.3) return 45;
if(p.equals("PP21504")) return 60;
if(p.equals("PP21516")) return 50;
if(p.equals("PP21526"))
 if(w <= 605)
  if(w <= 519)
   if(t <= 2.3) return 60;
   if(t > 2.3) return 50;
  if(w > 519)
   if(t <= 2.7) return 45;
   if(t > 2.7) return 50;
 if(w > 605) return 50;
if(p.equals("PP21531")) return 50;
if(p.equals("PP21533"))
 if(w <= 417) return 50;
 if(w > 417) return 60;
if(p.equals("PP21557"))
 if(w <= 368) return 60;
 if(w > 368) return 50;
if(p.equals("PP21577"))
 if(t <= 3.6)
  if(w <= 414)
   if(we <= 5014) return 40;
   if(we > 5014) return 60;
  if(w > 414) return 50;
 if(t > 3.6)
  if(t <= 3.8) return 45;
  if(t > 3.8)
   if(we <= 7608) return 35;
   if(we > 7608) return 60;
if(p.equals("PP21580")) return 50;
if(p.equals("PP21592")) return 50;
if(p.equals("PP21599")) return 60;
if(p.equals("PP21601"))
 if(t <= 5.45) return 45;
 if(t > 5.45) return 50;
if(p.equals("PP21606")) return 50;
if(p.equals("PP21632")) return 50;
if(p.equals("PP21644")) return 50;
if(p.equals("PP21647")) return 60;
if(p.equals("PP21671")) return 50;
if(p.equals("PP21677")) return 50;
if(p.equals("PP21690")) return 60;
if(p.equals("PP21694")) return 50;
if(p.equals("PP21726")) return 60;
if(p.equals("PP21730"))
 if(w <= 738)
  if(w <= 703) return 50;
  if(w > 703)
   if(we <= 12506) return 35;
   if(we > 12506) return 50;
 if(w > 738) return 60;
if(p.equals("PP21752")) return 40;
if(p.equals("PP21753")) return 60;
if(p.equals("PP21762")) return 50;
if(p.equals("PP21769")) return 60;
if(p.equals("PP21773")) return 40;
if(p.equals("PP21779")) return 50;
if(p.equals("PP21782")) return 50;
if(p.equals("PP21789")) return 50;
if(p.equals("PP21790")) return 50;
if(p.equals("PP21792")) return 50;
if(p.equals("PP21794"))
 if(we <= 5405) return 40;
 if(we > 5405) return 50;
if(p.equals("PP21798")) return 50;
if(p.equals("PP21802")) return 50;
if(p.equals("PP21804")) return 50;
if(p.equals("PP21805"))
 if(t <= 3.7) return 50;
 if(t > 3.7) return 45;
if(p.equals("PP21809")) return 40;
if(p.equals("PP21811")) return 60;
if(p.equals("PP21814")) return 50;
if(p.equals("PP21855")) return 40;
if(p.equals("PP21890")) return 40;
if(p.equals("PP21903"))
 if(t <= 6.1) return 50;
 if(t > 6.1) return 55;
if(p.equals("PP21908")) return 45;
if(p.equals("PP22001"))
 if(t <= 3.1)
  if(t <= 2.5) return 40;
  if(t > 2.5) return 60;
 if(t > 3.1)
  if(we <= 15739)
   if(t <= 3.7) return 35;
   if(t > 3.7)
    if(we <= 14710) return 30;
    if(we > 14710) return 40;
  if(we > 15739) return 40;
if(p.equals("PP22002"))
 if(we <= 14550)
  if(we <= 14300)
   if(we <= 14040)
    if(t <= 4.5)
     if(t <= 3.7)
      if(we <= 13629) return 40;
      if(we > 13629) return 35;
     if(t > 3.7) return 30;
    if(t > 4.5) return 40;
   if(we > 14040)
    if(t <= 4.5) return 35;
    if(t > 4.5)
     if(we <= 14120) return 35;
     if(we > 14120)
      if(we <= 14169) return 30;
      if(we > 14169) return 35;
  if(we > 14300)
   if(we <= 14350) return 45;
   if(we > 14350) return 30;
 if(we > 14550)
  if(we <= 14739)
   if(we <= 14629)
    if(we <= 14580) return 45;
    if(we > 14580) return 30;
   if(we > 14629) return 40;
  if(we > 14739) return 35;
if(p.equals("PP22003"))
 if(t <= 3.7)
  if(we <= 13999) return 35;
  if(we > 13999)
   if(we <= 14809) return 40;
   if(we > 14809) return 30;
 if(t > 3.7) return 30;
if(p.equals("PP22005")) return 45;
if(p.equals("PP22006"))
 if(we <= 14470)
  if(we <= 14040)
   if(we <= 12414) return 40;
   if(we > 12414)
    if(we <= 12797) return 30;
    if(we > 12797) return 40;
  if(we > 14040)
   if(we <= 14300)
    if(we <= 14169)
     if(we <= 14088) return 35;
     if(we > 14088) return 30;
    if(we > 14169) return 35;
   if(we > 14300) return 30;
 if(we > 14470)
  if(we <= 14660)
   if(we <= 14605) return 30;
   if(we > 14605) return 45;
  if(we > 14660)
   if(we <= 14729) return 40;
   if(we > 14729) return 35;
if(p.equals("PP22007"))
 if(we <= 14680)
  if(we <= 14605)
   if(we <= 14169) return 40;
   if(we > 14169) return 30;
  if(we > 14605) return 40;
 if(we > 14680) return 45;
if(p.equals("PP22016")) return 55;
if(p.equals("PP22018"))
 if(t <= 4.2)
  if(w <= 492) return 40;
  if(w > 492)
   if(w <= 507.5) return 50;
   if(w > 507.5) return 55;
 if(t > 4.2)
  if(w <= 476) return 60;
  if(w > 476)
   if(t <= 4.7)
    if(w <= 577) return 45;
    if(w > 577) return 50;
   if(t > 4.7)
    if(we <= 11175)
     if(we <= 8002) return 45;
     if(we > 8002) return 50;
    if(we > 11175) return 45;
if(p.equals("PP22037")) return 30;
if(p.equals("PP22038")) return 40;
if(p.equals("PP22041")) return 45;
if(p.equals("PP22049")) return 50;
if(p.equals("PP22059")) return 60;
if(p.equals("PP22060"))
 if(w <= 421) return 60;
 if(w > 421)
  if(we <= 9524)
   if(w <= 436) return 50;
   if(w > 436) return 45;
  if(we > 9524) return 50;
if(p.equals("PP22069"))
 if(t <= 2.35) return 50;
 if(t > 2.35) return 45;
if(p.equals("PP22071"))
 if(we <= 15640)
  if(we <= 15179) return 40;
  if(we > 15179) return 45;
 if(we > 15640) return 40;
if(p.equals("PP22072")) return 42.5;
if(p.equals("PP22074")) return 40;
if(p.equals("PP22075"))
 if(we <= 14749) return 30;
 if(we > 14749) return 35;
if(p.equals("PP22076")) return 50;
if(p.equals("PP22079")) return 40;
if(p.equals("PP22085"))
 if(we <= 10258)
  if(t <= 2.03) return 50;
  if(t > 2.03) return 45;
 if(we > 10258)
  if(t <= 4.2) return 50;
  if(t > 4.2) return 60;
if(p.equals("PP22086")) return 40;
if(p.equals("PP22096")) return 50;
if(p.equals("PP22102")) return 50;
if(p.equals("PP22107")) return 60;
if(p.equals("PP22135")) return 40;
if(p.equals("PP22136")) return 40;
if(p.equals("PP22137")) return 40;
if(p.equals("PP22157"))
 if(t <= 2.3) return 50;
 if(t > 2.3)
  if(t <= 2.8)
   if(we <= 4422) return 50;
   if(we > 4422) return 45;
  if(t > 2.8) return 45;
if(p.equals("PP22158")) return 60;
if(p.equals("PP22160")) return 50;
if(p.equals("PP22171"))
 if(t <= 2.6)
  if(t <= 2.3) return 50;
  if(t > 2.3)
   if(w <= 510) return 45;
   if(w > 510) return 50;
 if(t > 2.6) return 50;
if(p.equals("PP22179")) return 60;
if(p.equals("PP22180")) return 60;
if(p.equals("PP22181")) return 60;
if(p.equals("PP22184")) return 50;
if(p.equals("PP22201")) return 40;
if(p.equals("PP22215")) return 40;
if(p.equals("PP22233")) return 50;
if(p.equals("PP22234")) return 50;
if(p.equals("PP22240")) return 30;
if(p.equals("PP22243")) return 40;
if(p.equals("PP22261")) return 50;
if(p.equals("PP22267")) return 40;
if(p.equals("PP22270")) return 50;
if(p.equals("PP22272")) return 50;
if(p.equals("PP22274")) return 60;
if(p.equals("PP22276")) return 35;
if(p.equals("PP22277")) return 35;
if(p.equals("PP22279")) return 60;
if(p.equals("PP22282")) return 60;
if(p.equals("PP22285")) return 40;
if(p.equals("PP22288"))
 if(w <= 506) return 50;
 if(w > 506)
  if(w <= 622) return 40;
  if(w > 622) return 50;
if(p.equals("PP22289")) return 50;
if(p.equals("PP22290"))
 if(we <= 12102) return 45;
 if(we > 12102) return 60;
if(p.equals("PP22304"))
 if(w <= 515) return 60;
 if(w > 515)
  if(t <= 6.3) return 50;
  if(t > 6.3) return 55;
if(p.equals("PP22305"))
 if(we <= 10803) return 40;
 if(we > 10803) return 50;
if(p.equals("PP22310")) return 50;
if(p.equals("PP22315")) return 50;
if(p.equals("PP22318")) return 50;
if(p.equals("PP22324")) return 50;
if(p.equals("PP22343")) return 50;
if(p.equals("PP22345")) return 45;
if(p.equals("PP22354")) return 40;
if(p.equals("PP22355"))
 if(we <= 15760) return 45;
 if(we > 15760) return 40;
if(p.equals("PP22361")) return 40;
if(p.equals("PP22366")) return 50;
if(p.equals("PP22386")) return 60;
if(p.equals("PP22387")) return 50;
if(p.equals("PP22389")) return 60;
if(p.equals("PP22390")) return 50;
if(p.equals("PP22391")) return 50;
if(p.equals("PP22395")) return 50;
if(p.equals("PP22411")) return 50;
if(p.equals("PP22412")) return 50;
if(p.equals("PP22413")) return 50;
if(p.equals("PP22415"))
 if(we <= 4570) return 45;
 if(we > 4570) return 60;
if(p.equals("PP22419")) return 40;
if(p.equals("PP22420")) return 45;
if(p.equals("PP22421")) return 45;
if(p.equals("PP22422")) return 60;
if(p.equals("PP22431")) return 50;
if(p.equals("PP22438")) return 50;
if(p.equals("PP22447")) return 50;
if(p.equals("PP22456")) return 55;
if(p.equals("PP22457")) return 55;
if(p.equals("PP22460")) return 50;
if(p.equals("PP22461")) return 50;
if(p.equals("PP22468")) return 45;
if(p.equals("PP22469")) return 45;
if(p.equals("PP22470")) return 45;
if(p.equals("PP22478")) return 50;
if(p.equals("PP22479")) return 50;
if(p.equals("PP22480")) return 60;
if(p.equals("PP22486")) return 50;
if(p.equals("PP22490")) return 50;
if(p.equals("PP22496")) return 50;
if(p.equals("PP22522")) return 50;
if(p.equals("PP22539"))
 if(we <= 11694) return 50;
 if(we > 11694) return 45;
if(p.equals("PP22553")) return 45;
if(p.equals("PP22556")) return 60;
if(p.equals("PP22561")) return 50;
if(p.equals("PP22566"))
 if(we <= 2643) return 50;
 if(we > 2643) return 45;
return 45.0;
}
}
