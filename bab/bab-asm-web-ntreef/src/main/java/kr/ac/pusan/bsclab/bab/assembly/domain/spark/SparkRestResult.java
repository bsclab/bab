package kr.ac.pusan.bsclab.bab.assembly.domain.spark;

public class SparkRestResult {
    public String action;
    public String serverSparkVersion;
    public String submissionId;
    public boolean success;
	public String getAction() {
		return action;
	}
	public String getServerSparkVersion() {
		return serverSparkVersion;
	}
	public String getSubmissionId() {
		return submissionId;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public void setServerSparkVersion(String serverSparkVersion) {
		this.serverSparkVersion = serverSparkVersion;
	}
	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
    
    
    
}
