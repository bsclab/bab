package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_WS4 implements ANN_base {
	
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{2.58727479, 0.29858178, 0.68180156, 2.05912971, 3.79758644, 2.37720847
            				, -0.47024438, -1.0741415, 0.53615189, -0.91415519, 1.45542848, 0.35790312
            				, 1.08030236, -6.46592474, 0.98647034, -0.52476907, -0.18516792, -1.84387064
            				, 1.13333595, -0.05331126}, 
            			{-0.6042667, 1.49482453, -0.12515607, -1.27706504, -1.52498841, -1.882635
        					, -0.04088742, 1.09241402, -1.83059895, 0.12962864, 1.41415238, 0.27241287
        					, -2.03594422, 6.68038797, 1.21263719, 0.73518825, -0.43495107, -1.36914015
        					, -0.25018722, -1.50546527},
            			{1.11102951, -0.80965781, -1.22803116, -0.21328425, -1.22327983, 0.59149951
    						, -0.72708523, -0.11036703, -0.2646277, -2.52943635, 1.08341193, 0.91472936
    						, 0.51551539, -0.0086266, 0.79821283, 2.37823343, -0.19116896, -0.87192082
    						, 0.91197467, -0.90035748}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.12897222, -0.44578505, -0.48966253, 0.32621911, 1.64931679, 0.66418117
            			, -2.12700343, -0.58712071, -0.88885945, -0.24672897, -1.87237871, -1.0269711
            			, -1.3555944, 1.63157201, 0.92592025, -0.03188043, -0.7210229, -0.38325194
            			, -1.50920033, -0.59027618};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.37204471,
            			0.1857198,
            			0.47707322,
            			0.73453707,
            			1.21531773,
            			1.82084656,
            			0.73585624,
            			0.58048791,
            			0.63225657,
            			1.37991786,
            			-0.44587797,
            			0.42411032,
            			-0.67759001,
            			-1.20877743,
            			0.69464719,
            			0.03297515,
            			0.57999122,
            			-0.04130287,
            			0.54172796,
            			-0.9638142};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {1.93795609};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 11.0;
	}

	@Override
	public double getMaxWidth() {
		
		return 1285.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 24249.0;
	}

	@Override
	public double getMinThick() {
		
		return 1.61;
	}

	@Override
	public double getMinWidth() {
		
		return 203.0;
	}

	@Override
	public double getMinWeight() {
		
		return 1168.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
