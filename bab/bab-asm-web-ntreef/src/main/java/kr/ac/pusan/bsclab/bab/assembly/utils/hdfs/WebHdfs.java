package kr.ac.pusan.bsclab.bab.assembly.utils.hdfs;

import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;

@Component
public class WebHdfs {
	
	private static final Logger logger
	= LoggerFactory.getLogger(WebHdfs.class);
	
//	public static final String PATH_JOBS = "/jobs";
//	public static final String PATH_TEMP = "/temp";
//	public static final String PATH_WORKSPACES = "/workspaces";


	@Autowired
	private HdfsProperties hp;
	
	@Autowired
	RestRequest restRequest;

	public String getRepositoryPath(String workspaceId, String datasetId, String repositoryId) {
		return hp.getUserHome() + "/" + workspaceId
				+ "/" + datasetId
				+ "/" + repositoryId;
	}

	public String getRepositoryPath(String workspaceId, String datasetId, String repositoryId, String filterId) {
		return hp.getUserHome() + "/" + workspaceId
				+ "/" + datasetId
				+ "/" + repositoryId
				+ "/" + filterId;
	}
	
	public String getFilePath(String workspaceId, String datasetId, String repositoryId
			, String jobConfigurationHash, String jobExtenstion) {
		return hp.getUserHome() + "/" + workspaceId
				+ "/" + datasetId
				+ "/" + repositoryId
				+ "/" + jobConfigurationHash + "." + jobExtenstion;
	}

//	public String getPathOnly(String hdfsPath) {
//		return getPathOnly(hdfsPath, PATH_WORKSPACES);
//	}
//
//	public String getPathOnly(String hdfsPath, String type) {
//		String result = hp.getHome();
//
//		if (type.equals(PATH_JOBS) || type.equals(PATH_WORKSPACES) || type.equals(PATH_TEMP)) {
//			return result + type + hdfsPath;
//		}
//		return null;
//	}

//	public String getPath(String hdfsPath) {
//		return getPath(hdfsPath, PATH_WORKSPACES);
//	}

//	public String getPath(String hdfsPath, String type) {
//		String result = getPathOnly(hdfsPath, type);
//		if (result != null)
//			return hp.getApiUri() + result;
////			return hdfsUri + result;
//		return null;
//	}

	public String buildUri(String hdfsPath, String operation) {
//		return hdfsUri + hdfsPath + authParams + "&op=" + operation;
		return hp.getHttpUri() + hdfsPath + hp.getHdfsAuth() + "&op=" + operation;

	}

	
	public ResponseEntity<String> copyFromLocal(String serverPath, String hdfsPath) {
		try {
			String uri = buildUri(hdfsPath, "CREATE");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.PUT, entity, String.class);
			String saveUri = result.getHeaders().get("Location").get(0);
			HttpEntity<Resource> entityFile = new HttpEntity<Resource>(new FileSystemResource(serverPath), headers);
			result = restRequest.exchange(saveUri, HttpMethod.PUT, entityFile, String.class);
			
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public ResponseEntity<String> saveAsTextFile(String hdfsPath, String contents) {
		try {
			logger.debug("save file in hdfs={}", contents);
			String uri = buildUri(hdfsPath, "CREATE");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.PUT, entity, String.class);
			String saveUri = result.getHeaders().get("Location").get(0);
			HttpEntity<String> entityFile = new HttpEntity<String>(contents, headers);
			result = restRequest.exchange(saveUri, HttpMethod.PUT, entityFile, String.class);
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public boolean isExists(String hdfsPath) {
		try {
			String uri = buildUri(hdfsPath, "GETFILESTATUS");
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK && result.getBody().indexOf("FileStatus") > -1) {
				return true;
			}
		} catch (Exception ex) {
			return false;
//			ex.printStackTrace();
		}
		return false;
	}

	public List<FileStatus> listStatus(String hdfsPath) {
		try {
			String uri = buildUri(hdfsPath, "LISTSTATUS");
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				HdfsListStatuses hdfsFileStatus = mapper.readValue(result.getBody(), HdfsListStatuses.class);
				return hdfsFileStatus.FileStatuses.toFileStatus();
			}
		} catch (Exception ex) {
			return null;
		}
		return null;
	}
	
	public List<FileStatus> delete(String hdfsPath) {
		try {
			String uri = buildUri(hdfsPath, "DELETE");
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.DELETE, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				HdfsListStatuses hdfsFileStatus = mapper.readValue(result.getBody(), HdfsListStatuses.class);
				return hdfsFileStatus.FileStatuses.toFileStatus();
			}
		} catch (Exception ex) {
			return null;
		}
		return null;
	}

	public String openAsTextFile(String hdfsPath) {
		try {
			String uri = buildUri(hdfsPath, "OPEN");
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				return result.getBody();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
