package kr.ac.pusan.bsclab.bab.assembly.domain.info;

public enum JobStatus {
	
	RUNNING
	, SUCCEEDED
	, FAILED
}
