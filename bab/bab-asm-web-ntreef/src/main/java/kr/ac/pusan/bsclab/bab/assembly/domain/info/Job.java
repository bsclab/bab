package kr.ac.pusan.bsclab.bab.assembly.domain.info;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="job")
public class Job {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int no;
	
	private String jobId;
	
	private JobStatus jobStatus;
	
	private String job;
	
	private Timestamp start;
	
	private Timestamp end;
	
	private int progress;
	
	@Column(name="reportNo")
	private int reportNo;

	
	/**
	 * 
	 */
	public Job() {
	}

	/**
	 * @param no
	 * @param jobId
	 * @param jobType
	 * @param jobStatus
	 * @param job
	 * @param userNo
	 * @param start
	 * @param end
	 * @param progress
	 * @param reportNo
	 */
	public Job(int no, String jobId, JobStatus jobStatus, String job
			, Timestamp start,
			Timestamp end, int progress, int reportNo) {
		this.no = no;
		this.jobId = jobId;
		this.jobStatus = jobStatus;
		this.job = job;
		this.start = start;
		this.end = end;
		this.progress = progress;
		this.reportNo = reportNo;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public JobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Timestamp getStart() {
		return start;
	}

	public void setStart(Timestamp start) {
		this.start = start;
	}

	public Timestamp getEnd() {
		return end;
	}

	public void setEnd(Timestamp end) {
		this.end = end;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public int getReportNo() {
		return reportNo;
	}

	public void setReportNo(int reportNo) {
		this.reportNo = reportNo;
	}
	
	
	
}
