package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job;

import com.fasterxml.jackson.annotation.JsonProperty;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopElement;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;

/**
 * @author default
 *
 */
public class ConfigValue {
	int id;
	
	@JsonProperty("name")
	ConfigValueName configValueName;
	
	@JsonProperty("type")
	SqoopElement sqoopElement;
	
	Input[] inputs;

	
	
	
	/**
	 * 
	 */
	public ConfigValue() {
	}

	/**
	 * @param id
	 * @param configValueName
	 * @param sqoopElement
	 * @param inputs
	 */
	public ConfigValue(int id, ConfigValueName configValueName, SqoopElement sqoopElement, Input[] inputs) {
		this.id = id;
		this.configValueName = configValueName;
		this.sqoopElement = sqoopElement;
		this.inputs = inputs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ConfigValueName getConfigValueName() {
		return configValueName;
	}

	public void setConfigValueName(ConfigValueName configValueName) {
		this.configValueName = configValueName;
	}

	public SqoopElement getSqoopElement() {
		return sqoopElement;
	}

	public void setSqoopElement(SqoopElement sqoopElement) {
		this.sqoopElement = sqoopElement;
	}

	public Input[] getInputs() {
		return inputs;
	}

	public void setInputs(Input[] inputs) {
		this.inputs = inputs;
	}
	
	
}