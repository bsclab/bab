package kr.ac.pusan.bsclab.bab.assembly;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import kr.ac.pusan.bsclab.bab.assembly.job.JobListener;
import kr.ac.pusan.bsclab.bab.assembly.job.MysqlJobListener;
import kr.ac.pusan.bsclab.bab.v2.web.ext.sf.BabWebExtSf;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class BabServer extends BabWebExtSf {

	public static final String BASE_URL = "/bab";
	// public static final BabConfiguration CONFIG = new BabConfiguration();

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BabServer.class, args);
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		//slr.setDefaultLocale(Locale.KOREA);
		return slr;
	}
	
	@Bean
	public JobListener jobListener() {
		return new MysqlJobListener();
	}

}
