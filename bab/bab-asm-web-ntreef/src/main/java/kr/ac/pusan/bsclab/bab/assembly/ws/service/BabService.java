package kr.ac.pusan.bsclab.bab.assembly.ws.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueue;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.JobType;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.DBJobConfiguration;

@Service
public class BabService extends AbstractService {
	
	
	@Async
	public void csvToDB(MultipartFile multipartFile) throws IOException, InterruptedException {
		
		String uuid = UUID.randomUUID().toString();
		String jobId = uuid.substring(0, uuid.indexOf("-"));
		
		File uploadDir = new File(ap.getUploadPath()
									+ File.separator + uuid
									+ File.separator);
		File targetFile = new File(uploadDir.getAbsolutePath()
									+ File.separator + multipartFile.getOriginalFilename());
		byte[] bytesOfFile = multipartFile.getBytes();
		if (!uploadDir.exists()) uploadDir.mkdir();
		
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile));
		bos.write(bytesOfFile);
		bos.close();
		
		JobQueue jobQueue = jobQueueFactory.createJobQueue("uploadDB@" + jobId);
		Job<DBJobConfiguration> job = jf.createJob(JobType.DATABASE
				, jcf.createDBJobConfiguration(jobQueue.getJobQueueId().replaceAll("@", "")
												, targetFile.getAbsolutePath()));
		jobQueue.add(job);
		jobManager.runJobQueue(jobQueue);
	}
	
	
//	public List<Warning> 
	

}
