package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SS4 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.56688380e+00, 3.25123596e+00, 2.41584988e+01, -6.12394028e+01
            				, -2.40042162e+00, 2.97712498e+01, -1.99266970e+00, 1.44722013e+01
            				, 1.87141266e+01, 2.73189873e-01, 1.69597715e-01, -5.83502054e-01
            				, 3.06472778e-01, -5.70595131e+01, 2.53299961e+01, -6.40248179e-01
            				, 4.78882402e-01, 1.75535321e+00, 3.91327143e-01, 8.37518311e+00}, 
            			{-2.21633483e-04, 6.33359671e-01, -1.54024613e+00, 3.02781582e+00
        					, -2.06104445e+00, -2.67354298e+00, -3.91626924e-01, -9.55355346e-01
        					, -8.42441380e-01, -3.53102297e-01, 4.63345319e-01, 1.09629333e+00
        					, 1.52008200e+00, 2.13473916e+00, -2.12115264e+00, -1.13672495e+00
        					, -6.91023290e-01, 2.13661239e-01, 7.36341178e-01, -1.35231987e-01},
            			{-2.39491418e-01, 6.48252785e-01, -2.66389519e-01, -1.37203187e-02
    						, -9.57346976e-01, 6.22264504e-01, -2.96315014e-01, 2.01599613e-01
    						, 4.04398918e-01, -4.17366892e-01, 4.26958680e-01, -2.63609111e-01
    						, 8.67936015e-01, 3.80666494e-01, -1.35083511e-01, -2.92124093e-01
    						, -1.14234936e+00, -8.76998007e-01, 4.43265975e-01, -3.89856011e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.09146321, -1.0645318, 0.42678279, 0.51892501, -0.566163, 1.20850515
            			, -0.94855338, 0.23688865, 0.05326119, -0.69354272, -0.93963444, -0.81260478
            			, -1.16338038, 0.93651193, 0.48622489, -0.19468752, -0.90334821, -0.85805303
            			, -0.69119734, 0.54688269};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.08226681,
            			0.58045888,
            			0.81039608,
            			-0.5336377,
            			-0.3164424,
            			1.86448705,
            			1.06642473,
            			0.34205562,
            			0.70658559,
            			0.1448359,
            			1.89314306,
            			-0.80161291,
            			-0.51439488,
            			-0.51560444,
            			1.2252053,
            			-0.28386742,
            			-1.05431473,
            			0.25154483,
            			0.50615191,
            			0.23981471};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {1.52605724};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 2.18;
	}

	@Override
	public double getMaxWidth() {
		
		return 622.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 9366.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.35;
	}

	@Override
	public double getMinWidth() {
		
		return 28.0;
	}

	@Override
	public double getMinWeight() {
		
		return 69.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
