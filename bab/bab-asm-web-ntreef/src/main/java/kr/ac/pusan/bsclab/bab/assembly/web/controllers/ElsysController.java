package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class ElsysController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/elsys";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/elsys/{workspaceId}/{datasetId}/{repositoryId}")
	public ModelAndView getSchedule(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("elsys/elsys");

		// add jsondata here
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	}
}
