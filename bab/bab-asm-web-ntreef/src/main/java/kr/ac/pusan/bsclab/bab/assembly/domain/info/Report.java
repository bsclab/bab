package kr.ac.pusan.bsclab.bab.assembly.domain.info;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Report {
	
	@Id
	@NotNull
	@GeneratedValue(strategy=GenerationType.AUTO)
	long no;
	
	@NotNull
	int jobNo;
	
	@NotNull
	ReportType reportType;
	
	String report;

	public Report(long no, int jobNo, ReportType reportType, String report) {
		this.no = no;
		this.jobNo = jobNo;
		this.reportType = reportType;
		this.report = report;
	}

	public Report() {
	}

	public long getNo() {
		return no;
	}

	public void setNo(long no) {
		this.no = no;
	}

	public int getJobNo() {
		return jobNo;
	}

	public void setJobNo(int jobNo) {
		this.jobNo = jobNo;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
	
	
}
