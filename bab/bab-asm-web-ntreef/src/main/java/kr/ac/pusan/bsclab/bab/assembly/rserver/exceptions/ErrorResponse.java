package kr.ac.pusan.bsclab.bab.assembly.rserver.exceptions;

/**
 * Class for handling giving error information while using service of RStudio Server
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */
public class ErrorResponse {

	private int errorCode;
	private String message;

	/**
	 * @return error code
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param set error code
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return error message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}