package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue;

public enum JobQueueStatus {
	
	RUNNING, WAITING, FINISHED, FAILED, KILLED

}
