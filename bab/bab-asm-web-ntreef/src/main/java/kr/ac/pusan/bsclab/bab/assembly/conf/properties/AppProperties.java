package kr.ac.pusan.bsclab.bab.assembly.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-app.properties")
@ConfigurationProperties(prefix="app")
public class AppProperties {
	
	private String baseUrl;
	private String mainClass;
	private String reportUri;
	private String queueSize;
	private String apiWeb;
	private String apiRoot;
	private String jar;
	private String jarUri;
	
	private String webServer;
	private String webPort;
	
	private String name;
	private String version;
	
	private String uploadPath;
	private String logTableName;
	private String jdbcDriver;
	private String connectionString;
	private String dbUsername;
	private String dbPassword;
	
	
	private String fileSaveStart;
	private String fileSaveDone;
	private String fileSaveFail;
	private String dbToHdfsStart;
	private String dbToHdfsFailed;
	private String dbToHdfsDone;
	private long sqoopJobCheckerTime;
	
	private int testMode;
	private String testJarDir;
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public String getMainClass() {
		return mainClass;
	}
	public void setMainClass(String mainClass) {
		this.mainClass = mainClass;
	}
	public String getReportUri() {
		return reportUri;
	}
	public void setReportUri(String reportUri) {
		this.reportUri = reportUri;
	}
	public String getQueueSize() {
		return queueSize;
	}
	public void setQueueSize(String queueSize) {
		this.queueSize = queueSize;
	}
	public String getApiWeb() {
		return apiWeb;
	}
	public void setApiWeb(String apiWeb) {
		this.apiWeb = apiWeb;
	}
	public String getApiRoot() {
		return apiRoot;
	}
	public void setApiRoot(String apiRoot) {
		this.apiRoot = apiRoot;
	}
	public String getJar() {
		return jar;
	}
	public void setJar(String jar) {
		this.jar = jar;
	}
	public String getJarUri() {
		return jarUri;
	}
	public void setJarUri(String jarUri) {
		this.jarUri = jarUri;
	}
	public String getWebServer() {
		return webServer;
	}
	public void setWebServer(String webServer) {
		this.webServer = webServer;
	}
	public String getWebPort() {
		return webPort;
	}
	public void setWebPort(String webPort) {
		this.webPort = webPort;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getUploadPath() {
		return uploadPath;
	}
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
	public String getLogTableName() {
		return logTableName;
	}
	public void setLogTableName(String logTableName) {
		this.logTableName = logTableName;
	}
	public String getJdbcDriver() {
		return jdbcDriver;
	}
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}
	public String getConnectionString() {
		return connectionString;
	}
	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}
	public String getDbUsername() {
		return dbUsername;
	}
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getFileSaveStart() {
		return fileSaveStart;
	}
	public void setFileSaveStart(String fileSaveStart) {
		this.fileSaveStart = fileSaveStart;
	}
	public String getFileSaveDone() {
		return fileSaveDone;
	}
	public void setFileSaveDone(String fileSaveDone) {
		this.fileSaveDone = fileSaveDone;
	}
	public String getFileSaveFail() {
		return fileSaveFail;
	}
	public void setFileSaveFail(String fileSaveFail) {
		this.fileSaveFail = fileSaveFail;
	}
	public String getDbToHdfsStart() {
		return dbToHdfsStart;
	}
	public void setDbToHdfsStart(String dbToHdfsStart) {
		this.dbToHdfsStart = dbToHdfsStart;
	}
	public String getDbToHdfsFailed() {
		return dbToHdfsFailed;
	}
	public void setDbToHdfsFailed(String dbToHdfsFailed) {
		this.dbToHdfsFailed = dbToHdfsFailed;
	}
	public String getDbToHdfsDone() {
		return dbToHdfsDone;
	}
	public void setDbToHdfsDone(String dbToHdfsDone) {
		this.dbToHdfsDone = dbToHdfsDone;
	}
	public long getSqoopJobCheckerTime() {
		return sqoopJobCheckerTime;
	}
	public void setSqoopJobCheckerTime(long sqoopJobCheckerTime) {
		this.sqoopJobCheckerTime = sqoopJobCheckerTime;
	}
	public int getTestMode() {
		return testMode;
	}
	public void setTestMode(int testMode) {
		this.testMode = testMode;
	}
	public String getTestJarDir() {
		return testJarDir;
	}
	public void setTestJarDir(String testJarDir) {
		this.testJarDir = testJarDir;
	}
	
	
}
