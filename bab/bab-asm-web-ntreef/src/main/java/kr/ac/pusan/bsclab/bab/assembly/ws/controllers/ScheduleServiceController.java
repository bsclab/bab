package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.io.BufferedReader;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import kr.ac.pusan.bsclab.bab.assembly.parallelscheduling.SchedulingSAFSA;
import kr.ac.pusan.bsclab.bab.assembly.parallelscheduling.utils.ReconstructInput;
import kr.ac.pusan.bsclab.bab.assembly.parallelscheduling.DataAttributes;
import kr.ac.pusan.bsclab.bab.assembly.parallelscheduling.SchedulingDataPerOperation;
import kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.RegressionService;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;

@Controller
public class ScheduleServiceController extends AbstractServiceController {
	
	public static final String BASE_URL = BabWebService.BASE_URL + "/schedule";
	
	String[] masterColorArray = new String[] { "#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
			"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
			"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
			"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
			"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
			"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069",
			"#369", "#669", "#969", "#C69", "#F69", "#0F9", "#3F9", "#6F9", "#9F9", "#CF9", "#FF9", "#06C", "#36C",
			"#66C", "#96C", "#C6C", "#F6C", "#0FC", "#3FC", "#6FC", "#9FC", "#CFC", "#FFC", "#06F", "#36F", "#66F",
			"#96F", "#C6F", "#F6F", "#0FF", "#3FF", "#6FF", "#9FF", "#CFF", "#FFF", "#000", "#300", "#600", "#900",
			"#C00", "#F00", "#090", "#390", "#690", "#990", "#C90", "#F90", "#003", "#303", "#603", "#903", "#C03",
			"#F03", "#093", "#393", "#693", "#993", "#C93", "#F93", "#006", "#306", "#606", "#906", "#C06", "#F06",
			"#096", "#396", "#696", "#996", "#C96", "#F96", "#009", "#309", "#609", "#909", "#C09", "#F09", "#099",
			"#399", "#699", "#999", "#C99", "#F99", "#00C", "#30C", "#60C", "#90C", "#C0C", "#F0C", "#09C", "#39C",
			"#69C", "#99C", "#C9C", "#F9C", "#00F", "#30F", "#60F", "#90F", "#C0F", "#F0F", "#09F", "#39F", "#69F",
			"#99F", "#C9F", "#F9F", "#030", "#330", "#630", "#930", "#C30", "#F30", "#0C0", "#3C0", "#6C0", "#9C0",
			"#CC0", "#FC0", "#033", "#333", "#633", "#933", "#C33", "#F33", "#0C3", "#3C3", "#6C3", "#9C3", "#CC3",
			"#FC3", "#036", "#336", "#636", "#936", "#C36", "#F36", "#0C6", "#3C6", "#6C6", "#9C6", "#CC6",
			"#FC6" };
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/inits/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody String getInits(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request, HttpSession session) {
		
		HashMap<String, List<String>> jobs_per_machine_final = new HashMap<String, List<String>>(); // machine to list of jobs+seq
		double makespan_final;
		
		HashMap<String, Date> duedate_real_job = new HashMap<String, Date>();
		
		LinkedHashSet<String> list_of_coils = new LinkedHashSet<String>();
		HashMap<String, List<DataAttributes>> list_of_traces = new HashMap<String, List<DataAttributes>>(); // inside list is jobseq
		
		HashMap<String, String> colormap = new HashMap<String, String>();
		
		HashMap<String, HashSet<String>> list_group_opr = new HashMap<String, HashSet<String>>(); // list of true job:coil_no in each of raw material+seq 
		
		HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail = new HashMap<String, SchedulingDataPerOperation>(); // key is job+sequence : start from 0
		
		HashMap<String, List<String>> list_job_in_operation = new HashMap<String, List<String>>(); // list of job in position operation, key jobseq
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode outerObject = mapper.createObjectNode();
		
		String opsi = request.getParameter("opsi");
		
		try {
			String listcoil_ops = "";
			
			boolean header_flag = true; // flag the current data have header or not
			
			String fileName = "20160601_20170629_schedule_dataset_test.csv";
			
			if(!this.webHdfs.isExists("/test/schedule/"+fileName)) {
				this.webHdfs.copyFromLocal(fileName, "/test/schedule/"+fileName);
			}
			
			String content = this.webHdfs.openAsTextFile("/test/schedule/"+fileName);
				
			BufferedReader bufReader = new BufferedReader(new StringReader(content));
			
			String line = null;
		
			String prev_coilno = null;
	        String coilno = null;
	        
	        String opr = null;
	        String mach = null;
	        double thick = 0;
	        int width = 0;
	        int weight = 0;
	        
	        String linemap = null;
	        String spec_no = null;
	        
	        Date deadline = null;
	        double dur = -1;
	        
	        String parent_mtr = null;
	        String real_raw_mtr = null;
	        String raw_mtr = null;
	        
	        int seq_job = 0;
	        int seq_in_realjob = 0;
	        String jobseq = null;
	        //test 123
	        int index_drtcoino = 0;
	        int index_coilno = 1;
	        int index_hiscoino = 2;
	        int index_prccd = 3;
	        int index_operation = 4;
	        int index_machine = 20;
	        int index_thick = 8;
	        int index_width = 9;
	        int index_weight = 10;
	        int index_linemap = 16;
	        int index_specno = 19;
	        int index_deadline = 11;
	        int index_readydate = 27;
	        
	        int index_grdcd = 22;
	        int index_prdknd_cd = 23;
	        int index_stlcd = 24;
	        int index_cusid = 25;
	        int index_usrid = 26;
	        
	        boolean is_1st_aft_split = false;
	        
	        HashMap<String,String> merge_hiscoi = new HashMap<String,String>(); // merge raw_mat + seq + mach value is the first real job of this row
	        
	        List<String> pre_solution_1D = new ArrayList<String>();
	        
	    	DateTime start_date_scheduling = null;
	    	
	        while((line=bufReader.readLine()) != null) {
	        	String[] data_split = line.split(",");
				
	        	if(header_flag) // just to check if the first row data is header or data directly
	        	{
	        		header_flag = false;
	        		continue;
	        	}
	        	
        		coilno = data_split[index_coilno];
        		mach = data_split[index_machine];
        		opr = data_split[index_operation];
        		raw_mtr = data_split[index_hiscoino];
        		linemap = data_split[index_linemap]; 
        		
        		thick = Double.parseDouble(data_split[index_thick]);
        		width = Integer.parseInt(data_split[index_width]);
        		weight = Integer.parseInt(data_split[index_weight]);
        		try {
        			spec_no = String.format ("%.0f", Double.parseDouble(data_split[index_specno]));
        		}
        		catch(NumberFormatException e) {
        			spec_no = data_split[index_specno];
        		}
        		RegressionService rs; // Use machine learning to predict duration machine processing time
        		
        		if(opsi.equals("1")) // MEAN
        			rs = new RegressionService("MEAN", mach);
        		else if(opsi.equals("2")) // NN
        			rs = new RegressionService("NN", mach);
        		else // DT
        			rs = new RegressionService("DT", mach);
        		
        		dur = rs.getDuration(thick, width, weight, linemap);
        		
        		//DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        		DateFormat formatter = new SimpleDateFormat("M/d/yyyy H:mm");
        		deadline = (Date)formatter.parse(data_split[index_deadline]);
        		//deadline = LocalDateTime.parse(data_split[index_deadline]);
        		if(start_date_scheduling == null)
        			start_date_scheduling = new DateTime((Date)formatter.parse(data_split[index_readydate]));
        		
        		if((prev_coilno != null && !coilno.equals(prev_coilno))) // check if case is changed or not, if already changed set last row as due date of each case
        			duedate_real_job.put(prev_coilno, deadline);
        		
        		String histcoi = raw_mtr;
        		
        		if(!coilno.equals(prev_coilno)) { // set sequantial index to 0
        			parent_mtr = null;
        			real_raw_mtr = null;
        			seq_job = 0;
        			seq_in_realjob = 0;
        		}
        		
        		if(parent_mtr != null && is_1st_aft_split) { // this row have parent of job from same coilno, because this is new job set seq_job to 0
    				seq_job = 0;
    				is_1st_aft_split = false;
        		}
        		
        		if(opr.equals("WS") || opr.equals("SS")) {
        			if(real_raw_mtr == null)
        				raw_mtr = raw_mtr.substring(0, raw_mtr.length()-1);
        			else
        				raw_mtr = real_raw_mtr;
        			
        			jobseq = raw_mtr+"-"+seq_job;
        		}
        		else {	
        			jobseq = raw_mtr+"-"+seq_job;
        		}
        		
        		if(merge_hiscoi.get(jobseq+mach) == null) {
        			merge_hiscoi.put(jobseq+mach, "Exist");
        		        			
        			List<String> list = new ArrayList<String>();
        			list_job_in_operation.put(jobseq, list);
        			list.add(coilno);
        			
        			pre_solution_1D.add(raw_mtr);
        			list_of_coils.add(coilno);
        			
        			// set seq detail for each algorithm
    				
        			SchedulingDataPerOperation dsd = new SchedulingDataPerOperation();
        			dsd.job = raw_mtr;
        			dsd.realjob = coilno;
        			dsd.machine = mach;
        			dsd.parent_mat = parent_mtr;
        			dsd.duration = dur;
        			dsd.jobseq = jobseq;
        			
        			dsd.real_histcoi = histcoi;
        			dsd.thick = thick;
        			dsd.width = width;
        			dsd.weight = weight;
        			
        			dsd.seq_in_realjob = seq_in_realjob;
        			
        			dsd.line_map = data_split[index_linemap];
        			dsd.spec_code = spec_no;
        			dsd.grd_cd = data_split[index_grdcd];
        			dsd.prdknd_cd = data_split[index_prdknd_cd];
        	        dsd.stl_cd = data_split[index_stlcd];
        	        dsd.cus_id = data_split[index_cusid];
        	        dsd.usr_id = data_split[index_usrid];
        	        dsd.prc_cd = data_split[index_prccd];
        			dsd.drtcoi_no = data_split[index_drtcoino];
        			dsd.grd_cd = data_split[index_grdcd];
        			dsd.prc_cd = data_split[index_prccd];
        			
        			if(colormap.get(dsd.drtcoi_no) == null) {
        				colormap.put(dsd.drtcoi_no, masterColorArray[colormap.size()%216]);
        			}
        			
        			dsd.color_drtcoi = colormap.get(dsd.drtcoi_no);
        			
    				jobopr_per_seq_detail.put(jobseq, dsd);
        		}
        		else
        		{
        			List<String> list = list_job_in_operation.get(jobseq);
        			list.add(coilno);
        		}
        		
        		List<DataAttributes> traceopr = list_of_traces.get(coilno);
    			if(traceopr == null) {
    				traceopr = new ArrayList<DataAttributes>();
    				list_of_traces.put(coilno, traceopr);
    			}
    			
    			DataAttributes da = new DataAttributes();
    			da.coil = coilno;
    			da.histcoino = histcoi;
    			da.thick = thick;
    			da.width = width;
    			da.weight = weight;
    			da.jobseq = jobseq;
    			da.linemap = linemap;
    			da.spec_code = spec_no;
    			da.grd_cd = data_split[index_grdcd];
    			da.prdknd_cd = data_split[index_prdknd_cd];
    	        da.stl_cd = data_split[index_stlcd];
    	        da.cus_id = data_split[index_cusid];
    	        da.usr_id = data_split[index_usrid];
    	        da.drtcoi_no = data_split[index_drtcoino];
    	        da.prc_cd = data_split[index_prccd];
    	        
				traceopr.add(da);
				
				seq_job++;
        		seq_in_realjob++;
        		
        		prev_coilno = coilno;
        		real_raw_mtr = histcoi;
        		if((opr.equals("WS") || opr.equals("SS")) && !raw_mtr.equals(real_raw_mtr))
        		{	
        			parent_mtr = raw_mtr;
        			is_1st_aft_split = true;
        		}
			}
				
	        duedate_real_job.put(coilno, deadline); // set due date of last case in schedule data
	        
			SchedulingSAFSA ss = new SchedulingSAFSA(list_group_opr, pre_solution_1D, jobopr_per_seq_detail, list_of_coils, list_of_traces); // this is the most important method in scheduling algorithm, the core of scheduling algorithm is here
			ss.setDuration(1*50*1000);
			ss.runProcess();
			jobs_per_machine_final = ss.syncobj.bb_jobs_per_machine;
			makespan_final = ss.getBestMakespan();
			//System.out.println("Makespan_final "+makespan_final);	
		        
			int maxH = 0;
			int maxV = 0;
			
			int number_list_machine = jobs_per_machine_final.keySet().size();
			
			Object[] jobs_per_machine_array = jobs_per_machine_final.keySet().toArray();
			
			if(request.getParameter("order") != null) {
				Arrays.sort(jobs_per_machine_array);
			}
			
			String svgtop = "";
			String svgleft = "";
			String svg = "";
			String list_drtcoi = "";
			
			int delta = 30;
			int numb_divider = (int)Math.ceil(makespan_final/(double)delta);
			
			//PrintWriter writer = new PrintWriter("report_schedule.csv", "UTF-8");
//			String line_head = "COIL_NO,HISTCOIL_NO,MACHINE,OPERATION,THK,WDT,WGT,START_TIME,END_TIME,JOBSEQ";
			
			String report_s = "";
			
			int numrows = 0;
			
			int count_not_comply = 0;
			int total_coil = list_of_coils.size();
			
			String kpi = "";
			double efficiency_total = 0;
			
			for(String coilnos : list_of_coils) {
				listcoil_ops += "<option value='"+coilnos+"'>"+coilnos+"</option>";
				
				List<DataAttributes> list_opr_in_coil = list_of_traces.get(coilnos);
				
				boolean is_comply = true;
				
				Date dued = duedate_real_job.get(coilnos);
				Date real_ed = null;
				Date real_sd = null;
				
				double efficiency_per_coil = 0;
				long processing_time = 0;
				
				for(DataAttributes da : list_opr_in_coil) {
					SchedulingDataPerOperation dsss = ss.getJobOprPerSeqDetail().get(da.jobseq);
					
					DateTime sts = start_date_scheduling.plusMinutes((int) dsss.st_time);//.toString("yyyy-MM-dd kk:mm");
					DateTime ets = start_date_scheduling.plusMinutes((int) dsss.ed_time);//.toString("yyyy-MM-dd kk:mm");
					
					dsss.humanize_st_time = sts.toString("yyyy-MM-dd");
					dsss.humanize_ed_time = ets.toString("yyyy-MM-dd");
					
					Duration dura;
					
					if(real_sd == null)
						real_sd = sts.toDate();
					
					real_ed = ets.toDate();
					
					dura = Duration.between(sts.toDate().toInstant(), ets.toDate().toInstant());
					processing_time += dura.toMinutes();
					
					numrows++;
					
					report_s += "<tr style='width: 100%; display: table; table-layout: fixed'><td>"+numrows+"</td><td>"+coilnos+"</td><td>"+dsss.real_histcoi+"</td><td>"+dsss.machine.substring(0,2)+"</td><td>"+sts.toString("yyyy-MM-dd HH:mm")+"</td><td>"+ets.toString("yyyy-MM-dd HH:mm")+"</td><td>"+dsss.machine+"</td><td>"+da.thick+"</td><td>"+da.width+"</td><td>"+da.weight+"</td></tr>";
//					String line_in_row = coilnos+","+dsss.real_histcoi+","+dsss.machine+","+dsss.machine.substring(0,3)+","+da.thick+","+da.width+","+da.weight+","+sts.toString("yyyy-MM-dd HH:mm")+","+ets.toString("yyyy-MM-dd HH:mm");
					//writer.println(line_in_row);
				}
				
				double coil_makespan = Duration.between(real_sd.toInstant(), real_ed.toInstant()).toMinutes();
				
				if(coil_makespan != 0)
					efficiency_per_coil = processing_time/(double) coil_makespan;
				
				efficiency_per_coil *= 100;
				
				efficiency_total = efficiency_total+efficiency_per_coil;
				
				if(dued != null && real_ed != null)
					is_comply = dued.after(real_ed);
				
				if(!is_comply)
					count_not_comply++;
				
			}
			
			System.out.println(ss.syncobj.bb_sol_1D);
			efficiency_total = efficiency_total/(double)list_of_coils.size();
			
			//writer.close();
			
			kpi = "Makespan "+timeConvertFromSec((int)makespan_final*60)+"<br/>";
			kpi += "Efficiency "+(String.format("%.2f",efficiency_total)+"%<br/>");
			
			double percentage_not_comply = count_not_comply/(double)total_coil;
			double percentage_comply = (total_coil-count_not_comply)/(double)total_coil;
			
			kpi += "Satisfied "+String.format("%.2f",percentage_comply*100)+"%<br/>";
			kpi += "Not Satisfied "+String.format("%.2f",percentage_not_comply*100)+"%<br/>";
			
			//kpi += "<br/><a class='ui button' id='buttonCRM'>CRM by facility</a><br/>";
			//kpi += "<br/><a class='ui button' id='buttonCRM_ANN'>CRM / ANN</a>";
			
			int yi=0;
			
			JSONArray list_of_machines = new JSONArray();
			
			String json_jpa_store = mapper.writeValueAsString(jobs_per_machine_final);
			String json_jpsq_store = mapper.writeValueAsString(ss.getJobOprPerSeqDetail());
			String json_list_of_coils = mapper.writeValueAsString(list_of_coils);
			String json_list_of_traces = mapper.writeValueAsString(list_of_traces);
			String json_list_job_in_operation = mapper.writeValueAsString(list_job_in_operation);
			String store_to_server = "{\"jpa\":"+json_jpa_store+",\"jpsq\":"+json_jpsq_store+",\"makespan\":"+makespan_final+",\"st_dt\":\""+start_date_scheduling.toString("yyyy-MM-dd hh:mm:ss")+"\",\"list_of_coils\":"+json_list_of_coils+",\"list_of_traces\":"+json_list_of_traces+",\"list_job_in_operation\":"+json_list_job_in_operation+"}";
			String file_unique = request.getRemoteAddr()+"_"+UUID.randomUUID().toString().replaceAll("-", "")+".json";
			this.webHdfs.saveAsTextFile("/test/schedule/"+file_unique, store_to_server);
			
			Set<String> key_drtcoi = colormap.keySet();
			
			for(String drtcoi : key_drtcoi) {
				list_drtcoi += "<div>"
						+ "<input type='checkbox' class='coilSelectListener' onclick='test(this)' id='" + drtcoi + "' checked/>"
						+ "<span style='display: inline-block; width: 1em; height: 1em; background: "+colormap.get(drtcoi)+"; font-size:1em'></span>"
						+ drtcoi
						+ "</div>";
			}
			
			for(Object machine : jobs_per_machine_array) {
				JSONObject job_machine = new JSONObject();
				job_machine.put("name", (String)machine);
				job_machine.put("index", yi);
				list_of_machines.put(job_machine);
				
				svgleft += "<circle id='circ"+yi+"' cx='10' cy='"+(14+yi*30)+"' r='10'/><text id='text"+yi+"' x='25' y='"+(14+yi*30)+"' dy='.35em'>"+machine+"</text>";
				
				svg += "<g class='bar'><rect class=' rect"+yi+"' width='"+makespan_final*3+"' height='30' x='10' y='"+(yi*30)+"' style='fill: #ffffff'></rect>";
				
				maxH = (int) (makespan_final*3);
				
				for(String jobseq_mach : jobs_per_machine_final.get(machine)) {
					SchedulingDataPerOperation dsd = ss.getJobOprPerSeqDetail().get(jobseq_mach);
//					svg += "<rect id='" + dsd.jobseq + "' class='rect"+yi+"' value='"+dsd.jobseq+"' width='"+dsd.duration*3+"' height='30' x='"+(10+dsd.st_time*3)+"' y='"+(yi*30)+"' style='fill: "+dsd.color_drtcoi+"'></rect>";
					svg += "<rect class='colouredbar "+dsd.drtcoi_no+"'  value='"+dsd.jobseq+"' width='"+dsd.duration*3+"' height='30' x='"+(10+dsd.st_time*3)+"' y='"+(yi*30)+"' style='fill: "+dsd.color_drtcoi+"'></rect>";

					maxV = yi*30;
				}
				svg += "</g>";
				yi++;
			}
				
			for(int x=0; x<=numb_divider; x++) {
				String time = start_date_scheduling.toString("HH:mm");
				if(x==0 || time.equals("00:00"))
					svgtop += "<text class='small date' x='"+(90*x)+"' y='15'>"+start_date_scheduling.toString("yyyy-MM-dd")+"</text>";
				 
				svgtop += "<text class='small' x='"+(90*x)+"' y='25'>"+time+"</text>";
				start_date_scheduling = start_date_scheduling.plusMinutes(delta);
				
				if(time.equals("07:00") || time.equals("15:00") || time.equals("23:00"))
					svg += "<line class='grid x-grid' x1='"+(10+90*x)+"' x2='"+(10+90*x)+"' y1='0' y2='"+(number_list_machine*30)+"'></line>";		
			}
			
			//==================================================================================================
			List<Map<String, Object>> _jobs = new ArrayList<Map<String, Object>>();
			for (String key : jobs_per_machine_final.keySet()) {
				
				String p_code = key.substring(0, 2);
				
				for (String now_job : jobs_per_machine_final.get(key)) {
					Map<String, Object> _job = new HashMap<String, Object>();
					_job.put("p_code", p_code);
					_job.put("job", now_job);
					_job.put("start", ss.getJobOprPerSeqDetail().get(now_job).humanize_st_time);
					_job.put("end", ss.getJobOprPerSeqDetail().get(now_job).humanize_ed_time);
					_job.put("weight", ss.getJobOprPerSeqDetail().get(now_job).weight);
					_job.put("prc_cd", ss.getJobOprPerSeqDetail().get(now_job).prc_cd);
					
					if (key.matches("^[a-zA-Z]{3}\\d*")) { // siheung process code is [a-zA-Z]{3}[0-1]
						if (key.length()==3) key+="1";
						_job.put("site", "siheung");
						_job.put("machine_num", key.substring(3,4));
						
						
					} else if (key.matches("^[a-zA-Z]{2}\\d*")) { // pohang process code is [a-zA-Z]{2}[0-1]
						_job.put("machine_num", key.substring(2,3));
						_job.put("site", "pohang");
					} else {
						System.out.println("##### " + key);
					}
					
					if (p_code.equals("AN"))
						_job.put("machine_num", 1);
					else if (p_code.equals("PK"))
						_job.put("machine_num", 1);
					_jobs.add(_job);
					
				}	
			}
			//==================================================================================================
			
			
			
			outerObject.put("_jobs", mapper.writeValueAsString(_jobs));
			outerObject.put("svgleft",svgleft);
			outerObject.put("svgtop",svgtop);
			outerObject.put("svg",svg);
			outerObject.put("list_drtcoi",list_drtcoi);
			outerObject.put("report_s", report_s);
			outerObject.put("listops", listcoil_ops);
			outerObject.put("kpi",kpi);
			outerObject.put("maxH", maxH); // set maximum position of horizontally element in gantt chart
			outerObject.put("maxV", maxV); // set maximum position of vertically element in gantt chart
			outerObject.put("ordered_machine", list_of_machines.toString());
			outerObject.put("result_schedule_file", file_unique);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return outerObject.toString();
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/search/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody String getSearch(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			@RequestBody String requestBody,
			HttpServletRequest request, HttpSession session) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode outerObject = mapper.createObjectNode();
		
		String content = this.webHdfs.openAsTextFile("/test/schedule/"+request.getParameter("result_schedule_file"));
		
		TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
		TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
		TypeReference<LinkedHashSet<String>> typeRef3 = new TypeReference<LinkedHashSet<String>>() {};
		TypeReference<HashMap<String, List<DataAttributes>>> typeRef4 = new TypeReference<HashMap<String, List<DataAttributes>>>() {};
		
		HashMap<String, List<String>> jobs_per_machine_final;
		HashMap<String, List<String>> list_job_in_operation;
		
		HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail_final;
		LinkedHashSet<String> list_of_coils;
		HashMap<String, List<DataAttributes>> list_of_traces;
		
		JSONArray list_of_machines = new JSONArray();
		
		try {
			JSONArray list_of_machines_client = new JSONArray(requestBody);
			
			JSONObject job = new JSONObject(content);
			jobs_per_machine_final = mapper.readValue(job.getJSONObject("jpa").toString(), typeRef1);
			list_job_in_operation = mapper.readValue(job.getJSONObject("list_job_in_operation").toString(), typeRef1);
			jobopr_per_seq_detail_final = mapper.readValue(job.getJSONObject("jpsq").toString(), typeRef2);
			list_of_coils = mapper.readValue(job.getJSONArray("list_of_coils").toString(), typeRef3);
			list_of_traces = mapper.readValue(job.getJSONObject("list_of_traces").toString(), typeRef4);
			double makespan_final = job.getDouble("makespan");
			
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			DateTime start_date_scheduling = new DateTime((Date)formatter.parse(job.getString("st_dt")));
			
			int number_list_machine = jobs_per_machine_final.keySet().size();
			
			String filter = request.getParameter("filter");
			String search = request.getParameter("search");
			String edtx = request.getParameter("sdt");
			String sdtx = request.getParameter("edt");
			String recreate = request.getParameter("recreate");
			
			Object[] jobs_per_machine_array = new String[number_list_machine];
			if(recreate == null)
				for(int i=0; i<number_list_machine; i++) {
					String names = list_of_machines_client.getJSONObject(i).getString("name");
					jobs_per_machine_array[i] = names;
				}
			else
				jobs_per_machine_array = jobs_per_machine_final.keySet().toArray();
			
			String svgleft = "";
			String svg = "";
			
			int delta = 30;
			int numb_divider = (int)Math.ceil(makespan_final/(double)delta);
			
			String report_s = "";

			int numrows = 0;
			
			for(String coilnos : list_of_coils) {
				
				List<DataAttributes> list_opr_in_coil = list_of_traces.get(coilnos);
				for(DataAttributes da : list_opr_in_coil) {
					SchedulingDataPerOperation dsss = jobopr_per_seq_detail_final.get(da.jobseq);
					DateTime sts = start_date_scheduling.plusMinutes((int) dsss.st_time);//.toString("yyyy-MM-dd kk:mm");
					DateTime ets = start_date_scheduling.plusMinutes((int) dsss.ed_time);//.toString("yyyy-MM-dd kk:mm");
					numrows++;
					
					boolean show = false;
					if((sdtx == null || sdtx.equals("") || sts.toString("yyyy-MM-dd HH:mm").compareTo(sdtx) >= 0) && (edtx == null || edtx.equals("") || ets.toString("yyyy-MM-dd HH:mm").compareTo(edtx) <= 0))
					{
						if(filter.equals("PLNPRC_CD") && (search == null || search.equals("") || da.linemap.equals(search)))
							show = true;
						else if (filter.equals("PRD_KND") && (search == null || search.equals("") || da.prdknd_cd.equals(search)))
							show = true;
						else if (filter.equals("STL_CD") && (search == null || search.equals("") || da.stl_cd.equals(search)))
							show = true;
						else if (filter.equals("GRD_CD") && (search == null || search.equals("") || da.grd_cd.equals(search)))
							show = true;
						else if (filter.equals("USR_ID") && (search == null || search.equals("") || da.usr_id.equals(search)))
							show = true;
						else if (filter.equals("SPC_NO") && (search == null || search.equals("") || da.spec_code.equals(search)))
							show = true;
						else if (filter.equals("THICK") && (search == null || search.equals("") || da.thick == Double.parseDouble(search)))
							show = true;
						else if (filter.equals("WIDTH") && (search == null || search.equals("") || da.width == Double.parseDouble(search)))
							show = true;
						else if (filter.equals("CUST_ID") && (search == null || search.equals("") || da.cus_id.equals(search)))
							show = true;
					}
					
					if(show) {
//						report_s += "<tr><td style='padding: 10px; border: 1px solid black;'>"+numrows+"</td><td style='padding: 10px; border: 1px solid black;'>"+coilnos+"</td><td style='padding: 10px; border: 1px solid black;'>"+dsss.real_histcoi+"</td><td style='padding: 10px; border: 1px solid black;'>"+dsss.machine.substring(0, 3)+"</td><td style='padding: 10px; border: 1px solid black;'>"+sts.toString("yyyy-MM-dd HH:mm")+"</td><td style='padding: 10px; border: 1px solid black;'>"+ets.toString("yyyy-MM-dd HH:mm")+"</td><td style='padding: 10px; border: 1px solid black;'>"+dsss.machine+"</td><td style='padding: 10px; border: 1px solid black;'>"+da.thick+"</td><td style='padding: 10px; border: 1px solid black;'>"+da.width+"</td><td style='padding: 10px; border: 1px solid black;'>"+da.weight+"</td></tr>";
						report_s += "<tr style='width: 100%; display: table; table-layout: fixed'><td>"+numrows+"</td><td>"+coilnos+"</td><td>"+dsss.real_histcoi+"</td><td>"+dsss.machine.substring(0, 3)+"</td><td>"+sts.toString("yyyy-MM-dd HH:mm")+"</td><td>"+ets.toString("yyyy-MM-dd HH:mm")+"</td><td>"+dsss.machine+"</td><td>"+da.thick+"</td><td>"+da.width+"</td><td>"+da.weight+"</td></tr>";
					}
				}
			}
			
			int yi = 0;
			
			for(Object machine : jobs_per_machine_array) {
				JSONObject job_machine = new JSONObject();
				job_machine.put("name", (String)machine);
				job_machine.put("index", yi);
				list_of_machines.put(job_machine);
				
				svgleft += "<circle id='circ"+yi+"' cx='10' cy='"+(14+yi*30)+"' r='10'/><text id='text"+yi+"' x='25' y='"+(14+yi*30)+"' dy='.35em'>"+machine+"</text>";
				
				svg += "<g class='bar'><rect class='rect"+yi+"' width='"+makespan_final*3+"' height='30' x='10' y='"+(yi*30)+"' style='fill: #ffffff'></rect>";
				
				for(String jobseq_mach : jobs_per_machine_final.get(machine)) {
					SchedulingDataPerOperation dsd = jobopr_per_seq_detail_final.get(jobseq_mach);
					SchedulingDataPerOperation dsss = jobopr_per_seq_detail_final.get(dsd.jobseq);
					
					DateTime sts = start_date_scheduling.plusMinutes((int) dsss.st_time);//.toString("yyyy-MM-dd kk:mm");
					DateTime ets = start_date_scheduling.plusMinutes((int) dsss.ed_time);//.toString("yyyy-MM-dd kk:mm");
					
					for(String jobb : list_job_in_operation.get(dsd.jobseq)) {
						DataAttributes da = list_of_traces.get(jobb).get(dsd.seq_in_realjob);
						
						boolean show = false;
						if((sdtx == null || sdtx.equals("") || sts.toString("yyyy-MM-dd HH:mm").compareTo(sdtx) >= 0) && (edtx == null || edtx.equals("") || ets.toString("yyyy-MM-dd HH:mm").compareTo(edtx) <= 0))
						{
							if(filter.equals("PLNPRC_CD") && (search == null || search.equals("") || da.linemap.equals(search)))
								show = true;
							else if (filter.equals("PRD_KND") && (search == null || search.equals("") || da.prdknd_cd.equals(search)))
								show = true;
							else if (filter.equals("STL_CD") && (search == null || search.equals("") || da.stl_cd.equals(search)))
								show = true;
							else if (filter.equals("GRD_CD") && (search == null || search.equals("") || da.grd_cd.equals(search)))
								show = true;
							else if (filter.equals("USR_ID") && (search == null || search.equals("") || da.usr_id.equals(search)))
								show = true;
							else if (filter.equals("SPC_NO") && (search == null || search.equals("") || da.spec_code.equals(search)))
								show = true;
							else if (filter.equals("THICK") && (search == null || search.equals("") || da.thick == Double.parseDouble(search)))
								show = true;
							else if (filter.equals("WIDTH") && (search == null || search.equals("") || da.width == Double.parseDouble(search)))
								show = true;
							else if (filter.equals("CUST_ID") && (search == null || search.equals("") || da.cus_id.equals(search)))
								show = true;
						}
						
						if(show) {
//							svg += "<rect class='coloured rect"+yi+"' value='"+dsd.jobseq+"' width='"+dsd.duration*3+"' height='30' x='"+(10+dsd.st_time*3)+"' y='"+(yi*30)+"' style='fill: "+dsd.color_drtcoi+"'></rect>";
							svg += "<rect value='"+dsd.jobseq+"' width='"+dsd.duration*3+"' height='30' x='"+(10+dsd.st_time*3)+"' y='"+(yi*30)+"' style='fill: "+dsd.color_drtcoi+"'></rect>";

						}
					}			
				}
				svg += "</g>";
				yi++;
			}
			
			for(int x=0; x<numb_divider; x++) {
				String time = start_date_scheduling.toString("HH:mm");
				if(time.equals("07:00") || time.equals("15:00") || time.equals("23:00"))
					svg += "<line class='grid x-grid' x1='"+(10+90*x)+"' x2='"+(10+90*x)+"' y1='0' y2='"+(number_list_machine*30)+"'></line>";
			}
			
			outerObject.put("ordered_machine", list_of_machines.toString());
			outerObject.put("svgleft",svgleft);
			outerObject.put("svg",svg);
			outerObject.put("report_s", report_s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outerObject.toString();
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/dataForModal/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody String getDataForModal(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request, HttpSession session) {
		 
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode outerObject = mapper.createObjectNode();
		
		String jobseq_find = request.getParameter("jobseq");
		String flag = request.getParameter("flag");
		String content = this.webHdfs.openAsTextFile("/test/schedule/"+request.getParameter("result_schedule_file"));
		
		TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
		TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
		TypeReference<LinkedHashSet<String>> typeRef3 = new TypeReference<LinkedHashSet<String>>() {};
		TypeReference<HashMap<String, List<DataAttributes>>> typeRef4 = new TypeReference<HashMap<String, List<DataAttributes>>>() {};
		
		HashMap<String, List<String>> list_job_in_operation;
		
		HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail_final;
		LinkedHashSet<String> list_of_coils;
		HashMap<String, List<DataAttributes>> list_of_traces;
		
		try {
			JSONObject job = new JSONObject(content);
			list_job_in_operation = mapper.readValue(job.getJSONObject("list_job_in_operation").toString(), typeRef1);
			jobopr_per_seq_detail_final = mapper.readValue(job.getJSONObject("jpsq").toString(), typeRef2);
			list_of_coils = mapper.readValue(job.getJSONArray("list_of_coils").toString(), typeRef3);
			list_of_traces = mapper.readValue(job.getJSONObject("list_of_traces").toString(), typeRef4);
			
			SchedulingDataPerOperation ssd = jobopr_per_seq_detail_final.get(jobseq_find);
			
			if(flag.equals("one")) {
				outerObject.put("histcoi", "Histcoino "+ssd.real_histcoi);
				outerObject.put("list_of_job", list_job_in_operation.get(jobseq_find).toString());
				outerObject.put("linemap", ssd.line_map);
				outerObject.put("spec_number", ssd.spec_code);
				outerObject.put("cus_id", ssd.cus_id);
				outerObject.put("prc_cd", ssd.prc_cd);
				outerObject.put("duration", ssd.duration+" minutes");
			}
			else if(flag.equals("two")) {
				String drtcoi = ssd.drtcoi_no;
				
				SortedSet<String> list_jobseq = new TreeSet<String>();
				
				for(String coilnos : list_of_coils) {
					List<DataAttributes> list_opr_in_coil = list_of_traces.get(coilnos);
					for(DataAttributes da : list_opr_in_coil) {
						SchedulingDataPerOperation dsss = jobopr_per_seq_detail_final.get(da.jobseq);
						if(dsss.drtcoi_no.equals(drtcoi)) {
							String[] split_jobseq = dsss.jobseq.split("-");
							String jobse = split_jobseq[0];
							String seq = split_jobseq[1];
							if(seq.length() == 1)
								seq = "0"+seq;
							String jobseq_new = jobse+"-"+seq;
							list_jobseq.add(jobseq_new+"_"+dsss.machine+"_"+dsss.parent_mat);
						}
					}
				}
				
				outerObject.put("drtcoi", "DRTCOI "+drtcoi);
				outerObject.put("flow_drtcoi", ReconstructInput.constructToJSON(list_jobseq.iterator()).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outerObject.toString();
	}
	
	private String timeConvertFromSec(int time) {  // second to DD:HH:mm
		return time/24/60/60 + "<b>d</b> " + addif1(""+time/60/60%24) + ':' + addif1(""+time/60%60);
	}
	
	private String addif1(String time) {
		if(time.length() == 1)
			return "0"+time;
		else
			return time;
	}
}
