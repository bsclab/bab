package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SS1 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP11004")) return 30;
if(p.equals("PP11008"))
 if(t <= 2.98)
  if(w <= 317)
   if(w <= 199) return 20;
   if(w > 199)
    if(t <= 2.02) return 25;
    if(t > 2.02)
     if(we <= 1872) return 30;
     if(we > 1872) return 20;
  if(w > 317) return 35;
 if(t > 2.98)
  if(w <= 199) return 35;
  if(w > 199)
   if(we <= 2065)
    if(we <= 1981)
     if(we <= 1953)
      if(we <= 1887) return 40;
      if(we > 1887) return 30;
     if(we > 1953) return 40;
    if(we > 1981)
     if(we <= 2030)
      if(we <= 2015) return 36.6666666666667;
      if(we > 2015) return 35;
     if(we > 2030)
      if(we <= 2049) return 29;
      if(we > 2049) return 30;
   if(we > 2065)
    if(we <= 2141)
     if(we <= 2108) return 40;
     if(we > 2108) return 26.25;
    if(we > 2141)
     if(we <= 2189) return 25.8333333333333;
     if(we > 2189) return 35;
if(p.equals("PP11009"))
 if(t <= 2.3)
  if(we <= 1602) return 20;
  if(we > 1602)
   if(w <= 308) return 30;
   if(w > 308) return 20;
 if(t > 2.3) return 30;
if(p.equals("PP11010"))
 if(we <= 465)
  if(we <= 309)
   if(t <= 2.5) return 30;
   if(t > 2.5) return 55;
  if(we > 309)
   if(w <= 73)
    if(t <= 2.72) return 50;
    if(t > 2.72) return 45;
   if(w > 73) return 40;
 if(we > 465)
  if(we <= 1184)
   if(t <= 3.225)
    if(t <= 2.97)
     if(we <= 534)
      if(we <= 525) return 40;
      if(we > 525) return 30;
     if(we > 534)
      if(t <= 2.465)
       if(t <= 2.195) return 40;
       if(t > 2.195) return 35;
      if(t > 2.465) return 50;
    if(t > 2.97)
     if(t <= 3)
      if(t <= 2.98) return 45;
      if(t > 2.98) return 24.5;
     if(t > 3)
      if(t <= 3.21)
       if(we <= 692) return 25;
       if(we > 692) return 35;
      if(t > 3.21)
       if(w <= 78.5) return 40;
       if(w > 78.5) return 30;
   if(t > 3.225)
    if(t <= 3.915)
     if(t <= 3.3)
      if(t <= 3.24)
       if(we <= 1027) return 75;
       if(we > 1027)
        if(we <= 1092) return 35;
        if(we > 1092) return 40;
      if(t > 3.24) return 40;
     if(t > 3.3)
      if(w <= 78) return 40;
      if(w > 78) return 35;
    if(t > 3.915)
     if(t <= 4.45)
      if(w <= 135) return 40;
      if(w > 135) return 30;
     if(t > 4.45)
      if(we <= 660)
       if(w <= 89) return 40;
       if(w > 89) return 35;
      if(we > 660) return 50;
  if(we > 1184)
   if(we <= 1872)
    if(w <= 199)
     if(we <= 1277)
      if(w <= 194.8) return 30;
      if(w > 194.8) return 50;
     if(we > 1277) return 20;
    if(w > 199) return 30;
   if(we > 1872) return 40;
if(p.equals("PP11012")) return 45;
if(p.equals("PP11017"))
 if(t <= 3)
  if(we <= 1906) return 60;
  if(we > 1906) return 80;
 if(t > 3)
  if(we <= 788) return 40;
  if(we > 788) return 35;
if(p.equals("PP11018")) return 30;
if(p.equals("PP11021"))
 if(t <= 2.72)
  if(we <= 1709)
   if(t <= 2.45) return 30;
   if(t > 2.45)
    if(we <= 1512) return 40;
    if(we > 1512) return 35;
  if(we > 1709) return 30;
 if(t > 2.72)
  if(t <= 3.97)
   if(we <= 1428)
    if(t <= 3.615) return 40;
    if(t > 3.615)
     if(t <= 3.87)
      if(we <= 190) return 48.4666666666667;
      if(we > 190) return 35;
     if(t > 3.87)
      if(t <= 3.96)
       if(w <= 180) return 100;
       if(w > 180)
        if(we <= 1248) return 45;
        if(we > 1248) return 100;
      if(t > 3.96) return 40;
   if(we > 1428)
    if(t <= 3.38)
     if(t <= 3) return 65;
     if(t > 3) return 40;
    if(t > 3.38) return 50;
  if(t > 3.97)
   if(t <= 4.2)
    if(t <= 3.98)
     if(we <= 536) return 50;
     if(we > 536)
      if(w <= 242)
       if(we <= 585) return 30;
       if(we > 585)
        if(we <= 604) return 45;
        if(we > 604) return 30;
      if(w > 242)
       if(we <= 2112)
        if(we <= 1947) return 30;
        if(we > 1947)
         if(we <= 2026) return 35;
         if(we > 2026) return 32;
       if(we > 2112) return 40;
    if(t > 3.98)
     if(w <= 115)
      if(t <= 4.1) return 45;
      if(t > 4.1) return 70;
     if(w > 115)
      if(w <= 188) return 50;
      if(w > 188) return 45;
   if(t > 4.2)
    if(t <= 4.47) return 40;
    if(t > 4.47) return 35;
if(p.equals("PP11029"))
 if(w <= 212)
  if(w <= 113)
   if(we <= 289) return 52.5;
   if(we > 289) return 80;
  if(w > 113)
   if(w <= 188)
    if(we <= 1502) return 60;
    if(we > 1502) return 30;
   if(w > 188)
    if(t <= 2.595)
     if(we <= 1222) return 90;
     if(we > 1222) return 50;
    if(t > 2.595) return 50;
 if(w > 212)
  if(we <= 1144) return 30;
  if(we > 1144)
   if(t <= 3.87) return 25;
   if(t > 3.87) return 30;
if(p.equals("PP11030"))
 if(we <= 880) return 70;
 if(we > 880) return 35;
if(p.equals("PP11036"))
 if(we <= 728)
  if(w <= 48) return 45;
  if(w > 48)
   if(t <= 1.7)
    if(we <= 454) return 40;
    if(we > 454) return 50;
   if(t > 1.7) return 40;
 if(we > 728)
  if(t <= 1.53) return 65;
  if(t > 1.53) return 30;
if(p.equals("PP11038")) return 35;
if(p.equals("PP11045"))
 if(t <= 1.7)
  if(we <= 1650) return 50;
  if(we > 1650) return 30;
 if(t > 1.7) return 75;
if(p.equals("PP11047"))
 if(w <= 214)
  if(we <= 645) return 40;
  if(we > 645) return 60;
 if(w > 214) return 40;
if(p.equals("PP11050"))
 if(t <= 3.24) return 40;
 if(t > 3.24) return 60;
if(p.equals("PP11057"))
 if(we <= 2141) return 20;
 if(we > 2141)
  if(we <= 2276) return 40;
  if(we > 2276) return 20;
if(p.equals("PP11059")) return 35;
if(p.equals("PP11105")) return 35;
if(p.equals("PP11264"))
 if(we <= 1184) return 20;
 if(we > 1184) return 30;
if(p.equals("PP11288"))
 if(w <= 47)
  if(t <= 4.1) return 16.25;
  if(t > 4.1) return 70;
 if(w > 47) return 40;
if(p.equals("PP11294"))
 if(t <= 2.185)
  if(w <= 348.9) return 22.5;
  if(w > 348.9) return 35;
 if(t > 2.185)
  if(we <= 1167) return 40;
  if(we > 1167) return 20;
if(p.equals("PP11351"))
 if(we <= 384) return 45;
 if(we > 384) return 40;
if(p.equals("PP11659")) return 35;
if(p.equals("PP11671"))
 if(we <= 1636) return 60;
 if(we > 1636)
  if(t <= 2.385) return 40;
  if(t > 2.385)
   if(we <= 1821) return 30;
   if(we > 1821) return 40;
if(p.equals("PP11884"))
 if(t <= 1.3) return 40;
 if(t > 1.3) return 45;
if(p.equals("PP11891")) return 40;
if(p.equals("PP11892")) return 30;
if(p.equals("PP11931"))
 if(t <= 1.42)
  if(we <= 642)
   if(we <= 600) return 40;
   if(we > 600)
    if(t <= 1.3) return 40;
    if(t > 1.3)
     if(we <= 625)
      if(we <= 618) return 45;
      if(we > 618) return 43.3333333333333;
     if(we > 625)
      if(we <= 637)
       if(we <= 630) return 30;
       if(we > 630) return 40;
      if(we > 637)
       if(we <= 639) return 32.5;
       if(we > 639) return 35;
  if(we > 642)
   if(t <= 1.3)
    if(w <= 47) return 47.3;
    if(w > 47)
     if(we <= 750) return 45;
     if(we > 750) return 40;
   if(t > 1.3)
    if(we <= 660)
     if(we <= 649)
      if(we <= 646) return 37.5;
      if(we > 646) return 31.5333333333333;
     if(we > 649) return 40;
    if(we > 660)
     if(we <= 670) return 36.6666666666667;
     if(we > 670) return 30;
 if(t > 1.42)
  if(we <= 700)
   if(we <= 616)
    if(we <= 537) return 30;
    if(we > 537)
     if(we <= 603)
      if(w <= 142.5) return 20;
      if(w > 142.5) return 40;
     if(we > 603) return 25;
   if(we > 616)
    if(we <= 665)
     if(we <= 644)
      if(we <= 631) return 34.1666666666667;
      if(we > 631)
       if(we <= 638) return 33;
       if(we > 638) return 30;
     if(we > 644)
      if(we <= 649) return 60;
      if(we > 649)
       if(we <= 657) return 40;
       if(we > 657) return 34.45;
    if(we > 665) return 30;
  if(we > 700)
   if(we <= 722)
    if(we <= 713) return 37.5;
    if(we > 713)
     if(we <= 717) return 30;
     if(we > 717) return 30.7166666666667;
   if(we > 722)
    if(t <= 1.519)
     if(we <= 735) return 35;
     if(we > 735) return 40;
    if(t > 1.519)
     if(we <= 755) return 45;
     if(we > 755) return 35;
if(p.equals("PP12289")) return 40;
if(p.equals("PP12313"))
 if(we <= 2772)
  if(w <= 235)
   if(w <= 218) return 15;
   if(w > 218)
    if(we <= 2413)
     if(we <= 2229) return 20;
     if(we > 2229) return 10;
    if(we > 2413)
     if(we <= 2568) return 15;
     if(we > 2568) return 20;
  if(w > 235) return 25;
 if(we > 2772)
  if(t <= 2.35)
   if(we <= 2841) return 15;
   if(we > 2841) return 17.5;
  if(t > 2.35) return 15;
if(p.equals("PP12369"))
 if(w <= 383) return 20;
 if(w > 383) return 15;
if(p.equals("PP12370")) return 25;
if(p.equals("PP12373")) return 20;
if(p.equals("PP12376")) return 20;
if(p.equals("PP12377"))
 if(we <= 4652) return 20;
 if(we > 4652) return 15;
if(p.equals("PP12378")) return 20;
if(p.equals("PP12383")) return 15;
if(p.equals("PP12389"))
 if(w <= 272.1) return 20;
 if(w > 272.1) return 15;
if(p.equals("PP12392")) return 20;
if(p.equals("PP12393")) return 20;
if(p.equals("PP12524")) return 30;
if(p.equals("PP12575"))
 if(t <= 1.8) return 50;
 if(t > 1.8) return 60;
if(p.equals("PP21045"))
 if(t <= 2.72)
  if(t <= 2.42) return 25;
  if(t > 2.42)
   if(we <= 3603)
    if(w <= 360.5) return 30;
    if(w > 360.5) return 20;
   if(we > 3603) return 20;
 if(t > 2.72)
  if(we <= 2022) return 35;
  if(we > 2022)
   if(we <= 3315) return 40;
   if(we > 3315) return 35;
if(p.equals("PP21082"))
 if(t <= 2.695) return 25;
 if(t > 2.695)
  if(we <= 2116)
   if(we <= 2022) return 30;
   if(we > 2022) return 35;
  if(we > 2116)
   if(we <= 2934) return 40;
   if(we > 2934) return 30;
if(p.equals("PP21092"))
 if(w <= 300) return 30;
 if(w > 300) return 40;
if(p.equals("PP21107")) return 10;
if(p.equals("PP21114"))
 if(t <= 3.515) return 50;
 if(t > 3.515) return 40;
if(p.equals("PP21208"))
 if(t <= 2.11) return 40;
 if(t > 2.11) return 1;
if(p.equals("PP21209"))
 if(t <= 3.3)
  if(w <= 38) return 45;
  if(w > 38)
   if(we <= 305)
    if(t <= 3.255) return 35;
    if(t > 3.255) return 45;
   if(we > 305)
    if(t <= 3.27)
     if(w <= 360.5) return 38.4166666666667;
     if(w > 360.5) return 20;
    if(t > 3.27) return 40;
 if(t > 3.3) return 30;
if(p.equals("PP21252"))
 if(we <= 1878) return 20;
 if(we > 1878) return 25;
if(p.equals("PP21306"))
 if(we <= 2619) return 35;
 if(we > 2619) return 10;
if(p.equals("PP21320")) return 30;
if(p.equals("PP21328")) return 20;
if(p.equals("PP21345")) return 10;
if(p.equals("PP21346")) return 15;
if(p.equals("PP21398"))
 if(w <= 251) return 15;
 if(w > 251)
  if(we <= 2426) return 30;
  if(we > 2426) return 32.5;
if(p.equals("PP21429")) return 35;
if(p.equals("PP21473")) return 20;
if(p.equals("PP21476")) return 60;
if(p.equals("PP21484")) return 30;
if(p.equals("PP21524"))
 if(t <= 1.995) return 15;
 if(t > 1.995) return 30;
if(p.equals("PP21557")) return 45;
if(p.equals("PP21577")) return 50;
if(p.equals("PP21578"))
 if(we <= 566)
  if(we <= 560) return 20;
  if(we > 560) return 25;
 if(we > 566) return 20;
if(p.equals("PP21580")) return 20;
if(p.equals("PP21601"))
 if(t <= 3.5) return 25;
 if(t > 3.5)
  if(we <= 139) return 60;
  if(we > 139) return 40;
if(p.equals("PP21606"))
 if(t <= 3.6)
  if(t <= 2.72) return 50;
  if(t > 2.72)
   if(we <= 277) return 30;
   if(we > 277)
    if(we <= 296) return 40;
    if(we > 296) return 50;
 if(t > 3.6)
  if(we <= 274)
   if(we <= 252) return 25;
   if(we > 252) return 40;
  if(we > 274)
   if(t <= 3.7925) return 40;
   if(t > 3.7925) return 60;
if(p.equals("PP21632")) return 20;
if(p.equals("PP21647")) return 25;
if(p.equals("PP21677"))
 if(we <= 210) return 30;
 if(we > 210) return 50;
if(p.equals("PP21684")) return 70;
if(p.equals("PP21753")) return 50;
if(p.equals("PP21769")) return 25;
if(p.equals("PP21782"))
 if(we <= 360) return 40;
 if(we > 360)
  if(t <= 4.2) return 50;
  if(t > 4.2)
   if(we <= 380) return 50;
   if(we > 380)
    if(we <= 559) return 30;
    if(we > 559) return 60;
if(p.equals("PP21792")) return 45;
if(p.equals("PP21794"))
 if(we <= 477) return 30;
 if(we > 477)
  if(we <= 498) return 25;
  if(we > 498)
   if(we <= 553) return 40;
   if(we > 553) return 30;
if(p.equals("PP21798"))
 if(w <= 47)
  if(t <= 4.1) return 40;
  if(t > 4.1) return 70;
 if(w > 47) return 104.55;
if(p.equals("PP21805"))
 if(we <= 678)
  if(we <= 352) return 20;
  if(we > 352) return 40;
 if(we > 678)
  if(we <= 722)
   if(we <= 705)
    if(we <= 684) return 100;
    if(we > 684) return 45;
   if(we > 705)
    if(we <= 711) return 35;
    if(we > 711)
     if(we <= 719) return 30;
     if(we > 719) return 35;
  if(we > 722)
   if(w <= 433.5) return 40;
   if(w > 433.5) return 25;
if(p.equals("PP21809")) return 70;
if(p.equals("PP21884"))
 if(we <= 467)
  if(we <= 323)
   if(we <= 292) return 70;
   if(we > 292) return 30;
  if(we > 323)
   if(we <= 400) return 45;
   if(we > 400) return 40;
 if(we > 467) return 30;
if(p.equals("PP22018")) return 1;
if(p.equals("PP22020")) return 25;
if(p.equals("PP22066"))
 if(we <= 3507) return 20;
 if(we > 3507) return 25;
if(p.equals("PP22067"))
 if(w <= 427) return 20;
 if(w > 427) return 25;
if(p.equals("PP22296"))
 if(t <= 2.72)
  if(we <= 838) return 30;
  if(we > 838) return 42.5;
 if(t > 2.72) return 20;
if(p.equals("PP22334"))
 if(w <= 51.5) return 45;
 if(w > 51.5)
  if(we <= 321) return 20;
  if(we > 321) return 25;
if(p.equals("PP22343"))
 if(w <= 67.5) return 50;
 if(w > 67.5) return 40;
if(p.equals("PP22391"))
 if(w <= 67.5) return 30;
 if(w > 67.5) return 70;
if(p.equals("PP22419")) return 40;
if(p.equals("PP22423")) return 30;
if(p.equals("PP22456")) return 25;
if(p.equals("PP22478")) return 40;
if(p.equals("PP22556")) return 50;
if(p.equals("PP22561")) return 30;
if(p.equals("PP22567")) return 1;
if(p.equals("SS11131")) return 10;
return 10.0;
}
}
