package kr.ac.pusan.bsclab.bab.assembly.utils.ssh;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
public class SshCmdSender {
	
	public void sshCmdSender(String user
			, String password
			, String host
			, int port
			, String cmd) throws JSchException, IOException, InterruptedException {
		
		JSch jsch = new JSch();
		Session session = jsch.getSession(user, host, port);
		
		session.setPassword(password);
		session.setConfig("StricHostKeyChecking", "no");
		session.connect();
		
		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(cmd);
		
		channel.setInputStream(null);
		channel.connect();
		
		((ChannelExec)channel).setErrStream(System.err);

		channel.connect();
		
		while (true) {
			
			if (channel.isClosed()) {
				System.out.println(channel.getExitStatus());
				break;
			}
			Thread.sleep(1000);
		}
	}

}
