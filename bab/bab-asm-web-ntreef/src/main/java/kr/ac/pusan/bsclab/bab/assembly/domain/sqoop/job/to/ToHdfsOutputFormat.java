package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to;

import java.util.Arrays;

public enum ToHdfsOutputFormat {
	
	TEXT_FILE, SEQUENCE_FILE;
	
	public static String getAll() {
		return Arrays.toString(ToHdfsOutputFormat.values()).replace("[", "").replace("]", "").replace(" ", "");
	}
}
