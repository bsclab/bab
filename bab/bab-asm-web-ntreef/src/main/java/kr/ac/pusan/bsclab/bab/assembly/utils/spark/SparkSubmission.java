package kr.ac.pusan.bsclab.bab.assembly.utils.spark;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SparkSubmission {
	public String action = "CreateSubmissionRequest";
	public List<String> appArgs = new ArrayList<String>();
	public String appResource = "";
	public String clientSparkVersion = "";
	public Map<String, String> environmentVariables = new LinkedHashMap<String, String>();
	public String mainClass = "";
	public Map<String, String> sparkProperties = new LinkedHashMap<String, String>();
			
	public SparkSubmission(String appReportUri
			, String appName
			, String appVersion
			, String hdfsApiUri
			, String sparkMasterUri
			, String sparkVersion
			, String sparkMainClass
			, String sparkJarUri
			, String jobId, String jobClass, String jobPath, String userId) {

		appArgs.add(appReportUri + "/" + jobId);
		appArgs.add((String) hdfsApiUri);
		appArgs.add((String) sparkMasterUri);
		appArgs.add(jobClass);
		appArgs.add(jobPath);
		appArgs.add(userId);
		appResource = sparkJarUri;
		clientSparkVersion = (String) sparkVersion;
		environmentVariables.put("SPARK_ENV_LOADED", "1");
		mainClass = sparkMainClass;
		sparkProperties.put("spark.jars", sparkJarUri);
		sparkProperties.put("spark.driver.supervise", "false");
		sparkProperties.put("spark.driver.memory", "1g");
		sparkProperties.put("spark.app.name", "(" + appName + "-" + appVersion + ") " + jobId);
		sparkProperties.put("spark.eventLog.enabled", "true");
		sparkProperties.put("spark.eventLog.dir", hdfsApiUri + "/tmp/spark-events");
		sparkProperties.put("spark.submit.deployMode", "cluster");
		sparkProperties.put("spark.master", (String) sparkMasterUri);
	}
}
