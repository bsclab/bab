package kr.ac.pusan.bsclab.bab.assembly.domain.user.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.assembly.domain.user.User;

@Repository
public class UserDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public User findOneByUsername(String username) {
		
		String sql = "SELECT no"
						+ ", username"
						+ ", password"
						+ ", name"
						+ ", enabled"
						+ ", workspace"
						+ ", dataset"
						+ " FROM user"
						+ " WHERE username=?";
		return jdbcTemplate.queryForObject(sql
											, new Object[]{username}
											, new UserRowMapper());
	}
	
	public User findOneByNo(int no) {
		
		String sql = "SELECT no"
						+ ", username"
						+ ", password"
						+ ", name"
						+ ", enabled"
						+ ", workspace"
						+ ", dataset"
						+ " FROM user"
						+ " WHERE no=?";
		return jdbcTemplate.queryForObject(sql
											, new Object[]{no}
											, new UserRowMapper());
	}
	
//	@Deprecated
//	public int save(User user) {
//		String sql = "INSERT INTO user(username"
//										+ ", password"
//										+ ", name"
//										+ ", enabled)"
//											+ " VALUES(?, ?, ?, ?)";
//		return jdbcTemplate.update(sql,  new Object[] {
//										user.getUsername()
//										, user.getPassword()
//										, user.getName()
//										, user.isEnabled()
//									});
//	}
}

class UserRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setNo(rs.getInt("no"));
		user.setName(rs.getString("name"));
		user.setPassword(rs.getString("password"));
		user.setUsername(rs.getString("username"));
		user.setEnabled(rs.getBoolean("enabled"));
		user.setWorkspace(rs.getString("workspace"));
		user.setDataset(rs.getString("dataset"));
		return user;
	} 
}
