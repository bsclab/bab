package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class InfographicController extends AbstractWebController {
	
	public static final String BASE_URL = BabWeb.BASE_URL + "/infographic";
	
    @RequestMapping(method=RequestMethod.GET,
    	path=BASE_URL)
    public ModelAndView getIndex() {
        ModelAndView view = new ModelAndView("bab/index");

        String message = this.getClass().getName();
    	view.addObject("message", message);
    	
        return view;
    }	
}
