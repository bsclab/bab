package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_WSA implements ANN_base {
	MultiLayerNetwork net;
	//MSE 500.457 - 0.25366214031
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-3.09496284e+00, 1.15318632e+00, 2.72506773e-01, -1.04720736e+00
            				, 4.41210327e+01, -4.36974168e-01, -9.76063311e-02, -7.77594209e-01
            				, -3.40355921e+00, -5.36301062e-02, -1.47332096e+00, -3.49402905e+00
            				, 3.71369600e-01, -2.21182609e+00, -1.39923716e+00, -1.74904263e+00
            				, 6.20141563e+01, -5.81452560e+00, -1.66333187e+00, -1.97240257e+00}, 
            			{-1.25616372e+00, -1.16452944e+00, -7.18296826e-01, 3.88425618e-01
        					, 4.91021919e+00, 2.78708309e-01, 1.22901094e+00, 7.87066102e-01
        					, -7.29717374e-01, -6.84415579e-01, 3.10500294e-01, 5.92116341e-02
        					, -1.08753598e+00, -6.76734090e-01, -1.57807216e-01, 7.99035907e-01
        					, 9.72567940e+00, -1.23533785e+00, -2.08657503e+00, -9.21700239e-01},
            			{1.86045408e-01, -1.62352812e+00, -2.14041695e-01, -1.58359129e-02
    						, -1.90490000e-02, -8.39804471e-01, -1.79135394e+00, 2.49220058e-01
    						, -1.60858727e+00, 8.29696730e-02, -6.55016124e-01, 7.38858163e-01
    						, 1.00760746e+00, -5.87204456e-01, 8.52150381e-01, -4.81860161e-01
    						, -1.29925883e+00, -4.54672247e-01, -5.79575785e-02, -1.46222281e+00}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.79236734, -0.18986309, -1.1862185, -0.34693843, 0.53997028, -0.3288033
            			, -0.27940392, -0.98994088, 0.20098218, -0.37780198, -0.79611456, 1.7784214
            			, -1.9052546, 1.30655098, -0.19125614, 0.05636444, 0.48160559, 2.44680071
            			, -1.21470225, -1.01573229};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.37360951,
            			0.68420666,
            			1.68076038,
            			0.90837544,
            			-0.79597181,
            			-2.11495113,
            			-0.19050372,
            			-0.66598028,
            			-0.20005621,
            			-0.33034602,
            			0.10665055,
            			0.59242487,
            			0.18492851,
            			0.28112173,
            			0.05241865,
            			1.72563314,
            			-2.05141926,
            			1.76697671,
            			-0.98298806,
            			0.35826784};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.82351786};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.4;
	}

	@Override
	public double getMaxWidth() {
		
		return 724.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 8713.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.9;
	}

	@Override
	public double getMinWidth() {
		
		return 90.0;
	}

	@Override
	public double getMinWeight() {
		
		return 990.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
