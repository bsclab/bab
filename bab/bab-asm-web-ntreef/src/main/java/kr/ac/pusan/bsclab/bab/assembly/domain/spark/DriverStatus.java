package kr.ac.pusan.bsclab.bab.assembly.domain.spark;

enum DriverStatus {
	RUNNING, WAITING
}
