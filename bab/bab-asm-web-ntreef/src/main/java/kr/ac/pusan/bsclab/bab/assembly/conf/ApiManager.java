package kr.ac.pusan.bsclab.bab.assembly.conf;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;

@Component
public class ApiManager {
	
	@Autowired
	private AppProperties ap;
//	
//	public final Map<String, Map<String, String>> APIURI = setAPIURI();
//	public final Map<String, Map<String, String>> WEBURI = setWEBURI();

	public Map<String, Map<String, String>> getWEBURI(){
		Map<String, Map<String, String>> WEBURI= new HashMap<String, Map<String, String>>();
		Map<String, String> processvisualizer = new HashMap<String, String>();
//		processvisualizer.put("renderheuristic", WEB_ROOT+"/processvisualizer/renderheuristic/");
		processvisualizer.put("renderheuristic", ap.getApiWeb()+"/processvisualizer/renderheuristic/");
		WEBURI.put("processvisualizer", processvisualizer);

		return WEBURI;
	}
	
	public Map<String, Map<String, String>> getAPIURI(){
		Map<String, Map<String, String>> API= new HashMap<String, Map<String, String>>();
		Map<String, String> analysis = new HashMap<String, String>();
		Map<String, String> model = new HashMap<String, String>();
		Map<String, String> repository = new HashMap<String, String>();
		Map<String, String> job = new HashMap<String, String>();
		Map<String, String> schedule = new HashMap<String, String>();
		Map<String, String> rservices = new HashMap<String, String>();

		String apiRoot = ap.getApiRoot();

		analysis.put("associationrule", apiRoot+"/analysis/associationrule/");
		analysis.put("socialnetwork", apiRoot+"/analysis/socialnetwork/");
		analysis.put("timegap", apiRoot+"/analysis/timegap/");
		analysis.put("mtga", apiRoot+"/analysis/mtga/");
		analysis.put("delta", apiRoot+"/analysis/delta/");
		analysis.put("taskmatrix", apiRoot+"/analysis/taskmatrix/");
		analysis.put("dottedchart", apiRoot+"/analysis/dottedchart/");
		analysis.put("kpi", apiRoot+"/analysis/ymkpi/");

		model.put("heuristic", apiRoot+"/model/heuristic/");
		model.put("logreplay", apiRoot+"/model/logreplay/");

		repository.put("import", apiRoot+"/repository/import/");
		repository.put("map", apiRoot+"/repository/map/");
		repository.put("workspace", apiRoot+"/repository/workspace/");
		repository.put("datasets", apiRoot+"/repository/datasets/");
		repository.put("mappings", apiRoot+"/repository/mappings/");
		repository.put("repositories", apiRoot+"/repository/repositories/");
		repository.put("viewmap", apiRoot+"/repository/viewmap/");
		repository.put("view", apiRoot+"/repository/view/");
		repository.put("summary", apiRoot+"/repository/summary/");
		repository.put("upload", apiRoot+"/repository/upload");
		
		job.put("list", apiRoot+"/job");
		job.put("kill", apiRoot+"/job/kill/");
		job.put("pull", apiRoot+"/job/pull/");
		job.put("queue", apiRoot+"/job/queue/");
		job.put("report", apiRoot+"/job/report/");
		job.put("services", apiRoot+"/job/services/");
		

		schedule.put("logdata", apiRoot+"/logdata/");
		schedule.put("services", apiRoot+"/job/services/");
		rservices.put("rservices", apiRoot+"/rserver/rservices/");

		API.put("analysis", analysis);
		API.put("repository", repository);
		API.put("model", model);
		API.put("job", job);
		API.put("schedule", schedule);
		API.put("rservices", rservices);

		return API;
	}

}
