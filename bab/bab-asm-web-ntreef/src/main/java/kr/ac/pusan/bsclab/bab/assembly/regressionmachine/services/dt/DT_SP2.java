package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SP2 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10244"))
 if(we <= 24750) return 25;
 if(we > 24750) return 15;
if(p.equals("PP11261")) return 20;
if(p.equals("PP11264"))
 if(t <= 2)
  if(w <= 1199)
   if(we <= 20090) return 20;
   if(we > 20090)
    if(we <= 20640)
     if(t <= 1.565) return 30;
     if(t > 1.565) return 14.4333333333333;
    if(we > 20640)
     if(we <= 21990) return 20;
     if(we > 21990) return 25;
  if(w > 1199)
   if(t <= 1.21)
    if(w <= 1230)
     if(we <= 19660) return 17.6666666666667;
     if(we > 19660) return 30;
    if(w > 1230) return 24.9;
   if(t > 1.21)
    if(w <= 1208) return 25;
    if(w > 1208)
     if(t <= 1.7) return 25;
     if(t > 1.7) return 20;
 if(t > 2)
  if(w <= 1111) return 20;
  if(w > 1111)
   if(t <= 2.465)
    if(we <= 23850)
     if(we <= 23200)
      if(we <= 22390)
       if(t <= 2.21) return 15;
       if(t > 2.21) return 20;
      if(we > 22390) return 15;
     if(we > 23200) return 20;
    if(we > 23850)
     if(we <= 24010) return 15.15;
     if(we > 24010)
      if(we <= 24840) return 15;
      if(we > 24840) return 20;
   if(t > 2.465)
    if(t <= 2.98)
     if(t <= 2.5)
      if(w <= 1141)
       if(we <= 23120) return 25;
       if(we > 23120) return 15;
      if(w > 1141) return 20;
     if(t > 2.5)
      if(we <= 24190)
       if(we <= 23630)
        if(we <= 23170)
         if(we <= 21600) return 20;
         if(we > 21600) return 15;
        if(we > 23170) return 15.8666666666667;
       if(we > 23630) return 20;
      if(we > 24190)
       if(we <= 25110)
        if(we <= 24880) return 25;
        if(we > 24880)
         if(we <= 24980) return 20;
         if(we > 24980) return 25;
       if(we > 25110) return 14.2;
    if(t > 2.98)
     if(t <= 3.24)
      if(w <= 1154) return 14.5;
      if(w > 1154) return 16.6;
     if(t > 3.24)
      if(w <= 1219) return 30;
      if(w > 1219) return 12.55;
if(p.equals("PP11287"))
 if(t <= 0.7) return 31.9333333333333;
 if(t > 0.7) return 30;
if(p.equals("PP11288")) return 10;
if(p.equals("PP11325")) return 15;
if(p.equals("PP11403"))
 if(t <= 0.7) return 105;
 if(t > 0.7) return 25;
if(p.equals("PP11435")) return 20;
if(p.equals("PP11443")) return 25;
if(p.equals("PP11539")) return 15;
if(p.equals("PP11683"))
 if(t <= 0.4)
  if(we <= 16200)
   if(we <= 16040) return 45;
   if(we > 16040) return 40;
  if(we > 16200)
   if(we <= 20300) return 55;
   if(we > 20300) return 40;
 if(t > 0.4)
  if(we <= 16150)
   if(we <= 15510) return 40;
   if(we > 15510) return 35;
  if(we > 16150) return 30;
if(p.equals("PP11910")) return 17.2;
if(p.equals("PP11917")) return 59.1;
if(p.equals("PP11991")) return 20;
if(p.equals("PP12331")) return 20;
if(p.equals("PP12541")) return 22.1833333333333;
if(p.equals("PP21105"))
 if(t <= 0.9) return 25;
 if(t > 0.9) return 20;
if(p.equals("PP21107"))
 if(w <= 1154)
  if(we <= 22860)
   if(we <= 20780)
    if(we <= 19980)
     if(t <= 3.2)
      if(w <= 1020) return 11.55;
      if(w > 1020) return 15;
     if(t > 3.2)
      if(t <= 3.5) return 10.7;
      if(t > 3.5) return 18.1;
    if(we > 19980)
     if(t <= 2.6)
      if(t <= 2.495) return 12.1;
      if(t > 2.495)
       if(we <= 20130) return 15.0833333333333;
       if(we > 20130) return 15;
     if(t > 2.6)
      if(w <= 1040) return 16.4333333333333;
      if(w > 1040)
       if(w <= 1130) return 20;
       if(w > 1130) return 12.65;
   if(we > 20780)
    if(we <= 22810) return 20;
    if(we > 22810)
     if(t <= 2.8)
      if(we <= 22840) return 25;
      if(we > 22840) return 11.5;
     if(t > 2.8) return 18.0166666666667;
  if(we > 22860) return 20;
 if(w > 1154)
  if(w <= 1213)
   if(t <= 1.83)
    if(we <= 22310) return 20;
    if(we > 22310)
     if(we <= 23900) return 25;
     if(we > 23900)
      if(we <= 24430)
       if(we <= 24350)
        if(we <= 24100) return 20;
        if(we > 24100)
         if(we <= 24250) return 16.15;
         if(we > 24250)
          if(we <= 24290) return 15;
          if(we > 24290) return 25;
       if(we > 24350)
        if(we <= 24380) return 15;
        if(we > 24380) return 20;
      if(we > 24430) return 25;
   if(t > 1.83) return 20;
  if(w > 1213)
   if(t <= 2)
    if(we <= 19220)
     if(t <= 1.5)
      if(t <= 1.3) return 15.95;
      if(t > 1.3)
       if(we <= 17322)
        if(we <= 17050) return 21.8;
        if(we > 17050) return 18.5666666666667;
       if(we > 17322) return 15;
     if(t > 1.5) return 20;
    if(we > 19220)
     if(t <= 1.5) return 21.8333333333333;
     if(t > 1.5)
      if(t <= 1.7) return 30;
      if(t > 1.7) return 12.4666666666667;
   if(t > 2)
    if(w <= 1220)
     if(t <= 2.8) return 15;
     if(t > 2.8) return 20;
    if(w > 1220)
     if(t <= 2.8)
      if(t <= 2.685) return 20;
      if(t > 2.685)
       if(we <= 24300) return 14.9833333333333;
       if(we > 24300) return 100.183333333333;
     if(t > 2.8)
      if(w <= 1242) return 40;
      if(w > 1242) return 15;
if(p.equals("PP21108"))
 if(t <= 0.8)
  if(w <= 1154) return 30;
  if(w > 1154) return 25;
 if(t > 0.8)
  if(we <= 20040) return 20;
  if(we > 20040) return 50;
if(p.equals("PP21173")) return 20;
if(p.equals("PP21180"))
 if(t <= 3.195)
  if(we <= 20510)
   if(we <= 19990)
    if(t <= 2.72) return 15.8833333333333;
    if(t > 2.72)
     if(we <= 19780) return 15;
     if(we > 19780) return 17.45;
   if(we > 19990)
    if(we <= 20430)
     if(we <= 20210)
      if(we <= 20030) return 15.2;
      if(we > 20030) return 12.1666666666667;
     if(we > 20210) return 20;
    if(we > 20430)
     if(we <= 20470) return 14.0833333333333;
     if(we > 20470) return 15;
  if(we > 20510) return 20;
 if(t > 3.195)
  if(t <= 3.99)
   if(we <= 22365) return 15;
   if(we > 22365) return 20;
  if(t > 3.99)
   if(t <= 4)
    if(we <= 20360) return 20;
    if(we > 20360)
     if(we <= 20600) return 15;
     if(we > 20600)
      if(we <= 21190) return 25;
      if(we > 21190) return 15;
   if(t > 4)
    if(w <= 1012)
     if(we <= 20790) return 25;
     if(we > 20790) return 20;
    if(w > 1012) return 20;
if(p.equals("PP21185")) return 30;
if(p.equals("PP21209")) return 20;
if(p.equals("PP21288")) return 17.6;
if(p.equals("PP21379"))
 if(w <= 975) return 25.1666666666667;
 if(w > 975) return 22.2833333333333;
if(p.equals("PP21405"))
 if(t <= 4.96)
  if(w <= 1090) return 10;
  if(w > 1090) return 13.1;
 if(t > 4.96) return 20;
if(p.equals("PP21414")) return 20;
if(p.equals("PP21419"))
 if(we <= 17979) return 25;
 if(we > 17979) return 20;
if(p.equals("PP21433"))
 if(we <= 23630) return 20;
 if(we > 23630) return 15;
if(p.equals("PP21469")) return 20;
if(p.equals("PP21486"))
 if(we <= 22490) return 15;
 if(we > 22490) return 20;
if(p.equals("PP21508")) return 15;
if(p.equals("PP21563")) return 30;
if(p.equals("PP21576")) return 37.0833333333333;
if(p.equals("PP21597")) return 47.2666666666667;
if(p.equals("PP21667")) return 30;
if(p.equals("PP21689")) return 38.2833333333333;
if(p.equals("PP21701"))
 if(we <= 13480)
  if(we <= 12359) return 10;
  if(we > 12359) return 16.5666666666667;
 if(we > 13480)
  if(w <= 1030) return 21.0666666666667;
  if(w > 1030) return 20;
if(p.equals("PP21756")) return 40;
if(p.equals("PP21833")) return 15;
if(p.equals("PP21880"))
 if(w <= 1090) return 25;
 if(w > 1090)
  if(we <= 12820) return 15;
  if(we > 12820) return 40;
if(p.equals("PP21886")) return 11.3833333333333;
if(p.equals("PP21890")) return 14.35;
if(p.equals("PP22010")) return 20;
if(p.equals("PP22020"))
 if(t <= 2.72)
  if(we <= 24100)
   if(we <= 22490) return 12.9166666666667;
   if(we > 22490) return 12.65;
  if(we > 24100)
   if(we <= 24570) return 25;
   if(we > 24570) return 14.5333333333333;
 if(t > 2.72)
  if(we <= 22970) return 15;
  if(we > 22970)
   if(we <= 23220) return 15.5666666666667;
   if(we > 23220) return 11.2833333333333;
if(p.equals("PP22104"))
 if(we <= 21210) return 15;
 if(we > 21210) return 14.2666666666667;
if(p.equals("PP22178"))
 if(w <= 1162) return 13.7833333333333;
 if(w > 1162) return 13.9166666666667;
if(p.equals("PP22204")) return 25;
if(p.equals("PP22291"))
 if(we <= 19860)
  if(we <= 19560)
   if(we <= 19490) return 11.4;
   if(we > 19490) return 13;
  if(we > 19560)
   if(we <= 19610) return 10;
   if(we > 19610) return 25;
 if(we > 19860)
  if(we <= 20400)
   if(we <= 20210) return 17.8666666666667;
   if(we > 20210) return 15;
  if(we > 20400)
   if(we <= 20490) return 15.9;
   if(we > 20490) return 20;
if(p.equals("PP22296"))
 if(t <= 2.72)
  if(we <= 23820) return 20;
  if(we > 23820)
   if(we <= 25020) return 15;
   if(we > 25020)
    if(we <= 25110) return 17.5;
    if(we > 25110) return 20;
 if(t > 2.72)
  if(w <= 1120)
   if(w <= 1095) return 20;
   if(w > 1095)
    if(we <= 23080)
     if(we <= 21000) return 10;
     if(we > 21000) return 15;
    if(we > 23080)
     if(we <= 23170) return 16;
     if(we > 23170)
      if(we <= 23230) return 10;
      if(we > 23230) return 20;
  if(w > 1120) return 25;
if(p.equals("PP22376")) return 20;
if(p.equals("PP22378")) return 40;
if(p.equals("PP22389")) return 19.8333333333333;
if(p.equals("PP22390")) return 19.8333333333333;
if(p.equals("PP22399")) return 15;
if(p.equals("PP22404")) return 30;
if(p.equals("PP22411")) return 55;
if(p.equals("PP22537")) return 40;
if(p.equals("PP22567"))
 if(we <= 24920) return 17.75;
 if(we > 24920)
  if(we <= 25150) return 28.7333333333333;
  if(we > 25150) return 20.6333333333333;
if(p.equals("PP22582")) return 25;
if(p.equals("WS21130")) return 40;
return 40.0;
}
}
