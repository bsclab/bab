package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_AN1 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-2.05394197, -1.09463978, 0.95608336, -2.26503372, -0.17214458, -0.4945173, -2.53393579, 0.2802473, -2.68244958, -2.55259466, -4.80178881, 0.91851091, -1.31810737, -0.90968931, -0.0705812, -1.5415839, -1.70692241, -1.26910412, -0.64299697, -0.27237615}, 
            			{-1.91517699, -2.20137429, -0.46297008, 0.8919071, -0.31910658, -1.21972203, -0.3186205, 1.32446003, -0.35194778, -0.77543426, 0.23035619, -1.66686475, -1.09305847, 1.06251931, 0.53774798, -0.13319419, -0.35359582, -0.9790737, -2.21895337, 0.72216731},
            			{-1.16633439, -1.61058557, 0.24406828, -0.62847763, -0.49010989, -1.0253756, 1.10827851, 0.34915438, 1.44482124, -1.13310993, -1.40661168, 0.77976292, 1.40353334, -0.48281398, -2.1415391, -0.62788934, -0.94938332, -0.62866569, -0.0343047, -0.31372547}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.54239023, -0.10114017, -0.292395, 1.22431672, -2.02256656, -0.49255687, 0.54936832, -0.60610223, 0.30620664, 0.25998706, 1.80460453, 0.01924795, 2.1718595, -0.77069241, -0.2949512, 1.63815761, -0.24526775, -0.17109714, -0.89913595, -0.86410809};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.08272194, -0.70342231, -1.37790287, 0.76312226, 0.09466705, -0.89221191, 0.85339773, -0.9981032, 1.09240067, 0.25087368, 0.43898281, -0.31768227, 0.46393874, -0.54709768, -0.64481676, 2.36584258, 0.32684284, -1.19189084, 0.26585022, -1.04856372};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.48094806};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}

	@Override
	public double getMaxThick() {
		
		return 9.9;
	}

	@Override
	public double getMaxWidth() {
		
		return 1290.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25400.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.36;
	}

	@Override
	public double getMinWidth() {
		
		return 164.0;
	}

	@Override
	public double getMinWeight() {
		
		return 353.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
	
	public static int maxBase() {
		return 50;
	}
	
	public static float getMaxWeight(int index) { // in ton
		if(index >= 0 && index < 6) // 6 bases, 35 ton, ebner
			return 35000;
		else if(index >= 6 && index < 10) // 4 bases, 75 ton, 850 c, ebner
			return 75000;
		else if(index >= 10 && index < 15) // 5 bases, 75 ton, 850 c, ebner
			return 75000;
		else if(index >= 15 && index < 26) // 11 bases, 75 ton, 750 c, ebener
			return 75000;
		else if(index >= 26 && index < 32) // 6 bases, 75 ton, 750 c, loi
			return 75000;
		else // if(index >= 32 && index < 50) // 18 bases, 75 ton, 750 c, loi
			return 75000;
		
	}
}
