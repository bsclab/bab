package kr.ac.pusan.bsclab.bab.assembly.utils.yarn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.YarnProperties;
import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;

@Component
public class YarnSubmission {

	@Autowired
	YarnProperties yp;
	
	@Autowired
	RestRequest restRequest;
	
}
