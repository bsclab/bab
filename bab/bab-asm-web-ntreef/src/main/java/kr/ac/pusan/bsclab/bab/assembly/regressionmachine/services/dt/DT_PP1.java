package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_PP1 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP11004")) return 40;
if(p.equals("PP11008"))
 if(t <= 3.6) return 30;
 if(t > 3.6)
  if(we <= 18920)
   if(we <= 18240) return 25;
   if(we > 18240) return 190;
  if(we > 18920)
   if(we <= 19800) return 35;
   if(we > 19800) return 36;
if(p.equals("PP11009"))
 if(w <= 1134) return 30;
 if(w > 1134) return 32;
if(p.equals("PP11010"))
 if(w <= 1179)
  if(we <= 15600) return 28;
  if(we > 15600)
   if(we <= 21930) return 30;
   if(we > 21930) return 37;
 if(w > 1179)
  if(t <= 4.5)
   if(t <= 4.2)
    if(we <= 18720) return 30;
    if(we > 18720) return 20;
   if(t > 4.2) return 25;
  if(t > 4.5)
   if(we <= 18430)
    if(w <= 1220)
     if(we <= 17690) return 22;
     if(we > 17690) return 25;
    if(w > 1220) return 20;
   if(we > 18430) return 35;
if(p.equals("PP11012")) return 30;
if(p.equals("PP11013")) return 30;
if(p.equals("PP11015")) return 30;
if(p.equals("PP11017")) return 30;
if(p.equals("PP11018")) return 25;
if(p.equals("PP11021"))
 if(t <= 5)
  if(we <= 12160)
   if(we <= 12060) return 32;
   if(we > 12060) return 25;
  if(we > 12160)
   if(we <= 12240) return 28;
   if(we > 12240) return 50;
 if(t > 5)
  if(w <= 1140)
   if(we <= 16660)
    if(we <= 15650) return 30;
    if(we > 15650) return 25;
   if(we > 16660) return 30;
  if(w > 1140) return 25;
if(p.equals("PP11029"))
 if(we <= 17600)
  if(w <= 1141) return 28;
  if(w > 1141) return 25;
 if(we > 17600) return 30;
if(p.equals("PP11030")) return 30;
if(p.equals("PP11033"))
 if(t <= 4.8)
  if(we <= 21640) return 30;
  if(we > 21640)
   if(we <= 21820) return 35;
   if(we > 21820) return 30;
 if(t > 4.8)
  if(we <= 21620) return 28;
  if(we > 21620) return 30;
if(p.equals("PP11036"))
 if(w <= 1206) return 40;
 if(w > 1206) return 30;
if(p.equals("PP11037"))
 if(t <= 1.61)
  if(w <= 1066.5)
   if(we <= 14970) return 40;
   if(we > 14970)
    if(we <= 15190) return 33;
    if(we > 15190) return 34;
  if(w > 1066.5) return 50;
 if(t > 1.61)
  if(we <= 17200)
   if(w <= 1179) return 45;
   if(w > 1179) return 43;
  if(we > 17200) return 40;
if(p.equals("PP11038")) return 35;
if(p.equals("PP11039"))
 if(we <= 17600)
  if(we <= 17350) return 35;
  if(we > 17350) return 45;
 if(we > 17600) return 35;
if(p.equals("PP11041")) return 35;
if(p.equals("PP11045"))
 if(w <= 1007) return 40;
 if(w > 1007) return 35;
if(p.equals("PP11046")) return 35;
if(p.equals("PP11047")) return 35;
if(p.equals("PP11048")) return 35;
if(p.equals("PP11050")) return 35;
if(p.equals("PP11052")) return 40;
if(p.equals("PP11057")) return 30;
if(p.equals("PP11058"))
 if(w <= 985)
  if(we <= 14850) return 40;
  if(we > 14850)
   if(w <= 976)
    if(we <= 14940)
     if(we <= 14880) return 45;
     if(we > 14880) return 41.25;
    if(we > 14940) return 43;
   if(w > 976)
    if(we <= 14950) return 40;
    if(we > 14950)
     if(we <= 15050) return 45;
     if(we > 15050)
      if(we <= 15080) return 40;
      if(we > 15080) return 45;
 if(w > 985)
  if(w <= 1037)
   if(w <= 1000)
    if(we <= 15180)
     if(we <= 15000) return 40;
     if(we > 15000) return 33;
    if(we > 15180)
     if(we <= 15310) return 45;
     if(we > 15310)
      if(we <= 15810) return 40;
      if(we > 15810) return 45;
   if(w > 1000)
    if(we <= 15290) return 40;
    if(we > 15290)
     if(we <= 15540) return 45;
     if(we > 15540) return 41;
  if(w > 1037)
   if(t <= 1.8)
    if(w <= 1141)
     if(we <= 15940)
      if(we <= 14780)
       if(we <= 14110) return 40;
       if(we > 14110)
        if(we <= 14390) return 45;
        if(we > 14390) return 35;
      if(we > 14780) return 45;
     if(we > 15940) return 40;
    if(w > 1141)
     if(we <= 17230) return 38;
     if(we > 17230) return 42;
   if(t > 1.8)
    if(we <= 16030)
     if(we <= 14440) return 32;
     if(we > 14440) return 30;
    if(we > 16030) return 45;
if(p.equals("PP11059")) return 35;
if(p.equals("PP11060")) return 45;
if(p.equals("PP11064")) return 35;
if(p.equals("PP11065"))
 if(w <= 1032) return 25;
 if(w > 1032) return 30;
if(p.equals("PP11067"))
 if(we <= 21740)
  if(t <= 1.4) return 40;
  if(t > 1.4) return 45;
 if(we > 21740) return 60;
if(p.equals("PP11071"))
 if(t <= 1.8)
  if(t <= 1.4)
   if(we <= 21700)
    if(we <= 20910)
     if(we <= 19510) return 60;
     if(we > 19510) return 65;
    if(we > 20910) return 80;
   if(we > 21700)
    if(we <= 21940) return 90;
    if(we > 21940) return 50;
  if(t > 1.4)
   if(we <= 22420) return 55;
   if(we > 22420)
    if(w <= 1156) return 65;
    if(w > 1156) return 60;
 if(t > 1.8)
  if(t <= 2.2)
   if(t <= 2)
    if(w <= 1022) return 20;
    if(w > 1022) return 30;
   if(t > 2) return 28;
  if(t > 2.2)
   if(we <= 9590) return 15;
   if(we > 9590)
    if(t <= 3.1) return 25;
    if(t > 3.1) return 30;
if(p.equals("PP11081")) return 35;
if(p.equals("PP11105")) return 35;
if(p.equals("PP11107"))
 if(w <= 1185) return 40;
 if(w > 1185)
  if(we <= 19280) return 30;
  if(we > 19280) return 65;
if(p.equals("PP11137")) return 37;
if(p.equals("PP11260"))
 if(we <= 20690) return 30;
 if(we > 20690) return 35;
if(p.equals("PP11261"))
 if(t <= 2.5)
  if(we <= 21040)
   if(we <= 20770) return 40;
   if(we > 20770) return 35;
  if(we > 21040) return 37;
 if(t > 2.5) return 25;
if(p.equals("PP11264"))
 if(t <= 3.7)
  if(t <= 2.6)
   if(t <= 2.1)
    if(t <= 1.8) return 35;
    if(t > 1.8) return 40;
   if(t > 2.1) return 35;
  if(t > 2.6)
   if(we <= 19970) return 30;
   if(we > 19970)
    if(w <= 1206) return 33;
    if(w > 1206) return 35;
 if(t > 3.7)
  if(t <= 4.35) return 30;
  if(t > 4.35) return 35;
if(p.equals("PP11266"))
 if(t <= 3.7) return 35;
 if(t > 3.7)
  if(w <= 1060) return 30;
  if(w > 1060) return 80;
if(p.equals("PP11287")) return 37;
if(p.equals("PP11288")) return 30;
if(p.equals("PP11294")) return 30;
if(p.equals("PP11312"))
 if(t <= 2.3)
  if(we <= 21350) return 40;
  if(we > 21350) return 45;
 if(t > 2.3) return 35;
if(p.equals("PP11351")) return 30;
if(p.equals("PP11443")) return 38;
if(p.equals("PP11539")) return 30;
if(p.equals("PP11607"))
 if(we <= 24980) return 30;
 if(we > 24980) return 40;
if(p.equals("PP11671")) return 28;
if(p.equals("PP11683"))
 if(we <= 17880)
  if(we <= 16030) return 35;
  if(we > 16030)
   if(we <= 16210) return 40;
   if(we > 16210) return 35;
 if(we > 17880)
  if(we <= 20710) return 50;
  if(we > 20710) return 100;
if(p.equals("PP11686")) return 43;
if(p.equals("PP11884"))
 if(we <= 17040) return 25;
 if(we > 17040) return 28;
if(p.equals("PP11891")) return 25;
if(p.equals("PP11892")) return 30;
if(p.equals("PP11894")) return 35;
if(p.equals("PP11910")) return 40;
if(p.equals("PP11931")) return 25;
if(p.equals("PP11932"))
 if(we <= 17770) return 35;
 if(we > 17770) return 30;
if(p.equals("PP11954")) return 45;
if(p.equals("PP11984"))
 if(t <= 3.6) return 32;
 if(t > 3.6) return 42;
if(p.equals("PP11985")) return 37;
if(p.equals("PP11988")) return 38;
if(p.equals("PP11991")) return 40;
if(p.equals("PP12011")) return 45;
if(p.equals("PP12026")) return 27;
if(p.equals("PP12035")) return 35;
if(p.equals("PP12037")) return 45;
if(p.equals("PP12040")) return 28;
if(p.equals("PP12047"))
 if(we <= 17600) return 40;
 if(we > 17600) return 35;
if(p.equals("PP12069")) return 20;
if(p.equals("PP12073")) return 25;
if(p.equals("PP12087")) return 25;
if(p.equals("PP12095"))
 if(w <= 985) return 35;
 if(w > 985)
  if(we <= 15370) return 40;
  if(we > 15370)
   if(we <= 16000)
    if(w <= 1037)
     if(w <= 1000) return 40;
     if(w > 1000)
      if(we <= 15430) return 45;
      if(we > 15430)
       if(we <= 15480) return 42;
       if(we > 15480)
        if(we <= 15510) return 66.25;
        if(we > 15510) return 45;
    if(w > 1037)
     if(we <= 15860)
      if(we <= 15540) return 40;
      if(we > 15540)
       if(we <= 15760) return 45;
       if(we > 15760) return 40;
     if(we > 15860)
      if(we <= 15920) return 41.25;
      if(we > 15920) return 42;
   if(we > 16000) return 40;
if(p.equals("PP12129")) return 32;
if(p.equals("PP12130")) return 25;
if(p.equals("PP12146"))
 if(we <= 24080)
  if(w <= 1206) return 35;
  if(w > 1206)
   if(we <= 23730) return 50;
   if(we > 23730) return 42;
 if(we > 24080)
  if(w <= 1235) return 45;
  if(w > 1235)
   if(t <= 3.2) return 35;
   if(t > 3.2) return 30;
if(p.equals("PP12152")) return 20;
if(p.equals("PP12158"))
 if(we <= 9670) return 22.5;
 if(we > 9670) return 26;
if(p.equals("PP12164")) return 30;
if(p.equals("PP12167"))
 if(we <= 20950) return 45;
 if(we > 20950) return 55;
if(p.equals("PP12178"))
 if(w <= 1042)
  if(we <= 14810) return 35;
  if(we > 14810) return 27;
 if(w > 1042)
  if(we <= 16150) return 25;
  if(we > 16150) return 35;
if(p.equals("PP12183")) return 32;
if(p.equals("PP12184"))
 if(we <= 16060)
  if(we <= 14620)
   if(we <= 14090)
    if(w <= 1219.5) return 33;
    if(w > 1219.5) return 30;
   if(we > 14090) return 25;
  if(we > 14620) return 30;
 if(we > 16060) return 40;
if(p.equals("PP12186"))
 if(we <= 16860)
  if(we <= 16350) return 45;
  if(we > 16350) return 30;
 if(we > 16860) return 35;
if(p.equals("PP12187")) return 70;
if(p.equals("PP12188")) return 30;
if(p.equals("PP12192")) return 35;
if(p.equals("PP12193"))
 if(we <= 15070) return 30;
 if(we > 15070)
  if(we <= 17050) return 35;
  if(we > 17050) return 40;
if(p.equals("PP12194")) return 35;
if(p.equals("PP12195")) return 35;
if(p.equals("PP12196"))
 if(we <= 16720) return 35;
 if(we > 16720)
  if(we <= 16920) return 33;
  if(we > 16920) return 30;
if(p.equals("PP12197"))
 if(we <= 13540) return 25;
 if(we > 13540) return 22;
if(p.equals("PP12198"))
 if(w <= 1189)
  if(we <= 11950)
   if(w <= 1182)
    if(we <= 11770) return 25;
    if(we > 11770) return 20;
   if(w > 1182)
    if(we <= 10830) return 25;
    if(we > 10830)
     if(we <= 11800)
      if(we <= 11560)
       if(we <= 11090) return 30;
       if(we > 11090)
        if(we <= 11470) return 25;
        if(we > 11470) return 30;
      if(we > 11560) return 25;
     if(we > 11800) return 30;
  if(we > 11950) return 25;
 if(w > 1189) return 30;
if(p.equals("PP12199")) return 30;
if(p.equals("PP12201"))
 if(we <= 15850) return 35;
 if(we > 15850) return 32;
if(p.equals("PP12202")) return 30;
if(p.equals("PP12205")) return 30;
if(p.equals("PP12206")) return 30;
if(p.equals("PP12211"))
 if(we <= 18970)
  if(we <= 16160) return 30;
  if(we > 16160) return 25;
 if(we > 18970) return 40;
if(p.equals("PP12213")) return 30;
if(p.equals("PP12218")) return 50;
if(p.equals("PP12221"))
 if(we <= 15200) return 105;
 if(we > 15200) return 30;
if(p.equals("PP12222")) return 32.5;
if(p.equals("PP12226")) return 35;
if(p.equals("PP12227")) return 30;
if(p.equals("PP12228")) return 32;
if(p.equals("PP12229")) return 30;
if(p.equals("PP12230")) return 40;
if(p.equals("PP12231"))
 if(t <= 2.6) return 37;
 if(t > 2.6)
  if(w <= 1217) return 130;
  if(w > 1217) return 110;
if(p.equals("PP12233")) return 40;
if(p.equals("PP12234")) return 35;
if(p.equals("PP12235")) return 30;
if(p.equals("PP12237")) return 30;
if(p.equals("PP12238")) return 30;
if(p.equals("PP12239")) return 40;
if(p.equals("PP12245")) return 30;
if(p.equals("PP12247")) return 38;
if(p.equals("PP12248")) return 30;
if(p.equals("PP12249")) return 35;
if(p.equals("PP12251")) return 35;
if(p.equals("PP12252")) return 53;
if(p.equals("PP12253")) return 30;
if(p.equals("PP12254")) return 40;
if(p.equals("PP12256"))
 if(we <= 16540) return 40;
 if(we > 16540) return 35;
if(p.equals("PP12257"))
 if(t <= 2.6)
  if(t <= 2.3) return 35;
  if(t > 2.3) return 28;
 if(t > 2.6)
  if(w <= 1182)
   if(we <= 13250) return 25;
   if(we > 13250) return 27;
  if(w > 1182)
   if(we <= 12550)
    if(we <= 12030)
     if(we <= 11800)
      if(we <= 11610)
       if(we <= 11400) return 25;
       if(we > 11400) return 30;
      if(we > 11610)
       if(we <= 11680) return 25;
       if(we > 11680)
        if(we <= 11740) return 26;
        if(we > 11740) return 25;
     if(we > 11800)
      if(we <= 11940) return 30;
      if(we > 11940) return 27;
    if(we > 12030) return 25;
   if(we > 12550) return 28;
if(p.equals("PP12263")) return 35;
if(p.equals("PP12267"))
 if(we <= 15520) return 32;
 if(we > 15520) return 30;
if(p.equals("PP12269")) return 35;
if(p.equals("PP12275"))
 if(we <= 11930)
  if(w <= 1182) return 15;
  if(w > 1182)
   if(we <= 11680) return 25;
   if(we > 11680) return 26;
 if(we > 11930)
  if(t <= 2.8)
   if(we <= 12490)
    if(we <= 11990) return 27;
    if(we > 11990) return 30;
   if(we > 12490) return 27;
  if(t > 2.8)
   if(we <= 13250) return 25;
   if(we > 13250) return 27;
if(p.equals("PP12282")) return 130;
if(p.equals("PP12283")) return 30;
if(p.equals("PP12284")) return 170;
if(p.equals("PP12285")) return 35;
if(p.equals("PP12286")) return 20;
if(p.equals("PP12287")) return 37;
if(p.equals("PP12289")) return 30;
if(p.equals("PP12297")) return 130;
if(p.equals("PP12302")) return 35;
if(p.equals("PP12306")) return 30;
if(p.equals("PP12307")) return 45;
if(p.equals("PP12308")) return 30;
if(p.equals("PP12313"))
 if(t <= 2.8) return 35;
 if(t > 2.8) return 30;
if(p.equals("PP12315")) return 35;
if(p.equals("PP12319")) return 30;
if(p.equals("PP12324"))
 if(we <= 12720)
  if(we <= 12250) return 30;
  if(we > 12250) return 35;
 if(we > 12720) return 30;
if(p.equals("PP12325")) return 37;
if(p.equals("PP12326"))
 if(we <= 15600) return 35;
 if(we > 15600) return 32;
if(p.equals("PP12327"))
 if(we <= 15670) return 35;
 if(we > 15670)
  if(we <= 15810) return 30;
  if(we > 15810) return 40;
if(p.equals("PP12328"))
 if(we <= 15690) return 35;
 if(we > 15690) return 25;
if(p.equals("PP12329")) return 37;
if(p.equals("PP12332"))
 if(we <= 16980) return 35;
 if(we > 16980) return 40;
if(p.equals("PP12336")) return 30;
if(p.equals("PP12337")) return 30;
if(p.equals("PP12340"))
 if(we <= 16810) return 35;
 if(we > 16810) return 40;
if(p.equals("PP12342"))
 if(we <= 15710) return 30;
 if(we > 15710) return 40;
if(p.equals("PP12344")) return 30;
if(p.equals("PP12349")) return 35;
if(p.equals("PP12352")) return 105;
if(p.equals("PP12353")) return 35;
if(p.equals("PP12354")) return 30;
if(p.equals("PP12355")) return 40;
if(p.equals("PP12357"))
 if(t <= 2.3) return 30;
 if(t > 2.3)
  if(we <= 16470) return 28;
  if(we > 16470) return 40;
if(p.equals("PP12358")) return 38;
if(p.equals("PP12359")) return 105;
if(p.equals("PP12360"))
 if(we <= 15550) return 35;
 if(we > 15550) return 30;
if(p.equals("PP12361")) return 40;
if(p.equals("PP12362")) return 30;
if(p.equals("PP12363")) return 35;
if(p.equals("PP12364")) return 30;
if(p.equals("PP12366")) return 45;
if(p.equals("PP12369"))
 if(t <= 3.7) return 30;
 if(t > 3.7) return 25;
if(p.equals("PP12370")) return 35;
if(p.equals("PP12373")) return 35;
if(p.equals("PP12376")) return 80;
if(p.equals("PP12377")) return 35;
if(p.equals("PP12378")) return 80;
if(p.equals("PP12383")) return 30;
if(p.equals("PP12384")) return 35;
if(p.equals("PP12388")) return 33;
if(p.equals("PP12389")) return 30;
if(p.equals("PP12390")) return 30;
if(p.equals("PP12392")) return 30;
if(p.equals("PP12393")) return 30;
if(p.equals("PP12394"))
 if(t <= 3.4) return 33;
 if(t > 3.4) return 25;
if(p.equals("PP12395")) return 30;
if(p.equals("PP12399")) return 30;
if(p.equals("PP12401"))
 if(we <= 15600) return 30;
 if(we > 15600)
  if(we <= 22900) return 42.5;
  if(we > 22900) return 44;
if(p.equals("PP12408")) return 35;
if(p.equals("PP12409")) return 30;
if(p.equals("PP12410")) return 30;
if(p.equals("PP12411")) return 33;
if(p.equals("PP12413")) return 35;
if(p.equals("PP12414")) return 35;
if(p.equals("PP12422")) return 30;
if(p.equals("PP12423")) return 30;
if(p.equals("PP12424")) return 30;
if(p.equals("PP12428")) return 35;
if(p.equals("PP12429")) return 35;
if(p.equals("PP12430")) return 30;
if(p.equals("PP12431")) return 30;
if(p.equals("PP12433")) return 30;
if(p.equals("PP12436"))
 if(we <= 13210)
  if(w <= 1135) return 28;
  if(w > 1135)
   if(we <= 12160) return 30;
   if(we > 12160) return 33;
 if(we > 13210) return 30;
if(p.equals("PP12437"))
 if(we <= 16720) return 35;
 if(we > 16720)
  if(we <= 16850) return 33;
  if(we > 16850) return 30;
if(p.equals("PP12438")) return 40;
if(p.equals("PP12439")) return 25;
if(p.equals("PP12440"))
 if(t <= 2.3) return 37;
 if(t > 2.3) return 30;
if(p.equals("PP12443")) return 30;
if(p.equals("PP12444")) return 38;
if(p.equals("PP12445"))
 if(we <= 14390) return 40;
 if(we > 14390) return 50;
if(p.equals("PP12446"))
 if(we <= 16690) return 30;
 if(we > 16690) return 27;
if(p.equals("PP12447")) return 3;
if(p.equals("PP12449")) return 25;
if(p.equals("PP12450")) return 25;
if(p.equals("PP12452"))
 if(t <= 3.7)
  if(w <= 1074)
   if(we <= 19570) return 42.5;
   if(we > 19570) return 40;
  if(w > 1074) return 25;
 if(t > 3.7)
  if(t <= 4.5)
   if(we <= 24080) return 37.5;
   if(we > 24080) return 63.5;
  if(t > 4.5)
   if(we <= 23490) return 33.5;
   if(we > 23490) return 32.5;
if(p.equals("PP12459")) return 30;
if(p.equals("PP12461"))
 if(t <= 3.2)
  if(w <= 1135)
   if(we <= 13190) return 35;
   if(we > 13190) return 30;
  if(w > 1135)
   if(we <= 12160) return 30;
   if(we > 12160) return 33;
 if(t > 3.2)
  if(w <= 1182) return 25;
  if(w > 1182)
   if(we <= 11980) return 120;
   if(we > 11980) return 28;
if(p.equals("PP12462"))
 if(t <= 3.3)
  if(w <= 1134)
   if(we <= 13210)
    if(we <= 13160) return 35;
    if(we > 13160) return 28;
   if(we > 13210) return 30;
  if(w > 1134)
   if(we <= 11610) return 33;
   if(we > 11610) return 30;
 if(t > 3.3)
  if(we <= 13000)
   if(we <= 11980) return 120;
   if(we > 11980)
    if(we <= 12150) return 30;
    if(we > 12150)
     if(we <= 12800) return 28;
     if(we > 12800) return 20;
  if(we > 13000) return 25;
if(p.equals("PP12464")) return 40;
if(p.equals("PP12466")) return 35;
if(p.equals("PP12468")) return 30;
if(p.equals("PP12469"))
 if(we <= 16540) return 30;
 if(we > 16540) return 3;
if(p.equals("PP12471")) return 25;
if(p.equals("PP12472")) return 35;
if(p.equals("PP12477")) return 27;
if(p.equals("PP12478")) return 25;
if(p.equals("PP12480")) return 26;
if(p.equals("PP12483")) return 110;
if(p.equals("PP12485")) return 40;
if(p.equals("PP12487")) return 25;
if(p.equals("PP12499")) return 35;
if(p.equals("PP12500")) return 32;
if(p.equals("PP12501")) return 35;
if(p.equals("PP12506")) return 30;
if(p.equals("PP12513")) return 35;
if(p.equals("PP12524")) return 26.5;
if(p.equals("PP12526"))
 if(w <= 1198) return 35;
 if(w > 1198) return 32;
if(p.equals("PP12528")) return 130;
if(p.equals("PP12529")) return 130;
if(p.equals("PP12531")) return 35;
if(p.equals("PP12532")) return 35;
if(p.equals("PP12533")) return 42;
if(p.equals("PP12536")) return 38;
if(p.equals("PP12537")) return 40;
if(p.equals("PP12538")) return 30;
if(p.equals("PP12539")) return 45;
if(p.equals("PP12541")) return 48;
if(p.equals("PP12544"))
 if(we <= 16640) return 40;
 if(we > 16640) return 50;
if(p.equals("PP12545"))
 if(we <= 11750) return 30;
 if(we > 11750) return 35;
if(p.equals("PP12546")) return 35;
if(p.equals("PP12558")) return 30;
if(p.equals("PP12559")) return 38;
if(p.equals("PP12563"))
 if(w <= 1182)
  if(we <= 22670) return 63;
  if(we > 22670) return 95;
 if(w > 1182)
  if(we <= 22830) return 90;
  if(we > 22830) return 135;
if(p.equals("PP12565")) return 32;
if(p.equals("PP12575")) return 38;
if(p.equals("PP12578")) return 36;
if(p.equals("PP12598"))
 if(we <= 24640) return 70;
 if(we > 24640) return 50;
if(p.equals("PP21092"))
 if(we <= 15710) return 40;
 if(we > 15710)
  if(we <= 18480) return 35;
  if(we > 18480)
   if(we <= 18685) return 30;
   if(we > 18685) return 40;
if(p.equals("PP21105")) return 40;
if(p.equals("PP21108")) return 40;
if(p.equals("PP21188")) return 55;
if(p.equals("PP21208")) return 28;
if(p.equals("PP21292")) return 30;
if(p.equals("PP21306"))
 if(we <= 22670) return 40;
 if(we > 22670) return 32;
if(p.equals("PP21320")) return 80;
if(p.equals("PP21336")) return 30;
if(p.equals("PP21391")) return 38;
if(p.equals("PP21394")) return 25;
if(p.equals("PP21398")) return 35;
if(p.equals("PP21408")) return 38;
if(p.equals("PP21420")) return 45;
if(p.equals("PP21702"))
 if(we <= 15970) return 40;
 if(we > 15970)
  if(we <= 16090) return 45;
  if(we > 16090) return 30;
if(p.equals("PP21754")) return 40;
if(p.equals("PP21911"))
 if(we <= 16030) return 30;
 if(we > 16030) return 40;
if(p.equals("PP21913")) return 27;
if(p.equals("PP21922")) return 30;
if(p.equals("PP21923")) return 45;
if(p.equals("PP22213")) return 58;
if(p.equals("PP22262")) return 35;
if(p.equals("PP22263")) return 35;
if(p.equals("PP22264")) return 35;
if(p.equals("PP22341")) return 30;
if(p.equals("PP22342")) return 30;
if(p.equals("PP22416")) return 37;
if(p.equals("PP22481")) return 40;
if(p.equals("PP22482")) return 40;
if(p.equals("PP22498")) return 37;
if(p.equals("PP22505")) return 45;
if(p.equals("PP22517")) return 40;
if(p.equals("PP22539"))
 if(we <= 23600) return 30;
 if(we > 23600) return 33;
if(p.equals("WS11039")) return 45;
if(p.equals("WS21130")) return 35;
return 35.0;
}
}
