package kr.ac.pusan.bsclab.bab.assembly.ws.service;

import org.springframework.beans.factory.annotation.Autowired;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.factory.dao.LogDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueueFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.JobFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfigurationFactory;
import kr.ac.pusan.bsclab.bab.assembly.job.JobManager;

public abstract class AbstractService {

	@Autowired
	LogDao logDao;

	@Autowired
	AppProperties ap;
	
	@Autowired
	JobManager jobManager;
	
	@Autowired
	JobQueueFactory jobQueueFactory;
	
	@Autowired
	JobFactory jf;
	
	@Autowired
	JobConfigurationFactory jcf;
	
	

}
