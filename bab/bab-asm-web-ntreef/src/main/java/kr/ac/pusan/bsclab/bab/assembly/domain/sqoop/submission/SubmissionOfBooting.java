package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubmissionOfBooting extends Submission {
	
	@JsonProperty("from-schema")
	Map<String, Object> fromSchema;
	
	@JsonProperty("to-schema")
	Map<String, Object> toSchema;

}
