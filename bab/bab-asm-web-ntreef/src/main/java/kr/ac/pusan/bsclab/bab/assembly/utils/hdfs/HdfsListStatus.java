package kr.ac.pusan.bsclab.bab.assembly.utils.hdfs;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class HdfsListStatus {
	public List<HdfsFileStatus> FileStatus;
	
	@JsonIgnore
	private List<FileStatus> hdfsFileStatus;
	
	public List<FileStatus> toFileStatus() {
		if (hdfsFileStatus == null && FileStatus != null) {
			hdfsFileStatus = new ArrayList<FileStatus>();
			for (HdfsFileStatus hfs : FileStatus) {
				hdfsFileStatus.add(hfs.toFileStatus());
			}
		}
		return hdfsFileStatus;
	}
}
