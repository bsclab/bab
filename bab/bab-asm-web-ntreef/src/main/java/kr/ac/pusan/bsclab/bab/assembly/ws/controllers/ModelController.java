package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.io.FileReader;
import java.math.BigDecimal;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

//import kr.ac.pusan.bsclab.bab.assembly.GraphViz;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.EventListModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.LogReplayConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.LogReplayModel;

import org.apache.commons.codec.digest.DigestUtils;

@Controller
public class ModelController extends AbstractServiceController {

	public static final String BASE_URL = BabWebService.BASE_URL + "/model";

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/heuristic/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<HeuristicModel> postHeuristic(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt
			,HeuristicMinerJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
			
		try {
			
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			
			if (config == null) {
				config = new HeuristicMinerJobConfiguration();
			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, logDao.getChecksum(dc.getTimestamp(sdt), dc.getTimestamp(edt))));
//			}
			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<HeuristicModel> babResponse = this.submitJob("ModelHeuristicJob", "hmodel", workspaceId,
					datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), HeuristicModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * credit : http://stackoverflow.com/a/30929419/1843755
	 */
	public static float round(double d, int decimalPlace) {
		return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/computelayoutheuristic/{workspaceId}/{datasetId}/{repositoryId}")
	public @ResponseBody Object postComputeLayoutHeuristic(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,HeuristicMinerJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new HeuristicMinerJobConfiguration();
			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash));
//			}
			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<HeuristicModel> babResponse = this.submitJob("ModelHeuristicJob", "hmodel", workspaceId,
					datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), HeuristicModel.class);

			/*
			 * Nashorn : javasript on JVM (javascript engine only, no DOM!)
			 * http://winterbe.com/posts/2014/04/05/java8-nashorn-tutorial/
			 * https://blog.codecentric.de/en/2014/06/project-nashorn-javascript
			 * -jvm-polyglott/
			 */
			ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
			Bindings engineScope = engine.getBindings(ScriptContext.ENGINE_SCOPE);
			engineScope.put("window", engineScope);
			engine.eval(new FileReader("src/main/resources/static/bower_components/lodash/lodash.min.js"));
			engine.eval(
					new FileReader("src/main/resources/static/bower_components/graphlib/dist/graphlib.core.min.js"));
			engine.eval(new FileReader("src/main/resources/static/bower_components/dagre/dist/dagre.core.min.js"));
			engine.eval(new FileReader("src/main/resources/static/bab/vue-components/processmodel/lib/Nodee.js"));
			engine.eval(new FileReader("src/main/resources/static/bab/vue-components/processmodel/lib/Nodees.js"));
			engine.eval(new FileReader("src/main/resources/static/bab/vue-components/processmodel/lib/Arc.js"));
			engine.eval(new FileReader("src/main/resources/static/bab/vue-components/processmodel/lib/Arcs.js"));
			engine.eval(new FileReader(
					"src/main/resources/static/bab/vue-components/processmodel/lib/ProcessModelDagre.js"));

			Invocable invocable = (Invocable) engine;
			Object result = invocable.invokeFunction("computeDagreLayout", babResponse.getResponse().getNodes(),
					babResponse.getResponse().getArcs());

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
//			+ "/graphstreamheuristic/{workspaceId}/{datasetId}/{repositoryId}")
//	public @ResponseBody String postGraphstreamHeuristic(@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "sdt") String sdt,
//			@PathVariable(value = "edt") String edt, HeuristicMinerJobConfiguration config,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		try {
//			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
//					, dc.getTimestamp(edt));
//			
//			if (config == null) {
//				config = new HeuristicMinerJobConfiguration();
//			}
////			if (config.getRepositoryURI() == null) {
////				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, repositoryId));
////			}
//			ObjectMapper om = new ObjectMapper();
//			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
//					+ om.writeValueAsString(config));
//			String jobConfig = om.writeValueAsString(config);
//			Response<HeuristicModel> babResponse = this.submitJob("ModelHeuristicJob", "hmodel", workspaceId,
//					datasetId, resourceDataHash, jobConfig
//					, dc.getTimestamp(sdt), dc.getTimestamp(edt), HeuristicModel.class);
//
//			System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
//			System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
//			// Graph graph = new SingleGraph("Tutorial 1");
//			DefaultGraph graph = new DefaultGraph("my beautiful graph");
//
//			String nodeName = "";
//			int count = 0;
//			for (Node node : babResponse.getResponse().getNodes().values()) {
//				count++;
//				nodeName += String.valueOf(count) + ". " + node.getLabel() + "\n";
//				graph.addNode(node.getLabel());
//			}
//
//			for (Arc arc : babResponse.getResponse().getArcs().values()) {
//				graph.addEdge(arc.getSource() + "-" + arc.getTarget(), arc.getSource(), arc.getTarget());
//			}
//
//			for (org.graphstream.graph.Node n : graph) {
//				n.addAttribute("ui.label", n.getId());
//			}
//
//			for (Edge e : graph.getEachEdge()) {
//				// e.addAttribute("ui.label", e.getId());
//			}
//
//			graph.addAttribute("ui.stylesheet", styleSheet);
//
//			FileSinkImages pic = new FileSinkImages(OutputType.jpg, Resolutions.HD720);
//			pic.setStyleSheet(styleSheet);
//			pic.setOutputPolicy(OutputPolicy.BY_LAYOUT_STEP);
//			pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);
//
//			pic.writeAll(graph,
//					"target/classes/static/images/" + workspaceId + "_" + datasetId + "_" + resourceDataHash + ".png");
//			return nodeName;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/logreplay/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<LogReplayModel> postLogReplay(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			LogReplayConfiguration config,
//			ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			
			if (config == null) {
				config = new LogReplayConfiguration();
			}
			
			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<LogReplayModel> babResponse = this.submitJob("ModelLogReplayJob", "lrmodel", workspaceId,
					datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), LogReplayModel.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/logreplay/{workspaceId}/{datasetId}/{resourceDataHash}/{configurationHash}/{part}")
	public @ResponseBody Response<EventListModel> postLogReplay(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "resourceDataHash") String resourceDataHash,
			@PathVariable(value = "configurationHash") String configurationHash,
//			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "part") String part, 
			LogReplayConfiguration config, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
		try {
//			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
//					, dc.getTimestamp(edt));
			
			Response<EventListModel> babResponse = new Response<EventListModel>();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			String filePath = webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + configurationHash
					+ "/" + part + ".lrmodel";
			String result = webHdfs.openAsTextFile(filePath);
			EventListModel eventListModel = mapper.readValue(result, EventListModel.class);
			babResponse.setResponse(eventListModel);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected static String styleSheet = "node {" + "fill-color: white;" + "size-mode: dyn-size;"
			+ "shape: rounded-box;" + "stroke-mode: plain;" + "stroke-color: black;" + "padding: 3px, 2px;" + "}"
			+ "node.marked {" + "fill-color: red;" + "shape: cubic-curve;" + "arrow-shape: arrow;" + "}";
	
	
	
	
//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
//			+ "/graphvizheuristic/{workspaceId}/{datasetId}/{repositoryId}")
//	public @ResponseBody Response<HeuristicModel> postGraphvizHeuristic(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, HeuristicMinerJobConfiguration config,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		try {
//			if (config == null) {
//				config = new HeuristicMinerJobConfiguration();
//			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPathOnly(workspaceId, datasetId, repositoryId));
//			}
//			ObjectMapper mapper = new ObjectMapper();
//			String configJson = mapper.writeValueAsString(config);
//			Response<HeuristicModel> babResponse = this.submitJob("ModelHeuristicJob", "hmodel", workspaceId, datasetId,
//					repositoryId, configJson, HeuristicModel.class);
//
//			String executable = "";
//			String tempDir = "";
//
//			// check OS
//			// http://stackoverflow.com/questions/14288185/detecting-windows-or-linux
//			if (SystemUtils.IS_OS_WINDOWS) {
//				executable = "C:/Program Files (x86)/Graphviz2.38/bin/dot.exe";
//				tempDir = "C:/tmp/";
//			} else if (SystemUtils.IS_OS_LINUX) {
//				executable = "/usr/local/bin/dot";
//				tempDir = "/tmp/";
//			}
//
//			long startTime = System.nanoTime();
//
//			GraphViz gv = new GraphViz(executable, tempDir);
//			gv.addln(gv.start_graph());
//
//			/*
//			 * graphviz label
//			 * http://stackoverflow.com/questions/1494492/graphviz-how-to-go-
//			 * from-dot-to-a-graph
//			 */
//			Integer countNode = 1;
//			Map<String, Integer> nodes = new HashMap<String, Integer>();
//			for (Node node : babResponse.getResponse().getNodes().values()) {
//				nodes.put(node.getLabel(), countNode);
//				// String label = "\"" + node.getLabel() + "\"";
//				String label = String.valueOf(countNode);
//				// 4 [label="{<f0> PP (complete)|<f1> 2016}" shape=Mrecord]
//				String subLabel = String.valueOf(node.getFrequency().getAbsolute());
//				String options = "[label=\"{<f0>" + node.getLabel() + "|<f1>" + subLabel + "}\" shape=Mrecord]";
//				gv.addln(label + " " + options + ";");
//				countNode++;
//			}
//
//			for (Arc arc : babResponse.getResponse().getArcs().values()) {
//				// String source = "\"" + arc.getSource() + "\"";
//				// String destination = "\"" + arc.getTarget() + "\"";
//				String source = String.valueOf(nodes.get(arc.getSource()));
//				String destination = String.valueOf(nodes.get(arc.getTarget()));
//				String options = "[label=<" + String.valueOf(arc.getFrequency().getAbsolute()) + "<br/>"
//						+ String.valueOf(round(arc.getDependency(), 2)) + ">]";
//				gv.addln(source + " -> " + destination + " " + options + ";");
//			}
//			gv.addln(gv.end_graph());
//
//			// gv.increaseDpi(); // 106 dpi
//
//			String type = "svg"; // gif / dot / fig / pdf / ps / svg / png /
//									// plain
//			String repesentationType = "dot"; // dot / neato fdp / sfdp / twopi
//												// / circo
//
//			File out = new File(tempDir + "out" + workspaceId + "-" + datasetId + "-" + repositoryId + "." + type);
//			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, repesentationType), out);
//
//			long stopTime = System.nanoTime();
//
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
}
