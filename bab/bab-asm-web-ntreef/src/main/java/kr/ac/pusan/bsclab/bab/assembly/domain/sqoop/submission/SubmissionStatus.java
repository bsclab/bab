package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission;

public enum SubmissionStatus {
	BOOTING, SUCCEEDED, FAILURE_ON_SUBMIT

}
