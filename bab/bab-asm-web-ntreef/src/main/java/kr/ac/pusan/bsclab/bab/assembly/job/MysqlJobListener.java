package kr.ac.pusan.bsclab.bab.assembly.job;

import org.springframework.beans.factory.annotation.Autowired;

import kr.ac.pusan.bsclab.bab.assembly.domain.info.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.Report;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.dao.JobDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.dao.ReportDao;


public class MysqlJobListener implements JobListener {
	
	@Autowired
	JobDao jobDao;
	
	@Autowired
	ReportDao reportDao;

	@Override
	public int save(Job job) {
		return jobDao.save(job);
	}

	@Override
	public int updateByJobId(Job job) {
		return jobDao.updateByJobId(job);
	}

	@Override
	public int updateByNo(Job job) {
		return jobDao.updateByNo(job);
	}

	@Override
	public int save(Report report) {
		return reportDao.save(report);
	}

}
