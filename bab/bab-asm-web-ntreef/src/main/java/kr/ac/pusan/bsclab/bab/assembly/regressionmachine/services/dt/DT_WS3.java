package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_WS3 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CR21059")) return 70;
if(p.equals("CR21060")) return 43.3333333333333;
if(p.equals("CR21061")) return 40;
if(p.equals("CR21062")) return 70;
if(p.equals("CR21063")) return 43.3333333333333;
if(p.equals("CR21065")) return 50;
if(p.equals("CR21066")) return 60;
if(p.equals("CR21067")) return 60;
if(p.equals("CR21068")) return 35;
if(p.equals("CR31004")) return 70;
if(p.equals("CR31005")) return 65;
if(p.equals("CR31006")) return 60;
if(p.equals("CR31007")) return 50;
if(p.equals("CR31008")) return 65;
if(p.equals("CR31009")) return 70;
if(p.equals("CR31010")) return 65;
if(p.equals("CR31011")) return 50;
if(p.equals("OW11009")) return 70;
if(p.equals("PP11008"))
 if(t <= 3.6) return 60;
 if(t > 3.6)
  if(w <= 189.5) return 70;
  if(w > 189.5) return 35;
if(p.equals("PP11037")) return 60;
if(p.equals("PP11058"))
 if(we <= 4810)
  if(w <= 321.8)
   if(w <= 306) return 30;
   if(w > 306) return 40;
  if(w > 321.8)
   if(w <= 339) return 35;
   if(w > 339) return 50;
 if(we > 4810)
  if(w <= 339) return 60;
  if(w > 339)
   if(w <= 345) return 30;
   if(w > 345)
    if(t <= 1.8)
     if(we <= 5098) return 35;
     if(we > 5098) return 60;
    if(t > 1.8) return 40;
if(p.equals("PP11260"))
 if(w <= 184.2)
  if(t <= 1.9)
   if(t <= 1.8) return 80;
   if(t > 1.8) return 95;
  if(t > 1.9)
   if(t <= 3.69)
    if(w <= 75.5) return 50;
    if(w > 75.5) return 80;
   if(t > 3.69) return 70;
 if(w > 184.2)
  if(w <= 336)
   if(w <= 198)
    if(t <= 2.095) return 105;
    if(t > 2.095) return 81.4333333333333;
   if(w > 198)
    if(w <= 247.5) return 80;
    if(w > 247.5) return 70;
  if(w > 336)
   if(t <= 2.41) return 45;
   if(t > 2.41) return 50;
if(p.equals("PP11261"))
 if(we <= 6909)
  if(t <= 2.25) return 55;
  if(t > 2.25)
   if(we <= 5204) return 60;
   if(we > 5204) return 55;
 if(we > 6909)
  if(w <= 1020) return 50;
  if(w > 1020)
   if(we <= 18142) return 40;
   if(we > 18142)
    if(we <= 18625)
     if(we <= 18384) return 37;
     if(we > 18384) return 37.5;
    if(we > 18625)
     if(we <= 19028) return 45;
     if(we > 19028) return 40;
if(p.equals("PP11264"))
 if(we <= 1904)
  if(w <= 152)
   if(w <= 144)
    if(w <= 120)
     if(w <= 94.3) return 70;
     if(w > 94.3)
      if(t <= 1.54) return 90;
      if(t > 1.54) return 60;
    if(w > 120) return 70;
   if(w > 144)
    if(we <= 1342)
     if(we <= 1274) return 60;
     if(we > 1274)
      if(we <= 1306) return 80;
      if(we > 1306) return 70.3166666666667;
    if(we > 1342)
     if(we <= 1396)
      if(we <= 1368) return 60.85;
      if(we > 1368) return 82;
     if(we > 1396)
      if(we <= 1420) return 73.0666666666667;
      if(we > 1420) return 60;
  if(w > 152)
   if(t <= 1.45)
    if(we <= 899)
     if(we <= 832) return 100;
     if(we > 832) return 80;
    if(we > 899) return 100;
   if(t > 1.45)
    if(t <= 2.25)
     if(w <= 192)
      if(we <= 1194)
       if(t <= 1.565)
        if(we <= 1038) return 80;
        if(we > 1038) return 60;
       if(t > 1.565)
        if(t <= 1.85) return 70;
        if(t > 1.85)
         if(we <= 950) return 60;
         if(we > 950) return 150;
      if(we > 1194)
       if(w <= 182)
        if(we <= 1327) return 140;
        if(we > 1327) return 70;
       if(w > 182) return 50;
     if(w > 192)
      if(t <= 1.615) return 70;
      if(t > 1.615) return 90;
    if(t > 2.25)
     if(we <= 1066) return 80;
     if(we > 1066)
      if(we <= 1342)
       if(we <= 1142) return 50;
       if(we > 1142)
        if(we <= 1294) return 70;
        if(we > 1294)
         if(we <= 1318) return 55;
         if(we > 1318) return 70;
      if(we > 1342)
       if(we <= 1412) return 75;
       if(we > 1412)
        if(t <= 3.7) return 80;
        if(t > 3.7) return 70;
 if(we > 1904)
  if(w <= 160)
   if(we <= 2849)
    if(t <= 2.595) return 70;
    if(t > 2.595)
     if(t <= 2.84)
      if(we <= 2600) return 50;
      if(we > 2600)
       if(we <= 2706)
        if(we <= 2649) return 50.6666666666667;
        if(we > 2649) return 90;
       if(we > 2706) return 70;
     if(t > 2.84) return 50;
   if(we > 2849)
    if(we <= 2980) return 60;
    if(we > 2980)
     if(we <= 3017) return 65;
     if(we > 3017) return 55;
  if(w > 160)
   if(we <= 8674)
    if(we <= 2319)
     if(t <= 1.8)
      if(we <= 2250)
       if(we <= 2198) return 40;
       if(we > 2198) return 60;
      if(we > 2250) return 25;
     if(t > 1.8)
      if(t <= 2.6) return 80;
      if(t > 2.6) return 60;
    if(we > 2319)
     if(we <= 6178)
      if(w <= 300)
       if(t <= 1.9) return 60;
       if(t > 1.9)
        if(we <= 3728)
         if(t <= 2.84)
          if(we <= 3378) return 60;
          if(we > 3378) return 50;
         if(t > 2.84)
          if(w <= 175) return 40;
          if(w > 175) return 70;
        if(we > 3728)
         if(we <= 4054)
          if(t <= 2.98) return 70;
          if(t > 2.98) return 80;
         if(we > 4054) return 60;
      if(w > 300)
       if(t <= 2.1)
        if(t <= 1.95)
         if(t <= 1.3) return 70;
         if(t > 1.3) return 50;
        if(t > 1.95) return 70;
       if(t > 2.1)
        if(t <= 2.21)
         if(we <= 3136) return 65;
         if(we > 3136) return 40;
        if(t > 2.21) return 70;
     if(we > 6178)
      if(we <= 6860) return 80;
      if(we > 6860) return 60;
   if(we > 8674)
    if(t <= 2.15) return 40;
    if(t > 2.15) return 35;
if(p.equals("PP11266"))
 if(we <= 2969)
  if(w <= 338.4)
   if(w <= 197.6)
    if(t <= 2.7) return 80;
    if(t > 2.7)
     if(we <= 1066) return 60;
     if(we > 1066)
      if(t <= 3.75) return 80;
      if(t > 3.75) return 70;
   if(w > 197.6)
    if(w <= 327)
     if(w <= 243.6) return 60;
     if(w > 243.6)
      if(t <= 1.7)
       if(we <= 2795) return 80;
       if(we > 2795) return 70;
      if(t > 1.7)
       if(t <= 2.57) return 50;
       if(t > 2.57) return 70;
    if(w > 327)
     if(t <= 1.6)
      if(we <= 2027) return 70;
      if(we > 2027) return 60;
     if(t > 1.6) return 70;
  if(w > 338.4)
   if(w <= 345)
    if(we <= 2472) return 80;
    if(we > 2472) return 90;
   if(w > 345)
    if(w <= 364)
     if(we <= 2177)
      if(we <= 2032) return 70;
      if(we > 2032) return 60;
     if(we > 2177) return 70;
    if(w > 364) return 80;
 if(we > 2969)
  if(t <= 1.5)
   if(w <= 327)
    if(we <= 3054) return 58.5666666666667;
    if(we > 3054) return 126.666666666667;
   if(w > 327)
    if(we <= 3207) return 100;
    if(we > 3207) return 70;
  if(t > 1.5)
   if(t <= 2.5)
    if(we <= 3709)
     if(we <= 3551) return 50;
     if(we > 3551) return 45;
    if(we > 3709)
     if(w <= 215.2) return 80;
     if(w > 215.2) return 50;
   if(t > 2.5)
    if(we <= 3696) return 40;
    if(we > 3696)
     if(we <= 3827) return 50;
     if(we > 3827) return 70;
if(p.equals("PP11275")) return 48.3333333333333;
if(p.equals("PP11287")) return 70;
if(p.equals("PP11288")) return 65;
if(p.equals("PP11294"))
 if(t <= 3.3) return 80;
 if(t > 3.3) return 70;
if(p.equals("PP11312"))
 if(we <= 3110)
  if(w <= 208)
   if(t <= 1.85) return 100;
   if(t > 1.85)
    if(we <= 566)
     if(we <= 526) return 80;
     if(we > 526) return 70;
    if(we > 566) return 90;
  if(w > 208)
   if(w <= 344)
    if(we <= 2642) return 80;
    if(we > 2642) return 70;
   if(w > 344) return 80;
 if(we > 3110) return 40;
if(p.equals("PP11351")) return 70;
if(p.equals("PP11403"))
 if(t <= 0.71) return 110;
 if(t > 0.71) return 70;
if(p.equals("PP11443")) return 60;
if(p.equals("PP11539")) return 65;
if(p.equals("PP11607"))
 if(we <= 2618) return 40;
 if(we > 2618)
  if(we <= 2624.5) return 70;
  if(we > 2624.5) return 60;
if(p.equals("PP11894")) return 25;
if(p.equals("PP11910")) return 70;
if(p.equals("PP11912"))
 if(w <= 217)
  if(w <= 214) return 40;
  if(w > 214) return 50;
 if(w > 217)
  if(t <= 1.85) return 70;
  if(t > 1.85) return 40;
if(p.equals("PP11917")) return 60;
if(p.equals("PP11931"))
 if(we <= 5355)
  if(t <= 1.3) return 70;
  if(t > 1.3) return 45;
 if(we > 5355) return 65;
if(p.equals("PP11932"))
 if(t <= 1.195)
  if(t <= 0.9) return 60;
  if(t > 0.9) return 70;
 if(t > 1.195)
  if(we <= 3780) return 30;
  if(we > 3780) return 50;
if(p.equals("PP11982"))
 if(we <= 5373)
  if(we <= 5232) return 40;
  if(we > 5232) return 50;
 if(we > 5373)
  if(w <= 578)
   if(we <= 5591)
    if(we <= 5501) return 40;
    if(we > 5501) return 45;
   if(we > 5591) return 35;
  if(w > 578)
   if(we <= 6271) return 40;
   if(we > 6271)
    if(we <= 6380) return 45;
    if(we > 6380) return 35;
if(p.equals("PP12002")) return 45;
if(p.equals("PP12047"))
 if(t <= 0.895)
  if(we <= 8211) return 120;
  if(we > 8211) return 70;
 if(t > 0.895)
  if(we <= 3866) return 70;
  if(we > 3866) return 60;
if(p.equals("PP12095"))
 if(w <= 333.5)
  if(we <= 5098) return 45;
  if(we > 5098) return 70;
 if(w > 333.5) return 40;
if(p.equals("PP12184")) return 80;
if(p.equals("PP12253")) return 80;
if(p.equals("PP12331")) return 35;
if(p.equals("PP12340")) return 80;
if(p.equals("PP12401"))
 if(we <= 5744) return 50;
 if(we > 5744) return 45;
if(p.equals("PP12403")) return 60;
if(p.equals("PP12452"))
 if(w <= 585) return 70;
 if(w > 585) return 60;
if(p.equals("PP12539")) return 35;
if(p.equals("PP12541")) return 70;
if(p.equals("PP12550")) return 115;
if(p.equals("PP12551")) return 115;
if(p.equals("PP12598"))
 if(we <= 2399) return 55;
 if(we > 2399)
  if(we <= 2533) return 40;
  if(we > 2533) return 55;
if(p.equals("PP21045"))
 if(t <= 3.3) return 30;
 if(t > 3.3)
  if(w <= 181) return 70;
  if(w > 181) return 60;
if(p.equals("PP21082"))
 if(t <= 3.8) return 70;
 if(t > 3.8) return 35;
if(p.equals("PP21089"))
 if(we <= 2829)
  if(t <= 2.03)
   if(w <= 376)
    if(we <= 2561)
     if(t <= 1.6)
      if(we <= 578) return 90;
      if(we > 578) return 70;
     if(t > 1.6) return 90;
    if(we > 2561) return 70;
   if(w > 376) return 80;
  if(t > 2.03)
   if(t <= 2.595)
    if(t <= 2.195)
     if(w <= 264)
      if(we <= 2008) return 80;
      if(we > 2008)
       if(we <= 2047) return 65;
       if(we > 2047) return 75;
     if(w > 264) return 80;
    if(t > 2.195)
     if(we <= 1672) return 80;
     if(we > 1672)
      if(we <= 1801) return 50;
      if(we > 1801) return 70;
   if(t > 2.595)
    if(t <= 2.895) return 70;
    if(t > 2.895)
     if(t <= 2.95) return 60;
     if(t > 2.95) return 70;
 if(we > 2829)
  if(w <= 647)
   if(t <= 2.295)
    if(t <= 1.75)
     if(we <= 2908) return 60;
     if(we > 2908)
      if(we <= 3249) return 50;
      if(we > 3249) return 60;
    if(t > 1.75) return 70;
   if(t > 2.295)
    if(we <= 3092)
     if(we <= 3038) return 65;
     if(we > 3038) return 100;
    if(we > 3092)
     if(t <= 2.815) return 60;
     if(t > 2.815) return 70;
  if(w > 647)
   if(we <= 9872) return 50;
   if(we > 9872) return 40;
if(p.equals("PP21090")) return 35;
if(p.equals("PP21092"))
 if(t <= 1.83)
  if(w <= 240)
   if(t <= 1.45)
    if(we <= 628)
     if(we <= 601) return 60;
     if(we > 601) return 80;
    if(we > 628)
     if(we <= 655)
      if(we <= 645) return 55;
      if(we > 645) return 54.35;
     if(we > 655) return 50;
   if(t > 1.45)
    if(t <= 1.735) return 70;
    if(t > 1.735)
     if(w <= 217)
      if(we <= 1294)
       if(we <= 1180) return 60;
       if(we > 1180) return 105;
      if(we > 1294)
       if(we <= 1872) return 50;
       if(we > 1872)
        if(we <= 1900) return 70;
        if(we > 1900) return 60;
     if(w > 217)
      if(we <= 1611)
       if(we <= 1361) return 85;
       if(we > 1361)
        if(we <= 1465) return 60;
        if(we > 1465) return 105;
      if(we > 1611)
       if(we <= 2047) return 50;
       if(we > 2047)
        if(we <= 2092) return 53.3333333333333;
        if(we > 2092) return 70;
  if(w > 240)
   if(t <= 1.05)
    if(w <= 334.5)
     if(we <= 2779)
      if(we <= 2649)
       if(we <= 2363) return 60;
       if(we > 2363) return 50;
      if(we > 2649) return 70;
     if(we > 2779)
      if(we <= 3152) return 50;
      if(we > 3152)
       if(we <= 3277) return 80;
       if(we > 3277) return 50;
    if(w > 334.5)
     if(we <= 3260)
      if(t <= 0.995)
       if(we <= 3118) return 70;
       if(we > 3118) return 80;
      if(t > 0.995)
       if(we <= 2760)
        if(we <= 2399) return 80;
        if(we > 2399) return 70;
       if(we > 2760)
        if(we <= 2999)
         if(we <= 2931) return 55;
         if(we > 2931) return 75;
        if(we > 2999)
         if(we <= 3138)
          if(we <= 3054) return 50;
          if(we > 3054) return 70;
         if(we > 3138) return 75;
     if(we > 3260)
      if(we <= 3477)
       if(we <= 3303) return 81.4333333333333;
       if(we > 3303) return 70;
      if(we > 3477)
       if(we <= 3564) return 76;
       if(we > 3564)
        if(we <= 3696) return 50;
        if(we > 3696) return 75;
   if(t > 1.05)
    if(w <= 306)
     if(we <= 2424)
      if(we <= 2363) return 100;
      if(we > 2363) return 70;
     if(we > 2424)
      if(t <= 1.55) return 50;
      if(t > 1.55) return 90;
    if(w > 306)
     if(we <= 3137) return 70;
     if(we > 3137) return 80;
 if(t > 1.83)
  if(w <= 331.2)
   if(t <= 2.495)
    if(t <= 2.12)
     if(t <= 2.095)
      if(we <= 3194) return 70;
      if(we > 3194) return 50;
     if(t > 2.095)
      if(we <= 1732)
       if(we <= 1652) return 80;
       if(we > 1652) return 75;
      if(we > 1732) return 50;
    if(t > 2.12)
     if(t <= 2.21)
      if(w <= 189)
       if(we <= 1763) return 80;
       if(we > 1763) return 75;
      if(w > 189)
       if(we <= 3378) return 70;
       if(we > 3378) return 80;
     if(t > 2.21)
      if(we <= 1806)
       if(we <= 1642) return 100;
       if(we > 1642)
        if(w <= 177.5) return 70;
        if(w > 177.5) return 50;
      if(we > 1806) return 70;
   if(t > 2.495)
    if(t <= 2.5)
     if(we <= 1809)
      if(we <= 1692) return 0.333333333333333;
      if(we > 1692) return 50;
     if(we > 1809)
      if(we <= 1847) return 70;
      if(we > 1847) return 56.25;
    if(t > 2.5) return 70;
  if(w > 331.2)
   if(w <= 340)
    if(w <= 338.4)
     if(we <= 2903)
      if(we <= 2424) return 70;
      if(we > 2424) return 50;
     if(we > 2903)
      if(we <= 3134)
       if(we <= 2969) return 61.25;
       if(we > 2969)
        if(we <= 3079) return 60;
        if(we > 3079) return 61.25;
      if(we > 3134) return 61.15;
    if(w > 338.4)
     if(we <= 2744)
      if(we <= 2501) return 80;
      if(we > 2501) return 60;
     if(we > 2744) return 50;
   if(w > 340)
    if(w <= 353)
     if(we <= 3340) return 70;
     if(we > 3340)
      if(t <= 2.895)
       if(we <= 3438) return 55;
       if(we > 3438)
        if(we <= 3493) return 78.1833333333333;
        if(we > 3493) return 64;
      if(t > 2.895)
       if(t <= 3.6) return 50;
       if(t > 3.6)
        if(we <= 3440) return 65;
        if(we > 3440) return 80;
    if(w > 353)
     if(w <= 647)
      if(we <= 3665)
       if(t <= 2.75)
        if(we <= 3032)
         if(we <= 2773) return 70;
         if(we > 2773) return 50;
        if(we > 3032)
         if(we <= 3115) return 70;
         if(we > 3115)
          if(we <= 3232) return 75;
          if(we > 3232) return 70;
       if(t > 2.75)
        if(we <= 3260) return 70;
        if(we > 3260) return 50;
      if(we > 3665)
       if(we <= 3728) return 70;
       if(we > 3728) return 85;
     if(w > 647)
      if(t <= 2.65) return 40;
      if(t > 2.65) return 70;
if(p.equals("PP21105"))
 if(t <= 0.9)
  if(t <= 0.71) return 60;
  if(t > 0.71) return 70;
 if(t > 0.9)
  if(w <= 1149) return 100;
  if(w > 1149)
   if(we <= 7254) return 80;
   if(we > 7254) return 60;
if(p.equals("PP21107"))
 if(w <= 647)
  if(we <= 1420)
   if(w <= 162)
    if(we <= 1271)
     if(t <= 1.2325) return 70;
     if(t > 1.2325) return 60;
    if(we > 1271) return 90;
   if(w > 162)
    if(t <= 1.815) return 105;
    if(t > 1.815)
     if(t <= 3.1)
      if(we <= 1368)
       if(we <= 1236)
        if(we <= 1180) return 50;
        if(we > 1180) return 80;
       if(we > 1236) return 45;
      if(we > 1368)
       if(we <= 1399) return 57;
       if(we > 1399) return 50;
     if(t > 3.1) return 80;
  if(we > 1420)
   if(w <= 171)
    if(we <= 2490)
     if(we <= 2280) return 70;
     if(we > 2280)
      if(we <= 2328) return 60;
      if(we > 2328) return 50;
    if(we > 2490)
     if(we <= 2619)
      if(we <= 2552) return 61.7333333333333;
      if(we > 2552) return 50;
     if(we > 2619)
      if(we <= 2652) return 67.6166666666667;
      if(we > 2652) return 70;
   if(w > 171)
    if(w <= 174)
     if(we <= 1509) return 65;
     if(we > 1509)
      if(we <= 1559) return 80;
      if(we > 1559) return 50;
    if(w > 174)
     if(w <= 281)
      if(t <= 2.98) return 70;
      if(t > 2.98)
       if(we <= 2876)
        if(w <= 250)
         if(we <= 2112) return 90;
         if(we > 2112) return 70;
        if(w > 250)
         if(we <= 2208)
          if(we <= 2019) return 70;
          if(we > 2019) return 90;
         if(we > 2208) return 70;
       if(we > 2876) return 50;
     if(w > 281)
      if(we <= 2759)
       if(t <= 2.84) return 70;
       if(t > 2.84) return 80;
      if(we > 2759) return 50;
 if(w > 647)
  if(we <= 11226)
   if(w <= 1164)
    if(w <= 1090) return 40;
    if(w > 1090)
     if(t <= 2.8)
      if(we <= 10769) return 50;
      if(we > 10769)
       if(w <= 1135) return 60;
       if(w > 1135) return 43.3333333333333;
     if(t > 2.8)
      if(t <= 3.1)
       if(we <= 10581) return 40;
       if(we > 10581) return 60;
      if(t > 3.1) return 60;
   if(w > 1164)
    if(we <= 7544)
     if(t <= 2.8)
      if(t <= 1.95) return 70;
      if(t > 1.95)
       if(t <= 2.5) return 65;
       if(t > 2.5) return 40;
     if(t > 2.8) return 30;
    if(we > 7544)
     if(t <= 1.2)
      if(w <= 1217)
       if(we <= 9338) return 40;
       if(we > 9338) return 80;
      if(w > 1217) return 60;
     if(t > 1.2) return 40;
  if(we > 11226)
   if(w <= 1203) return 40;
   if(w > 1203)
    if(t <= 2.995)
     if(we <= 15233) return 40;
     if(we > 15233)
      if(t <= 2.15) return 35;
      if(t > 2.15)
       if(we <= 16467) return 40;
       if(we > 16467) return 50;
    if(t > 2.995)
     if(we <= 15323) return 40;
     if(we > 15323) return 60;
if(p.equals("PP21108"))
 if(t <= 0.71)
  if(w <= 1203)
   if(t <= 0.705) return 40;
   if(t > 0.705) return 60;
  if(w > 1203) return 60;
 if(t > 0.71)
  if(t <= 0.8) return 90;
  if(t > 0.8)
   if(w <= 166) return 80;
   if(w > 166)
    if(we <= 1247) return 130;
    if(we > 1247) return 80;
if(p.equals("PP21116")) return 40;
if(p.equals("PP21173")) return 60;
if(p.equals("PP21180"))
 if(w <= 647)
  if(w <= 289)
   if(w <= 121) return 110;
   if(w > 121)
    if(we <= 1222) return 85;
    if(we > 1222) return 80;
  if(w > 289)
   if(we <= 2522) return 55;
   if(we > 2522) return 70;
 if(w > 647)
  if(t <= 3.99)
   if(we <= 19299) return 40;
   if(we > 19299) return 35;
  if(t > 3.99)
   if(we <= 17672)
    if(w <= 995)
     if(we <= 16985) return 50;
     if(we > 16985) return 30;
    if(w > 995) return 40;
   if(we > 17672) return 40;
if(p.equals("PP21185")) return 40;
if(p.equals("PP21187")) return 62.5;
if(p.equals("PP21188"))
 if(w <= 281)
  if(t <= 2.4) return 80;
  if(t > 2.4) return 50;
 if(w > 281) return 70;
if(p.equals("PP21208"))
 if(we <= 2750)
  if(w <= 190.6)
   if(we <= 1629)
    if(w <= 132)
     if(we <= 584) return 92.5;
     if(we > 584)
      if(we <= 623) return 100;
      if(we > 623) return 60;
    if(w > 132)
     if(t <= 1.83)
      if(w <= 163) return 75;
      if(w > 163) return 70;
     if(t > 1.83)
      if(w <= 174)
       if(we <= 1415) return 50;
       if(we > 1415)
        if(we <= 1457) return 90;
        if(we > 1457) return 70;
      if(w > 174)
       if(we <= 1582)
        if(we <= 1483) return 50;
        if(we > 1483) return 70;
       if(we > 1582) return 50;
   if(we > 1629)
    if(t <= 1.895)
     if(we <= 1760)
      if(we <= 1685) return 90;
      if(we > 1685) return 60;
     if(we > 1760) return 50;
    if(t > 1.895)
     if(t <= 1.995) return 60;
     if(t > 1.995)
      if(we <= 1755) return 70;
      if(we > 1755)
       if(we <= 1799) return 60;
       if(we > 1799) return 95;
  if(w > 190.6)
   if(t <= 1.615)
    if(w <= 274)
     if(we <= 2371)
      if(t <= 1.3)
       if(t <= 1.1) return 75;
       if(t > 1.1)
        if(t <= 1.25) return 70;
        if(t > 1.25) return 75;
      if(t > 1.3)
       if(w <= 251) return 80;
       if(w > 251) return 70;
     if(we > 2371)
      if(we <= 2505) return 70;
      if(we > 2505)
       if(we <= 2542) return 50;
       if(we > 2542)
        if(w <= 258) return 50;
        if(w > 258) return 70;
    if(w > 274)
     if(t <= 1.55)
      if(w <= 281)
       if(we <= 2648) return 50;
       if(we > 2648)
        if(we <= 2683) return 80;
        if(we > 2683) return 53.3333333333333;
      if(w > 281)
       if(w <= 291)
        if(we <= 2686) return 70;
        if(we > 2686) return 75;
       if(w > 291)
        if(t <= 1.25) return 70;
        if(t > 1.25) return 50;
     if(t > 1.55)
      if(w <= 285.2)
       if(we <= 2490)
        if(we <= 2309) return 60;
        if(we > 2309) return 70;
       if(we > 2490)
        if(we <= 2581)
         if(we <= 2535) return 58.1833333333333;
         if(we > 2535) return 70;
        if(we > 2581) return 90;
      if(w > 285.2) return 60;
   if(t > 1.615)
    if(t <= 1.9)
     if(w <= 204)
      if(we <= 1716)
       if(we <= 1611) return 50;
       if(we > 1611) return 70;
      if(we > 1716)
       if(we <= 1778) return 90;
       if(we > 1778) return 60;
     if(w > 204)
      if(we <= 2437)
       if(w <= 270)
        if(we <= 2198) return 55;
        if(we > 2198)
         if(we <= 2260) return 90;
         if(we > 2260) return 70;
       if(w > 270)
        if(w <= 340)
         if(w <= 325) return 70;
         if(w > 325) return 80;
        if(w > 340) return 70;
      if(we > 2437)
       if(we <= 2513)
        if(w <= 276) return 65;
        if(w > 276) return 60;
       if(we > 2513)
        if(w <= 325)
         if(w <= 285.2)
          if(w <= 270) return 70;
          if(w > 270) return 60;
         if(w > 285.2) return 50;
        if(w > 325)
         if(we <= 2661) return 70;
         if(we > 2661) return 50;
    if(t > 1.9)
     if(we <= 2490)
      if(t <= 2.3) return 70;
      if(t > 2.3) return 60;
     if(we > 2490)
      if(t <= 2.4) return 50;
      if(t > 2.4)
       if(w <= 359) return 80;
       if(w > 359) return 70;
 if(we > 2750)
  if(w <= 333.5)
   if(w <= 321.8)
    if(we <= 2808)
     if(t <= 1.3) return 75;
     if(t > 1.3)
      if(w <= 294)
       if(w <= 280)
        if(t <= 2.03) return 50;
        if(t > 2.03) return 70;
       if(w > 280)
        if(w <= 286.8) return 80;
        if(w > 286.8) return 70;
      if(w > 294)
       if(w <= 302.6) return 60;
       if(w > 302.6) return 50;
    if(we > 2808)
     if(we <= 2931) return 70;
     if(we > 2931)
      if(we <= 3019) return 50;
      if(we > 3019) return 70;
   if(w > 321.8) return 70;
  if(w > 333.5)
   if(t <= 1.61) return 40;
   if(t > 1.61)
    if(we <= 3235)
     if(we <= 3038)
      if(we <= 2808) return 70;
      if(we > 2808)
       if(w <= 427.5) return 55;
       if(w > 427.5) return 70;
     if(we > 3038) return 60;
    if(we > 3235)
     if(t <= 2.41)
      if(we <= 3312) return 75;
      if(we > 3312) return 50;
     if(t > 2.41) return 80;
if(p.equals("PP21209"))
 if(w <= 356) return 60;
 if(w > 356) return 50;
if(p.equals("PP21252")) return 70;
if(p.equals("PP21292"))
 if(we <= 3298) return 85;
 if(we > 3298) return 60;
if(p.equals("PP21306"))
 if(we <= 2642)
  if(w <= 251) return 60;
  if(w > 251) return 90;
 if(we > 2642) return 70;
if(p.equals("PP21379"))
 if(we <= 20684) return 50;
 if(we > 20684) return 40;
if(p.equals("PP21391")) return 60;
if(p.equals("PP21398")) return 60;
if(p.equals("PP21414"))
 if(we <= 17451)
  if(we <= 16794) return 35;
  if(we > 16794) return 40;
 if(we > 17451) return 30;
if(p.equals("PP21419")) return 40;
if(p.equals("PP21433"))
 if(w <= 323)
  if(we <= 664) return 50;
  if(we > 664) return 60;
 if(w > 323)
  if(we <= 11187)
   if(we <= 10225) return 70;
   if(we > 10225) return 35;
  if(we > 11187)
   if(we <= 11738) return 45;
   if(we > 11738) return 40;
if(p.equals("PP21469")) return 70;
if(p.equals("PP21484"))
 if(t <= 1.9)
  if(t <= 1.21)
   if(we <= 5696)
    if(we <= 4897)
     if(we <= 1060) return 70;
     if(we > 1060)
      if(w <= 155.9) return 70;
      if(w > 155.9) return 60;
    if(we > 4897)
     if(we <= 5438)
      if(we <= 5316) return 45;
      if(we > 5316) return 50;
     if(we > 5438)
      if(we <= 5532) return 40;
      if(we > 5532) return 45;
   if(we > 5696) return 55;
  if(t > 1.21)
   if(t <= 1.615) return 50;
   if(t > 1.615)
    if(we <= 752) return 100;
    if(we > 752)
     if(w <= 102.15) return 100;
     if(w > 102.15) return 40;
 if(t > 1.9) return 60;
if(p.equals("PP21486")) return 45;
if(p.equals("PP21489")) return 50;
if(p.equals("PP21508")) return 65;
if(p.equals("PP21524")) return 70;
if(p.equals("PP21551")) return 40;
if(p.equals("PP21563"))
 if(we <= 7766) return 60;
 if(we > 7766) return 70;
if(p.equals("PP21576")) return 70;
if(p.equals("PP21578")) return 60;
if(p.equals("PP21597")) return 50;
if(p.equals("PP21605")) return 40;
if(p.equals("PP21620")) return 60;
if(p.equals("PP21628")) return 60;
if(p.equals("PP21667"))
 if(we <= 7500) return 70;
 if(we > 7500) return 55;
if(p.equals("PP21701"))
 if(w <= 1100)
  if(w <= 1030) return 40;
  if(w > 1030) return 45;
 if(w > 1100) return 40;
if(p.equals("PP21702")) return 70;
if(p.equals("PP21754"))
 if(we <= 11941) return 60;
 if(we > 11941) return 40;
if(p.equals("PP21777"))
 if(we <= 3523)
  if(we <= 3337)
   if(w <= 427.5) return 55;
   if(w > 427.5) return 70;
  if(we > 3337) return 50;
 if(we > 3523)
  if(we <= 3649)
   if(we <= 3586) return 70;
   if(we > 3586) return 54;
  if(we > 3649)
   if(we <= 3711) return 51.25;
   if(we > 3711) return 80;
if(p.equals("PP21791")) return 60;
if(p.equals("PP21831")) return 30;
if(p.equals("PP21833")) return 70;
if(p.equals("PP21886")) return 35;
if(p.equals("PP21901")) return 30;
if(p.equals("PP22014")) return 30;
if(p.equals("PP22020"))
 if(t <= 2.72)
  if(we <= 939) return 75;
  if(we > 939) return 40;
 if(t > 2.72) return 60;
if(p.equals("PP22104")) return 60;
if(p.equals("PP22175")) return 30;
if(p.equals("PP22178")) return 60;
if(p.equals("PP22186")) return 30;
if(p.equals("PP22204")) return 80;
if(p.equals("PP22232"))
 if(t <= 3.02)
  if(we <= 7145)
   if(w <= 73.5) return 50;
   if(w > 73.5) return 45;
  if(we > 7145)
   if(we <= 7457) return 60;
   if(we > 7457)
    if(we <= 7544) return 37.5;
    if(we > 7544) return 40;
 if(t > 3.02) return 65;
if(p.equals("PP22236")) return 40;
if(p.equals("PP22269"))
 if(t <= 2.9) return 60;
 if(t > 2.9) return 40;
if(p.equals("PP22271"))
 if(t <= 1.21) return 80;
 if(t > 1.21)
  if(we <= 16149) return 35;
  if(we > 16149) return 40;
if(p.equals("PP22273"))
 if(t <= 1.615)
  if(t <= 1.55) return 40;
  if(t > 1.55) return 35;
 if(t > 1.615)
  if(we <= 12696) return 50;
  if(we > 12696) return 40;
if(p.equals("PP22274")) return 60;
if(p.equals("PP22296"))
 if(t <= 2.72)
  if(we <= 919) return 80;
  if(we > 919)
   if(we <= 1050) return 70;
   if(we > 1050) return 50;
 if(t > 2.72)
  if(we <= 794)
   if(we <= 737) return 80;
   if(we > 737) return 60;
  if(we > 794)
   if(we <= 893) return 70;
   if(we > 893) return 45;
if(p.equals("PP22302"))
 if(we <= 2675) return 50;
 if(we > 2675) return 70;
if(p.equals("PP22323")) return 35;
if(p.equals("PP22334"))
 if(t <= 2.21) return 50;
 if(t > 2.21) return 60;
if(p.equals("PP22374")) return 50;
if(p.equals("PP22376")) return 65;
if(p.equals("PP22378")) return 50;
if(p.equals("PP22380")) return 50;
if(p.equals("PP22389")) return 60;
if(p.equals("PP22390")) return 60;
if(p.equals("PP22399")) return 70;
if(p.equals("PP22404")) return 85;
if(p.equals("PP22406")) return 40;
if(p.equals("PP22408")) return 40;
if(p.equals("PP22409")) return 60;
if(p.equals("PP22410")) return 60;
if(p.equals("PP22435")) return 55;
if(p.equals("PP22481")) return 35;
if(p.equals("PP22482")) return 35;
if(p.equals("PP22483")) return 40;
if(p.equals("PP22517"))
 if(we <= 2654)
  if(we <= 2390)
   if(we <= 2203)
    if(t <= 0.7) return 110;
    if(t > 0.7) return 60;
   if(we > 2203)
    if(we <= 2306) return 42.5;
    if(we > 2306) return 35;
  if(we > 2390)
   if(we <= 2569) return 50;
   if(we > 2569) return 70;
 if(we > 2654)
  if(we <= 11450)
   if(we <= 10847)
    if(we <= 2741) return 55;
    if(we > 2741)
     if(we <= 2791) return 40;
     if(we > 2791)
      if(w <= 637) return 30;
      if(w > 637) return 60;
   if(we > 10847) return 80;
  if(we > 11450)
   if(w <= 638)
    if(we <= 12031) return 60;
    if(we > 12031)
     if(we <= 12218) return 65;
     if(we > 12218) return 40;
   if(w > 638) return 40;
if(p.equals("PP22537")) return 60;
if(p.equals("PP22540")) return 70;
if(p.equals("PP22541")) return 70;
if(p.equals("PP22542")) return 70;
if(p.equals("PP22567"))
 if(we <= 11715) return 40;
 if(we > 11715) return 55;
if(p.equals("WS11039")) return 80;
if(p.equals("WS21007")) return 130;
if(p.equals("WS21008")) return 70;
if(p.equals("WS21130")) return 40;
if(p.equals("WS21143"))
 if(w <= 440) return 50;
 if(w > 440) return 40;
if(p.equals("WS31007")) return 70;
return 70.0;
}
}
