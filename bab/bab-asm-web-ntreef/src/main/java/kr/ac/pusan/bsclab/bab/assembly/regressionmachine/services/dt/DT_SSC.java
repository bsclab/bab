package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SSC implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP12186"))
 if(w <= 11)
  if(we <= 37)
   if(we <= 32) return 25.4;
   if(we > 32) return 30;
  if(we > 37)
   if(we <= 41) return 28.05;
   if(we > 41) return 45;
 if(w > 11)
  if(t <= 1)
   if(w <= 12.75) return 20;
   if(w > 12.75) return 23.8;
  if(t > 1) return 24.5166666666667;
if(p.equals("PP12193"))
 if(we <= 41)
  if(w <= 11) return 31.0833333333333;
  if(w > 11)
   if(we <= 38) return 40;
   if(we > 38) return 32.4666666666667;
 if(we > 41)
  if(w <= 12.75) return 27.4666666666667;
  if(w > 12.75) return 35;
if(p.equals("PP12230"))
 if(we <= 42) return 21.4166666666667;
 if(we > 42) return 26.9166666666667;
if(p.equals("PP12247")) return 22.9166666666667;
if(p.equals("PP12251"))
 if(t <= 1)
  if(w <= 11) return 17.3333333333333;
  if(w > 11) return 24.3666666666667;
 if(t > 1)
  if(we <= 42)
   if(we <= 38) return 24.3166666666667;
   if(we > 38) return 23.4333333333333;
  if(we > 42)
   if(we <= 44) return 23.3833333333333;
   if(we > 44) return 24.5;
if(p.equals("PP12253")) return 25;
if(p.equals("PP12267"))
 if(t <= 0.8) return 29.4333333333333;
 if(t > 0.8)
  if(we <= 35) return 24.9333333333333;
  if(we > 35) return 45;
if(p.equals("PP12273")) return 27.5;
if(p.equals("PP12280")) return 23.3333333333333;
if(p.equals("PP12286"))
 if(we <= 32) return 15;
 if(we > 32) return 23.2666666666667;
if(p.equals("PP12307")) return 25.6666666666667;
if(p.equals("PP12319")) return 22.5;
if(p.equals("PP12325")) return 39.95;
if(p.equals("PP12326"))
 if(t <= 0.425) return 22.5;
 if(t > 0.425) return 25;
if(p.equals("PP12340"))
 if(we <= 49) return 15;
 if(we > 49) return 24.9833333333333;
if(p.equals("PP12358"))
 if(we <= 31) return 17;
 if(we > 31) return 26.0833333333333;
if(p.equals("PP12360")) return 28.75;
if(p.equals("PP12366")) return 29.4666666666667;
if(p.equals("PP12376")) return 36;
if(p.equals("PP12383")) return 31.9166666666667;
if(p.equals("PP12384")) return 25.8333333333333;
if(p.equals("PP12408")) return 25;
if(p.equals("PP12409")) return 28;
if(p.equals("PP12423")) return 23.5166666666667;
if(p.equals("PP12438")) return 17.8833333333333;
if(p.equals("PP12457")) return 20;
if(p.equals("PP12479")) return 30.6833333333333;
if(p.equals("PP12508")) return 26.95;
if(p.equals("PP12519")) return 28.5666666666667;
if(p.equals("PP12528")) return 17.8833333333333;
if(p.equals("PP12578")) return 25;
if(p.equals("PP21901")) return 35;
if(p.equals("PP21933")) return 29.8833333333333;
if(p.equals("PP21943")) return 51.6666666666667;
if(p.equals("PP21958"))
 if(we <= 75) return 25;
 if(we > 75) return 33.5;
if(p.equals("PP21965")) return 20;
if(p.equals("PP21972")) return 20.1833333333333;
if(p.equals("PP22014")) return 33;
if(p.equals("PP22074")) return 32.5;
if(p.equals("PP22102")) return 19.95;
if(p.equals("PP22115"))
 if(we <= 48) return 25;
 if(we > 48) return 20;
if(p.equals("PP22175")) return 22.8666666666667;
if(p.equals("PP22186")) return 30;
if(p.equals("PP22215")) return 46.3333333333333;
if(p.equals("PP22229"))
 if(we <= 33) return 22.5;
 if(we > 33) return 25;
if(p.equals("PP22236"))
 if(t <= 0.8)
  if(we <= 52) return 19.3833333333333;
  if(we > 52)
   if(we <= 56) return 12;
   if(we > 56) return 10;
 if(t > 0.8)
  if(we <= 48) return 20;
  if(we > 48) return 21;
if(p.equals("PP22243")) return 29.5333333333333;
if(p.equals("PP22263")) return 41.8333333333333;
if(p.equals("PP22264")) return 35;
if(p.equals("PP22276")) return 29.2166666666667;
if(p.equals("PP22277"))
 if(t <= 0.5)
  if(w <= 11) return 32.8166666666667;
  if(w > 11) return 18.3333333333333;
 if(t > 0.5)
  if(we <= 19) return 30;
  if(we > 19) return 28.75;
if(p.equals("PP22312"))
 if(we <= 28) return 23.3166666666667;
 if(we > 28) return 25;
if(p.equals("PP22317")) return 31.8833333333333;
if(p.equals("PP22355"))
 if(w <= 6.65) return 30.5;
 if(w > 6.65) return 21.3333333333333;
if(p.equals("PP22362")) return 18.8666666666667;
if(p.equals("PP22380")) return 41.3333333333333;
if(p.equals("PP22390"))
 if(w <= 11)
  if(we <= 35) return 24.5;
  if(we > 35) return 25;
 if(w > 11)
  if(we <= 46) return 30;
  if(we > 46) return 35;
if(p.equals("PP22406")) return 29.3833333333333;
if(p.equals("PP22408"))
 if(w <= 6.65) return 38.3333333333333;
 if(w > 6.65)
  if(we <= 25) return 26.25;
  if(we > 25) return 27.2;
if(p.equals("PP22409")) return 23.2833333333333;
if(p.equals("PP22410"))
 if(w <= 11) return 21.25;
 if(w > 11) return 31.5;
if(p.equals("PP22426")) return 39.3333333333333;
if(p.equals("PP22435")) return 28.35;
if(p.equals("PP22460")) return 27;
if(p.equals("PP22469")) return 22.8166666666667;
if(p.equals("PP22484")) return 23.5666666666667;
if(p.equals("PP22485"))
 if(we <= 20)
  if(we <= 10) return 15;
  if(we > 10) return 25.5;
 if(we > 20)
  if(we <= 24) return 31.2;
  if(we > 24) return 22.8833333333333;
if(p.equals("PP22488")) return 27.9166666666667;
if(p.equals("PP22489")) return 25.15;
if(p.equals("PP22542")) return 43.2333333333333;
if(p.equals("PP22586")) return 29.5833333333333;
if(p.equals("PP22595")) return 35.8333333333333;
return 35.8333333333333;
}
}
