package kr.ac.pusan.bsclab.bab.assembly.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-hdfs.properties")
@ConfigurationProperties(prefix="hdfs")
public class HdfsProperties {
	
	private String namenodeIp;
	private String namenodeWebPort;
	private String defaultFSPort;
	private String user;
	private String version;
	private String apiUri;
	private String httpUri;
	private String home;
	private String userHome;
	private String baseDataDirName;
	private String hdfsAuth;
	
	public String getNamenodeIp() {
		return namenodeIp;
	}
	public void setNamenodeIp(String namenodeIp) {
		this.namenodeIp = namenodeIp;
	}
	public String getNamenodeWebPort() {
		return namenodeWebPort;
	}
	public void setNamenodeWebPort(String namenodeWebPort) {
		this.namenodeWebPort = namenodeWebPort;
	}
	public String getDefaultFSPort() {
		return defaultFSPort;
	}
	public void setDefaultFSPort(String defaultFSPort) {
		this.defaultFSPort = defaultFSPort;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getApiUri() {
		return apiUri;
	}
	public void setApiUri(String apiUri) {
		this.apiUri = apiUri;
	}
	public String getHttpUri() {
		return httpUri;
	}
	public void setHttpUri(String httpUri) {
		this.httpUri = httpUri;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getUserHome() {
		return userHome;
	}
	public void setUserHome(String userHome) {
		this.userHome = userHome;
	}
	public String getBaseDataDirName() {
		return baseDataDirName;
	}
	public void setBaseDataDirName(String baseDataDirName) {
		this.baseDataDirName = baseDataDirName;
	}
	public String getHdfsAuth() {
		return hdfsAuth;
	}
	public void setHdfsAuth(String hdfsAuth) {
		this.hdfsAuth = hdfsAuth;
	}
	
	
}
