package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SP1 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();

        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{4.15394859e+01, 1.62724590e+00, 5.70900977e-01, -1.39135199e+01
            				, 1.25866985e+00, -2.74966657e-01, -2.22247076e+00, 1.64014369e-01
            				, -3.05487990e-01, -2.39325449e-01, -4.70160782e-01, 2.02402696e-01
            				, -1.84920692e+01, -1.03670061e+00, 5.18309059e+01, -1.57538682e-01
            				, 7.75621176e-01, -3.14182401e-01, -3.77847701e-01, -1.48988676e+00}, 
            			{-1.37159669e+00, -1.42372561e+00, -2.07679963e+00, 4.78813618e-01
        					, 3.56024116e-01, -5.66146910e-01, -1.79485869e+00, -1.66651845e-01
        					, -2.19781470e+00, 5.05860269e-01, 1.65713632e+00, 1.30696788e-01
        					, -2.43553668e-01, 5.27693510e-01, -4.59031343e+00, -1.23790872e+00
        					, -6.56951359e-03, -1.25077873e-01, -7.84215331e-01, 3.52460951e-01},
            			{1.18041325e+00, 7.75223613e-01, 7.66760707e-01, -3.87309343e-01
    						, 7.23935142e-02, 5.25120795e-01, -4.61007088e-01, 1.42712280e-01
    						, -4.83896911e-01, -1.53003895e+00, -8.48963857e-01, -7.22049356e-01
    						, 5.70404947e-01, 6.38419628e-01, -3.15883756e-01, -4.98337984e-01
    						, -2.92547382e-02, -2.18906939e-01, -1.29990625e+00, -1.25162697e+00}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.01907305, -1.89425242, -0.97751427, 0.76295644, -0.28026265, -1.15950739
            			, -1.26561785, -1.78937876, 0.0113989, -1.13186741, -0.1073491, 0.15286975
            			, 1.02726209, -1.30959368, 0.97139823, -0.34341496, -0.47814783, -0.22207576
            			, 1.72455037, 0.26014087};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.73419613,
            			-0.31831276,
            			-0.91273874,
            			0.86854219,
            			-0.57943088,
            			-0.60356951,
            			-1.83344424,
            			0.90108633,
            			-0.59044623,
            			0.83875072,
            			0.11412804,
            			0.9707275,
            			1.7760216,
            			1.46755576,
            			-0.70740479,
            			-0.041819,
            			0.64300752,
            			-1.35151494,
            			0.0128371,
            			1.38317752};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.99669892};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.0;
	}

	@Override
	public double getMaxWidth() {
		
		return 438.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 6732.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.6;
	}

	@Override
	public double getMinWidth() {
		
		return 200.0;
	}

	@Override
	public double getMinWeight() {
		
		return 1477.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
