package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SS3 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10169")) return 100;
if(p.equals("AN10244"))
 if(we <= 3492) return 40;
 if(we > 3492)
  if(we <= 3558) return 50;
  if(we > 3558) return 47.5;
if(p.equals("AN10245"))
 if(t <= 4.97)
  if(t <= 4.75)
   if(t <= 4.65)
    if(w <= 376)
     if(we <= 3090)
      if(w <= 374.5) return 50;
      if(w > 374.5) return 40;
     if(we > 3090)
      if(we <= 3290) return 30;
      if(we > 3290) return 50;
    if(w > 376) return 35;
   if(t > 4.65)
    if(t <= 4.72)
     if(we <= 3152) return 50;
     if(we > 3152) return 60;
    if(t > 4.72) return 50;
  if(t > 4.75) return 80;
 if(t > 4.97)
  if(t <= 5.2)
   if(we <= 3483)
    if(w <= 404)
     if(we <= 3326) return 60;
     if(we > 3326) return 40;
    if(w > 404) return 40;
   if(we > 3483)
    if(we <= 3564) return 41.6666666666667;
    if(we > 3564)
     if(we <= 3673)
      if(w <= 416) return 40;
      if(w > 416) return 50;
     if(we > 3673) return 45;
  if(t > 5.2)
   if(t <= 5.6)
    if(we <= 3157) return 70;
    if(we > 3157) return 40;
   if(t > 5.6)
    if(w <= 426)
     if(we <= 3644) return 50;
     if(we > 3644) return 70;
    if(w > 426)
     if(we <= 3548) return 60;
     if(we > 3548) return 50;
if(p.equals("AN10252")) return 40;
if(p.equals("AN10258"))
 if(t <= 3.7) return 30;
 if(t > 3.7) return 50;
if(p.equals("PP11010"))
 if(we <= 1089) return 35;
 if(we > 1089) return 30;
if(p.equals("PP11017")) return 30;
if(p.equals("PP11021")) return 30;
if(p.equals("PP11029")) return 50;
if(p.equals("PP11033")) return 10;
if(p.equals("PP11045")) return 40;
if(p.equals("PP11880"))
 if(t <= 6.2) return 65;
 if(t > 6.2)
  if(t <= 6.4) return 60;
  if(t > 6.4) return 40;
if(p.equals("PP12040")) return 20;
if(p.equals("PP12069")) return 20;
if(p.equals("PP12073"))
 if(t <= 2.11)
  if(we <= 1074) return 53.3333333333333;
  if(we > 1074) return 40;
 if(t > 2.11)
  if(t <= 2.35)
   if(we <= 1156)
    if(we <= 1060)
     if(we <= 1014) return 100;
     if(we > 1014) return 60;
    if(we > 1060)
     if(we <= 1121)
      if(we <= 1079) return 50;
      if(we > 1079) return 40;
     if(we > 1121) return 60;
   if(we > 1156)
    if(t <= 2.1975) return 60;
    if(t > 2.1975) return 48.5666666666667;
  if(t > 2.35) return 30;
if(p.equals("PP12087"))
 if(w <= 134) return 40;
 if(w > 134) return 70;
if(p.equals("PP12130"))
 if(we <= 2080) return 40;
 if(we > 2080)
  if(we <= 2145) return 20;
  if(we > 2145)
   if(we <= 2189) return 30;
   if(we > 2189)
    if(we <= 2213) return 40;
    if(we > 2213) return 30;
if(p.equals("PP12146")) return 50;
if(p.equals("PP12152"))
 if(we <= 1986) return 40;
 if(we > 1986) return 20;
if(p.equals("PP12164")) return 30;
if(p.equals("PP12254")) return 30;
if(p.equals("PP12390")) return 30;
if(p.equals("PP12394")) return 20;
if(p.equals("PP12395")) return 20;
if(p.equals("PP12435")) return 30;
if(p.equals("PP12452"))
 if(w <= 304)
  if(t <= 3.44)
   if(t <= 2.6)
    if(we <= 2599) return 50;
    if(we > 2599) return 60;
   if(t > 2.6)
    if(we <= 2182) return 50;
    if(we > 2182) return 60;
  if(t > 3.44) return 30;
 if(w > 304) return 40;
if(p.equals("PP12539")) return 30;
if(p.equals("PP12563"))
 if(t <= 2.395)
  if(we <= 1937)
   if(we <= 1847) return 50;
   if(we > 1847)
    if(we <= 1921)
     if(w <= 189.3) return 60;
     if(w > 189.3) return 15;
    if(we > 1921) return 20;
  if(we > 1937)
   if(t <= 2.25) return 40;
   if(t > 2.25)
    if(we <= 2058)
     if(we <= 1948) return 58;
     if(we > 1948) return 40;
    if(we > 2058) return 60;
 if(t > 2.395)
  if(w <= 220)
   if(we <= 1832) return 40;
   if(we > 1832) return 70;
  if(w > 220) return 40;
if(p.equals("PP21107")) return 20;
if(p.equals("PP21317")) return 10;
if(p.equals("PP21320"))
 if(w <= 205)
  if(we <= 970)
   if(w <= 76)
    if(we <= 479)
     if(we <= 437) return 70;
     if(we > 437) return 80;
    if(we > 479) return 70;
   if(w > 76)
    if(we <= 503) return 100;
    if(we > 503)
     if(we <= 539) return 80;
     if(we > 539) return 70;
  if(we > 970)
   if(t <= 2.805)
    if(w <= 174) return 50;
    if(w > 174)
     if(t <= 2.3)
      if(we <= 1741)
       if(t <= 2.11) return 50;
       if(t > 2.11)
        if(we <= 1645) return 60;
        if(we > 1645) return 50;
      if(we > 1741) return 60;
     if(t > 2.3)
      if(w <= 195)
       if(t <= 2.6)
        if(we <= 1720) return 50;
        if(we > 1720) return 70;
       if(t > 2.6) return 60;
      if(w > 195)
       if(we <= 1672) return 40;
       if(we > 1672) return 70;
   if(t > 2.805)
    if(t <= 3.25) return 120;
    if(t > 3.25)
     if(we <= 1544)
      if(we <= 1525) return 50;
      if(we > 1525) return 41.4333333333333;
     if(we > 1544) return 40;
 if(w > 205)
  if(w <= 213)
   if(we <= 1817)
    if(t <= 2.395)
     if(we <= 1648)
      if(we <= 1298) return 35;
      if(we > 1298) return 60;
     if(we > 1648)
      if(we <= 1767)
       if(we <= 1696)
        if(we <= 1671) return 58;
        if(we > 1671) return 70;
       if(we > 1696) return 40;
      if(we > 1767)
       if(we <= 1792) return 43.3333333333333;
       if(we > 1792) return 55;
    if(t > 2.395) return 40;
   if(we > 1817)
    if(t <= 2.395)
     if(we <= 1983)
      if(we <= 1862)
       if(we <= 1839) return 50;
       if(we > 1839) return 52;
      if(we > 1862) return 10;
     if(we > 1983)
      if(we <= 2163)
       if(we <= 2080) return 55;
       if(we > 2080)
        if(we <= 2123) return 25;
        if(we > 2123) return 40;
      if(we > 2163) return 50;
    if(t > 2.395)
     if(we <= 1933)
      if(we <= 1833) return 40;
      if(we > 1833) return 70;
     if(we > 1933)
      if(we <= 2031) return 15;
      if(we > 2031) return 40;
  if(w > 213)
   if(t <= 2.11)
    if(we <= 1882)
     if(we <= 1806)
      if(we <= 1748)
       if(we <= 1712) return 15;
       if(we > 1712) return 50;
      if(we > 1748) return 10;
     if(we > 1806)
      if(t <= 2.095) return 50;
      if(t > 2.095) return 40;
    if(we > 1882)
     if(w <= 345)
      if(w <= 230) return 50;
      if(w > 230)
       if(t <= 2.095) return 50;
       if(t > 2.095)
        if(we <= 2073)
         if(we <= 2024) return 60;
         if(we > 2024) return 48.75;
        if(we > 2073) return 40;
     if(w > 345)
      if(we <= 2984)
       if(we <= 2763) return 30;
       if(we > 2763) return 50;
      if(we > 2984)
       if(w <= 362) return 40;
       if(w > 362) return 60;
   if(t > 2.11)
    if(we <= 2294)
     if(we <= 1912)
      if(t <= 2.725)
       if(t <= 2.395) return 50;
       if(t > 2.395) return 40;
      if(t > 2.725)
       if(we <= 1620)
        if(w <= 364.5) return 46;
        if(w > 364.5) return 60;
       if(we > 1620) return 50;
     if(we > 1912)
      if(t <= 2.725)
       if(w <= 228)
        if(w <= 216) return 50;
        if(w > 216)
         if(we <= 2031) return 40;
         if(we > 2031) return 60;
       if(w > 228)
        if(w <= 234) return 50;
        if(w > 234)
         if(t <= 2.395) return 50;
         if(t > 2.395) return 110;
      if(t > 2.725)
       if(w <= 262) return 40;
       if(w > 262) return 50;
    if(we > 2294)
     if(w <= 299)
      if(we <= 2568)
       if(w <= 275)
        if(w <= 262) return 70;
        if(w > 262) return 30;
       if(w > 275)
        if(we <= 2486) return 40;
        if(we > 2486) return 70;
      if(we > 2568) return 30;
     if(w > 299)
      if(t <= 2.995)
       if(we <= 3169)
        if(we <= 2991) return 40;
        if(we > 2991)
         if(we <= 3032) return 70;
         if(we > 3032) return 10;
       if(we > 3169)
        if(t <= 2.35)
         if(we <= 3276) return 40;
         if(we > 3276)
          if(we <= 3422) return 50;
          if(we > 3422) return 40;
        if(t > 2.35) return 60;
      if(t > 2.995)
       if(we <= 2679) return 50;
       if(we > 2679) return 53.3333333333333;
if(p.equals("PP21328"))
 if(t <= 5.35)
  if(t <= 4.9)
   if(t <= 2.4)
    if(t <= 2.11) return 60;
    if(t > 2.11) return 50;
   if(t > 2.4)
    if(w <= 78) return 45.3333333333333;
    if(w > 78)
     if(t <= 2.895)
      if(we <= 5308) return 70;
      if(we > 5308) return 60;
     if(t > 2.895)
      if(we <= 628) return 70;
      if(we > 628) return 60;
  if(t > 4.9)
   if(w <= 180)
    if(w <= 98)
     if(w <= 75)
      if(we <= 598)
       if(we <= 472)
        if(we <= 428) return 70;
        if(we > 428) return 80;
       if(we > 472) return 70;
      if(we > 598)
       if(we <= 627) return 130;
       if(we > 627) return 90;
     if(w > 75)
      if(we <= 682)
       if(we <= 644)
        if(we <= 593) return 120;
        if(we > 593) return 66;
       if(we > 644) return 75;
      if(we > 682)
       if(we <= 738)
        if(we <= 707) return 70;
        if(we > 707) return 55;
       if(we > 738)
        if(we <= 784) return 70;
        if(we > 784) return 60;
    if(w > 98)
     if(we <= 1247) return 60;
     if(we > 1247) return 80;
   if(w > 180) return 60;
 if(t > 5.35)
  if(w <= 325)
   if(w <= 122) return 40;
   if(w > 122) return 70;
  if(w > 325)
   if(t <= 5.7)
    if(we <= 3584)
     if(we <= 3223)
      if(t <= 5.6) return 50;
      if(t > 5.6) return 40;
     if(we > 3223)
      if(we <= 3335)
       if(we <= 3257) return 35;
       if(we > 3257)
        if(we <= 3305) return 45;
        if(we > 3305) return 35;
      if(we > 3335) return 40;
    if(we > 3584)
     if(we <= 3627) return 50;
     if(we > 3627) return 41.6666666666667;
   if(t > 5.7)
    if(we <= 3345)
     if(we <= 3199) return 70;
     if(we > 3199) return 20;
    if(we > 3345)
     if(we <= 3584)
      if(we <= 3478) return 40;
      if(we > 3478) return 45;
     if(we > 3584) return 40;
if(p.equals("PP21332")) return 60;
if(p.equals("PP21336"))
 if(t <= 7.9)
  if(t <= 2.41)
   if(we <= 1569)
    if(t <= 2.195)
     if(t <= 2.055) return 50;
     if(t > 2.055) return 60;
    if(t > 2.195) return 50;
   if(we > 1569) return 60;
  if(t > 2.41)
   if(w <= 204)
    if(t <= 3.92)
     if(w <= 190.6)
      if(t <= 2.95)
       if(t <= 2.5)
        if(w <= 176) return 40;
        if(w > 176)
         if(t <= 2.495)
          if(we <= 1587) return 50;
          if(we > 1587)
           if(we <= 1620) return 40;
           if(we > 1620) return 50;
         if(t > 2.495)
          if(we <= 1464) return 50;
          if(we > 1464) return 40;
       if(t > 2.5)
        if(t <= 2.6) return 60;
        if(t > 2.6)
         if(we <= 1755) return 50;
         if(we > 1755) return 60;
      if(t > 2.95)
       if(we <= 1528)
        if(w <= 157)
         if(we <= 1010) return 40;
         if(we > 1010) return 70;
        if(w > 157)
         if(w <= 183) return 40;
         if(w > 183)
          if(t <= 3.8) return 30;
          if(t > 3.8) return 40;
       if(we > 1528) return 60;
     if(w > 190.6)
      if(t <= 2.47)
       if(we <= 1443) return 60;
       if(we > 1443) return 40;
      if(t > 2.47)
       if(w <= 200)
        if(we <= 1442) return 50;
        if(we > 1442) return 60;
       if(w > 200)
        if(we <= 1769)
         if(we <= 1728)
          if(we <= 1673) return 50;
          if(we > 1673) return 60;
         if(we > 1728) return 50;
        if(we > 1769) return 60;
    if(t > 3.92)
     if(we <= 1289)
      if(t <= 4.03)
       if(t <= 4) return 60;
       if(t > 4)
        if(we <= 1246) return 90;
        if(we > 1246) return 40;
      if(t > 4.03)
       if(we <= 631)
        if(we <= 540) return 90;
        if(we > 540) return 70;
       if(we > 631)
        if(w <= 116.5)
         if(we <= 968) return 60;
         if(we > 968) return 80;
        if(w > 116.5) return 70;
     if(we > 1289)
      if(we <= 1591)
       if(w <= 179)
        if(t <= 4.03) return 60;
        if(t > 4.03)
         if(we <= 1455) return 50;
         if(we > 1455) return 45;
       if(w > 179) return 50;
      if(we > 1591)
       if(t <= 4.4)
        if(t <= 4)
         if(we <= 1711)
          if(we <= 1674)
           if(we <= 1617) return 70;
           if(we > 1617) return 60;
          if(we > 1674) return 65;
         if(we > 1711) return 60;
        if(t > 4)
         if(we <= 1671)
          if(we <= 1650) return 47.5;
          if(we > 1650) return 45;
         if(we > 1671)
          if(we <= 1714) return 60;
          if(we > 1714)
           if(w <= 184.5) return 65;
           if(w > 184.5) return 60;
       if(t > 4.4) return 60;
   if(w > 204)
    if(t <= 4)
     if(t <= 3.83)
      if(t <= 2.95)
       if(t <= 2.725)
        if(t <= 2.495) return 100;
        if(t > 2.495)
         if(we <= 2027) return 40;
         if(we > 2027)
          if(we <= 3148) return 60;
          if(we > 3148) return 40;
       if(t > 2.725)
        if(we <= 2085) return 80;
        if(we > 2085) return 60;
      if(t > 2.95)
       if(t <= 3.1)
        if(t <= 3)
         if(w <= 503.5) return 50;
         if(w > 503.5) return 90;
        if(t > 3) return 90;
       if(t > 3.1)
        if(t <= 3.3) return 40;
        if(t > 3.3) return 50;
     if(t > 3.83)
      if(t <= 3.85)
       if(we <= 2087) return 100;
       if(we > 2087) return 53.3333333333333;
      if(t > 3.85) return 60;
    if(t > 4)
     if(t <= 4.75) return 40;
     if(t > 4.75)
      if(we <= 2075) return 100;
      if(we > 2075) return 50;
 if(t > 7.9)
  if(w <= 275)
   if(w <= 230)
    if(w <= 197) return 110;
    if(w > 197)
     if(we <= 1835) return 100;
     if(we > 1835)
      if(we <= 1889) return 105;
      if(we > 1889) return 110;
   if(w > 230)
    if(we <= 2648) return 110;
    if(we > 2648) return 130;
  if(w > 275)
   if(we <= 2631) return 140;
   if(we > 2631) return 100;
if(p.equals("PP21341"))
 if(t <= 2.5)
  if(we <= 1693)
   if(we <= 1293) return 50;
   if(we > 1293) return 110;
  if(we > 1693) return 60;
 if(t > 2.5)
  if(we <= 3314) return 40;
  if(we > 3314) return 50;
if(p.equals("PP21346"))
 if(t <= 6.1)
  if(t <= 2.75)
   if(w <= 157)
    if(t <= 2.595)
     if(we <= 2134)
      if(t <= 2.35) return 60;
      if(t > 2.35) return 40;
     if(we > 2134)
      if(we <= 2540) return 50;
      if(we > 2540) return 60;
    if(t > 2.595)
     if(t <= 2.72) return 65;
     if(t > 2.72)
      if(we <= 2257)
       if(we <= 2177) return 50;
       if(we > 2177) return 57.5;
      if(we > 2257)
       if(w <= 136) return 55;
       if(w > 136)
        if(we <= 2716) return 50;
        if(we > 2716) return 55;
   if(w > 157)
    if(w <= 422)
     if(we <= 2991)
      if(t <= 2.48) return 30;
      if(t > 2.48) return 60;
     if(we > 2991) return 100;
    if(w > 422)
     if(we <= 2075)
      if(we <= 2049)
       if(we <= 1750) return 40;
       if(we > 1750)
        if(we <= 1924) return 70;
        if(we > 1924) return 50;
      if(we > 2049) return 54;
     if(we > 2075)
      if(we <= 2236)
       if(we <= 2104) return 70;
       if(we > 2104) return 60;
      if(we > 2236)
       if(we <= 2266) return 40;
       if(we > 2266) return 70;
  if(t > 2.75)
   if(t <= 4.98)
    if(we <= 1532)
     if(we <= 1240)
      if(t <= 4.72)
       if(t <= 4.15) return 50;
       if(t > 4.15) return 60;
      if(t > 4.72)
       if(w <= 183)
        if(w <= 145) return 90;
        if(w > 145)
         if(we <= 1014)
          if(we <= 968)
           if(we <= 911) return 50;
           if(we > 911) return 70;
          if(we > 968) return 90;
         if(we > 1014) return 50;
       if(w > 183) return 70;
     if(we > 1240)
      if(w <= 162) return 80;
      if(w > 162) return 50;
    if(we > 1532)
     if(we <= 1692)
      if(t <= 4.72)
       if(we <= 1603)
        if(we <= 1574) return 40;
        if(we > 1574) return 50;
       if(we > 1603)
        if(we <= 1620) return 70;
        if(we > 1620) return 40;
      if(t > 4.72)
       if(we <= 1643)
        if(we <= 1607)
         if(we <= 1571) return 60;
         if(we > 1571) return 52;
        if(we > 1607)
         if(we <= 1625) return 54.2833333333333;
         if(we > 1625) return 46;
       if(we > 1643) return 40;
     if(we > 1692)
      if(w <= 420) return 60;
      if(w > 420) return 40;
   if(t > 4.98)
    if(t <= 5.3)
     if(w <= 149) return 60;
     if(w > 149)
      if(we <= 1527)
       if(we <= 1295)
        if(we <= 1216) return 60;
        if(we > 1216) return 50;
       if(we > 1295)
        if(w <= 202) return 40;
        if(w > 202) return 60;
      if(we > 1527) return 50;
    if(t > 5.3)
     if(t <= 5.99)
      if(t <= 5.7)
       if(we <= 612) return 60;
       if(we > 612) return 40;
      if(t > 5.7)
       if(we <= 679) return 60;
       if(we > 679)
        if(w <= 80.5) return 60;
        if(w > 80.5) return 70;
     if(t > 5.99) return 90;
 if(t > 6.1)
  if(we <= 1131)
   if(w <= 116)
    if(we <= 713) return 80;
    if(we > 713)
     if(we <= 783)
      if(we <= 744) return 110;
      if(we > 744) return 130;
     if(we > 783)
      if(we <= 795) return 70;
      if(we > 795) return 60;
   if(w > 116) return 80;
  if(we > 1131)
   if(we <= 1771)
    if(w <= 171)
     if(we <= 1222) return 50;
     if(we > 1222) return 55;
    if(w > 171)
     if(we <= 1594)
      if(we <= 1514) return 70;
      if(we > 1514) return 60;
     if(we > 1594)
      if(we <= 1714)
       if(we <= 1667) return 55;
       if(we > 1667) return 80;
      if(we > 1714)
       if(we <= 1738) return 66;
       if(we > 1738) return 55;
   if(we > 1771)
    if(we <= 1857) return 70;
    if(we > 1857)
     if(we <= 1892) return 110;
     if(we > 1892) return 100;
if(p.equals("PP21364"))
 if(t <= 2.805) return 60;
 if(t > 2.805)
  if(we <= 1786) return 25;
  if(we > 1786)
   if(we <= 1842) return 40;
   if(we > 1842) return 60;
if(p.equals("PP21368"))
 if(we <= 1512) return 120;
 if(we > 1512) return 70;
if(p.equals("PP21389"))
 if(we <= 1235)
  if(t <= 4.15)
   if(we <= 758)
    if(w <= 88)
     if(w <= 78)
      if(w <= 68) return 120;
      if(w > 68)
       if(t <= 3.75) return 50;
       if(t > 3.75) return 60;
     if(w > 78)
      if(w <= 87.5) return 50;
      if(w > 87.5) return 40;
    if(w > 88)
     if(we <= 710)
      if(we <= 665) return 50;
      if(we > 665) return 80;
     if(we > 710) return 60;
   if(we > 758)
    if(t <= 3.6) return 50;
    if(t > 3.6)
     if(w <= 150)
      if(w <= 141)
       if(we <= 1086) return 50;
       if(we > 1086)
        if(we <= 1174) return 60;
        if(we > 1174) return 40;
      if(w > 141) return 50;
     if(w > 150)
      if(we <= 1129) return 60;
      if(we > 1129)
       if(we <= 1175) return 40;
       if(we > 1175) return 60;
  if(t > 4.15)
   if(we <= 586) return 40;
   if(we > 586) return 60;
 if(we > 1235)
  if(t <= 3.25) return 50;
  if(t > 3.25)
   if(t <= 3.65)
    if(we <= 1265) return 70;
    if(we > 1265) return 80;
   if(t > 3.65)
    if(t <= 4.15)
     if(w <= 140) return 70;
     if(w > 140) return 50;
    if(t > 4.15)
     if(w <= 225)
      if(t <= 4.5) return 80;
      if(t > 4.5) return 50;
     if(w > 225) return 70;
if(p.equals("PP21393"))
 if(w <= 106)
  if(we <= 778)
   if(w <= 75)
    if(t <= 2.75)
     if(t <= 2.4) return 70;
     if(t > 2.4) return 60;
    if(t > 2.75)
     if(t <= 3.5) return 50;
     if(t > 3.5)
      if(we <= 594)
       if(we <= 572) return 60;
       if(we > 572)
        if(w <= 68.5) return 60;
        if(w > 68.5) return 65;
      if(we > 594)
       if(t <= 4.15)
        if(we <= 685)
         if(t <= 3.97)
          if(we <= 632) return 80;
          if(we > 632) return 75;
         if(t > 3.97) return 60;
        if(we > 685) return 60;
       if(t > 4.15) return 80;
   if(w > 75)
    if(we <= 544)
     if(we <= 417) return 120;
     if(we > 417)
      if(t <= 4.15)
       if(t <= 3.87)
        if(w <= 94)
         if(t <= 2.725) return 40;
         if(t > 2.725) return 60;
        if(w > 94) return 40;
       if(t > 3.87)
        if(we <= 459) return 70;
        if(we > 459) return 50;
      if(t > 4.15)
       if(t <= 4.98) return 35;
       if(t > 4.98) return 40;
    if(we > 544)
     if(t <= 4.9)
      if(w <= 104)
       if(we <= 761)
        if(t <= 4.15)
         if(w <= 94)
          if(t <= 3.87)
           if(t <= 2.475)
            if(w <= 83) return 60;
            if(w > 83) return 50;
           if(t > 2.475) return 60;
          if(t > 3.87) return 50;
         if(w > 94) return 100;
        if(t > 4.15)
         if(t <= 4.46) return 100;
         if(t > 4.46)
          if(w <= 92) return 70;
          if(w > 92) return 60;
       if(we > 761)
        if(t <= 3.85)
         if(w <= 81) return 60;
         if(w > 81) return 35;
        if(t > 3.85) return 50;
      if(w > 104) return 65;
     if(t > 4.9)
      if(t <= 4.96) return 50;
      if(t > 4.96) return 60;
  if(we > 778)
   if(t <= 3.4)
    if(t <= 3.1) return 90;
    if(t > 3.1)
     if(we <= 1108) return 50;
     if(we > 1108) return 40;
   if(t > 3.4)
    if(t <= 4.15)
     if(w <= 99)
      if(w <= 90) return 80;
      if(w > 90)
       if(we <= 827) return 70;
       if(we > 827) return 60;
     if(w > 99)
      if(we <= 833) return 65;
      if(we > 833) return 80;
    if(t > 4.15)
     if(we <= 806)
      if(t <= 4.65) return 52.85;
      if(t > 4.65)
       if(we <= 784) return 65;
       if(we > 784) return 60;
     if(we > 806)
      if(we <= 835) return 80;
      if(we > 835)
       if(w <= 98)
        if(we <= 848) return 60;
        if(we > 848)
         if(t <= 4.485) return 60;
         if(t > 4.485) return 50;
       if(w > 98)
        if(w <= 103)
         if(t <= 4.9) return 80;
         if(t > 4.9) return 70;
        if(w > 103) return 60;
 if(w > 106)
  if(t <= 4.15)
   if(w <= 345)
    if(we <= 1030)
     if(we <= 736) return 40;
     if(we > 736)
      if(w <= 110)
       if(we <= 994) return 50;
       if(we > 994) return 40;
      if(w > 110)
       if(w <= 168)
        if(t <= 3.195)
         if(w <= 122) return 40;
         if(w > 122)
          if(we <= 819)
           if(we <= 773) return 50;
           if(we > 773) return 40;
          if(we > 819) return 60;
        if(t > 3.195)
         if(w <= 133)
          if(w <= 126)
           if(we <= 959) return 60;
           if(we > 959) return 40;
          if(w > 126) return 50;
         if(w > 133) return 60;
       if(w > 168) return 40;
    if(we > 1030)
     if(t <= 3.3)
      if(we <= 2044)
       if(t <= 3.1)
        if(we <= 1705)
         if(t <= 2.48) return 80;
         if(t > 2.48)
          if(w <= 199)
           if(we <= 1171) return 40;
           if(we > 1171)
            if(w <= 175)
             if(we <= 1238) return 50;
             if(we > 1238) return 65;
            if(w > 175)
             if(we <= 1516) return 40;
             if(we > 1516)
              if(we <= 1604) return 50;
              if(we > 1604) return 65;
          if(w > 199)
           if(w <= 204)
            if(we <= 1440) return 80;
            if(we > 1440) return 60;
           if(w > 204)
            if(we <= 1593) return 40;
            if(we > 1593) return 50;
        if(we > 1705)
         if(w <= 204) return 40;
         if(w > 204)
          if(t <= 2.9)
           if(we <= 1760) return 40;
           if(we > 1760)
            if(w <= 261) return 60;
            if(w > 261) return 50;
          if(t > 2.9) return 65;
       if(t > 3.1)
        if(we <= 1441)
         if(we <= 1399) return 60;
         if(we > 1399) return 35;
        if(we > 1441) return 50;
      if(we > 2044)
       if(t <= 2.6)
        if(t <= 2.48) return 80;
        if(t > 2.48) return 70;
       if(t > 2.6)
        if(t <= 3.1)
         if(t <= 2.72)
          if(w <= 157) return 60;
          if(w > 157) return 40;
         if(t > 2.72) return 60;
        if(t > 3.1) return 40;
     if(t > 3.3)
      if(t <= 3.46)
       if(t <= 3.435) return 70;
       if(t > 3.435)
        if(t <= 3.44)
         if(we <= 1079) return 74;
         if(we > 1079)
          if(we <= 1620) return 60;
          if(we > 1620)
           if(we <= 1901) return 70;
           if(we > 1901) return 60;
        if(t > 3.44) return 70;
      if(t > 3.46)
       if(t <= 3.97)
        if(we <= 1402) return 40;
        if(we > 1402) return 50;
       if(t > 3.97)
        if(w <= 122) return 50;
        if(w > 122)
         if(t <= 3.98) return 60;
         if(t > 3.98)
          if(we <= 1089) return 60;
          if(we > 1089) return 40;
   if(w > 345)
    if(w <= 385)
     if(t <= 2.45)
      if(t <= 2.25) return 60;
      if(t > 2.25) return 50;
     if(t > 2.45) return 40;
    if(w > 385)
     if(w <= 446)
      if(w <= 395)
       if(t <= 3.35) return 55;
       if(t > 3.35) return 40;
      if(w > 395)
       if(we <= 1937)
        if(we <= 1614) return 60;
        if(we > 1614) return 90;
       if(we > 1937) return 40;
     if(w > 446)
      if(t <= 2.995)
       if(we <= 4544)
        if(we <= 4462) return 60;
        if(we > 4462) return 50;
       if(we > 4544)
        if(we <= 4729) return 55;
        if(we > 4729) return 40;
      if(t > 2.995)
       if(t <= 3.355) return 50;
       if(t > 3.355) return 60;
  if(t > 4.15)
   if(w <= 183)
    if(w <= 128)
     if(w <= 122)
      if(t <= 4.95) return 55;
      if(t > 4.95) return 50;
     if(w > 122) return 60;
    if(w > 128)
     if(w <= 157)
      if(we <= 1226)
       if(t <= 4.65)
        if(we <= 806) return 50;
        if(we > 806) return 60;
       if(t > 4.65)
        if(we <= 1066) return 70;
        if(we > 1066)
         if(t <= 5.45)
          if(we <= 1162)
           if(we <= 1098) return 40;
           if(we > 1098) return 60;
          if(we > 1162) return 40;
         if(t > 5.45) return 70;
      if(we > 1226)
       if(we <= 1329)
        if(we <= 1269) return 80;
        if(we > 1269)
         if(t <= 5.36) return 50;
         if(t > 5.36) return 57.2666666666667;
       if(we > 1329)
        if(we <= 1382) return 80;
        if(we > 1382) return 70;
     if(w > 157)
      if(t <= 4.95)
       if(w <= 172)
        if(t <= 4.65)
         if(t <= 4.47) return 60;
         if(t > 4.47) return 80;
        if(t > 4.65)
         if(we <= 1078)
          if(we <= 953) return 60;
          if(we > 953) return 70;
         if(we > 1078) return 50;
       if(w > 172)
        if(t <= 4.65)
         if(we <= 933) return 50;
         if(we > 933)
          if(we <= 990) return 70;
          if(we > 990)
           if(we <= 1328) return 60;
           if(we > 1328) return 70;
        if(t > 4.65)
         if(we <= 1649) return 70;
         if(we > 1649) return 60;
      if(t > 4.95)
       if(t <= 5.45)
        if(we <= 1150) return 80;
        if(we > 1150) return 50;
       if(t > 5.45) return 70;
   if(w > 183)
    if(we <= 1470)
     if(we <= 1430)
      if(w <= 378)
       if(w <= 188) return 50;
       if(w > 188)
        if(t <= 4.38) return 70;
        if(t > 4.38) return 40;
      if(w > 378) return 60;
     if(we > 1430) return 90;
    if(we > 1470) return 60;
if(p.equals("PP21394"))
 if(w <= 254)
  if(we <= 1551) return 110;
  if(we > 1551) return 30;
 if(w > 254)
  if(we <= 2736) return 40;
  if(we > 2736)
   if(we <= 2947)
    if(we <= 2847) return 32.5;
    if(we > 2847) return 40;
   if(we > 2947) return 45;
if(p.equals("PP21398"))
 if(t <= 2.01)
  if(t <= 1.825) return 10;
  if(t > 1.825) return 40;
 if(t > 2.01)
  if(we <= 1814)
   if(we <= 1783) return 120;
   if(we > 1783) return 80;
  if(we > 1814)
   if(we <= 1854) return 70;
   if(we > 1854) return 90;
if(p.equals("PP21408"))
 if(we <= 1196)
  if(w <= 114) return 70;
  if(w > 114) return 50;
 if(we > 1196)
  if(w <= 112) return 70;
  if(w > 112)
   if(t <= 1.78) return 40;
   if(t > 1.78) return 60;
if(p.equals("PP21409")) return 20;
if(p.equals("PP21432"))
 if(t <= 5.7)
  if(t <= 3.87)
   if(we <= 449) return 30;
   if(we > 449) return 60;
  if(t > 3.87) return 80;
 if(t > 5.7)
  if(we <= 858) return 60;
  if(we > 858) return 110;
if(p.equals("PP21433")) return 50;
if(p.equals("PP21473"))
 if(t <= 6)
  if(w <= 101)
   if(w <= 58) return 35;
   if(w > 58)
    if(t <= 4.5)
     if(w <= 90) return 30;
     if(w > 90) return 70;
    if(t > 4.5) return 70;
  if(w > 101)
   if(t <= 5.2)
    if(we <= 899)
     if(t <= 4.5) return 40;
     if(t > 4.5)
      if(w <= 132) return 50;
      if(w > 132) return 30;
    if(we > 899)
     if(w <= 240)
      if(t <= 4.5)
       if(we <= 1174) return 80;
       if(we > 1174) return 60;
      if(t > 4.5) return 60;
     if(w > 240)
      if(t <= 3.65) return 80;
      if(t > 3.65) return 40;
   if(t > 5.2)
    if(we <= 1775)
     if(t <= 5.99)
      if(we <= 1094) return 60;
      if(we > 1094)
       if(t <= 5.7)
        if(w <= 287.5) return 40;
        if(w > 287.5) return 80;
       if(t > 5.7) return 30;
     if(t > 5.99)
      if(we <= 1582) return 40;
      if(we > 1582) return 60;
    if(we > 1775)
     if(we <= 3921.5) return 75;
     if(we > 3921.5) return 50;
 if(t > 6)
  if(t <= 6.5)
   if(t <= 6.09)
    if(w <= 155) return 50;
    if(w > 155)
     if(w <= 168)
      if(we <= 1485) return 50;
      if(we > 1485) return 70;
     if(w > 168)
      if(we <= 1596) return 50;
      if(we > 1596) return 70;
   if(t > 6.09)
    if(we <= 283) return 100;
    if(we > 283)
     if(we <= 321) return 60;
     if(we > 321) return 100;
  if(t > 6.5)
   if(w <= 179)
    if(we <= 869) return 250;
    if(we > 869) return 60;
   if(w > 179) return 90;
if(p.equals("PP21476"))
 if(t <= 2.4)
  if(w <= 302)
   if(we <= 2502)
    if(we <= 2400)
     if(we <= 2108) return 60;
     if(we > 2108)
      if(we <= 2197)
       if(we <= 2157) return 20;
       if(we > 2157) return 46.6666666666667;
      if(we > 2197)
       if(we <= 2257) return 25;
       if(we > 2257) return 60;
    if(we > 2400)
     if(we <= 2454)
      if(we <= 2435) return 38;
      if(we > 2435) return 20;
     if(we > 2454)
      if(we <= 2484) return 45.7166666666667;
      if(we > 2484) return 25;
   if(we > 2502)
    if(we <= 2609)
     if(we <= 2547)
      if(we <= 2530) return 49.3666666666667;
      if(we > 2530) return 20;
     if(we > 2547)
      if(we <= 2581) return 50;
      if(we > 2581) return 51.8166666666667;
    if(we > 2609)
     if(we <= 2716) return 60;
     if(we > 2716)
      if(we <= 2789)
       if(we <= 2736) return 50;
       if(we > 2736) return 20;
      if(we > 2789)
       if(we <= 3973.5) return 60;
       if(we > 3973.5) return 30;
  if(w > 302)
   if(we <= 2568)
    if(we <= 2390)
     if(we <= 2301) return 25;
     if(we > 2301) return 20;
    if(we > 2390)
     if(we <= 2554) return 50;
     if(we > 2554) return 20;
   if(we > 2568)
    if(we <= 2655) return 52.5;
    if(we > 2655) return 65;
 if(t > 2.4)
  if(t <= 4.35)
   if(w <= 93)
    if(we <= 591)
     if(t <= 3.48)
      if(w <= 72) return 70;
      if(w > 72) return 80;
     if(t > 3.48)
      if(w <= 78)
       if(w <= 72)
        if(we <= 532) return 50;
        if(we > 532) return 60;
       if(w > 72) return 40;
      if(w > 78)
       if(w <= 85) return 30;
       if(w > 85) return 40;
    if(we > 591)
     if(w <= 78)
      if(w <= 72)
       if(we <= 599) return 70;
       if(we > 599)
        if(we <= 618) return 60;
        if(we > 618) return 70;
      if(w > 72) return 50;
     if(w > 78)
      if(w <= 85)
       if(we <= 668)
        if(we <= 651) return 50;
        if(we > 651) return 60;
       if(we > 668)
        if(we <= 726)
         if(we <= 699) return 50;
         if(we > 699) return 56.6666666666667;
        if(we > 726) return 80;
      if(w > 85) return 50;
   if(w > 93)
    if(w <= 286)
     if(t <= 3.4)
      if(t <= 3.16)
       if(we <= 917)
        if(w <= 110)
         if(w <= 108.5)
          if(t <= 2.49) return 30;
          if(t > 2.49) return 60;
         if(w > 108.5) return 30;
        if(w > 110) return 70;
       if(we > 917) return 50;
      if(t > 3.16)
       if(t <= 3.18)
        if(t <= 3.175) return 60;
        if(t > 3.175) return 70;
       if(t > 3.18)
        if(t <= 3.25)
         if(w <= 165)
          if(we <= 1196) return 60;
          if(we > 1196) return 50;
         if(w > 165)
          if(we <= 1507) return 40;
          if(we > 1507)
           if(we <= 1597) return 65;
           if(we > 1597) return 40;
        if(t > 3.25) return 60;
     if(t > 3.4)
      if(we <= 801)
       if(we <= 687)
        if(we <= 645) return 40;
        if(we > 645) return 45;
       if(we > 687) return 40;
      if(we > 801)
       if(w <= 170)
        if(t <= 3.97)
         if(t <= 3.92) return 70;
         if(t > 3.92) return 60;
        if(t > 3.97) return 70;
       if(w > 170)
        if(w <= 216)
         if(we <= 1253) return 40;
         if(we > 1253) return 60;
        if(w > 216)
         if(we <= 2097) return 170;
         if(we > 2097) return 30;
    if(w > 286)
     if(t <= 3.24)
      if(we <= 1549) return 30;
      if(we > 1549) return 40;
     if(t > 3.24) return 45;
  if(t > 4.35)
   if(t <= 4.5)
    if(we <= 1160)
     if(w <= 110)
      if(t <= 4.46)
       if(we <= 545) return 40;
       if(we > 545) return 80;
      if(t > 4.46)
       if(t <= 4.49)
        if(t <= 4.48) return 60;
        if(t > 4.48)
         if(we <= 582) return 65;
         if(we > 582) return 50;
       if(t > 4.49)
        if(we <= 657)
         if(we <= 598) return 60;
         if(we > 598)
          if(we <= 637) return 50;
          if(we > 637) return 40;
        if(we > 657) return 60;
     if(w > 110) return 40;
    if(we > 1160)
     if(we <= 1467) return 50;
     if(we > 1467)
      if(we <= 1497) return 55;
      if(we > 1497)
       if(we <= 1542) return 56.6666666666667;
       if(we > 1542)
        if(we <= 1573) return 50;
        if(we > 1573) return 40;
   if(t > 4.5)
    if(we <= 1069)
     if(t <= 4.54)
      if(w <= 109)
       if(we <= 943) return 60;
       if(we > 943)
        if(we <= 993) return 80;
        if(we > 993) return 60;
      if(w > 109)
       if(we <= 1032) return 50;
       if(we > 1032)
        if(we <= 1053) return 57.65;
        if(we > 1053) return 75;
     if(t > 4.54)
      if(t <= 4.75) return 45;
      if(t > 4.75)
       if(we <= 672) return 50;
       if(we > 672) return 60;
    if(we > 1069)
     if(we <= 1488) return 60;
     if(we > 1488)
      if(we <= 1585) return 50;
      if(we > 1585)
       if(we <= 1606) return 110;
       if(we > 1606)
        if(we <= 1627) return 57.5;
        if(we > 1627) return 60;
if(p.equals("PP21486"))
 if(we <= 693)
  if(we <= 644) return 100;
  if(we > 644)
   if(we <= 683) return 60;
   if(we > 683) return 50;
 if(we > 693) return 100;
if(p.equals("PP21516")) return 130;
if(p.equals("PP21531"))
 if(t <= 4.5)
  if(we <= 1462) return 55;
  if(we > 1462) return 45;
 if(t > 4.5) return 40;
if(p.equals("PP21557"))
 if(w <= 182) return 60;
 if(w > 182) return 30;
if(p.equals("PP21578"))
 if(we <= 596)
  if(we <= 578) return 60;
  if(we > 578)
   if(we <= 591) return 50;
   if(we > 591) return 40;
 if(we > 596)
  if(we <= 604) return 46.3666666666667;
  if(we > 604)
   if(we <= 606) return 60;
   if(we > 606) return 55;
if(p.equals("PP21599"))
 if(t <= 4) return 20;
 if(t > 4) return 50;
if(p.equals("PP21601"))
 if(t <= 5.45) return 20;
 if(t > 5.45)
  if(t <= 6.1) return 50;
  if(t > 6.1) return 30;
if(p.equals("PP21606"))
 if(w <= 275) return 70;
 if(w > 275)
  if(w <= 362) return 50;
  if(w > 362)
   if(t <= 3.6)
    if(w <= 409) return 40;
    if(w > 409) return 50;
   if(t > 3.6) return 40;
if(p.equals("PP21632")) return 30;
if(p.equals("PP21644")) return 15;
if(p.equals("PP21647"))
 if(we <= 2876) return 30;
 if(we > 2876) return 40;
if(p.equals("PP21671")) return 20;
if(p.equals("PP21677"))
 if(w <= 388) return 40;
 if(w > 388) return 50;
if(p.equals("PP21726")) return 40;
if(p.equals("PP21754")) return 60;
if(p.equals("PP21769"))
 if(t <= 4.6) return 25;
 if(t > 4.6) return 40;
if(p.equals("PP21777"))
 if(we <= 1504)
  if(we <= 1411) return 35;
  if(we > 1411) return 30;
 if(we > 1504)
  if(we <= 1555) return 40;
  if(we > 1555)
   if(we <= 1578) return 30.7833333333333;
   if(we > 1578) return 30;
if(p.equals("PP21779"))
 if(t <= 3.15)
  if(t <= 2.805) return 30;
  if(t > 2.805) return 25;
 if(t > 3.15) return 35;
if(p.equals("PP21782"))
 if(w <= 148) return 50;
 if(w > 148)
  if(w <= 303)
   if(t <= 4.75)
    if(w <= 282) return 20;
    if(w > 282) return 30;
   if(t > 4.75) return 30;
  if(w > 303) return 50;
if(p.equals("PP21789")) return 20;
if(p.equals("PP21790")) return 40;
if(p.equals("PP21792")) return 60;
if(p.equals("PP21833"))
 if(t <= 2.55) return 60;
 if(t > 2.55)
  if(w <= 102)
   if(we <= 790) return 50;
   if(we > 790)
    if(we <= 821) return 70;
    if(we > 821) return 50;
  if(w > 102)
   if(we <= 889) return 50;
   if(we > 889)
    if(we <= 925) return 70;
    if(we > 925) return 50;
if(p.equals("PP21903"))
 if(t <= 3.46)
  if(t <= 3.25)
   if(we <= 2309) return 50;
   if(we > 2309) return 70;
  if(t > 3.25) return 50;
 if(t > 3.46)
  if(t <= 4.02)
   if(t <= 3.87)
    if(t <= 3.75) return 40;
    if(t > 3.75)
     if(we <= 3207) return 45;
     if(we > 3207)
      if(we <= 3568) return 40;
      if(we > 3568) return 45;
   if(t > 3.87)
    if(we <= 403.5) return 120;
    if(we > 403.5) return 40;
  if(t > 4.02)
   if(t <= 4.15)
    if(we <= 3069) return 30;
    if(we > 3069) return 35;
   if(t > 4.15)
    if(we <= 3107) return 45;
    if(we > 3107) return 35;
if(p.equals("PP21908")) return 30;
if(p.equals("PP22005"))
 if(we <= 4112) return 40;
 if(we > 4112) return 30;
if(p.equals("PP22016"))
 if(t <= 4) return 20;
 if(t > 4) return 50;
if(p.equals("PP22018"))
 if(t <= 2.48)
  if(t <= 2.455)
   if(t <= 2.35)
    if(w <= 78) return 60;
    if(w > 78)
     if(we <= 530) return 70;
     if(we > 530) return 50;
   if(t > 2.35)
    if(we <= 747) return 60;
    if(we > 747) return 80;
  if(t > 2.455)
   if(t <= 2.475) return 40;
   if(t > 2.475) return 30;
 if(t > 2.48)
  if(t <= 3.1)
   if(t <= 2.8)
    if(w <= 574)
     if(we <= 748)
      if(w <= 351) return 60;
      if(w > 351) return 50;
     if(we > 748) return 50;
    if(w > 574)
     if(we <= 5422)
      if(we <= 5079) return 60;
      if(we > 5079) return 40;
     if(we > 5422) return 50;
   if(t > 2.8)
    if(w <= 336)
     if(we <= 675) return 50;
     if(we > 675)
      if(w <= 96)
       if(we <= 712) return 60;
       if(we > 712) return 70;
      if(w > 96)
       if(we <= 953) return 50;
       if(we > 953)
        if(we <= 1006) return 60;
        if(we > 1006)
         if(t <= 3.065) return 70;
         if(t > 3.065) return 60;
    if(w > 336)
     if(t <= 2.9975) return 40;
     if(t > 2.9975) return 60;
  if(t > 3.1)
   if(w <= 126) return 60;
   if(w > 126) return 50;
if(p.equals("PP22049")) return 60;
if(p.equals("PP22059")) return 40;
if(p.equals("PP22060"))
 if(w <= 82)
  if(w <= 78) return 50;
  if(w > 78)
   if(we <= 710) return 60;
   if(we > 710)
    if(we <= 726)
     if(we <= 717) return 59.15;
     if(we > 717) return 55;
    if(we > 726)
     if(we <= 757) return 50;
     if(we > 757) return 60;
 if(w > 82)
  if(we <= 1553)
   if(w <= 172)
    if(t <= 4.15)
     if(t <= 3.98)
      if(we <= 1271)
       if(we <= 827) return 70;
       if(we > 827) return 60;
      if(we > 1271) return 70;
     if(t > 3.98) return 60;
    if(t > 4.15)
     if(we <= 671) return 50;
     if(we > 671) return 70;
   if(w > 172)
    if(w <= 203) return 80;
    if(w > 203)
     if(we <= 1212) return 50;
     if(we > 1212) return 60;
  if(we > 1553)
   if(we <= 2876) return 50;
   if(we > 2876)
    if(we <= 3320) return 40;
    if(we > 3320) return 50;
if(p.equals("PP22069")) return 40;
if(p.equals("PP22085"))
 if(we <= 455) return 70;
 if(we > 455)
  if(w <= 60) return 60;
  if(w > 60) return 120;
if(p.equals("PP22096")) return 30;
if(p.equals("PP22102")) return 30;
if(p.equals("PP22107")) return 30;
if(p.equals("PP22157"))
 if(t <= 2.3) return 25;
 if(t > 2.3) return 30;
if(p.equals("PP22158")) return 50;
if(p.equals("PP22160")) return 50;
if(p.equals("PP22171")) return 20;
if(p.equals("PP22178"))
 if(we <= 616) return 60;
 if(we > 616)
  if(we <= 636) return 50;
  if(we > 636) return 61.6666666666667;
if(p.equals("PP22179"))
 if(t <= 2.725) return 70;
 if(t > 2.725) return 50;
if(p.equals("PP22180"))
 if(w <= 362) return 40;
 if(w > 362)
  if(t <= 4.75)
   if(we <= 3210) return 35;
   if(we > 3210)
    if(t <= 4.65)
     if(we <= 3322) return 70;
     if(we > 3322) return 50;
    if(t > 4.65) return 70;
  if(t > 4.75) return 60;
if(p.equals("PP22181"))
 if(t <= 2.7) return 50;
 if(t > 2.7) return 60;
if(p.equals("PP22184")) return 40;
if(p.equals("PP22201")) return 50;
if(p.equals("PP22232"))
 if(we <= 1575)
  if(we <= 1513) return 40;
  if(we > 1513)
   if(w <= 180) return 40;
   if(w > 180)
    if(we <= 1532) return 50;
    if(we > 1532) return 48.3333333333333;
 if(we > 1575)
  if(we <= 1714)
   if(we <= 1660)
    if(w <= 180) return 55;
    if(w > 180) return 40;
   if(we > 1660)
    if(we <= 1690) return 45;
    if(we > 1690) return 55;
  if(we > 1714)
   if(t <= 2.995)
    if(we <= 1730) return 47.15;
    if(we > 1730)
     if(we <= 1750) return 50;
     if(we > 1750) return 60;
   if(t > 2.995) return 50;
if(p.equals("PP22233"))
 if(we <= 942) return 70;
 if(we > 942) return 1;
if(p.equals("PP22269"))
 if(t <= 3.44)
  if(t <= 2.9) return 50;
  if(t > 2.9)
   if(we <= 2435) return 60;
   if(we > 2435)
    if(we <= 2562) return 50;
    if(we > 2562) return 60;
 if(t > 3.44) return 100;
if(p.equals("PP22270")) return 60;
if(p.equals("PP22279")) return 20;
if(p.equals("PP22282")) return 40;
if(p.equals("PP22285")) return 40;
if(p.equals("PP22288"))
 if(we <= 1250) return 50;
 if(we > 1250) return 70;
if(p.equals("PP22289")) return 30;
if(p.equals("PP22290")) return 60;
if(p.equals("PP22304"))
 if(w <= 119) return 60;
 if(w > 119)
  if(t <= 4.6)
   if(we <= 1345) return 70;
   if(we > 1345) return 40;
  if(t > 4.6)
   if(t <= 5.5)
    if(we <= 908) return 60;
    if(we > 908) return 50;
   if(t > 5.5) return 60;
if(p.equals("PP22305"))
 if(we <= 837) return 50;
 if(we > 837) return 60;
if(p.equals("PP22310"))
 if(we <= 1501) return 70;
 if(we > 1501) return 55;
if(p.equals("PP22315"))
 if(we <= 1047)
  if(w <= 123)
   if(we <= 972)
    if(we <= 959) return 50;
    if(we > 959) return 55;
   if(we > 972)
    if(we <= 999) return 40;
    if(we > 999) return 50;
  if(w > 123)
   if(we <= 1015) return 50;
   if(we > 1015) return 40;
 if(we > 1047)
  if(we <= 1069) return 60;
  if(we > 1069) return 50;
if(p.equals("PP22324")) return 40;
if(p.equals("PP22366")) return 30;
if(p.equals("PP22386")) return 30;
if(p.equals("PP22387")) return 20;
if(p.equals("PP22399"))
 if(t <= 2.4)
  if(we <= 2338) return 40;
  if(we > 2338) return 30;
 if(t > 2.4) return 110;
if(p.equals("PP22414")) return 35;
if(p.equals("PP22415")) return 30;
if(p.equals("PP22416")) return 40;
if(p.equals("PP22417")) return 30;
if(p.equals("PP22418")) return 30;
if(p.equals("PP22419")) return 20;
if(p.equals("PP22420")) return 20;
if(p.equals("PP22421")) return 20;
if(p.equals("PP22422")) return 20;
if(p.equals("PP22424")) return 20;
if(p.equals("PP22426")) return 40;
if(p.equals("PP22447")) return 30;
if(p.equals("PP22468")) return 25;
if(p.equals("PP22469")) return 30;
if(p.equals("PP22470")) return 30;
if(p.equals("PP22479"))
 if(t <= 3.3) return 30;
 if(t > 3.3) return 40;
if(p.equals("PP22480")) return 30;
if(p.equals("PP22486"))
 if(t <= 4.15) return 20;
 if(t > 4.15) return 80;
if(p.equals("PP22490")) return 20;
if(p.equals("PP22539"))
 if(we <= 2442) return 60;
 if(we > 2442)
  if(we <= 2567) return 50;
  if(we > 2567)
   if(we <= 2663) return 60;
   if(we > 2663) return 50;
if(p.equals("PP22561")) return 40;
if(p.equals("PP22566")) return 20;
if(p.equals("PP22569")) return 40;
if(p.equals("PP22582")) return 10;
if(p.equals("PP22604")) return 70;
if(p.equals("WS11165")) return 20;
return 20.0;
}
}
