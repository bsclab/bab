package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_PP2 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10169")) return 28;
if(p.equals("AN10240")) return 10;
if(p.equals("AN10244"))
 if(we <= 24750) return 19;
 if(we > 24750) return 25;
if(p.equals("AN10245"))
 if(w <= 1275)
  if(we <= 7225) return 20;
  if(we > 7225)
   if(w <= 812.5) return 19;
   if(w > 812.5) return 20;
 if(w > 1275)
  if(we <= 24680)
   if(we <= 24610) return 22;
   if(we > 24610) return 37;
  if(we > 24680)
   if(we <= 24790) return 24;
   if(we > 24790) return 32;
if(p.equals("AN10252")) return 20;
if(p.equals("AN10258")) return 22;
if(p.equals("AN10259")) return 22;
if(p.equals("PP11008")) return 22;
if(p.equals("PP11010")) return 16;
if(p.equals("PP11021")) return 18;
if(p.equals("PP11029")) return 24;
if(p.equals("PP11032")) return 35;
if(p.equals("PP11033"))
 if(w <= 1069) return 25;
 if(w > 1069) return 23;
if(p.equals("PP11036")) return 30;
if(p.equals("PP11037")) return 30;
if(p.equals("PP11048")) return 30;
if(p.equals("PP11058"))
 if(we <= 14490)
  if(we <= 13090)
   if(w <= 1001)
    if(we <= 12490) return 32;
    if(we > 12490) return 24;
   if(w > 1001)
    if(we <= 12640) return 28;
    if(we > 12640) return 40;
  if(we > 13090)
   if(w <= 995)
    if(we <= 13960)
     if(w <= 976) return 33;
     if(w > 976) return 35;
    if(we > 13960) return 32;
   if(w > 995)
    if(we <= 13600) return 41;
    if(we > 13600)
     if(we <= 13880) return 37;
     if(we > 13880) return 33;
 if(we > 14490)
  if(w <= 985)
   if(w <= 976) return 34;
   if(w > 976)
    if(we <= 14780)
     if(we <= 14730) return 40.9;
     if(we > 14730) return 31.5;
    if(we > 14780)
     if(we <= 15050)
      if(we <= 14860) return 41;
      if(we > 14860) return 35;
     if(we > 15050) return 41;
  if(w > 985)
   if(w <= 1001)
    if(we <= 14960)
     if(we <= 14810) return 34;
     if(we > 14810)
      if(we <= 14880) return 33.5;
      if(we > 14880) return 33;
    if(we > 14960)
     if(we <= 15210)
      if(we <= 15130)
       if(we <= 15060) return 39;
       if(we > 15060) return 38;
      if(we > 15130)
       if(we <= 15170) return 32;
       if(we > 15170) return 39;
     if(we > 15210) return 125;
   if(w > 1001)
    if(w <= 1038)
     if(we <= 15160)
      if(we <= 14960)
       if(we <= 14770) return 37;
       if(we > 14770) return 35;
      if(we > 14960)
       if(we <= 15070) return 39;
       if(we > 15070) return 45;
     if(we > 15160)
      if(we <= 15290) return 31;
      if(we > 15290) return 36;
    if(w > 1038)
     if(we <= 15130) return 27;
     if(we > 15130) return 35;
if(p.equals("PP11064")) return 17;
if(p.equals("PP11065"))
 if(t <= 5.4)
  if(w <= 1054) return 24;
  if(w > 1054) return 16;
 if(t > 5.4) return 18;
if(p.equals("PP11067"))
 if(w <= 1061)
  if(we <= 21550) return 35;
  if(we > 21550)
   if(we <= 21630) return 33;
   if(we > 21630) return 31;
 if(w > 1061) return 32;
if(p.equals("PP11071"))
 if(w <= 1254)
  if(w <= 960) return 15;
  if(w > 960) return 21;
 if(w > 1254) return 22;
if(p.equals("PP11107"))
 if(w <= 1197) return 30;
 if(w > 1197) return 28;
if(p.equals("PP11260"))
 if(w <= 1189)
  if(t <= 5.95) return 22;
  if(t > 5.95) return 21;
 if(w > 1189) return 30;
if(p.equals("PP11261"))
 if(w <= 1127)
  if(we <= 20740)
   if(w <= 1015) return 21;
   if(w > 1015)
    if(we <= 19590) return 21;
    if(we > 19590)
     if(we <= 20370)
      if(we <= 20040) return 22;
      if(we > 20040)
       if(we <= 20170) return 18;
       if(we > 20170)
        if(we <= 20320) return 25;
        if(we > 20320) return 20;
     if(we > 20370)
      if(we <= 20560) return 23;
      if(we > 20560) return 22;
  if(we > 20740)
   if(we <= 21250)
    if(we <= 21000) return 26;
    if(we > 21000) return 21;
   if(we > 21250) return 24;
 if(w > 1127)
  if(t <= 3.235) return 23;
  if(t > 3.235)
   if(we <= 21030) return 25;
   if(we > 21030) return 30;
if(p.equals("PP11264"))
 if(t <= 3.1)
  if(we <= 16330)
   if(we <= 12920)
    if(we <= 12710) return 25;
    if(we > 12710) return 20;
   if(we > 12920)
    if(t <= 2.9) return 25;
    if(t > 2.9) return 161;
  if(we > 16330)
   if(t <= 2.5)
    if(t <= 2.1)
     if(w <= 1209) return 31;
     if(w > 1209) return 43;
    if(t > 2.1)
     if(t <= 2.2)
      if(we <= 19870) return 29;
      if(we > 19870) return 26;
     if(t > 2.2) return 40;
   if(t > 2.5)
    if(we <= 21120)
     if(t <= 2.7)
      if(w <= 1213) return 27;
      if(w > 1213)
       if(we <= 19940) return 24;
       if(we > 19940) return 26;
     if(t > 2.7)
      if(t <= 2.9)
       if(we <= 20210) return 26;
       if(we > 20210)
        if(we <= 20520) return 36;
        if(we > 20520) return 24;
      if(t > 2.9)
       if(w <= 1175) return 26;
       if(w > 1175) return 24;
    if(we > 21120) return 28;
 if(t > 3.1)
  if(we <= 22920)
   if(w <= 1201)
    if(t <= 4.35)
     if(w <= 1108)
      if(w <= 1087) return 24;
      if(w > 1087)
       if(we <= 20450) return 23;
       if(we > 20450) return 25;
     if(w > 1108)
      if(w <= 1129)
       if(we <= 19870) return 24;
       if(we > 19870)
        if(we <= 20950) return 25;
        if(we > 20950) return 25.5;
      if(w > 1129)
       if(we <= 22400) return 23;
       if(we > 22400)
        if(we <= 22600) return 26;
        if(we > 22600) return 23;
    if(t > 4.35)
     if(we <= 22600)
      if(t <= 4.8)
       if(we <= 21240)
        if(we <= 11005) return 18;
        if(we > 11005) return 25;
       if(we > 21240)
        if(we <= 22240) return 21;
        if(we > 22240)
         if(we <= 22420)
          if(we <= 22360) return 22;
          if(we > 22360) return 24;
         if(we > 22420) return 18;
      if(t > 4.8)
       if(we <= 19130)
        if(w <= 1187.5) return 18;
        if(w > 1187.5) return 23;
       if(we > 19130)
        if(we <= 19670) return 24;
        if(we > 19670) return 23;
     if(we > 22600) return 23;
   if(w > 1201)
    if(we <= 16450) return 20;
    if(we > 16450)
     if(t <= 5)
      if(we <= 22180) return 21;
      if(we > 22180) return 22;
     if(t > 5) return 19;
  if(we > 22920)
   if(t <= 4.35)
    if(we <= 23890)
     if(we <= 23230) return 24;
     if(we > 23230)
      if(we <= 23800) return 26;
      if(we > 23800) return 24;
    if(we > 23890)
     if(we <= 24600) return 27;
     if(we > 24600) return 26;
   if(t > 4.35)
    if(we <= 24200)
     if(we <= 23630) return 24;
     if(we > 23630)
      if(we <= 24050) return 22;
      if(we > 24050) return 24;
    if(we > 24200)
     if(we <= 24950)
      if(we <= 24800) return 29;
      if(we > 24800)
       if(we <= 24880) return 24;
       if(we > 24880) return 22;
     if(we > 24950)
      if(we <= 25050) return 27.5;
      if(we > 25050)
       if(we <= 25110) return 21;
       if(we > 25110) return 30;
if(p.equals("PP11266"))
 if(t <= 2.5)
  if(we <= 19880)
   if(w <= 1067)
    if(w <= 985)
     if(we <= 18830) return 31;
     if(we > 18830) return 36;
    if(w > 985) return 37;
   if(w > 1067)
    if(we <= 15850)
     if(we <= 15480) return 28;
     if(we > 15480) return 32;
    if(we > 15850)
     if(we <= 16230) return 31;
     if(we > 16230) return 32;
  if(we > 19880)
   if(w <= 1078)
    if(w <= 1016)
     if(we <= 20070) return 42;
     if(we > 20070) return 40;
    if(w > 1016)
     if(we <= 21070) return 37;
     if(we > 21070) return 41;
   if(w > 1078)
    if(w <= 1130)
     if(we <= 22390)
      if(we <= 22210) return 48;
      if(we > 22210) return 36;
     if(we > 22390)
      if(we <= 22500) return 103;
      if(we > 22500) return 50;
    if(w > 1130)
     if(we <= 21730)
      if(we <= 21390) return 38;
      if(we > 21390) return 36;
     if(we > 21730)
      if(we <= 22530) return 45;
      if(we > 22530) return 39;
 if(t > 2.5)
  if(t <= 2.9)
   if(t <= 2.8) return 29;
   if(t > 2.8) return 18;
  if(t > 2.9)
   if(we <= 19880)
    if(we <= 19210)
     if(w <= 1030)
      if(w <= 988) return 24;
      if(w > 988) return 25;
     if(w > 1030)
      if(t <= 3.3)
       if(w <= 1126) return 24;
       if(w > 1126) return 21;
      if(t > 3.3) return 21;
    if(we > 19210)
     if(we <= 19470) return 27;
     if(we > 19470) return 25;
   if(we > 19880)
    if(t <= 3.235)
     if(we <= 20980)
      if(w <= 1042)
       if(we <= 20880) return 30;
       if(we > 20880) return 27;
      if(w > 1042)
       if(w <= 1152) return 28;
       if(w > 1152) return 27;
     if(we > 20980)
      if(w <= 1100)
       if(we <= 21970)
        if(we <= 21590) return 32;
        if(we > 21590) return 25;
       if(we > 21970)
        if(w <= 1078) return 42;
        if(w > 1078) return 30;
      if(w > 1100)
       if(we <= 21540) return 27;
       if(we > 21540) return 31;
    if(t > 3.235)
     if(w <= 988) return 30;
     if(w > 988)
      if(we <= 20760)
       if(we <= 20600) return 24;
       if(we > 20600) return 33;
      if(we > 20760)
       if(we <= 21320) return 26;
       if(we > 21320) return 24;
if(p.equals("PP11275")) return 22;
if(p.equals("PP11287"))
 if(t <= 2.1) return 29;
 if(t > 2.1) return 34;
if(p.equals("PP11288")) return 18;
if(p.equals("PP11294")) return 28;
if(p.equals("PP11312"))
 if(t <= 3.4)
  if(we <= 20300) return 26;
  if(we > 20300)
   if(w <= 1054)
    if(we <= 21050) return 28;
    if(we > 21050) return 29;
   if(w > 1054) return 31;
 if(t > 3.4) return 24;
if(p.equals("PP11325")) return 26;
if(p.equals("PP11403"))
 if(w <= 1072) return 32;
 if(w > 1072) return 30;
if(p.equals("PP11435"))
 if(we <= 22130) return 27;
 if(we > 22130) return 22;
if(p.equals("PP11659")) return 19;
if(p.equals("PP11880")) return 19;
if(p.equals("PP11912"))
 if(t <= 3.2)
  if(t <= 2.9) return 30;
  if(t > 2.9) return 122;
 if(t > 3.2)
  if(we <= 20900) return 27;
  if(we > 20900)
   if(we <= 21520) return 23;
   if(we > 21520) return 111;
if(p.equals("PP11917")) return 31;
if(p.equals("PP11931"))
 if(we <= 17710)
  if(t <= 3.3) return 23;
  if(t > 3.3)
   if(we <= 15790)
    if(w <= 1126) return 19;
    if(w > 1126) return 22;
   if(we > 15790)
    if(we <= 16960)
     if(we <= 16660) return 22;
     if(we > 16660) return 25;
    if(we > 16960)
     if(we <= 17100)
      if(we <= 17040)
       if(we <= 17000) return 21.5;
       if(we > 17000) return 21.3333333333333;
      if(we > 17040)
       if(we <= 17060) return 22;
       if(we > 17060) return 23.8;
     if(we > 17100) return 23;
 if(we > 17710)
  if(we <= 18160)
   if(we <= 18100)
    if(we <= 17810) return 21;
    if(we > 17810) return 24;
   if(we > 18100) return 23.5;
  if(we > 18160)
   if(we <= 18200) return 22;
   if(we > 18200) return 40;
if(p.equals("PP11932"))
 if(we <= 15160)
  if(we <= 14230) return 21;
  if(we > 14230) return 22;
 if(we > 15160)
  if(t <= 3.3)
   if(t <= 2.6) return 28;
   if(t > 2.6)
    if(we <= 16980)
     if(we <= 16890) return 23;
     if(we > 16890) return 27;
    if(we > 16980)
     if(we <= 17020) return 25;
     if(we > 17020) return 125;
  if(t > 3.3)
   if(we <= 18190)
    if(we <= 17930) return 23;
    if(we > 17930) return 22;
   if(we > 18190) return 23;
if(p.equals("PP11954"))
 if(t <= 1.9) return 31;
 if(t > 1.9) return 32;
if(p.equals("PP11982"))
 if(t <= 2.5)
  if(we <= 11570)
   if(we <= 11100) return 18;
   if(we > 11100) return 20;
  if(we > 11570) return 23;
 if(t > 2.5)
  if(we <= 13030)
   if(we <= 12730)
    if(we <= 11600) return 18;
    if(we > 11600) return 19;
   if(we > 12730)
    if(we <= 12900) return 21;
    if(we > 12900)
     if(we <= 12930) return 20;
     if(we > 12930) return 19;
  if(we > 13030)
   if(we <= 13090) return 26;
   if(we > 13090) return 20;
if(p.equals("PP11984")) return 31;
if(p.equals("PP12002"))
 if(we <= 16720) return 20;
 if(we > 16720) return 24;
if(p.equals("PP12047"))
 if(we <= 17100)
  if(we <= 16100) return 22;
  if(we > 16100) return 23;
 if(we > 17100)
  if(we <= 17150) return 22;
  if(we > 17150) return 24;
if(p.equals("PP12095"))
 if(w <= 1001) return 33;
 if(w > 1001)
  if(we <= 15680)
   if(w <= 1038) return 35;
   if(w > 1038)
    if(we <= 14630) return 31;
    if(we > 14630) return 30;
  if(we > 15680) return 34;
if(p.equals("PP12130"))
 if(we <= 19580) return 20;
 if(we > 19580) return 21;
if(p.equals("PP12141")) return 34;
if(p.equals("PP12178")) return 24;
if(p.equals("PP12184"))
 if(we <= 16710) return 40;
 if(we > 16710)
  if(we <= 16920) return 29;
  if(we > 16920) return 26;
if(p.equals("PP12186"))
 if(we <= 16800)
  if(we <= 16340) return 26;
  if(we > 16340) return 30;
 if(we > 16800)
  if(we <= 16870) return 28;
  if(we > 16870) return 26;
if(p.equals("PP12188"))
 if(we <= 16400) return 28;
 if(we > 16400) return 29;
if(p.equals("PP12190")) return 30;
if(p.equals("PP12192"))
 if(we <= 16670) return 28;
 if(we > 16670) return 27;
if(p.equals("PP12193"))
 if(we <= 16450) return 28;
 if(we > 16450) return 128;
if(p.equals("PP12194"))
 if(we <= 14260) return 151;
 if(we > 14260) return 128;
if(p.equals("PP12195")) return 26;
if(p.equals("PP12196"))
 if(t <= 2.7) return 26;
 if(t > 2.7) return 19;
if(p.equals("PP12197")) return 17;
if(p.equals("PP12198"))
 if(w <= 1209)
  if(we <= 16710)
   if(we <= 15550) return 42;
   if(we > 15550) return 28;
  if(we > 16710) return 128;
 if(w > 1209)
  if(t <= 2.43) return 21;
  if(t > 2.43)
   if(we <= 16640) return 22;
   if(we > 16640) return 21;
if(p.equals("PP12199")) return 20;
if(p.equals("PP12201")) return 37;
if(p.equals("PP12202")) return 21;
if(p.equals("PP12204")) return 20;
if(p.equals("PP12205")) return 20;
if(p.equals("PP12229"))
 if(we <= 16440) return 30;
 if(we > 16440) return 29;
if(p.equals("PP12230"))
 if(we <= 15020) return 24;
 if(we > 15020) return 31;
if(p.equals("PP12232")) return 29;
if(p.equals("PP12234")) return 33;
if(p.equals("PP12235")) return 176;
if(p.equals("PP12238")) return 22;
if(p.equals("PP12245"))
 if(we <= 16650) return 27;
 if(we > 16650) return 30;
if(p.equals("PP12248")) return 28;
if(p.equals("PP12249")) return 24;
if(p.equals("PP12251"))
 if(we <= 16910)
  if(we <= 15670) return 22;
  if(we > 15670) return 28;
 if(we > 16910)
  if(we <= 17020) return 39;
  if(we > 17020) return 30;
if(p.equals("PP12252")) return 30;
if(p.equals("PP12253")) return 27;
if(p.equals("PP12256"))
 if(we <= 16630) return 27;
 if(we > 16630) return 26;
if(p.equals("PP12257")) return 30;
if(p.equals("PP12262")) return 22;
if(p.equals("PP12266"))
 if(we <= 16870) return 29;
 if(we > 16870) return 39;
if(p.equals("PP12267"))
 if(we <= 16720) return 26;
 if(we > 16720) return 28;
if(p.equals("PP12273")) return 25;
if(p.equals("PP12280")) return 30;
if(p.equals("PP12282")) return 30;
if(p.equals("PP12283"))
 if(w <= 1209) return 30;
 if(w > 1209) return 22;
if(p.equals("PP12285")) return 30;
if(p.equals("PP12286"))
 if(w <= 1198) return 29;
 if(w > 1198) return 31;
if(p.equals("PP12296")) return 37;
if(p.equals("PP12298")) return 34;
if(p.equals("PP12302")) return 20;
if(p.equals("PP12306")) return 28;
if(p.equals("PP12308")) return 25;
if(p.equals("PP12313")) return 20;
if(p.equals("PP12326")) return 25;
if(p.equals("PP12328")) return 25;
if(p.equals("PP12330")) return 28;
if(p.equals("PP12331")) return 20;
if(p.equals("PP12332")) return 25;
if(p.equals("PP12337")) return 38;
if(p.equals("PP12340"))
 if(w <= 1198)
  if(we <= 16590) return 27;
  if(we > 16590) return 26;
 if(w > 1198) return 40;
if(p.equals("PP12342")) return 26;
if(p.equals("PP12350")) return 30;
if(p.equals("PP12351")) return 26;
if(p.equals("PP12352")) return 30;
if(p.equals("PP12356"))
 if(we <= 16540) return 26;
 if(we > 16540) return 27;
if(p.equals("PP12358")) return 30;
if(p.equals("PP12359")) return 21;
if(p.equals("PP12361")) return 29;
if(p.equals("PP12363")) return 28;
if(p.equals("PP12364")) return 40;
if(p.equals("PP12369"))
 if(we <= 11740) return 23;
 if(we > 11740) return 20;
if(p.equals("PP12403")) return 32;
if(p.equals("PP12421"))
 if(we <= 15460) return 20;
 if(we > 15460) return 22;
if(p.equals("PP12428")) return 26;
if(p.equals("PP12432")) return 32;
if(p.equals("PP12433")) return 30;
if(p.equals("PP12435")) return 20;
if(p.equals("PP12436"))
 if(w <= 1122) return 21;
 if(w > 1122)
  if(we <= 16490) return 20;
  if(we > 16490) return 27;
if(p.equals("PP12437")) return 29;
if(p.equals("PP12439")) return 24;
if(p.equals("PP12440")) return 34;
if(p.equals("PP12443")) return 27;
if(p.equals("PP12444")) return 27;
if(p.equals("PP12446")) return 30;
if(p.equals("PP12449")) return 30;
if(p.equals("PP12450")) return 30;
if(p.equals("PP12455")) return 22;
if(p.equals("PP12457"))
 if(we <= 16540) return 26;
 if(we > 16540) return 27;
if(p.equals("PP12460")) return 42;
if(p.equals("PP12461")) return 21;
if(p.equals("PP12462")) return 21;
if(p.equals("PP12470")) return 22;
if(p.equals("PP12479")) return 27;
if(p.equals("PP12480")) return 19;
if(p.equals("PP12481")) return 19;
if(p.equals("PP12484")) return 127;
if(p.equals("PP12486")) return 24;
if(p.equals("PP12488")) return 30;
if(p.equals("PP12491")) return 28;
if(p.equals("PP12499")) return 22;
if(p.equals("PP12500")) return 26;
if(p.equals("PP12508")) return 28;
if(p.equals("PP12510")) return 33.3333333333333;
if(p.equals("PP12513")) return 18;
if(p.equals("PP12514")) return 18;
if(p.equals("PP12519")) return 34;
if(p.equals("PP12521")) return 34;
if(p.equals("PP12525")) return 29;
if(p.equals("PP12526")) return 29;
if(p.equals("PP12530")) return 29;
if(p.equals("PP12550")) return 32;
if(p.equals("PP12551")) return 32;
if(p.equals("PP12553")) return 21;
if(p.equals("PP12554")) return 21;
if(p.equals("PP12555")) return 21;
if(p.equals("PP21010"))
 if(t <= 2.2)
  if(t <= 1.5)
   if(t <= 1.457)
    if(t <= 1.4)
     if(we <= 21850) return 47;
     if(we > 21850) return 51;
    if(t > 1.4) return 44;
   if(t > 1.457) return 48;
  if(t > 1.5)
   if(t <= 2.05)
    if(we <= 16590) return 32;
    if(we > 16590) return 35;
   if(t > 2.05) return 106;
 if(t > 2.2)
  if(we <= 8710)
   if(we <= 7520)
    if(we <= 7215) return 20;
    if(we > 7215) return 13;
   if(we > 7520)
    if(t <= 2.3)
     if(w <= 862) return 19;
     if(w > 862) return 27;
    if(t > 2.3)
     if(w <= 1003)
      if(w <= 960) return 21;
      if(w > 960) return 12;
     if(w > 1003) return 13;
  if(we > 8710)
   if(t <= 5.45)
    if(w <= 1060)
     if(t <= 5.1) return 28;
     if(t > 5.1) return 35;
    if(w > 1060)
     if(we <= 12250) return 16;
     if(we > 12250)
      if(t <= 2.8)
       if(we <= 13400) return 23;
       if(we > 13400) return 37;
      if(t > 2.8)
       if(w <= 1207) return 22;
       if(w > 1207)
        if(t <= 4.35) return 21;
        if(t > 4.35) return 15;
   if(t > 5.45)
    if(we <= 18790)
     if(we <= 17880) return 20;
     if(we > 17880) return 18;
    if(we > 18790)
     if(w <= 1149) return 19;
     if(w > 1149) return 24;
if(p.equals("PP21020"))
 if(w <= 1191)
  if(t <= 2.2)
   if(t <= 2.02)
    if(t <= 1.91)
     if(t <= 1.81)
      if(t <= 1.61) return 40;
      if(t > 1.61) return 49;
     if(t > 1.81) return 40;
    if(t > 1.91)
     if(we <= 20950)
      if(we <= 19810) return 33;
      if(we > 19810) return 39;
     if(we > 20950)
      if(t <= 2) return 38;
      if(t > 2) return 36;
   if(t > 2.02)
    if(we <= 21120)
     if(we <= 19580) return 25;
     if(we > 19580) return 31;
    if(we > 21120)
     if(we <= 21550) return 34;
     if(we > 21550) return 35;
  if(t > 2.2)
   if(we <= 23430)
    if(t <= 2.7)
     if(we <= 23340) return 34;
     if(we > 23340) return 38;
    if(t > 2.7)
     if(we <= 21960)
      if(t <= 3.1) return 28;
      if(t > 3.1) return 27;
     if(we > 21960) return 27;
   if(we > 23430)
    if(we <= 24600) return 30;
    if(we > 24600) return 31;
 if(w > 1191)
  if(t <= 4.35)
   if(t <= 3.2) return 25;
   if(t > 3.2) return 29;
  if(t > 4.35)
   if(we <= 23720) return 26;
   if(we > 23720)
    if(we <= 24470) return 22;
    if(we > 24470) return 23;
if(p.equals("PP21028")) return 35;
if(p.equals("PP21039")) return 123;
if(p.equals("PP21045"))
 if(t <= 3.8) return 27;
 if(t > 3.8) return 16;
if(p.equals("PP21074"))
 if(we <= 19770)
  if(we <= 19230) return 23;
  if(we > 19230) return 22;
 if(we > 19770)
  if(we <= 20080) return 21;
  if(we > 20080) return 23;
if(p.equals("PP21078")) return 25;
if(p.equals("PP21082")) return 17;
if(p.equals("PP21089"))
 if(t <= 2.7)
  if(w <= 1118) return 32;
  if(w > 1118)
   if(we <= 22610) return 113;
   if(we > 22610) return 36;
 if(t > 2.7)
  if(we <= 20740)
   if(we <= 18580) return 21;
   if(we > 18580)
    if(t <= 3.4) return 28;
    if(t > 3.4)
     if(w <= 976)
      if(we <= 20240) return 28;
      if(we > 20240) return 24;
     if(w > 976)
      if(t <= 3.8) return 25;
      if(t > 3.8) return 20;
  if(we > 20740)
   if(t <= 3.7) return 29;
   if(t > 3.7)
    if(w <= 1099)
     if(w <= 976)
      if(we <= 20890) return 29;
      if(we > 20890) return 24;
     if(w > 976) return 30;
    if(w > 1099)
     if(w <= 1173)
      if(we <= 23740) return 26;
      if(we > 23740) return 33;
     if(w > 1173)
      if(w <= 1220) return 25;
      if(w > 1220) return 27;
if(p.equals("PP21090")) return 28;
if(p.equals("PP21092"))
 if(t <= 2.9)
  if(t <= 2.6)
   if(we <= 19700)
    if(t <= 1.91)
     if(we <= 15690)
      if(we <= 15390) return 32;
      if(we > 15390) return 34;
     if(we > 15690)
      if(we <= 15960)
       if(we <= 15830) return 33;
       if(we > 15830) return 115;
      if(we > 15960) return 34;
    if(t > 1.91)
     if(t <= 2.43)
      if(w <= 1025)
       if(we <= 17930)
        if(we <= 17650) return 27;
        if(we > 17650) return 34;
       if(we > 17930)
        if(we <= 19580) return 29;
        if(we > 19580) return 36;
      if(w > 1025)
       if(w <= 1038)
        if(we <= 18740)
         if(we <= 18610)
          if(we <= 18210) return 115;
          if(we > 18210) return 30;
         if(we > 18610) return 27;
        if(we > 18740)
         if(we <= 18980) return 30;
         if(we > 18980)
          if(we <= 19290) return 34;
          if(we > 19290) return 31;
       if(w > 1038) return 31;
     if(t > 2.43)
      if(we <= 19620) return 28;
      if(we > 19620) return 32;
   if(we > 19700)
    if(we <= 23250)
     if(w <= 1038)
      if(w <= 1025)
       if(we <= 20940)
        if(we <= 20570)
         if(we <= 20170) return 135;
         if(we > 20170) return 36;
        if(we > 20570) return 34;
       if(we > 20940) return 38;
      if(w > 1025)
       if(we <= 20740) return 31;
       if(we > 20740)
        if(we <= 20830) return 33;
        if(we > 20830)
         if(we <= 20920) return 35;
         if(we > 20920) return 36;
     if(w > 1038)
      if(we <= 21480)
       if(w <= 1056)
        if(we <= 20390) return 30;
        if(we > 20390) return 33;
       if(w > 1056)
        if(we <= 21280) return 50;
        if(we > 21280) return 31.5;
      if(we > 21480)
       if(w <= 1063) return 37;
       if(w > 1063)
        if(we <= 21850) return 34;
        if(we > 21850)
         if(we <= 23020)
          if(we <= 22220) return 32;
          if(we > 22220) return 33;
         if(we > 23020) return 43;
    if(we > 23250)
     if(we <= 23950)
      if(t <= 2.43)
       if(w <= 1038) return 39;
       if(w > 1038) return 37;
      if(t > 2.43)
       if(w <= 1195) return 33;
       if(w > 1195)
        if(we <= 23730) return 29;
        if(we > 23730) return 38;
     if(we > 23950)
      if(t <= 2.43) return 35;
      if(t > 2.43)
       if(we <= 24770) return 31;
       if(we > 24770) return 40;
  if(t > 2.6)
   if(w <= 1011)
    if(we <= 20390) return 32;
    if(we > 20390) return 33;
   if(w > 1011)
    if(we <= 20860)
     if(we <= 20390) return 29;
     if(we > 20390)
      if(we <= 20570) return 31;
      if(we > 20570)
       if(we <= 20800)
        if(we <= 20650) return 29;
        if(we > 20650)
         if(we <= 20720) return 28;
         if(we > 20720) return 29;
       if(we > 20800) return 33;
    if(we > 20860)
     if(t <= 2.7) return 28;
     if(t > 2.7)
      if(we <= 20970) return 30;
      if(we > 20970)
       if(w <= 1232) return 29;
       if(w > 1232)
        if(we <= 24430) return 28;
        if(we > 24430) return 27;
 if(t > 2.9)
  if(t <= 4.03)
   if(t <= 3.7)
    if(w <= 1048)
     if(we <= 20540)
      if(w <= 1025) return 40;
      if(w > 1025)
       if(we <= 20190)
        if(we <= 19990)
         if(we <= 19090) return 25;
         if(we > 19090) return 27;
        if(we > 19990) return 31;
       if(we > 20190) return 28;
     if(we > 20540) return 31;
    if(w > 1048)
     if(we <= 22130)
      if(t <= 3.1)
       if(we <= 16840)
        if(we <= 16660) return 25;
        if(we > 16660) return 26;
       if(we > 16840)
        if(we <= 20420)
         if(we <= 16990)
          if(we <= 16900) return 27;
          if(we > 16900) return 22;
         if(we > 16990)
          if(we <= 20090)
           if(we <= 18000) return 24;
           if(we > 18000) return 29;
          if(we > 20090)
           if(we <= 20300) return 26;
           if(we > 20300) return 24;
        if(we > 20420)
         if(we <= 20470) return 30;
         if(we > 20470) return 31;
      if(t > 3.1)
       if(w <= 1197) return 26;
       if(w > 1197)
        if(we <= 13645) return 21;
        if(we > 13645) return 23;
     if(we > 22130)
      if(we <= 24600)
       if(t <= 3.4)
        if(w <= 1213)
         if(we <= 22230) return 24;
         if(we > 22230)
          if(we <= 24210)
           if(t <= 3.1) return 26;
           if(t > 3.1)
            if(we <= 23230) return 26;
            if(we > 23230) return 28;
          if(we > 24210) return 38;
        if(w > 1213) return 29;
       if(t > 3.4)
        if(w <= 1232)
         if(we <= 23980)
          if(w <= 1197)
           if(we <= 22450) return 32;
           if(we > 22450) return 25;
          if(w > 1197)
           if(we <= 23730) return 26;
           if(we > 23730)
            if(we <= 23880) return 34;
            if(we > 23880) return 24;
         if(we > 23980)
          if(we <= 24500)
           if(we <= 24410)
            if(we <= 24290) return 25;
            if(we > 24290)
             if(we <= 24300) return 24;
             if(we > 24300) return 26;
           if(we > 24410) return 24;
          if(we > 24500) return 29;
        if(w > 1232)
         if(we <= 24350) return 28;
         if(we > 24350) return 27;
      if(we > 24600)
       if(t <= 3.4)
        if(t <= 3.1) return 29;
        if(t > 3.1) return 31;
       if(t > 3.4)
        if(w <= 1232)
         if(we <= 24670) return 30;
         if(we > 24670) return 29;
        if(w > 1232) return 35;
   if(t > 3.7)
    if(we <= 20730)
     if(we <= 20350)
      if(we <= 19210)
       if(we <= 17370) return 22;
       if(we > 17370)
        if(we <= 18970)
         if(w <= 976) return 24;
         if(w > 976)
          if(we <= 18540)
           if(we <= 18140) return 23;
           if(we > 18140) return 22;
          if(we > 18540) return 25;
        if(we > 18970) return 24;
      if(we > 19210)
       if(t <= 3.8) return 27;
       if(t > 3.8) return 25;
     if(we > 20350) return 28;
    if(we > 20730)
     if(t <= 3.8)
      if(w <= 1015)
       if(we <= 20820) return 24;
       if(we > 20820)
        if(we <= 20910) return 26;
        if(we > 20910) return 27;
      if(w > 1015)
       if(we <= 22790)
        if(we <= 22560)
         if(we <= 22210) return 30;
         if(we > 22210) return 25;
        if(we > 22560)
         if(we <= 22590) return 33.5;
         if(we > 22590)
          if(we <= 22710) return 26;
          if(we > 22710) return 28;
       if(we > 22790)
        if(we <= 23060) return 23;
        if(we > 23060) return 24;
     if(t > 3.8)
      if(w <= 1190)
       if(w <= 1057)
        if(we <= 21050)
         if(we <= 20760) return 26;
         if(we > 20760) return 25;
        if(we > 21050) return 27;
       if(w > 1057)
        if(we <= 23050)
         if(w <= 1062)
          if(we <= 21880) return 31;
          if(we > 21880) return 29;
         if(w > 1062)
          if(w <= 1105) return 28;
          if(w > 1105) return 25;
        if(we > 23050)
         if(w <= 1144)
          if(w <= 1122)
           if(we <= 24190)
            if(we <= 23850)
             if(we <= 23220) return 27;
             if(we > 23220) return 24;
            if(we > 23850) return 29;
           if(we > 24190)
            if(we <= 24250) return 27;
            if(we > 24250) return 25;
          if(w > 1122) return 27;
         if(w > 1144)
          if(we <= 24490) return 25;
          if(we > 24490) return 28;
      if(w > 1190)
       if(w <= 1220)
        if(we <= 24840)
         if(we <= 24330) return 21;
         if(we > 24330) return 25;
        if(we > 24840)
         if(we <= 24890) return 29;
         if(we > 24890) return 26;
       if(w > 1220) return 22;
  if(t > 4.03)
   if(w <= 1079)
    if(t <= 4.7) return 25;
    if(t > 4.7) return 27;
   if(w > 1079)
    if(t <= 5.3)
     if(t <= 4.8)
      if(we <= 23310)
       if(w <= 1162) return 21;
       if(w > 1162)
        if(t <= 4.35)
         if(we <= 19450) return 22;
         if(we > 19450) return 21;
        if(t > 4.35)
         if(we <= 22770) return 24;
         if(we > 22770) return 22;
      if(we > 23310)
       if(we <= 24710) return 23;
       if(we > 24710) return 24;
     if(t > 4.8)
      if(we <= 17940) return 18;
      if(we > 17940)
       if(w <= 1082) return 21;
       if(w > 1082)
        if(w <= 1211)
         if(we <= 24720)
          if(w <= 1119) return 24;
          if(w > 1119)
           if(we <= 24460) return 21;
           if(we > 24460)
            if(we <= 24600) return 22;
            if(we > 24600) return 25;
         if(we > 24720) return 20;
        if(w > 1211) return 20;
    if(t > 5.3)
     if(w <= 1126)
      if(t <= 5.8)
       if(w <= 1110) return 21;
       if(w > 1110) return 23;
      if(t > 5.8)
       if(we <= 22630) return 20;
       if(we > 22630)
        if(t <= 6.5) return 22;
        if(t > 6.5) return 23;
     if(w > 1126)
      if(w <= 1151)
       if(we <= 24640) return 22;
       if(we > 24640) return 23;
      if(w > 1151)
       if(we <= 24410) return 23;
       if(we > 24410) return 22;
if(p.equals("PP21105"))
 if(t <= 2.1)
  if(we <= 16840) return 28;
  if(we > 16840)
   if(we <= 17440) return 31;
   if(we > 17440) return 129;
 if(t > 2.1)
  if(t <= 3.1)
   if(we <= 19560) return 24;
   if(we > 19560) return 30;
  if(t > 3.1) return 23;
if(p.equals("PP21107"))
 if(t <= 3.8)
  if(w <= 1188)
   if(we <= 24290)
    if(t <= 3.235)
     if(we <= 23790)
      if(we <= 21560) return 28;
      if(we > 21560)
       if(we <= 22130) return 29;
       if(we > 22130)
        if(w <= 1187) return 32;
        if(w > 1187) return 30;
     if(we > 23790)
      if(we <= 23900) return 26;
      if(we > 23900)
       if(we <= 24250) return 27;
       if(we > 24250) return 28;
    if(t > 3.235)
     if(we <= 21190) return 27;
     if(we > 21190) return 26;
   if(we > 24290)
    if(we <= 24410)
     if(we <= 24350) return 35;
     if(we > 24350) return 28;
    if(we > 24410)
     if(we <= 24440) return 110;
     if(we > 24440)
      if(we <= 24470) return 30;
      if(we > 24470) return 34;
  if(w > 1188)
   if(t <= 2.4)
    if(t <= 2.2)
     if(we <= 17530)
      if(we <= 17210) return 25;
      if(we > 17210) return 28;
     if(we > 17530)
      if(t <= 2.1) return 35;
      if(t > 2.1) return 30;
    if(t > 2.2)
     if(w <= 1193)
      if(we <= 15950) return 25;
      if(we > 15950) return 102;
     if(w > 1193)
      if(w <= 1218) return 37;
      if(w > 1218) return 29;
   if(t > 2.4)
    if(we <= 18750)
     if(t <= 2.8)
      if(w <= 1219) return 30;
      if(w > 1219) return 24;
     if(t > 2.8) return 21;
    if(we > 18750)
     if(w <= 1216) return 31;
     if(w > 1216)
      if(we <= 19410) return 40;
      if(we > 19410)
       if(we <= 19920)
        if(we <= 19490) return 23;
        if(we > 19490) return 26;
       if(we > 19920) return 25;
 if(t > 3.8)
  if(we <= 22810)
   if(w <= 1036)
    if(we <= 20750) return 23;
    if(we > 20750)
     if(we <= 20940)
      if(we <= 20870) return 32;
      if(we > 20870) return 25;
     if(we > 20940)
      if(t <= 4.6) return 24;
      if(t > 4.6) return 22;
   if(w > 1036)
    if(we <= 19140)
     if(t <= 4.7)
      if(w <= 1219)
       if(w <= 1209) return 21;
       if(w > 1209)
        if(t <= 4.2) return 24;
        if(t > 4.2) return 21;
      if(w > 1219) return 20;
     if(t > 4.7)
      if(t <= 5.7)
       if(we <= 18360) return 18;
       if(we > 18360)
        if(we <= 18740)
         if(we <= 18660) return 23;
         if(we > 18660) return 19;
        if(we > 18740) return 17;
      if(t > 5.7) return 19;
    if(we > 19140)
     if(w <= 1110)
      if(w <= 1105)
       if(t <= 4.8)
        if(we <= 21950) return 15;
        if(we > 21950) return 25;
       if(t > 4.8)
        if(we <= 22080)
         if(we <= 19960) return 20;
         if(we > 19960) return 23;
        if(we > 22080) return 31;
      if(w > 1105)
       if(we <= 21480) return 23;
       if(we > 21480) return 33;
     if(w > 1110)
      if(we <= 21340)
       if(t <= 4.8)
        if(t <= 4.6)
         if(we <= 19490) return 23;
         if(we > 19490) return 19;
        if(t > 4.6)
         if(we <= 20300) return 26;
         if(we > 20300)
          if(we <= 20750) return 19;
          if(we > 20750) return 24;
       if(t > 4.8)
        if(t <= 5.2) return 23;
        if(t > 5.2)
         if(we <= 19480) return 22;
         if(we > 19480) return 27;
      if(we > 21340)
       if(we <= 22740)
        if(we <= 22620)
         if(t <= 5)
          if(t <= 4.6) return 23;
          if(t > 4.6)
           if(w <= 1125) return 22;
           if(w > 1125) return 21;
         if(t > 5) return 22;
        if(we > 22620)
         if(t <= 5.5) return 22;
         if(t > 5.5) return 21;
       if(we > 22740)
        if(we <= 22790)
         if(t <= 4.8) return 25;
         if(t > 4.8) return 24;
        if(we > 22790) return 21;
  if(we > 22810)
   if(we <= 22930)
    if(w <= 1110)
     if(we <= 22860) return 23.5;
     if(we > 22860) return 125;
    if(w > 1110)
     if(t <= 4.8)
      if(we <= 22890)
       if(we <= 22840) return 22;
       if(we > 22840) return 23;
      if(we > 22890)
       if(we <= 22910) return 24;
       if(we > 22910) return 29;
     if(t > 4.8)
      if(t <= 5.45) return 24;
      if(t > 5.45) return 20;
   if(we > 22930)
    if(w <= 1190)
     if(w <= 1138)
      if(we <= 23070)
       if(we <= 23000)
        if(t <= 4.8)
         if(w <= 1110) return 27;
         if(w > 1110) return 24;
        if(t > 4.8) return 21;
       if(we > 23000)
        if(w <= 1110)
         if(we <= 23020) return 24.6666666666667;
         if(we > 23020) return 21;
        if(w > 1110)
         if(we <= 23030) return 26;
         if(we > 23030) return 23;
      if(we > 23070)
       if(we <= 23240)
        if(w <= 1110)
         if(we <= 23120) return 26;
         if(we > 23120) return 22;
        if(w > 1110) return 28;
       if(we > 23240)
        if(we <= 23360)
         if(we <= 23300) return 25;
         if(we > 23300) return 20;
        if(we > 23360)
         if(w <= 1125) return 25;
         if(w > 1125) return 26;
     if(w > 1138)
      if(w <= 1183) return 25;
      if(w > 1183)
       if(we <= 24740) return 40;
       if(we > 24740) return 25;
    if(w > 1190)
     if(t <= 4.35)
      if(we <= 24320) return 34;
      if(we > 24320) return 28;
     if(t > 4.35)
      if(t <= 5.3) return 26;
      if(t > 5.3)
       if(we <= 23750)
        if(we <= 23310) return 21;
        if(we > 23310) return 29;
       if(we > 23750)
        if(we <= 23970) return 134;
        if(we > 23970) return 22;
if(p.equals("PP21108"))
 if(t <= 2.15)
  if(w <= 1209)
   if(w <= 1159) return 36;
   if(w > 1159) return 30;
  if(w > 1209) return 49;
 if(t > 2.15) return 29;
if(p.equals("PP21114")) return 19;
if(p.equals("PP21116"))
 if(we <= 14690) return 24;
 if(we > 14690) return 26;
if(p.equals("PP21173")) return 39;
if(p.equals("PP21180"))
 if(we <= 20630)
  if(we <= 20010)
   if(w <= 1006)
    if(we <= 19650)
     if(we <= 19520)
      if(we <= 19060) return 22;
      if(we > 19060) return 111;
     if(we > 19520) return 21;
    if(we > 19650)
     if(we <= 19710) return 20;
     if(we > 19710) return 22;
   if(w > 1006)
    if(t <= 5.7) return 21;
    if(t > 5.7)
     if(we <= 18320) return 17;
     if(we > 18320) return 25;
  if(we > 20010)
   if(we <= 20560)
    if(w <= 1021)
     if(we <= 20250) return 24;
     if(we > 20250)
      if(we <= 20490)
       if(t <= 5.7)
        if(we <= 20440) return 25;
        if(we > 20440) return 20.5;
       if(t > 5.7)
        if(we <= 20360) return 23;
        if(we > 20360)
         if(we <= 20430) return 22.5;
         if(we > 20430) return 25;
      if(we > 20490)
       if(we <= 20530) return 22;
       if(we > 20530) return 24;
    if(w > 1021)
     if(we <= 20130) return 20;
     if(we > 20130)
      if(we <= 20380) return 22;
      if(we > 20380) return 19;
   if(we > 20560)
    if(w <= 1001) return 24;
    if(w > 1001)
     if(we <= 20590) return 26;
     if(we > 20590) return 21;
 if(we > 20630)
  if(we <= 21180)
   if(w <= 1015)
    if(we <= 20830) return 23;
    if(we > 20830) return 21;
   if(w > 1015)
    if(we <= 20750) return 18;
    if(we > 20750) return 17;
  if(we > 21180)
   if(w <= 1092) return 47;
   if(w > 1092)
    if(we <= 22330) return 24;
    if(we > 22330) return 27;
if(p.equals("PP21185")) return 28;
if(p.equals("PP21187")) return 31;
if(p.equals("PP21188")) return 27;
if(p.equals("PP21190")) return 30;
if(p.equals("PP21208"))
 if(t <= 4.2)
  if(we <= 22960)
   if(w <= 1000)
    if(w <= 976)
     if(we <= 20510) return 25;
     if(we > 20510) return 24;
    if(w > 976)
     if(we <= 20370)
      if(we <= 19900)
       if(we <= 19140)
        if(we <= 18500)
         if(we <= 17540) return 26;
         if(we > 17540) return 23;
        if(we > 18500)
         if(we <= 18980) return 24;
         if(we > 18980) return 25;
       if(we > 19140)
        if(we <= 19510) return 24.5;
        if(we > 19510) return 27;
      if(we > 19900)
       if(we <= 19980)
        if(we <= 19960) return 38;
        if(we > 19960) return 24;
       if(we > 19980) return 25;
     if(we > 20370)
      if(we <= 20660)
       if(we <= 20490) return 25;
       if(we > 20490) return 28;
      if(we > 20660) return 25;
   if(w > 1000)
    if(we <= 20310)
     if(t <= 3.55)
      if(we <= 17880)
       if(we <= 16340)
        if(we <= 16000) return 24;
        if(we > 16000) return 25;
       if(we > 16340)
        if(we <= 17300) return 22;
        if(we > 17300) return 25;
      if(we > 17880)
       if(we <= 17970) return 23;
       if(we > 17970)
        if(we <= 18100) return 25;
        if(we > 18100)
         if(we <= 18210) return 23;
         if(we > 18210) return 24;
     if(t > 3.55)
      if(we <= 19320)
       if(we <= 18390) return 17;
       if(we > 18390)
        if(we <= 18890) return 24;
        if(we > 18890) return 25;
      if(we > 19320)
       if(we <= 19800) return 26;
       if(we > 19800) return 23;
    if(we > 20310)
     if(w <= 1122)
      if(we <= 20750)
       if(w <= 1032)
        if(we <= 20520)
         if(we <= 20420) return 29;
         if(we > 20420) return 105;
        if(we > 20520)
         if(we <= 20630) return 24;
         if(we > 20630)
          if(we <= 20730) return 26.5;
          if(we > 20730) return 25;
       if(w > 1032) return 27;
      if(we > 20750)
       if(t <= 3.8)
        if(we <= 21730) return 26;
        if(we > 21730) return 27;
       if(t > 3.8) return 25;
     if(w > 1122)
      if(t <= 3.8)
       if(w <= 1213)
        if(we <= 22320) return 26;
        if(we > 22320) return 24;
       if(w > 1213) return 33;
      if(t > 3.8)
       if(we <= 22380)
        if(w <= 1131) return 26;
        if(w > 1131)
         if(we <= 22000) return 21;
         if(we > 22000) return 27;
       if(we > 22380)
        if(we <= 22480)
         if(we <= 22460) return 24;
         if(we > 22460) return 21;
        if(we > 22480) return 28;
  if(we > 22960)
   if(t <= 3.8)
    if(w <= 1186)
     if(t <= 2.9)
      if(w <= 1158) return 31;
      if(w > 1158) return 30;
     if(t > 2.9) return 27;
    if(w > 1186)
     if(w <= 1205)
      if(we <= 24290)
       if(w <= 1189) return 30;
       if(w > 1189) return 22;
      if(we > 24290)
       if(we <= 24800) return 24;
       if(we > 24800) return 28;
     if(w > 1205) return 26;
   if(t > 3.8)
    if(w <= 1171)
     if(w <= 1144)
      if(w <= 1122)
       if(w <= 1113) return 28;
       if(w > 1113) return 130;
      if(w > 1122)
       if(w <= 1131) return 23;
       if(w > 1131)
        if(we <= 24570) return 32;
        if(we > 24570)
         if(we <= 24670) return 27;
         if(we > 24670) return 29;
     if(w > 1144)
      if(w <= 1163) return 26;
      if(w > 1163)
       if(we <= 24610)
        if(we <= 24470) return 25;
        if(we > 24470) return 28;
       if(we > 24610)
        if(we <= 24720)
         if(we <= 24630) return 110;
         if(we > 24630) return 26;
        if(we > 24720)
         if(we <= 24740) return 27;
         if(we > 24740) return 28;
    if(w > 1171)
     if(we <= 23940)
      if(w <= 1204) return 25;
      if(w > 1204)
       if(we <= 23690) return 28;
       if(we > 23690) return 26;
     if(we > 23940)
      if(we <= 24790) return 27;
      if(we > 24790)
       if(we <= 24950) return 25;
       if(we > 24950)
        if(we <= 25000) return 27;
        if(we > 25000) return 20;
 if(t > 4.2)
  if(w <= 1129)
   if(w <= 1119)
    if(w <= 1036)
     if(we <= 20650)
      if(we <= 18960)
       if(we <= 18830) return 22;
       if(we > 18830) return 21;
      if(we > 18960)
       if(we <= 19820) return 38;
       if(we > 19820) return 26;
     if(we > 20650) return 23;
    if(w > 1036)
     if(w <= 1074)
      if(we <= 22140)
       if(we <= 21840) return 24;
       if(we > 21840)
        if(we <= 21960) return 26;
        if(we > 21960) return 24;
      if(we > 22140) return 25;
     if(w > 1074)
      if(w <= 1081)
       if(we <= 21540)
        if(we <= 20790) return 25;
        if(we > 20790)
         if(we <= 21400) return 29;
         if(we > 21400) return 21;
       if(we > 21540) return 23;
      if(w > 1081)
       if(t <= 5.45)
        if(w <= 1107) return 24;
        if(w > 1107) return 26;
       if(t > 5.45)
        if(we <= 23490) return 21;
        if(we > 23490) return 22;
   if(w > 1119)
    if(we <= 21810)
     if(we <= 20770)
      if(we <= 20320) return 19;
      if(we > 20320) return 24;
     if(we > 20770)
      if(we <= 21050) return 21;
      if(we > 21050) return 23;
    if(we > 21810)
     if(w <= 1122)
      if(we <= 23060)
       if(we <= 22350)
        if(we <= 22070) return 22;
        if(we > 22070)
         if(we <= 22120) return 23;
         if(we > 22120) return 22;
       if(we > 22350) return 24;
      if(we > 23060)
       if(we <= 23470)
        if(we <= 23170) return 27;
        if(we > 23170) return 23;
       if(we > 23470)
        if(we <= 23550) return 104;
        if(we > 23550) return 23.5;
     if(w > 1122)
      if(we <= 24440) return 35;
      if(we > 24440)
       if(we <= 24530) return 27;
       if(we > 24530) return 22;
  if(w > 1129)
   if(t <= 5.45)
    if(we <= 23740)
     if(we <= 11005) return 18;
     if(we > 11005) return 23;
    if(we > 23740)
     if(we <= 24770)
      if(we <= 23960)
       if(we <= 23830) return 28;
       if(we > 23830)
        if(w <= 1154) return 50;
        if(w > 1154) return 23;
      if(we > 23960) return 24;
     if(we > 24770)
      if(we <= 24860)
       if(we <= 24830) return 20;
       if(we > 24830) return 26.5;
      if(we > 24860) return 21;
   if(t > 5.45)
    if(w <= 1169) return 24;
    if(w > 1169)
     if(t <= 5.95) return 22;
     if(t > 5.95)
      if(we <= 24760) return 21;
      if(we > 24760) return 22;
if(p.equals("PP21209"))
 if(we <= 18440) return 20;
 if(we > 18440) return 21;
if(p.equals("PP21252")) return 28;
if(p.equals("PP21288")) return 41;
if(p.equals("PP21292")) return 30;
if(p.equals("PP21306"))
 if(w <= 1162) return 27;
 if(w > 1162)
  if(we <= 24580) return 31;
  if(we > 24580)
   if(we <= 24650) return 30;
   if(we > 24650) return 37;
if(p.equals("PP21312"))
 if(t <= 2.6) return 50;
 if(t > 2.6) return 30;
if(p.equals("PP21317")) return 130;
if(p.equals("PP21320"))
 if(t <= 4.2)
  if(we <= 21870)
   if(we <= 20250)
    if(t <= 2.8) return 18;
    if(t > 2.8)
     if(we <= 16790) return 23;
     if(we > 16790) return 30;
   if(we > 20250) return 27;
  if(we > 21870)
   if(t <= 3.1)
    if(t <= 2.8)
     if(w <= 1218)
      if(we <= 23900) return 30;
      if(we > 23900) return 37;
     if(w > 1218)
      if(we <= 23530)
       if(we <= 23220)
        if(we <= 22680) return 128;
        if(we > 22680) return 31;
       if(we > 23220) return 27;
      if(we > 23530) return 32;
    if(t > 2.8)
     if(we <= 22820)
      if(we <= 22210) return 30;
      if(we > 22210)
       if(we <= 22380) return 29;
       if(we > 22380) return 34;
     if(we > 22820)
      if(w <= 1149)
       if(we <= 23630) return 33;
       if(we > 23630) return 31;
      if(w > 1149)
       if(we <= 23420) return 31;
       if(we > 23420) return 27;
   if(t > 3.1)
    if(t <= 3.6) return 28;
    if(t > 3.6)
     if(we <= 22800) return 26;
     if(we > 22800) return 25;
 if(t > 4.2)
  if(t <= 5.2)
   if(we <= 22680)
    if(w <= 1082) return 32;
    if(w > 1082) return 23;
   if(we > 22680)
    if(t <= 4.7)
     if(w <= 1120)
      if(w <= 1114) return 25;
      if(w > 1114) return 21;
     if(w > 1120)
      if(w <= 1207) return 22;
      if(w > 1207)
       if(we <= 24640) return 23;
       if(we > 24640) return 31;
    if(t > 4.7) return 25;
  if(t > 5.2)
   if(we <= 23000)
    if(w <= 1104)
     if(w <= 1062) return 21;
     if(w > 1062) return 25;
    if(w > 1104) return 20;
   if(we > 23000)
    if(t <= 5.99)
     if(we <= 23860)
      if(we <= 23700) return 26;
      if(we > 23700)
       if(we <= 23790) return 24;
       if(we > 23790) return 21;
     if(we > 23860)
      if(we <= 24000) return 23;
      if(we > 24000) return 30;
    if(t > 5.99) return 22;
if(p.equals("PP21323")) return 23;
if(p.equals("PP21328"))
 if(t <= 3.4)
  if(we <= 24830)
   if(we <= 24540) return 29;
   if(we > 24540)
    if(we <= 24740)
     if(we <= 24610) return 24;
     if(we > 24610) return 31;
    if(we > 24740) return 29;
  if(we > 24830)
   if(we <= 24990) return 22;
   if(we > 24990) return 31;
 if(t > 3.4)
  if(w <= 1295)
   if(w <= 1111)
    if(t <= 5.2)
     if(we <= 22520) return 27;
     if(we > 22520)
      if(we <= 22790) return 23;
      if(we > 22790) return 28;
    if(t > 5.2) return 23;
   if(w > 1111)
    if(we <= 22240)
     if(we <= 22210)
      if(we <= 21280)
       if(w <= 1176) return 21;
       if(w > 1176) return 17;
      if(we > 21280) return 25;
     if(we > 22210) return 18;
    if(we > 22240)
     if(we <= 24410)
      if(t <= 4.8)
       if(we <= 22800) return 22;
       if(we > 22800) return 23;
      if(t > 4.8)
       if(w <= 1232)
        if(w <= 1145) return 27;
        if(w > 1145)
         if(we <= 23560) return 35;
         if(we > 23560) return 24;
       if(w > 1232)
        if(we <= 22290) return 24;
        if(we > 22290) return 23;
     if(we > 24410)
      if(w <= 1219) return 28;
      if(w > 1219)
       if(we <= 24700) return 22;
       if(we > 24700) return 25;
  if(w > 1295)
   if(t <= 5.99)
    if(we <= 23590)
     if(we <= 22580) return 22;
     if(we > 22580) return 30;
    if(we > 23590)
     if(we <= 24100)
      if(we <= 23860) return 24;
      if(we > 23860) return 32;
     if(we > 24100)
      if(we <= 24470) return 20;
      if(we > 24470)
       if(we <= 24810) return 24;
       if(we > 24810) return 30;
   if(t > 5.99)
    if(we <= 24550)
     if(we <= 23820) return 20;
     if(we > 23820) return 22;
    if(we > 24550)
     if(we <= 25180) return 14;
     if(we > 25180) return 23;
if(p.equals("PP21330")) return 114;
if(p.equals("PP21331"))
 if(w <= 1216) return 27;
 if(w > 1216) return 26;
if(p.equals("PP21332"))
 if(we <= 21810) return 27;
 if(we > 21810) return 28;
if(p.equals("PP21334"))
 if(w <= 1118) return 27;
 if(w > 1118) return 23;
if(p.equals("PP21336"))
 if(t <= 4.03)
  if(we <= 23140)
   if(we <= 22290)
    if(we <= 19880)
     if(w <= 1135)
      if(t <= 2.6) return 24;
      if(t > 2.6) return 23;
     if(w > 1135) return 29;
    if(we > 19880)
     if(t <= 3.55)
      if(we <= 21590) return 30;
      if(we > 21590) return 35;
     if(t > 3.55)
      if(w <= 1144) return 29;
      if(w > 1144) return 30;
   if(we > 22290)
    if(w <= 1113)
     if(w <= 1087) return 27;
     if(w > 1087) return 30;
    if(w > 1113)
     if(w <= 1126)
      if(we <= 22940) return 24;
      if(we > 22940) return 25;
     if(w > 1126)
      if(we <= 22720)
       if(we <= 22440) return 26;
       if(we > 22440) return 27;
      if(we > 22720) return 26;
  if(we > 23140)
   if(w <= 1122)
    if(we <= 24170)
     if(we <= 23260) return 25;
     if(we > 23260) return 27;
    if(we > 24170)
     if(we <= 24260)
      if(we <= 24240) return 28;
      if(we > 24240) return 31.5;
     if(we > 24260)
      if(we <= 24290) return 27;
      if(we > 24290)
       if(we <= 24490) return 28;
       if(we > 24490) return 29;
   if(w > 1122)
    if(w <= 1204)
     if(w <= 1144) return 29;
     if(w > 1144)
      if(we <= 24370) return 31;
      if(we > 24370) return 24;
    if(w > 1204)
     if(t <= 3.55)
      if(we <= 24940)
       if(we <= 24050) return 29;
       if(we > 24050) return 25;
      if(we > 24940) return 27;
     if(t > 3.55) return 25;
 if(t > 4.03)
  if(t <= 5.3)
   if(w <= 1111)
    if(t <= 4.7)
     if(w <= 1104)
      if(t <= 4.2) return 24;
      if(t > 4.2)
       if(we <= 21160) return 22;
       if(we > 21160) return 26;
     if(w > 1104)
      if(we <= 22760)
       if(we <= 21700) return 23;
       if(we > 21700) return 24;
      if(we > 22760) return 25;
    if(t > 4.7)
     if(w <= 995)
      if(we <= 19580) return 20;
      if(we > 19580) return 22;
     if(w > 995)
      if(we <= 19900) return 27;
      if(we > 19900) return 40;
   if(w > 1111)
    if(w <= 1122)
     if(we <= 22560) return 119;
     if(we > 22560) return 22;
    if(w > 1122)
     if(w <= 1224)
      if(w <= 1212) return 21;
      if(w > 1212) return 26;
     if(w > 1224)
      if(w <= 1240) return 25;
      if(w > 1240) return 22;
  if(t > 5.3)
   if(t <= 6.5)
    if(t <= 5.8)
     if(we <= 24920) return 23;
     if(we > 24920) return 25;
    if(t > 5.8)
     if(w <= 1169)
      if(w <= 1142)
       if(we <= 21220) return 19;
       if(we > 21220)
        if(we <= 22850) return 25;
        if(we > 22850) return 22;
      if(w > 1142)
       if(we <= 23910)
        if(we <= 23240) return 19;
        if(we > 23240) return 23;
       if(we > 23910) return 19;
     if(w > 1169)
      if(w <= 1215) return 21;
      if(w > 1215)
       if(we <= 18610) return 20;
       if(we > 18610) return 23;
   if(t > 6.5) return 24;
if(p.equals("PP21341")) return 23;
if(p.equals("PP21345")) return 26;
if(p.equals("PP21346"))
 if(t <= 7.6)
  if(we <= 22680)
   if(we <= 20890)
    if(w <= 1182)
     if(t <= 4.35)
      if(we <= 19880) return 21;
      if(we > 19880)
       if(we <= 20260) return 25;
       if(we > 20260) return 23;
     if(t > 4.35)
      if(t <= 5.8) return 23;
      if(t > 5.8) return 21;
    if(w > 1182)
     if(t <= 4.8) return 19;
     if(t > 4.8) return 25;
   if(we > 20890)
    if(we <= 21840)
     if(t <= 5.6) return 24;
     if(t > 5.6)
      if(we <= 21160) return 26;
      if(we > 21160) return 27;
    if(we > 21840) return 21;
  if(we > 22680)
   if(w <= 1197)
    if(w <= 1110)
     if(w <= 1105)
      if(we <= 22770) return 25;
      if(we > 22770) return 23;
     if(w > 1105)
      if(we <= 22890) return 24;
      if(we > 22890) return 22;
    if(w > 1110)
     if(we <= 23810) return 25;
     if(we > 23810) return 27;
   if(w > 1197)
    if(w <= 1240)
     if(we <= 24640)
      if(t <= 5.8) return 24;
      if(t > 5.8)
       if(we <= 24430) return 22;
       if(we > 24430) return 26;
     if(we > 24640) return 24;
    if(w > 1240) return 45;
 if(t > 7.6)
  if(w <= 1207)
   if(w <= 1099)
    if(we <= 21830)
     if(we <= 21160) return 20;
     if(we > 21160) return 22;
    if(we > 21830)
     if(we <= 21970) return 24.4;
     if(we > 21970) return 24;
   if(w > 1099)
    if(we <= 21770) return 28;
    if(we > 21770)
     if(we <= 23680) return 22;
     if(we > 23680) return 20;
  if(w > 1207)
   if(we <= 24380)
    if(we <= 23450) return 19;
    if(we > 23450) return 26;
   if(we > 24380)
    if(we <= 24540) return 30;
    if(we > 24540) return 23;
if(p.equals("PP21364"))
 if(we <= 23100) return 24;
 if(we > 23100) return 26;
if(p.equals("PP21368")) return 25;
if(p.equals("PP21379"))
 if(w <= 980) return 28;
 if(w > 980) return 26;
if(p.equals("PP21389"))
 if(we <= 20470)
  if(t <= 5.7)
   if(w <= 1022) return 24;
   if(w > 1022) return 19;
  if(t > 5.7)
   if(t <= 6.3)
    if(w <= 1134)
     if(w <= 1055) return 19;
     if(w > 1055) return 17;
    if(w > 1134)
     if(w <= 1145) return 18;
     if(w > 1145) return 20;
   if(t > 6.3) return 16;
 if(we > 20470)
  if(t <= 5.7)
   if(we <= 21430) return 26;
   if(we > 21430) return 23;
  if(t > 5.7)
   if(w <= 1079)
    if(w <= 1073) return 21;
    if(w > 1073) return 100;
   if(w > 1079)
    if(w <= 1174)
     if(we <= 22740)
      if(we <= 21690)
       if(w <= 1122) return 27;
       if(w > 1122) return 20;
      if(we > 21690)
       if(we <= 22510)
        if(w <= 1125) return 22;
        if(w > 1125)
         if(we <= 22010) return 24;
         if(we > 22010) return 22;
       if(we > 22510)
        if(we <= 22570) return 28;
        if(we > 22570) return 22;
     if(we > 22740)
      if(t <= 6.5)
       if(w <= 1145) return 31;
       if(w > 1145) return 23;
      if(t > 6.5) return 23;
    if(w > 1174)
     if(w <= 1183)
      if(we <= 22740) return 27;
      if(we > 22740)
       if(we <= 23760) return 19;
       if(we > 23760) return 28;
     if(w > 1183) return 20;
if(p.equals("PP21391"))
 if(we <= 20830)
  if(we <= 18600)
   if(t <= 1.75)
    if(we <= 15160)
     if(we <= 14420)
      if(we <= 13830) return 39;
      if(we > 13830) return 32;
     if(we > 14420)
      if(we <= 14750) return 31;
      if(we > 14750) return 32;
    if(we > 15160)
     if(w <= 1134)
      if(we <= 15320)
       if(we <= 15270) return 34;
       if(we > 15270) return 37;
      if(we > 15320)
       if(we <= 15360) return 36;
       if(we > 15360) return 29;
     if(w > 1134)
      if(we <= 15220) return 33.5;
      if(we > 15220) return 31;
   if(t > 1.75)
    if(t <= 1.91)
     if(we <= 17940)
      if(we <= 17900) return 164;
      if(we > 17900) return 28;
     if(we > 17940) return 30;
    if(t > 1.91)
     if(t <= 2.15) return 29;
     if(t > 2.15) return 30;
  if(we > 18600)
   if(w <= 1010)
    if(w <= 985)
     if(we <= 19800)
      if(we <= 19050) return 27;
      if(we > 19050) return 30;
     if(we > 19800)
      if(we <= 20060)
       if(we <= 19910) return 38;
       if(we > 19910) return 42;
      if(we > 20060)
       if(we <= 20160) return 33;
       if(we > 20160) return 31;
    if(w > 985) return 50;
   if(w > 1010)
    if(t <= 2.5)
     if(w <= 1034) return 105;
     if(w > 1034)
      if(w <= 1078)
       if(we <= 20430) return 36;
       if(we > 20430) return 35.5;
      if(w > 1078) return 30;
    if(t > 2.5)
     if(we <= 20500) return 30;
     if(we > 20500) return 29;
 if(we > 20830)
  if(w <= 1034)
   if(w <= 1016) return 40;
   if(w > 1016)
    if(we <= 21180)
     if(we <= 21020) return 42;
     if(we > 21020) return 38;
    if(we > 21180)
     if(we <= 21240) return 27;
     if(we > 21240) return 40;
  if(w > 1034)
   if(w <= 1112)
    if(w <= 1042) return 37;
    if(w > 1042)
     if(w <= 1065) return 36;
     if(w > 1065) return 34;
   if(w > 1112)
    if(we <= 21830) return 35;
    if(we > 21830)
     if(we <= 23220)
      if(w <= 1130) return 38;
      if(w > 1130) return 37;
     if(we > 23220)
      if(we <= 23380) return 40;
      if(we > 23380) return 38;
if(p.equals("PP21393"))
 if(we <= 21530)
  if(w <= 1144)
   if(t <= 4.6) return 25;
   if(t > 4.6)
    if(we <= 20540)
     if(t <= 6.3)
      if(w <= 1005)
       if(we <= 19590) return 20;
       if(we > 19590) return 21;
      if(w > 1005)
       if(we <= 16610) return 20;
       if(we > 16610) return 23;
     if(t > 6.3)
      if(w <= 1012)
       if(we <= 19760) return 23;
       if(we > 19760) return 21;
      if(w > 1012)
       if(we <= 19340) return 22;
       if(we > 19340) return 21;
    if(we > 20540)
     if(t <= 5.45)
      if(t <= 4.8) return 21;
      if(t > 4.8) return 25;
     if(t > 5.45)
      if(we <= 20740) return 130;
      if(we > 20740)
       if(we <= 21370)
        if(we <= 20810) return 22;
        if(we > 20810) return 20;
       if(we > 21370) return 22;
  if(w > 1144)
   if(w <= 1191)
    if(we <= 19780)
     if(w <= 1165) return 15;
     if(w > 1165) return 29;
    if(we > 19780)
     if(we <= 20900)
      if(t <= 6.1) return 28;
      if(t > 6.1) return 21;
     if(we > 20900)
      if(t <= 5.5) return 22;
      if(t > 5.5) return 20;
   if(w > 1191)
    if(t <= 5.45)
     if(we <= 18420)
      if(t <= 4.5)
       if(we <= 16860) return 19;
       if(we > 16860) return 22;
      if(t > 4.5) return 19;
     if(we > 18420) return 23;
    if(t > 5.45)
     if(t <= 6.1)
      if(we <= 18430)
       if(we <= 18110) return 18;
       if(we > 18110) return 43;
      if(we > 18430)
       if(we <= 18640) return 20;
       if(we > 18640)
        if(we <= 18930) return 19;
        if(we > 18930) return 17;
     if(t > 6.1) return 23;
 if(we > 21530)
  if(we <= 23010)
   if(t <= 4.8)
    if(we <= 22140)
     if(t <= 4.6) return 26;
     if(t > 4.6) return 22;
    if(we > 22140)
     if(w <= 1115)
      if(we <= 22760) return 24;
      if(we > 22760) return 25;
     if(w > 1115) return 23;
   if(t > 4.8)
    if(t <= 6.1)
     if(w <= 1133)
      if(we <= 22510) return 23;
      if(we > 22510) return 107;
     if(w > 1133) return 21;
    if(t > 6.1)
     if(t <= 6.5)
      if(we <= 22050) return 26;
      if(we > 22050) return 35;
     if(t > 6.5)
      if(t <= 7.5)
       if(w <= 1075)
        if(we <= 21800) return 24;
        if(we > 21800) return 21;
       if(w > 1075) return 37;
      if(t > 7.5) return 27;
  if(we > 23010)
   if(w <= 1172)
    if(we <= 23890)
     if(we <= 23510)
      if(we <= 23120) return 26;
      if(we > 23120) return 20;
     if(we > 23510)
      if(w <= 1144)
       if(t <= 6.3) return 23;
       if(t > 6.3) return 25;
      if(w > 1144)
       if(t <= 6.3) return 24;
       if(t > 6.3) return 22;
    if(we > 23890)
     if(we <= 23930) return 30;
     if(we > 23930) return 32;
   if(w > 1172)
    if(t <= 6.1)
     if(w <= 1216) return 24;
     if(w > 1216)
      if(t <= 4.6) return 25;
      if(t > 4.6) return 23;
    if(t > 6.1)
     if(we <= 24610)
      if(we <= 24370) return 23;
      if(we > 24370) return 26;
     if(we > 24610)
      if(t <= 6.5) return 28;
      if(t > 6.5) return 23;
if(p.equals("PP21394"))
 if(t <= 3.4)
  if(we <= 22500)
   if(w <= 1191)
    if(we <= 21270) return 25;
    if(we > 21270) return 32;
   if(w > 1191)
    if(w <= 1207) return 17;
    if(w > 1207) return 33;
  if(we > 22500)
   if(w <= 1218)
    if(we <= 23900)
     if(we <= 23320)
      if(we <= 22700) return 30;
      if(we > 22700) return 35;
     if(we > 23320) return 30;
    if(we > 23900)
     if(t <= 3) return 37;
     if(t > 3) return 33;
   if(w > 1218)
    if(we <= 23660)
     if(we <= 23440) return 30;
     if(we > 23440) return 34;
    if(we > 23660)
     if(we <= 23990) return 28;
     if(we > 23990) return 27;
 if(t > 3.4)
  if(we <= 24390)
   if(w <= 1144)
    if(w <= 1084)
     if(t <= 4.5)
      if(we <= 22800) return 26;
      if(we > 22800) return 25;
     if(t > 4.5) return 25;
    if(w > 1084)
     if(w <= 1127)
      if(we <= 21450)
       if(we <= 21340) return 24;
       if(we > 21340) return 28;
      if(we > 21450) return 22;
     if(w > 1127)
      if(we <= 23290) return 111;
      if(we > 23290) return 31;
   if(w > 1144)
    if(w <= 1173) return 25;
    if(w > 1173)
     if(we <= 23440)
      if(w <= 1190) return 25;
      if(w > 1190) return 22;
     if(we > 23440)
      if(we <= 23600) return 24;
      if(we > 23600)
       if(w <= 1190)
        if(we <= 23810) return 26;
        if(we > 23810) return 23;
       if(w > 1190) return 24;
  if(we > 24390)
   if(w <= 1188)
    if(we <= 24560)
     if(we <= 24450) return 27;
     if(we > 24450) return 37;
    if(we > 24560) return 28;
   if(w > 1188)
    if(we <= 24840)
     if(we <= 24670) return 26;
     if(we > 24670) return 23;
    if(we > 24840)
     if(we <= 24920)
      if(t <= 4.5) return 35;
      if(t > 4.5) return 24;
     if(we > 24920) return 28.5;
if(p.equals("PP21398"))
 if(t <= 2.7)
  if(t <= 2.2)
   if(w <= 1133)
    if(t <= 1.75) return 30;
    if(t > 1.75)
     if(w <= 1096) return 40;
     if(w > 1096) return 32;
   if(w > 1133)
    if(we <= 15510) return 35;
    if(we > 15510) return 31;
  if(t > 2.2)
   if(we <= 23940)
    if(we <= 22710) return 31;
    if(we > 22710)
     if(we <= 23430) return 33;
     if(we > 23430)
      if(we <= 23540) return 153;
      if(we > 23540) return 35;
   if(we > 23940) return 36;
 if(t > 2.7)
  if(w <= 1213)
   if(we <= 24320)
    if(w <= 1187)
     if(t <= 3.55)
      if(we <= 20460)
       if(w <= 1034) return 40;
       if(w > 1034) return 25;
      if(we > 20460)
       if(t <= 3.1) return 31;
       if(t > 3.1) return 25;
     if(t > 3.55)
      if(w <= 1113) return 26;
      if(w > 1113) return 22;
    if(w > 1187)
     if(w <= 1201) return 28;
     if(w > 1201)
      if(we <= 22400) return 25;
      if(we > 22400)
       if(we <= 23830)
        if(we <= 23550) return 26;
        if(we > 23550) return 29;
       if(we > 23830)
        if(we <= 24220) return 30;
        if(we > 24220) return 26;
   if(we > 24320)
    if(t <= 3.1)
     if(t <= 2.9) return 30;
     if(t > 2.9)
      if(we <= 25030)
       if(we <= 24530)
        if(we <= 24420) return 34;
        if(we > 24420) return 32;
       if(we > 24530) return 28;
      if(we > 25030)
       if(we <= 25070) return 30;
       if(we > 25070) return 42;
    if(t > 3.1)
     if(we <= 24570) return 27;
     if(we > 24570) return 23;
  if(w > 1213)
   if(we <= 24230)
    if(t <= 2.9) return 37;
    if(t > 2.9) return 24;
   if(we > 24230)
    if(we <= 24490)
     if(we <= 24360)
      if(we <= 24260) return 29;
      if(we > 24260) return 26;
     if(we > 24360)
      if(we <= 24400) return 28.5;
      if(we > 24400) return 27.5;
    if(we > 24490)
     if(we <= 24660) return 34;
     if(we > 24660) return 28;
if(p.equals("PP21404"))
 if(we <= 20780)
  if(w <= 1002)
   if(we <= 20680)
    if(we <= 18980) return 19;
    if(we > 18980)
     if(we <= 20640)
      if(we <= 19870) return 25;
      if(we > 19870) return 19;
     if(we > 20640) return 29;
   if(we > 20680)
    if(we <= 20720) return 19.6666666666667;
    if(we > 20720)
     if(we <= 20750) return 21;
     if(we > 20750) return 20;
  if(w > 1002) return 18;
 if(we > 20780)
  if(t <= 6) return 23;
  if(t > 6)
   if(we <= 20850)
    if(we <= 20810) return 21.5;
    if(we > 20810) return 18;
   if(we > 20850)
    if(we <= 21000) return 20.5;
    if(we > 21000) return 18;
if(p.equals("PP21405"))
 if(we <= 23670)
  if(we <= 19960) return 20;
  if(we > 19960) return 22;
 if(we > 23670) return 21;
if(p.equals("PP21408"))
 if(t <= 7)
  if(w <= 1138) return 20;
  if(w > 1138) return 25;
 if(t > 7)
  if(we <= 20890) return 20;
  if(we > 20890) return 16;
if(p.equals("PP21409"))
 if(w <= 1095)
  if(we <= 21970)
   if(we <= 20840) return 20;
   if(we > 20840)
    if(we <= 21720)
     if(we <= 21620) return 25;
     if(we > 21620)
      if(we <= 21690) return 22;
      if(we > 21690) return 24;
    if(we > 21720)
     if(we <= 21860)
      if(we <= 21750) return 23.3333333333333;
      if(we > 21750) return 19;
     if(we > 21860)
      if(we <= 21920) return 30;
      if(we > 21920) return 22;
  if(we > 21970)
   if(we <= 22160)
    if(we <= 22120)
     if(we <= 22040) return 27;
     if(we > 22040) return 24;
    if(we > 22120) return 27;
   if(we > 22160)
    if(we <= 22260) return 25;
    if(we > 22260) return 28;
 if(w > 1095)
  if(we <= 22720) return 22;
  if(we > 22720)
   if(we <= 22810) return 20;
   if(we > 22810)
    if(we <= 22940) return 31;
    if(we > 22940) return 24;
if(p.equals("PP21414"))
 if(t <= 5.7)
  if(we <= 19420)
   if(we <= 19080)
    if(we <= 17810) return 17;
    if(we > 17810)
     if(we <= 18160) return 23;
     if(we > 18160) return 22;
   if(we > 19080) return 24;
  if(we > 19420)
   if(we <= 19580)
    if(we <= 19540) return 21;
    if(we > 19540)
     if(we <= 19560) return 24.5;
     if(we > 19560) return 108;
   if(we > 19580)
    if(we <= 19680)
     if(we <= 19630) return 22;
     if(we > 19630) return 31;
    if(we > 19680) return 23;
 if(t > 5.7)
  if(we <= 22630) return 16;
  if(we > 22630) return 20;
if(p.equals("PP21416"))
 if(t <= 5) return 24;
 if(t > 5)
  if(we <= 23580)
   if(we <= 22180) return 21;
   if(we > 22180) return 23;
  if(we > 23580)
   if(we <= 23890)
    if(we <= 23770) return 25;
    if(we > 23770) return 24;
   if(we > 23890) return 22;
if(p.equals("PP21419")) return 29;
if(p.equals("PP21420"))
 if(we <= 21800)
  if(we <= 20420)
   if(we <= 19930)
    if(w <= 1036)
     if(we <= 18390)
      if(we <= 18060)
       if(we <= 17550) return 19;
       if(we > 17550) return 20;
      if(we > 18060)
       if(we <= 18190) return 22;
       if(we > 18190) return 19.5;
     if(we > 18390)
      if(we <= 19640)
       if(we <= 18670) return 21;
       if(we > 18670)
        if(we <= 19470)
         if(we <= 18900) return 22;
         if(we > 18900) return 21;
        if(we > 19470) return 19;
      if(we > 19640)
       if(we <= 19800) return 18;
       if(we > 19800) return 24;
    if(w > 1036)
     if(w <= 1173) return 20;
     if(w > 1173) return 21;
   if(we > 19930)
    if(we <= 20040)
     if(t <= 5.45) return 34;
     if(t > 5.45)
      if(we <= 20000) return 23.5;
      if(we > 20000) return 24;
    if(we > 20040)
     if(we <= 20260)
      if(we <= 20160) return 23;
      if(we > 20160)
       if(we <= 20230) return 20.5;
       if(we > 20230) return 22;
     if(we > 20260)
      if(we <= 20290) return 20;
      if(we > 20290) return 21;
  if(we > 20420)
   if(we <= 20740)
    if(we <= 20610) return 22;
    if(we > 20610)
     if(we <= 20670) return 23;
     if(we > 20670)
      if(we <= 20710) return 24;
      if(we > 20710) return 22.6666666666667;
   if(we > 20740)
    if(we <= 21610)
     if(we <= 21340)
      if(we <= 21170) return 21;
      if(we > 21170) return 33;
     if(we > 21340)
      if(we <= 21550) return 21;
      if(we > 21550) return 22;
    if(we > 21610)
     if(we <= 21720) return 21.5;
     if(we > 21720) return 21;
 if(we > 21800)
  if(we <= 22570)
   if(we <= 22380)
    if(we <= 22280)
     if(we <= 22100)
      if(we <= 21880) return 24;
      if(we > 21880) return 20;
     if(we > 22100)
      if(we <= 22240) return 22;
      if(we > 22240) return 23.5;
    if(we > 22280)
     if(we <= 22330) return 20;
     if(we > 22330) return 22.5;
   if(we > 22380)
    if(we <= 22470)
     if(we <= 22430) return 25;
     if(we > 22430)
      if(we <= 22450) return 27;
      if(we > 22450) return 22;
    if(we > 22470)
     if(we <= 22500)
      if(we <= 22480) return 23;
      if(we > 22480) return 27.25;
     if(we > 22500)
      if(we <= 22550) return 27;
      if(we > 22550) return 25;
  if(we > 22570)
   if(we <= 22720)
    if(we <= 22640)
     if(we <= 22600) return 24;
     if(we > 22600) return 25.3333333333333;
    if(we > 22640) return 23;
   if(we > 22720)
    if(w <= 1137)
     if(we <= 22790)
      if(we <= 22750) return 24;
      if(we > 22750) return 20;
     if(we > 22790) return 36;
    if(w > 1137) return 24;
if(p.equals("PP21423"))
 if(we <= 16720)
  if(t <= 2.4)
   if(we <= 15350)
    if(we <= 14510) return 26;
    if(we > 14510) return 30;
   if(we > 15350)
    if(w <= 1156) return 32;
    if(w > 1156) return 26;
  if(t > 2.4) return 23;
 if(we > 16720)
  if(w <= 1194)
   if(w <= 1072) return 25;
   if(w > 1072)
    if(w <= 1136) return 33;
    if(w > 1136) return 31;
  if(w > 1194)
   if(we <= 17220) return 30;
   if(we > 17220)
    if(w <= 1199) return 32;
    if(w > 1199)
     if(we <= 17560) return 33;
     if(we > 17560) return 29;
if(p.equals("PP21429")) return 29;
if(p.equals("PP21432"))
 if(t <= 3.8) return 27;
 if(t > 3.8)
  if(w <= 1070) return 21;
  if(w > 1070) return 107;
if(p.equals("PP21433"))
 if(we <= 22460) return 23;
 if(we > 22460)
  if(we <= 23640)
   if(we <= 23570) return 24;
   if(we > 23570) return 26;
  if(we > 23640) return 30;
if(p.equals("PP21456"))
 if(we <= 16810)
  if(we <= 16500) return 23;
  if(we > 16500)
   if(we <= 16720) return 27;
   if(we > 16720) return 31;
 if(we > 16810)
  if(w <= 1204)
   if(we <= 17260) return 45;
   if(we > 17260)
    if(we <= 17550) return 34;
    if(we > 17550) return 25;
  if(w > 1204)
   if(w <= 1213) return 32;
   if(w > 1213) return 24;
if(p.equals("PP21469")) return 20;
if(p.equals("PP21473"))
 if(we <= 21100)
  if(w <= 1021)
   if(we <= 18530) return 26;
   if(we > 18530) return 24;
  if(w > 1021)
   if(we <= 18580)
    if(t <= 4.7) return 18;
    if(t > 4.7)
     if(t <= 5.45) return 19;
     if(t > 5.45) return 18;
   if(we > 18580)
    if(we <= 19970) return 20;
    if(we > 19970)
     if(w <= 1048) return 20;
     if(w > 1048)
      if(we <= 20600) return 22;
      if(we > 20600) return 21;
 if(we > 21100)
  if(we <= 22920)
   if(w <= 1137)
    if(t <= 5.5) return 23;
    if(t > 5.5) return 25;
   if(w > 1137) return 33;
  if(we > 22920) return 24;
if(p.equals("PP21475"))
 if(we <= 17450) return 26;
 if(we > 17450) return 25;
if(p.equals("PP21476"))
 if(we <= 18860)
  if(t <= 5.7)
   if(we <= 18150) return 20;
   if(we > 18150) return 24;
  if(t > 5.7)
   if(we <= 16620)
    if(we <= 16530) return 16;
    if(we > 16530) return 20;
   if(we > 16620)
    if(w <= 1074) return 20;
    if(w > 1074)
     if(w <= 1120)
      if(we <= 16860) return 18;
      if(we > 16860) return 15;
     if(w > 1120) return 17;
 if(we > 18860)
  if(t <= 5.8)
   if(t <= 4.7)
    if(we <= 24200)
     if(t <= 4.2) return 26;
     if(t > 4.2) return 119;
    if(we > 24200)
     if(we <= 24720) return 25;
     if(we > 24720) return 21;
   if(t > 4.7)
    if(w <= 1051)
     if(we <= 19550)
      if(w <= 991) return 25;
      if(w > 991) return 21;
     if(we > 19550) return 24;
    if(w > 1051) return 23;
  if(t > 5.8)
   if(w <= 1079)
    if(we <= 21650)
     if(w <= 1036)
      if(w <= 1007) return 25;
      if(w > 1007)
       if(w <= 1030)
        if(we <= 20640) return 22;
        if(we > 20640) return 21;
       if(w > 1030) return 23;
     if(w > 1036)
      if(w <= 1047)
       if(we <= 20550) return 17;
       if(we > 20550) return 21;
      if(w > 1047)
       if(we <= 20610) return 19;
       if(we > 20610) return 22;
    if(we > 21650)
     if(we <= 22020) return 32;
     if(we > 22020)
      if(we <= 22070) return 18;
      if(we > 22070) return 19;
   if(w > 1079)
    if(t <= 5.99)
     if(we <= 22690)
      if(w <= 1115) return 23;
      if(w > 1115) return 25;
     if(we > 22690)
      if(w <= 1159) return 18;
      if(w > 1159) return 23;
    if(t > 5.99)
     if(we <= 22450) return 18;
     if(we > 22450) return 22;
if(p.equals("PP21478")) return 21;
if(p.equals("PP21484"))
 if(w <= 1160)
  if(t <= 3.6) return 22;
  if(t > 3.6)
   if(we <= 22280)
    if(t <= 3.8) return 25;
    if(t > 3.8)
     if(w <= 1062)
      if(w <= 1057) return 26;
      if(w > 1057) return 28;
     if(w > 1062)
      if(we <= 22060) return 32;
      if(we > 22060) return 25;
   if(we > 22280)
    if(w <= 1086) return 39;
    if(w > 1086)
     if(we <= 22760) return 25;
     if(we > 22760) return 23;
 if(w > 1160)
  if(we <= 12740)
   if(we <= 11700) return 20;
   if(we > 11700)
    if(we <= 12050)
     if(we <= 11790) return 19;
     if(we > 11790) return 24;
    if(we > 12050) return 20;
  if(we > 12740)
   if(we <= 13880) return 37;
   if(we > 13880) return 24;
if(p.equals("PP21485"))
 if(we <= 22050) return 22;
 if(we > 22050) return 33;
if(p.equals("PP21486"))
 if(we <= 22520) return 21;
 if(we > 22520) return 23;
if(p.equals("PP21487")) return 28;
if(p.equals("PP21489")) return 24;
if(p.equals("PP21493")) return 24;
if(p.equals("PP21504")) return 27;
if(p.equals("PP21508")) return 23;
if(p.equals("PP21516")) return 25;
if(p.equals("PP21524")) return 31;
if(p.equals("PP21526"))
 if(w <= 1215)
  if(w <= 1196)
   if(we <= 22460)
    if(we <= 22420) return 29;
    if(we > 22420) return 30;
   if(we > 22460)
    if(t <= 3.1)
     if(we <= 22520) return 37;
     if(we > 22520) return 27;
    if(t > 3.1) return 22;
  if(w > 1196)
   if(we <= 17030)
    if(we <= 16720) return 27;
    if(we > 16720) return 31;
   if(we > 17030)
    if(w <= 1201)
     if(we <= 17760) return 30;
     if(we > 17760)
      if(we <= 18080) return 32.5;
      if(we > 18080) return 33;
    if(w > 1201) return 27;
 if(w > 1215)
  if(t <= 3.3) return 24;
  if(t > 3.3) return 23;
if(p.equals("PP21529"))
 if(we <= 16490) return 27;
 if(we > 16490) return 33;
if(p.equals("PP21531")) return 22;
if(p.equals("PP21533")) return 18;
if(p.equals("PP21551")) return 26;
if(p.equals("PP21557"))
 if(t <= 4.2) return 19;
 if(t > 4.2)
  if(w <= 1219) return 26;
  if(w > 1219) return 20;
if(p.equals("PP21563")) return 26;
if(p.equals("PP21576")) return 33;
if(p.equals("PP21577"))
 if(we <= 17020)
  if(t <= 2.9)
   if(t <= 2.5) return 27;
   if(t > 2.5) return 22;
  if(t > 2.9) return 20;
 if(we > 17020)
  if(t <= 3.1)
   if(we <= 23270) return 28;
   if(we > 23270) return 30;
  if(t > 3.1)
   if(w <= 1193)
    if(t <= 3.5)
     if(we <= 23450)
      if(we <= 21980) return 25;
      if(we > 21980) return 28;
     if(we > 23450)
      if(we <= 24230) return 29;
      if(we > 24230) return 31;
    if(t > 3.5) return 24;
   if(w > 1193)
    if(t <= 3.235)
     if(w <= 1206) return 204;
     if(w > 1206) return 121;
    if(t > 3.235)
     if(we <= 25010)
      if(we <= 24340) return 26;
      if(we > 24340) return 28;
     if(we > 25010)
      if(we <= 25280) return 37;
      if(we > 25280) return 24;
if(p.equals("PP21578"))
 if(we <= 21670) return 24;
 if(we > 21670) return 23;
if(p.equals("PP21580"))
 if(we <= 23680)
  if(we <= 22440)
   if(we <= 21950) return 22;
   if(we > 21950) return 31;
  if(we > 22440) return 35;
 if(we > 23680) return 43;
if(p.equals("PP21592"))
 if(w <= 1242) return 30;
 if(w > 1242) return 29;
if(p.equals("PP21597")) return 33;
if(p.equals("PP21599")) return 23;
if(p.equals("PP21601"))
 if(t <= 5.45) return 27;
 if(t > 5.45) return 24;
if(p.equals("PP21605")) return 30;
if(p.equals("PP21606"))
 if(we <= 22930) return 24;
 if(we > 22930) return 25;
if(p.equals("PP21620"))
 if(t <= 3.235) return 28;
 if(t > 3.235)
  if(we <= 22020) return 22;
  if(we > 22020) return 125;
if(p.equals("PP21628")) return 25;
if(p.equals("PP21632")) return 45;
if(p.equals("PP21644")) return 24;
if(p.equals("PP21647")) return 21;
if(p.equals("PP21667"))
 if(t <= 2.1) return 28;
 if(t > 2.1) return 35;
if(p.equals("PP21671")) return 30;
if(p.equals("PP21677")) return 42;
if(p.equals("PP21684")) return 40;
if(p.equals("PP21689")) return 19;
if(p.equals("PP21690")) return 22;
if(p.equals("PP21694"))
 if(we <= 22350) return 29;
 if(we > 22350)
  if(we <= 23680) return 35;
  if(we > 23680) return 34;
if(p.equals("PP21701"))
 if(t <= 2.7) return 26;
 if(t > 2.7)
  if(w <= 1100) return 24;
  if(w > 1100)
   if(we <= 13360) return 18;
   if(we > 13360)
    if(we <= 13680) return 21;
    if(we > 13680) return 119;
if(p.equals("PP21702"))
 if(t <= 2.6)
  if(we <= 16340)
   if(w <= 1104) return 28;
   if(w > 1104)
    if(w <= 1163)
     if(we <= 16100)
      if(we <= 15520) return 26;
      if(we > 15520) return 29;
     if(we > 16100) return 30;
    if(w > 1163) return 29;
  if(we > 16340)
   if(we <= 16930)
    if(t <= 1.9)
     if(we <= 16420) return 31;
     if(we > 16420) return 28;
    if(t > 1.9) return 31;
   if(we > 16930)
    if(w <= 1087) return 30;
    if(w > 1087)
     if(we <= 23230) return 35;
     if(we > 23230) return 37;
 if(t > 2.6)
  if(t <= 3.6)
   if(we <= 24690) return 29;
   if(we > 24690) return 25;
  if(t > 3.6)
   if(we <= 23020) return 25;
   if(we > 23020) return 30;
if(p.equals("PP21726")) return 20;
if(p.equals("PP21730"))
 if(t <= 4.5)
  if(w <= 1206)
   if(we <= 23960) return 26;
   if(we > 23960) return 24;
  if(w > 1206)
   if(we <= 16690) return 19;
   if(we > 16690) return 20;
 if(t > 4.5) return 23;
if(p.equals("PP21752")) return 31;
if(p.equals("PP21753")) return 24;
if(p.equals("PP21754"))
 if(t <= 2.5)
  if(w <= 1235)
   if(we <= 23830) return 31;
   if(we > 23830) return 33;
  if(w > 1235) return 30;
 if(t > 2.5)
  if(we <= 24460) return 29;
  if(we > 24460) return 31;
if(p.equals("PP21756")) return 20;
if(p.equals("PP21762")) return 28;
if(p.equals("PP21769")) return 39;
if(p.equals("PP21773")) return 49;
if(p.equals("PP21777"))
 if(we <= 22620)
  if(we <= 22300) return 24;
  if(we > 22300) return 20;
 if(we > 22620)
  if(we <= 23860) return 23;
  if(we > 23860) return 24.3333333333333;
if(p.equals("PP21779"))
 if(t <= 3.1) return 38;
 if(t > 3.1) return 28;
if(p.equals("PP21782"))
 if(we <= 21380) return 20;
 if(we > 21380) return 23;
if(p.equals("PP21789")) return 30;
if(p.equals("PP21790")) return 21;
if(p.equals("PP21791")) return 27;
if(p.equals("PP21792")) return 22;
if(p.equals("PP21794"))
 if(we <= 16920) return 23;
 if(we > 16920) return 27;
if(p.equals("PP21798")) return 26;
if(p.equals("PP21802"))
 if(we <= 23690) return 123;
 if(we > 23690) return 27;
if(p.equals("PP21805"))
 if(we <= 16570)
  if(we <= 15640)
   if(we <= 14990)
    if(we <= 14940) return 19;
    if(we > 14940) return 21;
   if(we > 14990) return 19;
  if(we > 15640) return 21;
 if(we > 16570)
  if(we <= 16770)
   if(we <= 16690)
    if(we <= 16630) return 23;
    if(we > 16630) return 20.75;
   if(we > 16690)
    if(we <= 16740) return 21;
    if(we > 16740) return 22.5;
  if(we > 16770) return 22;
if(p.equals("PP21807")) return 20;
if(p.equals("PP21809")) return 26;
if(p.equals("PP21811")) return 21;
if(p.equals("PP21814")) return 22;
if(p.equals("PP21831")) return 31;
if(p.equals("PP21833")) return 25;
if(p.equals("PP21855")) return 21;
if(p.equals("PP21880"))
 if(t <= 2.9)
  if(we <= 13210) return 20;
  if(we > 13210) return 25;
 if(t > 2.9) return 31;
if(p.equals("PP21884"))
 if(w <= 1126) return 20;
 if(w > 1126)
  if(we <= 18020) return 22;
  if(we > 18020) return 25;
if(p.equals("PP21886")) return 26;
if(p.equals("PP21890")) return 21;
if(p.equals("PP21894"))
 if(we <= 11960)
  if(we <= 11550)
   if(w <= 1189)
    if(we <= 11160)
     if(t <= 3.25) return 27;
     if(t > 3.25) return 20;
    if(we > 11160)
     if(we <= 11340) return 19;
     if(we > 11340) return 17;
   if(w > 1189)
    if(we <= 10820) return 19;
    if(we > 10820)
     if(we <= 11230) return 25;
     if(we > 11230) return 21;
  if(we > 11550)
   if(w <= 1182)
    if(we <= 11830) return 20;
    if(we > 11830)
     if(we <= 11900) return 21;
     if(we > 11900) return 20;
   if(w > 1182)
    if(we <= 11690) return 19;
    if(we > 11690) return 20;
 if(we > 11960)
  if(we <= 14080)
   if(w <= 1182)
    if(w <= 1132) return 22;
    if(w > 1132)
     if(we <= 13660)
      if(we <= 12070)
       if(we <= 11990) return 21;
       if(we > 11990) return 19.5;
      if(we > 12070) return 34;
     if(we > 13660)
      if(t <= 2.8)
       if(we <= 13730) return 19;
       if(we > 13730) return 22;
      if(t > 2.8)
       if(we <= 13675) return 19;
       if(we > 13675) return 21;
   if(w > 1182)
    if(t <= 2.8)
     if(we <= 12120) return 19;
     if(we > 12120) return 21;
    if(t > 2.8)
     if(we <= 12590) return 20;
     if(we > 12590) return 112;
  if(we > 14080)
   if(t <= 2.15)
    if(we <= 16940) return 29;
    if(we > 16940) return 28.5;
   if(t > 2.15) return 24;
if(p.equals("PP21895"))
 if(t <= 4.2) return 17;
 if(t > 4.2) return 16;
if(p.equals("PP21896"))
 if(t <= 4.2)
  if(we <= 12760) return 17;
  if(we > 12760) return 16;
 if(t > 4.2) return 18;
if(p.equals("PP21901")) return 47;
if(p.equals("PP21903"))
 if(w <= 1170)
  if(we <= 22570)
   if(we <= 21900) return 23;
   if(we > 21900) return 18;
  if(we > 22570) return 26;
 if(w > 1170)
  if(t <= 6.1) return 21;
  if(t > 6.1) return 24;
if(p.equals("PP21904"))
 if(w <= 1189)
  if(we <= 13620)
   if(t <= 4.7)
    if(we <= 13140)
     if(we <= 12810)
      if(w <= 1182)
       if(we <= 12430) return 16;
       if(we > 12430) return 17;
      if(w > 1182) return 16;
     if(we > 12810) return 18;
    if(we > 13140) return 16;
   if(t > 4.7)
    if(we <= 12890)
     if(we <= 12390) return 18;
     if(we > 12390) return 14;
    if(we > 12890)
     if(w <= 1182)
      if(we <= 13070) return 16;
      if(we > 13070)
       if(we <= 13470) return 18;
       if(we > 13470) return 19;
     if(w > 1182) return 16;
  if(we > 13620)
   if(w <= 1135) return 18;
   if(w > 1135)
    if(we <= 14230)
     if(t <= 4.7)
      if(we <= 13760) return 19;
      if(we > 13760) return 18;
     if(t > 4.7)
      if(we <= 13880) return 20;
      if(we > 13880) return 18;
    if(we > 14230)
     if(we <= 14600) return 17;
     if(we > 14600)
      if(we <= 14910) return 16;
      if(we > 14910) return 19;
 if(w > 1189)
  if(w <= 1207)
   if(t <= 3.7) return 21;
   if(t > 3.7)
    if(we <= 11090) return 19;
    if(we > 11090) return 15;
  if(w > 1207)
   if(we <= 16760) return 18;
   if(we > 16760) return 22;
if(p.equals("PP21906"))
 if(we <= 14080)
  if(we <= 13910) return 19;
  if(we > 13910) return 21;
 if(we > 14080) return 24;
if(p.equals("PP21908")) return 19;
if(p.equals("PP21909")) return 26;
if(p.equals("PP21911"))
 if(we <= 13650)
  if(t <= 4)
   if(we <= 11930) return 16;
   if(we > 11930) return 18;
  if(t > 4)
   if(we <= 12900) return 15;
   if(we > 12900)
    if(we <= 13645) return 17;
    if(we > 13645) return 18;
 if(we > 13650)
  if(w <= 1162)
   if(w <= 1100) return 18;
   if(w > 1100)
    if(we <= 15880) return 21;
    if(we > 15880) return 17;
  if(w > 1162)
   if(t <= 3.4)
    if(t <= 2.7) return 27;
    if(t > 2.7) return 19;
   if(t > 3.4)
    if(t <= 4.7)
     if(t <= 3.7) return 19;
     if(t > 3.7)
      if(we <= 13685) return 19;
      if(we > 13685) return 20;
    if(t > 4.7) return 18;
if(p.equals("PP21912"))
 if(we <= 12060)
  if(t <= 2.8)
   if(we <= 11470) return 17;
   if(we > 11470) return 15;
  if(t > 2.8)
   if(w <= 1132) return 28;
   if(w > 1132) return 18;
 if(we > 12060)
  if(w <= 1125)
   if(we <= 13230) return 20;
   if(we > 13230) return 22;
  if(w > 1125) return 20;
if(p.equals("PP21913"))
 if(we <= 13730)
  if(t <= 2.8)
   if(w <= 1182)
    if(we <= 11870)
     if(we <= 11840) return 20;
     if(we > 11840) return 19;
    if(we > 11870)
     if(we <= 12030) return 21;
     if(we > 12030)
      if(we <= 13660) return 34;
      if(we > 13660) return 19;
   if(w > 1182)
    if(w <= 1189)
     if(we <= 11500)
      if(we <= 11160) return 27;
      if(we > 11160)
       if(we <= 11310) return 21;
       if(we > 11310) return 17;
     if(we > 11500)
      if(we <= 12120) return 19;
      if(we > 12120) return 21;
    if(w > 1189)
     if(we <= 10500) return 19;
     if(we > 10500) return 20;
  if(t > 2.8)
   if(w <= 1119)
    if(we <= 12510) return 28;
    if(we > 12510) return 22;
   if(w > 1119)
    if(t <= 3.235)
     if(we <= 11570)
      if(w <= 1187) return 17;
      if(w > 1187)
       if(we <= 11170) return 17;
       if(we > 11170) return 18;
     if(we > 11570) return 21;
    if(t > 3.235)
     if(w <= 1171) return 19;
     if(w > 1171) return 19.5;
 if(we > 13730)
  if(t <= 2.6)
   if(w <= 1209) return 29;
   if(w > 1209)
    if(we <= 16690)
     if(t <= 2.43) return 26;
     if(t > 2.43)
      if(we <= 16330) return 25;
      if(we > 16330) return 23;
    if(we > 16690)
     if(we <= 16820) return 24;
     if(we > 16820) return 22;
  if(t > 2.6)
   if(t <= 3.7)
    if(w <= 1219)
     if(we <= 13770) return 23;
     if(we > 13770) return 22;
    if(w > 1219)
     if(we <= 16500) return 19;
     if(we > 16500) return 22;
   if(t > 3.7)
    if(t <= 4.2) return 16;
    if(t > 4.2) return 18;
if(p.equals("PP21914"))
 if(t <= 2.8)
  if(we <= 11230)
   if(we <= 10900) return 18;
   if(we > 10900)
    if(we <= 11060) return 20;
    if(we > 11060) return 24.5;
  if(we > 11230)
   if(we <= 11980) return 19;
   if(we > 11980) return 20;
 if(t > 2.8)
  if(we <= 11780)
   if(we <= 11180)
    if(t <= 3.235)
     if(we <= 10380) return 20;
     if(we > 10380)
      if(we <= 10650) return 18;
      if(we > 10650)
       if(we <= 10860) return 17;
       if(we > 10860) return 18;
    if(t > 3.235)
     if(we <= 10870) return 18;
     if(we > 10870)
      if(we <= 10960) return 21;
      if(we > 10960) return 31;
   if(we > 11180)
    if(t <= 3.235)
     if(we <= 11360)
      if(we <= 11240) return 19;
      if(we > 11240)
       if(we <= 11301) return 18.6;
       if(we > 11301) return 19;
     if(we > 11360)
      if(we <= 11570) return 20;
      if(we > 11570)
       if(we <= 11610) return 21;
       if(we > 11610) return 20;
    if(t > 3.235)
     if(we <= 11560)
      if(we <= 11545) return 17;
      if(we > 11545) return 20;
     if(we > 11560) return 15;
  if(we > 11780)
   if(t <= 3.235)
    if(w <= 1189) return 18;
    if(w > 1189)
     if(we <= 12000)
      if(we <= 11900) return 20.6666666666667;
      if(we > 11900) return 22;
     if(we > 12000) return 20;
   if(t > 3.235)
    if(we <= 12990)
     if(w <= 1189) return 18;
     if(w > 1189)
      if(we <= 11830) return 17.5;
      if(we > 11830)
       if(we <= 11930) return 17;
       if(we > 11930) return 19;
    if(we > 12990)
     if(we <= 13460) return 20;
     if(we > 13460)
      if(we <= 13680) return 24;
      if(we > 13680) return 22;
if(p.equals("PP21915"))
 if(we <= 12870)
  if(we <= 11700) return 20;
  if(we > 11700) return 17;
 if(we > 12870) return 20;
if(p.equals("PP21917"))
 if(w <= 1189)
  if(t <= 3.7)
   if(we <= 13530) return 20;
   if(we > 13530)
    if(we <= 13770) return 24;
    if(we > 13770) return 22;
  if(t > 3.7)
   if(we <= 13630) return 16;
   if(we > 13630)
    if(we <= 14260) return 19;
    if(we > 14260) return 20;
 if(w > 1189)
  if(w <= 1219) return 19;
  if(w > 1219) return 36;
if(p.equals("PP21918")) return 29;
if(p.equals("PP21920"))
 if(t <= 3.7)
  if(w <= 1185) return 21;
  if(w > 1185)
   if(we <= 16330)
    if(we <= 15690) return 24;
    if(we > 15690) return 25;
   if(we > 16330) return 24;
 if(t > 3.7)
  if(t <= 4.7)
   if(we <= 14380)
    if(we <= 13990) return 16;
    if(we > 13990) return 27;
   if(we > 14380)
    if(t <= 4.2)
     if(we <= 16740)
      if(we <= 16690) return 16;
      if(we > 16690) return 20;
     if(we > 16740) return 19;
    if(t > 4.2)
     if(we <= 16450) return 19;
     if(we > 16450) return 20;
  if(t > 4.7)
   if(w <= 1130) return 19;
   if(w > 1130) return 17;
if(p.equals("PP21922"))
 if(t <= 4.2)
  if(we <= 14150)
   if(we <= 12940) return 17;
   if(we > 12940) return 19;
  if(we > 14150) return 20;
 if(t > 4.2)
  if(we <= 15380) return 16;
  if(we > 15380) return 18;
if(p.equals("PP21923"))
 if(w <= 1193)
  if(t <= 3.7)
   if(we <= 15420)
    if(we <= 15280) return 21;
    if(we > 15280) return 22;
   if(we > 15420)
    if(we <= 15600) return 19;
    if(we > 15600) return 18;
  if(t > 3.7) return 20;
 if(w > 1193)
  if(t <= 4.2)
   if(we <= 16860)
    if(t <= 3.3) return 27;
    if(t > 3.3) return 18;
   if(we > 16860) return 20;
  if(t > 4.2)
   if(we <= 16170) return 16;
   if(we > 16170)
    if(we <= 16580) return 19;
    if(we > 16580) return 18;
if(p.equals("PP21926")) return 33;
if(p.equals("PP21927"))
 if(we <= 16700) return 26;
 if(we > 16700)
  if(we <= 16800) return 27;
  if(we > 16800) return 25;
if(p.equals("PP21931")) return 23;
if(p.equals("PP21932")) return 30;
if(p.equals("PP21933")) return 27;
if(p.equals("PP21935")) return 22;
if(p.equals("PP21936"))
 if(t <= 3.6) return 23;
 if(t > 3.6) return 16;
if(p.equals("PP21937"))
 if(we <= 13910) return 21;
 if(we > 13910) return 22;
if(p.equals("PP21938")) return 18;
if(p.equals("PP21939")) return 23;
if(p.equals("PP21940"))
 if(w <= 1078)
  if(t <= 3)
   if(we <= 14660) return 23;
   if(we > 14660) return 25;
  if(t > 3)
   if(we <= 15040)
    if(we <= 14800)
     if(we <= 14720) return 22;
     if(we > 14720) return 24;
    if(we > 14800) return 22;
   if(we > 15040) return 21;
 if(w > 1078)
  if(we <= 16620)
   if(t <= 3.3)
    if(w <= 1081)
     if(we <= 12940) return 20;
     if(we > 12940) return 18;
    if(w > 1081) return 32;
   if(t > 3.3)
    if(w <= 1187) return 103;
    if(w > 1187)
     if(we <= 14800)
      if(t <= 3.7) return 15;
      if(t > 3.7) return 19;
     if(we > 14800)
      if(we <= 14825) return 19;
      if(we > 14825) return 17;
  if(we > 16620)
   if(t <= 2.9) return 27;
   if(t > 2.9) return 23;
if(p.equals("PP21942")) return 20;
if(p.equals("PP21943")) return 32;
if(p.equals("PP21945")) return 31;
if(p.equals("PP21956")) return 19;
if(p.equals("PP21957")) return 27;
if(p.equals("PP21958")) return 27;
if(p.equals("PP21965")) return 23;
if(p.equals("PP21972")) return 23;
if(p.equals("PP21973")) return 31;
if(p.equals("PP21975"))
 if(w <= 1189)
  if(we <= 13390) return 20;
  if(we > 13390)
   if(we <= 13680)
    if(we <= 13490) return 21;
    if(we > 13490) return 18;
   if(we > 13680)
    if(we <= 13800) return 22;
    if(we > 13800) return 103;
 if(w > 1189)
  if(t <= 3.235)
   if(we <= 12100) return 19;
   if(we > 12100) return 21;
  if(t > 3.235)
   if(we <= 11301)
    if(we <= 11120) return 14;
    if(we > 11120) return 18;
   if(we > 11301)
    if(we <= 11870) return 22;
    if(we > 11870) return 16;
if(p.equals("PP21976")) return 24;
if(p.equals("PP21994"))
 if(t <= 4.2)
  if(w <= 1189)
   if(we <= 13280)
    if(t <= 3.7)
     if(we <= 11930)
      if(we <= 11925) return 16;
      if(we > 11925) return 20;
     if(we > 11930) return 18;
    if(t > 3.7) return 17;
   if(we > 13280)
    if(we <= 13580) return 19;
    if(we > 13580)
     if(t <= 3.7) return 25;
     if(t > 3.7) return 17;
  if(w > 1189)
   if(t <= 3.7) return 16;
   if(t > 3.7)
    if(we <= 14510)
     if(we <= 11090) return 19;
     if(we > 11090) return 15;
    if(we > 14510) return 20;
 if(t > 4.2)
  if(w <= 1200)
   if(we <= 13350)
    if(we <= 13160) return 18;
    if(we > 13160) return 14;
   if(we > 13350) return 20;
  if(w > 1200) return 23;
if(p.equals("PP21996")) return 19;
if(p.equals("PP22001"))
 if(t <= 3.7)
  if(t <= 3.1)
   if(w <= 1154) return 35;
   if(w > 1154) return 28;
  if(t > 3.1) return 21;
 if(t > 3.7)
  if(we <= 14210)
   if(we <= 12600)
    if(we <= 12150) return 15;
    if(we > 12150)
     if(we <= 12460) return 17;
     if(we > 12460) return 55;
   if(we > 12600) return 15;
  if(we > 14210)
   if(t <= 4.2) return 20;
   if(t > 4.2) return 19;
if(p.equals("PP22002"))
 if(we <= 14100)
  if(we <= 13400)
   if(we <= 12590)
    if(t <= 4.5) return 19;
    if(t > 4.5) return 17;
   if(we > 12590) return 16;
  if(we > 13400)
   if(w <= 1222) return 21;
   if(w > 1222)
    if(we <= 13920) return 14;
    if(we > 13920) return 16;
 if(we > 14100)
  if(we <= 14650)
   if(we <= 14480) return 18;
   if(we > 14480)
    if(we <= 14550)
     if(we <= 14530) return 17;
     if(we > 14530) return 20;
    if(we > 14550) return 21;
  if(we > 14650)
   if(we <= 15100)
    if(we <= 14690)
     if(we <= 14670) return 22;
     if(we > 14670) return 17.5;
    if(we > 14690)
     if(t <= 4.5)
      if(we <= 14770) return 20;
      if(we > 14770) return 18;
     if(t > 4.5)
      if(we <= 14730) return 17;
      if(we > 14730) return 20;
   if(we > 15100)
    if(we <= 16620) return 20;
    if(we > 16620) return 24;
if(p.equals("PP22003"))
 if(t <= 3.7) return 24;
 if(t > 3.7)
  if(we <= 12600)
   if(we <= 12150) return 15;
   if(we > 12150)
    if(we <= 12420) return 17;
    if(we > 12420) return 55;
  if(we > 12600) return 19;
if(p.equals("PP22005")) return 23;
if(p.equals("PP22006"))
 if(we <= 14570)
  if(we <= 14090) return 17;
  if(we > 14090)
   if(we <= 14370)
    if(we <= 14280) return 18;
    if(we > 14280)
     if(we <= 14310) return 17;
     if(we > 14310) return 19;
   if(we > 14370)
    if(we <= 14460) return 15;
    if(we > 14460) return 18;
 if(we > 14570)
  if(we <= 14700)
   if(we <= 14650)
    if(we <= 14610) return 15.5;
    if(we > 14610) return 23;
   if(we > 14650)
    if(we <= 14670) return 22;
    if(we > 14670) return 17.5;
  if(we > 14700)
   if(w <= 1167) return 24;
   if(w > 1167)
    if(we <= 14730)
     if(we <= 14725) return 17;
     if(we > 14725) return 20;
    if(we > 14730)
     if(we <= 14780) return 20;
     if(we > 14780) return 16;
if(p.equals("PP22007"))
 if(we <= 14630) return 17;
 if(we > 14630) return 16;
if(p.equals("PP22009"))
 if(t <= 2.8) return 19;
 if(t > 2.8)
  if(t <= 3.235)
   if(we <= 11590)
    if(we <= 11150)
     if(we <= 10260) return 15;
     if(we > 10260) return 18;
    if(we > 11150)
     if(we <= 11200) return 20;
     if(we > 11200) return 19;
   if(we > 11590)
    if(w <= 1189) return 18;
    if(w > 1189) return 20;
  if(t > 3.235)
   if(we <= 13060)
    if(w <= 1187)
     if(we <= 12710) return 21;
     if(we > 12710) return 18;
    if(w > 1187)
     if(we <= 11060) return 21;
     if(we > 11060) return 20;
   if(we > 13060) return 103;
if(p.equals("PP22010")) return 20;
if(p.equals("PP22014")) return 37;
if(p.equals("PP22015")) return 21;
if(p.equals("PP22016")) return 27;
if(p.equals("PP22017"))
 if(t <= 3.7)
  if(we <= 14160) return 100;
  if(we > 14160) return 18;
 if(t > 3.7) return 24;
if(p.equals("PP22018"))
 if(we <= 21710)
  if(we <= 18520) return 15;
  if(we > 18520) return 22;
 if(we > 21710)
  if(w <= 1125)
   if(w <= 1062) return 26;
   if(w > 1062) return 34;
  if(w > 1125)
   if(w <= 1161)
    if(we <= 23760) return 25;
    if(we > 23760) return 23;
   if(w > 1161)
    if(we <= 23960) return 23;
    if(we > 23960) return 26;
if(p.equals("PP22020"))
 if(t <= 4.5) return 25;
 if(t > 4.5)
  if(we <= 23120)
   if(we <= 22560) return 31;
   if(we > 22560) return 24;
  if(we > 23120)
   if(we <= 23290) return 25;
   if(we > 23290) return 26;
if(p.equals("PP22026"))
 if(we <= 21720)
  if(we <= 20870) return 18;
  if(we > 20870) return 23;
 if(we > 21720)
  if(we <= 22160) return 43;
  if(we > 22160) return 19;
if(p.equals("PP22035"))
 if(t <= 2.15) return 28;
 if(t > 2.15) return 27;
if(p.equals("PP22036"))
 if(t <= 3.7)
  if(we <= 16020) return 100;
  if(we > 16020) return 21;
 if(t > 3.7) return 18;
if(p.equals("PP22037")) return 18;
if(p.equals("PP22038")) return 24;
if(p.equals("PP22041")) return 48;
if(p.equals("PP22049")) return 20;
if(p.equals("PP22052"))
 if(t <= 4) return 20;
 if(t > 4) return 22;
if(p.equals("PP22054"))
 if(t <= 4.2) return 17;
 if(t > 4.2) return 15;
if(p.equals("PP22056")) return 29;
if(p.equals("PP22059")) return 24;
if(p.equals("PP22060"))
 if(we <= 23050)
  if(w <= 1072)
   if(we <= 20470) return 21;
   if(we > 20470) return 24;
  if(w > 1072) return 23;
 if(we > 23050)
  if(we <= 23220)
   if(we <= 23170)
    if(we <= 23110) return 18;
    if(we > 23110) return 21;
   if(we > 23170) return 27;
  if(we > 23220)
   if(we <= 23490) return 21;
   if(we > 23490)
    if(we <= 23710) return 20;
    if(we > 23710) return 21;
if(p.equals("PP22066"))
 if(we <= 12910) return 19;
 if(we > 12910) return 18;
if(p.equals("PP22067"))
 if(we <= 13350) return 19;
 if(we > 13350) return 18;
if(p.equals("PP22069"))
 if(t <= 2.9)
  if(t <= 2.43) return 27;
  if(t > 2.43) return 23;
 if(t > 2.9) return 18;
if(p.equals("PP22070"))
 if(w <= 1182) return 20;
 if(w > 1182) return 15;
if(p.equals("PP22071"))
 if(we <= 15600)
  if(we <= 14950) return 27;
  if(we > 14950) return 48;
 if(we > 15600)
  if(we <= 15660) return 33;
  if(we > 15660) return 30;
if(p.equals("PP22072")) return 33;
if(p.equals("PP22074"))
 if(we <= 15620) return 32;
 if(we > 15620) return 31;
if(p.equals("PP22075"))
 if(we <= 14330) return 18;
 if(we > 14330) return 20;
if(p.equals("PP22076")) return 28;
if(p.equals("PP22079")) return 40;
if(p.equals("PP22080"))
 if(we <= 15800) return 30;
 if(we > 15800) return 31;
if(p.equals("PP22083")) return 29;
if(p.equals("PP22085"))
 if(w <= 1182) return 31;
 if(w > 1182)
  if(w <= 1206) return 28;
  if(w > 1206) return 20;
if(p.equals("PP22086")) return 35;
if(p.equals("PP22093"))
 if(t <= 2.15) return 27;
 if(t > 2.15)
  if(we <= 14910) return 23;
  if(we > 14910) return 24;
if(p.equals("PP22094")) return 29;
if(p.equals("PP22096")) return 30;
if(p.equals("PP22102")) return 25;
if(p.equals("PP22104"))
 if(we <= 21210) return 33;
 if(we > 21210) return 30;
if(p.equals("PP22107")) return 27;
if(p.equals("PP22111")) return 28;
if(p.equals("PP22112"))
 if(we <= 16940) return 29;
 if(we > 16940)
  if(we <= 16990) return 28.6666666666667;
  if(we > 16990) return 30;
if(p.equals("PP22115"))
 if(we <= 16790) return 26;
 if(we > 16790) return 25;
if(p.equals("PP22132")) return 29;
if(p.equals("PP22135")) return 26;
if(p.equals("PP22136")) return 29;
if(p.equals("PP22137")) return 27;
if(p.equals("PP22143")) return 18;
if(p.equals("PP22157"))
 if(w <= 1209) return 30;
 if(w > 1209)
  if(t <= 2.9)
   if(we <= 16820) return 20;
   if(we > 16820) return 22;
  if(t > 2.9) return 19;
if(p.equals("PP22158")) return 27;
if(p.equals("PP22159"))
 if(w <= 1130)
  if(we <= 13760) return 24;
  if(we > 13760) return 23;
 if(w > 1130) return 27;
if(p.equals("PP22160")) return 22;
if(p.equals("PP22171"))
 if(t <= 2.3)
  if(we <= 16780) return 29;
  if(we > 16780)
   if(we <= 16900) return 31;
   if(we > 16900) return 30;
 if(t > 2.3)
  if(w <= 1069)
   if(we <= 16890) return 25;
   if(we > 16890) return 27;
  if(w > 1069) return 23;
if(p.equals("PP22174")) return 19;
if(p.equals("PP22175")) return 24;
if(p.equals("PP22178"))
 if(w <= 1162) return 21;
 if(w > 1162) return 22;
if(p.equals("PP22179")) return 21;
if(p.equals("PP22180"))
 if(we <= 24010) return 19;
 if(we > 24010) return 24;
if(p.equals("PP22181")) return 24;
if(p.equals("PP22183")) return 32;
if(p.equals("PP22184")) return 28;
if(p.equals("PP22186")) return 47;
if(p.equals("PP22195"))
 if(we <= 17050) return 20;
 if(we > 17050) return 26;
if(p.equals("PP22201")) return 25;
if(p.equals("PP22202"))
 if(we <= 14220) return 23;
 if(we > 14220) return 27;
if(p.equals("PP22204")) return 30;
if(p.equals("PP22209"))
 if(we <= 15080) return 108;
 if(we > 15080) return 19;
if(p.equals("PP22213"))
 if(we <= 16780) return 23;
 if(we > 16780) return 33;
if(p.equals("PP22215")) return 35;
if(p.equals("PP22225")) return 24;
if(p.equals("PP22229"))
 if(we <= 16590) return 27;
 if(we > 16590) return 126;
if(p.equals("PP22230")) return 28;
if(p.equals("PP22231")) return 33;
if(p.equals("PP22232"))
 if(we <= 23120) return 21;
 if(we > 23120)
  if(we <= 24180)
   if(we <= 23840) return 23;
   if(we > 23840)
    if(we <= 23900) return 36;
    if(we > 23900)
     if(we <= 24070) return 19;
     if(we > 24070) return 20;
  if(we > 24180)
   if(we <= 24220) return 24;
   if(we > 24220) return 23;
if(p.equals("PP22233")) return 19;
if(p.equals("PP22234")) return 25;
if(p.equals("PP22236")) return 24;
if(p.equals("PP22237")) return 18;
if(p.equals("PP22240")) return 42;
if(p.equals("PP22243")) return 29;
if(p.equals("PP22246")) return 18;
if(p.equals("PP22250")) return 22;
if(p.equals("PP22256")) return 33;
if(p.equals("PP22259")) return 45;
if(p.equals("PP22261")) return 23;
if(p.equals("PP22262")) return 29;
if(p.equals("PP22263")) return 29;
if(p.equals("PP22267")) return 50;
if(p.equals("PP22268")) return 27;
if(p.equals("PP22269")) return 22;
if(p.equals("PP22270")) return 21;
if(p.equals("PP22271"))
 if(t <= 3.034) return 31;
 if(t > 3.034) return 23;
if(p.equals("PP22272")) return 50;
if(p.equals("PP22273"))
 if(we <= 19390)
  if(we <= 15270)
   if(t <= 3.4) return 21;
   if(t > 3.4) return 25;
  if(we > 15270)
   if(we <= 17450) return 23;
   if(we > 17450) return 24;
 if(we > 19390)
  if(w <= 1105)
   if(we <= 22420) return 25;
   if(we > 22420) return 29;
  if(w > 1105) return 31;
if(p.equals("PP22274")) return 23;
if(p.equals("PP22276")) return 28;
if(p.equals("PP22277")) return 28;
if(p.equals("PP22278"))
 if(w <= 1185) return 18;
 if(w > 1185) return 17;
if(p.equals("PP22279")) return 25;
if(p.equals("PP22282")) return 23;
if(p.equals("PP22284")) return 28;
if(p.equals("PP22285")) return 29;
if(p.equals("PP22288")) return 20;
if(p.equals("PP22289")) return 52;
if(p.equals("PP22290")) return 29;
if(p.equals("PP22291"))
 if(we <= 20260) return 18;
 if(we > 20260)
  if(we <= 20380)
   if(we <= 20320) return 26.5;
   if(we > 20320) return 25;
  if(we > 20380)
   if(we <= 20420) return 22;
   if(we > 20420) return 21;
if(p.equals("PP22292"))
 if(we <= 16580) return 29;
 if(we > 16580) return 30;
if(p.equals("PP22293")) return 18;
if(p.equals("PP22295"))
 if(we <= 11600) return 18;
 if(we > 11600) return 17.5;
if(p.equals("PP22296"))
 if(t <= 4.8)
  if(w <= 1138) return 26;
  if(w > 1138) return 25;
 if(t > 4.8)
  if(w <= 1127)
   if(we <= 23130)
    if(we <= 21050) return 26;
    if(we > 21050)
     if(we <= 23040) return 21;
     if(we > 23040)
      if(we <= 23060) return 22;
      if(we > 23060) return 23;
   if(we > 23130)
    if(we <= 23190) return 25.5;
    if(we > 23190) return 22;
  if(w > 1127)
   if(we <= 23320) return 22;
   if(we > 23320) return 26;
if(p.equals("PP22297"))
 if(w <= 1201) return 126;
 if(w > 1201) return 30;
if(p.equals("PP22298")) return 27;
if(p.equals("PP22299")) return 28;
if(p.equals("PP22300")) return 28;
if(p.equals("PP22302")) return 24;
if(p.equals("PP22303")) return 20;
if(p.equals("PP22304"))
 if(we <= 21450) return 130;
 if(we > 21450) return 22;
if(p.equals("PP22305"))
 if(we <= 21570) return 23;
 if(we > 21570) return 22;
if(p.equals("PP22306")) return 27;
if(p.equals("PP22307")) return 19;
if(p.equals("PP22310")) return 35;
if(p.equals("PP22312")) return 32;
if(p.equals("PP22315")) return 22;
if(p.equals("PP22316")) return 24;
if(p.equals("PP22317")) return 24;
if(p.equals("PP22318")) return 23;
if(p.equals("PP22319")) return 19;
if(p.equals("PP22321"))
 if(we <= 12940) return 17;
 if(we > 12940) return 19;
if(p.equals("PP22322"))
 if(we <= 12940) return 17;
 if(we > 12940) return 19;
if(p.equals("PP22323"))
 if(we <= 17060) return 23;
 if(we > 17060) return 24;
if(p.equals("PP22324")) return 37;
if(p.equals("PP22325")) return 29;
if(p.equals("PP22326")) return 29;
if(p.equals("PP22333")) return 20;
if(p.equals("PP22334"))
 if(w <= 1024) return 27;
 if(w > 1024) return 25;
if(p.equals("PP22336"))
 if(we <= 13240)
  if(t <= 4.2)
   if(we <= 12390) return 17;
   if(we > 12390) return 19;
  if(t > 4.2)
   if(w <= 1182) return 17;
   if(w > 1182) return 15;
 if(we > 13240)
  if(we <= 14150) return 16;
  if(we > 14150) return 19;
if(p.equals("PP22339"))
 if(we <= 15650) return 16;
 if(we > 15650) return 20;
if(p.equals("PP22340")) return 20;
if(p.equals("PP22342"))
 if(w <= 1173)
  if(we <= 13400) return 18;
  if(we > 13400) return 20;
 if(w > 1173)
  if(we <= 13780) return 17;
  if(we > 13780) return 19;
if(p.equals("PP22343")) return 22;
if(p.equals("PP22345")) return 48;
if(p.equals("PP22348")) return 18;
if(p.equals("PP22350")) return 24;
if(p.equals("PP22351")) return 24;
if(p.equals("PP22352"))
 if(t <= 4.2)
  if(w <= 1182) return 44;
  if(w > 1182)
   if(we <= 12390) return 17;
   if(we > 12390)
    if(we <= 12680) return 18;
    if(we > 12680)
     if(we <= 12960) return 19;
     if(we > 12960) return 17;
 if(t > 4.2)
  if(we <= 13180)
   if(w <= 1182) return 17;
   if(w > 1182) return 15;
  if(we > 13180)
   if(we <= 14100) return 16;
   if(we > 14100) return 19;
if(p.equals("PP22354")) return 25;
if(p.equals("PP22355"))
 if(we <= 15710) return 25;
 if(we > 15710) return 28;
if(p.equals("PP22356"))
 if(we <= 15880) return 16;
 if(we > 15880) return 19;
if(p.equals("PP22357"))
 if(t <= 3.235) return 18;
 if(t > 3.235) return 17;
if(p.equals("PP22358"))
 if(t <= 3.235) return 18;
 if(t > 3.235) return 17;
if(p.equals("PP22361")) return 26;
if(p.equals("PP22362")) return 22;
if(p.equals("PP22363")) return 22;
if(p.equals("PP22365")) return 24;
if(p.equals("PP22366")) return 30;
if(p.equals("PP22367")) return 26;
if(p.equals("PP22369")) return 26;
if(p.equals("PP22374")) return 27;
if(p.equals("PP22376"))
 if(we <= 25050) return 26;
 if(we > 25050) return 30;
if(p.equals("PP22378")) return 18;
if(p.equals("PP22380")) return 29;
if(p.equals("PP22386")) return 17;
if(p.equals("PP22387")) return 14;
if(p.equals("PP22389")) return 28;
if(p.equals("PP22390")) return 27;
if(p.equals("PP22391")) return 28;
if(p.equals("PP22395"))
 if(w <= 1218) return 30;
 if(w > 1218)
  if(we <= 23140) return 33;
  if(we > 23140) return 34;
if(p.equals("PP22399")) return 22;
if(p.equals("PP22404")) return 27;
if(p.equals("PP22406")) return 25;
if(p.equals("PP22408")) return 25;
if(p.equals("PP22409")) return 26;
if(p.equals("PP22410")) return 26;
if(p.equals("PP22411")) return 32;
if(p.equals("PP22412")) return 22;
if(p.equals("PP22413")) return 22;
if(p.equals("PP22415")) return 17;
if(p.equals("PP22416"))
 if(t <= 2.8) return 21;
 if(t > 2.8)
  if(we <= 12450) return 19;
  if(we > 12450) return 20;
if(p.equals("PP22417"))
 if(t <= 3.7) return 20;
 if(t > 3.7)
  if(we <= 13160) return 18;
  if(we > 13160)
   if(we <= 14080)
    if(we <= 13320) return 16.5;
    if(we > 13320) return 21;
   if(we > 14080)
    if(we <= 14470) return 22;
    if(we > 14470)
     if(we <= 14510) return 19;
     if(we > 14510) return 18;
if(p.equals("PP22419")) return 35;
if(p.equals("PP22420")) return 19;
if(p.equals("PP22421")) return 19;
if(p.equals("PP22422")) return 16;
if(p.equals("PP22423")) return 30;
if(p.equals("PP22424"))
 if(we <= 13120) return 14;
 if(we > 13120) return 18;
if(p.equals("PP22425"))
 if(we <= 16920) return 27;
 if(we > 16920) return 32;
if(p.equals("PP22426"))
 if(we <= 16920) return 27;
 if(we > 16920) return 32;
if(p.equals("PP22427"))
 if(we <= 16920) return 27;
 if(we > 16920) return 32;
if(p.equals("PP22431")) return 30;
if(p.equals("PP22435")) return 27;
if(p.equals("PP22437")) return 23;
if(p.equals("PP22438")) return 22;
if(p.equals("PP22442")) return 17;
if(p.equals("PP22447")) return 25;
if(p.equals("PP22448"))
 if(t <= 3.235) return 29;
 if(t > 3.235) return 19;
if(p.equals("PP22456")) return 35;
if(p.equals("PP22457")) return 35;
if(p.equals("PP22459")) return 29;
if(p.equals("PP22460")) return 30;
if(p.equals("PP22461")) return 30;
if(p.equals("PP22464")) return 21;
if(p.equals("PP22468")) return 31;
if(p.equals("PP22469")) return 27;
if(p.equals("PP22470")) return 30;
if(p.equals("PP22471"))
 if(we <= 14680) return 18;
 if(we > 14680) return 20;
if(p.equals("PP22473")) return 20;
if(p.equals("PP22474")) return 20;
if(p.equals("PP22478")) return 16;
if(p.equals("PP22479")) return 14;
if(p.equals("PP22480")) return 19;
if(p.equals("PP22481"))
 if(t <= 2.5) return 29;
 if(t > 2.5) return 19;
if(p.equals("PP22482"))
 if(w <= 1164) return 29;
 if(w > 1164) return 19;
if(p.equals("PP22483")) return 28;
if(p.equals("PP22484")) return 27;
if(p.equals("PP22485")) return 27;
if(p.equals("PP22486")) return 18;
if(p.equals("PP22487")) return 28;
if(p.equals("PP22488")) return 28;
if(p.equals("PP22489")) return 28;
if(p.equals("PP22490")) return 31;
if(p.equals("PP22491")) return 24;
if(p.equals("PP22492")) return 24;
if(p.equals("PP22493")) return 31.5;
if(p.equals("PP22494")) return 32.5;
if(p.equals("PP22496")) return 27;
if(p.equals("PP22498"))
 if(t <= 3.7)
  if(we <= 14980) return 21;
  if(we > 14980) return 22;
 if(t > 3.7)
  if(w <= 1222)
   if(w <= 1182) return 44;
   if(w > 1182)
    if(w <= 1201) return 18;
    if(w > 1201) return 17;
  if(w > 1222)
   if(we <= 14770)
    if(we <= 13960) return 19;
    if(we > 13960) return 18;
   if(we > 14770) return 19;
if(p.equals("PP22499")) return 29;
if(p.equals("PP22500")) return 29;
if(p.equals("PP22502")) return 29;
if(p.equals("PP22503"))
 if(we <= 11350) return 114;
 if(we > 11350) return 18;
if(p.equals("PP22504")) return 21;
if(p.equals("PP22505"))
 if(w <= 1175)
  if(we <= 13140) return 18;
  if(we > 13140)
   if(we <= 13990) return 21;
   if(we > 13990)
    if(we <= 14470) return 22;
    if(we > 14470) return 19;
 if(w > 1175) return 20;
if(p.equals("PP22507"))
 if(we <= 12180) return 107;
 if(we > 12180)
  if(t <= 3.235) return 20;
  if(t > 3.235) return 23;
if(p.equals("PP22508"))
 if(t <= 3.3)
  if(we <= 15430) return 22;
  if(we > 15430) return 23;
 if(t > 3.3)
  if(we <= 11360) return 17;
  if(we > 11360) return 16;
if(p.equals("PP22509")) return 28;
if(p.equals("PP22510")) return 16;
if(p.equals("PP22513")) return 22;
if(p.equals("PP22514"))
 if(we <= 12210)
  if(t <= 2.8)
   if(we <= 11290)
    if(we <= 10600) return 19;
    if(we > 10600) return 18;
   if(we > 11290) return 19;
  if(t > 2.8)
   if(we <= 12100) return 18;
   if(we > 12100) return 107;
 if(we > 12210)
  if(t <= 3.235)
   if(we <= 12490)
    if(we <= 12390) return 20;
    if(we > 12390) return 15;
   if(we > 12490)
    if(we <= 12690)
     if(we <= 12580) return 21;
     if(we > 12580) return 17;
    if(we > 12690)
     if(we <= 12870) return 22;
     if(we > 12870) return 19;
  if(t > 3.235)
   if(we <= 13120) return 18;
   if(we > 13120) return 23;
if(p.equals("PP22515"))
 if(t <= 2.8) return 19;
 if(t > 2.8)
  if(we <= 12750) return 18;
  if(we > 12750) return 21;
if(p.equals("PP22516")) return 29;
if(p.equals("PP22517"))
 if(we <= 24600)
  if(we <= 22280)
   if(we <= 16700) return 19;
   if(we > 16700) return 25;
  if(we > 22280)
   if(w <= 1290)
    if(we <= 23850) return 27;
    if(we > 23850) return 31;
   if(w > 1290)
    if(we <= 23260) return 27;
    if(we > 23260) return 33;
 if(we > 24600)
  if(we <= 24920) return 28;
  if(we > 24920)
   if(we <= 25170) return 37;
   if(we > 25170) return 25;
if(p.equals("PP22520")) return 18;
if(p.equals("PP22521")) return 16;
if(p.equals("PP22522")) return 26;
if(p.equals("PP22530"))
 if(t <= 4.2) return 15;
 if(t > 4.2) return 21;
if(p.equals("PP22531"))
 if(w <= 1189)
  if(t <= 3.7) return 18;
  if(t > 3.7) return 17;
 if(w > 1189)
  if(we <= 11350)
   if(t <= 4.2) return 15;
   if(t > 4.2) return 114;
  if(we > 11350) return 18;
if(p.equals("PP22534"))
 if(we <= 12460)
  if(we <= 11350)
   if(we <= 11160) return 19;
   if(we > 11160) return 114;
  if(we > 11350) return 18;
 if(we > 12460)
  if(we <= 13770) return 17;
  if(we > 13770) return 16;
if(p.equals("PP22535"))
 if(w <= 1222)
  if(we <= 15620) return 13;
  if(we > 15620) return 22;
 if(w > 1222)
  if(t <= 4.5) return 19;
  if(t > 4.5)
   if(we <= 14730) return 15;
   if(we > 14730) return 18;
if(p.equals("PP22537")) return 45;
if(p.equals("PP22540")) return 28;
if(p.equals("PP22541")) return 28;
if(p.equals("PP22542")) return 28;
if(p.equals("PP22546")) return 19;
if(p.equals("PP22550")) return 19;
if(p.equals("PP22553")) return 27;
if(p.equals("PP22556")) return 31;
if(p.equals("PP22558")) return 17;
if(p.equals("PP22561")) return 22;
if(p.equals("PP22563"))
 if(t <= 3.1)
  if(we <= 11660) return 20;
  if(we > 11660) return 17;
 if(t > 3.1) return 16;
if(p.equals("PP22566")) return 14;
if(p.equals("PP22567"))
 if(we <= 24920) return 24;
 if(we > 24920) return 28;
if(p.equals("PP22568")) return 25;
if(p.equals("PP22569"))
 if(we <= 14730) return 15;
 if(we > 14730) return 18;
if(p.equals("PP22570")) return 17;
if(p.equals("PP22571")) return 19;
if(p.equals("PP22573")) return 19;
if(p.equals("PP22574")) return 13;
if(p.equals("PP22576")) return 21;
if(p.equals("PP22581")) return 25;
if(p.equals("PP22582")) return 19;
if(p.equals("PP22586")) return 31;
if(p.equals("PP22590")) return 21;
if(p.equals("PP22595")) return 30;
if(p.equals("PP22600")) return 19;
if(p.equals("PP22601")) return 19;
if(p.equals("PP22604")) return 52;
return 52.0;
}
}
