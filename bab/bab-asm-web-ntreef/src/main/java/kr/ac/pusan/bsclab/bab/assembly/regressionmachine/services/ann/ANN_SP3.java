package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SP3 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{0.54626817, -1.90949416, 0.91649097, -0.51148707, -0.26361737, -0.04935346
            				, -0.94496894, -0.75679034, -1.47593892, -0.01685208, 1.21168327, 0.14757587
            				, -0.76766336, 0.04092104, -0.69181067, -0.16910306, -0.35665712, -2.10061741
            				, 2.16484475, 0.4382388}, 
            			{0.38477662, -2.14562845, -0.22318409, 0.357476, 0.40559882, -1.20126855
        					, -0.39391643, -2.01292872, -0.94036931, -0.5889371, 0.25789964, 0.18064801
        					, 0.45484012, 0.23228644, -1.55303562, 0.53134847, 1.24368227, 0.3188484
        					, -0.79029351, 0.02896946},
            			{-1.70457959, 0.56570274, 1.50310481, 0.32849088, -0.09822328, -1.77680159
    						, -0.68410975, -0.6180464, 0.38735986, -1.33120763, 0.06323946, 0.77602136
    						, -1.45065713, 2.46931839, 0.97950488, 0.87023395, -1.05233598, 0.2889705
    						, 0.92714339, 1.14249945}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.15071657, 2.61515164, -0.68056786, 1.11521769, -1.01145792, -1.19498193
            			, 0.0153304, 0.71812236, 1.9277631, 0.29593703, -0.62867528, -0.62053484
            			, 0.45523849, 0.00329974, 0.42377439, 0.01861488, -0.35573038, -0.96246892
            			, 0.43067455, -1.73642337};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.7233001,
            			0.33667749,
            			-0.27637857,
            			0.66232753,
            			-0.9114176,
            			-0.06927133,
            			0.85187489,
            			-1.35130227,
            			0.5539462,
            			-0.74100381,
            			-0.10655108,
            			-0.58281934,
            			-0.25599119,
            			0.0345368,
            			-0.18084979,
            			0.18508448,
            			1.50739229,
            			1.25532615,
            			-1.64874303,
            			1.70645761};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.16601014};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 6.2;
	}

	@Override
	public double getMaxWidth() {
		
		return 642.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 12826.00;
	}

	@Override
	public double getMinThick() {
		
		return 2.5;
	}

	@Override
	public double getMinWidth() {
		
		return 325.0;
	}

	@Override
	public double getMinWeight() {
		
		return 5956.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
