package kr.ac.pusan.bsclab.bab.assembly.utils.spark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SparkProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkDriverStatus;
import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkRestResult;
import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkSubmissionStatus;
import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;

@Component
public class SparkWeb {
	
	private static final Logger logger
		= LoggerFactory.getLogger(SparkWeb.class);

	@Autowired
	SparkProperties sp;
	
	@Autowired
	RestRequest restRequest;

	public SparkSubmissionStatus submit(SparkSubmission submission) {
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(submission);
			logger.debug("submit spark configuration={}", json);
			// String uri = BabServer.CONFIG.SPARK_GATEWAY +
			// "/v1/submissions/create";
			String uri = sp.getHiddenApiUri() + "/v1/submissions/create";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>(json, headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.POST, entity, String.class);
			SparkSubmissionStatus status = mapper.readValue(result.getBody(), SparkSubmissionStatus.class);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public SparkDriverStatus status(SparkRestResult submission) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			// String uri = BabServer.CONFIG.SPARK_GATEWAY +
			// "/v1/submissions/status/" + submission.submissionId;
			String uri = sp.getHiddenApiUri() + "/v1/submissions/status/" + submission.submissionId;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
			SparkDriverStatus status = mapper.readValue(result.getBody(), SparkDriverStatus.class);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public SparkDriverStatus status(String submissionId) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			// String uri = BabServer.CONFIG.SPARK_GATEWAY +
			// "/v1/submissions/status/" + submission.submissionId;
			String uri = sp.getHiddenApiUri() + "/v1/submissions/status/" + submissionId;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
			SparkDriverStatus status = mapper.readValue(result.getBody(), SparkDriverStatus.class);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public SparkSubmissionStatus kill(SparkRestResult submission) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			// String uri = BabServer.CONFIG.SPARK_GATEWAY +
			// "/v1/submissions/kill/" + submission.submissionId;
			String uri = sp.getHiddenApiUri() + "/v1/submissions/kill/" + submission.submissionId;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.POST, entity, String.class);
			SparkSubmissionStatus status = mapper.readValue(result.getBody(), SparkSubmissionStatus.class);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
