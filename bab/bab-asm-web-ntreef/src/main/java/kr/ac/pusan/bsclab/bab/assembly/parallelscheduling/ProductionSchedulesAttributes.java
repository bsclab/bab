package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

public class ProductionSchedulesAttributes {
	public String project;
	public String block;
	public double weight;
	public String origin_area_of_block;
	public double ready_time_to_pickup_block_in_origin; // date convert to double, reference with start scheduling
	public String production_workplace;;
	public double production_start;
	public double production_end;
	public String color_block;
}
