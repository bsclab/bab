package kr.ac.pusan.bsclab.bab.assembly.ws.models;

import java.sql.Timestamp;

import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkRestResult;
import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;

public class JobSubmission extends JobConfiguration {
	
	protected SparkRestResult sparkStatus;
	protected String forceKillUrl;

	public JobSubmission() {
		
	}
	
	public JobSubmission(
			String home,
			String jobId,
			String jobClass,
			String jobExtension,
			String workspaceId,
			String datasetId,
			String repositoryId,
			String configuration,
			Timestamp sdt,
			Timestamp edt) {
		setJobId(jobClass + "+" + jobId);
		setJobClass(jobClass);
		setWorkspaceId(workspaceId);
		setDatasetId(datasetId);
		setRepositoryId(repositoryId);
		setPath(home+ "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId);
		setExt(jobExtension);
		setJobPath(home + "/jobs/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/" + jobId + "." + jobExtension);
		setResourcePath(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/basecsv");
		setResultDir(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId);
		setResultPath(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/" + jobId + "." + jobExtension);
		setConfiguration(configuration);
		setSdt(sdt);
		setEdt(edt);
		
	}
	

	public JobSubmission(
			String home,
			String jobId,
			String jobClass,
			String jobExtension,
			String workspaceId,
			String datasetId,
			String repositoryId,
			String configuration) {
		setJobId(jobClass + "+" + jobId);
		setJobClass(jobClass);
		setWorkspaceId(workspaceId);
		setDatasetId(datasetId);
		setRepositoryId(repositoryId);
		setPath(home+ "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId);
		setExt(jobExtension);
		setJobPath(home + "/jobs/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/" + jobId + "." + jobExtension);
		setResourcePath(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/basecsv");
		setResultDir(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId);
		setResultPath(home + "/workspaces/" + workspaceId + "/" + datasetId
				+ "/" + repositoryId + "/" + jobId + "." + jobExtension);
		setConfiguration(configuration);
	}

	public void setSparkStatus(SparkRestResult sparkStatus) {
		this.sparkStatus = sparkStatus;
	}

	public String getForceKillUrl() {
		return forceKillUrl;
	}

	public void setForceKillUrl(String forceKillUrl) {
		this.forceKillUrl = forceKillUrl;
	}	
}
