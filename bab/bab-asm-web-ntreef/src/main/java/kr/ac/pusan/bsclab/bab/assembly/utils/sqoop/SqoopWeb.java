package kr.ac.pusan.bsclab.bab.assembly.utils.sqoop;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SqoopProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.Link;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission.Submission;
import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;

@Component
@SuppressWarnings("unchecked")
public class SqoopWeb {
	
	@Autowired
	AppProperties ap;
	
	@Autowired
	SqoopProperties sp;
	
	@Autowired
	HdfsProperties hp;
	
	@Autowired
	RestRequest restRequest;
	
//	@Transactional
	public void stopAndDeleteSqoopFlow(Job job)
			throws JsonParseException, JsonMappingException, IOException {
		this.reqStopJob(job.getName());
		this.reqDeleteJob(job.getName());
		this.reqDeletetLink(job.getFromLinkId());
		this.reqDeletetLink(job.getToLinkId());
	}
	
	@Transactional
	public int reqCreateLink(Link link)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/link";
		ObjectMapper om = new ObjectMapper();
		om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		String json = om.writeValueAsString(link);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.POST, entity, String.class);
		Map<String, Object> response = om.readValue(result.getBody(), HashMap.class);
		return (int) response.get("id");
	}
	
	
	@Transactional
	public int reqCreateJob(Job job)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job";
		ObjectMapper om = new ObjectMapper();
		om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		String json = om.writeValueAsString(job);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.POST, entity, String.class);
		Map<String, Object> response = om.readValue(result.getBody(), HashMap.class);
		return (int) response.get("id");
	}
	
	@Transactional
	public Link getLink(int linkId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/link/" + linkId;
		ObjectMapper om = new ObjectMapper();
		om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		String json = om.writeValueAsString(null);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
		return om.readValue(result.getBody(), Link.class);
	}

	@Transactional
	public Job getJob(int jobId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + jobId;
		ObjectMapper om = new ObjectMapper();
		om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String json = om.writeValueAsString(null);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);	
		return om.readValue(result.getBody(), Job.class);
	}
	
	@Transactional
	public Submission reqStartJob(Job job)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + job.getName() + "/start";
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		try {
			ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.PUT, entity, String.class);
			Submission se = om.readValue(result.getBody(), Submission.class);
			return se;
		} catch (Exception ex) {
			ex.printStackTrace();
			toString();
		}
		return null;
	}
	
	@Transactional
	public Submission reqGetJobStatus(Job job)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + job.getName() + "/status";
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
		return om.readValue(result.getBody(), Submission.class);
	}

	@Transactional
	public void reqStopJob(String jobName)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + jobName + "/stop";
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.PUT, entity, String.class);
		if (result != null) result.toString();
	}
	
	@Transactional
	public void reqStopJob(int jobId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + jobId + "/stop";
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.PUT, entity, String.class);	
		if (result != null) result.toString();
	}
	
	@Transactional
	public void reqDeleteJob(int jobId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + jobId;
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.DELETE, entity, String.class);
		if (result != null) result.toString();
	}
	
	@Transactional
	public void reqDeleteJob(String jobName)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/job/" + jobName;
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.DELETE, entity, String.class);		
		if (result != null) result.toString();
	}

	@Transactional
	public void reqDeletetLink(String linkName)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/link/" + linkName;
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.DELETE, entity, String.class);		
		if (result != null) result.toString();
	}
	
	@Transactional
	public void reqDeletetLink(int linkId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = sp.getApiUri() + "/link/" + linkId;
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString("");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.DELETE, entity, String.class);		
		if (result != null) result.toString();
	}
}
