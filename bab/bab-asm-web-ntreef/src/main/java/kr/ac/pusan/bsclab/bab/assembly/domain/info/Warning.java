package kr.ac.pusan.bsclab.bab.assembly.domain.info;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Warning {
	
	@Id
	String level;
	
	String code;
	
	String message;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
