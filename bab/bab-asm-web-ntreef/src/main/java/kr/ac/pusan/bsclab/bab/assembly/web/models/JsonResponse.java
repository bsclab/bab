package kr.ac.pusan.bsclab.bab.assembly.web.models;

import kr.ac.pusan.bsclab.bab.assembly.ws.models.JobSubmission;

public class JsonResponse {
	private String id;
	private JobSubmission request;
	private Object response;
	private String status;
	
	public JobSubmission getRequest() {
		return request;
	}
	public void setRequest(JobSubmission request) {
		this.request = request;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
