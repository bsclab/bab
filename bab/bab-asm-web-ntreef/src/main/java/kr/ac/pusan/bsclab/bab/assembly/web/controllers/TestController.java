package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Node;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;

@Controller
public class TestController extends AbstractWebController {
	
	public static final String BASE_URL = BabWeb.BASE_URL + "/test";
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex() {
		ModelAndView view = new ModelAndView("bab/index");

		HeuristicMinerJobConfiguration config = new HeuristicMinerJobConfiguration();
		config.setPositiveObservation(0);
		config.getThreshold().setDependency(0.1);
		Response<HeuristicModel> model = callBabService(
				"http://localhost:8080/bab/api/v1_0/model/heuristic/devel4/20160331101604/BLOCK_DEFAULT", config,
				HeuristicModel.class);

		String a = "";

		for (Node node : model.getResponse().getNodes().values()) {
			a += node.getLabel() + "-";
		}
		view.addObject("message", a);
		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/woo")
	public ModelAndView woo_test(HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("bab/index");

		System.out.println((String) session.getAttribute("sdt"));
		System.out.println((String) session.getAttribute("edt"));
		
		return view;
	}
}
