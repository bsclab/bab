package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.io.IOException;
import java.util.*;
import java.text.DecimalFormat;
import java.util.GregorianCalendar;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;
//import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config.DottedChartAnalysisJobConfiguration;
//import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.*;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models.Ymkpi;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models.YmkpiParameter;

@Controller
public class ReportingController extends AbstractWebController {
	
	public static final String BASE_URL = BabWeb.BASE_URL + "/reporting";
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex(){
		ModelAndView view = new ModelAndView("bab/index");
		
		String message = this.getClass().getName();
		view.addObject("message", message);
		
		return view;
	}
	
	//@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/kpi/{workspaceId}/{datasetId}/{repositoryId}")
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/kpi/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getKpi(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			//@PathVariable(value = "repositoryId") String repositoryId,
			HttpServletRequest request, 
			HttpSession session) throws JsonParseException, JsonMappingException, IOException {
	
		
		ModelAndView view = new ModelAndView("reporting/kpi/kpi");
		
		session.setAttribute("workspaceId", workspaceId);
		session.setAttribute("datasetId", datasetId);
		session.setAttribute("sdt", sdt);
		session.setAttribute("edt", edt);
		//session.setAttribute("repositoryId", repositoryId);

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		//jsonData.put("repositoryId", repositoryId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		
		YmkpiParameter config = new YmkpiParameter();
		//String apiURI = apiManager.getAPIURI().get("analysis").get("kpi")+ workspaceId + "/" + datasetId + "/" + repositoryId;
		String apiURI = apiManager.getAPIURI().get("analysis").get("kpi")+ workspaceId + "/" + datasetId + "/" + sdt + "/" + edt;
		Response<Ymkpi> model = callBabService(apiURI, config, Ymkpi.class);
		
		if(model.getResponse() != null){
			Map<String, Map<String,Double>> allYearData = model.getResponse().getYearly();
			Map<String, Map<String,Double>> allMonthData = model.getResponse().getMonthly();		
			
			String jsonGraphYear = "";
			String jsonGraphUtilizationYear = "";
			String jsonGraphMonth = "";
			String jsonGraphUtilizationMonth = "";
					
			Double dOriginatorDataValue = 0.0;
			Double dOriginatorDataValueMonth = 0.0;
			
			String tableYear = "";
			String tableDynamicHeader = "";
			String tableDynamicContent = "";		
			
			String tableYearTranspose = "";
			String tableDynamicHeaderTranspose = "";
			String tableDynamicContentTranspose = "";
			
			String tableYearUtilization = "";
			String tableYearUtilizationDH = "";
			String tableYearUtilizationDC = "";
			
			String tableYearUtilizationTranspose = "";
			String tableYearUtilizationTransposeDH = "";
			String tableYearUtilizationTransposeDC = "";
			
			String tableMonth = "";
			String tableMonthYearDynamicHeader = "";
			String tableMonthDynamicHeader = "";
			String tableMonthDynamicContent = "";
			
			String tableMonthUtilization = "";
			String tableMonthYearUtilizationDH = "";
			String tableMonthUtilizationDH = "";
			String tableMonthUtilizationDC = "";
			
			String tableMonthTranspose = "";
			String tableMonthDynamicHeaderTitleTranspose = "";
//			String tableMonthDynamicHeaderTranspose = "";
			String tableMonthDynamicContentTranspose = "";
			
			String tableMonthUtilizationTranspose = "";
			String tableMonthUtilizationTransposeTitleDH = "";
//			String tableMonthUtilizationTransposeDH = "";
			String tableMonthUtilizationTransposeDC = "";
			
			int countAllYearData = allYearData.size();
			int numberOfOriginator = 0;
			
			int countAllMonthData = allMonthData.size();
			int numberOfOriginatorMonth = 0;
			
			Calendar gc; 
//			int daysOfYear = 0;
//			int daysOfMonth = 0;
//			
			Double percentage = 0.0;
			int mc = 0;
			
			String pattern = "###,###";
			String pattern2 = "##.###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern);
			DecimalFormat percentageFormat = new DecimalFormat(pattern2);
			
			/* 2017.07.04
			 * Store the Machine Capacity to HashMap
			 * 
			 */
			
			HashMap<String, Integer> machineCapacity = new HashMap<String, Integer>();
			
			machineCapacity.put("PP1", 300000);
			machineCapacity.put("PP2", 300000);
			machineCapacity.put("WS1", 140000);
			machineCapacity.put("WS2", 120000);
			machineCapacity.put("WS3", 150000);
			machineCapacity.put("WS4", 190000);
			machineCapacity.put("CR1", 80000);
			machineCapacity.put("CR2", 270000);
			machineCapacity.put("CR3", 150000);
			machineCapacity.put("SS1", 86000);
			machineCapacity.put("SS2", 43000);
			machineCapacity.put("SS3", 100000);
			machineCapacity.put("SS4", 50000);
			
			/*	2017.06.21
			 *  Counting all the child of allYearData
			 *  Since the number of originator each year is different
			 *  We need to count number of originator each year
			 *  and take the most  
			 */
			
			HashSet<String> originatorNameHS = new HashSet<String>();
//			int tempNumberOfOriginator = 0;		
			
			for(String yearData : allYearData.keySet()){
	
				Map<String, Double> allChildYearDatas = allYearData.get(yearData);
				for(String originatorData : allChildYearDatas.keySet()){
					originatorNameHS.add(originatorData);
				}
			}
			
			List<String> originatorDataAL = new ArrayList<String>(originatorNameHS);
			Collections.sort(originatorDataAL);
			numberOfOriginator = originatorDataAL.size();
			String[] originatorName = originatorDataAL.toArray(new String[numberOfOriginator]);
			
			
			/* 2017.06.21
			 * Counting all the child of allMonthData
			 * Since the number of originator each month is different
			 * We need to count number of originator each month
			 * and take the most
			 */
			
			HashSet<String> originatorNameMonthHS = new HashSet<String>();
//			tempNumberOfOriginator = 0;
			
			for(String monthData : allMonthData.keySet()){
	
				Map<String, Double> allChildMonthDatas = allMonthData.get(monthData);
				for(String originatorDataMonth : allChildMonthDatas.keySet()){
					originatorNameMonthHS.add(originatorDataMonth);
				}
			}
			
			List<String> originatorDataMonthAL = new ArrayList<String>(originatorNameMonthHS);
			Collections.sort(originatorDataMonthAL);
			numberOfOriginatorMonth = originatorDataMonthAL.size(); 
			String[] originatorNameMonth = originatorDataMonthAL.toArray(new String[numberOfOriginatorMonth]);
			
			/*
			 * Storing 'allYearData' to Array
			 * Forming JSON for YoY Graph 
			 */
			
			String[] arrAllYearData = new String[countAllYearData];	
			Double[][] originatorValue = new Double[numberOfOriginator][countAllYearData];
			Double[][] yearUtilizationValue = new Double[numberOfOriginator][countAllYearData];
			int[] arrNumDaysOfYear = new int[countAllYearData];
			
			Double tmpPercentage = 0.0;
			int tmpMc = 0;
			
			jsonGraphYear += "[";
			jsonGraphUtilizationYear += "[";
			
			int x=0;
			for(String yearData : allYearData.keySet()){		
				arrAllYearData[x] = yearData;
				
				gc = new GregorianCalendar(Integer.parseInt(yearData),0,1);
				arrNumDaysOfYear[x] = gc.getActualMaximum(Calendar.DAY_OF_YEAR);
				
				tableDynamicHeader += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+yearData+"</th>";
				tableYearUtilizationDH += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+yearData+"</th>";
				
				jsonGraphYear += "{ \"Year\":\""+yearData+"\", \"Originators\":[";
				jsonGraphUtilizationYear += "{ \"Year\":\""+yearData+"\", \"Originators\":[";
				
				/* 2017.06.22
				 * Force all Year to have SAME NUMBER of originator name
				 * Set value to 0 for the non existing originator name 
				 */
				int y = 0;
				Map<String, Double> allChildYearDatas = allYearData.get(yearData);
				for(String originatorData : allChildYearDatas.keySet()){
					dOriginatorDataValue = allChildYearDatas.get(originatorData);
					
					for(int a=0;a<originatorName.length;a++){
						if(originatorData.equals(originatorName[a])){
							originatorValue[a][x] = dOriginatorDataValue;
						}
					}
					
					y++;
					
					jsonGraphYear += "{\"Name\":\""+originatorData+"\", \"Value\":"+dOriginatorDataValue+"}";
					
					/* Calculate Utilization */
					if(machineCapacity.get(originatorData.substring(0, 3)) == null){
						tmpPercentage = 0.0;
					}else{
						tmpMc = machineCapacity.get(originatorData.substring(0, 3));
						tmpPercentage = (dOriginatorDataValue/tmpMc)*100;
					}
					
					jsonGraphUtilizationYear += "{\"Name\":\""+originatorData+"\", \"Value\":"+percentageFormat.format(tmpPercentage)+"}";
					
					if(y < allChildYearDatas.size()){
						jsonGraphYear += ",";
						jsonGraphUtilizationYear += ",";
					}
				}
				
				x++;
				jsonGraphYear += "]}";
				jsonGraphUtilizationYear += "]}";
				
				if(x < countAllYearData){
					jsonGraphYear += ",";
					jsonGraphUtilizationYear += ",";
				}
			}
			
			jsonGraphYear += "]";
			jsonGraphUtilizationYear += "]";
			
			/*
			 * Storing 'allMonthData' to Array
			 * Forming JSON for MoM Graph 
			 */
			
			String[] arrAllMonthData = new String[countAllMonthData];		
			Double[][] originatorValueMonth = new Double[numberOfOriginatorMonth][countAllMonthData];
			Double[][] monthUtilizationValue = new Double[numberOfOriginatorMonth][countAllMonthData];
			
			Map<String, Map<String,Double>> sortedMonthData = new TreeMap<String, Map<String,Double>>(allMonthData);
			
			String monthYear = "0000";
			String tempMonthYear = "0000";
//			String tempMonth = "00";
			String[] nameOfMonthArray = {"Jan","Feb","Mar","Apr","Mei","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
			
			int numOfColYear = 1;
			int indexOfYear = 0;
			int z=0;
			
			ArrayList<Integer> numMonthOfYear = new ArrayList<Integer>();
			
			jsonGraphMonth += "[";
			jsonGraphUtilizationMonth += "[";
			
			for(String monthData : sortedMonthData.keySet()){
				arrAllMonthData[z] = monthData;
				tempMonthYear = monthData.substring(0,4);
//				tempMonth = monthData.substring(5,7);
				
				jsonGraphMonth += "{ \"Year\":\""+monthData+"\", \"Originators\":[";
				jsonGraphUtilizationMonth += "{ \"Year\":\""+monthData+"\", \"Originators\":[";
				
				if(monthYear.equals(tempMonthYear) == false)
				{
					if(monthYear.equals("0000") == false){
						
						tableMonthYearDynamicHeader += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numOfColYear+"'>"+monthYear+"</th>";
						tableMonthYearUtilizationDH += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numOfColYear+"'>"+monthYear+"</th>";
						
						numMonthOfYear.add(numOfColYear);
						numOfColYear = 1;
					}
					
					indexOfYear++;
					monthYear = tempMonthYear;
				}else{
					numOfColYear++;
				}
				
				tableMonthDynamicHeader += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+nameOfMonthArray[Integer.parseInt(monthData.substring(5,7))-1]+"</th>";
				tableMonthUtilizationDH += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+nameOfMonthArray[Integer.parseInt(monthData.substring(5,7))-1]+"</th>";
				
				/* 2017.06.22
				 * Force all Month to have SAME NUMBER of originator name
				 * Set value to 0 for the non existing originator name 
				 */
				
				int w = 0;
				Map<String, Double> allChildMonthDatas = sortedMonthData.get(monthData);
				int sizeOfSubMonthData = allChildMonthDatas.size();
				
				for(String originatorDataMonth : allChildMonthDatas.keySet()){
					dOriginatorDataValueMonth = allChildMonthDatas.get(originatorDataMonth);
					
					for(int a=0;a<originatorNameMonth.length;a++){
						if(originatorDataMonth.equals(originatorNameMonth[a])){
							originatorValueMonth[a][z] = dOriginatorDataValueMonth;
						}
					}
					
					w++;
					
					jsonGraphMonth += "{\"Name\":\""+originatorDataMonth+"\", \"Value\":"+allChildMonthDatas.get(originatorDataMonth)+"}";
					
					/* Calculate Utilization */
					if(machineCapacity.get(originatorDataMonth.substring(0, 3)) == null){
						tmpPercentage = 0.0;
					}else{
						tmpMc = machineCapacity.get(originatorDataMonth.substring(0, 3));
						tmpPercentage = (dOriginatorDataValueMonth/tmpMc)*100;
					}
					
					jsonGraphUtilizationMonth += "{\"Name\":\""+originatorDataMonth+"\", \"Value\":"+percentageFormat.format(tmpPercentage)+"}";
					
					if(w < sizeOfSubMonthData){
						jsonGraphMonth += ",";
						jsonGraphUtilizationMonth += ",";
					}
				}
				
				z++;
				
				jsonGraphMonth += "]}";
				jsonGraphUtilizationMonth += "]}";
				
				//if(indexOfYear < 2){
				//if(indexOfYear < countAllYearData){
				if(z<countAllMonthData){	
					jsonGraphMonth += ",";
					jsonGraphUtilizationMonth += ",";
				}
			}
			
			jsonGraphMonth += "]";
			jsonGraphUtilizationMonth += "]";
			
			if(indexOfYear <= countAllYearData){
				numMonthOfYear.add(numOfColYear);
				tableMonthYearDynamicHeader += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numOfColYear+"'>"+monthYear+"</th>";
				tableMonthYearUtilizationDH += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numOfColYear+"'>"+monthYear+"</th>";
			}
	
	/* -------------------------------------------------- TABLE CONSTRUCTION --------------------------------------------------------------------- */
			
			Double ov = 0.0;
			Double prevOv = 0.0;
			String caretIcon = "";
			String classColumn = "";
			String rowStyle = "";
			
			int c= 0;
					
			/*
			 * Default YEAR Table Construction
			 */
			for(int a = 0;a < numberOfOriginator;a++){
				rowStyle = "";
				
				if(a%2 == 1){
					rowStyle = "style=\"background-color:#F0F0F0\"";
				}
				
				tableDynamicContent += "\n\t<tr "+rowStyle+">";
				tableDynamicContent += "\n\t\t<td class=\"center aligned\">"+originatorName[a]+"</td>";
				
				tableYearUtilizationDC += "\n\t<tr "+rowStyle+">";
				tableYearUtilizationDC += "\n\t\t<td class=\"center aligned\">"+originatorName[a]+"</td>";
				
				caretIcon = ""; 
				classColumn = "";
				c = 0;
				for(int b = 0;b < countAllYearData;b++){
					ov = originatorValue[a][b];
					ov = (ov == null) ? 0.0 : ov;
					
					if(machineCapacity.get(originatorName[a].substring(0, 3)) == null){	
						percentage = 0.0;
					}else{
						mc = machineCapacity.get(originatorName[a].substring(0, 3));
						percentage = (ov/mc)*100;
					}
					
					yearUtilizationValue[a][b] = percentage;
					
					if(b>=1){
						c=b-1;
						prevOv = originatorValue[a][c];
						prevOv = (prevOv == null) ? 0.0 : prevOv;
						
						if(ov > prevOv){
							caretIcon = "<i class=\"small green caret up icon\"></i>";
							classColumn = "positive";
						}else if(ov < prevOv){
							caretIcon = "<i class=\"small red caret down icon\"></i>";
							classColumn = "negative";
						}else{
							classColumn = "";
						}
					}
					tableDynamicContent += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+decimalFormat.format(ov)+" "+caretIcon+"</td>";
					tableYearUtilizationDC += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+percentageFormat.format(percentage)+"% </td>";				
				}			
				tableDynamicContent += "\n\t</tr>";
				tableYearUtilizationDC += "\n\t</tr>";
			}
			
			/*
			 * Default MONTH Table Construction
			 */		
			
			for(int a = 0;a < numberOfOriginatorMonth;a++){ //Change from numberOfOriginator to numberOfOriginatorMonth
				rowStyle = "";
				
				if(a%2 == 1){
					rowStyle = "style=\"background-color:#F0F0F0\"";
				}
				
				tableMonthDynamicContent += "\n\t<tr "+rowStyle+">";
				tableMonthDynamicContent += "\n\t\t<td class=\"center aligned\">"+originatorNameMonth[a]+"</td>";
				
				tableMonthUtilizationDC += "\n\t<tr "+rowStyle+">";
				tableMonthUtilizationDC += "\n\t\t<td class=\"center aligned\">"+originatorNameMonth[a]+"</td>";
				
				caretIcon = ""; 
				classColumn = "";
				c = 0;
				for(int b = 0;b < countAllMonthData;b++){
					ov = originatorValueMonth[a][b]; 
					ov = (ov == null) ? 0.0 : ov;
					
					if(machineCapacity.get(originatorNameMonth[a].substring(0, 3)) == null){	
						percentage = 0.0;
					}else{
						mc = machineCapacity.get(originatorNameMonth[a].substring(0, 3));
						percentage = (ov/mc)*100;
					}
					
					monthUtilizationValue[a][b] = percentage;
					
					if(b>=1){
						c=b-1;
						prevOv = originatorValueMonth[a][c];
						prevOv = (prevOv == null) ? 0.0 : prevOv;
						
						if(ov > prevOv){
							caretIcon = "<i class=\"small green caret up icon\"></i>";
							classColumn = "positive";
						}else if(ov < prevOv){
							caretIcon = "<i class=\"small red caret down icon\"></i>";
							classColumn = "negative";
						}else{
							classColumn = "";
						}
					}
					tableMonthDynamicContent += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+decimalFormat.format(ov)+" "+caretIcon+"</td>";
					tableMonthUtilizationDC += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+percentageFormat.format(percentage)+"% </td>";
				}
				tableMonthDynamicContent += "\n\t</tr>";
				tableMonthUtilizationDC += "\n\t</tr>";
			}
					
			
			/*
			 * Transpose Year Table Construction
			 */
			
			int oneTimesHeader = 0;
			for(int a = 0;a < countAllYearData;a++){
				
				tableDynamicContentTranspose += "\n\t<tr>";
				tableDynamicContentTranspose += "\n\t\t<td class=\"center aligned\">"+arrAllYearData[a]+"</td>";
				
				tableYearUtilizationTransposeDC += "\n\t<tr>";
				tableYearUtilizationTransposeDC += "\n\t\t<td class=\"center aligned\">"+arrAllYearData[a]+"</td>";
				
				caretIcon = ""; 
				classColumn = "";
				for(int b = 0;b < numberOfOriginator;b++){
					if(oneTimesHeader<1){
						tableDynamicHeaderTranspose += "\n\t\t<th class=\"center aligned \" style=\"background-color:#D9E9FF\">"+originatorName[b]+"</th>";
						tableYearUtilizationTransposeDH += "\n\t\t<th class=\"center aligned \" style=\"background-color:#D9E9FF\">"+originatorName[b]+"</th>";
					}
					
					ov = originatorValue[b][a];
					ov = (ov == null) ? 0.0 : ov;
					
					if(b>=1){
						c=b-1;
						prevOv = originatorValue[c][a];
						prevOv = (prevOv == null) ? 0.0 : prevOv;
						
						if(ov > prevOv){
							caretIcon = "<i class=\"small green caret up icon\"></i>";
							classColumn = "positive";
						}else if(ov < prevOv){
							caretIcon = "<i class=\"small red caret down icon\"></i>";
							classColumn = "negative";
						}
					}
					tableDynamicContentTranspose += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+decimalFormat.format(ov)+" "+caretIcon+"</td>";
					tableYearUtilizationTransposeDC += "\n\t\t<td class=\"right aligned "+classColumn+"\">"+percentageFormat.format(yearUtilizationValue[b][a])+"% </td>";
				}
				oneTimesHeader++;
				tableDynamicContentTranspose += "\n\t</tr>";
				tableYearUtilizationTransposeDC += "\n\t</tr>";
			}
			
			
			/*
			 * TRANSPOSE MONTH Table Construction
			 */		
			
			indexOfYear = 0;
			oneTimesHeader = 0;
			
			int tempIndexOfYear = -1;
			int currentMonthOfYear = 1;
			
			for(int a = 0;a < countAllMonthData;a++){
				rowStyle = "";
				
				if(a%2 == 1){
					rowStyle = "style=\"background-color:#F0F0F0\"";
				}
				
				tableMonthDynamicContentTranspose += "\n\t<tr>";
				tableMonthUtilizationTransposeDC += "\n\t<tr>";
				
				caretIcon = ""; 
				classColumn = "";
				c = 0;
				for(int b = 0;b < numberOfOriginatorMonth;b++){ //Change from numberOfOriginator to numberOfOriginatorMonth
					ov = originatorValueMonth[b][a]; 
					ov = (ov == null) ? 0.0 : ov;			
					
					if(oneTimesHeader<1){
						tableMonthDynamicHeaderTitleTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+originatorName[b]+"</th>";
						tableMonthUtilizationTransposeTitleDH += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\">"+originatorName[b]+"</th>";
					}
					
					if(b == 0 && currentMonthOfYear == 1 && tempIndexOfYear < indexOfYear){
						tableMonthDynamicContentTranspose += "\n\t\t<td class=\"center aligned\" rowspan='"+numMonthOfYear.get(indexOfYear)+"'>"+arrAllYearData[indexOfYear]+"</td>";						
						
						tableMonthUtilizationTransposeDC += "\n\t\t<td class=\"center aligned\" rowspan='"+numMonthOfYear.get(indexOfYear)+"'>"+arrAllYearData[indexOfYear]+"</td>";
						tempIndexOfYear = indexOfYear;
					}
					
					if(b>=1){
						c=b-1;
						prevOv = originatorValueMonth[c][a];
						prevOv = (prevOv == null) ? 0.0 : prevOv;
						
						if(ov > prevOv){
							caretIcon = "<i class=\"small green caret up icon\"></i>";
							classColumn = "positive";
						}else if(ov < prevOv){
							caretIcon = "<i class=\"small red caret down icon\"></i>";
							classColumn = "negative";
						}else{
							classColumn = "";
						}
					}
					
					tableMonthDynamicContentTranspose += "\n\t\t<td class=\"center aligned\">"+nameOfMonthArray[currentMonthOfYear-1]+"</td>";
					tableMonthDynamicContentTranspose +="\n\t\t<td class=\"right aligned "+classColumn+"\">"+decimalFormat.format(ov)+" "+caretIcon+"</td>";
					
					tableMonthUtilizationTransposeDC +="\n\t\t<td class=\"right aligned "+classColumn+"\">"+percentageFormat.format(monthUtilizationValue[b][a])+"% </td>";
					
					if((b+1) == numberOfOriginatorMonth){
						int tempNumberMonthOfYear = (int) numMonthOfYear.get(indexOfYear);
						if(currentMonthOfYear < tempNumberMonthOfYear){
							currentMonthOfYear++;
						}else{
							currentMonthOfYear = 1;
							if(indexOfYear < countAllYearData){
								indexOfYear++;
							}
						}										
					}
				}
				tableMonthDynamicContentTranspose += "\n\t</tr>";
				tableMonthUtilizationTransposeDC += "\n\t</tr>";
				oneTimesHeader++;
			}
			
			
			/* Default Year Table */
			tableYear  = "<table class=\"ui celled structured table\">";
			tableYear += "\n<thead>";
			tableYear += "\n\t<tr>";
			tableYear += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\"><div id=\"titleMainClassifierYear\">Machine</div></th>";
			tableYear += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+countAllYearData+"'>Year</th>";
			tableYear += "\n</tr>";
			tableYear += "\n\t<tr>";		
			tableYear += tableDynamicHeader;
			tableYear += "\n\t</tr>";
			tableYear += "\n</thead>";		
			tableYear += "\n<tbody>";
			tableYear += tableDynamicContent;
			tableYear += "\n</tbody>";
			tableYear += "\n</table>";	
			
			/* Transpose Year Table */
			tableYearTranspose  = "<table class=\"ui celled structured table\">";
			tableYearTranspose += "\n<thead>";
			tableYearTranspose += "\n\t<tr>";
			tableYearTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\">Year</th>";
			tableYearTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numberOfOriginator+"'><div id=\"titleMainClassifierYearTranspose\">Machine</div></th>";
			tableYearTranspose += "\n</tr>";
			tableYearTranspose += "\n\t<tr>";		
			tableYearTranspose += tableDynamicHeaderTranspose;
			tableYearTranspose += "\n\t</tr>";
			tableYearTranspose += "\n</thead>";		
			tableYearTranspose += "\n<tbody>";
			tableYearTranspose += tableDynamicContentTranspose;
			tableYearTranspose += "\n</tbody>";
			tableYearTranspose += "\n</table>";
			
			/* Default Year Utilization */
			tableYearUtilization  = "<table class=\"ui celled structured table\">";
			tableYearUtilization += "\n<thead>";
			tableYearUtilization += "\n\t<tr>";
			tableYearUtilization += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\"><div id=\"titleMainClassifierYearUtilization\">Machine</div></th>";
			tableYearUtilization += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+countAllYearData+"'>Year</th>";
			tableYearUtilization += "\n</tr>";
			tableYearUtilization += "\n\t<tr>";		
			tableYearUtilization += tableYearUtilizationDH;
			tableYearUtilization += "\n\t</tr>";
			tableYearUtilization += "\n</thead>";		
			tableYearUtilization += "\n<tbody>";
			tableYearUtilization += tableYearUtilizationDC;
			tableYearUtilization += "\n</tbody>";
			tableYearUtilization += "\n</table>";
			
			/* Transpose Year Utilization */
			tableYearUtilizationTranspose  = "<table class=\"ui celled structured table\">";
			tableYearUtilizationTranspose += "\n<thead>";
			tableYearUtilizationTranspose += "\n\t<tr>";
			tableYearUtilizationTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\">Year</th>";
			tableYearUtilizationTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numberOfOriginator+"'><div id=\"titleMainClassifierYearUtilizationTranspose\">Machine</div></th>";
			tableYearUtilizationTranspose += "\n</tr>";
			tableYearUtilizationTranspose += "\n\t<tr>";		
			tableYearUtilizationTranspose += tableYearUtilizationTransposeDH;
			tableYearUtilizationTranspose += "\n\t</tr>";
			tableYearUtilizationTranspose += "\n</thead>";		
			tableYearUtilizationTranspose += "\n<tbody>";
			tableYearUtilizationTranspose += tableYearUtilizationTransposeDC;
			tableYearUtilizationTranspose += "\n</tbody>";
			tableYearUtilizationTranspose += "\n</table>";
			
			/* Default Month Table */
			tableMonth = "<table class=\"ui celled structured table\">";
			tableMonth += "\n<thead>";
			tableMonth += "\n\t<tr>";
			tableMonth += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\"><div id=\"titleMainClassifierMonth\">Machine</div></th>";
			tableMonth += tableMonthYearDynamicHeader;
			tableMonth += "\n</tr>";
			tableMonth += "\n\t<tr>";
			tableMonth += tableMonthDynamicHeader;
			tableMonth += "\n</tr>";
			tableMonth += "\n</thead>";
			tableMonth += "\n<tbody>";
			tableMonth += tableMonthDynamicContent;
			tableMonth += "\n</tbody>";
			tableMonth += "\n</table>";
			
			/* Transpose Month Table */
			tableMonthTranspose = "<table class=\"ui celled structured table\">";
			tableMonthTranspose += "\n<thead>";
			tableMonthTranspose += "\n\t<tr>";
			tableMonthTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan=\"2\" rowspan=\"2\">Year</th>";
			tableMonthTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numberOfOriginator+"'><div id=\"titleMainClassifierMonthTranspose\">Machine</div></th>";
			tableMonthTranspose += "\n</tr>";
			tableMonthTranspose += "\n\t<tr>";
			tableMonthTranspose += tableMonthDynamicHeaderTitleTranspose;
			tableMonthTranspose += "\n</tr>";
			tableMonthTranspose += "\n</thead>";
			tableMonthTranspose += "\n<tbody>";
			tableMonthTranspose += tableMonthDynamicContentTranspose ;
			tableMonthTranspose += "\n</tbody>";
			tableMonthTranspose += "\n</table>";
			
			/* Default Utilization Month Table */
			tableMonthUtilization = "<table class=\"ui celled structured table\">";
			tableMonthUtilization += "\n<thead>";
			tableMonthUtilization += "\n\t<tr>";
			tableMonthUtilization += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\"><div id=\"titleMainClassifierMonthUtilization\">Machine</div></th>";
			tableMonthUtilization += tableMonthYearUtilizationDH;
			tableMonthUtilization += "\n</tr>";
			tableMonthUtilization += "\n\t<tr>";
			tableMonthUtilization += tableMonthUtilizationDH;
			tableMonthUtilization += "\n</tr>";
			tableMonthUtilization += "\n</thead>";
			tableMonthUtilization += "\n<tbody>";
			tableMonthUtilization += tableMonthUtilizationDC;
			tableMonthUtilization += "\n</tbody>";
			tableMonthUtilization += "\n</table>";
			
			/* Transpose Utilization Month Table */
			tableMonthUtilizationTranspose = "<table class=\"ui celled structured table\">";
			tableMonthUtilizationTranspose += "\n<thead>";
			tableMonthUtilizationTranspose += "\n\t<tr>";
			tableMonthUtilizationTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" rowspan=\"2\">Year</th>";
			tableMonthUtilizationTranspose += "\n\t\t<th class=\"center aligned\" style=\"background-color:#D9E9FF\" colspan='"+numberOfOriginator+"'><div id=\"titleMainClassifierMonthUtilizationTranspose\">Machine</div></th>";
			tableMonthUtilizationTranspose += "\n</tr>";
			tableMonthUtilizationTranspose += "\n\t<tr>";
			tableMonthUtilizationTranspose += tableMonthUtilizationTransposeTitleDH;
			tableMonthUtilizationTranspose += "\n</tr>";
			tableMonthUtilizationTranspose += "\n</thead>";
			tableMonthUtilizationTranspose += "\n<tbody>";
			tableMonthUtilizationTranspose += tableMonthUtilizationTransposeDC ;
			tableMonthUtilizationTranspose += "\n</tbody>";
			tableMonthUtilizationTranspose += "\n</table>";			
			
			view.addObject("numberOfOriginatorMonth", numberOfOriginatorMonth);
			view.addObject("numberOfOriginatorYear", numberOfOriginator);
			view.addObject("countAllYearData",countAllYearData);
			view.addObject("countAllMonthData",countAllMonthData);
			view.addObject("jsonGraphYear",jsonGraphYear);
			view.addObject("jsonGraphUtilizationYear",jsonGraphUtilizationYear);		
			view.addObject("jsonGraphMonth",jsonGraphMonth);
			view.addObject("jsonGraphUtilizationMonth",jsonGraphUtilizationMonth);
			view.addObject("tableYear", tableYear);
			view.addObject("tableYearTranspose", tableYearTranspose);
			view.addObject("tableYearUtilization", tableYearUtilization);
			view.addObject("tableYearUtilizationTranspose", tableYearUtilizationTranspose);
			view.addObject("tableMonth", tableMonth);
			view.addObject("tableMonthTranspose", tableMonthTranspose);
			view.addObject("tableMonthUtilization", tableMonthUtilization);
			view.addObject("tableMonthUtilizationTranspose", tableMonthUtilizationTranspose);
			
			view.addObject("isModelNull", "false");
			view.addObject("apiURI", apiURI);
			view.addObject("jsonData", jsonData);			
		}else{
			view.addObject("isModelNull", "true");
			view.addObject("apiURI", apiURI);
			view.addObject("jsonData", jsonData);
		}
		
		return view;
	
	}
}
