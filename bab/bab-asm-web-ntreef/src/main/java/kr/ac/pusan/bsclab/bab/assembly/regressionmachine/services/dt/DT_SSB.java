package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SSB implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP12183"))
 if(we <= 760) return 60;
 if(we > 760) return 50;
if(p.equals("PP12197")) return 1;
if(p.equals("PP12199"))
 if(w <= 68) return 120;
 if(w > 68)
  if(w <= 223)
   if(t <= 1.7)
    if(we <= 368) return 80;
    if(we > 368) return 95;
   if(t > 1.7) return 80;
  if(w > 223) return 60;
if(p.equals("PP12202"))
 if(w <= 250)
  if(we <= 1575) return 65;
  if(we > 1575) return 70;
 if(w > 250)
  if(t <= 2.05)
   if(we <= 1440) return 40;
   if(we > 1440) return 75;
  if(t > 2.05) return 30;
if(p.equals("PP12204")) return 130;
if(p.equals("PP12205")) return 50;
if(p.equals("PP12206")) return 50;
if(p.equals("PP12211"))
 if(we <= 237)
  if(we <= 191)
   if(we <= 185)
    if(we <= 176) return 45;
    if(we > 176) return 50;
   if(we > 185) return 46.6666666666667;
  if(we > 191) return 75;
 if(we > 237)
  if(we <= 267) return 55;
  if(we > 267) return 50;
if(p.equals("PP12237")) return 60;
if(p.equals("PP12238"))
 if(we <= 334) return 65;
 if(we > 334) return 50;
if(p.equals("PP12249"))
 if(w <= 12) return 150;
 if(w > 12)
  if(t <= 1.3) return 85;
  if(t > 1.3)
   if(we <= 440) return 45;
   if(we > 440) return 65;
if(p.equals("PP12262")) return 45;
if(p.equals("PP12302"))
 if(t <= 1.6) return 45;
 if(t > 1.6) return 110;
if(p.equals("PP12308"))
 if(w <= 12) return 111.2;
 if(w > 12) return 105;
if(p.equals("PP12336"))
 if(we <= 308) return 45;
 if(we > 308) return 40;
if(p.equals("PP12337"))
 if(t <= 2.55)
  if(we <= 538) return 50;
  if(we > 538) return 35;
 if(t > 2.55)
  if(w <= 85) return 40;
  if(w > 85) return 35;
if(p.equals("PP12359"))
 if(w <= 260) return 40;
 if(w > 260) return 70;
if(p.equals("PP12393")) return 50;
if(p.equals("PP12395")) return 40;
if(p.equals("PP12410"))
 if(t <= 1.9)
  if(we <= 1046) return 45;
  if(we > 1046) return 115;
 if(t > 1.9) return 60;
if(p.equals("PP12411")) return 40;
if(p.equals("PP12421"))
 if(we <= 1342) return 70;
 if(we > 1342) return 40;
if(p.equals("PP12422")) return 115;
if(p.equals("PP12436")) return 20;
if(p.equals("PP12439"))
 if(t <= 1.65) return 105;
 if(t > 1.65) return 60;
if(p.equals("PP12446")) return 65;
if(p.equals("PP12447")) return 40;
if(p.equals("PP12449"))
 if(t <= 1.8) return 65;
 if(t > 1.8) return 50;
if(p.equals("PP12450")) return 105;
if(p.equals("PP12459")) return 40;
if(p.equals("PP12469"))
 if(w <= 242) return 50;
 if(w > 242) return 55;
if(p.equals("PP12470"))
 if(w <= 260) return 25;
 if(w > 260) return 30;
if(p.equals("PP12471")) return 30;
if(p.equals("PP12477")) return 40;
if(p.equals("PP12478")) return 40;
if(p.equals("PP12480"))
 if(we <= 352) return 50;
 if(we > 352)
  if(we <= 359) return 35;
  if(we > 359) return 50;
if(p.equals("PP12481")) return 40;
if(p.equals("PP12486"))
 if(t <= 2)
  if(we <= 401) return 45;
  if(we > 401) return 35;
 if(t > 2)
  if(we <= 320) return 35;
  if(we > 320) return 40;
if(p.equals("PP12487")) return 95;
if(p.equals("PP12499")) return 60;
if(p.equals("PP12501")) return 65;
if(p.equals("PP12514")) return 35;
if(p.equals("PP12531")) return 60;
if(p.equals("PP12532")) return 75;
if(p.equals("PP12545"))
 if(we <= 102)
  if(we <= 74) return 37.5;
  if(we > 74) return 45;
 if(we > 102)
  if(we <= 106) return 40;
  if(we > 106) return 20;
if(p.equals("PP12546"))
 if(w <= 98) return 115;
 if(w > 98) return 50;
if(p.equals("PP12550")) return 140;
if(p.equals("PP12553")) return 45;
if(p.equals("PP12555")) return 50;
if(p.equals("PP21895"))
 if(we <= 174) return 60;
 if(we > 174) return 40;
if(p.equals("PP21896"))
 if(t <= 1.87)
  if(t <= 1.76) return 115;
  if(t > 1.76) return 55;
 if(t > 1.87)
  if(w <= 110)
   if(we <= 168)
    if(we <= 149)
     if(we <= 143) return 30;
     if(we > 143) return 35;
    if(we > 149)
     if(we <= 163) return 40;
     if(we > 163) return 60;
   if(we > 168) return 40;
  if(w > 110)
   if(we <= 1918) return 35;
   if(we > 1918) return 70;
if(p.equals("PP21904"))
 if(t <= 3.2)
  if(w <= 238)
   if(t <= 2.61)
    if(we <= 2182) return 35;
    if(we > 2182)
     if(w <= 223) return 30;
     if(w > 223)
      if(we <= 2376) return 30;
      if(we > 2376) return 25;
   if(t > 2.61) return 35;
  if(w > 238)
   if(w <= 288)
    if(t <= 3.15)
     if(we <= 2934)
      if(t <= 3.1)
       if(w <= 270) return 20;
       if(w > 270) return 25;
      if(t > 3.1) return 20;
     if(we > 2934)
      if(w <= 270) return 25;
      if(w > 270)
       if(t <= 3.1) return 30;
       if(t > 3.1) return 25;
    if(t > 3.15) return 30;
   if(w > 288) return 25;
 if(t > 3.2)
  if(t <= 3.65)
   if(t <= 3.3) return 30;
   if(t > 3.3)
    if(t <= 3.6)
     if(w <= 299)
      if(t <= 3.44) return 45;
      if(t > 3.44) return 20;
     if(w > 299)
      if(we <= 3486) return 45;
      if(we > 3486) return 40;
    if(t > 3.6)
     if(we <= 3742) return 30;
     if(we > 3742) return 40;
  if(t > 3.65)
   if(t <= 4)
    if(w <= 356)
     if(w <= 324)
      if(we <= 3610) return 25;
      if(we > 3610)
       if(we <= 3742) return 50;
       if(we > 3742) return 35;
     if(w > 324)
      if(we <= 3602) return 35;
      if(we > 3602) return 25;
    if(w > 356)
     if(we <= 3842) return 30;
     if(we > 3842) return 35;
   if(t > 4)
    if(we <= 3454) return 20;
    if(we > 3454)
     if(we <= 3508) return 30;
     if(we > 3508) return 37.5;
if(p.equals("PP21911"))
 if(t <= 3.3)
  if(t <= 2.35)
   if(we <= 135) return 40;
   if(we > 135) return 45;
  if(t > 2.35)
   if(w <= 51) return 40;
   if(w > 51) return 50;
 if(t > 3.3)
  if(t <= 3.7) return 55;
  if(t > 3.7)
   if(we <= 702) return 60;
   if(we > 702) return 40;
if(p.equals("PP21914"))
 if(t <= 2.22)
  if(w <= 199)
   if(w <= 185)
    if(t <= 2.16) return 35;
    if(t > 2.16)
     if(we <= 1640) return 45;
     if(we > 1640)
      if(we <= 1664) return 35;
      if(we > 1664) return 40;
   if(w > 185)
    if(we <= 1657) return 40;
    if(we > 1657)
     if(t <= 2.13) return 45;
     if(t > 2.13)
      if(we <= 1774) return 60;
      if(we > 1774) return 40;
  if(w > 199)
   if(we <= 1821) return 45;
   if(we > 1821)
    if(we <= 1842) return 40;
    if(we > 1842) return 45;
 if(t > 2.22)
  if(w <= 198)
   if(t <= 2.3)
    if(t <= 2.26) return 25;
    if(t > 2.26) return 40;
   if(t > 2.3)
    if(we <= 1780)
     if(we <= 1712)
      if(t <= 2.4)
       if(t <= 2.38)
        if(we <= 1568)
         if(we <= 1540) return 45;
         if(we > 1540) return 40;
        if(we > 1568)
         if(t <= 2.34)
          if(we <= 1630)
           if(we <= 1589) return 30;
           if(we > 1589) return 35;
          if(we > 1630)
           if(we <= 1689) return 30;
           if(we > 1689) return 40;
         if(t > 2.34) return 45;
       if(t > 2.38) return 40;
      if(t > 2.4)
       if(t <= 2.48) return 45;
       if(t > 2.48) return 30;
     if(we > 1712) return 35;
    if(we > 1780)
     if(t <= 2.45)
      if(we <= 1828) return 45;
      if(we > 1828)
       if(we <= 1880) return 30;
       if(we > 1880)
        if(we <= 1900) return 40;
        if(we > 1900) return 45;
     if(t > 2.45) return 40;
  if(w > 198)
   if(w <= 199)
    if(we <= 1774)
     if(we <= 1610)
      if(we <= 1536) return 30;
      if(we > 1536)
       if(we <= 1602)
        if(we <= 1552) return 50;
        if(we > 1552) return 35;
       if(we > 1602) return 45;
     if(we > 1610)
      if(we <= 1757) return 40;
      if(we > 1757)
       if(we <= 1768) return 27.5;
       if(we > 1768) return 35;
    if(we > 1774)
     if(we <= 1828)
      if(we <= 1793) return 40;
      if(we > 1793) return 65;
     if(we > 1828)
      if(we <= 1900)
       if(we <= 1878)
        if(we <= 1855) return 45;
        if(we > 1855) return 15;
       if(we > 1878) return 40;
      if(we > 1900)
       if(we <= 2026) return 45;
       if(we > 2026) return 55;
   if(w > 199)
    if(we <= 1752)
     if(w <= 208) return 35;
     if(w > 208) return 25;
    if(we > 1752)
     if(t <= 2.475)
      if(w <= 212)
       if(w <= 204)
        if(we <= 1888) return 30;
        if(we > 1888) return 40;
       if(w > 204)
        if(t <= 2.42)
         if(we <= 1962) return 40;
         if(we > 1962) return 20;
        if(t > 2.42)
         if(we <= 1830) return 20;
         if(we > 1830) return 30;
      if(w > 212) return 35;
     if(t > 2.475) return 40;
if(p.equals("PP21915"))
 if(t <= 2.4)
  if(we <= 2207)
   if(we <= 2174) return 30;
   if(we > 2174) return 35;
  if(we > 2207) return 30;
 if(t > 2.4)
  if(w <= 250)
   if(w <= 236)
    if(we <= 2052) return 25;
    if(we > 2052) return 50;
   if(w > 236)
    if(t <= 2.71) return 30;
    if(t > 2.71) return 10;
  if(w > 250) return 20;
if(p.equals("PP21917"))
 if(w <= 256)
  if(we <= 1686)
   if(t <= 1.96)
    if(we <= 1601) return 50;
    if(we > 1601) return 35;
   if(t > 1.96) return 30;
  if(we > 1686)
   if(we <= 2182)
    if(t <= 2.4) return 40;
    if(t > 2.4) return 45;
   if(we > 2182) return 50;
 if(w > 256)
  if(t <= 3.44)
   if(t <= 3.2)
    if(t <= 3.1)
     if(we <= 3402) return 25;
     if(we > 3402)
      if(we <= 3486) return 15;
      if(we > 3486) return 30;
    if(t > 3.1)
     if(we <= 3160)
      if(w <= 301.5) return 35;
      if(w > 301.5) return 30;
     if(we > 3160) return 20;
   if(t > 3.2)
    if(w <= 301) return 25;
    if(w > 301)
     if(we <= 3110) return 30;
     if(we > 3110) return 25;
  if(t > 3.44)
   if(we <= 3486) return 30;
   if(we > 3486) return 20;
if(p.equals("PP21920"))
 if(t <= 2.1)
  if(we <= 388) return 100;
  if(we > 388) return 90;
 if(t > 2.1)
  if(we <= 204)
   if(we <= 179) return 80;
   if(we > 179) return 75;
  if(we > 204)
   if(we <= 521)
    if(w <= 75)
     if(t <= 3.25)
      if(t <= 2.34) return 35;
      if(t > 2.34)
       if(we <= 298) return 45;
       if(we > 298)
        if(we <= 314)
         if(t <= 2.82) return 50;
         if(t > 2.82) return 40;
        if(we > 314)
         if(we <= 337) return 65;
         if(we > 337) return 40;
     if(t > 3.25)
      if(we <= 320) return 40;
      if(we > 320) return 110;
    if(w > 75)
     if(t <= 2.55) return 70;
     if(t > 2.55) return 65;
   if(we > 521)
    if(we <= 770)
     if(w <= 114) return 55;
     if(w > 114) return 60;
    if(we > 770) return 35;
if(p.equals("PP21922"))
 if(w <= 15) return 110;
 if(w > 15)
  if(we <= 186)
   if(we <= 174)
    if(we <= 166) return 42.5;
    if(we > 166) return 40;
   if(we > 174) return 45;
  if(we > 186)
   if(we <= 192) return 55;
   if(we > 192) return 40;
if(p.equals("PP21923"))
 if(w <= 82)
  if(t <= 2.34)
   if(t <= 1.85) return 127.5;
   if(t > 1.85)
    if(we <= 420) return 40;
    if(we > 420) return 45;
  if(t > 2.34)
   if(w <= 58)
    if(we <= 317) return 60;
    if(we > 317)
     if(we <= 357) return 100;
     if(we > 357)
      if(we <= 365) return 40;
      if(we > 365) return 50;
   if(w > 58)
    if(w <= 75) return 85;
    if(w > 75) return 60;
 if(w > 82)
  if(w <= 178)
   if(we <= 547)
    if(t <= 2.15) return 55;
    if(t > 2.15) return 60;
   if(we > 547)
    if(we <= 1059)
     if(w <= 131)
      if(we <= 552) return 40;
      if(we > 552) return 70;
     if(w > 131)
      if(we <= 890) return 30;
      if(we > 890)
       if(we <= 908) return 50;
       if(we > 908)
        if(we <= 1004) return 40;
        if(we > 1004) return 50;
    if(we > 1059)
     if(t <= 3.05) return 30;
     if(t > 3.05) return 45;
  if(w > 178)
   if(we <= 1064) return 45;
   if(we > 1064) return 60;
if(p.equals("PP21936"))
 if(we <= 544) return 35;
 if(we > 544) return 50;
if(p.equals("PP21938")) return 55;
if(p.equals("PP21940"))
 if(we <= 1150)
  if(we <= 953)
   if(we <= 722)
    if(we <= 702)
     if(we <= 579)
      if(we <= 542) return 70;
      if(we > 542) return 30;
     if(we > 579)
      if(we <= 613) return 35;
      if(we > 613)
       if(we <= 639) return 30;
       if(we > 639) return 55;
    if(we > 702)
     if(we <= 708) return 60;
     if(we > 708) return 50;
   if(we > 722)
    if(we <= 753)
     if(we <= 730) return 65;
     if(we > 730)
      if(we <= 743) return 55;
      if(we > 743) return 55.9166666666667;
    if(we > 753)
     if(we <= 771)
      if(we <= 763) return 70;
      if(we > 763) return 45;
     if(we > 771)
      if(t <= 2.03) return 60;
      if(t > 2.03) return 55;
  if(we > 953)
   if(t <= 2.21)
    if(we <= 1129) return 55;
    if(we > 1129) return 50;
   if(t > 2.21)
    if(w <= 180)
     if(we <= 1003)
      if(we <= 993)
       if(we <= 980) return 40;
       if(we > 980) return 50;
      if(we > 993) return 45;
     if(we > 1003)
      if(we <= 1134)
       if(we <= 1013) return 50;
       if(we > 1013) return 40;
      if(we > 1134)
       if(we <= 1142) return 55;
       if(we > 1142) return 50;
    if(w > 180) return 45;
 if(we > 1150)
  if(t <= 2.21)
   if(t <= 2.18)
    if(w <= 180)
     if(we <= 1170) return 60;
     if(we > 1170) return 45;
    if(w > 180) return 110;
   if(t > 2.18)
    if(we <= 1256) return 70;
    if(we > 1256)
     if(we <= 1316) return 40;
     if(we > 1316) return 30;
  if(t > 2.21) return 60;
if(p.equals("PP21942")) return 85;
if(p.equals("PP21956")) return 70;
if(p.equals("PP21975"))
 if(t <= 2.55)
  if(t <= 2.4) return 25;
  if(t > 2.4)
   if(we <= 1831) return 25;
   if(we > 1831) return 20;
 if(t > 2.55)
  if(t <= 2.61)
   if(w <= 225)
    if(we <= 2366) return 20;
    if(we > 2366) return 30;
   if(w > 225) return 30;
  if(t > 2.61)
   if(t <= 2.67)
    if(we <= 2066) return 30;
    if(we > 2066)
     if(we <= 2246) return 25;
     if(we > 2246) return 30;
   if(t > 2.67) return 25;
if(p.equals("PP21994"))
 if(t <= 2.7)
  if(we <= 2154)
   if(w <= 225)
    if(we <= 2026)
     if(we <= 1948) return 25;
     if(we > 1948) return 30;
    if(we > 2026) return 25;
   if(w > 225) return 30;
  if(we > 2154)
   if(w <= 232)
    if(t <= 2.545)
     if(we <= 2366)
      if(we <= 2345) return 20;
      if(we > 2345) return 15;
     if(we > 2366)
      if(we <= 2540) return 20;
      if(we > 2540)
       if(we <= 2642) return 25;
       if(we > 2642) return 20;
    if(t > 2.545)
     if(we <= 2868) return 25;
     if(we > 2868) return 20;
   if(w > 232)
    if(we <= 2274) return 20;
    if(we > 2274)
     if(we <= 2536) return 25;
     if(we > 2536) return 20;
 if(t > 2.7)
  if(we <= 2207)
   if(t <= 2.85) return 25;
   if(t > 2.85) return 20;
  if(we > 2207)
   if(we <= 2554)
    if(t <= 2.95)
     if(we <= 2434)
      if(we <= 2340)
       if(we <= 2258)
        if(we <= 2252) return 30;
        if(we > 2252) return 50;
       if(we > 2258)
        if(we <= 2270) return 35;
        if(we > 2270) return 30;
      if(we > 2340)
       if(t <= 2.82)
        if(we <= 2378) return 20;
        if(we > 2378) return 30;
       if(t > 2.82)
        if(we <= 2406) return 50;
        if(we > 2406) return 30;
     if(we > 2434) return 40;
    if(t > 2.95) return 20;
   if(we > 2554)
    if(we <= 2974)
     if(t <= 2.8) return 25;
     if(t > 2.8) return 35;
    if(we > 2974) return 30;
if(p.equals("PP21996"))
 if(w <= 86) return 50;
 if(w > 86) return 55;
if(p.equals("PP22001"))
 if(t <= 2.85)
  if(we <= 322)
   if(w <= 40) return 125;
   if(w > 40)
    if(w <= 51)
     if(we <= 187)
      if(we <= 184) return 50;
      if(we > 184) return 40;
     if(we > 187) return 75;
    if(w > 51)
     if(we <= 267) return 80;
     if(we > 267) return 50;
  if(we > 322)
   if(t <= 1.895)
    if(t <= 1.6) return 70;
    if(t > 1.6)
     if(we <= 505) return 35;
     if(we > 505)
      if(we <= 602) return 65;
      if(we > 602) return 50;
   if(t > 1.895)
    if(w <= 103.5)
     if(t <= 2.15) return 70;
     if(t > 2.15) return 55;
    if(w > 103.5)
     if(we <= 704) return 67.5;
     if(we > 704) return 70;
 if(t > 2.85)
  if(we <= 1119)
   if(we <= 1001) return 115;
   if(we > 1001) return 30;
  if(we > 1119)
   if(w <= 125)
    if(w <= 118) return 35;
    if(w > 118)
     if(we <= 1315)
      if(we <= 1210) return 35;
      if(we > 1210) return 50;
     if(we > 1315) return 60;
   if(w > 125)
    if(w <= 145)
     if(we <= 1258) return 55;
     if(we > 1258) return 25;
    if(w > 145)
     if(we <= 1493)
      if(we <= 1475) return 60;
      if(we > 1475) return 45;
     if(we > 1493) return 35;
if(p.equals("PP22002"))
 if(we <= 691)
  if(t <= 2.38)
   if(w <= 45) return 55;
   if(w > 45) return 70;
  if(t > 2.38)
   if(w <= 45) return 60;
   if(w > 45)
    if(w <= 69) return 40;
    if(w > 69)
     if(t <= 2.48) return 40;
     if(t > 2.48)
      if(we <= 375) return 50;
      if(we > 375) return 45;
 if(we > 691)
  if(we <= 1697)
   if(t <= 2.9)
    if(t <= 2.48)
     if(w <= 120)
      if(w <= 96)
       if(we <= 822) return 50;
       if(we > 822) return 25;
      if(w > 96) return 40;
     if(w > 120)
      if(we <= 1307)
       if(we <= 1264) return 50;
       if(we > 1264) return 30;
      if(we > 1307) return 45;
    if(t > 2.48)
     if(we <= 1314)
      if(w <= 106)
       if(we <= 971) return 35;
       if(we > 971) return 50;
      if(w > 106) return 30;
     if(we > 1314) return 25;
   if(t > 2.9)
    if(we <= 1167)
     if(we <= 1013)
      if(we <= 997) return 45;
      if(we > 997)
       if(we <= 1006) return 35;
       if(we > 1006) return 30;
     if(we > 1013)
      if(we <= 1053) return 25;
      if(we > 1053)
       if(we <= 1068) return 35;
       if(we > 1068)
        if(we <= 1152) return 45;
        if(we > 1152) return 55;
    if(we > 1167)
     if(w <= 118)
      if(we <= 1205) return 30;
      if(we > 1205)
       if(we <= 1219)
        if(we <= 1214) return 55;
        if(we > 1214) return 38.3333333333333;
       if(we > 1219) return 40;
     if(w > 118)
      if(t <= 3.05)
       if(w <= 125)
        if(we <= 1322) return 45;
        if(we > 1322)
         if(we <= 1332) return 30;
         if(we > 1332) return 15;
       if(w > 125)
        if(we <= 1473) return 30;
        if(we > 1473)
         if(we <= 1620) return 35;
         if(we > 1620) return 30;
      if(t > 3.05) return 50;
  if(we > 1697)
   if(we <= 1768) return 35;
   if(we > 1768) return 40;
if(p.equals("PP22003"))
 if(t <= 2.71)
  if(t <= 1.3) return 30;
  if(t > 1.3)
   if(w <= 199)
    if(we <= 1528) return 50;
    if(we > 1528) return 30;
   if(w > 199)
    if(we <= 1404) return 35;
    if(we > 1404) return 40;
 if(t > 2.71) return 30;
if(p.equals("PP22007")) return 50;
if(p.equals("PP22009"))
 if(we <= 105)
  if(we <= 94)
   if(t <= 1.6) return 40;
   if(t > 1.6)
    if(we <= 55) return 40;
    if(we > 55) return 60;
  if(we > 94)
   if(we <= 101)
    if(we <= 98)
     if(we <= 96) return 30;
     if(we > 96) return 35;
    if(we > 98)
     if(t <= 1.84) return 40;
     if(t > 1.84) return 35.6166666666667;
   if(we > 101)
    if(t <= 1.84)
     if(we <= 103) return 36.1666666666667;
     if(we > 103) return 40.0666666666667;
    if(t > 1.84)
     if(we <= 103) return 40;
     if(we > 103) return 30;
 if(we > 105)
  if(t <= 1.93)
   if(t <= 1.7) return 45;
   if(t > 1.7)
    if(we <= 2965) return 37.5;
    if(we > 2965) return 1;
  if(t > 1.93)
   if(t <= 2.13)
    if(t <= 1.97) return 85;
    if(t > 1.97)
     if(we <= 113) return 55;
     if(we > 113) return 35;
   if(t > 2.13)
    if(we <= 113) return 45;
    if(we > 113) return 40;
if(p.equals("PP22015")) return 40;
if(p.equals("PP22017")) return 60;
if(p.equals("PP22037"))
 if(we <= 968) return 45;
 if(we > 968) return 40;
if(p.equals("PP22052"))
 if(t <= 1.87)
  if(we <= 109) return 35;
  if(we > 109) return 40;
 if(t > 1.87)
  if(we <= 141) return 30;
  if(we > 141)
   if(we <= 1600) return 45;
   if(we > 1600) return 35;
if(p.equals("PP22054"))
 if(we <= 2474)
  if(we <= 2016) return 45;
  if(we > 2016)
   if(we <= 2222) return 35;
   if(we > 2222) return 40;
 if(we > 2474) return 30;
if(p.equals("PP22066")) return 50;
if(p.equals("PP22067"))
 if(we <= 181)
  if(we <= 172) return 30;
  if(we > 172) return 45;
 if(we > 181) return 25;
if(p.equals("PP22070"))
 if(t <= 2.55) return 105;
 if(t > 2.55) return 35;
if(p.equals("PP22075"))
 if(we <= 1090) return 70;
 if(we > 1090) return 30;
if(p.equals("PP22184"))
 if(we <= 50) return 35;
 if(we > 50) return 40;
if(p.equals("PP22195")) return 95;
if(p.equals("PP22209"))
 if(t <= 2.34) return 35;
 if(t > 2.34) return 55;
if(p.equals("PP22210")) return 45;
if(p.equals("PP22246")) return 30;
if(p.equals("PP22250")) return 95;
if(p.equals("PP22278"))
 if(t <= 1.6) return 80;
 if(t > 1.6) return 40;
if(p.equals("PP22285")) return 55;
if(p.equals("PP22293")) return 110;
if(p.equals("PP22295"))
 if(we <= 1900)
  if(we <= 1826) return 20;
  if(we > 1826) return 45;
 if(we > 1900) return 35;
if(p.equals("PP22303")) return 45;
if(p.equals("PP22307"))
 if(we <= 303) return 70;
 if(we > 303) return 60;
if(p.equals("PP22321"))
 if(we <= 178) return 45;
 if(we > 178)
  if(we <= 189) return 50;
  if(we > 189) return 110;
if(p.equals("PP22322"))
 if(w <= 110)
  if(we <= 180)
   if(we <= 166) return 38.2833333333333;
   if(we > 166) return 40;
  if(we > 180)
   if(we <= 186) return 30;
   if(we > 186)
    if(we <= 191)
     if(we <= 188) return 35;
     if(we > 188) return 70;
    if(we > 191)
     if(we <= 197) return 30;
     if(we > 197) return 40;
 if(w > 110)
  if(we <= 2226) return 50;
  if(we > 2226) return 40;
if(p.equals("PP22336"))
 if(we <= 182)
  if(we <= 167) return 40;
  if(we > 167)
   if(we <= 178)
    if(we <= 173) return 35;
    if(we > 173) return 45;
   if(we > 178) return 40;
 if(we > 182)
  if(we <= 187) return 42.4166666666667;
  if(we > 187) return 25;
if(p.equals("PP22339"))
 if(we <= 315) return 55;
 if(we > 315) return 40;
if(p.equals("PP22340")) return 90;
if(p.equals("PP22341"))
 if(t <= 1.97) return 60;
 if(t > 1.97) return 65;
if(p.equals("PP22352"))
 if(we <= 181)
  if(we <= 168)
   if(we <= 162) return 30;
   if(we > 162)
    if(we <= 165) return 25.9333333333333;
    if(we > 165) return 25;
  if(we > 168)
   if(we <= 174)
    if(we <= 171) return 35.85;
    if(we > 171) return 36.25;
   if(we > 174)
    if(we <= 177) return 34.8833333333333;
    if(we > 177) return 28.5666666666667;
 if(we > 181)
  if(w <= 110)
   if(we <= 188) return 35;
   if(we > 188) return 40;
  if(w > 110)
   if(we <= 2330)
    if(we <= 2156) return 35;
    if(we > 2156) return 40;
   if(we > 2330)
    if(we <= 2356) return 25;
    if(we > 2356)
     if(we <= 2406) return 45;
     if(we > 2406) return 50;
if(p.equals("PP22356"))
 if(w <= 125) return 55;
 if(w > 125)
  if(we <= 945) return 35;
  if(we > 945)
   if(we <= 1034) return 45;
   if(we > 1034) return 40;
if(p.equals("PP22357"))
 if(t <= 2.34) return 40;
 if(t > 2.34) return 45;
if(p.equals("PP22358"))
 if(t <= 1.78)
  if(we <= 109) return 30;
  if(we > 109) return 40;
 if(t > 1.78)
  if(we <= 93) return 35;
  if(we > 93)
   if(we <= 103) return 65;
   if(we > 103) return 35;
if(p.equals("PP22365"))
 if(w <= 195) return 100;
 if(w > 195) return 75;
if(p.equals("PP22369")) return 55;
if(p.equals("PP22378")) return 45;
if(p.equals("PP22387")) return 40;
if(p.equals("PP22415")) return 70;
if(p.equals("PP22420")) return 60;
if(p.equals("PP22421")) return 50;
if(p.equals("PP22422")) return 50;
if(p.equals("PP22424"))
 if(we <= 2910) return 40;
 if(we > 2910) return 45;
if(p.equals("PP22437")) return 30;
if(p.equals("PP22447")) return 75;
if(p.equals("PP22473"))
 if(t <= 2.82) return 45;
 if(t > 2.82) return 60;
if(p.equals("PP22474"))
 if(w <= 63) return 55;
 if(w > 63) return 65;
if(p.equals("PP22486"))
 if(t <= 2.23) return 120;
 if(t > 2.23)
  if(we <= 614) return 25;
  if(we > 614) return 30;
if(p.equals("PP22498"))
 if(w <= 103)
  if(t <= 1.97)
   if(t <= 1.87)
    if(we <= 528) return 30;
    if(we > 528) return 50;
   if(t > 1.87) return 90;
  if(t > 1.97)
   if(w <= 36)
    if(we <= 172) return 138;
    if(we > 172) return 80;
   if(w > 36)
    if(w <= 92)
     if(w <= 80) return 40;
     if(w > 80) return 55;
    if(w > 92) return 85;
 if(w > 103)
  if(t <= 2.95)
   if(w <= 114) return 35;
   if(w > 114) return 30;
  if(t > 2.95) return 45;
if(p.equals("PP22503"))
 if(t <= 3.2)
  if(we <= 1020) return 40;
  if(we > 1020) return 35;
 if(t > 3.2) return 115;
if(p.equals("PP22504")) return 60;
if(p.equals("PP22507"))
 if(t <= 1.83)
  if(w <= 98)
   if(we <= 107)
    if(t <= 1.6) return 41.7333333333333;
    if(t > 1.6) return 41.6666666666667;
   if(we > 107) return 40;
  if(w > 98) return 45;
 if(t > 1.83) return 55;
if(p.equals("PP22508")) return 40;
if(p.equals("PP22510")) return 82.4166666666667;
if(p.equals("PP22513")) return 60;
if(p.equals("PP22514"))
 if(w <= 197) return 15;
 if(w > 197)
  if(we <= 2044) return 35;
  if(we > 2044)
   if(we <= 2165) return 40;
   if(we > 2165) return 35;
if(p.equals("PP22515"))
 if(t <= 1.84)
  if(t <= 1.65) return 40;
  if(t > 1.65)
   if(we <= 108) return 45;
   if(we > 108) return 30;
 if(t > 1.84)
  if(we <= 107)
   if(we <= 80) return 42.5833333333333;
   if(we > 80) return 35.1666666666667;
  if(we > 107)
   if(we <= 110) return 40;
   if(we > 110) return 31.7833333333333;
if(p.equals("PP22520"))
 if(t <= 1.93) return 40;
 if(t > 1.93) return 50;
if(p.equals("PP22521")) return 50;
if(p.equals("PP22530"))
 if(w <= 206)
  if(t <= 2.4) return 40;
  if(t > 2.4) return 70;
 if(w > 206) return 35;
if(p.equals("PP22531"))
 if(we <= 2012)
  if(w <= 225) return 20;
  if(w > 225)
   if(we <= 1930) return 10;
   if(we > 1930) return 20;
 if(we > 2012)
  if(w <= 225) return 25;
  if(w > 225)
   if(we <= 2120) return 20;
   if(we > 2120) return 25;
if(p.equals("PP22534"))
 if(we <= 2846)
  if(w <= 242)
   if(we <= 1884) return 45;
   if(we > 1884) return 40;
  if(w > 242)
   if(we <= 2726)
    if(we <= 2324)
     if(we <= 2258) return 35;
     if(we > 2258) return 20;
    if(we > 2324)
     if(w <= 260) return 30;
     if(w > 260) return 35;
   if(we > 2726)
    if(we <= 2790) return 20;
    if(we > 2790) return 40;
 if(we > 2846)
  if(t <= 3.44)
   if(w <= 270) return 25;
   if(w > 270)
    if(we <= 3348) return 20;
    if(we > 3348) return 25;
  if(t > 3.44) return 25;
if(p.equals("PP22535"))
 if(t <= 2.67)
  if(t <= 2.49) return 110;
  if(t > 2.49)
   if(we <= 51) return 120;
   if(we > 51) return 40;
 if(t > 2.67)
  if(t <= 3.05)
   if(w <= 125)
    if(w <= 114)
     if(we <= 1119) return 15;
     if(we > 1119)
      if(we <= 1163) return 30;
      if(we > 1163) return 35;
    if(w > 114) return 30;
   if(w > 125)
    if(we <= 1422) return 50;
    if(we > 1422) return 35;
  if(t > 3.05)
   if(t <= 3.3) return 45;
   if(t > 3.3)
    if(we <= 332) return 30;
    if(we > 332) return 40;
if(p.equals("PP22546")) return 30;
if(p.equals("PP22558")) return 50;
if(p.equals("PP22563"))
 if(w <= 216) return 30;
 if(w > 216)
  if(t <= 2.49) return 40;
  if(t > 2.49) return 15;
if(p.equals("PP22566"))
 if(t <= 2.8) return 120;
 if(t > 2.8) return 50;
if(p.equals("PP22568")) return 55;
if(p.equals("PP22570")) return 72.5;
if(p.equals("PP22571")) return 40;
if(p.equals("PP22573"))
 if(w <= 90) return 40;
 if(w > 90)
  if(we <= 1152) return 35;
  if(we > 1152) return 25;
if(p.equals("PP22574"))
 if(t <= 2.71) return 25;
 if(t > 2.71) return 30;
if(p.equals("PP22590")) return 40;
if(p.equals("PP22601"))
 if(t <= 1.81) return 50;
 if(t > 1.81) return 45;
if(p.equals("WSA1253")) return 60;
return 60.0;
}
}
