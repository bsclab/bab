package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_PP2 implements ANN_base {
	
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.27064121e+00, -2.30282173e+01, -1.38827009e+01, 5.58864594e+01
            				, -9.39249456e-01, 1.03313971e+01, -1.94530022e+00, -1.66605175e+00
            				, 9.95366898e+01, 1.16042495e+00, -2.60933781e+00, -5.37920117e-01
            				, -2.29312515e+00, -1.22396517e+00, -1.26956677e+00, -4.12648693e-02
            				, -6.87946305e-02, -1.25745523e+00, 1.81487694e-01, 1.51956767e-01}, 
            			{-1.04445374e+00, -9.48310733e-01, 3.92594896e-02, 1.21826363e+00
        					, -2.13212460e-01, 6.56289697e-01, -2.72885418e+00, 1.24082589e+00
        					, 4.83445454e+00, 1.72370568e-01, -1.87130213e-01, 1.11907518e+00
        					, 1.37981534e+00, -1.12254405e+00, -5.42422771e-01, 1.34130025e+00
        					, -1.18948114e+00, 4.37056832e-02, -8.51925388e-02, 2.12441683e-02},
            			{-7.03827962e-02, 2.94243336e+00, 6.05958998e-01, 2.36535263e+00
    						, -1.76548958e+00, -1.64863503e+00, -7.27174044e-01, -1.29072225e+00
    						, 5.02224624e-01, 1.33415127e+00, -6.25964880e-01, 4.98625487e-01
    						, 7.64593661e-01, -2.16906333e+00, -3.48227441e-01, 9.65759084e-02
    						, 2.34517306e-01, -6.75120771e-01, 7.25269020e-02, 1.09408490e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.77564496, 2.6546185, -0.14942244, -0.32106155, -0.05108396, 0.94733322
            			, -0.49026537, -0.2859396, 0.44009012, -2.02603555, 0.05428407, -0.86530155
            			, -1.55351019, -1.34387112, -1.69411004, -1.24700761, -0.41967845, -0.02905503
            			, -0.41536161, -0.37566686};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.45825803,
            			1.5103811,
            			0.41479728,
            			-1.54807389,
            			-1.35826695,
            			-0.81722003,
            			0.36020204,
            			0.93246293,
            			-1.50227511,
            			-0.74510026,
            			-0.44126478,
            			-0.47856978,
            			0.51396143,
            			-0.938263,
            			1.01222062,
            			0.25335065,
            			0.44709113,
            			1.66815364,
            			-0.34677973,
            			1.21001065};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.15376551};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 11.0;
	}

	@Override
	public double getMaxWidth() {
		
		return 1330.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25930.0;
	}

	@Override
	public double getMinThick() {
		
		return 1.4;
	}

	@Override
	public double getMinWidth() {
		
		return 801.0;
	}

	@Override
	public double getMinWeight() {
		
		return 7215.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
