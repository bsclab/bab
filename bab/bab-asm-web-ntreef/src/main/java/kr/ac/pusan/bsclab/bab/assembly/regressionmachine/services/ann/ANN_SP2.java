package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SP2 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{0.32158327, -0.13008972, -0.02624439, -0.29794562, 0.74068052, -0.31770688
            				, 0.42515114, -0.42143422, 0.65707016, -0.97624296, 0.40220508, 3.77770615
            				, -0.18284252, 0.83398926, -1.25690532, -0.03483936, -2.33332419, -1.19606733
            				, -0.75556743, -1.08629692}, 
    					{1.35724044, -0.09600909, -1.42126083, 0.25244716, 1.09012389, 1.02621734
        					, 0.51059163, -0.10511979, -1.855214, -0.13344733, 1.89303839, 2.03116345
        					, -0.7291832, 0.19752607, 2.10976887, 0.1758869, -0.50136334, 0.25367239
        					, 1.31638288, 2.06626582},
            			{-0.8704446, 0.24658498, 0.36258882, -0.08811539, -0.2484182, -0.17132024
    						, 0.42079455, -2.04988408, -0.0056495, 3.52519798, -0.47550938, -0.46939689
    						, 0.85729408, 0.41862825, 0.41385111, 0.87221885, 0.0670551, -0.13362607
    						, -0.10659926, 1.84856951}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.54111499, -2.08355021, -1.21421981, 0.92106909, -0.15989512, -0.83948189
            			, -0.18410893, 0.01875225, -0.25343215, -0.81359422, -1.03881168, 0.89385611
            			, 1.84771311, -0.68854672, 2.37593246, -1.22023141, -1.41172171, 1.54503608
            			, -0.64503646, -0.4781929};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {1.27674913,
            			0.07614195,
            			-1.93929386,
            			-0.98120862,
            			1.80512047,
            			-1.34460819,
            			-0.5514406,
            			0.32598698,
            			-0.62973803,
            			-0.81859541,
            			-1.08468843,
            			-1.69543886,
            			-0.24621114,
            			0.18252264,
            			-0.7551958,
            			-0.98100346,
            			0.4807421,
            			1.58509028,
            			0.64604032,
            			-2.15223765};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.5647645};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.35;
	}

	@Override
	public double getMaxWidth() {
		
		return 1275.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25300.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.35;
	}

	@Override
	public double getMinWidth() {
		
		return 960.0;
	}

	@Override
	public double getMinWeight() {
		
		return 3516.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
