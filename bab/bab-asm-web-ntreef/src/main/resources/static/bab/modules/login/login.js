new Vue({
	el: '#app'
	, ready: function() {
		
		$('.ui.form').form({
			fields : {
				username : {
					identifier : 'username',
					rules : [ {
						type : 'empty',
						prompt : 'Please enter your id'
					}]
				},
				password : {
					identifier : 'password',
					rules : [ {
						type : 'empty',
						prompt : 'Please enter your password'
					}]
				}
			}
		});

				
		$('.message .close').on('click', function() {
			$(this).closest('.message.loginInfo').transition('fade');
		});
	}
})