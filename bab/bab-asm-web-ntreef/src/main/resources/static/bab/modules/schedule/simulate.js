Date.prototype.yyyymmdd = function()
{
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();


    return yyyy + (mm[1] ? mm : '0'+mm[0]) + (dd[1] ? dd : '0'+dd[0]);
}

Date.prototype.yyyymmdd_ = function()
{
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();


    return yyyy +'-'+ (mm[1] ? mm : '0'+mm[0])+ '-' + (dd[1] ? dd : '0'+dd[0]);
}

Date.prototype.mmddHHMM = function()
{
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();
    var hh = this.getHours().toString();
    var minutes = this.getMinutes().toString();

    return yyyy + '-' + (mm[1] ? mm : '0'+mm[0])+ "-" + (dd[1] ? dd : '0'+dd[0])+ " " + hh + ":" + minutes;
}

function dateType(date) {
	console.log(date);
	var year = date.substr(0,4);
	var month = date.substr(4,2);
	var day = date.substr(6,2);
	
	return year + '-' + month + '-' + day;
}

new Vue({
	el: '#app',
	components: {
		'loader': LoaderComponent
	},
	data: {
		isProcessing: false,
		arrayData: [],
		summaryTable:"",
		svg:""
	},
	created: function(){
		
		$(function(){
			$('#date_timepicker_start').datetimepicker({
				format:'Y-m-d',
				onShow:function( ct ){
					this.setOptions({
						maxDate:$('#date_timepicker_end').val()?$('#date_timepicker_end').val():false
					})
				}, timepicker:false
			});
			$('#date_timepicker_end').datetimepicker({
				format:'Y-m-d',
				onShow:function( ct ){
					this.setOptions({
						minDate:$('#date_timepicker_start').val()?$('#date_timepicker_start').val():false
					})
				}, timepicker:false
			});
		});
		
		var taskArray = [
		 {
		 ORD_NO: "conceptualize",
		 PRC_CD1: "development",
		 SDT: 1466060510000, //year/month/day
		 EDT: 1466060525000,
		 details: "This actually didn't take any conceptualization"
		 },

		 {
		 ORD_NO: "sketch",
		 PRC_CD1: "development",
		 SDT: 1466060521200,
		 EDT: 1466060524000,
		 details: "No sketching either, really"
		 },

		 {
		 ORD_NO: "color profiles",
		 PRC_CD1: "development",
		 SDT: 1466060521300,
		 EDT: 1466060523000
		 },

		 {
		 ORD_NO: "HTML",
		 PRC_CD1: "coding",
		 SDT: 1466060521400,
		 EDT: 1466060522000,
		 details: "all three lines of it"
		 },

		 {
		 ORD_NO: "write the JS",
		 PRC_CD1: "coding",
		 SDT: 1466060521500,
		 EDT: 1466060521900
		 },

		 {
		 ORD_NO: "advertise",
		 PRC_CD1: "promotion",
		 SDT: 1466060521600,
		 EDT: 1466060522800,
		 details: "This counts, right?"
		 },

		 {
		 ORD_NO: "spam links",
		 PRC_CD1: "promotion",
		 SDT: 1466060521700,
		 EDT: 1466060522700
		 },
		 {
		 ORD_NO: "eat",
		 PRC_CD1: "celebration",
		 SDT: 1466060521800,
		 EDT: 1466060522600,
		 details: "All the things"
		 },

		 {
		 ORD_NO: "crying",
		 PRC_CD1: "celebration",
		 SDT: 1466060519000,
		 EDT: 1466060522500
		 }

		 ];

//		drowGraph(taskArray);
		
		var sdt = (new Date()).yyyymmdd();
		var edt = (new Date()).yyyymmdd();
		
		$('#date_timepicker_start').val((new Date()).yyyymmdd_());
		$('#date_timepicker_end').val((new Date()).yyyymmdd_());
		var self = this;
		console.log(apiURI);
		self.isProcessing = true;
		var status = "";
		var interval = window.setInterval(function() {
			// call API
			self.$http.get(apiURI.schedule.logdata+'schedule'+'/'+sdt+'/'+edt).
							then(function(response) {
				console.log("start Interval",response);
				console.log(_.isNull(response.data));
				if(!_.isNull(response.data)) {
					status = response.data.status;
					if (status == "FINISHED") {
						console.log("finished");

						var responseData = response.data.response;
						//self.arrayData = _.values(responseData);
						// console.log(this.arrayData);
						
						console.log(responseData);
						drowGraph(responseData);
						
						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
						
					} else if(status == "RUNNING"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				} else {
					console.log("null");
					self.isProcessing = false;
					window.clearInterval(interval);
				}
			});	
		}, 3000);
		

		
	},
	methods: {
		checkBtn: function(e) {
			var sdt = $('#date_timepicker_start').val().replace(/-/gi, "");
			var edt = $('#date_timepicker_end').val().replace(/-/gi, "");
			var self = this;
			console.log(apiURI);
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				self.$http.get(apiURI.schedule.logdata+'schedule'+'/'+sdt+'/'+edt).
								then(function(response) {
					console.log("start Interval",response);
					console.log(_.isNull(response.data));
					if(!_.isNull(response.data)) {
						status = response.data.status;
						if (status == "FINISHED") {
							console.log("finished");

							var responseData = response.data.response;
							//self.arrayData = _.values(responseData);
							// console.log(this.arrayData);
							
							console.log(responseData);
							$('.svg').empty();
							drowGraph(responseData);
							
							console.log("end Interval", interval);
							self.isProcessing = false;
							window.clearInterval(interval);
							
						} else if(status == "RUNNING"){
							console.log("wait");
						} else {
							console.log("error");
							self.isProcessing = false;
							window.clearInterval(interval);
						}
					} else {
						console.log("null");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
		}
	}
});

function drowGraph(data) {

	var taskArray = data;
	
	var textHeight = 18;
	var barHeight = 25; // inside rect height
	var gap = barHeight + 5; // inside rect space(top and bottom)
	var topPadding = 85;
	var sidePadding = 85; // 'label text' left space
	var w = 1500;
	
	var categories = new Array();

	for (var i = 0; i < taskArray.length; i++) {
		categories.push(taskArray[i].PRC_CD1);
	}

	var catsUnfiltered = categories; 	//for vert labels

	categories = checkUnique(categories);
	
	var h = categories.length * gap + topPadding * 2;
	
	$(".graph-simul").height(h + 20); // segment responsive

	var svg = d3.selectAll(".svg")
	//.selectAll("svg")
	.append("svg").attr("width", w).attr("height", h).attr("class", "svg");

	// graph resize
	//		  var aspect = w / h,
	//		    chart = d3.select('#graph');
	//		  d3.select(window).on("resize", function() {
	//		    var targetWidth = $("#container").width();
	//		    chart.attr("width", targetWidth);
	//		    chart.attr("height", targetWidth / aspect);
	//		  });

	
	var timeScale = d3.time.scale()
	.domain([ 
	          d3.min(taskArray, function(d) {
	        	  return new Date(d.SDT);}),
	          d3.max(taskArray, function(d) {
	        	  return new Date(d.EDT);})])
	.range([ 0, w - 150 ]);

	var yAxisSize = function(d, i) {
		console.log(i);
		return i * theGap + theTopPad - 2;
	};


	makeGant(taskArray, w, h);

	/*var title = svg.append("text").text("공정 시뮬레이트").attr("x", w / 2).attr("y",
			25).attr("text-anchor", "middle").attr("font-size", 20).attr(
			"font-weight", "bold").attr("fill", "#000");*/

	function makeGant(tasks, pageWidth, pageHeight) {

		var colorScale = d3.scale.linear().domain([ 0, categories.length ])
				.range([ "#74BBF2", "#009FFC" ]).interpolate(d3.interpolateHcl);

		makeGrid(sidePadding, topPadding, gap, pageWidth, pageHeight);
		drawRects(tasks, gap, topPadding, sidePadding, barHeight, colorScale,
				pageWidth, pageHeight);
		vertLabels(gap, topPadding, sidePadding, barHeight, colorScale);
	}

	function drawRects(theArray, theGap, theTopPad, theSidePad, theBarHeight,
			theColorScale, w, h) {

		var bigRects = svg.append("g").selectAll("rect").data(categories).enter()
				.append("rect").attr("x", 0)
				.attr("y", function(d, i) {
					//console.log(i);
					return i * theGap + theTopPad - 2;
				}).attr("width", function(d) {
					return w - theSidePad / 2;
				}).attr("height", theGap).attr("stroke", "none")
				.attr("fill", 
						//"white"
				function(d, i) {
					if(i%2 == 0) {
						return d3.rgb("#B5B5B5");
					} else 
						return d3.rgb("white");
				}
/*						function(d) {					
							for (var i = 0; i < categories.length; i++) {
								if (d.PRC_CD1 == categories[i]) {
									return d3.rgb(theColorScale(i));
								}
							}
						}*/
				)
				.attr("opacity", 0.2);

		var rectangles = svg.append('g').selectAll("rect").data(theArray)
				.enter();

		var innerRects = rectangles
				.append("rect")
				.attr("rx", 3)
				.attr("ry", 3)
				.attr("x", function(d) {
					if (!_.isUndefined(d.SDT)) {
						return timeScale(new Date(d.SDT)) + theSidePad;
					} else {
						return 0;
					}
					
				})
				.attr("y", function(d) {
					for (var i = 0; i < categories.length; i++) {
						if (d.PRC_CD1 == categories[i]) {
							return i * theGap + theTopPad;
						}
					}
				})
				.attr("width", function(d) {
					if (!_.isUndefined(d.SDT)) {
						return (timeScale(new Date(d.EDT)) - timeScale(new Date(d.SDT))+1);
					} else {
						return 0;
					}
				}).attr("height", theBarHeight).attr("stroke", "none")
				.attr("fill", 
						function(d) {
							for (var i = 0; i < categories.length; i++) {
								if (d.PRC_CD1 == categories[i]) {
									return d3.rgb("#67CEFD");
								}
							}
						}
				)

		// inside rect text
		var rectText = rectangles
				.append("text")
				.text(function(d) {
					return d.ORD_NO;
				})
				.attr(
						"x",
						function(d) {
							if (!_.isUndefined(d.SDT)) {
								return (timeScale(new Date(d.EDT)) - timeScale(new Date(d.SDT)))
								/ 2
								+ timeScale(new Date(d.SDT))
								+ theSidePad;
							} else {
								return 0;
							}
						})
				.attr("y", function(d) {
					for (var i = 0; i < categories.length; i++) {
						if (d.PRC_CD1 == categories[i]) {
							return i * theGap + textHeight + theTopPad;
						}
					}
				})
				.attr("font-size", 11).attr("text-anchor", "middle").attr(
						"text-height", theBarHeight).attr("fill", "#000");

		rectText.on(
				'mouseover',
				function(e) {
					// console.log(this.x.animVal.getItem(this));
					var tag = "주문번호: " 
						+ d3.select(this).data()[0].ORD_NO
						+ "<br/>" + "용도: "
						+ d3.select(this).data()[0].PURPOSE
						+ "<br/>" + "두께: "
						+ d3.select(this).data()[0].THK
						+ "<br/>" + "폭: "
						+ d3.select(this).data()[0].WDT
						+ "<br/>" + "납기일: "
						+ dateType(d3.select(this).data()[0].DELIVERY_DT)
						+ "<br/>" + "시작시간: "
						+ new Date(d3.select(this).data()[0].SDT).mmddHHMM()
						+ "<br/>" + "종료시간: "
						+ new Date(d3.select(this).data()[0].EDT).mmddHHMM();
					var output = document.getElementById("tag");

					var x = this.x.animVal.getItem(this) + "px";
					var y = this.y.animVal.getItem(this) + 50 + "px";

					output.innerHTML = tag;
					output.style.top = y;
					output.style.left = x;
					output.style.display = "block";
				}).on('mouseout', function() {
			var output = document.getElementById("tag");
			output.style.display = "none";
		});

		innerRects
				.on(
						'mouseover',
						function(e) {
							//console.log(this);
							var tag = "주문번호: " 
										+ d3.select(this).data()[0].ORD_NO
										+ "<br/>" + "용도: "
										+ d3.select(this).data()[0].PURPOSE
										+ "<br/>" + "두께: "
										+ d3.select(this).data()[0].THK
										+ "<br/>" + "폭: "
										+ d3.select(this).data()[0].WDT
										+ "<br/>" + "납기일: "
										+ dateType(d3.select(this).data()[0].DELIVERY_DT)
										+ "<br/>" + "시작시간: "
										+ new Date(d3.select(this).data()[0].SDT).mmddHHMM()
										+ "<br/>" + "종료시간: "
										+ new Date(d3.select(this).data()[0].EDT).mmddHHMM();
							var output = document.getElementById("tag");

							var x = (this.x.animVal.value + this.width.animVal.value / 2)
									+ "px";
							var y = this.y.animVal.value + 35 + "px";

							output.innerHTML = tag;
							output.style.top = y;
							output.style.left = x;
							output.style.display = "block";
						}).on('mouseout', function() {
					var output = document.getElementById("tag");
					output.style.display = "none";

				});
	}

	function makeGrid(theSidePad, theTopPad, theGap, w, h) {
		var xAxis = d3.svg
		.axis()
		.scale(timeScale)
		.orient('bottom')
		.ticks(d3.time.hour, 1)
		.tickSize(-h + theTopPad + 20, 0, 0)
		.tickFormat(d3.time.format('%m/%d %H:%M'));
		
		var xTopAxis = d3.svg
		.axis()
		.scale(timeScale)
		.orient('bottom')
		.ticks(d3.time.hour, 1)
		.tickSize(0, 0, 0)
		.tickFormat(d3.time.format('%m/%d %H:%M'));
		
		var yAxis = d3.svg
		.axis()
		.scale(timeScale)
		.orient('right')
		.ticks(17)
		.tickSize(h + theSidePad + 20, 0, 0)
		.tickFormat("");
		
		var bottomGrid = svg.append('g')
		.attr('class', 'grid')
		.attr('transform', 'translate(' + theSidePad + ', ' + (h - 50) + ')')
		.call(xAxis).selectAll("text")
		.style("text-anchor", "middle")
		.attr("fill","#000")
		.attr("stroke", "none")
		.attr("font-size", 13) // xAxis font size( ex.28 Jan)
		.attr("dy", "1em");
		
		var topGrid = svg.append('g')
		.attr('class', 'grid')
		.attr('transform', 'translate('+theSidePad+','+barHeight+')')
		.call(xTopAxis).selectAll("text")
		.style("text-anchor", "middle")
		.attr("fill","#000")
		.attr("stroke", "none")
		.attr("font-size", 13) // xAxis font size( ex.28 Jan)
		.attr("dy", "1em");
		
/*		var rightGrid = svg.append('g')
		.attr('class', 'grid')
		.attr('transform', 'translate(' + theTopPad + ')')
		.call(yAxis).selectAll("text")
		.style("text-anchor", "middle")
		.attr("fill","#000")
		.attr("stroke", "none")
		.attr("font-size", 12) // xAxis font size( ex.28 Jan)
		.attr("dy", "1em");*/
	}

	function vertLabels(theGap, theTopPad, theSidePad, theBarHeight,
			theColorScale) {
		var numOccurances = new Array();
		var prevGap = 0;

		for (var i = 0; i < categories.length; i++) {
			numOccurances[i] = [ categories[i],getCount(categories[i], catsUnfiltered) ];
		}
		var axisText = svg.append("g") //without doing this, impossible to put grid lines behind text
		.selectAll("text").data(numOccurances).enter().append("text").text(
				function(d) {
					return d[0];
				}).attr("x", 10) // 'label text' direction
		.attr("y", function(d) {
			console.log(d);
			for (var i = 0; i < categories.length; i++) {				
				if (d[0] == categories[i]) {
					return i * theGap + theTopPad + textHeight;
				}
			}
/*			if (i > 0) {
				for (var j = 0; j < i; j++) {
					prevGap += numOccurances[i - 1][1];
					// console.log(prevGap);
					return d[1] * theGap / 2 + prevGap * theGap + theTopPad;
				}
			} else {
				return d[1] * theGap / 2 + theTopPad;
			}*/
		}).attr("font-size", 13).attr("font-weight", "bold").attr("text-anchor", "start").attr(
				"text-height", 14).attr("fill", function(d) {
			for (var i = 0; i < categories.length; i++) {
				if (d[0] == categories[i]) {
					//  console.log("true!");
					return d3.rgb("#000").darker();
				}
			}
		});

	}

	//from this stackexchange question: http://stackoverflow.com/questions/1890203/unique-for-arrays-in-javascript
	function checkUnique(arr) {
		var hash = {}, result = [];
		for (var i = 0, l = arr.length; i < l; ++i) {
			if (!hash.hasOwnProperty(arr[i])) { //it works with objects! in FF, at least
				hash[arr[i]] = true;
				result.push(arr[i]);
			}
		}
		return result;
	}

	//from this stackexchange question: http://stackoverflow.com/questions/14227981/count-how-many-strings-in-an-array-have-duplicates-in-the-same-array
	function getCounts(arr) {
		var i = arr.length, // var to loop over
		obj = {}; // obj to store results
		while (i)
			obj[arr[--i]] = (obj[arr[i]] || 0) + 1; // count occurrences
		return obj;
	}

	// get specific from everything
	function getCount(word, arr) {
		return getCounts(arr)[word] || 0;
	}
	
}