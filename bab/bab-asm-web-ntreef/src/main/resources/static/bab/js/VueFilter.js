/* @description list of filter for vuejs, used in template, most of the function only wrapper of function in BabHelpe
 *
 * @dependency BabHelper
 * @author http://twitter.com/superpikar
 */
Vue.filter('formatDate', function (milliseconds, format) {
  return BabHelper.formatDate(milliseconds, format);
});

Vue.filter('formatDateTime', function (milliseconds, format) {
  return BabHelper.formatDateTime(milliseconds, format);
});

Vue.filter('formatNumber', function (number, format) {
  return BabHelper.formatNumber(number, format);
});

Vue.filter('absoluteNumber', function (number) {
	return Math.abs(number);
});

Vue.filter('formatDuration', function (number) {
	return BabHelper.printTimeDuration(number, false);
});

Vue.filter('formatDurationPretty', function (number) {
	return BabHelper.printTimeDuration(number, true);
});
