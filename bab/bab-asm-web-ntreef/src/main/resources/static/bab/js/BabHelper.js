/*
 * BabHelper
 * @description contain list of helper function used by another javscript file
 *
 * @credit http://codereview.stackexchange.com/questions/21105/pattern-for-creating-a-globally-accessible-custom-plugin
 * @author http://twitter.com/superpikar
 */
(function(bab){
  /* @description function to get the highest duration unit from timestamp
   *
   * @params second unixtimestamp
   * @dependency momentjs
   */
	bab.getDurationUnit = function(second){
		var theDuration = moment.duration(second);
		if(theDuration.years()!=0){
			return "year";
		}
		else if(theDuration.months()!=0){
			return "month";
		}
		else if(theDuration.days()!=0){
			return "day";
		}
		else if(theDuration.hours()!=0){
			return "hour";
		}
		else if(theDuration.minutes()!=0){
			return "minute";
		}
		else{
			return "second";
		}
	}

  /* @description function to get the duration unit from timestamp
   *
   * @params second unixtimestamp
   * @params unit String "year/month/day/hour/minute/second"
   * @dependency momentjs
   */
	bab.getDurationUnitValue = function(second, unit){
		var theDuration = moment.duration(second);
		if(unit=="year"){
			number = Math.round(theDuration.asYears());
		}
		else if(unit=="month"){
			number = Math.round(theDuration.asMonths());
		}
		else if(unit=="day"){
			number = Math.round(theDuration.asDays());
		}
		else if(unit=="hour"){
			number = Math.round(theDuration.asHours());
		}
		else if(unit=="minute"){
			number = Math.round(theDuration.asMinutes());
		}
		else{
			number = Math.round(theDuration.asSeconds());
		}
		return number == 0 ? '<1':String(number);
	}

  /* @description function to get the duration in humanize format. i.e, 1 year 3 month
   *
   * @params seconds unixtimestamp
   * @params isPretty Boolean
   * @dependency momentjs
   */
	bab.printTimeDuration = function(seconds, isPretty) {
		var year = isPretty? 'Y':'years';
		var month = isPretty? 'M':'months';
		var day = isPretty? 'D':'days';
		var hour = isPretty? 'h':'hours';
		var minute = isPretty? 'm':'minute';
		var second = isPretty? 's':'seconds';

		var objDate = moment.duration(seconds)._data;
		// console.log(objDate);
		var result = '';
		if (objDate.years != 0) {
			result += ' ' + objDate.years + ' ' + year;
			if (objDate.months != 0) {
				result += ' ' + objDate.months + ' ' + month;
			} else if (objDate.days != 0) {
				result += ' ' + objDate.days + ' ' + day;
			} else if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' ' + hour;
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' ' + minute;
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' ' + second;
			}
		} else if (objDate.months != 0) {
			result += ' ' + objDate.months + ' ' + month;
			if (objDate.days != 0) {
				result += ' ' + objDate.days + ' ' + day;
			} else if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' ' + hour;
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' ' + minute;
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' ' + second;
			}
		} else if (objDate.days != 0) {
			result += ' ' + objDate.days + ' ' + day;
			if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' ' + hour;
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' ' + minute;
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' ' + second;
			}
		} else if (objDate.hours != 0) {
			result += ' ' + objDate.hours + ' ' + hour;
			if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' ' + minute;
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' ' + second;
			}
		} else if (objDate.minutes != 0) {
			result += ' ' + objDate.minutes + ' ' + minute;
			if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' ' + second;
			}
		} else if (objDate.seconds != 0) {
			result += ' ' + objDate.seconds + ' ' + second;
		} else if (objDate.seconds == 0) {
			result = '0 ' + second;
		}

		return result;
	};

  /* @description function to print number in decimal format
   *
   * @params number Integer/Float
   * @params decimalPoint Integer number of decimal point
   * @dependency jquery
   */
	bab.printNumber = function(number, decimalPoint) {
		if (jQuery.isNumeric(number)) {
			if (number % 1 != 0) { // if decimal
				if (typeof decimalPoint == 'undefined') {
					decimalPoint = 2
				}
				return Number(number.toFixed(decimalPoint)).toLocaleString();
			} else {
				return Number(number).toLocaleString();
			}
		} else {
			return number;
		}
	};

  /* @description function to format the number
   *
   * @params number Integer/Float
   * @params format String format of output, see numeraljs docs
   * @dependency numeraljs
   */
	bab.formatNumber = function(number, format){
	  if(!_.isNumber(number) || _.isNaN(number)){
		  number = 0;
	  }
	  if(!_.isUndefined(format)){
	    return numeral(parseFloat(number)).format(format);
	  }
	  else{
	    return numeral(parseFloat(number)).format('0,0');
	  }
	}

  /* @description function to format the date, by default the format is 'YYYY-MM-DD'
   *
   * @params milliseconds unixtimestamp
   * @params format String format of output, see momentjs docs
   * @dependency momentjs
   */
	bab.formatDate = function(milliseconds, format){
		if(!_.isUndefined(format)){
			return moment(parseInt(milliseconds)).format(format);
		}
		else{
			return moment(parseInt(milliseconds)).format('YYYY-MM-DD');
		}
	}

  /* @description function to format the date, by default the format is 'YYYY-MM-DD'
   *
   * @params milliseconds unixtimestamp
   * @params format String format of output, see momentjs docs
   * @dependency momentjs
   */
	bab.formatDateTime = function(milliseconds, format){
	  if(!_.isUndefined(format)){
	    return moment(parseInt(milliseconds)).format(format);
	  }
	  else{
	    return moment(parseInt(milliseconds)).format('YYYY-MM-DD HH:mm:ss');
	  }
	}

}(this.BabHelper = this.BabHelper || {}));
