/* @description list of filter for handlebar, used in template, most of the function only wrapper of function in BabHelpe
 *
 * @dependency BabHelper
 * @author http://twitter.com/superpikar
 */
Handlebars.registerHelper('formatDate', function (milliseconds, format) {
	return BabHelper.formatDate(milliseconds, format);
});

Handlebars.registerHelper('formatDateTime', function (milliseconds, format) {
	return BabHelper.formatDateTime(milliseconds, format);
});

Handlebars.registerHelper('formatNumber', function (number, format) {
	return BabHelper.formatNumber(number, format);
});

Handlebars.registerHelper('formatDuration', function (number) {
	return BabHelper.printTimeDuration(number, false);
});

Handlebars.registerHelper('formatDurationPretty', function (number) {
	return BabHelper.printTimeDuration(number, true);
});
