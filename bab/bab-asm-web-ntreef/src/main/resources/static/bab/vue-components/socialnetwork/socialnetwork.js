/**
 * 
 */
Vue.component('socialnetwork', {
  template: '#socialnetwork-template',
  props: ['apiURI','conf'],
  components: {
	  	'loader': LoaderComponent
  },	
  data: function(){
	  return {
		  isProcessing: false
	  };
  },
  ready: function(){
    // credit : http://codepen.io/darrengriffith/pen/KpKxqR?editors=0010
	  	
	  	// loader position middle
	  	$('.social-graph').css("min-height", $(window).height()/2);
	  	
	  	var self = this;
		self.isProcessing = true;
		var status = "";
		
		var intervalDuration = 3000;
		var snURI = apiURI.analysis.socialnetwork + jsonData.workspaceId 
		+ '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
		var jobQueueURI = apiURI.job.queue;
		
		console.log(apiURI);
		console.log(snURI);
		
		self.isProcessing = true;
		this.$http.get(snURI).then(function(response) {
			var snRes = response;
			var snResStatus = snRes.data.status;
			if (snResStatus == 'FINISHED') {
				self.isProcessing = false;
				this.drawSN(snRes.data.response);
			} else if (snResStatus == 'FAILED') {
				self.isProcessing = false;
				console.log('FAILED')
			} else if (snResStatus == 'RUNNING' || snResStatus == 'QUEUED') {
				var snQueueChecker = window.setInterval(_.bind(function() {
					this.$http.get(jobQueueURI + snRes.data.request.jobId)
					.then(function(response) {
						var snQueueRes = response;
						var snQueueResStatus = snQueueRes.data.jobQueueStatus;
						
						if (snQueueResStatus == 'RUNNING') {
							
						} else if (snQueueResStatus == 'FAILED') {
							window.clearInterval(snQueueChecker);
							this.isProcessing = false;
						} else if (snQueueResStatus == 'FINISHED') {
							window.clearInterval(snQueueChecker);
							
							this.$http.get(snURI).then(function(response) {
								var snRes = response;
								var snResStatus = snRes.data.status;

								this.isProcessing = false;
								if (snResStatus == 'FINISHED') {
									this.drawSN(snRes.data.response);
								} else {
									console.log('FAILED');
								}
							});
						}
					});
				}, this), intervalDuration);
			}
		});
		
/*	  this.$http.get(this.apiURI.socialnetwork).then(function (response) {
	      var theResponse = response.data.response;
	      console.log(response.data.response);
	      var menuHtml = "";
	      $.each(theResponse.nodes, function(key, value){
	    	    menuHtml += '<div class="item" data-value="'+key+'">'+key+'</div>'
	    	});
	      $('#machin-dropdown1 .menu').html(menuHtml);
	      $('#machin-dropdown2 .menu').html(menuHtml);
	      $('#machin-dropdown3 .menu').html(menuHtml);
	      $('#machin-dropdown4 .menu').html(menuHtml);
	      $('#machin-dropdown5 .menu').html(menuHtml);
	      
	      if(!_.isNull(theResponse)){
	    	  this.social = new SocialClass(theResponse);
	    	  
	      }
	      else{
	    	  alert('response is NULL!');
	      }
	  }, function (response) {	
	      // error callback
		  alert('error get process model!');
	  });*/
  },
  methods: {
	  drawSN : function(data) {

//			var theResponse = response.data.response;
			var menuHtml = "";
			$.each(data.nodes, function(key, value){menuHtml += '<div class="item" data-value="'+key+'">'+key+'</div>'});
			$('#machin-dropdown1 .menu').html(menuHtml);
			$('#machin-dropdown2 .menu').html(menuHtml);
			$('#machin-dropdown3 .menu').html(menuHtml);
			$('#machin-dropdown4 .menu').html(menuHtml);
			$('#machin-dropdown5 .menu').html(menuHtml);

			this.social = new SocialClass(data);
	  },
	  subconBtn: function(e) {
		    var confData = this.conf;
		  	if($('#machin-dropdown1').dropdown('get item')) {
		  		console.log("dropbox data not null");
		  		console.log($('#machin-dropdown1').dropdown('get value'));
		  		var str = $('#machin-dropdown1').dropdown('get value');
		  		confData.states = str;
		  	}else {
		  		console.log("dropbox null");
		  		confData.states = [];
		  	}
	  		confData.method.algorithm = "subcontracting";
	  		confData.method.casuality = $('#subCauCheckbox').is(':checked').toString();
	  		confData.method.multipleTransfer = $('#subMultiCheckbox').is(':checked').toString();
	  		confData.method.directSubcontract.beta = $('#subBeta').val();
	  		confData.method.directSubcontract.depth = $('#subDepth').val();
	  		
			
			$('.ui.modal.modal1').modal('hide');
		  	var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				console.log("confData", confData);
				self.$http.post('/bab/api/v1_0/analysis/socialnetwork'+'/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
					demension: confData.demension,
					states: confData.states,
					"method.algorithm": confData.method.algorithm,
					"method.casuality": confData.method.casuality,
					"method.directSubcontract.beta": confData.method.directSubcontract.beta,
					"method.directSubcontract.depth": confData.method.directSubcontract.depth,
					"method.workingTogetherBy": confData.method.workingTogetherBy,
					"method.multipleTransfer": confData.method.multipleTransfer,
					'method.distance': confData.method.distance
				
				}).then(function(response){
					console.log("start Interval",response);
					status = response.data.status;
					console.log(status);
					if (status == "FINISHED") {
						console.log("finished");

						console.log(response.data.response);
						var theResponse = response.data.response;
						$('#graph_color').empty();
						$('#graph').empty();
						this.social = new SocialClass(theResponse);
						$('#machin-dropdown1').dropdown('clear');

						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
		},
		handoverBtn: function(e) {
			var confData = this.conf;
		  	if($('#machin-dropdown2').dropdown('get item')) {
		  		console.log("dropbox data not null");
		  		console.log($('#machin-dropdown2').dropdown('get value'));
		  		var str = $('#machin-dropdown2').dropdown('get value');
		  		confData.states = str;
		  	}else {
		  		console.log("dropbox null");
		  		confData.states = [];
		  	}
			confData.method.algorithm = "handoverOfWork";
	  		confData.method.casuality = $('#handCauCheckbox').is(':checked');
	  		confData.method.multipleTransfer = $('#handMultiCheckbox').is(':checked');
	  		//confData.method.casuality = $('#subDirectCheckbox').is(':checked');
	  		confData.method.directSubcontract.beta = $('#handBeta').val();
	  		confData.method.directSubcontract.depth = $('#handDepth').val();
			
			console.log("confData", confData);
			
			$('.ui.modal.modal2').modal('hide');
		  	var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				console.log("confData", confData);
				self.$http.post('/bab/api/v1_0/analysis/socialnetwork'+'/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
					demension: confData.demension,
					states: confData.states,
					"method.algorithm": confData.method.algorithm,
					"method.casuality": confData.method.casuality,
					"method.directSubcontract.beta": confData.method.directSubcontract.beta,
					"method.directSubcontract.depth": confData.method.directSubcontract.depth,
					"method.workingTogetherBy": confData.method.workingTogetherBy,
					"method.multipleTransfer": confData.method.multipleTransfer,
					'method.distance': confData.method.distance
				
				}).then(function(response){
					console.log("start Interval",response);
					status = response.data.status;
					console.log(status);
					if (status == "FINISHED") {
						console.log("finished");

						console.log(response.data.response);
						var theResponse = response.data.response;
						
						$('#graph_color').empty();
						$('#graph').empty();
						var theResponse = response.data.response;
						this.social = new SocialClass(theResponse);
						$('#machin-dropdown2').dropdown('clear');

						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
			
		},
		workingBtn: function(e) {
			  var confData = this.conf;
			confData.method.algorithm = "workingTogether";
		  	if($('#machin-dropdown3').dropdown('get item')) {
		  		console.log("dropbox data not null");
		  		console.log($('#machin-dropdown3').dropdown('get value'));
		  		var str = $('#machin-dropdown3').dropdown('get value');
		  		confData.states = str;
		  	}else {
		  		console.log("dropbox null");
		  		confData.states = [];
		  	}
			
			confData.method.workingTogetherBy = $('input:radio[name="togetherRadio"]:checked').val();
			
			console.log("confData", confData);
			
			$('.ui.modal.modal3').modal('hide');
		  	var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				console.log("confData", confData);
				self.$http.post('/bab/api/v1_0/analysis/socialnetwork'+'/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
					demension: confData.demension,
					states: confData.states,
					"method.algorithm": confData.method.algorithm,
					"method.casuality": confData.method.casuality,
					"method.directSubcontract.beta": confData.method.directSubcontract.beta,
					"method.directSubcontract.depth": confData.method.directSubcontract.depth,
					"method.multipleTransfer": confData.method.multipleTransfer
				
				}).then(function(response){
					console.log("start Interval",response);
					status = response.data.status;
					console.log(status);
					if (status == "FINISHED") {
						console.log("finished");

						console.log(response.data.response);
						var theResponse = response.data.response;
						
						$('#graph_color').empty();
						$('#graph').empty();
						this.social = new SocialClass(theResponse);
						$('#machin-dropdown3').dropdown('clear');

						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
			
		},
		similarBtn: function(e) {
			var confData = this.conf;
			confData.method.algorithm = "SimilarTask";
			confData.method.distance = $('input:radio[name="similarRadio"]:checked').val();
		  	if($('#machin-dropdown4').dropdown('get item')) {
		  		console.log("dropbox data not null");
		  		console.log($('#machin-dropdown4').dropdown('get value'));
		  		var str = $('#machin-dropdown4').dropdown('get value');
		  		confData.states = str;
		  	}else {
		  		console.log("dropbox null");
		  		confData.states = [];
		  	}
			
			console.log("confData", confData);
			
			$('.ui.modal.modal4').modal('hide');
		  	var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				console.log("confData", confData);
				self.$http.post('/bab/api/v1_0/analysis/socialnetwork'+'/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
					demension: confData.demension,
					states: confData.states,
					"method.algorithm": confData.method.algorithm,
					"method.casuality": confData.method.casuality,
					"method.directSubcontract.beta": confData.method.directSubcontract.beta,
					"method.directSubcontract.depth": confData.method.directSubcontract.depth,
					"method.multipleTransfer": confData.method.multipleTransfer
				
				}).then(function(response){
					console.log("start Interval",response);
					status = response.data.status;
					console.log(status);
					if (status == "FINISHED") {
						console.log("finished");

						console.log(response.data.response);
						var theResponse = response.data.response;
						
						$('#graph_color').empty();
						$('#graph').empty();
						this.social = new SocialClass(theResponse);
						$('#machin-dropdown4').dropdown('clear');

						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
			
		},
		reassignmentBtn: function(e) {
			var confData = this.conf;
			confData.method.algorithm = "Reassignment";
	  		confData.method.multipleTransfer = $('#reassignCheckbox').is(':checked');
		  	if($('#machin-dropdown5').dropdown('get item')) {
		  		console.log("dropbox data not null");
		  		console.log($('#machin-dropdown5').dropdown('get value'));
		  		var str = $('#machin-dropdown5').dropdown('get value');
		  		confData.states = str;
		  	}else {
		  		console.log("dropbox null");
		  		confData.states = [];
		  	}
			console.log("confData", confData);
			
			$('.ui.modal.modal5').modal('hide');
		  	var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				console.log("confData", confData);
				self.$http.post('/bab/api/v1_0/analysis/socialnetwork'+'/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
					demension: confData.demension,
					states: confData.states,
					"method.algorithm": confData.method.algorithm,
					"method.casuality": confData.method.casuality,
					"method.directSubcontract.beta": confData.method.directSubcontract.beta,
					"method.directSubcontract.depth": confData.method.directSubcontract.depth,
					"method.multipleTransfer": confData.method.multipleTransfer
				
				}).then(function(response){
					console.log("start Interval",response);
					status = response.data.status;
					console.log(status);
					if (status == "FINISHED") {
						console.log("finished");

						console.log(response.data.response);
						var theResponse = response.data.response;
						
						$('#graph_color').empty();
						$('#graph').empty();
						this.social = new SocialClass(theResponse);
						$('#machin-dropdown5').dropdown('clear');

						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}
				});	
			}, 3000);
		},
		subcauDirectBtn: function(e) {
			
			$('#subBeta').val();
	  		$('#subDepth').val();
		},
		handDirectBtn: function(e) {
			
			$('#subBeta').val();
	  		$('#subDepth').val();
		}
  }
});



SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

function initializeNodes(nodes){
  return _.map(nodes, function(val, key){
    val.id = _.kebabCase(key);
    val.label = key;
    val.target = [];
    return val;
  });
}

function initializeArcs(arcs, nodes){
  return _.map(arcs, function(val, key){
    val.id = _.kebabCase(val.source)+'-'+_.kebabCase(val.target);
    val.label = key;
    val.source = _.findWhere(nodes, {id: _.kebabCase(val.source )});
    val.target = _.findWhere(nodes, {id: _.kebabCase(val.target )});
    val.source.target.push(val.target.id);
    val.target.target.push(val.source.id);
    return val;
  });
}

function initializeAttributes(obj){
  var attributes = [];
  // credit : http://stackoverflow.com/questions/8312459/iterate-through-object-properties
  for(var property in obj){
    if(obj.hasOwnProperty(property)){
      if(_.isObject(obj[property]) && property != 'target' && property != 'source'){
        for(var prop in obj[property]){
          if(obj[property].hasOwnProperty(prop)){
            attributes.push(property+'.'+prop);
          }
        }
      }
      else if(property != 'target' || property != 'id' || property != 'source'){
        attributes.push(property);
      }
    }
  }
  return attributes;
}