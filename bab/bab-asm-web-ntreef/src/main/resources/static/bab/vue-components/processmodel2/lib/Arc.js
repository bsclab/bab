/* @description class of Arc's process model, all of arc attribute are set when initiate the Arc
 *
 * @example
 *    var arc = new Arc({
              type: 'heuristic',
              source: 'Node A',
              target: 'Node B',
              svgId: 'arc1',
              duration: { },
              frequency: { }
            })
 *
 * @params options Object
 * @dependency lodash, d3
 * @author http://twitter.com/superpikar
 */

Arc = (function(){
  function Arc(options){
	_.extend(this, options);
    this.type = _.isUndefined(options.type)?'heuristic': options.type;
    this.isLogReplay = _.isUndefined(options.isLogReplay)? false: options.isLogReplay;
    this.meanNonSuccessive = _.isUndefined(options.type)?'timegap': options.meanNonSuccessive;
    this.source = options.source;
    this.sourceId = _.snakeCase(options.source);
    this.target = options.target;
    this.targetId = _.snakeCase(options.target);
//    this.sourceId = this.source.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
//    this.targetId = this.target.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
    this.id = this.sourceId+'-'+this.targetId;
    this.svgId = options.svgId;
    this.duration = options.duration;
    this.frquency = options.frequency;
    this.difference = _.isUndefined(options.difference)? false: options.difference;		// only true and false
  }

  Arc.prototype = {
    setPath: function(){
    	this.path = d3.select('#'+this.svgId+' path').node();
    }
  };

  return Arc;
})();
