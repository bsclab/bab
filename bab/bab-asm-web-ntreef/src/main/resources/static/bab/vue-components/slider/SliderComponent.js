/*
 * @description this is slider component, a wrapper of noUiSlider for vuejs
 *
 * @example : <slider :slider-min="0" :slider-max="1000" :slider-value="100" :slider-step="10" :slider-show-placeholder="true"></slider>
 *
 * @props sliderId String id of slider
 * @props sliderMin Integer minimum value of the slider
 * @props sliderMax Integer maximum value of the slider
 * @props sliderStep Integer step value of the slider
 * @props sliderValue Integer initial value of the slider
 * @props sliderConnect Boolean whether slider need to be connected or not
 * @props sliderPlaceholder Integer value of the placeholder in the textbox
 * @props sliderShowPlaceholder Boolean whether show plaeholder textbox or not
 * @props sliderOrientation String slider orientation "vertical/horizontal"
 * @dependency Semantic-UI CSS, vuejs, lodash, noUiSlider
 * @author http://twitter.com/superpikar
 * */
var SliderComponent = Vue.extend({
  props: {
    sliderId: { default: 'slider-'+this._uid },
    sliderMin: { default: 0 },
    sliderMax: { default: 100 },
    sliderStep: { default: 1 },
    sliderValue: {
        default: 1,
        type: Number,
        coerce: function (val) {
            var number = parseFloat(val);
            if(_.isNaN(number)){
                number = this.sliderMin;
            }
            return number // cast the value to string
        }
    },
    sliderConnect: {default:false},
    sliderPlaceholder: {default:1},
    sliderShowPlaceholder: {default: true},
    sliderOrientation: {default:'horizontal'},
  },
  template:
    '<div style="margin-bottom:45px;">'+
    '<input v-if="sliderShowPlaceholder" style="margin-bottom:15px;" type="text" v-bind:placeholder="sliderPlaceholder" v-model="sliderValue" v-on:keyup.enter="setSliderValue" />'+
    '<div v-bind:id="sliderId"></div>'+
    '</div>'
  ,
  ready: function(){
      var self = this;
//      console.log(this);
      this.theslider = document.getElementById(this.sliderId);

      noUiSlider.create(this.theslider , {
          start: this.sliderValue, // Handle start position
          step: this.sliderStep, // Slider moves in increments of '10'
          margin: 20, // Handles must be more than '20' apart
          connect: this.sliderConnect, // Display a colored bar between the handles
          orientation: this.sliderOrientation, // Orient the slider vertically
          range: { // Slider can select '0' to '100'
              'min': this.sliderMin,
              'max': this.sliderMax
          },
          pips: { // Show a scale with the slider
            mode: 'positions',
            values: [0,100],
            density: 20,
            stepped: true
          }
      });
      this.theslider.noUiSlider.on('update', function( values, handle ) {
        self.sliderValue = values[handle];
    });
  },
  methods: {
      setSliderValue: function(value){
          if(_.isUndefined(value)){
              this.theslider.noUiSlider.set(this.sliderValue);
          }
          else{
              this.theslider.noUiSlider.set(value);
          }
      }
  }
})
