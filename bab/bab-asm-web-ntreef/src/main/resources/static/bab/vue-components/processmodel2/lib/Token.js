Token = (function(){
  
  function Token(options){
    this.svgTokens = undefined;
    this.eld3 = undefined;
    this.eltokend3 = undefined;
    this.source = options.source;
    this.target = options.target;    
    this.id = 'token'+options.id;
    this.caseId = options.caseId;
    this.start = options.start;
    this.complete = options.complete;
    this.startOriginal = options.startOriginal;
    this.completeOriginal = options.completeOriginal;
    this.path = options.path;
    this.show = true;
    this.state = 'initial';
    this.tokensGroup = options.tokensGroup || [];
    this.tokensGroupLength = this.tokensGroup.length==0? 1: this.tokensGroup.length;
    this.svgTokens = options.svgTokens;
    this.size = 5 + Math.round(this.tokensGroup.length/2);
    this.tokenTemplate = options.tokenTemplate;
    this.uiData = options.uiData;
    this.qtip = undefined;
    
//    this.duration = this.complete - this.start;
  }

  Token.prototype = {
    removeToken: function(){
    	this.qtip.destroy();
    	this.eltokend3.remove();
    	this.eld3.remove();
    },
    createToken: function(){
    	this.eld3 = this.svgTokens.append('g')
    					.attr('class', 'walker hidden caseId_'+this.caseId)
    					.attr('id', this.id)
    					.attr('transform', 'translate(0,0)');
    	this.eld3.append('text')
    				.attr('class', 'label')
    				.attr('transform', 'translate('+this.size+','+this.size+')')
    				.text(this.caseId);
    	this.eltokend3 = this.eld3.append('circle')
    				.attr('transform', 'translate(0,0)')
    				.attr('cx', 0)
    				.attr('cy', 0)
    				.attr('r', this.size);
    	
    	this.qtip = $('g#'+this.id).qtip({
  	      overwrite: true,
  	      style: {
  	        classes: 'qtip-light qtip-shadow'
  	      },
  	      position: {
  	        target: 'mouse',
  	        adjust: {
  	          mouse: true  // Can be omitted (e.g. default behaviour)
  	        }
  	      },
  	      content: {
  	        title: 'Token Information',
  	        text: _.bind(function(){
  	        	return this.tokenTemplate(this);
  	        }, this)
  	      }
  	    });
    	
//    	this.eld3 = eld3;
    },
    setSelector: function(){
    	this.eld3 = d3.select('#'+this.id);
    	this.show = false;
    },
    showToken: function(){
//    	this.createToken();
    	this.eld3.classed('hidden', false);
    },
    hideToken: function(){
    	if(this.uiData.showFadeOut){
    	  this.eltokend3.transition()
    		.duration(250)
    		.attr("r", 30 + this.size)
    		.style("opacity", 0);
    		
    	  window.setTimeout(_.bind(function (){
			this.eltokend3.attr("r", this.size).style("opacity", 1);
			this.eld3.classed('hidden', true);
//			this.removeToken();
    	  }, this), 250);    		
    	}
    	else{
    		this.eld3.classed('hidden', true);    		
//    		this.removeToken();
    	}
    },    
    walking: function(tween, token){
    	if(tween.progress()>0 && tween.progress()<1){
    		if(token.state!='running'){
    			token.state = 'running';
    			token.showToken();    			
    		}
    		
    		offsetLength = token.path.getTotalLength() * tween.progress();
    		point = token.path.getPointAtLength(offsetLength);
    		
    		token.eld3.attr('transform',function(){
    			return 'translate('+[point.x, point.y]+')'; 
    		});       
    	}
    	else if(tween.progress()==1 && token.state!='finish'){
    		token.state = 'finish';
    		token.hideToken();
    	}
    	else if(isNaN(tween.progress()) || tween.progress()==0 && token.state!='initial'){
    		token.state = 'initial';
    		token.hideToken();    		
    	}
    	
    }
  };

  return Token;
})();