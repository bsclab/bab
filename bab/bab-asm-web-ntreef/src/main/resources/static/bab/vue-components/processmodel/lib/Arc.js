Arc = (function(){
  function Arc(options){
	_.extend(this, options);
    this.type = _.isUndefined(options.type)?'heuristic': options.type;
    this.isLogReplay = _.isUndefined(options.isLogReplay)? false: options.isLogReplay;
    this.meanNonSuccessive = _.isUndefined(options.type)?'timegap': options.meanNonSuccessive;
    this.source = options.source;
    this.sourceId = _.snakeCase(options.source);
//    this.sourceId = this.source.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
    this.target = options.target;
    this.targetId = _.snakeCase(options.target);
//    this.targetId = this.target.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
    this.id = this.sourceId+'-'+this.targetId;
    this.svgId = options.svgId;
    this.duration = options.duration;
    this.frquency = options.frequency;
    this.path = undefined;
    this.curve = undefined;
    this.difference = _.isUndefined(options.difference)? false: options.difference;		// only true and false
  }

  Arc.prototype = {
//    createArrow: function(scene, radius, material){
//      this.arrowMesh = ThreejsHelper.createOctahedronGeometry(scene, radius, this.curve.getPoint(1).x, this.curve.getPoint(1).y, 0, material);
//    },
  };

  return Arc;
})();