/* 
 * this is process model component based on dagre d3
 * author @superpikar
 * */
var DELAY = 500;
var INTERVALDURATION = 3000;

var HeuristicMinerComponent = ProcessModelComponent.extend({
  components: {
	'slider': SliderComponent  
  },
  ready: function() {
//	noUiSlider.create(document.getElementById('config-positiveObservation'), {
//		start: [ this.pmData.config.positiveObservation ],
//		range: {
//			'min': [   0 ],
//			'max': [ 100 ]
//		}
//	});
	  console.log('ready');
  },
  methods: {
	  updateProcessModel: function(nodes, arcs, originalConfig){
		  console.log(_.keys(nodes).length, 'nodes', _.keys(arcs).length, 'arcs');
		  var start = new Date();
		  
		  this.processModel.update(nodes, arcs, this.pmData.options);
		  this.processModel.drawProcessModel();
		  
		  var end = new Date();
		  var duration = end - start;
		  console.log('duration '+duration+'ms');
		  
		  // reset to previous value
		  console.log(this);
		  this.$set('pmData.config', JSON.parse(originalConfig))
		  // access children component by ref ID, the ID is set in the component using v-ref in parent template
		  this.$refs.sliderdependency.setSliderValue(this.pmData.config.threshold.dependency);
		  this.$refs.sliderpositiveobservation.setSliderValue(this.pmData.config.positiveObservation);
	  },
	  submitHeuristic: function(){
		  if(this.isProcessing){
			  alert('wait for previous processing')
		  }
		  else{
			  console.log(this.pmData.config);
//		  var args = JSON.flatten(config);
//		  delete args.repositoryURI;	// remove attribute
			  var args = {
					  "positiveObservation": parseInt(this.pmData.config.positiveObservation),
					  "option.allConnected": this.pmData.config.option.allConnected, 
					  "threshold.dependency": parseFloat(this.pmData.config.threshold.dependency)
			  };
			  var originalConfig = JSON.stringify(this.pmData.config);
			  
			  this.isProcessing = true;
			  
			  this.$http.post(this.apiURI.processModel, args).then(function (response) {
				  // success callback
				  if(response.data.status == 'FINISHED'){
					  this.updateProcessModel(response.data.response.nodes, response.data.response.arcs, originalConfig);
					  this.isProcessing = false;
				  }
				  else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
					  // if status is not FINISHED or RUNNING
					  this.isProcessing = false;
				  }
				  else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
					  // if status is running then check for each 3000 second
					  var intervalId = window.setInterval(_.bind(function(){
						  console.log('this is check every '+INTERVALDURATION+'ms');
						  this.$http.get(apiURI.job.queue).then(function(response2){
							  // success callback
							  var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
							  var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
							  
							  if(isJobInQueue || isJobInRunning){
								  console.log('current status is QUEUE/RUNNING');
								  // do nothing
							  }
							  else{
								  console.log('current status is FINISHED, request POST');
								  this.$http.post(this.apiURI.processModel, args).then(function (response3) {
									  
									  // error callback
									  if(response3.data.status == 'FINISHED'){
										  window.clearInterval(intervalId)
										  this.updateProcessModel(response3.data.response.nodes, response3.data.response.arcs, originalConfig);
										  this.isProcessing = false;
									  }
								  }, function(response3){
									  this.isProcessing = false;
								  });
							  }
						  }, function(response2){
							  // error callback
							  this.isProcessing = false;
						  })			  
						  
					  }, this), INTERVALDURATION);					  
				  }
			  }, function(response){
				  // error callback
				  this.isProcessing = false;
			  });			  
		  }
	  }
  }
});
