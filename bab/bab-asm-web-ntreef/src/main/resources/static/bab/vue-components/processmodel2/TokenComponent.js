var TokenComponent = Vue.extend({
	props: ['item'],
//	template: '#token-template',
	template: '<g v-if="item.show" v-bind:id="item.id" class="walker" transform="translate(0,0)">'+ 
		'<text class="label hidden" transform="translate(10,-10)">{{item.caseId}}</text>'+
      	'<circle cx="0" cy="0" r="10" transform="translate(0,0)"></circle>'+
      '</g>',
    ready: function(){
    	this.item.$eld3 = d3.select('#'+this.item.id);
    	console.log(this.item.id, 'ready', this.item.$eld3.attr('class'));
    },
    destroyed: function(){
    	console.log(this.item.id, 'destroyed');
    },
	methods: {
		
	}
})