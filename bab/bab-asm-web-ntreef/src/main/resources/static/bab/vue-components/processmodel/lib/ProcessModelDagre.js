// Write your package code here!
ProcessModelDagre = (function(){
  function ProcessModelDagre(containerEl, nodes, arcs, options){
    var self = this;
    
    this.containerEl = containerEl;
    this.nodes = new Nodees();
    this.nodes.initialize(nodes, options);
    this.arcs = new Arcs();
    this.arcs.initialize(arcs, options);
    
    this.g = new dagre.graphlib.Graph();
    
    this.options = _.extend({
      nodesep: 70,
      ranksep: 50,
      rankdir: "TB",
      marginx: 20,
      marginy: 20,
      isLogReplay: false
    }, options);
    
}

  ProcessModelDagre.prototype = {
    update: function(nodes, arcs, options){
      
    },
    computeProcessModelLayout: function() {
    	var g = new dagre.graphlib.Graph();

    	// Set an object for the graph label
    	g.setGraph({});

    	// Default to assigning a new object as a label for each new edge.
    	g.setDefaultEdgeLabel(function() { return {}; });

    	// Add nodes to the graph. The first argument is the node id. The second is
    	// metadata about the node. In this case we're going to add labels to each of
    	// our nodes.
    	this.nodes.getAll().forEach(function(val, key){
    		g.setNode(val.id, val);   		
    	});
    	
    	this.arcs.getAll().forEach(function(val, key){
    		g.setEdge(val.sourceId, val.targetId, val);   		
    	});
    	dagre.layout(g);
    	
    	var nodes = [];
    	var arcs = [];
    	
    	g.nodes().forEach(function(v){
    		nodes.push(g.node(v));
    	});
    	g.edges().forEach(function(e) {
    	    arcs.push(g.edge(e));
    	});
    	return {
    		nodes: nodes,
    		arcs: arcs
    	};
    },
    draw3DProcessModel: function(){
      // draw 3d here
    },
    setSVGSize: function(width, height){
      
    },
    graphScaleToFit: function(){
      
    },
    graphScaleToActual: function(){
      
    },
    updateGraph: function(){
      
    },
    changeArcDirection: function(direction){
      
    },
    changeArcType: function(type){
      
    }
  };

  return ProcessModelDagre;
})();


var computeDagreLayout = function(nodes, arcs){
	var options = {
        is3D: false,
        rankdir: "TB",
        arctype: 'bundle',
        isLogReplay: false,
        type: 'heuristic',
        renderer: 'dagred3'
      };
	var pm = new ProcessModelDagre('#processmodel-content', nodes, arcs, options);
	return JSON.stringify(pm.computeProcessModelLayout());
}