/*
 * @description this is statistic component based on Semantic-UI
 *
 * @example : <statistic label="Number of Cases" :value="data.numberOfCases" format="formatNumber"></statistic>
 *
 * @props label String label of the statistic
 * @props value String value of the statistic
 * @props format String format of the statistic value "formatNumber/formatDateTime"
 * @props theclass String additional class of statistic component
 * @dependency Semantic-UI CSS, vuejs, lodash
 * @author http://twitter.com/superpikar
 * */
var StatisticComponent = Vue.extend({
	props: ['label', 'value', 'format', 'theclass'],
	computed: {
		formatNumber: function(){
			return this.format == 'formatNumber';
		},
		formatDateTime: function(){
			return this.format == 'formatDateTime';
		},
		noFormat: function(){
			return _.isUndefined(this.format);
		}
	},
	template:
		'<div class="ui statistic" v-bind:class="{mini:formatDateTime, tiny:formatNumber}">'+
		'<div class="value" v-if="formatNumber">{{value | formatNumber}}</div>'+
		'<div class="value" v-if="formatDateTime">{{value | formatDateTime}}</div>'+
		'<div class="value" v-if="noFormat">{{value}}</div>'+
		'<div class="label">{{label}}</div>'+
		'</div>'
});
