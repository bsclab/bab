Nodee = (function(){
  function setPosX(pos, maxPos){
    return pos - (maxPos/2);
  }

  function setPosY(pos, maxPos){
    return (pos - (maxPos/2)) * -1;
  }

  function Nodee(options){
	_.extend(this, options);
    this.type = _.isUndefined(options.type)?'heuristic': options.type;
    this.isLogReplay = _.isUndefined(options.isLogReplay)? false: options.isLogReplay;
    this.label = options.label;
    this.id = _.snakeCase(options.label);
//    this.id = this.label.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
    this.duration = options.duration;
    this.frequency = options.frequency;
    this.svgId = options.svgId;
    this.nodeDOM = undefined;
    this.boxMesh = undefined;
    this.boxBorderMesh = undefined;
    this.difference = _.isUndefined(options.difference)? false: options.difference;		// only true and false;
  }

  Nodee.prototype = {

  };

  return Nodee;
})();
