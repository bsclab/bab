/* @description class of Arcs's process model, to initiate list of arc
 *
 * @example
 *    var arcs = new Arcs();
      arcs.add(new Arc({ }));
 *
 * @params options Object
 * @dependency lodash
 * @author http://twitter.com/superpikar
 */
Arcs = function(arcs){
  this.arcs = arcs || [];
};

Arcs.prototype = {
  initialize: function(arcs, options){
    this.arcs = [];
    var count = 1;
    for(var key in arcs){
      arcs[key].svgId = 'edge'+count;
      arcs[key].type = options.type;
      arcs[key].isLogReplay = options.isLogReplay;
      this.arcs.push(new Arc(arcs[key]));
      count++;
    }
  },
  findArc: function(source, target){
    return _.findWhere(this.arcs, {source: source, target: target});
  },
  add: function(arc){
    this.arcs.push(arc);
  },
  get: function(id){
    return _.findWhere(this.arcs, {id: id});
  },
  getBySvgId: function(id){
    return _.findWhere(this.arcs, {svgId: id});
  },
  updateAll: function(arcs){
    this.arcs = arcs;
  },
  getAll: function(){
    return this.arcs;
  }
};
