/*
 * @description this is file upload component, inspired by http://jsfiddle.net/bm0u8snr/1/
 *
 * @example : <file-upload title="The Title" placeholder="Please upload your file"></file-upload>
 *
 * @props title String label of the button
 * @props placeholder String plaeholder text
 * @dependency Semantic-UI CSS, vuejs
 * @author http://twitter.com/superpikar
 * */
var FileUploadComponent = Vue.extend({
  props: ['title', 'placeholder'],
  template:
	  '<div class="ui fluid action input">'+
  		'<input type="text" placeholder="{{placeholder}}" v-on:click="selectFile()" />'+
  		'<input type="file" name="file" v-on:change="setFile()" />'+
  		'<div class="ui button" v-on:click="selectFile()">{{title}}</div>'+
  	  '</div>',
  methods: {
	  selectFile: function(){
		  $(this.$el).find('input:file').click();
	  },
	  setFile: function(){
		  // get file name http://stackoverflow.com/a/13780375/1843755
		  var filename = $(this.$el).find('input:file')[0].files[0].name;
		  $(this.$el).find('input:text').val(filename);
	  }
  }
});
