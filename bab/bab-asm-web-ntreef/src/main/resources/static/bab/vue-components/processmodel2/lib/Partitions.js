Partitions = (function(){
  function Partitions(partitions){
	this.partitions = [];
	this.currentPartition = undefined;
  }

  Partitions.prototype = {
	  initialize: function(partitions){
	    this.partitions = [];
	    for(var key in partitions){    
	      this.partitions.push(new Partition(partitions[key]));
	    }
//	    console.log(this.partitions);
	    for(var key in this.partitions){
	      if(key < partitions.length-1){
	    	this.partitions[key].nextPartitionStart = this.partitions[key+1].start;  
	      }
	      else{
	    	  this.partitions.isLast = true;
	      }
	    }	    
	  },
	  setCurrentPartition: function(part){
		  for(var key in this.partitions){    
		      this.partitions[key].isActive = false;
		  }
//		  console.log(this.partitions);
		  this.currentPartition = _.findWhere(this.partitions, {part: part});
//		  console.log(this.currentPartition);
		  this.currentPartition.isActive = true;
	  },
	  add: function(partition){
	    this.partitions.push(partition);
	  },
	  get: function(id){
	    return _.findWhere(this.partitions, {id: id});
	  },
	  getByPart: function(part){
		  return _.findWhere(this.partitions, {part: part});
	  },
	  getAll: function(){
	    return this.partitions;
	  }
  };

  return Partitions;
})();