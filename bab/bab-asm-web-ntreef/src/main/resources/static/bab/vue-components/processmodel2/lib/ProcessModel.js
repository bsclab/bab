// credit : https://github.com/cpettitt/dagre-d3/issues/202
//SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
//    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
//};

// Write your package code here!
ProcessModel = (function(){
  var nodes = new Nodees();
  var arcs = new Arcs();
  var render;
  var g;
  
  var svg;
  var svgId; 
  var container;
  var svgGroup;
  var zoom;
  var nodeQtip;
  var arcQtip;
  var size = {
	  width: 800,
	  height: 600
  };
  var data;
  
  var options = {
	  nodesep: 70,
	  ranksep: 50,
	  rankdir: "TB",
	  marginx: 20,
	  marginy: 20,
	  isLogReplay: false
  };
  
  var nodeTemplate = Handlebars.compile($('#node-template').html());
  var arcTemplate = Handlebars.compile($('#arc-template').html());
  var arcHoverTemplate = Handlebars.compile($('#archovertooltip-template').html());
  var nodeHoverTemplate = Handlebars.compile($('#nodehovertooltip-template').html());
  var nodeClickTemplate = Handlebars.compile($('#nodeclicktooltip-template').html());

  function ProcessModel(containerEl, theNodes, theArcs, newOptions, data){
    nodes.initialize(theNodes, newOptions);
    arcs.initialize(theArcs, newOptions);

    _.extend(options, newOptions);
    // console.log('options', this.options, options);
    container = d3.select(containerEl);
    
    render = new dagreD3.render();
    g = new dagreD3.graphlib.Graph()
  		  .setGraph({})
  		  .setDefaultEdgeLabel(function() { return {}; });
    
    svgId = _.snakeCase(containerEl);
    svg = container.append('svg')
    				.attr('id', svgId)
    				.attr("version", "1.1")
    				.attr("xmlns", "http://www.w3.org/2000/svg")
  					.attr("xmlns:xlink", "http://www.w3.org/1999/xlink");
    svgGroup = svg.append("g");
    zoom = d3.behavior.zoom().on("zoom", function(){
      svgGroup.attr("transform", "translate(" + d3.event.translate + ")" + "scale(" + d3.event.scale + ")");
    });
    
    size.width = $(containerEl).width();
    if(options.isLogReplay){
//    	console.log('height', $('.logreplay-control').height());
    	size.height = $('body').height() - $('.top.fixed.menu').height() - $('.processmodel-control').height() - $('.logreplay-control').height() - 120;
    }
    else{
    	size.height = $('body').height() - $('.top.fixed.menu').height() - $('.processmodel-control').height() - 120;    	
    }
	
//    console.log('multiple', options.multiple);
    if(options.multiple){
    	this.container = container;
	    this.g = g;
	    this.svg = svg;
	    this.svgGroup = svgGroup;
	    this.render = render;
	    this.zoom = zoom;
    }
  }

  ProcessModel.prototype = {
	nodes: nodes,
	arcs: arcs,
	exportAsImage: function(){
      alert('not implemented yet')
	},
    update: function(theNodes, theArcs, newOptions){
      
      nodes.initialize(theNodes, newOptions);
      arcs.initialize(theArcs, newOptions);

      _.extend(options, newOptions);
    },
    destroyProcessModel: function(){
    	// remove node, arc, and tooltip
    	if(!_.isEmpty(g.nodes())){
//    		console.log('remove nodes');
    		g.nodes().forEach(function(val) {
    			g.removeNode(val);
    		});
    	}
    	if(!_.isEmpty(g.edges())){
//    		console.log('remove edges');
    		g.edges().forEach(function(val) {
    			g.removeEdge(val.v, val.w);
    		});
    	}
    	if(!_.isUndefined(nodeQtip)){
    		nodeQtip.qtip('destroy');
    	}
    	if(!_.isUndefined(arcQtip)){
    		arcQtip.qtip('destroy');
    	}    	
    },
    drawProcessModel: function() {
      this.destroyProcessModel();
      
      nodes.getAll().forEach(function(val, key){
	    var theClass= val.difference? 'difference': 'normal';
//	    console.log(val)
        g.setNode(val.svgId,  {
          id: val.svgId,
          labelType: "html",
          label: nodeTemplate(val),
          rx: 5,
          ry: 5,
          class: theClass
          // label: setNodeLabel(val, options.type, options.isLogreplay, false)
        });
      });

      // Set up edges, no special attributes.
      arcs.getAll().forEach(function(val, key){
    	var theClass= val.difference? 'difference': 'normal';
//    	console.log(val.sourceId, val.targetId);
    	var nodeSource = nodes.get(val.sourceId);
    	var nodeTarget = nodes.get(val.targetId);
    	g.setEdge(nodeSource.svgId, nodeTarget.svgId, {
          id: val.svgId,
          weight: 1,
          labelType: "html",
          label: arcTemplate(val),
          lineInterpolate: 'basis',
          labelpos: 'c',
          labelId: 'label-' + nodeSource.svgId+'-'+nodeTarget.svgId,
          source: val.source,
          target: val.target,
          class: theClass
        });
      });

      svg.call(zoom);

      this.setSVGSize(size.width, size.height);
      this.updateGraph();
      
      arcs.getAll().forEach(function(val, key){
    	 val.setPath();	// set path of each arcs 
      });
      
      nodeQtip = $('.graph g.node').qtip({
	      overwrite: true,
	      style: {
	        classes: 'qtip-light qtip-shadow'
	      },
	      position: {
	        target: 'mouse',
	        adjust: {
	          mouse: true  // Can be omitted (e.g. default behaviour)
	        }
	      },
	      content: {
	        title: 'Node Information',
	        text: function(event, api){
	          var node = nodes.getBySvgId($(this).attr('id'));
	          return nodeClickTemplate(node);
	        }
	      }
	  });
	  
	  // initialize node tooltip
	  arcQtip = $('.graph g.edge').qtip({
	      overwrite: true,
	      style: {
	        classes: 'qtip-light qtip-shadow'
	      },
	      position: {
	        target: 'mouse',
	        adjust: {
	          mouse: true  // Can be omitted (e.g. default behaviour)
	        }
	      },
	      content: {
	        title: 'Arc Information',
	        text: function(event, api){
	          var node = arcs.getBySvgId($(this).attr('id'));
	          return arcClickTemplate(node);
	        }
	      }
	  });
    },
    setSVGSize: function(width, height){
      svg.attr('width', width);
      svg.attr('height', height);
    },
    graphScaleToFit: function(){
      var isUpdate = true;
      var zoomScale = zoom.scale();
      var graphWidth = g.graph().width + 80;
      var graphHeight = g.graph().height + 40;
      var width = parseInt(svg.style("width").replace(/px/, ""));
      var height = parseInt(svg.style("height").replace(/px/, ""));
      zoomScale = Math.min(width / graphWidth, height / graphHeight);
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      zoom.translate(translate);
      zoom.scale(zoomScale);
      zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
    },
    graphScaleToActual: function(){
      var isUpdate = true;
      var graphWidth = g.graph().width + 80;
      var graphHeight = g.graph().height + 40;
      var width = parseInt(svg.style("width").replace(/px/, ""));
      var height = parseInt(svg.style("height").replace(/px/, ""));
      var zoomScale = 1;
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      zoom.translate(translate);
      zoom.scale(zoomScale);
      zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
    },
    updateGraph: function(){
      // update the graph param
      g.setGraph(options);
      // Run the renderer. This is what draws the final graph.
      
      render(svgGroup, g);
      
//       Center the graph
      var xCenterOffset = (svg.attr("width") - g.graph().width) / 2;
      svgGroup.attr("transform", "translate(" + xCenterOffset + ", 20)");
      this.graphScaleToFit();
      
//      console.log(g);
    },
    changeArcDirection: function(direction){
      options.rankdir = direction;
      this.updateGraph();
    },
    changeArcType: function(type){
      for (var row in g._edgeLabels){
        var edge = g._edgeLabels[row];
        edge.lineInterpolate = type;
      }
      this.updateGraph();
    },
    searchNode: function(nodeName){
      d3.selectAll('.node').classed('found', false);
      if(nodeName.trim() == ''){
      	this.graphScaleToFit();
      	nodes.nodeFound = 0;
      }
      else{
  	    var theNodes = nodes.searchNode(nodeName);
  	    
  	    if(theNodes.length===0){
  	    	nodes.nodeFound = -1;
  	    	this.graphScaleToFit();
  	    }	
  	    else{
  	    	nodes.nodeFound = theNodes.length;
  	    	// console.log('search node '+nodeName+' '+this.nodes.nodeFound.length);
  	    	theNodes.forEach(function(val, key){
  	    		d3.select('#'+val.svgId).classed('found', true);
  	    	});
  	    	
  	    	// if founded nodes only 1 then zoom to the node
  	    	if(theNodes.length==1){
  	    		var bbox = d3.select('#'+theNodes[0].svgId)[0][0].getBBox();
  	    		
  	    		var isUpdate = true;
  	    		var translate = [Math.abs(bbox.x), Math.abs(bbox.y)];
  	    		zoom.translate(translate);
  	    		zoom.scale(1);
  	    		zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
  	    	}
  	    	else if(theNodes.length>1){
  	    		this.graphScaleToFit();
  	    	}
  	    }	    
      }
    }
  };

  return ProcessModel;
})();
