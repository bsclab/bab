/* 
 * this is process model component based on dagre d3
 * author @superpikar
 * */
var LogReplayComponent = ProcessModelComponent.extend({
  ready: function() {
	console.log('logreplay is ready', this.repoData);
	this.loadLogReplay();
  },
  methods: {
    // methods for log replay
	alertLogReplay: function(){
    	alert('this is log replay component')
    },
    loadLogReplay: function(){
	    var self = this;
	
    	this.$http.post(this.apiURI.logReplay, { limit: this.lrData.options.tokensPerPartition }).then(function(response2){
    		console.log('log replay', response2.data);
    		
    	}, function(response2){
    		alert('error get log replay!');	    		
    	});	    	
	    
//	    reqwest({
//	      url: this.apiURI.repository,
//	      method: 'get',
//	      crossOrigin: true
//	    })
//	    .then(function(response){
//	      console.log('repository', response.status);
//	
//	      if(response.status == 'QUEUED' || response.status == 'RUNNING'){
//	        WaitingHelper.show();
//	      }
//	      else if(response.status == 'FAILED'){
//	        alert('Cannot load data.');
//	      }
//	      else if(response.status == 'FINISHED'){
//	        var cases = response.response.cases;
//	        
//	        reqwest({
//	          url: this.apiURI.logreplay,
//	          method: 'post',
//	          data: {
//	            limit: self.lrData.options.tokensPerPartition
//	          },
//	          crossOrigin: true
//	        })
//	        .then(function(response2){
//	          console.log('logreplay', response2.status);
//	          if(response2.status == 'QUEUED' || response2.status == 'RUNNING'){
//	            WaitingHelper.show();
//	          }
//	          else if(response2.status == 'FAILED'){
//	            alert('Cannot load data.');
//	          }
//	          else if(response2.status == 'FINISHED'){
//	            self.lrData.numberOfTokens = response2.response.eventsNumber;
//	            self.logReplay.initializeData(self.processModel, response2.response, cases);
//	            self.logReplay.initialize();
//	          }
//	        })
//	        .fail(function(error2){
//	          alert('log replay error!');
//	        });
//	      }
//	    })
//	    .fail(function(error2){
//	      alert('get repository error!');
//	    });
	},
    play: function(){
      if(this.lrData.state=='play'){
        this.logReplay.pause();
      }
      else if(this.lrData.state=='pause'){
        this.logReplay.resume();
      }
      else if(this.lrData.state=='stop'){
        this.logReplay.play();
      }
      console.log(this.logReplay.state, '-', this.lrData.state);
    },
    restart: function(){
      this.logReplay.restart();
    },
    stop: function(){
      this.logReplay.stop();
      console.log(this.logReplay.state, '-', this.lrData.state);
    },
    toggleKPI: function(){
      this.lrData.setting.toggleKPI = !this.lrData.setting.toggleKPI;
      if(this.lrData.setting.toggleKPI){
        this.logReplay.startKPICalculation();
      }
      else{
        this.logReplay.stopKPICalculation();
      }
    },
    toggleRealtimeKPI: function(){
      this.lrData.setting.toggleRealtimeKPI = !this.lrData.setting.toggleRealtimeKPI;
    },
    toggleFadeout: function(){
      this.lrData.setting.toggleFadeout = !this.lrData.setting.toggleFadeout;
    }
  }
});
