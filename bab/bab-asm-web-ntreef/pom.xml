<!-- 
/******************************************************************************
*       This file is part of Best Analytics of Big Data (BAB) v2 Package.      *
* ============================================================================ *
*                                                                              * 
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
 ******************************************************************************/
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  	<!-- Identification -->
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>kr.ac.pusan.bsclab</groupId>
		<artifactId>bab</artifactId>
		<version>2.2.0-SNAPSHOT</version>
	</parent>
	<artifactId>bab-asm-web-ntreef</artifactId>
	<name>BAB v2 - NTreeF Project Web Visualizer</name>

	<!-- License -->
	<licenses>
		<license>
			<name>GNU Lesser General Public License, Version 3.0</name>
			<url>http://www.gnu.org/licenses/lgpl-3.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<!-- Configuration -->
	<properties>
		<!-- Application Configuration -->
		<failOnMissingWebXml>false</failOnMissingWebXml>
	</properties>

	<!-- Dependencies -->
	<dependencies>
		<!-- NTreeF Web Related Dependencies -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<!-- scope>runtime</scope -->
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.neo4j</groupId>
			<artifactId>neo4j-cypher-compiler-2.1</artifactId>
		</dependency>
		<dependency>
			<groupId>org.graphstream</groupId>
			<artifactId>gs-core</artifactId>
			<version>1.3</version>
		</dependency>
		<dependency>
			<groupId>org.graphstream</groupId>
			<artifactId>gs-ui</artifactId>
			<version>1.3</version>
		</dependency>
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>1.3.0</version>
		</dependency>
		<!-- Spring Related Dependencies -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-integration</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-tomcat</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- BAB Module Dependencies -->
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-core</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-common</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-ext-sf</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-asm-ntreef</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-web</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-web-common</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-web-ext-sf</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<!-- Apache Hadoop Dependencies -->
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-annotations</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-assemblies</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-common</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-hdfs</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-nfs</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-auth</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-client</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-api</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-common</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-client</artifactId>
			<scope>compile</scope>
		</dependency>
		<!-- Apache Spark Dependencies -->
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-core_2.10</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-sql_2.10</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-streaming_2.10</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-mllib_2.10</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-graphx_2.10</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-yarn_2.10</artifactId>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-hive_2.10</artifactId>
			<scope>compile</scope>
		</dependency>
		
		<!-- Jackson Dependencies -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<scope>compile</scope>
		</dependency>

		
		<!-- RServe -->
		<dependency>
			<groupId>org.nuiton.thirdparty</groupId>
			<artifactId>REngine</artifactId>
			<version>1.7-3</version>
		</dependency>
		<dependency>
			<groupId>org.rosuda.REngine</groupId>
			<artifactId>Rserve</artifactId>
			<version>1.8.1</version>
		</dependency>

		<dependency>
		    <groupId>junit</groupId>
		    <artifactId>junit</artifactId>
		    <scope>test</scope>
		</dependency>
		<!-- dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>compile</scope>
		</dependency>
		 -->
		<!-- DL47 -->
		<dependency>
    		<groupId>org.nd4j</groupId>
    		<artifactId>nd4j-api</artifactId>
    		<version>0.8.0</version>
		</dependency>
    	<dependency>
        	<groupId>org.nd4j</groupId>
         	<artifactId>nd4j-native-platform</artifactId>
       		<version>0.8.0</version>
    	</dependency>
    	<!-- Core DL4J functionality -->
        <dependency>
            <groupId>org.deeplearning4j</groupId>
            <artifactId>deeplearning4j-core</artifactId>
            <version>0.8.0</version>
        </dependency>

        <dependency>
            <groupId>org.deeplearning4j</groupId>
            <artifactId>deeplearning4j-nlp</artifactId>
            <version>0.8.0</version>
        </dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<!-- dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
		</dependency>
		 -->
		<dependency>
			<groupId>com.jcraft</groupId>
			<artifactId>jsch</artifactId>
		</dependency>
	
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
		</dependency>
		<dependency>
		    <groupId>junit</groupId>
		    <artifactId>junit</artifactId>
		    <scope>test</scope>
		</dependency>
	</dependencies>

	<!-- Build Configuration -->
	<build>
		<!-- Plugins -->
	    <plugins>
		    <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<!--
					<skipTests>true</skipTests>  
					-->
					<testFailureIgnore>true</testFailureIgnore>
				</configuration>
		    </plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<mainClass>kr.ac.pusan.bsclab.bab.assembly.BabServer</mainClass>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<description>BAB Framework 2nd Version - Web Visualizer Build for NTreeF Project</description>
</project>