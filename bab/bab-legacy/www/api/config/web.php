<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
	
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
		'modules' => [
			'v1.0' => 'app\modules\v1_0\v1_0',
		],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
				'urlManager' => [
						'enablePrettyUrl'=>'true',
						'showScriptName'=>true,
						//'enableStrictParsing' => true,
						'rules' => [
							'' => 'bab/index',
							'v1.0/doc/<action:\w+>/<page:\w+>' => 'v1.0/doc/<action>',
							//*/
							'POST							v1.0/api/<group:\w+>/<controller:\w+>' 								=> 'v1.0/<group>_<controller>/create',
							'GET,HEAD,POST		v1.0/api/<group:\w+>/<controller:\w+>/<resource:\w+>' => 'v1.0/<group>_<controller>/read',
							'PUT,PATCH				v1.0/api/<group:\w+>/<controller:\w+>/<resource:\w+>' => 'v1.0/<group>_<controller>/update',
							'DELETE						v1.0/api/<group:\w+>/<controller:\w+>/<resource:\w+>' => 'v1.0/<group>_<controller>/delete',
							'GET,HEAD					v1.0/api/<group:\w+>/<controller:\w+>' 								=> 'v1.0/<group>_<controller>/index',
							'v1.0/api/<group:\w+>/<controller:\w+>/<action:\w+>/<resource:\w+>'			=> 'v1.0/<group>_<controller>/<action>',
							'v1.0/api/<group:\w+>/<controller:\w+>/<action:\w+>'										=> 'v1.0/<group>_<controller>/<action>',
							//*/
						],
				],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
