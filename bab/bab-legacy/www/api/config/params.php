<?php

return [
	'adminEmail' => 'admin@example.com',
	'versions' => [
		'v1.0',
	],
	'latestVersion' => 'v1.0',
	'hdfs' => [
		'dfs'       => 'hdfs://kim.bab:8020',
		'host'      => 'http://kim.bab:50070/webhdfs/v1',
		'executor'  => 'http://localhost/api/web/run.php',
		'user'      => 'hadoop',
		'home'      => '/bab/workspaces',
	],
	'spark' => [
		'master'    => 'http://kim.bab:18080/',
		'rest'      => 'http://kim.bab:6066',
		'rmaster'   => 'spark://kim.bab:7077',
		'version'   => '1.3.0',
	],
	'curl' => [
		'useProxy' => false,
		'proxyHost' => '',
		'proxyType' => 7,
	],
];
