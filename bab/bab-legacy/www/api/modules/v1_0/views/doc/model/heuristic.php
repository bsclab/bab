<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Heuristics Miner API Reference</h1>
		<p></p>
	</section>
	<section>
		<h2>Description</h2>
		<p>
		</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>POST /api/model/heuristic/[repository.id] {
	"threshold": {
		"relativeToBest": 0.05,
		"l1Loop": 0.9,
		"l2Loop": 0.9,
		"longDistance": 0.9,
		"and": 0.1,
	},
	"positiveObservation": 10,
	"dependencyDivisor": 1,
	"option": {
		"extra": false,
		"allConnected": false,
		"longDistance": false,
	}
}</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>threshold[relativeToBest]</code></td>
			<td><code>Double</code></td>
			<td>Heuristics thresholds</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>threshold[l1Loop]</code></td>
			<td><code>Double</code></td>
			<td>Heuristics thresholds</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>threshold[l2Loop]</code></td>
			<td><code>Double</code></td>
			<td>Heuristics thresholds</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>threshold[longDistance]</code></td>
			<td><code>Double</code></td>
			<td>Heuristics thresholds</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>threshold[and]</code></td>
			<td><code>Double</code></td>
			<td>Heuristics thresholds</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>positiveObservation</code></td>
			<td><code>String</code></td>
			<td>Positive observation of event or relations, event that occur below this threshold will be removed</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>dependencyDivisor</code></td>
			<td><code>String</code></td>
			<td>Dependency divisor for dependency matrix</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>option[allConnected]</code></td>
			<td><code>String</code></td>
			<td>Use all connected heuristics</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>option[longDistance]</code></td>
			<td><code>String</code></td>
			<td>Use long distance heuristics</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{
	"nodes": [
		"node-ActA": {
			"label": "ActA",
			"frequency": {
				"absolute": 10,
				"relative": 0.1,
				"cases": {
					"include": 10,
					"asStartEvent": 2,
					"asEndEvent": 0,
				},
				"maxRepetition": 2
			},
			"duration": {
				"total": 10000000,
				"min": 1000,
				"max": 100000,
				"mean": 1000,
				"median": 1000
			}
		}
	],
	"arcs": [
		"arc-ActAtoActB": {
			"source": "ActA",
			"target": "ActB";
			"frequency": {
				"absolute": 10,
				"relative": 0.1,
				"cases": 5,
				"maxRepetition": 2
			},
			"duration": {
				"total": 10000000,
				"min": 1000,
				"max": 100000,
				"mean": 1000,
				"median": 1000
			}
		}
	]
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td></td>
		</tr>
		</tbody>
		</table>
	</section>
</article>