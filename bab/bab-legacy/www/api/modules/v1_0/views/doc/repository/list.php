<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Repository List API Reference</h1>
		<p>Get event repository list for selected workspace</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>Get event repository list for selected workspace</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>GET /api/repository/workspace/[workspaceId]
</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>userId</code></td>
			<td><code>String</code></td>
			<td>User id</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
	"workspaceId": "[workspaceId]",
	"repositories": {
		"[repositoryId]": {
			"repositoryId": "[repositoryId]",
		}
	}
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>workspaceId</code></td>
			<td><code>String</code></td>
			<td>Workspace id</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>repositoryId</code></td>
			<td><code>String</code></td>
			<td>Repository id</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<!--
		<tr>
			<td><code>UnsupportedFileFormat</code></td>
			<td>File format is not supported. Supported file format : .MXML, .MXML.GZ, .CSV, .XLS, .XLSX, .XES</td>
		</tr>
		-->
		</tbody>
		</table>
	</section>
</article>