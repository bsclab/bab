<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1> API Reference</h1>
		<p>test</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>
		</p>
		<footer>
			<h3>Reference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>GET /api/
         {
		  "series": "Task"
		  "Y": "Sum"
		  "Z": "Weekly"
		  "unit": "seconds"
		  "calculation": "Working Time"
		  }

</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>Series</code></td>
			<td><code>String</code></td>
			<td>selected series for performance chart series<br/>Task Originator </td>
			<td>Yes</td>
		</tr>
		<tr>
		     <td><code>Y</code></td>
			 <td><code>String</code></td>
			 <td> Y axis selection <br />Sum Average Count Minimum Maximum Std.dev</td>
			 <td>Yes</td>
		</tr>
		<tr>
		     <td><code>Z</code></td>
			 <td><code>String</code></td>
			 <td> Z axis selection <br/>
			      -None<br/> 
				  -Weekly<br/> 
				  -Monthly<br/> 
				  -Quarterly<br/> 
				  -sixMonthly<br/> 
				  -annualy</td>
			 <td>Optional</td>
		</tr>
		<tr>
			 <td><code>Unit</code>
			 <td><code>String</code></td>
			 <td> Selection time unit <br/>seconds minutes hours days weeks months</td>
			 <td>Yes</td>
		</tr>
		<tr>
			 <td><code>calculation</code></td>
			 <td><code>String</code></td>
			 <td>Selection for performance chart calculation method<br/>Working Time Waiting Time Both</td>
			 <td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
        "id" : [performance id]
		"series": [selected series]
		"labels": [array of labels of all instances]
		"matrix": [
		            {
					   "[label]" : [series of instance]
					   "[X]": [X value]
					   "[Y]": [Y value]
					   "[Z]": [Z value]
					}
					.....
					]
	}
	
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>id</code></td>
			<td><code>String</code></td>
			<td>performance id</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>series</code></td>
			<td><code>String</code></td>
			<td>selected series</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>labels</code></td>
			<td><code>String</code></td>
			<td>all series about selected series</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>label</code></td>
			<td><code>String</code></td>
			<td>series of instance</td>
			<td> Yes </td>
		</tr>
		<tr>
			<td><code>X</code></td>
			<td><code>long</code></td>
			<td>X axis value of instance</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>Y</code></td>
			<td><code>Double</code></td>
			<td>Y axis value of instance</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>Z</code></td>
			<td><code>String</code></td>
			<td>Z axis value of instance</td>
			<td>Optional</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td></td>
		</tr>
		</tbody>
		</table>
	</section>
</article>