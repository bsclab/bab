<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Repository_SummaryController extends ServiceController {

	public function actionRead($resource) {
		return $this->runBabJob($resource, 'RepositoryLogSummary', 'blsum');
	}

	public function actionRead2($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$path = '/' . $workspaceId . '/' . $repositoryId . '.blsum';
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		} else {
			$this->executorRun('RepositoryLogSummaryJob', $workspaceId . ' ' . $repositoryId);
			$response['returnUri'] = $resource;
		}; 
		return $response;
	}

}