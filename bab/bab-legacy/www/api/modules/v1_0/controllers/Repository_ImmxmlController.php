<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Repository_ImmxmlController extends ServiceController {

	public function actionRead($resource) {
		return $this->runBabJob($resource, 'RepositoryMxmlImportJob', 'mrepo');
	}
	
}