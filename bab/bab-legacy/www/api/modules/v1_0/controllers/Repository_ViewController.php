<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Repository_ViewController extends ServiceController {

	public function actionRead($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$path = '/' . $workspaceId . '/' . $repositoryId . '.brepo';
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		}; 
		return $response;
	}

	public function actionDataset($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$path = '/' . $workspaceId . '/' . $repositoryId . '.mrepo';
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		}; 
		return $response;
	}

	public function actionHbmodel($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$path = '/' . $workspaceId . '/' . $repositoryId . '.hbmodel';
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		}; 
		return $response;
	}
}