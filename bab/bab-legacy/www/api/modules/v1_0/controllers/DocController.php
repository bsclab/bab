<?php

namespace app\modules\v1_0\controllers;

use app\components\Controller;

class DocController extends Controller {
	public $layout = "//v1_0_docs";
	public $api = '';

	public function actionIndex() {
		$this->layout = "//v1_0";
		echo $this->render('index');
	}

	public function actionRepository($page = 'index') {
		$this->api = 'repository';
		echo $this->render('repository/' . $page);
	}

	public function actionModel($page = 'index') {
		$this->api = 'model';
		echo $this->render('model/' . $page);
	}

	public function actionAnalysis($page = 'index') {
		$this->api = 'analysis';
		echo $this->render('analysis/' . $page);
	}
}
