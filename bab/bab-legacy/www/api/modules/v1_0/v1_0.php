<?php

namespace app\modules\v1_0;

class v1_0 extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\v1_0\controllers';

    public function init()
    {
        parent::init();
    }
}
