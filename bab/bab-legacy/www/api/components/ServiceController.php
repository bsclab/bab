<?php

namespace app\components;

use Yii;
use yii\rest\Controller;
use yii\rest\Serializer;
use yii\web\Response;
use yii\helpers\ArrayHelper;

class ServiceController extends Controller {
	
	public function getParams() {
		return Yii::$app->params;
	}
	
	public function getHdfsConfiguration() {
		return $this->params['hdfs'];
	}
	
	public function getHdfsHome() {
		return $this->params['hdfs']['home'];
	}
		
	public function executorRun($job, $arguments = '', $verb = 'GET') {
        $args = explode(" ", $arguments);
        $version = $this->params['spark']['version'];
        $hdfs = $this->params['hdfs']['dfs'];
        $spark = $this->params['spark']['rmaster'];
        $args[] = $hdfs;
        $args[] = $spark;
        $submission = [
            "action"    => "CreateSubmissionRequest",
            "appArgs"   => $args,
            "appResource"   => $hdfs . "/bab/lib/bab-1.0.0.jar",
            "clientSparkVersion"    => $version,
            "environmentVariables"  => [
                "SPARK_ENV_LOADED" => "1"
            ],
            "mainClass"=> "kr.ac.pusan.re.ebiz.bab.ws.executor.BabExecutor",
            "sparkProperties" => [
                "spark.jars" => $hdfs . "/bab/lib/bab-1.0.0.jar",
                "spark.app.name" => "BAB",
                "spark.submit.deployMode" => "cluster",
                "spark.master" => $spark
            ]
        ];
        
		//$arguments .= ' 2';
		//$url = $this->params['hdfs']['executor'] . '?cmd=' . $job . '&args=' . base64_encode($arguments);
        $url = $this->params['spark']['rest'] . '/v1/submissions/create';
        $verb = "POST";
        $json = json_encode($submission);
		$ch = curl_init();
		curl_setopt($ch ,CURLOPT_CUSTOMREQUEST	,$verb);
		curl_setopt($ch ,CURLOPT_URL 						,$url);
		curl_setopt($ch ,CURLOPT_RETURNTRANSFER ,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 
		curl_setopt($ch ,CURLOPT_VERBOSE 				,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($json))                                                                       
);                                                                                                                   
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		return $response;
	}	
	
	public function hdfsFileExists($path) {
		$cacheStatus = $this->hdfsExecute($this->hdfsHome . $path . '?op=LISTSTATUS');
		return (!(isset($cacheStatus['RemoteException']['exception']) && $cacheStatus['RemoteException']['exception'] == 'FileNotFoundException'));
	}
	
	public function hdfsExecute($url, $verb = 'GET') {
		$url = $this->params['hdfs']['host'] . $url . '&user.name=' . $this->params['hdfs']['user'];
		$ch = curl_init();
		curl_setopt($ch ,CURLOPT_CUSTOMREQUEST	,$verb);
		curl_setopt($ch ,CURLOPT_URL 						,$url);
		curl_setopt($ch ,CURLOPT_RETURNTRANSFER ,1);
		curl_setopt($ch ,CURLOPT_VERBOSE 				,1);
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		return $response;
	}	
			
	public function hdfsLoad($url) {
		$url = $this->params['hdfs']['host'] . $url . '&user.name=' . $this->params['hdfs']['user'];
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_URL , $url );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $ch , CURLOPT_VERBOSE , 1 );
		curl_setopt( $ch , CURLOPT_HEADER , 1 );
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		$rurl = $info['url'];
		curl_close($ch);
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_URL , $rurl );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		return $response;
	}
		
	public function hdfsSave($url, $pfile) {
		$url = $this->params['hdfs']['host']. $url . '&user.name=' . $this->params['hdfs']['user'];
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_URL , $url );
		curl_setopt( $ch , CURLOPT_PUT , 1 );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $ch , CURLOPT_VERBOSE , 1 );
		curl_setopt( $ch , CURLOPT_HEADER , 1 );
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		$rurl = $info['redirect_url'];
		$rurls = explode(':', $rurl);
		$host = substr($rurls[1], 2);
		curl_close($ch);
		$fh = fopen($pfile, 'r');
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_URL , $rurl );
		curl_setopt( $ch , CURLOPT_PUT , 1 );
		curl_setopt( $ch , CURLOPT_INFILE , $fh );
		curl_setopt( $ch , CURLOPT_INFILESIZE , filesize($pfile) );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $ch , CURLOPT_VERBOSE , 1 );
		curl_setopt( $ch , CURLOPT_HEADER , 1 );
		if ($this->params['curl']['useProxy']) {
			curl_setopt($ch , CURLOPT_PROXY, $this->params['curl']['proxyHost']);
			curl_setopt($ch , CURLOPT_PROXYTYPE, $this->params['curl']['proxyType']);
		}
		$response = json_decode(curl_exec($ch));
		$info = curl_getinfo($ch);
		curl_close($ch);
		return $response;
	}

	public function beforeAction($action) {
		$return = parent::beforeAction($action);
		Yii::$app->response->format = Response::FORMAT_JSON;
		return $return;
	}

	public function actionIndex() {
		return array('action' => 'index');
	}

	public function actionCreate() {
		return array('action' => 'create');
	}

	public function actionRead($resource) {
		return array('action' => 'read/' . $resource);
	}

	public function actionUpdate($resource) {
		return array('action' => 'update/' . $resource);
	}

	public function actionDelete($resource) {
		return array('action' => 'delete/' . $resource);
	}
	
	public function toJson($arr) {
		$result = array();
		foreach ($arr as $idx => $val) {
			if (is_array($val) || is_object($val)) {
				$result[$idx] = $this->toJson($val);
				continue;
			}
			$val = trim($val);
			$lval = strtolower($val);
			if ($lval == 'false' || $lval == 'true') {
				$result[$idx] = $lval == 'true' ? true : false;
			} else if (is_numeric($val)) {
				$result[$idx] = $val + 0;
			} else {
				$result[$idx] = $val;
			}
		}
		return $result;
	}

	public function runBabJob($resource, $job, $output, $params = null) {

		if ($params == null) {
			$params = (is_array($_POST)) ? $_POST : array();
		}
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$request = $this->toJson($params);
		$request['repositoryURI'] = $this->hdfsHome . '/' . $workspaceId . '/' . $repositoryId;

		$json = json_encode($request);
		$signature = md5($json);
		$temp = sys_get_temp_dir() . '/' . $repositoryId . '_' . $signature;
		file_put_contents($temp, $json);
		$jobPath = '/' . $workspaceId . '/jobs/' . $repositoryId . '_' . $signature . '.' . $output;
		$run = false;
		if (!$this->hdfsFileExists($jobPath)) {
			$run = true;
			$this->hdfsSave($this->hdfsHome . '/' . $workspaceId . '/jobs/' . $repositoryId . '_' . $signature . '.' . $output . '?op=CREATE&overwrite=true', $temp);
		}
		
		$path = '/' . $workspaceId . '/' . $repositoryId . '_' . $signature . '.' . $output;
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		} else {
			$jobString = $job . ' ' . $workspaceId . ' ' . $repositoryId . '_' . $signature . ' ' . $output;
			if ($run) {
				$this->executorRun('BabJob', $jobString);
			}
			$response['returnUri'] = $workspaceId . '_' . $repositoryId . '_' . $signature;
			$response['jobTrackingId'] = base64_encode('BAB:' . $job . '@' . $workspaceId . '_' . $repositoryId . '_' . $signature);
		}; 
		return $response;
	}
	
}