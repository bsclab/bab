<!DOCTYPE html>
<html>
	<head>
		<title>Sample of Process Model</title>
		<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	</head>
	<body>
		<div class="container">
			<div id="content" class="row">
				<div id="content-graph" class="col-md-9">
					<h1>this is chart</h1>
					<svg width="800"></svg>
				</div>
				<div id="content-sidebar" class="col-md-3"></div>
			</div>
		</div>
		
		<script src="http://d3js.org/d3.v3.min.js"></script>
		<script src="http://cpettitt.github.io/project/dagre-d3/latest/dagre-d3.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/string.js/2.2.0/string.min.js"></script>
		
		<!--<script src='public/js/jquery/jquery.min.js'></script>
		<script src='public/js/underscore/underscore.min.js'></script>
		<script src='public/js/backbone/backbone.min.js'></script>
		-->

		<script src="{{ asset('js/require/require.js') }}"></script>
		<!--<script data-main="public/bab/js/processvisualizer/heuristicminer/main" src="public/js/require/require.js"></script>-->
		<!--<script src="public/bab/js/processvisualizer/heuristicminer/main.js"></script>-->
		
		<script type="text/javascript">
			require.config({
				// disable chaching for requirejs http://stackoverflow.com/questions/8315088/prevent-requirejs-from-caching-required-scripts
				urlArgs: "bust=" +  (new Date()).getTime(),
				paths: {
				jquery: 'js/jquery/jquery.min',
					underscore: 'js/underscore/underscore.min',
					backbone: 'js/backbone/backbone.min',
					backbonerelational: 'js/backbone/backbone-relational',
					dagreD3: 'http://cpettitt.github.io/project/dagre-d3/latest/dagre-d3'
				}
			});

			require(
				[
					'bab/js/chart/processmodel/views/ProcessmodelView',
					'backbone'
				],

				function(ProcessmodelView, Backbone) {

					var myJson = null;
				    $.ajax({
				        'async': false,
				        'global': false,
				        'url': 'http://localhost/experiment-thesis/processmodel/public/files/heuristicExampleSmall.json',
				        'dataType': "json",
				        'success': function (data) {
				            myJson = data;
				        }
				    });				    

					var TheModel = Backbone.Model.extend({
						defaults: { 
							jsonModel: myJson 
						}
					});
					
					var pm = new ProcessmodelView({
						model: new TheModel()
					});
				}
			);
		</script>

	</body>
</html>