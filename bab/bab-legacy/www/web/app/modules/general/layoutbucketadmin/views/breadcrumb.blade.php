<?php

$ws = '';
$user = User::find(Auth::id());
$current = Session::get('workspaceId');
if ($user != null && $current != '') {
	$workspaces = explode(',', $user->workspaces);
	foreach ($workspaces as $workspace) {
		list($uri, $alias) = explode(':', $workspace, 2);
		if ($uri == $current) {
			$ws = $alias; 
			break;
		}
	}
}

?>
<div class="row" id="breadcrumb">
    <div class="col-md-12">
        <!--breadcrumbs start -->
		<?php $user = User::find(Auth::id()); ?>
		@if ($user != null)
        <span class="pull-right label label-warning" style="text-align: left;">
		Workspace : {{ $ws }}
		@if (Session::get('resourceId') != '')<br />
				Repository : {{ substr(substr(Session::get('resourceId'), strpos(Session::get('resourceId'), '_') + 1), 0, -14) }}
				@endif
				</span>
				@endif
        {{ Breadcrumbs::render() }}
        <!--breadcrumbs end -->
    </div>
</div>