<?php $user = User::find(Auth::id()); ?>
<!--header start-->
<header class="header fixed-top clearfix">
	<!--logo start-->
	<div class="brand">
		<a href="{{ action('App\Modules\Dashboard\Home\Controllers\HomeController@getIndex') }}" class="logo">
			<div class="logo-logminer"></div>
			<div class="logo-text"></div>
		</a>
		<div class="sidebar-toggle-box">
			<div class="fa fa-bars"></div>
		</div>
	</div>
		<?php if ($user != null) { ?>
    <!--logo end-->
	<div class="nav notify-row" id="top_menu">
		<!--  notification start -->
		<ul class="nav top-menu">
					<?php $workspaces = explode(',', $user->workspaces); ?>
            <!-- settings start -->
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-tasks"></i> 
					<span class="badge bg-success"><?php echo count($workspaces); ?></span>
				</a>
				<ul class="dropdown-menu extended tasks-bar">
					<li>
						<div class="task-info clearfix">
							<div class="desc pull-left">
								<h5>Workspaces</h5>
							</div>
						</div>
					</li>
			<?php
			$current = Session::get ( 'workspaceId' );
			foreach ( $workspaces as $workspace ) {
				list ( $uri, $alias ) = explode ( ':', $workspace, 2 );
				?>
                    <li><a
						href="<?php echo url('/home/changeworkspace/' . $uri); ?>">
							<div class="task-info clearfix">
								<div class="desc pull-left">
									<h5><?php echo $alias; ?></h5>
									<?php if ($uri == $current) { ?>
									<span><i>Current Workspace</i></span>
									<?php } ?>
                           		</div>
							</div>
					</a></li>
				<?php } ?>
                </ul></li>
			<!-- settings end -->
		</ul>
		<!-- notification end -->
	</div>
	<div class="top-nav clearfix">
		<!--search & user info start-->
		<ul class="nav pull-right top-menu">
			<li><input type="text" class="form-control search"
				placeholder=" Search"></li>
			<!-- user login dropdown start-->
			<li class="dropdown"><a data-toggle="dropdown"
				class="dropdown-toggle" href="#"> <img alt=""
					src="<?php echo babModuleAsset('general/layout/assets/lib/images/user.png'); ?>"> <span
					class="username"><?php echo $user->fullname; ?></span> <b
					class="caret"></b>
			</a>
				<ul class="dropdown-menu extended logout">
					<li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
					<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
					<li><a href="<?php echo url('home/logout'); ?>"><i
							class="fa fa-key"></i> Sign Out</a></li>
				</ul></li>
			<!-- user login dropdown end -->
		</ul>
		<!--search & user info end-->
	</div>
		<?php } ?>
</header>
<!--header end-->