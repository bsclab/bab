<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>{{ $title }}</title>
    
    @foreach( Config::get('base::config.cssfiles') as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach

    @foreach( Config::get('layoutbucketadmin::config.cssfiles') as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach
    
    @foreach( $cssFiles as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach

    @section('headerscript')
    @show

</head>
<body>
	<section id="container">
        
        @include('layoutbucketadmin::headermenu')
        @include('layoutbucketadmin::leftsidebar')
        @include('base::partial.waitingmodal')      

        <!--main content start-->
        <section id="main-content">
	        <section class="wrapper">
                
                @yield('maincontent')

			</section>
		</section>
        <!--main content end-->
    	
    </section>
    
    @foreach( Config::get('base::config.jsfiles') as $value)
        <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    @if(!is_null(Config::get('layoutbucketadmin::config.jsfiles')))
        @foreach( Config::get('layoutbucketadmin::config.jsfiles') as $value)
            <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
        @endforeach
    @endif

    @foreach( $jsFiles as $value)
        <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    @section('footerscript')
    @show

</body>
</html>