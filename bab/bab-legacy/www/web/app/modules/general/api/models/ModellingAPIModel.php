<?php namespace App\Modules\General\API\Models;

use Config;

class ModellingAPIModel extends \Eloquent {

	public function getHeuristicModel($resourceId)
	{	
		return file_get_contents( Config::get('api::config.model.heuristic').$resourceId );
	}

	public function postHeuristicModel($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.model.heuristic').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}

	public function getProximityModel($resourceId)
	{	
		return file_get_contents( Config::get('api::config.model.proximity').$resourceId );
	}

	public function getFuzzyModel($resourceId)
	{	
		return file_get_contents( Config::get('api::config.model.fuzzy').$resourceId );
	}

	public function getLogReplayModel($resourceId)
	{	
		return file_get_contents( Config::get('api::config.model.logreplay').$resourceId );
	}

	public function postLogReplayModel($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.model.logreplay').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}
    
    public function getKmeansClustering($resourceId)
	{	
		return file_get_contents( Config::get('api::config.model.kmeans').$resourceId );
	}
	
}