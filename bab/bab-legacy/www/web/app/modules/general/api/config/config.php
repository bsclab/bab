<?php

$debuggerAPI = 'http://localhost/bab/web/debug.php?route=';
$originalAPI = 'http://localhost/api/v1.0/api';
$urlAPI = $originalAPI;
$workspaceId = Session::get('workspaceId') == '' ? 'devel' : Session::get('workspaceId');
$resourceId = Session::get('resourceId');
$userId = Session::get('userId');

return array(
	'job' => array(
		'status' => $urlAPI.'/job/status/',
	),
	'repository'=> array(
        'workspacelist' => $urlAPI.'/repository/workspace/'.$userId,
        'repositorylist' => $urlAPI.'/repository/workspace/'.$workspaceId,
        'repositorydatasetlist' => $urlAPI.'/repository/workspace/dataset/'.$workspaceId,
        'repositorydatasetderivationlist' => $urlAPI.'/repository/workspace/repository/',
        'repositorylistforworkspace' => $urlAPI.'/repository/workspace/',
        'importmxmlgz' => $urlAPI.'/repository/immxmlgz/'.$workspaceId,
        'importmxml' => $urlAPI.'/repository/immxml/'.$workspaceId,
        'importcsv' => $urlAPI.'/repository/imcsv/'.$workspaceId,
        'importxls' => $urlAPI.'/repository/imxls/'.$workspaceId,
        'importxlsx' => $urlAPI.'/repository/imxlsx/'.$workspaceId,
        'import' => $urlAPI.'/repository/workspace/import/'.$workspaceId,
        'view' => $urlAPI.'/repository/view/',
        'delete' => $urlAPI.'/repository/workspace/delete/',
        'viewdataset' => $originalAPI.'/repository/view/dataset/',
		'mapping' => $urlAPI.'/repository/mmap/',
        'summary' => $urlAPI.'/repository/summary/'
    ),
	'model' => array(
        'heuristic' => $urlAPI.'/model/heuristic/',
        'fuzzy'=>$urlAPI.'/model/fuzzy/',
        'logreplay'=>$urlAPI.'/model/logreplay/',
    ),
    'analysis' => array(
        'taskmatrix' => $urlAPI.'/analysis/taskmatrix/',
        'dottedchart' => $urlAPI.'/analysis/dottedchart/',
        'performancechart' => $urlAPI.'/analysis/performancechart/',
        'timegap' => $urlAPI.'/analysis/timegap/',
        'conformancechecking' => $urlAPI.'/analysis/conformancechecking/',
        'associationrule' => $urlAPI.'/analysis/associationrule/',
    ),
);
