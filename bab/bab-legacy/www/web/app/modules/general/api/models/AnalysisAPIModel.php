<?php namespace App\Modules\General\API\Models;

use Config;

class AnalysisAPIModel extends \Eloquent {

	public function getTaskMatrix($resourceId)
	{	
		return file_get_contents( Config::get('api::config.analysis.taskmatrix').$resourceId );
	}
    
    public function postTaskMatrix($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.analysis.taskmatrix').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}


	public function getDottedChart($resourceId, $param=NULL)
	{	
		if (!is_null($param))
            return file_get_contents( Config::get('api::config.analysis.dottedchart').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}


	public function getPerformanceChart($resourceId, $param=NULL)
	{	if (!is_null($param))
            return file_get_contents( Config::get('api::config.analysis.performancechart').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}

	public function getTimeGap($resourceId)
	{	
		return file_get_contents( Config::get('api::config.analysis.timegap').$resourceId );
	}
	
	public function postTimeGap($resourceId, $param=NULL)
	{	
		if(!is_null($param))
			return file_get_contents( Config::get('api::config.analysis.timegap').$resourceId, false, stream_context_create($param));
		else
			return NULL;
	}

	public function getMultidimensionaltimeGap($resourceId, $param=NULL)
	{	
		if(!is_null($param))
			return file_get_contents( Config::get('api::config.analysis.multidimensionaltimegap').$resourceId, false, stream_context_create($param));
		else
			return NULL;
	}

	public function getConformanceChecking()
	{	
		return file_get_contents( Config::get('api::config.analysis.conformancechecking') );
	}

	public function getDeltaAnalysis($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.analysis.delta').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}

	public function getAssociationRule($resourceId, $params=NULL)
	{	
		if (is_null($params))
			return file_get_contents( Config::get('api::config.analysis.associationrule').$resourceId );
		else
			return file_get_contents( Config::get('api::config.analysis.associationrule').$resourceId, false, stream_context_create($params));
	}

	public function getLinearTemporalLogic()
	{	
		return file_get_contents( Config::get('api::config.analysis.lineartemporallogic') );
	}	

	public function getHierarchicalClustering()
	{	
		return file_get_contents( Config::get('api::config.analysis.hierarchicalclustering') );
	}	
    
    public function getSocialNetwork($resourceId)
	{	
		return file_get_contents( Config::get('api::config.analysis.socialnetwork').$resourceId );
	}
    
    public function postSocialNetwork($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.analysis.socialnetwork').$resourceId, false, stream_context_create($param));
        else
            return NULL;
	}

	public function getBayesianNetwork($resourceId)
	{	
		return file_get_contents( Config::get('api::config.analysis.bayesian').$resourceId);
				
	}

	public function postBayesianNetwork($resourceId, $param=NULL )
	{	
		if(!is_null($param)){
			return file_get_contents( Config::get('api::config.analysis.bayesian').$resourceId, false, stream_context_create($param));
		}else{
			return null;
		}
		
	}
}