<div class="modal fade in" id="waiting-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Processing the data ...</h4>
			</div>
			<div class="modal-body">
				<p>Please wait a few seconds ...</p>
				<div class="progress">
					<div id="waiting-modal-progress" class="progress-bar progress-bar-striped active"
						role="progressbar" aria-valuenow="10" aria-valuemin="0"
						aria-valuemax="100" style="width: 10%"></div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->