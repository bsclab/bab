<?php namespace App\Modules\General\Base\Controllers;

// use View, Auth, Redirect, User, Session, Config, BabHelper, Breadcrumbs ;
use View, Config, Session, URL;
use App\Modules\General\API\Models\RepositoryAPIModel;

class BaseController extends \Controller {

	protected $layout;
	protected $model;
	protected $headerscript;
	protected $footerscript;
	protected $repositoryAPIModel;
	
	/**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct()
    {
        // echo BabHelper::displayNumber(7);
    }
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		$this->layout = Config::get('base::config.layout');
		$this->repositoryAPIModel;
		if (!is_null($this->layout))
		{
			// var_dump($this->layout);
			// var_dump($this->cssfiles);
			// var_dump($this->jsfiles);
			$this->layout = View::make($this->layout);

			// if(!is_null($this->cssfiles)){
			// 	$cssList = Config::get($this->cssfiles);
			// 	$this->layout->headerscript = View::make('base::partial.headerscript', compact('cssList'));
			// }
			// if(!is_null($this->jsfiles)){
			// 	$jsList = Config::get($this->jsfiles);
			// 	$this->layout->footerscript = View::make('base::partial.footerscript', compact('jsList'));
			// }
		}
	}
		
	public function trackJobJson($response, $redirectUri = null, $post = false) {
		return $this->trackJob(json_decode($response, true), $redirectUri, $post);
	}
		
	public function trackJob($response, $redirectUri = null, $post = false) {
		$isWaiting = isset($response['jobTrackingId']);
		if ($isWaiting) {
			Session::set('bab.api.job.trackingId', URL::to('/home/status/' . $response['jobTrackingId']));
			if ($redirectUri != null) {
				Session::set('bab.api.job.redirectUri', URL::to($redirectUri));
			}
			if ($post && count($_POST) > 0 != null) {
				Session::set('bab.api.job.postParams', json_encode($_POST));
			}
		}
		return $isWaiting;
	}
}
