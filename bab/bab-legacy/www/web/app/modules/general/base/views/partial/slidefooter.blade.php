<div class="col-md-12">
	<div id="setting-bottom" class="slide-panel slide-bottom weather-2">
		<div class="weather-2-header">
			<div class="row">				
				<div class="col-sm-11 col-xs-11">
					<ul class="nav nav-pills bluer" role="pilllist">
						@yield('slidefootertitle')
					</ul>
				</div>
                <div class="col-sm-1 col-xs-1">
					<a href="#" class="slide-opener go-top">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
	                        <i class="fa fa-angle-down slide-icon slide-icon-active fa-stack-1x"></i> 
	                        <i class="fa fa-angle-up slide-icon slide-icon-inactive fa-stack-1x"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<!-- Tab panes -->
			<div class="tab-content slide-content">@yield('slidefootercontent')</div>
		</div>
	</div>
</div>