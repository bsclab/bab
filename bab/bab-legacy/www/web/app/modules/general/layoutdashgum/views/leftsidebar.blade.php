<?php
$resourceId = null;
if (! is_null ( Session::get ( 'resourceId' ) )) {
    $resourceId = Session::get ( 'resourceId' );
}

if (is_null ( $resourceId )) {
    Menu::handler ( 'main', array (
                        'id' => 'nav-accordion',
                        'class' => 'sidebar-menu') 
        )
        ->add ( 'home/index', '<div class="icon-bab-1 icon-bab-dims">정식</div>
                        <span class="title-en title-home">Home</span>
                        <span class="title-kr"></span>' );
    
} else {
    $arrResourceId = explode('_', trim($resourceId));
    // $repo = substr(substr($resourceId, strpos($resourceId, '_') + 1), 0, -14);
    $repo = substr($arrResourceId[1], 0, -14);
    $workspaceId = Session::get ( 'workspaceId' );
    $user = Sentry::getUser();
    $workspaces = explode(',', $user->workspaces); 
    
    foreach($workspaces as $workspace){
        list ( $uri, $alias ) = explode ( ':', $workspace, 2 );
        if($uri == $workspaceId){
            $workspaceId = $alias;
        }
    }
    
    // gpv,pm,hm,fm,lr,
    // gpc,km,hc,
    // gtv,lc,ar,bn,sn,
    // gfv,tg,dl,
    // gig,tm,dc,pc    

    $menu = Menu::handler ( 'main', array (
            'id' => 'nav-accordion',
            'class' => 'sidebar-menu' 
    ) )
    ->raw ('
            <div class="box1 box-repo-info">
                <span class="li_data"></span>
                <h5>'.$repo.'</h5>
                <span class="bg-info workspaceid showqtip-top" title="Workspace name"><i class="fa fa-bookmark-o"></i> '.$workspaceId.'</span>
            </div>', null, array('class'=>'sidebar-repo'))
    ->add ( 'home/index', '<div class="icon-bab-1 icon-bab-dims">정식</div><span class="title-en title-home">Home</span><span class="title-kr"></span>', 
          null, array('class' => 'mt'))
    ->add ( 'dashboard/index/' . $resourceId, '<div class="icon-bab-2 icon-bab-dims">오곡밥</div>
                                <span class="title-en">Dashboard</span>
                                <span class="title-kr"></span>' );
    
    /* -------------------------- --------------------------- ------------------------------- */
    /* create parent menu process-visualizer*/
    $pvmenu = Menu::items ( 'process-visualizer' );        
    //if (Sentry::getUser()->hasAccess('procVis'))
        $menu->add ( '#', '<div class="icon-bab-3 icon-bab-dims">김밥</div>
                                <span class="title-en">Process Visualizer</span> 
                                <span class="title-kr"></span>', $pvmenu );
    /* create child menu */
    //if (Sentry::getUser()->hasAccess('hm'))
        $pvmenu->add ( 'processvisualizer/heuristicminer/index/' . $resourceId, 'Heuristic Miner' );
    //if (Sentry::getUser()->hasAccess('fm'))
        $pvmenu->add ( 'processvisualizer/fuzzyminer/index/' . $resourceId, 'Fuzzy Miner' );
    //if (Sentry::getUser()->hasAccess('lr'))
        $pvmenu->add ( 'processvisualizer/logreplay/index/' . $resourceId, 'Log Replay' );
    
    /* -------------------------- --------------------------- ------------------------------- */
    /* create parent menu pattern-visualizer*/
    $tvmenu = Menu::items ( 'pattern-visualizer' );
    //if (Sentry::getUser()->hasAccess('pattVis'))
        $menu->add ( '#', '<div class="icon-bab-5 icon-bab-dims">볶음밥</div>
                                <span class="title-en">Pattern Visualizer</span>
                                <span class="title-kr"></span>', $tvmenu );
    /* create child menu */
    //if (Sentry::getUser()->hasAccess('ar'))
        $tvmenu->add ( 'patternvisualizer/associationrule/index/' . $resourceId, 'Association Rule' );
    
    /* -------------------------- --------------------------- ------------------------------- */
    /* create parent menu performance-visualizer*/
    $fvmenu = Menu::items ( 'performance-visualizer' );
    //if (Sentry::getUser()->hasAccess('perfVis'))
        $menu->add ( '#', '<div class="icon-bab-6 icon-bab-dims">비빔밥</div>
                                <span class="title-en">Performance Visualizer</span>
                                <span class="title-kr"></span>', $fvmenu );
    /* create child menu */
    //if (Sentry::getUser()->hasAccess('tg'))
        $fvmenu->add ( 'performancevisualizer/timegap/index/'.$resourceId, 'Time Gap' );
    
    /* -------------------------- --------------------------- ------------------------------- */
    /* create parent menu infographic*/
    $igmenu = Menu::items ( 'infographic' );
    //if (Sentry::getUser()->hasAccess('infoGraph'))
        $menu->add ( '#', '<div class="icon-bab-7 icon-bab-dims">쌈밥</div>
                                <span class="title-en">Infographic</span>
                                <span class="title-kr"></span>', $igmenu );
    /* create child menu */    
    //if (Sentry::getUser()->hasAccess('tm'))
        $igmenu->add ( 'infographic/taskmatrix/index/' . $resourceId, 'Task Matrix' );
    //if (Sentry::getUser()->hasAccess('dc'))
        $igmenu->add ( 'infographic/dottedchart/index/' . $resourceId, 'Dotted Chart' );
    //if (Sentry::getUser()->hasAccess('pc'))
        $igmenu->add ( 'infographic/performancechart/index/' . $resourceId, 'Performance Chart' );
    
}

Menu::handler ( 'main' )->getItemsAtDepth ( 0 )->map ( function ($item) {
    if ($item->hasChildren()) {
        $item->addClass( 'sub-menu' );
        $item->getChildren()->addClass ( 'sub' );
        
        // $item->getContent()->nest('');
    }
} );
?>
<!--*******************************************************************
                    MAIN SIDEBAR MENU
    ******************************************************************* -->
<!--sidebar start-->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">        
        <!-- sidebar menu start-->
        <?php echo Menu::handler('main')->render();?>
        <!-- sidebar menu end-->
        <div class="copyright text-center">
            <small> © <?php date("Y"); ?> PNU IE 
            <br />All Rights Reserved.
            </small>
            <div class="logo-bsclab"></div>
        </div>
    </div>
</aside>
<!--sidebar end-->