<!-- *****************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
************************************************* -->
<?php  ?>
<!--header start-->
<header class="header black-bg green-bg">
    <!--logo start-->
    <a href="{{ url('home') }}" class="logo">
            <div class="logo-logminer"></div>
            <div class="logo-text"></div>
        </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--logo end-->
    <?php $user = Sentry::getUser(); ?>

    @if(!is_null($user))
    <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
        </ul>
        <!--  notification end -->
    </div>

    <div class="nav notify-row login-row pull-right">
        <ul class="nav top-menu">
            <?php
            $workspaces = explode(',', $user->workspaces);
            $current = Session::get ( 'workspaceId' );

            ?>
            <!-- settings start -->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="fa fa-tasks"></i>
                    <span class="badge bg-theme"><?php echo count($workspaces); ?></span>
                </a>
                <ul class="dropdown-menu extended tasks-bar workspace">
                    <div class="notify-arrow notify-arrow-green"></div>
                    <li>
                        <p class="green">Workspaces</p>
                    </li>
                    @foreach($workspaces as $workspace)
                    <?php list ( $uri, $alias ) = explode ( ':', $workspace, 2 ); ?>
                    <li style="<?php if($uri == $current) echo 'background:#ccd1d9;'?>">
                        <a href="{{ url('/home/changeworkspace/'. $uri) }}">
                            <span class="subject">
                                <span class="from">{{ $alias }}</span>
                            </span>
                            <br/>
                            @if ($uri == $current)
                            <span class="message">
                                <i>Current Workspace</i>
                            </span>
                            @endif
                        </a>
                    </li>
                    @endforeach
                </ul>
            </li>
            <!-- settings end -->
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle profile" href="#">
                    <i class="fa fa-user"></i>
                    <!-- <img alt="" src="<?php //echo babModuleAsset('general/base/assets/bab/images/user.png'); ?>">  -->
                    <span class="username"><?php echo $user->first_name; ?></span>
                    <b class="caret pull-right"></b>
                </a>
                <ul class="dropdown-menu extended tasks-bar profile">
                    <li><a href="<?php echo url('profile'); ?>"><i class=" fa fa-suitcase"></i> Profile</a></li>
                    <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                    <li class="dropdown-submenu pull-left">
                        <a tabindex="-1" href="#"><i class="fa fa-cog"></i> Plugin Settings</a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="<?php echo url('plugin/apromore'); ?>"><i class="fa fa-cog"></i> Apromore Settings</a></li>
                            <li><a href="<?php echo url('plugin/apromore'); ?>"><i class="fa fa-cog"></i> Another Settings</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo url('home/logout'); ?>"><i class="fa fa-key"></i> Sign Out</a></li>
                </ul>
            </li>
        </ul>
    </div>
    @endif

</header>
<!--header end-->
