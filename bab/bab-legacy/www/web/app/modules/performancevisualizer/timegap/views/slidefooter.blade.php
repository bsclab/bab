@extends('base::partial.slidefooter') 

@section('slidefootertitle')
<li class="active"><a href="#setting-bottom-overall" role="pill"
	data-toggle="pill"> <i class="fa fa-home"></i><span>Overall</span>
</a></li>
<li><a href="#setting-bottom-detail" role="pill" data-toggle="pill"> <i
		class="fa fa-user"></i><span>Detail</span>
</a></li>
<li><a href="#setting-bottom-settings" role="pill" data-toggle="pill"> <i
		class="fa fa-gears"></i><span>Settings</span>
</a></li>
@stop 

@section('slidefootercontent')
<div class="tab-pane active fade in" id="setting-bottom-overall">
	<!-- this is overall setting -->
	<div class="row">
		<div class="col-sm-12">
			<div class="checkbox">
				<label> <input type="checkbox" id="sCheckBox">Successive Activities
					Only
				</label>
			</div>
			<div class="panel-body">
				<table class="table table-hover table-striped table-condensed cf"
					id="SummaryTable">
					<thead>
						<tr>
							<th class="numeric">#</th>
							<th>From</th>
							<th>To</th>
							<th class="numeric">Mean Time</th>
							<th class="numeric">Std. Deviation Time</th>
							<th class="numeric">PI Count</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="tab-pane fade" id="setting-bottom-detail">
	<!-- this is detail setting -->
	<div class="row">
		<div class="col-sm-6">
			<div class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">From</label>
					<div class="col-sm-9">
						<select class="form-control m-bot-15" id="select-node-source"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">To</label>
					<div class="col-sm-9">
						<select class="form-control m-bot-15" id="select-node-target"></select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<div class="checkbox">
							<label> 
								<input type="checkbox" id="dCheckBox">Successive Activities Only 
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button id="update_detail" name="Update" class="btn btn-info">Update</button>
					</div>
				</div>
			</div>			
		</div>
		<div class="col-sm-6">
			<div class="panel-body">
			{{
				Form::open(array(
							'url' => 'performancevisualizer/timegap/index/'.$resourceId,
							'id'=>'timegap',
							'files'=>true, 
							'method' => 'POST',
							'class'=>'form-horizontal bucket-form')
						)
			}}
				<button type="submit" onCLick="test()" id="analyzeMore" name="analyzeMore" class="btn btn-info">Analyze Selected Rows</button>
				<input type="hidden" name="source" id="source" value="analyzeMore" />
				<input type="hidden" name="astartActivity" id="astartActivity" />
				<input type="hidden" name="aendActivity" id="aendActivity" />
				<input type="hidden" name="caseIds" id="caseIds"/>
			{{ Form::close() }}
			{{ Form::open(array(
							'url' => 'performancevisualizer/multidimensionaltimegap/index/'.$resourceId,
							'id'=>'mtgaId',
							'files'=>true, 
							'method' => 'POST',
							'class'=>'form-horizontal bucket-form')
						) }} 
				<input type="hidden" name="source" id="source" value="tg" />
				<input type="hidden" name="startActivity" id="startActivity" />
				<input type="hidden" name="endActivity" id="endActivity" />
				<input type="hidden" name="caseID" id="caseID" />
				<table class="table table-hover table-striped table-condensed cf" id="DetailTable">
					<thead>
						<tr>
							<th class="numeric">#</th>
							<th>Process Instance</th>
							<th class="numeric">Time Gap</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
<div class="tab-pane fade" id="setting-bottom-settings">
	<!-- this is setting setting -->
	<div class="row">
		<div class="col-sm-12">
			<div class="checkbox">
				<label> <input type="checkbox" id="eCheckBox">Show text on arc
				</label>
			</div>
			<label class="col-sm-6 control-label">Node Shape</label> <select
				class="col-sm-6 form-control m-bot-15">
				<!-- Put options here -->
				<option>Box</option>
				<option>Round</option>
			</select> <label class="col-sm-12 control-label">Node Color</label>
			<button class="btn btn-danger" type="button">
				Orange</span>
		
		</div>
	</div>
</div>
@stop 

