<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="BAB ">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Reset Password</title>

    @foreach( $cssFiles as $value)
    <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">@endforeach

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie8-responsive-file-warning.js') }}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

    <div class="container">
        <div class="brand-login logo-bab-big"></div>
                
        <div id="resetbox" style="margin-top:-80px" class="">
            <div class="form-signin">
                <div style="padding-top:30px" class="login-wrap">
                    
                    <form id="passwordform" class="user-login-info" role="form" data-toggle="validator" method="" action="#" style="min-height: 110px;">
                        <div class="form-group">
                            @if ($success)
                                <div id="info" class="alert alert-success col-sm-12">{{ $messages }}</div>
                            @else
                                <div id="alert" class="alert alert-danger col-sm-12">{{ $messages }}</div>
                            @endif
                        </div>
                        <div class="form-group" id="loginInfo" >
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                <a href="{{ URL::route('babLogin') }}" >Sign in</a> or Don't have an account!
                                <a href="{{ URL::route('babLogin') }}" >Sign Up Here</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="copyright text-center">
            <small> © {{ date("Y") }} 
					<a href="{{ Config::get('sitesetting.author.developer.url') }}"
					target="_blank">
						{{ Config::get('sitesetting.author.developer.fullname') }}</a>. 
						<br />All Right Reserved.
				</small> 
            <div class="footer-logo">
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-pnu"></div>
                </a>
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-bsclab"></div>
                </a>
            </div>
            </a>
        </div>
    </div>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="{{ babModuleAsset('general/base/assets/lib/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ babModuleAsset('general/base/assets/lib/bootstrap3/js/bootstrap.min.js') }}"></script>
     <script src="{{ babModuleAsset('general/base/assets/lib/js/bootstrap-validator/validator.min.js') }}"></script>
    @foreach( $jsFiles as $value)
    <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach
    
</body>

</html>

