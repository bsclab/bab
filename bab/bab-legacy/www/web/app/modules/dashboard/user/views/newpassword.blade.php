<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="BAB ">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Reset Password</title>

    @foreach( $cssFiles as $value)
    <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">@endforeach

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie8-responsive-file-warning.js') }}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

    <div class="container">
        <div class="brand-login logo-bab-big"></div>
                
        <div id="resetbox" style="margin-top:-80px" class="">
            <div class="form-signin">
                <div style="padding-top:30px" class="login-wrap">
                    <div style="display:none" id="alert" class="alert alert-danger col-sm-12"></div>
                    <div style="display:none" id="info" class="alert alert-success col-sm-12"></div>

                    <form id="passwordform" class="user-login-info" role="form" data-toggle="validator" method="" action="#" style="min-height: 110px;">
                        @if ($success)
                        <div class="form-group" id="formbox">
                            <input id="password" type="password" class="form-control" name="password" value="" placeholder="enter new password" required data-minlength="6">
                            <input id="email" type="hidden" class="form-control" name="email" value="{{ $email }}" >
                            <input id="token" type="hidden" class="form-control" name="token" value="{{ $token }}" >
                            <div class="help-block with-errors"></div>
                            <button class="btn btn-lg btn-login btn-block" id="password-button" type="submit" >Submit</button>
                        </div>
                        <div class="form-group" id="loginInfo" style="display:none">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                <a href="{{ URL::route('babLogin') }}" >Sign in</a> or Don't have an account!
                                <a href="{{ URL::route('babLogin') }}" >Sign Up Here</a>
                            </div>
                        </div>
                        @else
                        <div class="form-group">
                            <div id="alert" class="alert alert-danger col-sm-12">{{ $messages }}</div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        
        <div class="copyright text-center">
            <small> © {{ date("Y") }} 
					<a href="{{ Config::get('sitesetting.author.developer.url') }}"
					target="_blank">
						{{ Config::get('sitesetting.author.developer.fullname') }}</a>. 
						<br />All Right Reserved.
				</small> 
            <div class="footer-logo">
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-pnu"></div>
                </a>
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-bsclab"></div>
                </a>
            </div>
            </a>
        </div>
    </div>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="{{ babModuleAsset('general/base/assets/lib/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ babModuleAsset('general/base/assets/lib/bootstrap3/js/bootstrap.min.js') }}"></script>
     <script src="{{ babModuleAsset('general/base/assets/lib/js/bootstrap-validator/validator.min.js') }}"></script>
    @foreach( $jsFiles as $value)
    <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    <script>
        
        /* submit login data */
        $('#passwordform').submit(function(e){            
            var formData = new FormData();
            formData.append('email', $('#email').val());
            formData.append('password', $('#password').val());
            formData.append('token', $('#token').val());
            
            $.ajax({
                type: "POST",
                url: 'newPassword',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if (!data.success){
                        $('#info').hide();
                        $('#loginInfo').hide();
                        $('#alert').html(data.errorMessages).fadeIn(1200);
                    }else{
                        $('#alert').hide();
                        $('#formbox').fadeOut(900);
                        $('#info').html(data.messages).fadeIn(900);
                        $('#loginInfo').fadeIn(1200);
                    }
                },
                error: function(data){
                    console.log(data);
                    window.location = data.url;
                }
            });
            
            return false;
        });
        
    </script>
</body>

</html>

