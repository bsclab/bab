<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="BAB ">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Login</title>

    @foreach( $cssFiles as $value)
    <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">@endforeach

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie8-responsive-file-warning.js') }}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

    <div class="container">
        <div class="brand-login logo-bab-big"></div>
        <!-- Sign in form -->
        <div id="loginbox" style="margin-top:-80px;" class="">
            <div class="form-signin">
                <div style="padding-top:30px" class="login-wrap">
                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <form id="loginform" class="user-login-info" role="form" data-toggle="validator" method="POST" action="#">
                        <div class="form-group">
                            <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email" required data-error="Please enter your username!">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="password" required data-error="Please enter your password!">
                            <div class="help-block with-errors"></div>
                            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
                        </div>
                        <div class="form-group">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                <a href="#" onClick="showReset(0)">Forgot password?</a> or Don't have an account!
                                <a href="#" onClick="showRegister(0)">Sign Up Here</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Request password reset form -->
        <div id="resetbox" style="display:none; margin-top:-80px" class="">
            <div class="form-signin">
                <div style="padding-top:30px" class="login-wrap">
                    <div style="display:none" id="reset-alert" class="alert alert-danger col-sm-12"></div>
                    <div style="display:none" id="reset-info" class="alert alert-success col-sm-12"></div>

                    <form id="resetform" class="user-login-info" role="form" data-toggle="validator" method="" action="#">
                        <div class="form-group">
                            <input id="reset-email" type="email" class="form-control" name="email" value="" placeholder="enter your email" required data-error="Invalid email address">
                            <div class="help-block with-errors"></div>
                            <button class="btn btn-lg btn-login btn-block" id="reset-button" type="submit" style="text-transform: none">Send Password Reset</button>
                        </div>
                        <div class="form-group">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                <a href="#" onClick="showLogin(0)">Sign in</a> or Don't have an account!
                                <a href="#" onClick="showRegister(1)">Sign Up Here</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Sign up form -->
        <div id="signupbox" style="display:none; margin-top:-80px" class="">
            <div class="form-signin">
                <div style="padding-top:30px" class="login-wrap">
                    <div id="message" class="user-login-info" style="display:none; min-height:100px;">
                        <div style="display:none" id="signup-alert" class="alert alert-danger col-sm-12"></div>
                        <div style="display:none" id="signup-info" class="alert alert-success col-sm-12"></div>
                        <div style="padding-top:10px; font-size:85%">
                            <a href="#" onClick="showReset(1)">Forgot password?</a> or Already have an account!
                            <a href="#" onClick="showLogin(1)">Sign In</a>
                        </div>
                    </div>

                    <form id="signupform" class="user-login-info" role="form" data-toggle="validator" method="POST" action="#">
                        <div class="form-group">
                            <input type="text" class="form-control" id="signup-username" name="username" placeholder="Username" required pattern="^[a-z0-9_\.]{8,}$" data-error="Username may only contain lowercase alphanumeric and minimum 8 characters">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="signup-email" name="email" placeholder="Email Address" required data-error="Invalid email address">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="signup-password" name="password" placeholder="Password" required data-minlength="6" data-error="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="signup-first_name" name="first_name" placeholder="Full Name" required pattern="^[a-zA-Z\s]{3,}$" data-error="Name may only contain alpha, spaces and minimum 3 characters">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="signup-affiliation" name="affiliation" placeholder="Affiliation" required pattern="^[a-zA-Z0-9\s]{3,}$" data-error="Affiliation may only contain alphanumeric, spaces and minimum 3 characters">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="signup-country" name="country" placeholder="Country" pattern="^[a-zA-Z\s]$" data-error="" data-provide="typeahead" data-source='{{ $countries }}'>
                            <button class="btn btn-lg btn-login btn-block" type="submit">Sign Up</button>
                        </div>
                        <div class="form-group">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                <a href="#" onClick="showReset(1)">Forgot password?</a> or Already have an account!
                                <a href="#" onClick="showLogin(1)">Sign In</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="copyright text-center">
            <small> © {{ date("Y") }}
					<a href="{{ Config::get('sitesetting.author.developer.url') }}"
					target="_blank">
						{{ Config::get('sitesetting.author.developer.fullname') }}</a>.
						<br />All Right Reserved.
				</small>
            <div class="footer-logo">
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-pnu"></div>
                </a>
                <a href="{{ Config::get('sitesetting.author.parent.url') }}" target="_blank">
                    <div class="logo-bsclab"></div>
                </a>
            </div>
            </a>
        </div>
    </div>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="{{ babModuleAsset('general/base/assets/lib/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ babModuleAsset('general/base/assets/lib/bootstrap3/js/bootstrap.min.js') }}"></script>
    <script src="{{ babModuleAsset('general/base/assets/lib/js/bootstrap-validator/validator.min.js') }}"></script>
    <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-typeahead.js"></script>
    @foreach( $jsFiles as $value)
    <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    <script>
        function showRegister(id) {
            if (id==0)
                $('#loginbox').hide();
            else if (id==1)
                $('#resetbox').hide();

            $('#signupbox').show();
        }

        function showReset(id) {
            if (id==0)
                $('#loginbox').hide();
            else if (id==1)
                $('#signupbox').hide();

            $('#resetbox').show();
        }

        function showLogin(id) {
            if (id==0)
                $('#resetbox').hide();
            else if (id==1)
                $('#signupbox').hide();

            $('#loginbox').show();
        }
        $('.typeahead').typeahead();

        /* submit login data */
        $('#loginform').submit(function(e){
            var formData = new FormData();
            formData.append('username', $('#login-username').val());
            formData.append('password', $('#login-password').val());

            $.ajax({
                type: "POST",
                url: 'login',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if (!data.success){
                        $('#login-alert').html(data.errorMessages).fadeIn(1200);
                    }else{
                        $(location).attr('href', data.url);
                    }
                },
                error: function(data){
                    console.log(data);
                    window.location = data.url;
                }
            });

            return false;
        });

        /* submit login data */
        $('#signupform').submit(function(e){
            var formData = new FormData();
            formData.append('username', $('#signup-username').val());
            formData.append('password', $('#signup-password').val());
            formData.append('email', $('#signup-email').val());
            formData.append('first_name', $('#signup-first_name').val());
            formData.append('affiliation', $('#signup-affiliation').val());
            formData.append('country', $('#signup-country').val());

            $.ajax({
                type: "POST",
                url: 'register',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: "json",
                success: function(data){
                    console.log(formData.get());
                    console.log(data);
                    if (!data.success){
                        $('#signup-info').hide();
                        $('#message').fadeIn(1000);
                        $('#signup-alert').html(data.errorMessages).fadeIn(1200);
                    }else{
                        $('#signup-alert').hide();
                        $('#signupform').fadeOut(900);
                        $('#message').fadeIn(1000);
                        $('#signup-info').html(data.messages).fadeIn(1200);
                    }
                },
                error: function(data){
                    console.log(data);
                    $('#signup-info').html(data.messages).fadeIn(1200);
                }
            });

            return false;
        });

        /* submit reset password data */
        $('#resetform').submit(function(e){
            var formData = new FormData();
            formData.append('email', $('#reset-email').val());
            console.log(formData);

            $.ajax({
                type: "POST",
                url: 'password_request',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: "json",
                success: function(data){

                    $('#reset-email').val('');
                    if (!data.success){
                        $('#reset-info').hide();
                        $('#reset-alert').html(data.errorMessages).fadeIn(1200);
                    }else{
                        $('#reset-alert').hide();
                        $('#reset-info').html(data.messages).fadeIn(1200);
                    }
                },
                error: function(data){
                    console.log(data);
                    $('#reset-info').html(data.messages+'3').fadeIn(1200);
                }
            });

            return false;
        });
    </script>
</body>

</html>
