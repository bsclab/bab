<script type="text/x-handlebars-template" id="table-row-template">
<tr>
	{{#each columns }}
	<td>{{ value }}</td>
	{{/each }}
</tr>
</script>

<script type="text/x-handlebars-template" id="label-template">
	<li class="{{ active }} showqtip-top" title="{{ description }}" data-label='{{ label }}'  >
		<a href="#{{ id }}" class="tab-enabler" data-toggle="tab">
			<i class="{{ icon }}"></i><span>{{ label }}</span>
		</a>
	</li>
</script>

<script type="text/x-handlebars-template" id="content-template">
	<div id="{{ id }}" class="tab-pane {{ active }}">
		<div class="row weather-full-info log-summary-info">
			<div class="col-md-1 today-status"><i class="fa fa-info-circle"></i><h1>Key Info</h1></div>
			<div class="col-md-11">
				<ul class="row">
					{{#each keyInfos}}
					<li>
						<h3 class="showqtip-top" title="{{ description }}">{{ key }}</h3>
						<div class="statistics">{{ label }}</div>
					</li>
					{{/each}}
				</ul>
			</div>
		</div>
		<div class="row log-summary-pills">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					{{#each charts }}
					<li class="{{ active }} showqtip-right" title="{{ description }}">
						<a href="#{{ id }}" class="pill-enabler" data-toggle="pill">{{ label }}</a>
					</li>
					{{/each}}
				</ul>
			</div>
			<div class="col-sm-9">
				<div id="tab-content-overview" class="tab-content">
					{{#each charts }}
					<div id="{{ id }}" class="tab-pane loading-wait {{ active }} {{ type }}">
					    <div class="row">
						    <div id="{{ id }}-chart" class="log-summary-chart col-sm-12">
						    	{{#if renderTabs }}
						    	<div role="tabpanel">
								  	<!-- Nav tabs -->
								  	<ul class="nav nav-tabs" role="tablist">
								    	<li role="presentation" class="active"><a href="#{{ id }}-panel-chart1" role="tab" data-toggle="tab"><i class="fa fa-pie-chart"></i> Pie Chart</a></li>
									    <li role="presentation"><a href="#{{ id }}-panel-chart2" role="tab" data-toggle="tab"><i class="fa fa-bar-chart"></i> Bar Chart</a></li>
								  	</ul>

								  	<!-- Tab panes -->
								  	<div class="tab-content">
									    <div role="tabpanel" id="{{ id }}-panel-chart1" class="tab-pane active">
									    	<div id="{{ id }}-chart-chart1"></div>
									    	<button type="button" class="btn btn-warning button-howtouse" data-toggle="modal" data-target="#howtouse-{{ type }}"><i class="fa fa-info"></i></button>
									    </div>
									    <div role="tabpanel" id="{{ id }}-panel-chart2" class="tab-pane">
									    <div id="{{ id }}-chart-chart2"></div>
									    	<button type="button" class="btn btn-warning button-howtouse" data-toggle="modal" data-target="#howtouse-{{ type }}"><i class="fa fa-info"></i></button>
									    </div>
								  	</div>
								</div>
								{{ else }}
								<div id="{{ id }}-chart-chart1">
								</div>
								<button type="button" class="btn btn-warning button-howtouse" data-toggle="modal" data-target="#howtouse-{{ type }}"><i class="fa fa-info"></i></button>
						    	{{/if }}
						    </div>
						    
						    <div class="col-sm-8 log-summary-statistic ">
						        <h4 class="text-center">Statistics of {{ label }}</h4>
						        <table class="table table-striped table-hover">
						        	<tbody>
						        		{{#each statistics }}
						        		<tr>
						        			<td>{{ label }}</td>
						        			<td>{{ value }}</td>
						        		</tr>
						        		{{/each }}
						        	</tbody>
						        </table>
						    </div>
					    </div>
					</div>
					{{/each }}
				</div>
			</div>
		</div>
		<div class="row log-summary-table">
			<div class="col-md-12">
				<h3 class="text-center col-md-12">{{ table.label }}</h3>
				<table id="{{ table.id }}" class="table table-hover table-striped">
					<thead>
						<tr>
							{{#each table.columns }}
							<td>{{ label }}</td>
							{{/each }}
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</script>