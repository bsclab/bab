<?php namespace App\Modules\Dashboard\LogSummary\Controllers;

use View, BabHelper, Breadcrumbs, Session, Config ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Dashboard\LogSummary\Models\LogSummaryModel;
use App\Modules\General\API\Models\RepositoryAPIModel;

class LogSummaryController extends BaseController {
    
    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(LogsummaryModel $model, RepositoryAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('hasAccess:basic');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Dashboard ';
        $cssFiles = Config::get('logsummary::config.cssfiles');
        $jsFiles = Config::get('logsummary::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }

    public function getIndex($resourceId)
    {
        Session::put('resourceId', $resourceId);        

        $jsonData = array(
            'jsonSummary' => $this->APIModel->getRepositorySummary($resourceId),
            'jsonKeyInfo' => $this->APIModel->getRepositoryStatistics($resourceId)
        );
		$this->trackJobJson($jsonData['jsonSummary']);
        // $isWaiting = BabHelper::isWaitingJsonLoaded(Config::get('base::api.repository.summary'));
        // Breadcrumbs::addCrumb('Dashboard', '');
        // Breadcrumbs::addCrumb('Log ' . $resourceId, '');
        
        $this->layout->maincontent = View::make('logsummary::index', compact('jsonData'));
    }       
}