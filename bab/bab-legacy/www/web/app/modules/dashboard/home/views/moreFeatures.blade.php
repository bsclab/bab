@section('headerscript')
@stop

@section('maincontent')
<div class="row mt"></div>
<div class="row mt">
	<div class="col-md-6 col-md-offset-3 mb">
		<div class="product-panel-2 pn">
            <div class="badge badge-hot">HOT</div>
            <img src="{{ babModuleAsset('general/layoutdashgum/assets/lib/img/product.jpg') }}" width="200" alt="">
            <h5 class="mt">Want more features?</h5>
            <h6>This {{ $feature_name }} feature is not accessible</h6>
            <button class="btn btn-small btn-theme04">Contact Our Support</button>
        </div>
	</div>
</div>

<div class="row mt" style="display:none" id="contact">
    <div class="col-md-4 col-md-offset-4 mb">
        <div class="content-panel pn">
            <div id="profile-01">                
            </div>
            <div class="profile-01 centered">
                <p><i class="fa fa-envelope"></i> bab.bsclab@gmail.com</p>
            </div>
            <div class="centered">
                <h6><i class="fa fa-phone-square"></i><br>Phone : +82-51-510-1482</h6>
            </div>
        </div><!-- --/content-panel ---->
    </div>	
</div>
@stop 

@section('footerscript')
    <script>
        $('.btn-small').on('click', function(){
            $('#contact').slideToggle(900);
        });
    </script>
@stop

