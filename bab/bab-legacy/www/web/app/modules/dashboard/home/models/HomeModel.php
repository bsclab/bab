<?php namespace App\Modules\Dashboard\Home\Models;

use Config;

class HomeModel extends \Eloquent {

	public function getAllRepository()
	{	
		// var_dump(file_get_contents( Config::get('layout::api.repository.all') ));
		return file_get_contents( Config::get('base::api.repository.all') );
	}

}