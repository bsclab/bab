<div id="graph-toolbox" class="col-sm-12">
	<div class="col-sm-2">
		<fieldset>
			<legend>Toolbox</legend>
			<div class="form-group">
				<button id="button-graph-fitwindow" class="btn btn-warning reset-view-graph">
				<i class="fa fa-compress"></i> Scale to fit</button>
			</div>
			<div class="form-group">
				<button id="button-graph-fitactual" class="btn btn-warning reset-view-graph">
				<i class="fa fa-expand"></i> Actual Size</button>
			</div>
		</fieldset>
	</div>
	<div class="col-sm-4">
		<fieldset>
			<legend>Rendering</legend>
			<div class="form-group">
				<div class="input-group">
			      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Change font size">
			      		<i class="fa fa-text-height fa-fw"></i>
			      	</span>
			      	<div class="btn-group btn-group-justified" role="group" aria-label="...">
				      	<a class="btn btn-default" href="#" id="font-inc">A+</a> 
				      	<a class="btn btn-default" href="#" id="font-dec">A-</a>
					</div>
			    </div>
			</div>
			<div class="form-group">
			    <div class="input-group">
			      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Change arc type">
			      		<i class="fa fa-random fa-fw"></i>
			      	</span>
			      	<?php $arcType = array("bundle"=>"Rounded","linear"=>"Linear","cardinal"=>"Linear Smooth","step-before"=>"Rectangular"); ?>
			      	{{ Form::select("arc-type", $arcType, null, array("id"=>"arc-type", "class"=>"form-control")) }}
			    </div>
			</div>
			<div class="form-group">
			    <div class="input-group">
			      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Change arc direction">
			      		<i class="fa fa-sitemap fa-fw"></i>
			      	</span>
			      	<?php $arcDirection = array("TB"=>"Top to Bottom","LR"=>"Left to Right"); ?>
					{{ Form::select("arc-direction", $arcDirection, null, array("id"=>"arc-direction", "class"=>"form-control")) }}
			    </div>
			</div>
		</fieldset>
	</div>
	<div class="col-sm-6">
		<fieldset>
			<legend>Search</legend>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
					    <div class="input-group">
					      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Search node name">
					      		<i class="fa fa-circle-o fa-fw"></i>
					      		<i class="fa fa-font fa-fw"></i>
					      	</span>
					      	{{ Form::text('node_name', '-', array('id'=>'node_name', 'class'=>'form-control')); }}
					    </div>				
					</div>	
					<div class="form-group">
					    <div class="input-group">
					      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Search node frequency">
					      		<i class="fa fa-circle-o fa-fw"></i>
					      		123
					      	</span>
					      	{{ Form::text('node_frequency', '-', array('id'=>'node_frequency', 'class'=>'form-control')); }}
					    </div>				
					</div>		
				</div>
				<div class="col-sm-6">
					<div class="form-group">
					    <div class="input-group">
					      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Search arc frequency">
					      		<i class="fa fa-arrow-right fa-fw"></i>
					      		123
					      	</span>
					      	{{ Form::text('node_name', '-', array('id'=>'node_name', 'class'=>'form-control')); }}
					    </div>				
					</div>	
					<div class="form-group">
					    <div class="input-group">
					      	<span class="input-group-addon tooltips" data-placement="right" data-original-title="Search arc dependency">
					      		<i class="fa fa-arrow-right fa-fw"></i>
					      		0.9
					      	</span>
					      	{{ Form::text('node_frequency', '-', array('id'=>'node_frequency', 'class'=>'form-control')); }}
					    </div>				
					</div>					
				</div>
			</div>
		</fieldset>
	</div>
</div>	