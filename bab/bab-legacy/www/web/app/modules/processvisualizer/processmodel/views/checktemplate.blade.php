@extends('layout::base')

@section('headerscript')
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@stop

@section('sidebar')
	this is sidebar
@stop


@section('maincontent')
	<div id="content-graph" class="col-md-9">
		<h1>this is chart</h1>
		<h2>this is chart</h2>
		{{ printHello('hai!') }}
	</div>
@stop


@section('footerscript')
<script>
	//Load common code that includes config, then load the app logic for this page.
	// require([ "{{ Config::get('sitesetting.assets.requirejs.common')}}" ], function (common) {
	
</script>
@stop