<?php namespace App\Modules\ProcessVisualizer\ProcessModel\Controllers;

use App, Entry, View, Response;

class ProcessModelController extends \BaseController {

	/*
		@author: Dzulfikar Adi Putra
		@email : dzulfikar.adiputra@gmail.com
	*/
	public function getIndex(){
		return View::make('processmodel::index');
	}

	public function getCheckTemplate(){
		return View::make('processmodel::checktemplate');
	}

	public function getTest()
	{
		// return trim('welcome');
		return Response::make('<parent><child>a</child><child>b</child></parent>', 200, ['Content-Type' => 'application/xml']);
	}

}
