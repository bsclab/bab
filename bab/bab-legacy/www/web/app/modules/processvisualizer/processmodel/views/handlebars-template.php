<script type="text/x-handlebars-template" id="node-template">
	<ul class="node-label list-unstyled">
		<li class="name">{{ label }}</li>
		{{#each attributes}}
		<li class="{{key}}">{{ value }}</li>
		{{/each}}
	</ul>
	{{#if isLogReplay}}
	<div class="indicatorlist-2">
	    <div class="indicator-1 bg-success">-</div>
	    <div class="indicator-2 bg-info">-</div>
	</div>
	{{/if}}

	<br/>
</script>

<script type="text/x-handlebars-template" id="node-bayesian">
	<ul class="node-label list-unstyled">
		<li class="name">{{ label }}</li>
		<li>______</li>
		{{#each attributes}}
			<li><small><em>{{ key }}:{{ value }}</em></small></li>
		{{/each}}
	</ul>
	{{#if isLogReplay}}
	<div class="indicatorlist-2">
	    <div class="indicator-1 bg-success">-</div>
	    <div class="indicator-2 bg-info">-</div>
	</div>
	{{/if}}
</script>

<script type="text/x-handlebars-template" id="arc-template">
	<div>
		<ul>
		{{#each attributes}}
			<li class="{{ key }}">{{value}}</li>
		{{/each}}
		</ul>
	</div>
	{{#if isLogReplay}}
	<div class="indicatorlist-2">
	    <div class="indicator-1 bg-success">-</div>
	    <div class="indicator-2 bg-info">-</div>
	</div>
	{{/if}}
</script>

<script type="text/x-handlebars-template" id="tooltip-template">
{{#if isLogReplay}}
	{{#each logReplayData}}
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="text-center" colspan="2">{{ label }}</th>
			</tr>
		</thead>
		<tbody>
			{{#each data}}
			<tr class="{{ key }}">
				<td>{{ label }}</td>
				<td>{{ value }}</td>
			</tr>
			{{/each}}
		</tbody>
	</table>
	{{/each}}
{{/if }}

{{#each data}}
<table class="table table-hover">
	<thead>
		<tr>
			<th class="text-center" colspan="2">{{ label }}</th>
		</tr>
	</thead>
	<tbody>
		{{#each data}}
		<tr class="{{ key }}">
			<td>{{ label }}</td>
			<td>{{ value }}</td>
		</tr>
		{{/each}}
	</tbody>
</table>
{{/each}}
</script>

<script type="text/x-handlebars-template" id="range-template">
<p>{{ label }}</p>
{{#each ranges}}
<div class="legends-box {{class}}">{{ max }}</div>
{{/each}}
</script>
