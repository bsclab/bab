<div id="graph-toolbox" class="col-sm-12">
	<div class="panel panel-success">
		<div class="panel-heading">
            <div class="btn-group">
                <button id="button-graph-fit-window" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Process model fit to window">
                    <i class="fa fa-compress"></i>
                </button>
                <button id="button-graph-fit-actual" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Process model actual size">
                    <i class="fa fa-expand"></i>
                </button>
            </div>
            <div class="btn-group">
                <button id="button-graph-font-inc" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Increase font size">
                    <i class="fa fa-text-height"></i>
                </button>
                <button id="button-graph-font-dec" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Decrease font size">
                    <i class="fa fa-text-width"></i>
                </button>
            </div>
            <div class="btn-group">
                <div class="btn-group open">
                    <a class="btn btn-default disable-in-page" href="#">
                        <i class="fa fa-random fa-fw"></i> Arc Type
                    </a>
                    <a class="btn btn-default dropdown-toggle disable-in-page" data-toggle="dropdown" href="#">
                        <span class="fa fa-caret-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li><a id="button-graph-arctype-rounded" class="disable-in-page" data-arctype="bundle" href="#">Rounded</a></li>
                        <li><a id="button-graph-arctype-linear" class="disable-in-page" data-arctype="linear" href="#">Linear</a></li>
                        <li><a id="button-graph-arctype-linearsmooth" class="disable-in-page" data-arctype="cardinal" href="#">Linear Smooth</a></li>
                        <li><a id="button-graph-arctype-rectangular" class="disable-in-page" data-type="step-before" href="#">Rectangular</a></li>
                    </ul>
                </div>      
            </div>
            <div class="btn-group">
                <div class="btn-group open">
                    <a class="btn btn-default disable-in-page" href="#">
                        <i class="fa fa-sitemap fa-fw"></i> Arc Direction
                    </a>
                    <a class="btn btn-default dropdown-toggle disable-in-page" data-toggle="dropdown" href="#">
                        <span class="fa fa-caret-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li><a id="button-graph-arcdirection-tb" class="disable-in-page" data-arcdirection="TB" href="#">Top to Bottom</a></li>
                        <li><a id="button-graph-arcdirection-lr" class="disable-in-page" data-arcdirection="LR" href="#">Left to Right</a></li>
                    </ul>
                </div>      
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default disable-in-page" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-circle-o"></i> Search Node
                </button>
                <button type="button" class="btn btn-default disable-in-page" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
                    <i class="fa fa-exchange"></i> Search Arc
                </button>
            </div> 
            <div class="btn-group">
                <button id="button-graph-showartificial" type="button" class="btn btn-default btn-success" data-toggle="tooltip" data-placement="top" title="Show artificial nodes">
                    <i class="fa fa-asterisk"></i>
                </button>
            </div>     
        </div>
        <div id="collapseExample" class="collapse form-inline">
            <div class="panel-body" >
                <div class="form-group">
                    <div class="input-group margin-bottom-sm">
                      <span class="input-group-addon"><i class="fa fa-circle-o fa-fw"></i> A</span>
                      <input id="node-name" name="node-name" class="form-control" type="text" placeholder="Node Name">
                    </div>
                    <div class="input-group margin-bottom-sm">
                      <span class="input-group-addon"><i class="fa fa-circle-o fa-fw"></i> 12</span>
                      <input id="node-frequency" name="node-frequency" class="form-control" type="text" placeholder="Node Frequency">
                    </div>
                </div>
            </div>
            
        </div>
        <div id="collapseExample2" class="collapse form-inline" >
            <div class="panel-body">
              <div class="form-group">
                <div class="input-group margin-bottom-sm">
                  <span class="input-group-addon"><i class="fa fa-exchange fa-fw"></i> 123</span>
                  <input id="arc-frequency" name="arc-frequency" class="form-control" type="text" placeholder="Arc Frequency">
                </div>
                <div class="input-group margin-bottom-sm">
                  <span class="input-group-addon"><i class="fa fa-exchange fa-fw"></i> 0.99</span>
                  <input id="arc-dependency" name="arc-dependency" class="form-control" type="text" placeholder="Arc Dependency">
                </div>
              </div>                
            </div>
        </div>
	</div>
</div>