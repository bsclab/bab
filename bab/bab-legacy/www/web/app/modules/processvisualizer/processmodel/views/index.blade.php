@section('maincontent')
	<div id="content" class="row">
		<div class="col-md-12 fixto-container">
			<section class="panel">
				<header class="panel-heading">
					<h3>This is process model</h3>
				</header>
				<div class="panel-body">
					<div class="row">
						<div id="content-graph" class="col-md-9">
							<div id="graph-toolbox" class="col-md-12">
								<button id="button-graph-fitwindow" class="btn btn-warning reset-view-graph">scale to fit</button>
							</div>	
						</div>
						<div class="col-md-2" >
							<div id="range-legends">
								<div id="range-nodes" class="col-md-6"></div>
								<div id="range-arcs" class="col-md-6"></div>
							</div>
							<div id="post_output"></div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	@include('processmodel::snippet-tooltipinfo')
	@include('processmodel::slideside')
@stop


@section('footerscript')
<script type="text/javascript">
	console.log('masuk ke fungsi utama!');
	console.log(_.keys({ 1:'asem', 2:'asem2'}));
	
	var myJson = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': 'http://localhost/experiment-thesis/processmodel/public/files/heuristicExampleSmall.json',
        'dataType': "json",
        'success': function (data) {
            myJson = data;
        }
    });	

    var TheModel = Backbone.Model.extend();

	var theView = new ProcessModelView({
		model : new TheModel({
			jsonProcessModel: myJson, 
			elem: {
				processModel: '#content-graph',
			},
			addon: {
				sidepanel: true
			},
			setting: {
				type: 'heuristic',
				size: {
					width: '100%', 
					height: '80%'
				}
			},
			dagreParam: {
				nodesep: 70,
			    ranksep: 50,
			    rankdir: "TB",
			    marginx: 20,
			    marginy: 20
			}
		})
	});

</script>


@stop