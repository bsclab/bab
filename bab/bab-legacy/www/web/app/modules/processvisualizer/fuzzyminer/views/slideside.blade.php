@extends('base::partial.slideside') 

@section('slidesidetitle') 
Setting Fuzzy Miner 
@stop 

@section('slidesidecontent')
	<div class="panel slide-side-subpanel">
		<div class="panel slide-side-subpanel">
			<div class="row">
				<div class="col-sm-1">
					<a class="slide-opener panel-title" href="#"><i class="fa fa-flask slide-icon"></i></a>
				</div>
				<div class="col-sm-10 slide-content">
					<a href="#setting-side-linkoptions" data-toggle="collapse" class="panel-title">Link Options</a>
					<div class="panel-collapse collapse" id="setting-side-linkoptions">
							{{ BootForm::openHorizontal(6,6) }}
								<?php $arrVal = array('Critical Pattern', 'Longest Execution Time'); ?>
				      			@foreach ($arrVal as $key => $value)
				      				{{ BootForm::checkbox($value, $key) }}
				      			@endforeach
				  			{{ BootForm::close() }}
						</div>
				</div>
			</div>
		</div>
	</div>
@stop
