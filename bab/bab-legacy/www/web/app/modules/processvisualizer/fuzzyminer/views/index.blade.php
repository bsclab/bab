@section ('headerscript')
@stop 

@section('maincontent')
	<div id="content" class="row mt">
		<div class="col-sm-12 fixto-container">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $title }}</h3> 
				</div>
				<div class="panel-body">
					<div class="row">
						<div id="content-graph" class="col-md-10">
							@include('processmodel::snippet-graphtoolbox-simple')
						</div>
						<div class="col-md-2">
							@include('processmodel::snippet-basicprocessmodelinfo')
							<div id="transformer" class="row" style="margin-top:20px">
								<div class="col-md-6">
									<p style="min-height:35px;">Node Filter</p>
									<div id="node-slider" style="height: 200px;"></div>
									<p id="node-filter"></p>
								</div>
								<div class="col-md-6">
									<p style="min-height:35px;">Arc Filter</p>
									<div id="edge-slider" style="height: 200px;"></div>
									<p id="edge-filter"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop 

@section('footerscript')
	@include('processmodel::handlebars-template')
	<script type="text/javascript">
		var jsonData = {{ $jsonData['fuzzyMiner'] }};
		// var jsonDataOriginal = JSON.parse(JSON.stringify( jsonData ));
		
		if(BabHelper.isWaitingJsonLoad(jsonData)){
	       	//BabHelper.setWaiting('#waiting-modal');
		}
		else{
			var model = new ProcessModelModel({
				jsonProcessModel: jsonData, 
				type: 'fuzzy'			
			});

			var theView = new ProcessModelView({
				el: '#content-graph',
				model : model
			});

			$("#node-slider").slider({
				orientation: "vertical",
				min: 0,
				max: 1000,
				range: "min",
				value: 900,
				tooltip: "hide"
			});

			$("#node-slider").on("slideStop", function(ev) {
				$('.ui-slider-handle').html((1000 - ev.value) / 1000);
				$("#node-filter").html((1000 - ev.value) / 1000);
				updateGraph();
			});

			$("#node-filter").html((1000 - $("#node-slider").slider("getValue")) / 1000);

			$("#edge-slider").slider({
				orientation: "vertical",
				min: 0,
				max: 1000,
				range: "min",
				value: 900,
				tooltip: "hide"
			});

			$("#edge-slider").on("slideStop", function(ev) {
				$("#edge-filter").html((1000 - ev.value) / 1000);
				updateGraph();
			});

			$("#edge-filter").html((1000 - $("#edge-slider").slider("getValue")) / 1000);
			updateGraph();

			function updateGraph(){
				var model = {{ $jsonData['fuzzyMiner'] }};
				// var model = jsonDataOriginal;
				var minNode = (1000 - $("#node-slider").slider("getValue")) / 1000;
				// Filter node
				var nodes = {};
				var node_cluster_candidates = {};
				for (var node in model.nodes) {  
					var n = model.nodes[node];
					if (n.significance >= minNode) {
						nodes[node] = n;
					} else {
						var s = n.significance.toFixed(3);
						if (!(s in node_cluster_candidates)) node_cluster_candidates[s] = {};
						node_cluster_candidates[s][node] = n;
					}
				}
				// Cluster nodes
				var node_clusters = {};
				var node_cluster_index = 0;
				var map_node_clusters = {};
				for (var c in node_cluster_candidates) {
					var i = 0;
					var s = "";
					for (var d in node_cluster_candidates[c]) {
						i++;
						s = d;
					}
					if (i > 1) {
						node_clusters[c] = node_cluster_candidates[c];
						var l = "Cluster " + node_cluster_index + " (" + i + ")";
						//var l = "Cluster" + node_cluster_index;
						//*/
						nodes[l] = JSON.parse(JSON.stringify(node_cluster_candidates[c][d]));
						nodes[l].label = l;
						for (var d in node_cluster_candidates[c]) {
							map_node_clusters[d] = l;
						}
						//*/
						node_cluster_index++;
					}
				}
				model.nodes = nodes;
				var arcs = {};
				for (var arc in model.arcs) {
					var a = JSON.parse(JSON.stringify(model.arcs[arc]));
					if (a.source in map_node_clusters) {
						a.source = map_node_clusters[a.source];
					}
					if (a.target in map_node_clusters) {
						a.target = map_node_clusters[a.target];
					}
					var narc = a.source + "|" + a.target;
					if (a.source in nodes && a.target in nodes) {
						arcs[narc] = a;
					}
				}
				model.arcs = arcs;
				// Filter arcs
				var minArc = (1000 - $("#edge-slider").slider("getValue")) / 1000;
				var arcs = {};
				var arc_cluster_candidates = {};
				for (var arc in model.arcs) {
					var a = model.arcs[arc];
					if (a.significance >= minArc) {
						arcs[arc] = a;
					} else {
						var s = a.significance.toFixed(3);
						if (!(s in arc_cluster_candidates)) arc_cluster_candidates[s] = {};
						arc_cluster_candidates[s][arc] = a;
					}
				}
				// Cluster arcs
				var arc_clusters = {};
				var arc_cluster_index = 0;
				var map_arc_clusters = {};
				for (var c in arc_cluster_candidates) {
					var i = 0;
					var s = "";
					for (var d in arc_cluster_candidates[c]) {
						i++;
						s = d;
					}
					if (i > 1) {
						arc_clusters[c] = arc_cluster_candidates[c];
						arc_cluster_index++;
					}
				}
				model.arcs = arcs;
				
				//assign the model, automatic generate new graph by event binding in ProcessModelView
				theView.model.set({jsonProcessModel: model});
				
				$('.vertex-cluster').on("click", function(ev) {
					alert();
				});
			}
		}


	</script>
@stop

