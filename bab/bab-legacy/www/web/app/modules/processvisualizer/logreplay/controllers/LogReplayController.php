<?php namespace App\Modules\ProcessVisualizer\LogReplay\Controllers;

use View, Config, Breadcrumbs, BabHelper;

use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\General\API\Models\ModellingAPIModel;
use App\Modules\General\API\Models\RepositoryAPIModel;
use App\Modules\ProcessVisualizer\LogReplay\Models\LogReplayModel;

class LogReplayController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(LogReplayModel $model, ModellingAPIModel $modellingAPIModel, RepositoryAPIModel $repositoryAPIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:lr');
        
        $this->model = $model;
        $this->modellingAPIModel = $modellingAPIModel;
        $this->repositoryAPIModel = $repositoryAPIModel;

        $title = 'Log Replay ';
        $cssFiles = Config::get('logreplay::config.cssfiles');
        $jsFiles = Config::get('logreplay::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }


    public function getIndex($resourceId)
    {

        $limit = 5000;

        if($limit == 1000){
            $jsonData = array(
                'heuristicMiner' => $this->modellingAPIModel->getHeuristicModel($resourceId),
                'logReplay' => $this->modellingAPIModel->getLogReplayModel($resourceId),
                'logSummaryStatistics' => $this->repositoryAPIModel->getRepositoryStatistics($resourceId)
            );   
        }
        else{
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query(array(
                        'limit' => $limit
                    )),
                )
            );
            $jsonData = array(
                'heuristicMiner' => $this->modellingAPIModel->getHeuristicModel($resourceId),
                'logReplay' => $this->modellingAPIModel->postLogReplayModel($resourceId, $opts),
                'logSummaryStatistics' => $this->repositoryAPIModel->getRepositoryStatistics($resourceId)
            );
        }

        $this->trackJobJson($jsonData['heuristicMiner']);
        $this->trackJobJson($jsonData['logSummaryStatistics']);
        $this->trackJobJson($jsonData['logReplay']);
                
        $this->layout->maincontent =  View::make('logreplay::index', compact('jsonData','limit'));
    }

    public function postIndex($resourceId)
    {   
        if (isset($_POST['limit']))
        {
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query(array(
                        'limit' => $_POST['limit']
                    )),
                )
            );
            $jsonData = array(
                'heuristicMiner' => $this->modellingAPIModel->getHeuristicModel($resourceId),
                'logReplay' => $this->modellingAPIModel->postLogReplayModel($resourceId, $opts),
                'logSummaryStatistics' => $this->repositoryAPIModel->getRepositoryStatistics($resourceId)
            );
            $limit = $_POST['limit'];                
            $this->trackJobJson($jsonData['heuristicMiner']);
            $this->trackJobJson($jsonData['logSummaryStatistics']);
            $this->trackJobJson($jsonData['logReplay']);
            $this->layout->maincontent =  View::make('logreplay::index', compact('jsonData', 'limit'));
        }        
    }
    
}