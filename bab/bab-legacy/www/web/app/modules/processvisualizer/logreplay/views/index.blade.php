@section ('headerscript')
@stop 

@section('maincontent')
<?php 
    $enableGlobalKPI = true;
    $enableNodeArcKPI = false;
    $enableChangeAttribute = false;
    $enableDataPartitionInfo = false;
?>
<div id="content-logreplay" class="row mt">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            <h3>{{ $title }}</h3>
            </div>
            <div class="panel-body" >
                <div class="row" style="height:65%;">
                    <div class="col-md-9">
                        <div id="content-graph" class="col-md-12">
                            @include('processmodel::snippet-graphtoolbox-simple')
                        </div>

                        <div class='walkerController col-md-3'>
                            <div class="player player2"> 
                                <button type="button" id="button_play" class="btn">
                                    <i class="fa fa-play"></i>
                                </button>

                                <button type="button" id="button_stop" class="btn">
                                    <i class="fa fa-stop"></i>
                                </button>

                                <button type="button" id="button_repeat" class="btn">
                                    <i class="fa fa-repeat"></i>
                                </button>
                            </div>	
                            <div class="loading"></div>
                        </div> 

                        <div id="datetime-information" class="col-md-5">
                            <dl class="count">
                                <dd><span class="date">Sun, January 1st 1970</span><br/><span class="time">00:00:00</span></dd>
                            </dl>
                        </div>
                        <div class="col-md-4">
                            Speed
                            <div class="slider_speed">
                                <div class="sliding"></div>
                                <div class="info"></div>
                                <small>* Set speed slider to max value will finish whole animation in 5 minutes.</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            Timeline
                            <div class="slider_timeline">
                                <div class="sliding"></div>
                                <div class="info"></div>
                                <div class="timeline-master"></div>
                                <div id="animation-starttime" class="pull-left"><i class="fa fa-star-o"></i> <span></span></div>
                                <div id="animation-finishtime" class="pull-right"><i class="fa fa-star"></i> <span></span></div>
                            </div>
                            <div id="projected-chart">
                                <div id="projected-events-chart"></div>
                                <div id="notprojected-events-chart"></div>
                                <div id="projected-events-linechart-combined"></div>
                                <div id="projected-events-barchart-combined"></div>
                                <div id="projected-events-linechart-combined-legend" class="text-center"></div>
                            </div>							
                        </div>

                    </div>
                    <div id="statistics" class="col-md-3">
                        <div id="gauge-indicator">
                            <h4>Tokens Running</h4>
                            <div id="gauge-chart-events-running">
                                <div class="progress"></div>
                                <!-- <h5 class="minmax"></h5> -->
                            </div>
                            <h4>Tokens Finished</h4>
                            <div id="gauge-chart-events-finish">
                                <div class="progress"></div>
                                <!-- <h5 class="minmax"></h5> -->
                            </div>
                            <!-- <div id="gauge-chart-animation-completion"></div> -->
                        </div>
                        <hr/>

                        @if($enableDataPartitionInfo)
                        <div style="margin-top:20px;">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#data-partition-info" aria-expanded="false"  aria-controls="data-partition-info">
                              Data Partition Info
                            </button>
                            <!-- <div class="well">INFO :<br/>current setting, amount of tokens per partition is <strong>{{-- $limit --}} tokens</strong></div> -->
                            <div class="modal fade" id="data-partition-info" role="dialog" aria-hidden="true" >
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">Partition Info</div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h4>Partition Settings</h4>
                                                    {{ Form::open(array('method' => 'POST')) }}
                                                        {{ Form::label('limit_label', '#token per partition :') }}<br/>
                                                        {{ Form::select('limit', array('100' => '100', '500' => '500', '1000' => '1000', '5000' => '5000', '10000' => '10000')) }}
                                                        {{ Form::submit('Submit!') }}
                                                    {{ Form::close() }}
                                                </div>
                                                <div class="col-sm-8">
                                                <h4>State</h4>
                                                    <div class="stepwizard">
                                                        <div class="stepwizard-row">
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">NA</button>
                                                                <p>Not Available</p>
                                                            </div>
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">DL</button>
                                                                <p>Download</p>
                                                            </div>
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">ST</button>
                                                                <p>Stored</p>
                                                            </div>
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">IN</button>
                                                                <p>Initialize</p>
                                                            </div>
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">RD</button>
                                                                <p>Ready</p>
                                                            </div>
                                                            <div class="stepwizard-step">
                                                                <button type="button" class="btn btn-default btn-circle">PL</button>
                                                                <p>Play</p>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="well">INFO : <br/>current setting, amount of tokens per partition is <strong>{{ $limit }} tokens</strong></div>
                                            </div>
                                            <table class="table table-hover" id="pm-table" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" rowspan="2">Part</th>
                                                        <th class="text-center" rowspan="2"><i class="fa fa-star"> State</th>
                                                        <th class="text-center" rowspan="2"><i class="fa fa-clock-o"> Start</th>
                                                        <th class="text-center" rowspan="2"><i class="fa fa-clock-o"> Complete</th>
                                                        <th class="text-center" colspan="2"><i class="fa fa-bus"> Number of Tokens</th>
                                                        <th class="text-center" rowspan="2"><i class="fa fa-spinner"></i> Init Time</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center" colspan="2">Projected | Not</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>                                
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        @endif

                        <div id="logreplay-toolbox" class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-info">
                                <div id="logreplay-toolbox-setting-heading" class="panel-heading" role="tab">
                                    <h5><a data-toggle="collapse" data-parent="#logreplay-toolbox" href="#logreplay-toolbox-setting-body" aria-expanded="true" aria-controls="logreplay-toolbox-setting-body">Token Display Setting</a></h5>
                                </div>
                                <div id="logreplay-toolbox-setting-body" class="panel-collapse panel-body collapse in" role="tabpanel" aria-labelledby="logreplay-toolbox-setting-heading">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Same color</label>
                                            <div class="col-sm-7">
                                                <button class="btn btn-default btn-block" id="logreplay-defaultcolor" title="Show same color">Apply</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Show case ID</label>
                                            <div class="col-sm-7">
                                                <button class="btn btn-default btn-block" id="logreplay-showcaseid" title="Show case id">Apply</button>
                                            </div>
                                        </div>
                                        @if($enableChangeAttribute)
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Color by attribute</label>
                                            <div class="col-sm-7">
                                                <select id="logreplay-selectattribute" class="form-control"></select>
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Color by attribute</label>
                                            <div class="col-sm-7">
                                                <select id="logreplay-selectattribute" class="form-control" disabled>
                                                    <option>Case ID</option>
                                                </select>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Enable Token Fade Out</label>
                                            <div class="col-sm-7">
                                                <button class="btn btn-default btn-block" id="logreplay-enabletokenfadeout" title="Enable fade out effect when a token is finished.">Apply</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Enable KPI</label>
                                            <div class="col-sm-7">
                                                @if($enableGlobalKPI)
                                                <button class="btn btn-default btn-block" id="logreplay-enableglobalkpi" title="Enable Global KPI (tokens tunning and tokens finished)">Global KPI</button>
                                                @endif
                                                @if($enableNodeArcKPI)
                                                <button class="btn btn-default btn-block" id="logreplay-enablearcnodekpi" title="Enable Arcs and Nodes KPI">Arcs & Nodes KPI</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-warning">
                                <div id="logreplay-toolbox-legend-heading" class="panel-heading" role="tab">
                                    <h5><a data-toggle="collapse" data-parent="#logreplay-toolbox" href="#logreplay-toolbox-legend-body" aria-expanded="false" aria-controls="logreplay-toolbox-legend-body">Token Colors Legend</a></h5>
                                </div>
                                <div id="logreplay-toolbox-legend-body" class="panel-collapse panel-body collapse" role="tabpanel" aria-labelledby="logreplay-toolbox-legend-heading">
                                    Select attribute(s) to display<br>
                                    Click on color palette to change color
                                    <br><br>
                                    <div class="form">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                <input type="text" id="logreplay-searchattribute" class="form-control" placeholder="attribute name" />
                                            </div>
                                        </div>
                                        <button id="logreplay-hideunselectedattribute" class="btn btn-default">Apply changes</button>
                                    </div>
                                    <br><br>
                                    <ul id="list-attribute-values" class="list-group">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>	    
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center" id="myModalLabel">Initialize Animation</h4>
      </div>
      <div class="modal-body text-center">
        Please wait, animation is still being initialized by system..
        <br/><br/><br/>
        <div class="progress">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
            <span class="sr-only"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@include('processmodel::snippet-tooltipinfo')
@stop 

@section('footerscript')
@include('processmodel::handlebars-template')
@include('logreplay::handlebars-template')

<script type="text/javascript">
	var jsonData = {{ $jsonData['heuristicMiner'] }};
    var eventListHeader = {{ $jsonData['logReplay'] }};
    var logSummaryStatistics = {{ $jsonData['logSummaryStatistics'] }};
    
    if(BabHelper.isWaitingJsonLoad(jsonData) && BabHelper.isWaitingJsonLoad(eventListHeader) && BabHelper.isWaitingJsonLoad(logSummaryStatistics)){
    
    }
    else{
        
        var model = new ProcessModelModel({
            jsonProcessModel: jsonData, 
            type: 'heuristic',
            isLogReplay: false
        });

        var theView = new ProcessModelView({
            el: '#content-graph',
            model : model
        });

		var logReplayModel = new LogReplayModel({
			start               : eventListHeader.startTime, 
			finish              : eventListHeader.endTime,
			totalEvent          : eventListHeader.eventsNumber,
            baseUrl             : "{{ url('hdfs', $parameters = array(), $secure = null) }}?url=",
			minSpeed            : 1,
            currentSpeed        : 1,
            stepSpeedSlider     : 1,
            stepTimelineSlider  : 1,
			jsonEventsHeader    : eventListHeader, 
			jsonProcessModel    : jsonData,
            state               : 'ready',
            tokenAttributes: [
                {name: 'case', show: true, type:'array', options: _.keys(logSummaryStatistics.cases) },
                {name: 'weight', show: false, type:'number'},
                {name: 'vehicle', show: false, type:'array', options: ['VH300T', 'VH600T', 'VH900T', 'VH1200T']},
                {name: 'country', show: false, type:'array', options: ['South Korea', 'Japan', 'Hongkong', 'China', 'US']},
                {name: 'port', show: false, type:'array', options: ['BSN', 'BJN', 'OSK', 'PRL', 'HK', 'GHZ', 'SF', 'LA']}
            ]
        });

        console.log('initial state backbone model');
        
        var logReplayView = new LogReplayView({
            el: '#content-logreplay',
            model: logReplayModel,
            nodeCollection      : theView.nodeCollection,
            arcCollection       : theView.arcCollection,
            nodeViews           : theView.nodeViews,
            arcViews            : theView.arcViews            
		});

        var logReplayToolboxView = new LogReplayToolboxView({
            el: '#logreplay-toolbox',
            model: logReplayModel,
            collection: logReplayView.eventCollection
        });

        var dateTimeInformationView = new DateTimeInformationView({
            model: logReplayModel,
            el: '#datetime-information',
        });

        var projectedChartView = new ProjectedChartView({
            el: '#projected-chart',
            model: logReplayModel
        })

        $('.sidebar-toggle-box .fa-bars').on('click', function(){
            projectedChartView.resizeProjectedCombined();
        });

	}
</script>
@stop