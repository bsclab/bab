@section ('headerscript')
<style media="screen">
    .modal-body .panel-body {
        height: 200px;
        overflow: auto;
    }
    
    .null {
        fill: white !important;
    }
</style>
@stop

@section('maincontent')
<div id="content" class="row mt">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>{{ $title }}</h3>
            </div>
            <div class="panel-body" >
                <div class="col-sm-12">
                    <div class="row well">
                        {{ Form::open(array('action' => array('App\Modules\Infographic\Taskmatrix\Controllers\TaskmatrixController@postIndex', $resourceId ), 'id'=>'heatForm','method' => 'POST', 'class'=>'form-horizontal')) }}

                            <div class="col-md-3">
                                <h4>Row</h4>
                                <div class='settingDiv'>
                                    {{ Form::select('row', $formData['entities'], $formData['row'], array('class'=>'form-control input-sm m-bot15')); }}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h4>Column</h4>
                                <div class='settingDiv'>
                                    {{ Form::select('column', $formData['entities'], $formData['column'], array('class'=>'form-control input-sm m-bot15')); }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h4>Unit</h4>
                                <div class='settingDiv'>
                                    {{ Form::select('unit', $formData['units'], $formData['unit'], array('class'=>'form-control input-sm m-bot15')); }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h4>Order</h4>
                                <div class='settingDiv'>
                                    {{ Form::select('order', $formData['orders'], $formData['order'], array('class'=>'form-control input-sm m-bot15')); }}
                                </div>
                            </div>
                            <div class="col-md-1 pager">
                                <button type="submit" id="update" name="update" class="btn btn-info btn-lg">Update</button>
                            </div>

                        {{ Form::close() }}
                        <button type="button" class="btn btn-warning button-howtouse" data-toggle="modal" data-target="#howtouse-barchart"><i class="fa fa-info"></i></button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-8" >
                        <div class="row">
                            <div class="col-sm-12" id="legend">
                                <small>Relative <span class="unitTitle"></span></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-11">
                                <small>Bad events</small>
                                <div class="progress">
                                    <div id="bad" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        <span id="badLabel"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-11">
                                <small>Neutral events</small>
                                <div class="progress">
                                    <div id="neutral" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        <span id="neutralLabel"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-11">
                                <small>Good events</small>
                                <div class="progress">
                                    <div id="good" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        <span id="goodLabel"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <table class="display table table-striped table-hover">
                            <thead>Statisctic of <span class="unitTitle"></span></thead>
                            <tbody id="statistic">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tooltip" class="hidden" style="">
                    <p><span id="value"></p>
                </div>

                <div class="col-sm-12 pager" id="content-graph" style=""></div>

            </div>

            <!-- Modal -->
            <div class="modal fade in" id="howtouse-barchart" tabindex="-1" role="dialog" aria-labelledby="howtouse-barchart" aria-hidden="true">
              <div class="modal-backdrop fade in" style="height: auto;"></div>
            	<div class="modal-dialog">
            		<div class="modal-content">
            		  	<div class="modal-header">
            		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            		    	<h4 class="modal-title" id="howtouse-barchart">How to choose better options?</h4>
            		  	</div>
            		  	<div class="modal-body">
            		    	<ul class="list-unstyled">
            					<li><i class="fa fa-fw fa-info-circle"></i> Info: put the larger number of attribute as row option to have better visualization</li>
            				</ul>
            		  	</div>
            		</div>
            	</div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Event Evaluation</h4>
                  </div>
                  <div class="modal-body">
                    <form id="movementForm">
                      <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                                <label for="from-name" class="control-label">From:</label>
                                <input type="text" class="form-control" name="from" id="from-name" readonly>
                                <input type="hidden" class="form-control" name="fromHidden" id="from-name" readonly>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">To:</label>
                                <input type="text" class="form-control" name="to" id="to-name" readonly>
                                <input type="hidden" class="form-control" name="toHidden" id="to-name" readonly>
                            </div>
                          </div>
                          <div class="col-sm-6" id="myList">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                      <span class="label label-info"><i class="fa fa-signal"></i> Number of cases: <span id="casesNumber"></span></span>
                                    </h4>
                                </div>
                                <div class="panel-body">
                                    <ul class="list-group">
                                    </ul>
                                </div>
                                <div class="panel-footer"><a href="#" id="download">Download</a></div>
                            </div>                            
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label"><span class="unitTitle"></span>:</label>
                                    <input type="text" class="form-control" name="value" id="to-name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Ratio:</label>
                                    <input type="text" class="form-control" name="ratio" id="to-name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Movement:</label>
                                    <h4 id="badChoice" style="display:none"><span class="label label-danger">Bad event</span></h4>
                                    <h4 id="neutralChoice" style="display:none"><span class="label label-info">Neutral </span></h4>
                                    <h4 id="goodChoice" style="display:none"><span class="label label-success">Good event</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group" data-toggle="buttons" style="display:none">
                            <label id="btnBad" class="btn btn-danger">
                                <input type="radio" name="options" id="option1" value="-1" autocomplete="off" ><i class="fa fa-thumbs-down"></i> Bad Event
                            </label>
                            <label id="btnNeutral" class="btn btn-info active">
                                <input type="radio" name="options" id="option2" value="0" autocomplete="off"><i class="fa fa-circle-o"></i> Neutral
                            </label>
                            <label id="btnGood" class="btn btn-success">
                                <input type="radio" name="options" id="option3" value="1" autocomplete="off"><i class="fa fa-thumbs-up"></i> Good Event
                            </label>
                        </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="dismissModal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveMovement">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<script>
    //var aAPI = "http://localhost/babv2/public/packages/module/app_modules_general/Datacontainer20150607194553_ContainerKMCT_16491c9ea3ea4b48972f3bb1802447b8.tmans";
    var jsonData = {{ $jsonData['taskmatrix'] }};
    // var jdata =
    // $.getJSON( aAPI, {
    //   tags: "mount rainier",
    //   tagmode: "any",
    //   format: "json"
    // }).done(function( data ) { console.log(data); //return data.readyState

    var args = {
            content:"#content-graph",
            baseUrl: "{{ url('hdfs', $parameters = array(), $secure = null) }}?url=",
            margin: {top: 140, right: 100, bottom: 120, left: 180},
            width: 650,
            height: 550,
            sort: {{ $formData['order'] }},
            json: jsonData
    }
    TaskMatrixModel = new TaskMatrixModel(args);
  //});

</script>
@stop
