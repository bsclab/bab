<div class="modal unfade" id="addChart1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close"
                    type="button">×</button>
                <h4 class="modal-title">Setting</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('action' => array('App\Modules\Infographic\Performancechart\Controllers\PerformancechartController@postIndex', $resourceId ), 'id'=>'csv_map','files'=>true, 'method' => 'POST', 'class'=>'form-horizontal bucket-form')) }} 
                
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">Chart Type</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'linechart' => 'Line Chart',
                                    'piechart' => 'Pie Chart',
                                    'radarchart' => 'Radar Chart',
                                );
                        ?>
                        {{ Form::select('chartType', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputchartvisualization"
                        class="col-lg-2 col-sm-2 control-label">Visualization</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'single' => 'Single',
                                    'stack' => 'Stack'
                                );
                        ?>
                        {{ Form::select('visualization', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">X-Axis</label>
                    <div class="col-lg-10">
                        <?php $list = array('time' => 'Time'); ?>
                        {{ Form::select('x', $list, null, array("class"=>"form-control", "disabled")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">Y-Axis</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'Sum' => 'Sum',
                                    'Minimum' => 'Minimum',
                                    'Average' => 'Average',
                                    'Maximum' => 'Maximum',
                                    'Std.Deviation' => 'Std.Deviation'
                                ) 
                        ?>
                        {{ Form::select('y', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">Series</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'Task' => 'Task', 
                                    'Originator' => 'Originator'
                                );
                        ?>
                        {{ Form::select('series', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">Unit</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'seconds' => 'Seconds', 
                                    'minutes' => 'Minutes',
                                    'hours' => 'Hours',
                                    'weeks' => 'Weeks',
                                    'months' => 'Months',
                                    'years' => 'Years'
                                );
                        ?>
                        {{ Form::select('unit', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputchartcalculationmethod"
                        class="col-lg-2 col-sm-2 control-label">Calculation Method</label>
                    <div class="col-lg-10">
                        <?php $list = array(
                                    'workingtime' => 'Working Time', 
                                    'waitingtime' => 'Waiting Time',
                                    'both' => 'Both'
                                );
                        ?>
                        {{ Form::select('calculation', $list, null, array("class"=>"form-control")) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-default" name="submit">OK</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>