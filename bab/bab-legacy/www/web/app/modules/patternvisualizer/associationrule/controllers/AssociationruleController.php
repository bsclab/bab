<?php namespace App\Modules\Patternvisualizer\Associationrule\Controllers;

use View, Config, Breadcrumbs, BabHelper ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Patternvisualizer\Associationrule\Models\AssociationruleModel;
use App\Modules\General\API\Models\AnalysisAPIModel;

class AssociationruleController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(AssociationruleModel $model, AnalysisAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:ar');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Association Rule ';
        $cssFiles = Config::get('associationrule::config.cssfiles');
        $jsFiles = Config::get('associationrule::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }


    public function getIndex($resourceId)
    {
		if (!isset($_POST['noOfRelation'])) $_POST['noOfRelation'] = 0;
		if (!isset($_POST['support'])) $_POST['support'] = 0.2;
		if (!isset($_POST['confidence'])) $_POST['confidence'] = 0.9;
		$config = $_POST;
		$params = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query(array(
					'noOfRelation' 	=> 0,
                ))
            )
        );
		$jsonData = array(
            'model' => $this->APIModel->getAssociationRule($resourceId, $params)
        );
		$this->trackJobJson($jsonData['model']);

        $this->layout->maincontent = View::make('associationrule::index', compact('jsonData', 'config', 'resourceId'));
    }

    public function postIndex($resourceId)
    {
		if (!isset($_POST['noOfRelation'])) $_POST['noOfRelation'] = 0;
		if (!isset($_POST['support'])) $_POST['support'] = 0.2;
		if (!isset($_POST['confidence'])) $_POST['confidence'] = 0.9;
		$config = $_POST;
		$params = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query(array(
					'noOfRelation' 	=> $_POST['noOfRelation'],
					'threshold' => array(
						'support' => array(
							'min' => $_POST['support'],
						),
						'confidence' => array(
							'min' => $_POST['confidence'],
						),
					),
                ))
            )
        );
        $jsonData = array(
            'model' => $this->APIModel->getAssociationRule($resourceId, $params)
        );
		$this->trackJobJson($jsonData['model'], null, true);

        $this->layout->maincontent = View::make('associationrule::index', compact('jsonData', 'config', 'resourceId'));
    }

    public function postFilter($resourceId)
    {
		debug($_POST);
		$params = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query(array(
					'noOfRelation' 	=> 0,
                ))
            )
        );
        $jsonData = array(
            'model' => $this->APIModel->getAssociationRule($resourceId, $params)
        );
		$this->trackJobJson($jsonData['model'], null, true);

        $this->layout->maincontent = View::make('associationrule::index', compact('jsonData', 'resourceId'));
    }
    
}