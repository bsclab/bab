@section ('headerscript')
@stop 
<?php
	$model = json_decode($jsonData['model'], true);
?>

@section('maincontent')
<div class="row mt">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>{{ $title }}</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div id="graph-toolbox" class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<form method="post">
									<div class="btn-group">
										<input type="hidden" name="noOfRelation" value="{{$config['noOfRelation']}}" />
										<?php if (isset($config['noOfRelation'])) { ?>
											<?php if ($config['noOfRelation'] == 0) { ?>
											<button id="id-chart-birdview-button" type="submit" name="noOfRelation" value="1" class="btn btn-success" 
												data-toggle="tooltip" title="Use Activity Relation">
												<i class="fa fa-arrows-alt"></i>
											</button>
											<?php } else { ?>
											<button id="id-chart-birdview-button" type="submit" name="noOfRelation" value="0" class="btn btn-primary active" 
												data-toggle="tooltip" title="Use Activity Only">
												<i class="fa fa-arrows-alt"></i>
											</button>
											<?php } ?>
										<?php } ?>
									</div>
									<div class="btn-group">
										<input name="support" type="text" size="4" value="{{$config['support']}}" class="btn" data-toggle="tooltip" title="Support Threshold" />
										<input name="confidence" type="text" size="4" value="{{$config['confidence']}}" class="btn" data-toggle="tooltip" title="Confidence Threshold" />
										<button type="submit" class="btn btn-primary">
											Update
										</button>
									</div>								
								</form>
							</div>					
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style="overflow: auto;">
						<table class="table table-hover table-striped table-condensed dataTable cf"
							id="SummaryTable">
							<thead>
								<tr>
									<th>IF</th>
									<th>THEN</th>
									<th>Confidence</th>
									<th>Support</th>
									<!--th>Cases</th-->
								</tr>
							</thead>
							<tbody>
							<?php
								if(isset($model['associationRules']) && is_array($model['associationRules'])){
									$ars = $model['associationRules'];
									arsort ( $ars );
									foreach ( $ars as $key => $rule ) {
										?>
								<tr>
									<td><ol><li>{{ implode('</li><li>', $rule['left']) }}</li></ol></td>
									<td><ol><li>{{ implode('</li><li>', $rule['right']) }}</li></ol></td>
									<td>{{ number_format($rule['confidence'] * 100, 2) }} %</td>
									<td>
										<form target="_blank" method="post" action="{{ url('patternvisualizer/associationrule/filter/' . $resourceId) }}">
											<input type="hidden" name="includeLeft"  value="{{ implode('|', $rule['left']) }}" />
											<input type="hidden" name="includeRight" value="{{ implode('|', $rule['right']) }}" />
											<button type="button" class="btn btn-small">{{ number_format($rule['support'] * 100, 2) }} %</button>
										</form>
									</td>
								</tr>
										<?php
									}								
								}
							
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop 

@section('footerscript')

<script type="text/javascript">
	var jsonData = {{ $jsonData['model'] }};

	$('#SummaryTable').DataTable();
	
	if(BabHelper.isWaitingJsonLoad(jsonData)){
		//BabHelper.setWaiting('#waiting-modal');
	}
</script>
@stop

