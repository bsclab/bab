<?php

// View::addNamespace('processvisualizer', app_path().'/modules/processvisualizer/views');
// View::addNamespace('layout', app_path().'/modules/layout/views');

// View::addNamespace('processvisualizer.heuristicminer', app_path().'/modules/processvisualizer/heuristicminer/views');

return array(

	/**
	 * The path that will contain our modules
	 * This can also be an array with multiple paths
	 */
	'path' => array(
				'app/modules/general',
				'app/modules/dashboard',
				'app/modules/processvisualizer',
				'app/modules/processclustering',
				'app/modules/patternvisualizer',
				'app/modules/performancevisualizer',
				'app/modules/infographic',
		),

	/**
	 * If set to 'auto', the modules path will be scanned for modules
	 */
	'mode' => 'manual',

	/**
	 * In case the auto detect mode is disabled, these modules will be loaded
	 * If the mode is set to 'auto', this setting will be discarded
	 */
	'modules' => array(
		'app/modules/general' => array(
			'layoutbucketadmin'	=> array('enabled' => true),
			'layoutdashgum'	=> array('enabled' => true),
			'api'				=> array('enabled' => true),
			'base'				=> array('enabled' => true),
			'webhdfs'				=> array('enabled' => true)
		),
		'app/modules/dashboard' => array(
			'home'			=> array('name'=>'Dashboard', 'enabled' => true),
			'logsummary'	=> array('name'=>'Log Summary', 'enabled' => true),
			'user'=> array('name'=>'User', 'enabled' => true),
		),
		'app/modules/processvisualizer' => array(
			'fuzzyminer'	=> array('name'=>'Fuzzy Miner', 'enabled' => true),
			'heuristicminer'=> array('name'=>'Heuristic Miner', 'enabled' => true),
			'logreplay'		=> array('name'=>'Log Replay', 'enabled' => true),
			'processmodel' 	=> array('name'=>'Process Model', 'enabled' => true),
		),
		'app/modules/patternvisualizer' => array(
			'associationrule' => array('name'=>'Association Rule', 'enabled' => true),
		),
		'app/modules/performancevisualizer' => array(
			'timegap' => array('name'=>'Time Gap', 'enabled' => true),
		),
		'app/modules/infographic' => array(
			'performancechart' => array('name'=>'Performance Chart', 'enabled' => true),
			'dottedchart' => array('name'=>'Dotted Chart', 'enabled' => true),
			'taskmatrix' => array('name'=>'Task Matrix', 'enabled' => true),
		),
	),

	/**
	 * Default files that are included automatically for each module
	 */
	'include' => array(
		'helpers.php',
		'bindings.php',
		'observers.php',
		'filters.php',
		'composers.php',
		'routes.php',
	),

	/**
	 * Debug mode
	 */
	'debug' => false,

);
