<?php
use App\Modules\General\webhdfs\WebHDFS;
use Illuminate\Support\Facades\Response;
//use Illuminate\Contracts\Routing\ResponseFactory;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Queue::getIron()->ssl_verifypeer = false;


View::composer('syntara::layouts.dashboard.master', function($view)
{
    $view->with('siteName', 'BAB User Management');
});


function authFeatures($alias) {
	$user = User::find(Auth::id());
	if ($user == null) return false;
	$features = $user->features;
	if ($features == '*') return true;
	$features = explode(',', $features);
	return in_array($alias, $features);
}

// Route::get('/', 'App\Modules\Dashboard\Home\Controllers\HomeController@getIndex');

Route::get('/', function(){
        return Redirect::to('home/index');
    }
);

Route::get('hdfs', function(){
      $path = Input::get('url');

      if ($path != ""){
        $hdfsHome = 'bab/workspaces/devel2/';
        $hdfs = new WebHDFS('164.125.62.134', '50070', '');
        $arr = str_split($path);
        if ($arr[0] == "/"){
          $arr[0] = "";
          $path = implode("", $arr);
        }

        $response = json_decode($hdfs->open($path), true);
        //$json = json_encode($response);

        return Response::json($response)
            ->header('Content-Type', 'application/json');
        }
    }
);

Route::get('login', array('as'=>'babLogin', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getLogin'));
Route::post('login', array('as'=>'babLogin', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@postLogin'));
Route::post('logout', array('as'=>'babLogout', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getLogout'));

Route::post('register', array('as'=>'register', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@postRegister'));
Route::get('activate/{email}/{token}', array('as'=>'activate', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getActivate'));

Route::post('password_request', array('as'=>'password_request', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@postResetPassword'));
Route::get('password_reset/{email}/{token}', array('as'=>'password_reset', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getNewPassword'));
Route::post('password_reset/{email}/{token}', array('as'=>'password_reset', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@postNewPassword'));

Route::get('profile', array('as'=>'profile', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getProfile'));
Route::get('kosong', array('as'=>'kosong', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@getKosong'));
Route::post('profile', array('as'=>'profile', 'uses'=>'App\Modules\Dashboard\User\Controllers\UserController@postProfile'));

Route::controller('home', 'App\Modules\Dashboard\Home\Controllers\HomeController');
Route::controller('dashboard', 'App\Modules\Dashboard\LogSummary\Controllers\LogSummaryController');

Route::group(
    array('prefix' => 'processvisualizer'),
    function() {
        Route::controller('fuzzyminer', 	'App\Modules\ProcessVisualizer\FuzzyMiner\Controllers\FuzzyMinerController');
        Route::controller('heuristicminer', 'App\Modules\ProcessVisualizer\HeuristicMiner\Controllers\HeuristicMinerController');
        Route::controller('logreplay', 		'App\Modules\ProcessVisualizer\LogReplay\Controllers\LogReplayController');
    }
);

Route::group(
    array('prefix' => 'patternvisualizer'),
    function() {
        Route::controller('associationrule', 	'App\Modules\Patternvisualizer\Associationrule\Controllers\AssociationruleController');
    }
);

Route::group(
	array('prefix' => 'performancevisualizer'),
	function(){
		Route::controller('timegap',                  'App\Modules\Performancevisualizer\Timegap\Controllers\TimegapController');
	}
);

Route::group(
    array('prefix' => 'infographic'),
    function() {
        Route::controller('taskmatrix', 		'App\Modules\Infographic\Taskmatrix\Controllers\TaskmatrixController');
        Route::controller('performancechart', 	'App\Modules\Infographic\Performancechart\Controllers\PerformancechartController');
        Route::controller('dottedchart', 	'App\Modules\Infographic\Dottedchart\Controllers\DottedchartController');
    }
);
