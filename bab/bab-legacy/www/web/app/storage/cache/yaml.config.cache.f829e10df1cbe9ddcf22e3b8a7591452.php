<?php

return array (
  'cssfiles' => 
  array (
    0 => 'processvisualizer/processmodel/assets/css/ProcessModel.css',
  ),
  'jsfiles' => 
  array (
    0 => 'general/base/assets/lib/js/dagred3/dagre-d3.js',
    1 => 'processvisualizer/processmodel/assets/js/models/NodeModel.js',
    2 => 'processvisualizer/processmodel/assets/js/models/ArcModel.js',
    3 => 'processvisualizer/processmodel/assets/js/models/ProcessModelModel.js',
    4 => 'processvisualizer/processmodel/assets/js/collections/NodeCollection.js',
    5 => 'processvisualizer/processmodel/assets/js/collections/ArcCollection.js',
    6 => 'processvisualizer/processmodel/assets/js/views/NodeView.js',
    7 => 'processvisualizer/processmodel/assets/js/views/ArcView.js',
    8 => 'processvisualizer/processmodel/assets/js/views/RangeLegendsView.js',
    9 => 'processvisualizer/processmodel/assets/js/views/ProcessModelView.js',
  ),
);