<?php

return array (
  'cssfiles' => 
  array (
    0 => 'dashboard/logsummary/assets/css/style.css',
    1 => 'general/base/assets/lib/js/jquery-ui/jquery-ui.min.css',
    2 => 'general/base/assets/lib/js/rickshaw/rickshaw.min.css',
    3 => 'general/base/assets/bab/js/rickshaw-bab/rickshaw-bab.css',
    4 => 'general/base/assets/lib/js/c3/c3.min.css',
  ),
  'jsfiles' => 
  array (
    0 => 'general/base/assets/lib/js/jquery-ui/jquery-ui.min.js',
    1 => 'general/base/assets/lib/js/c3/c3.min.js',
    2 => 'general/base/assets/bab/js/dygraph/DyGraphView.js',
    3 => 'general/base/assets/bab/js/chartjs/ChartJSView.js',
    4 => 'dashboard/logsummary/assets/js/views/DashboardTableView.js',
    5 => 'dashboard/logsummary/assets/js/views/DashboardChartView.js',
    6 => 'dashboard/logsummary/assets/js/views/DashboardTabView.js',
  ),
);