<?php

return array (
  'author' => 
  array (
    'developer' => 
    array (
      'name' => 'PNU IE BSC Lab',
      'fullname' => 'Business & Service Computing Laboratory',
      'url' => 'http://bsclab.pusan.ac.kr/',
    ),
    'parent' => 
    array (
      'name' => 'PNU',
      'fullname' => 'Pusan National University',
      'url' => 'http://pusan.ac.kr/',
    ),
  ),
  'plugin' => 
  array (
    'apromore' => 
    array (
      'managerUrl' => 'http://apromore.qut.edu.au/manager/',
      'portalUrl' => 'http://apromore.qut.edu.au/portal/',
    ),
  ),
  'path' => 
  array (
    'uploads_relative' => 'uploads/',
    'uploads_absolute' => '/uploads/',
  ),
  'file_allowed' => 
  array (
    'extension' => 
    array (
      0 => 'mxml',
      1 => 'gz',
      2 => 'xls',
      3 => 'xlsx',
      4 => 'csv',
    ),
    'mime_type' => 
    array (
      0 => 'application/octet-stream',
      1 => 'application/x-gzip',
    ),
  ),
  'csv_map' => 
  array (
    'concept.case' => 'Case Id',
    'concept.event' => 'Event',
    'concept.event.type' => 'Event Type',
    'concept.performer' => 'Performer',
    'concept.time.start:yyyy-mm-dd hh:ii:ss' => 'Start',
    'concept.time.complete:yyyy-mm-dd hh:ii:ss' => 'Complete',
    'concept.originator' => 'Originator',
    'concept.resource' => 'Resource',
  ),
);