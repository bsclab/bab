<?php

return array (
  'cssfiles' => 
  array (
    0 => 'general/base/assets/lib/js/bootstrap-colorpicker/css/colorpicker.css',
    1 => 'general/base/assets/lib/js/bootstrap-switch/bootstrap-switch.min.css',
    2 => 'general/base/assets/lib/js/jquery-qtip/jquery.qtip.min.css',
  ),
  'jsfiles' => 
  array (
    0 => 'general/base/assets/lib/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
    1 => 'general/base/assets/lib/js/bootstrap-switch/bootstrap-switch.min.js',
    2 => 'general/base/assets/lib/js/jquery-qtip/jquery.qtip.min.js',
  ),
);