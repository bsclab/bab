//---------------------------------------------------------------------------------
var RightPanelInform = function() {
	this.name = "null";
	this.min_value = 0;
	this.max_value = 0;
	this.mean_value = 0;
	this.start_time = 0;
	this.end_time = 0;
	this.data_counter = 0;
};

var PanelDataSet = function() {
	this.PanelSet_Case = [];
	this.PanelSet_Task = [];
	this.PanelSet_Origi = [];
};

var RightPanel_DataSet = new PanelDataSet();

var LineData = function() {
	this.task = 0;
	this.case_id = 0;
	this.origi = 0;
	this.time_stamp = 0;
};

var Line = function() {
	this.start = 0;
	this.end = 0;
	this.c_type = 0;
};

var LineSet = function() {
	this.LineCase = [];
	this.LineTask = [];
	this.LineOrigi = [];
};

var LineDataSet = new LineSet();
var LineShow;

var DataForm = function(taskID, task_int, CaseID, case_int, EventCase,
		event_int, TimeStamp, time_int, Dataidx, type) {
	this.task_id = taskID;
	this.case_id = CaseID;
	this.origi_id = EventCase;
	this.time_stamp = TimeStamp;

	this.task_id_int = task_int;
	this.case_id_int = case_int;
	this.origi_id_int = event_int;
	this.time_stamp_int = time_int;

	this.case_color = "#000000";
	this.task_color = "#000000";
	this.origi_color = "#000000";

	this.type = type;

	this.data_idx = Dataidx;
};
var DataForm_elements_compare = function(task1, task2, case1, case2, origi1,
		origi2, time1, time2) {
	this.task_id_min = task1;
	this.task_id_max = task2;
	this.case_id_min = case1;
	this.case_id_max = case2;
	this.origi_id_min = origi1;
	this.origi_id_max = origi2;
	this.timestamp_min = time1;
	this.timestamp_max = time2;

	this.sort_case = 0;
};
var DataForm_detail_inform = function(resourceClass, id, unit, started, ended) {
	this.resourceCLS = resourceClass;
	this.id = id;
	this.unit = unit;
	this.start_unit = started;
	this.end_unit = ended;
};
var Vector = function(x, y) {
	this.x = x;
	this.y = y;
	this.a = 0;
	this.b = 0;
};

// space separation...
var SpaceInform = function(w_size, h_size) {
	this.wSize = w_size;
	this.hSize = h_size;
};
var Space = function(sx, sy, px1, py1, px2, py2) {
	this.spx = sx;
	this.spy = sy;
	this.x1 = px1;
	this.y1 = py1;
	this.x2 = px2;
	this.y2 = py2;

	this.data_case = [];
	this.data_task = [];
	this.data_origi = [];

};
var SpaceGrid, SpaceEditor;

var Colorby_Setting;
var ColorList = [];
var ColorNum;

var Enum = {
	PanelSetting : {
		taskby : 0,
		caseby : 1,
		originatorby : 2
	},
	ScaleMode : {
		linearMode : 0,
		timeMode : 1
	},
	AxisFormat : {
		decimal : 0,
		time : 1
	},
	SortBy : {
		case_id : 0,
		task_id : 1,
		origi_id : 2
	}
}
Object.freeze(Enum);
var ScaleStruct = new Vector();
var AxisStruct = new Vector();

var select_ScaleStruct = new Vector();
var original_ScaleStruct = new Vector();

var Before_sortby;
var Data_compare;
var Data_detail;
var private_data_set = [];
var sorting_data_set = [];
var selected_data = [];
// ------------------------------------------------------------------------------------
var dottedMath = $('#dottedBody');
var flexWidth = dottedMath[0].offsetWidth;
var w = flexWidth - 30;
var h = 600;
var paddingX = 40;
var paddingY = 60;

var origi_w, origi_h;

var xAxisTick_Init_num;
var yAxisTick_Init_num;

var minXAxis, maxXAxis;
var minYAxis, maxYAxis;
minXAxis = maxXAxis = minYAxis = maxYAxis = 0;

var mPointMode;

var task_name_data = [];
var origi_name_data = [];

var xdot = 0, ydot = 0;

// Mouse DataStructure===============================================
var msUP = new Vector();
var msDOWN = new Vector();
var msMOVE = new Vector();

var rangeCheck_x1, rangeCheck_y1, rangeCheck_x2, rangeCheck_y2;
var doClick = false;
var mpx, mpy;
// ==================================================================

var Ctrl_btn = false, Shift_btn = false;
var ctrl_flag = false;
var shift_flag = false;
var shift_st_idx = -1;
var shift_st_y;
var shift_list = [];
var stack_data = []; // ctrl, shift --> private

var cutId;
// ------------------------------------------------------------------------------------
