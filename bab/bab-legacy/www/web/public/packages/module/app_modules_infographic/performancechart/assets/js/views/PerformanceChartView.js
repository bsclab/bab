PerformanceChartView = Backbone.View.extend({
	initialize: function (options) {
		this.options = options || {};
		this.$charts = this.$el.find('#charts');

		var self = this;
		this.$el.find('[name="submit"]').html(function(){
			if(_.isEmpty(self.model.get('post')))
				return 'Add Chart';
			else
				return 'Update Chart';
		});
	},

	events: {
		'change [name="chartType"]': 'disabledVisualization'
	},

	render: function(){
		var self = this; 
		var TheModel = Backbone.Model.extend();

		if(this.model.get('post').chartType == 'linechart'){
			if(this.model.get('post').visualization == 'single'){
				this.model.get('seriesList').forEach(function(value, key){
					template = Handlebars.compile($('#small-chart-template').html());
					self.$charts.append(template({ id: 'chart-'+key, title: value.label }));
					
					// console.log(value.data);
					chartParam = {
						axis:{
							x: {
								label: 'time',
								type: 'timeseries'
							},
							y: {
								label: 'duration',
								type: 'duration'
							}
						}						
					};

					if(chartParam.axis.x.type == 'timeseries'){
						series = _.map(value.data, function(value, key){
							return [moment(value[0]*1000).toDate(), value[1]];
						})
					}
					else{
						series = value.data;						
					}

					BabHelper.drawLineChartDY('#chart-'+key, [series], ['frequency'], chartParam);
				});
			}
			else{
				template = Handlebars.compile($('#big-chart-template').html());
				this.$charts.append(template({ id: 'chart-1', title: post.series }));
				var sortedKeys = [];
				// seriesList = [
				//   { data: [[0,1],[1,2]] },
				//   { data: [[1,4],[2,8]] },
				//   { data: [[2,10],[3,20]] },
				// ]

				this.model.get('seriesList').forEach(function(value, key){
				  key = _.map(value.data, function(value, key){
				    return value[0];
				  })
				  sortedKeys = _.union(sortedKeys, key)
				});

				newList = _.map(sortedKeys, function(value, key){ return {x:value, y:[]} })
				console.log(newList);

				newList.forEach(function(value, key){
				  self.model.get('seriesList').forEach(function(v,k){
				    filtered = _.find(v.data, function(arr){ return arr[0] == value.x });
				    // console.log(filtered)
				    if(_.isUndefined(filtered)){
				      value.y.push(0);
				    }
				    else{
				      value.y.push(filtered[1])
				    }
				  })
				});

				newList = _.map(newList, function(v,k){ return _.flatten([v.x, v.y]) })
				console.log(newList);

				labelSeries = _.map(this.model.get('series'), function(v,k){ return k });

				new DyGraphView({
					el: '#chart-1', 
					model: new TheModel({
						data : newList,
						labelSeries : labelSeries,
						chartParam: {
							visualization: 'stack',
							axis:{
								x: {
									label: 'time',
									type: 'timeseries',
									stringFormat: 'DD/MM/YYYY HH:mm'
								},
								y: {
									label: 'duration',
									type: 'duration', 
								}
							}
						}
					})
				});
			}
		}
		else if(this.model.get('post').chartType == 'radarchart'){
			var canvasWidth = 0.9 * this.$charts[0].clientWidth;
			template = Handlebars.compile($('#big-chart-template').html());
			this.$charts.append(template({ id: 'chart', title: post.series, isCanvas:true, canvasWidth: canvasWidth, canvasHeight: 0.5*canvasWidth }));
			
			var labels = _.map(this.model.get('seriesList'), function(v,k){
				return v.label
			});

			var data = _.map(this.model.get('seriesList'), function(v,k){
				result = 0;
				v.data.forEach(function(val,key){
					result+= val[1];
				});
				return result
			});
			console.log(data);
			
			var chart = new ChartJSView({
				el: '#chart-canvas', 
				model: new TheModel({
					data : [data], 
					labelSeries: labels,
					chartParam: {
						axis:{
							x: {
								label: 'time',
								type: 'timeseries', 
							},
							y: {
								label: 'duration',
								type: 'duration', 
							}
						}
					}
				})
			});
			chart.renderRadarChart();
		}
		else if(this.model.get('post').chartType == 'piechart'){
			template = Handlebars.compile($('#big-chart-template').html());
			this.$charts.append(template({ id: 'chart', title: post.series }));
			
			var colors = randomColor({ count: this.model.get('seriesList').length });

			var data = _.map(this.model.get('seriesList'), function(v,k){
				result = 0;
				v.data.forEach(function(val,key){
					result+= val[1];
				});
				return [v.label, result]
			});
			
			var chart = new C3JSView({
				el: '#chart', 
				model: new TheModel({
					data : data, 
					colors: colors,
					chartParam: {
						axis:{
							x: {
								label: 'time',
								type: 'timeseries', 
							},
							y: {
								label: 'duration',
								type: 'duration', 
							}
						}
					}
				})
			});
			chart.renderPieChart();
		}
	},

	disabledVisualization: function(ev){
		console.log(ev);
		if(ev.currentTarget.value == 'piechart' || ev.currentTarget.value == 'radarchart'){
			this.$el.find('[name="visualization"]').attr('disabled', 'disabled');
		}
		else{
			this.$el.find('[name="visualization"]').removeAttr('disabled');
		}
	}
})