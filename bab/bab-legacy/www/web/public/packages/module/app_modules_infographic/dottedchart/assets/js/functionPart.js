//-----------------------main function part----------------------
function Setting_Init(ref_data, y_sort_by) {
	// 0 case, 1 task, 2 origi

	console.log(ref_data);
	Before_sortby = y_sort_by;
	for ( var key in ref_data.componentAnalysisMap) {
		switch (key) {
		case "event.case":
			ParseHelper(ref_data.componentAnalysisMap[key],
					Enum.SortBy.case_id, true);
			break;
		case "event.activity":
			ParseHelper(ref_data.componentAnalysisMap[key],
					Enum.SortBy.task_id, false);
			break;
		case "event.originator":
			ParseHelper(ref_data.componentAnalysisMap[key],
					Enum.SortBy.origi_id, false);
			break;
		}
	}
	// task label setting===============
	OverlapStringRemove(task_name_data);
	task_1 = 0;
	task_2 = task_name_data.length;
	// =================================
	OverlapStringRemove(origi_name_data);
	origi_1 = 0;
	origi_2 = origi_name_data.length;
	// =================================
	// data id_int setting==============
	for (var i = 0; i < private_data_set.length; i++) {
		for (var k = 0; k < task_name_data.length; k++) {
			if (private_data_set[i].task_id == task_name_data[k]) {
				private_data_set[i].task_id_int = k + 1;
			}
		}
		for (var k = 0; k < origi_name_data.length; k++) {
			if (private_data_set[i].origi_id == origi_name_data[k]) {
				private_data_set[i].origi_id_int = k + 1;
			}
		}
	}

	for (var count = 0; count < 3; count++) {
		var r_cnt;
		if (count == Enum.SortBy.case_id)
			r_cnt = LineDataSet.LineCase.length;
		else if (count == Enum.SortBy.task_id)
			r_cnt = LineDataSet.LineTask.length;
		else if (count == Enum.SortBy.origi_id)
			r_cnt = LineDataSet.LineOrigi.length;

		for (var i = 0; i < r_cnt; i++) {
			for (var k = 0; k < task_name_data.length; k++) {
				if (count == Enum.SortBy.case_id) {
					if (LineDataSet.LineCase[i].start.task == task_name_data[k]) {
						LineDataSet.LineCase[i].start.task = parseInt(k + 1);
					}
					if (LineDataSet.LineCase[i].end.task == task_name_data[k]) {
						LineDataSet.LineCase[i].end.task = parseInt(k + 1);
					}
				} else if (count == Enum.SortBy.task_id) {
					if (LineDataSet.LineTask[i].start.task == task_name_data[k]) {
						LineDataSet.LineTask[i].start.task = parseInt(k + 1);
					}
					if (LineDataSet.LineTask[i].end.task == task_name_data[k]) {
						LineDataSet.LineTask[i].end.task = parseInt(k + 1);
					}
				} else if (count == Enum.SortBy.origi_id) {
					if (LineDataSet.LineOrigi[i].start.task == task_name_data[k]) {
						LineDataSet.LineOrigi[i].start.task = parseInt(k + 1);
					}
					if (LineDataSet.LineOrigi[i].end.task == task_name_data[k]) {
						LineDataSet.LineOrigi[i].end.task = parseInt(k + 1);
					}
				}
			}
			for (var k = 0; k < origi_name_data.length; k++) {
				if (count == Enum.SortBy.case_id) {
					if (LineDataSet.LineCase[i].start.origi == origi_name_data[k]) {
						LineDataSet.LineCase[i].start.origi = parseInt(k + 1);
					}
					if (LineDataSet.LineCase[i].end.origi == origi_name_data[k]) {
						LineDataSet.LineCase[i].end.origi = parseInt(k + 1);
					}
				} else if (count == Enum.SortBy.task_id) {
					if (LineDataSet.LineTask[i].start.origi == origi_name_data[k]) {
						LineDataSet.LineTask[i].start.origi = parseInt(k + 1);
					}
					if (LineDataSet.LineTask[i].end.origi == origi_name_data[k]) {
						LineDataSet.LineTask[i].end.origi = parseInt(k + 1);
					}
				} else if (count == Enum.SortBy.origi_id) {
					if (LineDataSet.LineOrigi[i].start.origi == origi_name_data[k]) {
						LineDataSet.LineOrigi[i].start.origi = parseInt(k + 1);
					}
					if (LineDataSet.LineOrigi[i].end.origi == origi_name_data[k]) {
						LineDataSet.LineOrigi[i].end.origi = parseInt(k + 1);
					}
				}
			}
		}
	}
	// =================================
	if (task_1 <= 0)
		task_1 = 0;
	else
		task_1 -= 1;
	if (case_1 <= 0)
		case_1 = 0;
	else
		case_1 -= 1;
	if (origi_1 <= 0)
		origi_1 = 0;
	else
		origi_1 -= 1;

	Data_compare = new DataForm_elements_compare(task_1, task_2 + 1,
			parseInt(case_1), parseInt(case_2) + 1, origi_1, origi_2 + 1,
			time_1, time_2);
	Data_compare.sort_case = y_sort_by;

	minXAxis = time_1;
	maxXAxis = time_2;
	switch (y_sort_by) { // change enum...
	case Enum.SortBy.case_id:
		minYAxis = parseInt(Data_compare.case_id_min);
		maxYAxis = parseInt(Data_compare.case_id_max);
		break;
	case Enum.SortBy.task_id:
		minYAxis = Data_compare.task_id_min;
		maxYAxis = Data_compare.task_id_max;
		break;
	case Enum.SortBy.origi_id:
		minYAxis = Data_compare.origi_id_min;
		maxYAxis = Data_compare.origi_id_max;
		break;
	}

	MakeColorTable(); // making color table // colorNum 1000

	MousePoint_Mode(0); // select mode //1 : moving mode //init only 0
	LineShow = false;

	// RightPanel Case Option initilize
	RightPanelWrap("cases");

	console.log(private_data_set);
	console.log(LineDataSet);
	console.log(RightPanel_DataSet);
	console.log(Data_compare);
}

// temp.....
var index_counter = 0;
var case_1, case_2;
var task_1, task_2;
var origi_1, origi_2;
var time_1, time_2;

function ParseHelper(ref_data, panel_setting, dot_setting) {
	var trigger_1, trigger_2;
	trigger_1 = trigger_2 = false;

	var type_set;

	var test_counter = 0;

	// 0 case, 1 task, 2 origi
	var overall_infor = new RightPanelInform();
	overall_infor.name = "overall";
	overall_infor.min_value = ref_data.min
	overall_infor.max_value = ref_data.max;
	overall_infor.mean_value = ref_data.mean;
	overall_infor.start_time = ref_data.overallStartTimeStamp;
	overall_infor.end_time = ref_data.overallEndTimeStamp;

	var line_counter = ref_data.lines.length;
	for (var i = 0; i < line_counter; i++) {
		var line_1 = new LineData();
		var line_2 = new LineData();
		try {
			line_1.case_id = parseInt(ref_data.lines[i].first.caseID);
			line_1.task = ref_data.lines[i].first.label;
			line_1.origi = ref_data.lines[i].first.originator;
			line_1.time_stamp = ref_data.lines[i].first.timestamp;
			line_1.type = ref_data.lines[i].first.type;

			line_2.case_id = parseInt(ref_data.lines[i].second.caseID);
			line_2.task = ref_data.lines[i].second.label;
			line_2.origi = ref_data.lines[i].second.originator;
			line_2.time_stamp = ref_data.lines[i].second.timestamp;
			line_2.type = ref_data.lines[i].second.type;
		} catch (e) {
			console.log(i);
		}
		var line = new Line();
		line.start = line_1;
		line.end = line_2;

		switch (panel_setting) {
		case Enum.SortBy.case_id:
			LineDataSet.LineCase.push(line);
			break;
		case Enum.SortBy.task_id:
			LineDataSet.LineTask.push(line);
			break;
		case Enum.SortBy.origi_id:
			LineDataSet.LineOrigi.push(line);
			break;
		}
	}

	var overall_counter = 0;
	var summary = ref_data.componentMap;
	for ( var key2 in summary) {
		overall_counter++;
		var activity_infor = new RightPanelInform();
		activity_infor.name = key2.toString();
		activity_infor.min_value = summary[key2].min;
		activity_infor.max_value = summary[key2].max;
		activity_infor.mean_value = summary[key2].mean;
		activity_infor.start_time = summary[key2].startTimestamp;
		activity_infor.end_time = summary[key2].endTimestamp;
		activity_infor.data_counter = summary[key2].events.length;
		switch (panel_setting) {
		case Enum.SortBy.case_id:
			type_set = "caseN";
			RightPanel_DataSet.PanelSet_Case.push(activity_infor);
			break;
		case Enum.SortBy.task_id:
			type_set = "taskN";
			RightPanel_DataSet.PanelSet_Task.push(activity_infor);
			break;
		case Enum.SortBy.origi_id:
			type_set = "origiN";
			RightPanel_DataSet.PanelSet_Origi.push(activity_infor);
			break;
		}
		// event~
		if (dot_setting) {
			var count = summary[key2].events.length;
			for (var i = 0; i < count; i++) {
				var dot_data = new DataForm(summary[key2].events[i].label, 0,
						parseInt(summary[key2].events[i].caseID),
						parseInt(summary[key2].events[i].caseID),
						summary[key2].events[i].originator, 0,
						summary[key2].events[i].timestamp,
						parseInt(summary[key2].events[i].timestamp),
						index_counter++, summary[key2].events[i].type);

				// case order=============================================
				var case_v = parseInt(summary[key2].events[i].caseID);
				if (!trigger_2) {
					case_1 = case_2 = case_v;
					trigger_2 = true;
				}
				if (case_1 > case_v)
					case_1 = case_v;
				else if (case_2 < case_v)
					case_2 = case_v;
				// =======================================================
				// time order=============================================
				var time_v = parseInt(summary[key2].events[i].timestamp);
				if (!trigger_1) {
					time_1 = time_2 = time_v;
					trigger_1 = true;
				}
				if (time_1 > time_v)
					time_1 = time_v;
				else if (time_2 < time_v)
					time_2 = time_v;
				// =======================================================

				task_name_data.push(summary[key2].events[i].label);
				origi_name_data.push(summary[key2].events[i].originator);
				test_counter++;
				private_data_set.push(dot_data);
			}
		}
	}
	overall_infor.data_counter = overall_counter;
	switch (panel_setting) {
	case Enum.SortBy.case_id:
		RightPanel_DataSet.PanelSet_Case.push(overall_infor);
		break;
	case Enum.SortBy.task_id:
		RightPanel_DataSet.PanelSet_Task.push(overall_infor);
		break;
	case Enum.SortBy.origi_id:
		RightPanel_DataSet.PanelSet_Origi.push(overall_infor);
		break;
	}
	console.log(test_counter);
}

// scale setting...
function Setting_Scale(scale_s, x_scale_mode, y_scale_mode, domain_xmin,
		domain_xmax, domain_ymin, domain_ymax, range_xmin, range_xmax,
		range_ymin, range_ymax) {

	switch (x_scale_mode) {
	case Enum.ScaleMode.linearMode:
		scale_s.x = d3.scale.linear().domain([ domain_xmin, domain_xmax ])
				.range([ range_xmin, range_xmax ]);
		break;
	case Enum.ScaleMode.timeMode:
		scale_s.x = d3.time.scale().domain(
				[ TimeConvert(domain_xmin), TimeConvert(domain_xmax) ])
				.rangeRound([ range_xmin, range_xmax ]);
		break;
	}
	switch (y_scale_mode) {
	case Enum.ScaleMode.linearMode:
		scale_s.y = d3.scale.linear().domain([ domain_ymin, domain_ymax ])
				.range([ range_ymin, range_ymax ]);
		break;
	case Enum.ScaleMode.timeMode:
		break;
	}
}

// Axis setting...
function Setting_Axis(axis_s, x_formatMode, y_formatMode) {
	var xformatSetting, yformatSetting;
	switch (x_formatMode) {
	case Enum.AxisFormat.decimal:
		xformatSetting = d3.format("0d");
		break;
	case Enum.AxisFormat.time:
		// xformatSetting = d3.time.format("%d.%m.%Y.%H.%M.%S");
		xformatSetting = d3.time.format("%Y.%m.%d");
		break;
	}
	switch (y_formatMode) {
	case Enum.AxisFormat.decimal:
		yformatSetting = d3.format("0d");
		break;
	case Enum.AxisFormat.time:
		yformatSetting = d3.time.format("%d.%m.%Y.%H.%M.%S");
		break;
	}
	var tickSize = 5;

	axis_s.x = d3.svg.axis().scale(ScaleStruct.x).orient("top").tickFormat(
			xformatSetting).tickSize(tickSize);
	axis_s.y = d3.svg.axis().scale(ScaleStruct.y).orient("left").tickFormat(
			yformatSetting).tickSize(0);

	switch (Data_compare.sort_case) { // change enum...
	case Enum.SortBy.case_id:
		axis_s.b = Data_compare.case_id_max + 1;
		break;
	case Enum.SortBy.task_id:
		axis_s.b = Data_compare.task_id_max + 1;
		break;
	case Enum.SortBy.origi_id:
		axis_s.b = Data_compare.origi_id_max + 1;
		break;
	}

	var h_size = parseInt(ScaleStruct.y(maxYAxis))
			- parseInt(ScaleStruct.y(minYAxis));
	var h_slice = h_size / 20; // one_slice..
	var temp = h_size / (maxYAxis + 1);
	if (h_slice <= temp) {
		axis_s.b = maxYAxis;
	} else {
		var i = 2;
		while (true) {
			var t1 = maxYAxis - i;
			var t2 = h_size / t1;
			if (t2 >= h_slice) {
				axis_s.b = t1;
				break;
			}
			i += 2;
		}
	}

	// ....loading.....
	axis_s.a = 5;
	var temp_w_tick = (axis_s.a * (w - paddingX)) / w;
	var x_tick_num = temp_w_tick * 1.1;

	xAxisTick_Init_num = x_tick_num;
	yAxisTick_Init_num = axis_s.b;

	axis_s.x.ticks(x_tick_num);
	axis_s.y.ticks(axis_s.b);
}

// Data Draw.....
function Setting_DataDraw() {
	// SVG
	// setting===============================================================
	var div_table_Width = w - 10;
	var div_table_Height = h - 0;
	var WidthPadding_Fix = 30;
	var HeightPadding_Fix = 30;

	var edit = d3.select("#dottedBody").append("div")
			.attr("id", "rappingTable").style("overflow", "hidden").style(
					"width", div_table_Width + "px").style("height",
					div_table_Height + "px").append("svg").attr("width",
					(w - paddingX) + WidthPadding_Fix).attr("height",
					(h - paddingY) + HeightPadding_Fix); // .style("background-color",
	// "rgb(30,30,30)");
	// ============================================================================
	origi_w = d3.select("svg").attr("width");
	origi_h = d3.select("svg").attr("height");

	// guide
	// draw==================================================================
	var xStepWidth = parseInt(ScaleStruct.x(TimeConvert(maxXAxis)))
			- parseInt(ScaleStruct.x(TimeConvert(minXAxis)));
	var yStepHeight = parseInt(ScaleStruct.y(maxYAxis))
			- parseInt(ScaleStruct.y(minYAxis));
	var n = (maxYAxis - minYAxis);

	var rect_h = yStepHeight / n;
	var color1 = "#419AFA";
	var color2 = "#F0F4F7";
	var color_set;
	var sty = ScaleStruct.y((minYAxis + 0.5));

	var edit_order1 = edit.append("g").attr("id", "order_1");
	for (var i = 0; i < n; i++) {
		var t = sty + (rect_h * i);
		if (i % 2 == 0)
			color_set = color1;
		else
			color_set = color2;
		edit_order1.append("rect").attr("x", paddingX).attr("y", t).attr(
				"width", w - paddingX - paddingX).attr("height", rect_h).attr(
				"fill", color_set).attr("opacity", 0.3).attr("id",
				"g_rectX" + i);
	}
	d3.select("#g_rectX" + (n - 1)).remove(); // need..!!

	var edit_order2 = edit.append("g").attr("id", "order_2");
	edit_order2.append("rect").attr("x", paddingX).attr("y", paddingY).attr(
			"width", w - paddingX - paddingX).attr("height", yStepHeight).attr(
			"fill", "#5899ED").attr("opacity", 0.2).attr("id", "g_rect_rap");
	// draw
	// axis==================================================================
	edit.append("g").attr("class", "x axis").attr("id", "x_axis").attr(
			"transform", "translate(0," + 50 + ")").call(AxisStruct.x);
	edit.append("g").attr("class", "y axis").attr("id", "y_axis").attr(
			"transform", "translate(30, 0)").call(AxisStruct.y);

	edit.select("#x_axis").selectAll("text").attr("transform",
			"rotate(-10)translate(0,-10)").style("text-anchor", "middle")
			.style("-webkit-user-select", "none");
	edit.select("#y_axis").selectAll("text").attr("transform",
			"rotate(0)translate(-27,0)").attr("class", "yClass").style(
			"text-anchor", "start").style("cursor", "pointer").style(
			"-webkit-user-select", "none");
	// edit.select("#y_axis").select(".domain").remove();
	// ======================================================================

	var edit_order3 = edit.append("g").attr("id", "order_3");
	edit_order3.selectAll("line x").data(ScaleStruct.x.ticks).enter().append(
			"line").attr("x1", ScaleStruct.x).attr("x2", ScaleStruct.x).attr(
			"y1", paddingY).attr("y2", h - paddingY)
			.attr("stroke-width", "1px").style("stroke", "#fff").attr("id",
					"lineX");

	// grid
	// setting===================================================================
	GridSetting(d3.select("svg").attr("width"), d3.select("svg").attr("height"));
	// ===============================================================================

	// draw
	// draw===========================================================================
	var temp_form = function() {
		this.st_x = 0
		this.st_y_case = 0;
		this.st_y_task = 0;
		this.st_y_origi = 0;
		this.ed_x = 0;
		this.ed_y_case = 0;
		this.ed_y_task = 0;
		this.ed_y_origi = 0;
	};

	var edit_order4 = edit.append("g").attr("id", "order_4").selectAll("line");

	var line_counter, ref_line_data, type;

	for (var cn = 0; cn < 3; cn++) {
		if (cn == Enum.SortBy.case_id) {
			line_counter = LineDataSet.LineCase.length;
			ref_line_data = LineDataSet.LineCase;
			type = "case";
		} else if (cn == Enum.SortBy.task_id) {
			line_counter = LineDataSet.LineTask.length;
			ref_line_data = LineDataSet.LineTask;
			type = "task";
		} else {
			line_counter = LineDataSet.LineOrigi.length;
			ref_line_data = LineDataSet.LineOrigi;
			type = "origi";
		}
		// visibility collapse hidden
		var temp_arr = [];
		for (var i = 0; i < line_counter; i++) {
			var temp = new temp_form();
			temp.st_x = ref_line_data[i].start.time_stamp;
			temp.st_y_case = ref_line_data[i].start.case_id;
			temp.st_y_task = ref_line_data[i].start.task;
			temp.st_y_origi = ref_line_data[i].start.origi;

			temp.ed_x = ref_line_data[i].end.time_stamp;
			temp.ed_y_case = ref_line_data[i].end.case_id;
			temp.ed_y_task = ref_line_data[i].end.task;
			temp.ed_y_origi = ref_line_data[i].end.origi;

			temp_arr.push(temp);
		}

		edit_order4.data(temp_arr).enter().append("line").attr("x1",
				function(e) {
					return ScaleStruct.x(e.st_x);
				}).attr("x2", function(e) {
			return ScaleStruct.x(e.ed_x);
		}).attr("y1", function(e) {
			if (cn == Enum.SortBy.case_id)
				return ScaleStruct.y(e.st_y_case);
			else if (cn == Enum.SortBy.task_id)
				return ScaleStruct.y(e.st_y_task);
			else
				return ScaleStruct.y(e.st_y_origi);
		}).attr("y2", function(e) {
			if (cn == Enum.SortBy.case_id)
				return ScaleStruct.y(e.ed_y_case);
			else if (cn == Enum.SortBy.task_id)
				return ScaleStruct.y(e.ed_y_task);
			else
				return ScaleStruct.y(e.ed_y_origi);
		}).attr("stroke", "#000").attr("id", type + "_work_line").style(
				"visibility", "hidden");
		/*
		 * .style("visibility", function () { if (Data_compare.sort_case == cn)
		 * return "visible"; else return "hidden"; });
		 */
	}

	var edit_order5 = edit.append("g").attr("id", "order_5")
			.selectAll("circle");
	edit_order5.data(private_data_set).enter().append("circle").attr("r", 3)
			.attr("fill", function(e) {
				if (Colorby_Setting == Enum.SortBy.case_id)
					return e.case_color;
				else if (Colorby_Setting == Enum.SortBy.task_id)
					return e.task_color;
				else
					return e.origi_color;
			}).attr("cx", function(e) {
				return ScaleStruct.x(TimeConvert(e.time_stamp_int));
			}).attr("cy", function(e) {
				if (Data_compare.sort_case == Enum.SortBy.case_id)
					return ScaleStruct.y(e.case_id_int);
				else if (Data_compare.sort_case == Enum.SortBy.task_id)
					return ScaleStruct.y(e.task_id_int);
				else
					return ScaleStruct.y(e.origi_id_int);
			}).attr("id", function(e) {
				return "idx" + e.data_idx;
			});
	// ================================================================================

	// mouse
	// area=====================================================================
	var edit_order6 = edit.append("g").attr("id", "order_6");
	edit_order6.append("rect").attr("id", "mouse_area").attr("x", 0).attr("y",
			0).attr("width", 0).attr("height", 0).attr("fill", "#1F7BDE").attr(
			"fill-opacity", 0.4);
	// ==============================
	LineShowTigger(false);

	Re_Setting(0, 0, false);

	wrapFunc();
}

// grid init....
function GridSetting(w2, h2) {
	var SpaceDivide_Debug = false;
	var c1 = "rgb(100,0,0)";
	var c2 = "rgb(100,0,100)";
	var c = c1;
	var idx = 0;
	// ====================================================
	Setting_Scale(original_ScaleStruct, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis, minYAxis, maxYAxis,
			paddingX, w - paddingX, paddingY, h - paddingY);
	Setting_Scale(select_ScaleStruct, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis, minYAxis, maxYAxis,
			paddingX, w - paddingX, paddingY, h - paddingY);

	SpaceEditor = new SpaceInform(18, 18);
	var divide_w = w2 / SpaceEditor.wSize;
	var divide_h = h2 / SpaceEditor.hSize;
	SpaceGrid = new Array(SpaceEditor.hSize);
	for (var i = 0; i < SpaceEditor.hSize; i++) {
		SpaceGrid[i] = new Array(SpaceEditor.wSize);
		for (var k = 0; k < SpaceEditor.wSize; k++) {
			var px1 = divide_w * k;
			var py1 = divide_h * i;
			var px2 = px1 + divide_w;
			var py2 = py1 + divide_h;

			if (SpaceDivide_Debug) {
				if (k % 2 == 0 || i % 2 == 0)
					c = c1;
				else
					c = c2;
				d3.select("svg").append("rect").attr("x", px1).attr("y", py1)
						.attr("width", divide_w).attr("height", divide_h).attr(
								"opacity", 0.5).attr("fill", c).attr("id",
								"pos" + idx++);
			}
			SpaceGrid[i][k] = new Space(k, i, px1, py1, px2, py2);
		}
	}
	// ---------------------------------------------------------------
	var temp_scale_task = new Vector();
	var temp_scale_origi = new Vector();
	Setting_Scale(temp_scale_task, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis,
			Data_compare.task_id_min, Data_compare.task_id_max, paddingX, w
					- paddingX, paddingY, h - paddingY);
	Setting_Scale(temp_scale_origi, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis,
			Data_compare.origi_id_min, Data_compare.origi_id_max, paddingX, w
					- paddingX, paddingY, h - paddingY);

	// ========================================================================
	var color_num = GetColorNum();
	var case_clr = new Array(Data_compare.case_id_max);
	var task_clr = new Array(Data_compare.task_id_max);
	var origi_clr = new Array(Data_compare.origi_id_max);
	for (var i = 0; i < Data_compare.case_id_max; i++)
		case_clr[i] = GetColor(i);
	for (var i = 0; i < Data_compare.task_id_max; i++)
		task_clr[i] = GetColor(i);
	for (var i = 0; i < Data_compare.origi_id_max; i++)
		origi_clr[i] = GetColor(i);

	for (var i = 0; i < private_data_set.length; i++) {
		var xPos = ScaleStruct
				.x(TimeConvert(private_data_set[i].time_stamp_int));
		var yPos_case = ScaleStruct.y(private_data_set[i].case_id_int);
		var yPos_task = temp_scale_task.y(private_data_set[i].task_id_int);
		var yPos_origi = temp_scale_origi.y(private_data_set[i].origi_id_int);

		var temp_w = (SpaceEditor.wSize * xPos) / w2;
		var temp_h_case, temp_h_task, temp_h_origi;

		temp_h_case = (SpaceEditor.hSize * yPos_case) / h2;
		temp_h_task = (SpaceEditor.hSize * yPos_task) / h2;
		temp_h_origi = (SpaceEditor.hSize * yPos_origi) / h2;
		temp_w = parseInt(temp_w);
		temp_h_case = parseInt(temp_h_case);
		temp_h_task = parseInt(temp_h_task);
		temp_h_origi = parseInt(temp_h_origi);

		if (temp_w >= SpaceEditor.wSize)
			temp_w = SpaceEditor.wSize - 1;
		if (temp_h_case >= SpaceEditor.hSize)
			temp_h_case = SpaceEditor.hSize - 1;
		if (temp_h_task >= SpaceEditor.hSize)
			temp_h_task = SpaceEditor.hSize - 1;
		if (temp_h_origi >= SpaceEditor.hSize)
			temp_h_origi = SpaceEditor.hSize - 1;

		SpaceGrid[temp_h_case][temp_w].data_case.push(private_data_set[i]);
		SpaceGrid[temp_h_task][temp_w].data_task.push(private_data_set[i]);
		SpaceGrid[temp_h_origi][temp_w].data_origi.push(private_data_set[i]);
		// =============================================================================
		private_data_set[i].case_color = case_clr[private_data_set[i].case_id_int
				% color_num];
		private_data_set[i].task_color = task_clr[private_data_set[i].task_id_int
				% color_num];
		private_data_set[i].origi_color = origi_clr[private_data_set[i].origi_id_int
				% color_num];
	}
	Colorby_Setting = Enum.SortBy.case_id;

	console.log(SpaceGrid);
}

// space selector
function GridSelector(px, py) {
	// invert -> original scale convert -> search grid position -> good!!
	var inv_px = select_ScaleStruct.x.invert(px);
	var inv_py = select_ScaleStruct.y.invert(py);
	var cov_px = original_ScaleStruct.x(inv_px);
	var cov_py = original_ScaleStruct.y(inv_py);
	var sw = origi_w;
	var sh = origi_h;
	var gpx = (SpaceEditor.wSize * parseInt(cov_px)) / sw;
	var gpy = (SpaceEditor.hSize * parseInt(cov_py)) / sh;
	if (gpx >= SpaceEditor.wSize)
		gpx = SpaceEditor.wSize - 1;
	if (gpy >= SpaceEditor.hSize)
		gpy = SpaceEditor.hSize - 1;
	// console.log("gy : "+ gpy + ", gx : "+ gpx);
	return new Vector(parseInt(gpx), parseInt(gpy));
}
// ==============================================================

// -----------------------sub function part------------------------
// pick data....
function Pick_Data(chk_p1x, chk_p1y, chk_p2x, chk_p2y, pick_mode, pick_y_com) {
	var TempStruct = function(resource_cls, id, unit, unit_start, unit_end,
			time_n, label, taskName, origi) {
		this.resourceCLR = resource_cls;
		this.ID = id;
		this.Unit = unit;
		this.start = unit_start;
		this.end = unit_end;
		this.timeN = time_n;
		this.Label = label;
		this.task_name = taskName;
		this.originator = origi;
	}
	var temp_set = [];
	/*
	 * var resourceCLS = Data_detail.resourceCLS; var id = Data_detail.id; var
	 * unit = Data_detail.unit; var start_unit = Data_detail.start_unit; var
	 * end_unit = Data_detail.end_unit;
	 */
	var g_st = GridSelector(chk_p1x, chk_p1y);
	var g_ed = GridSelector(chk_p2x, chk_p2y);

	var temp_data = [];
	for (var i = g_st.y; i <= g_ed.y; i++) {
		for (var k = g_st.x; k <= g_ed.x; k++) {
			switch (Data_compare.sort_case) {
			case Enum.SortBy.case_id:
				if (SpaceGrid[i][k].data_case.length != 0)
					temp_data.push(SpaceGrid[i][k].data_case);
				break;
			case Enum.SortBy.task_id:
				if (SpaceGrid[i][k].data_task.length != 0)
					temp_data.push(SpaceGrid[i][k].data_task);
				break;
			case Enum.SortBy.origi_id:
				if (SpaceGrid[i][k].data_origi.length != 0)
					temp_data.push(SpaceGrid[i][k].data_origi);
				break;
			}
		}
	}

	if (pick_mode != 2) {
		if (selected_data.length > 0) {
			for (var i = 0; i < selected_data.length; i++) {
				d3.select("#" + selected_data[i]).classed({
					'setStroke' : false
				});
			}
			selected_data = [];
		}
		stack_data = [];
	}

	if (pick_mode == 0) {
		// data pick....
		var inv_p1x = ScaleStruct.x.invert(chk_p1x);
		var inv_p1y = ScaleStruct.y.invert(chk_p1y);
		var inv_p2x = ScaleStruct.x.invert(chk_p2x);
		var inv_p2y = ScaleStruct.y.invert(chk_p2y);

		if (temp_data != null) {
			for (var i = 0; i < temp_data.length; i++) {
				var el_data = temp_data[i];
				for (var k = 0; k < el_data.length; k++) {
					var x_obj = TimeConvert(el_data[k].time_stamp_int);
					var y_ob;
					switch (Data_compare.sort_case) {
					case Enum.SortBy.case_id:
						y_obj = el_data[k].case_id_int;
						break;
					case Enum.SortBy.task_id:
						y_obj = el_data[k].task_id_int;
						break;
					case Enum.SortBy.origi_id:
						y_obj = el_data[k].origi_id_int;
						break;
					}

					if (inv_p1x <= x_obj && inv_p2x >= x_obj
							&& inv_p1y <= y_obj && inv_p2y >= y_obj) {
						var spID = el_data[k].data_idx;
						d3.select("#idx" + spID).classed({
							'setStroke' : true
						});
						selected_data.push(el_data[k].data_idx);
						stack_data.push(new TempStruct("resourceCLS", "id",
								"unit", "start_unit", "end_unit",
								el_data[k].time_stamp_int, el_data[k].case_id,
								el_data[k].task_id, el_data[k].origi_id));
					}

				}
			}
		}
	} else {
		if (temp_data != null) {
			for (var i = 0; i < temp_data.length; i++) {
				var el_data = temp_data[i];
				for (var k = 0; k < el_data.length; k++) {
					var x_obj = TimeConvert(el_data[k].time_stamp_int);
					var y_ob;
					switch (Data_compare.sort_case) {
					case Enum.SortBy.case_id:
						y_obj = el_data[k].case_id_int;
						break;
					case Enum.SortBy.task_id:
						y_obj = el_data[k].task_id_int;
						break;
					case Enum.SortBy.origi_id:
						y_obj = el_data[k].origi_id_int;
						break;
					}
					var x_temp = ScaleStruct.x(x_obj);
					var y_temp = ScaleStruct.y(y_obj);
					chk_p1y = y_temp - 0.5;
					chk_p2y = y_temp + 0.5;
					if (chk_p1x <= x_temp && chk_p2x >= x_temp
							&& chk_p1y <= y_temp && chk_p2y >= y_temp
							&& pick_y_com == y_obj) {
						var spID = el_data[k].data_idx;
						d3.select("#idx" + spID).classed({
							'setStroke' : true
						});
						selected_data.push(el_data[k].data_idx);
						stack_data.push(new TempStruct("resourceCLS", "id",
								"unit", "start_unit", "end_unit",
								el_data[k].time_stamp_int, el_data[k].case_id,
								el_data[k].task_id, el_data[k].origi_id));
					}

				}
			}
		}
	}

	console.log(stack_data);
	// data converting~~~ json~~~>>>>
	return stack_data;
}

// Re-Setting
function Re_Setting(xSize, ySize, do_sort) {
	var strech_force = 100;
	var xScaleEx, yScaleEx;

	if (!do_sort) {
		xScaleEx = (w - paddingX) + (xSize * strech_force);
		yScaleEx = (h - paddingY) + (ySize * strech_force);
	} else {
		for (var i = 0; i < (maxYAxis - minYAxis); i++)
			d3.select("#g_rectX" + i).remove();
		// =======================================================================
		xScaleEx = (w - paddingX);
		yScaleEx = (h - paddingY);
		switch (Data_compare.sort_case) {
		case Enum.SortBy.case_id:
			minYAxis = Data_compare.case_id_min;
			maxYAxis = Data_compare.case_id_max;
			break;
		case Enum.SortBy.task_id:
			minYAxis = Data_compare.task_id_min;
			maxYAxis = Data_compare.task_id_max;
			break;
		case Enum.SortBy.origi_id:
			minYAxis = Data_compare.origi_id_min;
			maxYAxis = Data_compare.origi_id_max;
			break;
		}
		// ===================================
		var h_size = parseInt(ScaleStruct.y(maxYAxis))
				- parseInt(ScaleStruct.y(minYAxis));
		var h_slice = h_size / 20; // one_slice..
		var temp = h_size / (maxYAxis + 1);
		if (h_slice <= temp) {
			AxisStruct.b = maxYAxis;
		} else {
			var i = 2;
			while (true) {
				var t1 = maxYAxis - i;
				var t2 = h_size / t1;
				if (t2 >= h_slice) {
					AxisStruct.b = t1;
					break;
				}
				i += 2;
			}
		}
		// ======================================
	}

	Setting_Scale(ScaleStruct, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis, minYAxis, maxYAxis,
			paddingX, xScaleEx, paddingY, yScaleEx);
	Setting_Scale(select_ScaleStruct, Enum.ScaleMode.timeMode,
			Enum.ScaleMode.linearMode, minXAxis, maxXAxis,
			Data_compare.case_id_min, Data_compare.case_id_max, paddingX,
			xScaleEx, paddingY, yScaleEx); // for grid select

	if (do_sort)
		Setting_Axis(AxisStruct, Enum.AxisFormat.time, Enum.AxisFormat.decimal);

	var xScaleEx_Padding = 30;
	var yScaleEx_Padding = 30; // fix....
	var resizeWidth = xScaleEx + xScaleEx_Padding;
	var resizeHeight = yScaleEx + yScaleEx_Padding;

	if (xSize == 0)
		d3.select("#rappingTable").style("overflow-x", "hidden");
	else
		d3.select("#rappingTable").style("overflow-x", "auto");
	if (ySize == 0)
		d3.select("#rappingTable").style("overflow-y", "hidden");
	else
		d3.select("#rappingTable").style("overflow-y", "auto");

	d3.select("svg").attr("width", resizeWidth).attr("height", resizeHeight);

	// rect draw-----------------------------------------
	var yStepHeight = parseFloat(ScaleStruct.y(maxYAxis))
			- parseFloat(ScaleStruct.y(minYAxis));
	var n = (maxYAxis - minYAxis);
	var rect_h = yStepHeight / n;
	var sty = ScaleStruct.y((minYAxis + 0.5));

	if (do_sort) {
		var temp_svg = d3.select("#order_1");
		var color1 = "#419AFA";
		var color2 = "#F0F4F7";
		for (var i = 0; i < n; i++) {
			var t = sty + (rect_h * i);
			if (i % 2 == 0)
				color_set = color1;
			else
				color_set = color2;
			temp_svg.append("rect").attr("x", paddingX).attr("y", t).attr(
					"width", resizeWidth - paddingX - paddingX + 10).attr(
					"height", 0).attr("fill", color_set).attr("opacity", 0.4)
					.attr("id", "g_rectX" + i);
		}
		d3.select("#g_rectX" + (n - 1)).remove();
	}

	d3.selectAll("#g_rect_rap").transition().duration(1000).attr("height",
			yStepHeight).attr("width", resizeWidth - paddingX - paddingX + 10);

	for (var i = 0; i < parseInt(n); i++) {
		d3.selectAll("#g_rectX" + i).transition().duration(1000).attr("width",
				resizeWidth - paddingX - paddingX + 10).attr("height", rect_h)
				.attr("y", sty + (rect_h * i));
	}
	// grid draw-----------------------------------------
	d3.selectAll("#lineX").transition().duration(1000)
			.attr("x1", ScaleStruct.x).attr("x2", ScaleStruct.x).attr("y1",
					paddingY).attr("y2", resizeHeight - paddingY + 30).attr(
					"stroke-width", "1px").style("stroke", "#fff");
	// --------------------------------------------------

	d3.selectAll("circle").transition().duration(200).attr("cx", function(e) {
		return ScaleStruct.x(TimeConvert(e.time_stamp_int));
	}).attr("cy", function(e) {
		if (Data_compare.sort_case == Enum.SortBy.case_id)
			return ScaleStruct.y(e.case_id_int);
		else if (Data_compare.sort_case == Enum.SortBy.task_id)
			return ScaleStruct.y(e.task_id_int);
		else
			return ScaleStruct.y(e.origi_id_int);
	});

	var type;
	// Before_sortby
	if (Before_sortby == Enum.SortBy.case_id)
		type = "case";
	else if (Before_sortby == Enum.SortBy.task_id)
		type = "task";
	else
		type = "origi";

	d3.selectAll("#" + type + "_work_line").style("visibility", "hidden");

	if (Data_compare.sort_case == Enum.SortBy.case_id)
		type = "case";
	else if (Data_compare.sort_case == Enum.SortBy.task_id)
		type = "task";
	else
		type = "origi";

	d3.selectAll("#" + type + "_work_line").transition().duration(200).attr(
			"x1", function(e) {
				return ScaleStruct.x(e.st_x);
			}).attr("x2", function(e) {
		return ScaleStruct.x(e.ed_x);
	}).attr("y1", function(e) {
		if (Data_compare.sort_case == Enum.SortBy.case_id)
			return ScaleStruct.y(e.st_y_case);
		else if (Data_compare.sort_case == Enum.SortBy.task_id)
			return ScaleStruct.y(e.st_y_task);
		else
			return ScaleStruct.y(e.st_y_origi)
	}).attr("y2", function(e) {
		if (Data_compare.sort_case == Enum.SortBy.case_id)
			return ScaleStruct.y(e.ed_y_case);
		else if (Data_compare.sort_case == Enum.SortBy.task_id)
			return ScaleStruct.y(e.ed_y_task);
		else
			return ScaleStruct.y(e.ed_y_origi)
	});
	if (LineShow)
		LineShowTigger(true);
	else
		LineShowTigger(false);

	AxisStruct.x.scale(ScaleStruct.x);
	AxisStruct.y.scale(ScaleStruct.y);

	// ....loading.....
	var temp_w_tick = (AxisStruct.a * resizeWidth) / w;
	var temp_h_tick = (AxisStruct.b * resizeHeight) / h;
	var x_tick_num = temp_w_tick * 1.4;
	var y_tick_num = temp_h_tick * 2;

	if (xSize <= 0)
		x_tick_num = xAxisTick_Init_num;
	if (ySize <= 0)
		y_tick_num = yAxisTick_Init_num;

	AxisStruct.x.ticks(x_tick_num);
	AxisStruct.y.ticks(y_tick_num);
	// .................

	d3.select(".x.axis").transition().duration(1000).call(AxisStruct.x);
	d3.select(".y.axis").transition().duration(1000).call(AxisStruct.y);

	d3.select("#x_axis").selectAll("text").attr("transform",
			"rotate(-10)translate(0,-10)").attr("font-size", "10px").style(
			"text-anchor", "middle").style("-webkit-user-select", "none");
	d3.select("#y_axis").selectAll("text").attr("transform",
			"rotate(0)translate(-27,0)").attr("class", "yClass").style(
			"text-anchor", "start").style("cursor", "pointer").style(
			"-webkit-user-select", "none");
	// d3.select("#y_axis").select(".domain").remove();

	if (Data_compare.sort_case == Enum.SortBy.task_id) {
		var temp = d3.select("#y_axis").selectAll("text");
		var temp2 = temp[0];
		for (var i = 0; i < temp[0].length; i++) {
			var num = parseInt(d3.select(temp2[i]).text());

			if (num <= task_name_data.length && num > 0) {
				var str = "";
				var temp_str = task_name_data[num - 1];
				for (var k = 0; k < task_name_data[num - 1].length; k++) {
					str += temp_str[k];
					if (k == 3)
						break;
				}
				str += "??";
				d3.select(temp2[i]).text(str).attr("id", "into_task_ID" + num)
						.attr("class", "yClass " + task_name_data[num - 1]);
			} else if (num == 0 || num == task_name_data.length + 1) {
				d3.select(temp2[i]).text(" ");
			}
		}
	} else if (Data_compare.sort_case == Enum.SortBy.case_id) {
		var temp = d3.select("#y_axis").selectAll("text");
		var temp2 = temp[0];
		for (var i = 0; i < temp[0].length; i++) {
			var num = parseInt(d3.select(temp2[i]).text());
			var current_str = d3.select(temp2[i]).text();

			if (num <= maxXAxis - 1 && num > 0) {
				if (current_str.length > 4) {
					var str = "";
					var temp_str = current_str;
					for (var k = 0; k < current_str.length; k++) {
						str += temp_str[k];
						if (k == 3)
							break;
					}
					str += "";
				} else
					str = current_str;
				d3.select(temp2[i]).text(str).attr("id", "into_case_ID" + num)
						.attr("class", "yClass " + num);
			} else if (num == 0 || num == maxXAxis) {
				d3.select(temp2[i]).text(" ");
			}
		}
	} else if (Data_compare.sort_case == Enum.SortBy.origi_id) {
		var temp = d3.select("#y_axis").selectAll("text");
		var temp2 = temp[0];
		for (var i = 0; i < temp[0].length; i++) {
			var num = parseInt(d3.select(temp2[i]).text());

			if (num <= origi_name_data.length && num > 0) {
				var str = "";
				var temp_str = origi_name_data[num - 1];
				for (var k = 0; k < origi_name_data[num - 1].length; k++) {
					str += temp_str[k];
					if (k == 3)
						break;
				}
				str += "??";
				d3.select(temp2[i]).text(str).attr("id", "into_origi_ID" + num)
						.attr("class", "yClass " + origi_name_data[num - 1]);
			} else if (num == 0 || num == origi_name_data.length + 1) {
				d3.select(temp2[i]).text(" ");
			}
		}
	}

	Before_sortby = Data_compare.sort_case;
}

/*
 * //tip view helper var tip_view_trigger = 0; var tip_out = 0; var tip_num = 0;
 * function TipView(px, py, scrX, scrY) { if (tip_view_trigger != 1 || tip_out ==
 * 1) { tip_view_trigger = 0; return; } var half_size = 15;
 * 
 * 
 * if (selected_data.length > 0) { for (var i = 0; i < selected_data.length;
 * i++) { d3.select("#" + selected_data[i]).classed({ 'setStroke': false }); }
 * selected_data = []; }
 * 
 * var tip_data = Pick_Data(px - half_size, py - half_size, px + half_size, py +
 * half_size, 0, 0);
 * 
 * var edit = d3.select("#datatip").style("left", scrX + "px").style("top", scrY +
 * "px"); for (var i = 0; i < tip_data.length; i++) { var temp_x =
 * TimeConvert(parseInt(tip_data[i].timeN)); var inform = "[Time : " +
 * temp_x.getFullYear() + "." + temp_x.getMonth() + "." + temp_x.getDate() + "]" +
 * "[Label : " + tip_data[i].Label + "]" + "[Task : " + tip_data[i].task_name +
 * "]" + "[Originator : " + tip_data[i].originator + "]";
 * edit.append("div").attr("id", "tip").text(inform); tip_num++; }
 * 
 * tip_view_trigger = 2; if (tip_data.length == 0) { tip_view_trigger = 0;
 * tip_num = 1; d3.select("#datatip").classed("hidden", true); } else
 * d3.select("#datatip").classed("hidden", false); }
 * //======================================================================================================
 */

// event mapping
var is_view = false;
function wrapFunc() {
	$('.yClass').bind(
			'mouseover',
			function(e) {
				var temp = $(this).attr("class");
				switch (Data_compare.sort_case) {
				case Enum.SortBy.case_id:
					d3.select("#ylabel_tip").classed("hidden", false).style(
							"left", event.x + "px")
							.style("top", event.y + "px");
					$('#label_tip>strong').html(Y_CaseBy(temp));
					break;
				case Enum.SortBy.task_id:
					d3.select("#ylabel_tip").classed("hidden", false).style(
							"left", event.x + "px")
							.style("top", event.y + "px");
					$('#label_tip>strong').html(Y_CaseBy(temp));
					break;
				case Enum.SortBy.origi_id:
					d3.select("#ylabel_tip").classed("hidden", false).style(
							"left", event.x + "px")
							.style("top", event.y + "px");
					$('#label_tip>strong').html(Y_CaseBy(temp));
					break;
				}
			});

	$('.yClass').bind('mouseout', function(e) {
		d3.select("#ylabel_tip").classed("hidden", true);
	});

	$('.yClass').bind('click', function(e) {
		var testClick = $(this);
		var clickPro = testClick[0].__data__;
		// ========================
		var temp = yAreaLoad(parseInt(clickPro) - 1);
		var x1 = parseFloat(temp.x);
		var x2 = x1 + parseFloat(temp.a);
		var y1 = e.offsetY - 20;
		var y2 = e.offsetY + 20;
		if (Ctrl_btn && !ctrl_flag) {
			ctrl_flag = true;
			Pick_Data(x1, y1, x2, y2, 2, parseInt(clickPro));
		} else if (Shift_btn && !shift_flag) {
			shift_flag = true;
			var getId = $(this).attr('id');
			switch (Data_compare.sort_case) {
			case Enum.SortBy.case_id:
				cutId = "#g_rectX" + (getId.split("into_case_ID")[1] - 1);
				break;
			case Enum.SortBy.task_id:
				cutId = "#g_rectX" + (getId.split("into_task_ID")[1] - 1);
				break;
			case Enum.SortBy.origi_id:
				cutId = "#g_rectX" + (getId.split("into_origi_ID")[1] - 1);
				break;
			}
			$(cutId).attr('fill', 'red');
			shift_st_idx++;
			if (shift_st_idx == 0) {
				shift_st_y = event.offsetY;
				shift_list.push(cutId);
			} else {
				var wd = parseInt($(cutId).attr("height"));
				if (shift_st_idx == 1) {
					shift_st_idx = -1;
					var a = shift_st_y, b = event.offsetY;
					if (a > b) {
						var t = a;
						a = b;
						b = t;
					}
					Pick_Data(x1, a - wd, x2, b, 0, 0);
					shift_list.push(cutId);
				}
				ReturnOriginal(shift_list);
			}
		} else if (!Ctrl_btn && !Shift_btn) {
			Pick_Data(x1, y1, x2, y2, 1, parseInt(clickPro));
		}
	});

	$('.yClass').bind('mouseup', function(e) {
		shift_flag = false;
		ctrl_flag = false;
	});
}

// helper function======================================================
function yAreaLoad(idx) {
	// g_rectX
	if (minYAxis > 0)
		idx = idx - minYAxis;

	var temp = new Vector(d3.select("#g_rectX" + idx).attr("x"), d3.select(
			"#g_rectX" + idx).attr("y"));
	temp.a = d3.select("#g_rectX" + idx).attr("width");
	temp.b = d3.select("#g_rectX" + idx).attr("height");
	return temp;
}

// y axis sort....
function SortBy_yAxis(sort_case) {
	if (sort_case < 0 || sort_case > 2)
		Data_compare.sort_case = 0;
	else
		Data_compare.sort_case = sort_case;
	Re_Setting(0, 0, true);
	wrapFunc();
}

function ZoomOut() {
	Re_Setting(0, 0, false);
}

function MousePoint_Mode(mode) {
	if (mode < 0 || mode > 1)
		mPointMode = 0;
	else
		mPointMode = mode;
}

// time setting...
function TimeConvert(m1) {
	return new Date(m1);
}

function TimeToString(v) {
	var data = TimeConvert(v);
	var year = data.getYear();
	var month = data.getMonth() + 1;
	var day = data.getDay();
	var h = data.getHours();
	var m = data.getMinutes();
	var h_c = h >= 12 ? "??" : "??";
	h = h != 12 ? h % 12 : h;
	var str = year + ". " + month + ". " + day + " " + h_c + " " + h + ":" + m;
	return str;
}

function OverlapStringRemove(ref_data) {
	var a = {};
	for (var i = 0; i < ref_data.length; i++) {
		if (typeof a[ref_data[i]] == "undefined")
			a[ref_data[i]] = 1;
	}
	ref_data.length = 0;
	for ( var i in a)
		ref_data[ref_data.length] = i;
}

function MakeColorTable() {
	var n = 10;
	colorset = new Array('60', 'e0', '20', 'c0', 'ff', 'd0', 'a0', '80', '54',
			'a3');
	for (var i = 0; i < n; i++) {
		for (var p = 0; p < n; p++) {
			for (var k = 0; k < n; k++) {
				var temp_clr = '#' + colorset[i] + colorset[p] + colorset[k];
				ColorList.push(temp_clr);
			}
		}
	}
	// hesh calcu...
	ColorNum = n * n * n;
	var halfSize = ColorList.length / 2 - 1;
	for (var i = 1, k = 100; i < ColorList.length - 1; i++, k++) {
		SwapData(ColorList, (halfSize + ((i * k) * i)) % GetColorNum(),
				(halfSize * (k * i)) % GetColorNum());
	}
	for (var i = 1, k = 325; i < ColorList.length - 1; i++, k++) {
		SwapData(ColorList, (halfSize * (i + k * i)) % GetColorNum(), (i
				* halfSize * (k * i))
				% GetColorNum());
	}
}

function SwapData(ref_data, a, b) {
	var temp = ref_data[a];
	ref_data[a] = ref_data[b];
	ref_data[b] = temp;
}

function GetColor(idx) {
	return ColorList[idx];
}

function GetColorNum() {
	return ColorNum;
}

function ReturnOriginal(cell_list) {
	var color1 = "#419AFA";
	var color2 = "#F0F4F7";
	for (var i = 0; i < cell_list.length; i++) {
		var temp_str = parseInt(cell_list[i].split("#g_rectX")[1]) + 1;
		if (temp_str % 2 != 0)
			d3.select(cell_list[i]).attr("fill", color1);
		else
			d3.select(cell_list[i]).attr("fill", color2);
	}
	cell_list = [];
}

function Y_CaseBy(data) {
	var temp = data.split("yClass ")[1];
	switch (Data_compare.sort_case) {
	case Enum.SortBy.case_id:
		temp = "Case : " + temp;
		break;
	case Enum.SortBy.task_id:
		temp = "Event : " + temp;
		break;
	case Enum.SortBy.origi_id:
		temp = "Originator : " + temp;
		break;
	}
	return temp;
}

function Y_ColorBySelect(y_case) {
	d3.selectAll("circle").attr("fill", function(e) {
		if (y_case == Enum.SortBy.case_id)
			return e.case_color;
		else if (y_case == Enum.SortBy.task_id)
			return e.task_color;
		else
			return e.origi_color;
	});
}

function LineShowTigger(trigger) {
	var type;
	LineShow = trigger;
	if (Data_compare.sort_case == Enum.SortBy.case_id)
		type = "case";
	else if (Data_compare.sort_case == Enum.SortBy.task_id)
		type = "task";
	else
		type = "origi";
	if (trigger)
		d3.selectAll("#" + type + "_work_line").style("visibility", "visible");
	else
		d3.selectAll("#" + type + "_work_line").style("visibility", "hidden");
}

// Right Panel infomation generation Function
// author : dowitcher
function RightPanelWrap(dottedType) {
	if (dottedType === "cases") {

		var shootCase = RightPanel_DataSet.PanelSet_Case;
		var caseNum = shootCase.length;
		$('#compoNum').html(caseNum);

		$('div#rightPanel').empty();
		for ( var shootKey in shootCase) {
			var dataName = shootCase[shootKey].name;
			var startTime = TimeToString(shootCase[shootKey].start_time);
			var endTime = TimeToString(shootCase[shootKey].end_time);
			var tempAvg = shootCase[shootKey].mean_value;
			var avgSpr = tempAvg;//.toFixed(2);
			var minSpr = shootCase[shootKey].min_value;
			var maxSpr = shootCase[shootKey].max_value;

			$('div#rightPanel')
					.append(
							"<table class='table table-bordered'><thread id='rightHead'><tr><th>items</th><th>values</th></tr></thread><tbody id='rightBody'><tr><td>name</td><td>"
									+ dataName
									+ "</td></tr><tr><td>time(first)</td><td>"
									+ startTime
									+ "</td></tr><tr><td>time(end)</td><td>"
									+ endTime
									+ "</td></tr><tr><td>avg spread</td><td>"
									+ avgSpr
									+ "</td></tr><tr><td>min spread</td><td>"
									+ minSpr
									+ "</td></tr><tr><td>max spread</td><td>"
									+ maxSpr + "</td></tr></tbody></table>");
		}

	} else if (dottedType === "task") {

		console.log(RightPanel_DataSet.PanelSet_Task);

		var shootTask = RightPanel_DataSet.PanelSet_Task;
		var taskNum = shootTask.length;
		$('#compoNum').html(taskNum);

		$('div#rightPanel').empty();
		for ( var shootKey in shootTask) {
			var dataName = shootTask[shootKey].name;
			var startTime = TimeToString(shootTask[shootKey].start_time);
			var endTime = TimeToString(shootTask[shootKey].end_time);
			var tempAvg = shootTask[shootKey].mean_value;
			var avgSpr = tempAvg.toFixed(2);
			var minSpr = shootTask[shootKey].min_value;
			var maxSpr = shootTask[shootKey].max_value;

			$('div#rightPanel')
					.append(
							"<table class='table table-bordered'><thread id='rightHead'><tr><th>items</th><th>values</th></tr></thread><tbody id='rightBody'><tr><td>name</td><td>"
									+ dataName
									+ "</td></tr><tr><td>time(first)</td><td>"
									+ startTime
									+ "</td></tr><tr><td>time(end)</td><td>"
									+ endTime
									+ "</td></tr><tr><td>avg spread</td><td>"
									+ avgSpr
									+ "</td></tr><tr><td>min spread</td><td>"
									+ minSpr
									+ "</td></tr><tr><td>max spread</td><td>"
									+ maxSpr + "</td></tr></tbody></table>");
		}

	} else if (dottedType === "origi") {

		var shootOrigin = RightPanel_DataSet.PanelSet_Origi;
		var origiNum = shootOrigin.length;
		$('#compoNum').html(origiNum);

		$('div#rightPanel').empty();
		for ( var shootKey in shootOrigin) {
			var dataName = shootOrigin[shootKey].name;
			var startTime = TimeToString(shootOrigin[shootKey].start_time);
			var endTime = TimeToString(shootOrigin[shootKey].end_time);
			var tempAvg = shootOrigin[shootKey].mean_value;
			var avgSpr = tempAvg.toFixed(2);
			var minSpr = shootOrigin[shootKey].min_value;
			var maxSpr = shootOrigin[shootKey].max_value;

			$('div#rightPanel')
					.append(
							"<table class='table table-bordered'><thread id='rightHead'><tr><th>items</th><th>values</th></tr></thread><tbody id='rightBody'><tr><td>name</td><td>"
									+ dataName
									+ "</td></tr><tr><td>time(first)</td><td>"
									+ startTime
									+ "</td></tr><tr><td>time(end)</td><td>"
									+ endTime
									+ "</td></tr><tr><td>avg spread</td><td>"
									+ avgSpr
									+ "</td></tr><tr><td>min spread</td><td>"
									+ minSpr
									+ "</td></tr><tr><td>max spread</td><td>"
									+ maxSpr + "</td></tr></tbody></table>");
		}
	}

	// mouse over event mapping
	$('#rightBody tr').on('mouseover', function(e) {
		$(this).css({
			'background-color' : '#C9EAFF'
		});
	});
	$('#rightBody tr').on('mouseleave', function(e) {
		$(this).css({
			'background-color' : 'white'
		});
	});

}