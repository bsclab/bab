var DashboardChartView = Backbone.View.extend({
    initialize: function(options) {
        this.options = options || {};

        this.model.on('change:rendered', this.render, this);
    },

    render: function(){
        start = new Date();

        switch (this.model.get('type')) {
            case 'barchart':
                console.log('render barchart');
                BabHelper.drawBarChartC3(this.$el.selector+'-chart1', this.model.get('series'), undefined, this.model.get('colorSeries'), this.model.get('chartParam'));
                break;
            case 'linechart':
                console.log('render linechart');

                // console.log(series);
                /*--------- prepare data part ---------*/
                var dataResult  = [];
                var values      = _.values(this.model.get('series')[0]);

                var minY = math.min(values);
                var maxY = math.max(values);
                
                var sortedData = {};
                var newObj = {};

                /* sorted data first */
                // make new object to remove 0.0 on key
                _.each(this.model.get('series')[0], function(value, key){ 
                  newObj[Number(key)]= value;
                });

                // sort the keys
                var keys = _.keys(newObj);
                var sortedKeys = _.sortBy(keys, function(value, key){
                  return Math.min(value);
                })
                // console.log(sortedKeys);
                if(this.model.get('chartParam').axis.x.type=='timeseries'){
                    sortedKeys.forEach(function(value, key){
                      dataResult.push( [new Date(Number(value)), newObj[value] ]);
                    });            
                }
                else{
                    sortedKeys.forEach(function(value, key){
                      dataResult.push( [Number(value), newObj[value] ]);
                    });   
                }
                // console.log(dataResult);

                BabHelper.drawLineChartDY(this.$el.selector+'-chart1', [dataResult], this.model.get('labelSeries'), this.model.get('chartParam'))
                break;
            case 'piechart':
                console.log('render linechart');

                /* make barchart first then pie chart */
                BabHelper.drawPieChartC3(this.$el.selector+'-chart1', this.model.get('series'), undefined, this.model.get('colorSeries'), this.model.get('chartParam'));
                BabHelper.drawBarChartC3(this.$el.selector+'-chart2', this.model.get('series'), undefined, this.model.get('colorSeries'), this.model.get('chartParam'));
                break;
        }

        /* show rendertime growl */
        end = new Date();
        /*$.notify({
            // options
            icon: 'fa fa-info-circle',
            message: 'chart rendered within '+numeral(end-start).format('0,0')+'ms'
        },{
            // settings
            type: 'success',
            offset: {
                x: 20, 
                y: 300, 
            },
            allow_dismiss: true,
            delay: 3000,    
        });*/
    },
});
