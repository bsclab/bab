var w = 650;
var h = 650;
var paddingX = 0;
var paddingY = 0;

var center_x = w / 2;
var center_y = h / 2;

var Vector2d = function() {
	this.x = 0;
	this.y = 0;
}
var particle = function() {
	this.Pos = new Vector2d();
	this.oldPos = new Vector2d();
	this.accel = new Vector2d();
	this.Fix = false;
}
var Connt = function(p1, p2, p1name, p2name, L, rel, clr) {
	this.p1_idx = p1;
	this.p2_idx = p2;
	this.p1_name = p1name;
	this.p2_name = p2name;
	this.L = L;
	this.relative = rel;
	this.p1_color_set = clr
}
var Node = function() {
	this.label = 0;
	this.id = 0;
	this.name = 0;
	// frequency
	this.absolute = 0;
	this.cases = 0;
	this.maxRepetition = 0;
	this.relative = 0;

	this.color_set = 0;

	this.mpNode = 0;
}
var Conn = function() {
	this.label_title = 0;
	// frequency
	this.absolute = 0;
	this.cases = 0;
	this.maxRepetition = 0;
	this.relative = 0;
	// connect inform
	this.source_idx = 0;
	this.target_idx = 0;
	this.source_name = 0;
	this.target_name = 0;

	this.source_color = 0;
};

var Originator = function() {
	this.name = 0;
}

var Social_DataSet = function() {
	this.Node = [];
	this.Connection = [];
	this.Originators = [];
};

var ColorList = [];
var ColorNum;

var social_data = new Social_DataSet();

var temp_conn = [];
var temp_conn2 = [];
var conn = [];
// connect set
var conn_cpy = [];
var lineorder;

var node_radius;

var doClick = false;
var doOver = false;
var selected_idx = -1;

var centering_is = true;
var push_energy = 0;

var animation_count = 0;

// ===============================chord chart
var resultArr;
var innerRadius, outerRadius;
var arrNum;
var svg;
var chord;

var shootArr;

//
