function MakeColorTable() {
	var n = 6;
	colorset = new Array('20', '40', '80', 'a0', 'c0', 'ff');
	for (var i = 0; i < n; i++) {
		for (var p = 0; p < n; p++) {
			for (var k = 0; k < n; k++) {
				var temp_clr = '#' + colorset[i] + colorset[p] + colorset[k];
				ColorList.push(temp_clr);
			}
		}
	}
	ColorNum = n * n * n;
	var halfSize = ColorList.length / 2 - 1;
	for (var i = 1; i < halfSize - 1; i++) {
		if (i % 2 == 0) {
			SwapData(ColorList, i, halfSize + i);
		}
	}
}

function SwapData(ref_data, a, b) {
	var temp = ref_data[a];
	ref_data[a] = ref_data[b];
	ref_data[b] = temp;
}

function GetColor(idx) {
	return ColorList[idx];
}

function GetColorNum() {
	return ColorNum;
}

function Setting_Init(ref_data) {
	// node...
	MakeColorTable();
	var i = 0;
	for ( var key in ref_data.nodes) {
		var temp_node = new Node();
		temp_node.name = key;
		temp_node.label = ref_data.nodes[key].label;
		temp_node.absolute = ref_data.nodes[key].frequency.absolute;
		temp_node.cases = ref_data.nodes[key].frequency.cases;
		temp_node.maxRepetition = ref_data.nodes[key].frequency.maxRepetition;
		temp_node.relative = ref_data.nodes[key].frequency.relative;
		temp_node.color_set = GetColor(++i % GetColorNum());
		temp_node.mpNode = new particle();
		social_data.Node.push(temp_node);
	}
	// connection...
	for ( var key in ref_data.arcs) {
		if (ref_data.arcs[key].frequency.relative <= 0.0)
			continue;

		var temp_conn = new Conn();
		temp_conn.label_title = key;
		temp_conn.absolute = ref_data.arcs[key].frequency.absolute;
		temp_conn.cases = ref_data.arcs[key].frequency.cases;
		temp_conn.relative = ref_data.arcs[key].frequency.relative;
		temp_conn.maxRepetition = ref_data.arcs[key].frequency.maxRepetition;

		for (var i = 0; i < social_data.Node.length; i++) {
			if (social_data.Node[i].name == ref_data.arcs[key].source) {
				temp_conn.source_idx = i;
				temp_conn.source_name = social_data.Node[i].name;
				temp_conn.source_color = social_data.Node[i].color_set;
			}
			if (social_data.Node[i].name == ref_data.arcs[key].target) {
				temp_conn.target_idx = i;
				temp_conn.target_name = social_data.Node[i].name;
			}
		}
		if (temp_conn.source_idx != temp_conn.target_idx) {
			conn.push(new Connt(temp_conn.source_idx, temp_conn.target_idx,
					temp_conn.source_name, temp_conn.target_name, 280,
					temp_conn.relative, temp_conn.source_color));
			conn_cpy.push(conn[conn.length - 1]);
		}
		social_data.Connection.push(temp_conn);
	}

	// originator
	/*
	 * for (var key in ref_data.originators) { var temp_originator = new
	 * Originator(); temp_originator.name = String(ref_data.originators[key]);
	 * 
	 * social_data.Originators.push(temp_originator); }
	 */

	ref_data.originators.map(function(shoot) {
		var temp_originator = new Originator();
		temp_originator.name = shoot;

		social_data.Originators.push(temp_originator);
	});

	push_energy = 3;
	// console.log(social_data);
}

function Setting_DrawInit() {
	d3.select("#socialBody").style("display", "block").append("svg").attr("id",
			"plane").attr("width", w).attr("height", h);

	svg = d3.select("#socialBody_color").style("display", "none").append("svg")
			.attr("id", "plane2").attr("width", w).attr("height", h)
			.append("g").attr("id", "svg_plane").attr("transform",
					"translate(" + w / 2 + "," + h / 2 + ")");
}

function Setting_Draw() {
	node_radius = 8;

	var svg_plane = d3.select("#plane");

	lineorder = new Array(social_data.Node.length);
	for (var i = 0; i < social_data.Node.length; i++) {
		lineorder[i] = svg_plane.append("g").attr("id", "lineorder" + i);
	}

	var order1 = svg_plane.append("g").attr("id", "order1");
	var order2 = svg_plane.append("g").attr("id", "order2");
	var order3 = svg_plane.append("g").attr("id", "order3");

	for (var i = 0; i < social_data.Node.length; i++) {

		social_data.Node[i].mpNode.Pos.x = Math.floor(Math.random() * 700) + 100;
		social_data.Node[i].mpNode.Pos.y = Math.floor(Math.random() * 300) + 100;
		social_data.Node[i].mpNode.oldPos.x = social_data.Node[i].mpNode.Pos.x;
		social_data.Node[i].mpNode.oldPos.y = social_data.Node[i].mpNode.Pos.y;

		order2.append("circle").attr("id", social_data.Node[i].name).attr(
				"class", i).attr("cx", social_data.Node[i].mpNode.Pos.x).attr(
				"cy", social_data.Node[i].mpNode.Pos.y).attr("r", node_radius)
				.style("fill", social_data.Node[i].color_set)

		order3.append("text").attr("id", "label_" + social_data.Node[i].name)
				.attr("class", i).text(social_data.Node[i].name).attr("x",
						social_data.Node[i].mpNode.Pos.x).attr("y",
						social_data.Node[i].mpNode.Pos.y - 15).style(
						"text-anchor", "middle")
				.style("font-family", "verdana").style("-webkit-user-select",
						"none");
	}

	for (var i = 0; i < conn.length; i++) {
		order1.append("line").attr("x1",
				social_data.Node[conn[i].p1_idx].mpNode.Pos.x).attr("x2",
				social_data.Node[conn[i].p2_idx].mpNode.Pos.x).attr("y1",
				social_data.Node[conn[i].p1_idx].mpNode.Pos.y).attr("y2",
				social_data.Node[conn[i].p2_idx].mpNode.Pos.y).attr("id",
				"lineSet" + i).style("stroke",
				social_data.Node[conn[i].p1_idx].color_set);
	}

	for (var n = 0; n < social_data.Node.length; n++) {
		var temp = [];
		var temp2 = [];
		for (var i = 0; i < conn_cpy.length; i++) {
			if (n == conn_cpy[i].p1_idx) {
				temp.push(conn_cpy[i]);
			}
		}
		for (var k = 0; k < temp.length; k++) {
			for (var i = 0; i < conn_cpy.length; i++) {
				if (temp[k].p2_idx == conn_cpy[i].p1_idx
						&& temp[k].p1_idx == conn_cpy[i].p2_idx) {
					temp2.push(conn_cpy[i]);
				}
			}
		}
		/*for (var i = 0; i < temp.length; i++) { // ordering....1
			var id_str = "lineNodeIDX" + n;
			lineorder[n].append("line").attr("id", i + id_str).attr("class",
					"id" + i).style("stroke", social_data.Node[n].color_set);
		}*/
		for (var i = 0; i < temp.length; i++) { // ordering....2
			var id_str2 = "relConnIDX" + n;
			lineorder[n].append("text").attr("id", i + id_str2).attr("class",
					"Aid" + i).text(temp[i].relative.toFixed(5)).style(
					"text-anchor", "middle").style("font-family", "verdana")
					.style("-webkit-user-select", "none").style("font-weight",
							"bold").style("fill", temp[i].p1_color_set);

			var id_str3 = "RrelConnIDX" + n;
			lineorder[n].append("text").attr("id", i + id_str3).attr("class",
					"Bid" + i).text(temp2[i].relative.toFixed(5)).style(
					"text-anchor", "middle").style("font-family", "verdana")
					.style("-webkit-user-select", "none").style("fill",
							temp2[i].p1_color_set);
		}
		temp_conn.push(temp);
		temp_conn2.push(temp2);
	}
	// console.log(temp_conn);
	// console.log(temp_conn2);
}

function All_Hide() {
	animation_exit();
}

function All_Show() {
	animation_start();
}
// ==============animation================
function animation_exit() {
	animation_count = 0;
	ani_switch = false;
}

function animation_start() {
	ani_switch = true;
	requestAnimationFrameID = requestAnimationFrame(doAnim);
}
//cjftya animation set
var ani_switch = true;
function doAnim() {
    if (!ani_switch) {
        cancelAnimationFrame(requestAnimationFrameID);
        return;
    }
    cjftya_module();
    requestAnimationFrameID = requestAnimationFrame(doAnim);
}

function cjftya_module() {
	if (centering_is)
		cjftya_centering_force();
	else
		cjftya_resolve();
	cjftya_node_rePosition();
	cjftya_update();

	animation_count++;
	if (animation_count > 700)
		animation_exit();
}

function cjftya_resolve() {
	for (var i = 0; i < temp_conn[selected_idx].length; i++) {
		var dx = social_data.Node[conn[i].p2_idx].mpNode.Pos.x
				- social_data.Node[conn[i].p1_idx].mpNode.Pos.x;
		var dy = social_data.Node[conn[i].p2_idx].mpNode.Pos.y
				- social_data.Node[conn[i].p1_idx].mpNode.Pos.y;
		var L = dx * dx + dy * dy;
		var E = ((conn[i].L * conn[i].L - L) / L) * 0.01 * 1; // step delta
		var fx = dx * E;
		var fy = dy * E;
		if (!social_data.Node[conn[i].p1_idx].mpNode.Fix
				&& conn[i].p1_idx != selected_idx) {
			social_data.Node[conn[i].p1_idx].mpNode.Pos.x -= fx;
			social_data.Node[conn[i].p1_idx].mpNode.Pos.y -= fy;
		}
		if (!social_data.Node[conn[i].p2_idx].mpNode.Fix
				&& conn[i].p2_idx != selected_idx) {
			social_data.Node[conn[i].p2_idx].mpNode.Pos.x += fx;
			social_data.Node[conn[i].p2_idx].mpNode.Pos.y += fy;
		}
	}
}

function cjftya_node_rePosition() {
	for (var i = 0; i < social_data.Node.length; i++) {
		for (var k = 0; k < social_data.Node.length; k++) {
			if (i == k || i == selected_idx || k == selected_idx)
				continue;

			var dx = social_data.Node[i].mpNode.Pos.x
					- social_data.Node[k].mpNode.Pos.x;
			var dy = social_data.Node[i].mpNode.Pos.y
					- social_data.Node[k].mpNode.Pos.y;
			var dist = dx * dx + dy * dy;
			var dist_sq = Math.sqrt(dist);
			var nx = dx / dist_sq;
			var ny = dy / dist_sq;
			var energy = (push_energy / dist_sq);
			var fx = nx * energy;
			var fy = ny * energy;

			social_data.Node[i].mpNode.Pos.x += fx;
			social_data.Node[i].mpNode.Pos.y += fy;
			social_data.Node[k].mpNode.Pos.x -= fx;
			social_data.Node[k].mpNode.Pos.y -= fy;
		}
	}
}

function cjftya_centering_force() {
	for (var i = 0; i < social_data.Node.length; i++) {
		if (i == selected_idx)
			continue;

		var dx = (center_x - social_data.Node[i].mpNode.Pos.x);
		var dy = (center_y - social_data.Node[i].mpNode.Pos.y);
		var fx = dx * 0.001;
		var fy = dy * 0.001;
		social_data.Node[i].mpNode.Pos.x += fx;
		social_data.Node[i].mpNode.Pos.y += fy;
	}
}

function cjftya_update() {
	for (var i = 0; i < social_data.Node.length; i++) {
		if (i != selected_idx) {
			var nx = social_data.Node[i].mpNode.Pos.x
					+ ((social_data.Node[i].mpNode.Pos.x - social_data.Node[i].mpNode.oldPos.x) * 0.95)
					+ social_data.Node[i].mpNode.accel.x;
			var ny = social_data.Node[i].mpNode.Pos.y
					+ ((social_data.Node[i].mpNode.Pos.y - social_data.Node[i].mpNode.oldPos.y) * 0.95)
					+ social_data.Node[i].mpNode.accel.y;

			social_data.Node[i].mpNode.oldPos.x = social_data.Node[i].mpNode.Pos.x;
			social_data.Node[i].mpNode.oldPos.y = social_data.Node[i].mpNode.Pos.y;
			social_data.Node[i].mpNode.Pos.x = nx;
			social_data.Node[i].mpNode.Pos.y = ny;
			social_data.Node[i].mpNode.accel.x = social_data.Node[i].mpNode.accel.y = 0;

			cjftya_rap(i);
		}
		d3.select("#" + social_data.Node[i].name).attr("cx",
				social_data.Node[i].mpNode.Pos.x).attr("cy",
				social_data.Node[i].mpNode.Pos.y);

		d3.select("#label_" + social_data.Node[i].name).attr("x",
				social_data.Node[i].mpNode.Pos.x).attr("y",
				social_data.Node[i].mpNode.Pos.y - 15);
	}
	if (centering_is) {
		for (var i = 0; i < conn.length; i++) {
			d3.select("#lineSet" + i).attr("x1",
					social_data.Node[conn[i].p1_idx].mpNode.Pos.x).attr("x2",
					social_data.Node[conn[i].p2_idx].mpNode.Pos.x).attr("y1",
					social_data.Node[conn[i].p1_idx].mpNode.Pos.y).attr("y2",
					social_data.Node[conn[i].p2_idx].mpNode.Pos.y);
		}
	} else {
		for (var i = 0; i < conn.length; i++) {
			$("#" + i + "lineNodeIDX" + selected_idx).attr("x1",
					social_data.Node[conn[i].p1_idx].mpNode.Pos.x).attr("x2",
					social_data.Node[conn[i].p2_idx].mpNode.Pos.x).attr("y1",
					social_data.Node[conn[i].p1_idx].mpNode.Pos.y).attr("y2",
					social_data.Node[conn[i].p2_idx].mpNode.Pos.y);

			var dx = social_data.Node[conn[i].p2_idx].mpNode.Pos.x
					- social_data.Node[conn[i].p1_idx].mpNode.Pos.x;
			var dy = social_data.Node[conn[i].p2_idx].mpNode.Pos.y
					- social_data.Node[conn[i].p1_idx].mpNode.Pos.y;
			var ddx = dx * -0.6;
			var ddy = dy * -0.6;
			var cx = social_data.Node[conn[i].p2_idx].mpNode.Pos.x + ddx;
			var cy = social_data.Node[conn[i].p2_idx].mpNode.Pos.y + ddy;
			$("#" + i + "relConnIDX" + selected_idx).attr("x", cx)
					.attr("y", cy);
			ddx = dx * -0.3;
			ddy = dy * -0.3;
			var cx2 = social_data.Node[conn[i].p2_idx].mpNode.Pos.x + ddx;
			var cy2 = social_data.Node[conn[i].p2_idx].mpNode.Pos.y + ddy;
			$("#" + i + "RrelConnIDX" + selected_idx).attr("x", cx2).attr("y",
					cy2);
		}
	}
}

function cjftya_rap(idx) {
	if (social_data.Node[idx].mpNode.Pos.x < 0 + node_radius + 20) {
		social_data.Node[idx].mpNode.Pos.x = 0 + node_radius + 20;
	} else if (social_data.Node[idx].mpNode.Pos.x > w - node_radius - 100) {
		social_data.Node[idx].mpNode.Pos.x = w - node_radius - 100;
	}
	if (social_data.Node[idx].mpNode.Pos.y < 0 + node_radius + 20) {
		social_data.Node[idx].mpNode.Pos.y = 0 + node_radius + 20;
	} else if (social_data.Node[idx].mpNode.Pos.y > h - node_radius) {
		social_data.Node[idx].mpNode.Pos.y = h - node_radius;
	}
}

function cjftya_pick_node(id) {
	if (selected_idx != -1 && id != selected_idx) {
		d3.select("#lineorder" + selected_idx).style("display", "none");
	}
	selected_idx = parseInt(id);
	push_energy = 2;

	conn = [];
	for (var i = 0; i < temp_conn[selected_idx].length; i++) {
		conn.push(temp_conn[selected_idx][i]);
	}
	d3.select("#lineorder" + selected_idx).style("display", "inline");

	centering_is = false;
	d3.select("#order1").style("display", "none");
}

function cjftya_pick_move(px, py) {
	if (selected_idx == -1)
		return;

	social_data.Node[selected_idx].mpNode.Pos.x += px;
	social_data.Node[selected_idx].mpNode.Pos.y += py;
	social_data.Node[selected_idx].mpNode.oldPos.x = social_data.Node[selected_idx].mpNode.Pos.x;
	social_data.Node[selected_idx].mpNode.oldPos.y = social_data.Node[selected_idx].mpNode.Pos.y;
}

function cjftya_pick_release() {
	d3.select("#lineorder" + selected_idx).style("display", "none");
	selected_idx = -1;

	push_energy = 3;
	centering_is = true;
	conn = [];
	for (var i = 0; i < conn_cpy.length; i++) {
		conn.push(conn_cpy[i]);
	}
	d3.select("#order1").style("display", "inline");
}

// =============================================================================

function Setting_Init_2() {
	// Social Chart 두번째 차트

	// 1씩 더 큰 배열 생성
	arrNum = social_data.Node.length;
	var relativeArr = new Array(arrNum + 1);
	for (var k = 0; k < arrNum + 1; k++) {
		relativeArr[k] = new Array(arrNum + 1);
	}

	// 결과 배열
	resultArr = new Array(arrNum);
	for (var k = 0; k < arrNum; k++) {
		resultArr[k] = new Array(arrNum);
	}
	var startNum = 0;
	for (var a = 1; a < arrNum + 1; a++) {
		relativeArr[0][a] = social_data.Node[startNum].name;
		relativeArr[a][0] = social_data.Node[startNum].name;
		startNum++;
	}

	// console.log(relativeArr);

	shootArr = social_data.Originators;

	console.log(social_data.Originators);

	for (var p = 0; p < social_data.Connection.length; p++) {
		for (var yNum = 1; yNum <= arrNum; yNum++) {
			for (var xNum = 1; xNum <= arrNum; xNum++) {
				if ((social_data.Connection[p].source_name === relativeArr[xNum][0])
						&& (social_data.Connection[p].target_name === relativeArr[0][yNum])) {
					resultArr[xNum - 1][yNum - 1] = social_data.Connection[p].relative * 10000;
				}
			}
		}
	}

	// 관계가 없는 배열에 0 을 입력
	for (var l = 0; l < arrNum; l++) {
		for (var m = 0; m < arrNum; m++) {
			if (resultArr[l][m] === undefined) {
				resultArr[l][m] = 0;
			}
		}
	}
	// ==================================
	// console.log(resultArr);
}

function color_array() {
	var color_temp = [];
	for (var i = 0; i < arrNum; i++) {
		color_temp.push(social_data.Node[i].color_set);
	}
	return color_temp;
}

function Setting_Draw_2() {
	var fill = d3.scale.ordinal().domain(d3.range(5)).range(color_array());

	innerRadius = Math.min(w, h) * .41, outerRadius = innerRadius * 1.1;

	var arc = d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius);

	var layout = d3.layout.chord().padding(.05).sortSubgroups(d3.descending)
			.sortChords(d3.ascending).matrix(resultArr);

	var path = d3.svg.chord().radius(innerRadius);

	var group = svg.selectAll(".group").data(layout.groups).enter().append("g")
			.attr("class", "group").on("mouseover", mouseover);

	// Add the group arc.
	var groupPath = group.append("path").attr("id", function(d, i) {
		return "group" + i;
	}).attr("d", arc).style("fill", function(d) {
		return fill(d.index);
	});

	// Add a text label.
	var groupText = group.append("text").attr("x", 6).attr("dy", 15);

	groupText.append("textPath").attr("xlink:href", function(d, i) {
		return "#group" + i;
	}).style("fill", "white").style("font-size", "15px").style("font-family",
			"verdana").text(function(d, i) {
		return social_data.Node[i].name;
	});

	// Add the chords.
	chord = svg.selectAll(".chord").data(layout.chords).enter().append("path")
			.attr("class", "chord").style("fill", function(d) {
				return fill(d.target.index);
			})
			// .style("opacity","0.4")
			.attr("d", path);

}
//
function mouseover(d, i) {
	chord.classed("fade", function(p) {
		return p.source.index != i && p.target.index != i;
	});
}