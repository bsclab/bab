function RandomColor() {
	return '#' + Math.floor(Math.random() * 13777215 + 3000000).toString(16);
}

function RemoveOfAll() {
	d3.select("#order0").remove();
}

function Moduler(case_id) {
	var temp = d3.select("#order0")[0];
	if (temp[0] != null) {
		RemoveOfAll();
	}
	switch (case_id) {
	case Enum.DrawMode.case0:
		Setting_Draw_01();
		break;
	case Enum.DrawMode.case1:
		Setting_Draw_02();
		break;
	}
}

function Setting_Init(ref_data) {
	var block_count = -1;
	for ( var key in ref_data) {
		switch (key) {
		case "resourceClass":
			Cluster_data.resourceClass = ref_data.resourceClass;
			break;
		case "id":
			Cluster_data.id = ref_data.id;
			break;
		case "clusters":
			for ( var key2 in ref_data.clusters) {
				var temp = ref_data.clusters[key2];

				var block = new Clusterblock();
				block.Cluster_Number = key2;
				block.Cluster_Center = temp.center;
				Cluster_data.Cluster_Blocks.push(block);
				block_count++;
				for (var i = 0; i < temp.cases.length; i++) {
					var temp2 = new Clusternode();
					temp2.string_value = temp.cases[i];
					Cluster_data.Cluster_Blocks[block_count].Cluster_Nodes
							.push(temp2);
				}
			}
			break;
		}
	}
	for (var i = 0; i < block_count + 1; i++) {
		Cluster_data.all_node_num += Cluster_data.Cluster_Blocks[i].Cluster_Nodes.length;
	}

	Setting_Draw_Load();

	console.log(Cluster_data);
}

function Setting_Draw_Load() {
	for (var i = 0; i < Cluster_data.Cluster_Blocks.length; i++) {
		var temp = Cluster_data.Cluster_Blocks[i].Cluster_Nodes.length;
		data.label.push(Cluster_data.Cluster_Blocks[i].Cluster_Number);
		data.data_value.push(temp);
		data.color_value.push(RandomColor());

		var pervalue = (100 * temp) / Cluster_data.all_node_num;
		data.per.push(pervalue.toFixed(1));
	}
}

function Setting_Draw_01() {
	var radius = 180;
	// var color = d3.scale.ordinal().range(["#aaaaaa", "#00ff00", "#eeeeee"]);

	var f_w = w - paddingX;
	var f_h = h - paddingY;

	var d_centerX = f_w / 2;
	var d_centerY = f_h / 2;

	var chk = d3.select("svg")[0];
	var range_w = f_w;

	if (chk[0] == null) {
		var svg = d3.select("#donutBody").append("svg").attr("width", f_w)
				.attr("height", f_h).style("background-color", "#000000");
	} else {
		d3.select("svg").attr("width", range_w);
	}
	d3.select("#donutBox").style("overflow-x", "hidden");

	var order0 = d3.select("svg").append("g").attr("id", "order0").attr(
			"transform", "translate(" + d_centerX + "," + d_centerY + ")");

	var order1 = order0.append("g").attr("id", "order1");
	var arc = d3.svg.arc().outerRadius(radius).innerRadius(radius - 50);
	var pie = d3.layout.pie().sort(null);
	var g = order1.selectAll("#arc").data(pie(data.data_value)).enter().append(
			"g").attr("id", "arc").style("stroke", "#fff").style(
			"stroke-width", "4px");
	g.append("path").attr("d", arc).attr("id", function(e, i) {
		return data.label[i]
	}).style("fill", function(e, i) {
		return data.color_value[i];
	});

	// var order2 = svg.append("g").attr("id", "order2");
	/*
	 * for (var i = 0; i < data.data_value.length; i++) { var px =
	 * arc.centroid(pie(data.data_value)[i])[0] + d_centerX; var py =
	 * arc.centroid(pie(data.data_value)[i])[1] + d_centerY;
	 * 
	 * order2.append("text") .attr("id", "label="+data.label) .attr("dy",
	 * ".35em") .attr("transform", "translate(" + px + ", " + py + ")")
	 * .text(data.data_value[i]) .style("text-anchor", "middle")
	 * .style("font-size","15pt") .style("fill", "white");
	 * 
	 * console.log(arc.centroid(pie(data.data_value)[i])[0]+300);
	 *  }
	 */

	var order2 = order0.append("g").attr("id", "order2");
	order2.append("text").text("events").attr("id", "title").attr("transform",
			"translate(" + 0 + ", " + (0 + 40) + ")").style("text-anchor",
			"middle").style("fill", "white").style("font-size", "25pt").style(
			"font-family", "verdana");

	order2.append("text").text(Cluster_data.all_node_num).attr("id",
			"title_value").attr("transform", "translate(" + 0 + ", " + 0 + ")")
			.style("text-anchor", "middle").style("fill", "white").style(
					"font-weight", "bold").style("font-size", "35pt").style(
					"font-family", "verdana");

	zoom_posX = d_centerX;
	zoom_posY = d_centerY;

	console.log(data);
}

function Setting_Draw_02() {
	var temp_data_set = function() {
		this.value = [];
		this.color = [];
		this.label = [];
		this.per = [];
	};

	var f_w = w - paddingX;
	var f_h = h - paddingY;

	// var d_centerX = f_w / 2;
	var d_centerY = f_h / 2;
	var d_centerX = 0;
	// var d_centerY = 0;

	var radius = 110;
	var range_w = 0;
	if (data.data_value.length > 4) {
		range_w += 30;
		for (var i = 0; i < data.data_value.length; i++) {
			range_w += radius * 2 + 10;
		}
		d3.select("#donutBox").style("overflow-x", "auto");
	} else {
		range_w = f_w;
		d3.select("#donutBox").style("overflow-x", "hidden");
	}

	var chk = d3.select("svg")[0];
	if (chk[0] == null) {
		var svg = d3.select("#donutBody").append("svg").attr("width", f_w)
				.attr("height", f_h).style("background-color", "#000000");
	} else {
		d3.select("svg").attr("width", range_w);
	}

	var order0 = d3.select("svg").append("g").attr("id", "order0").attr(
			"transform", "translate(" + d_centerX + "," + d_centerY + ")");

	var order1 = order0.append("g").attr("id", "order1");
	var arc = d3.svg.arc().outerRadius(radius).innerRadius(radius - 35);
	var pie = d3.layout.pie().sort(null);

	var temp_data = new temp_data_set();
	temp_data.value.push(Cluster_data.all_node_num);
	temp_data.color.push("#777777");
	temp_data.label.push("no_label");
	temp_data.value.push(data.data_value[0]);
	temp_data.color.push(data.color_value[0]);
	temp_data.label.push(data.label[0]);

	for (var i = 0; i < data.data_value.length; i++) {
		temp_data.value[1] = data.data_value[i];
		temp_data.color[1] = data.color_value[i];
		temp_data.label[1] = data.label[i];

		var arc_list = order1.append("g").attr("id", "arc_order" + i);

		var g = arc_list.selectAll("#arc").data(pie(temp_data.value)).enter()
				.append("g").attr("id", "arc" + i).style("stroke", "#000")
				.style("stroke-width", "4px");

		g.append("path").attr("d", arc).attr("id", function(e, k) {
			return temp_data.label[k]
		}).style("fill", function(e, k) {
			return temp_data.color[k];
		}).style("stroke", "#fff").style("stroke-width", "3px");

		// custom rate value.........hum..not good
		arc_list.append("text").text("Cluster " + (i + 1)).attr("id",
				"cluster_name").attr("transform",
				"translate(" + 0 + ", " + (radius / 2.5) + ")").style(
				"text-anchor", "middle").style("fill", "white").style(
				"font-size", (radius / 7) + "pt").style("font-family",
				"verdana");

		arc_list.append("text").text(data.data_value[i]).attr("id",
				"cluster_num").attr("transform",
				"translate(" + 0 + ", " + (radius / 5.5) + ")").style(
				"text-anchor", "middle").style("fill", "white").style(
				"font-size", (radius / 3) + "pt").style("font-family",
				"verdana");

		arc_list.append("text").text(data.per[i] + "%").attr("id",
				"cluster_per").attr("transform",
				"translate(" + 0 + ", " + (-radius / 4.5) + ")").style(
				"text-anchor", "middle").style("fill", data.color_value[i])
				.style("font-size", (radius / 7) + "pt").style("font-family",
						"verdana");

		// =====================================================================
		d3.select("#arc_order" + i).attr("transform",
				"translate(" + (radius + 10 + (radius * 2 + 10) * i) + ",0)");
	}

	zoom_posX = f_w / 2;
	zoom_posY = d_centerY;
}