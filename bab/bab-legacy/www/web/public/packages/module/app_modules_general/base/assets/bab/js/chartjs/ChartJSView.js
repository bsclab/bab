ChartJSView = Backbone.View.extend({
	initialize: function(){
		
	},
	renderRadarChart: function(){
		var self = this;
		var ctx = this.$el[0].getContext("2d");

		var datasets = _.map(this.model.get('data'), function(v, k){
			var color = randomColor({
				   luminosity: 'bright',
				   format: 'rgbArray' // e.g. 'rgb(225,200,20)'
				});
			return {
				label : _.uniqueId('label'),
				fillColor: "rgba("+color[0]+","+color[1]+","+color[2]+", 0.2)",
				strokeColor: "rgba("+color[0]+","+color[1]+","+color[2]+", 0.2)",
				pointColor: "rgba("+color[0]+","+color[1]+","+color[2]+", 0.5)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba("+color[0]+","+color[1]+","+color[2]+", 1)",
	            data: v
			}
		});
		console.log(this);

		var data = {
		    labels: this.model.get('labelSeries'),
		    datasets: datasets
		};

		this.chart = new Chart(ctx).Radar(data, {
			tooltipTemplate: function(v){
				return v.label+': '+BabHelper.getFormattedValueByType(self.model.get('chartParam').axis.y.type, v.value);
			}
		});
	} 
})