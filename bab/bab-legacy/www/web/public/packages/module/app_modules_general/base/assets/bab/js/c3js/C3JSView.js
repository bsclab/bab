C3JSView = Backbone.View.extend({
	initialize: function(){

	},

	/*
		this.model.get('data') = 
		[	['data1', 30],
            ['data2', 120],
        ]

        this.model.get('colors') = ['colors1', 'colors2']
	*/
	renderPieChart: function(){
		var self = this;
		var colors = {};
		var arrLegend = [];

		this.model.get('data').forEach(function(v,k){
			colors[v[0]] = self.model.get('colors')[k],
			arrLegend.push({ index: k, label: v[0], value: v[1], color: self.model.get('colors')[k] });
		});	
		console.log(colors);

		var chart = c3.generate({
            bindto: this.$el.selector,
            size: {
                width: this.model.get('chartParam').width, 
                height: this.model.get('chartParam').height
            },
            data: {
                type    : 'pie',
                columns : this.model.get('data'),
                colors  : colors
            },
            legend: {
                position: 'bottom',
                show: false
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                        return BabHelper.getFormattedValueByType(self.model.get('chartParam').axis.y.type, value)
                    }
                }
            }, 
            pie:{
                label: {
                    format: function (value, ratio, id) {
                        return numeral(ratio).format('0%');
                    }
                }
            }
        });

        d3.select(this.$el.selector).append('div').attr('class','custom-legend col-md-12')
                            .append('ul').attr('class','row list-unstyled');

        d3.select(this.$el.selector + ' .custom-legend ul').selectAll(' li')
            .data(arrLegend).enter()
            .append('li').attr('class', 'legend-item col-md-3')
            .html(function(obj) {
                    return '<span style="background-color:' + obj.color + '"></span>' + obj.label;
            })
            .on('mouseover', function(obj) {
                chart.focus(obj.label);
            })
            .on('mouseout', function(obj) {
                chart.revert();
            })
            .on('click', function(obj) {
                chart.toggle(obj.label);
            });
	}
})