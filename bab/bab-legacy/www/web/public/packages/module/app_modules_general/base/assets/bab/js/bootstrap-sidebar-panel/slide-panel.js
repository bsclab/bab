$(document).ready(function(){
	$('.slide-side .slide-opener').on('click', function() {		
		var panel = $('.slide-side.slide-panel');

		if (panel.hasClass("visible")) {
			panel.removeClass('visible').animate({'margin-right':'-280px'});
			//$('.main-content').css({'margin-right':'0px'});
		} 
		else {
			panel.addClass('visible').animate({'margin-right':'-35px'});
			//$('.main-content').css({'margin-left':'-250px'});
		}	
		return false;	
	});

	$('.slide-bottom .slide-opener').on('click', function(){
		var panel=$('.slide-bottom.slide-panel');

		if (panel.hasClass("visible")) {
			panel.removeClass('visible').animate({'margin-bottom':'-350px'});
			//$('.main-content').css({'margin-right':'0px'});
		} 
		else {
			panel.addClass('visible').animate({'margin-bottom':'0px'});
			//$('.main-content').css({'margin-left':'-250px'});
		}	
		return false;	
	});
});