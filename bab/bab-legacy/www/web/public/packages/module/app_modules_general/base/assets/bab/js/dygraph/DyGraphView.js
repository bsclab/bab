DyGraphView = Backbone.View.extend({
	initialize: function(){
		this.render()
	}, 

	render: function(){
		var self = this;
		console.log(this.$el[0]);
		this.chart = new Dygraph(
	        document.querySelector(this.$el.selector),
	        this.model.get('data'),
	        {
	            width: this.$el[0].clientWidth,
	            height: 0.5 * this.$el[0].clientWidth,
	            xlabel: this.model.get('chartParam').axis.x.label,
	            ylabel: this.model.get('chartParam').axis.y.label,
	            axisLabelFontSize: 12,
	            animatedZooms: true,
	            strokeWidth:1.5,
	            legend: 'always',
	            labels: ['X'].concat(this.model.get('labelSeries')),
	            labelsSeparateLines : true, 
	            labelsDivStyles: {
	                'text-align': 'left',
	                'background': '#fff'
	            },
	            axes: {
	                x: {
	                    axisLabelWidth : 75,
	                    axisLabelFormatter: function(e,a,i) {
	                    	if(self.model.get('chartParam').axis.x.type == 'duration'){
                                return BabHelper.printTimeDurationPretty(e);
                            }
                            else if(self.model.get('chartParam').axis.x.type == 'timeseries'){
                                return BabHelper.getFormattedDYChart(e,a,i);                               
                            }
                            else{
                                return BabHelper.getFormattedDYChart(e,a,i);
                            }	                    },
	                    valueFormatter: function(e,a){
	                    	if(self.model.get('chartParam').axis.x.type == 'duration'){
                                return BabHelper.printTimeDuration(e);
                            }
                            else if(self.model.get('chartParam').axis.x.type == 'timeseries'){
                                return BabHelper.getFormattedDYChart(e,a);
                            }
                            else{
                                return BabHelper.getFormattedDYChart(e,a)
                            }
	                    }
	                }, 
	                y: {
	                	axisLabelFormatter: function(e,a,i) {
	                        if(self.model.get('chartParam').axis.y.type == 'duration'){
                                return BabHelper.printTimeDurationPretty(e);
                            }
                            else if(self.model.get('chartParam').axis.y.type == 'timeseries'){
                                return BabHelper.getFormattedDYChart(e,a,i);
                            }
                            else{
                                return BabHelper.getFormattedDYChart(e,a,i);
                            }
	                    },
	                    valueFormatter: function(e,a){
	                    	if(self.model.get('chartParam').axis.y.type == 'duration'){
                                return BabHelper.printTimeDuration(e);
                            }
                            else if(self.model.get('chartParam').axis.y.type == 'timeseries'){
                                return BabHelper.getFormattedDYChart(e,a);
                            }
                            else{
                                return BabHelper.getFormattedDYChart(e,a);
                            }
	                    }
	                }
	            }
	        }
	    );
	}
})