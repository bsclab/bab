function LogminerC3() {
	//this.logminerHelper = new LogminerHelper();
}

LogminerC3.prototype = {

	getFirstKey : function(data) {
		for (elem in data) {
			return elem;
		}
	},

	/* function to create table */
	createC3Table : function(args) {
		var table = d3.select(args.elem.container).append('table').attr('id',
				args.elem.table.substring(1)).attr('class', 'display');

		var tbody = table.append('tbody');
		var thead = table.append('thead').append('tr');

		for ( var col in args.columnList) {
			thead.append('th').text(args.columnList[col]);
		}

		Object.keys(args.objList).forEach(function(row) {
			var tr = tbody.append('tr');
			for ( var col in args.columnList) {
				tr.append('td').text(args.objList[row][col]);
			}
		});
	},

	jsonSummaryObject : function(args) {
		// console.log('asem');
		// console.log(args);
		var isObject;
		var padding = {
			top : 20,
			right : 20,
			bottom : 20,
			left : 50
		};
		var ratio = 1;

		if (args.objList == null || args.objList.length == 0) {
			isObject = false;
		} else {
			isObject = true;
		}

		if (isObject) {
			var xData = [], xDataLabel = [], yData = [];

			// console.log(args.xAxis.count);
			if (args.xAxis.count == undefined) {
				args.xAxis.count = 20;
			}
			if (args.yAxis.count == undefined) {
				args.yAxis.count = 5;
			}

			xData.push(args.xAxis.name);
			yData.push(args.yAxis.name);

			Object.keys(args.objList).forEach(function(row) {
				/* mapping your object here */
				xData.push(args.objList[row]);
				yData.push(row);
			});

			/*
			 * if yAxis name is duration, then need extra space for the yAxis
			 * value
			 */
			if (S(args.yAxis.name.toLowerCase()).contains('duration')) {
				padding.left = 100;
			}

			if (xData.length < 100) {
				ratio = 0.5;
			}
			console.log('date length: ' + xData.length);

		} else {
			xData = null;
			yData = null;
		}

		return {
			isObject : isObject,
			padding : padding,
			ratio : ratio,
			xAxis : {
				data : xData,
				name : args.xAxis.name,
				format : args.xAxis.format,
				count : args.xAxis.count,
				type : args.xAxis.type
			},
			yAxis : {
				data : yData,
				name : args.yAxis.name,
				format : args.yAxis.format,
				count : args.yAxis.count,
				type : args.yAxis.type
			}
		};

	},

	jsonPerformanceChartObject : function(objList, xAxisName, yAxisName) {
		var xData = [], yData = [];

		xData.push(xAxisName);
		yData.push(yAxisName);

		for ( var row in objList) {
			xData.push(objList[row].x);
			yData.push(objList[row].y);
		}

		return {
			'xAxis' : {
				'data' : xData,
				'name' : xAxisName
			},
			'yAxis' : {
				'data' : yData,
				'name' : yAxisName
			}
		};
	},

	jsonPerformanceChartObjectType : function(objList, objListType, xAxisName,
			yAxisName) {
		var xData = [], yData = [];

		yData.push(yAxisName);
		for ( var row in objList) {
			yData.push(objList[row].y);
		}

		/* initialize y data */
		xData.push(yData);

		objListType.forEach(function(row) {
			var arrTemp = [];
			arrTemp.push(row); // initialize first element as identifier

			/* create array for each event type */
			for ( var row2 in objList) {
				arrTemp.push(objList[row2]['y']);
			}
			xData.push(arrTemp);
		});

		return {
			'xAxis' : {
				'data' : xData,
				'name' : xAxisName
			},
			'yAxis' : {
				'data' : yData,
				'name' : yAxisName
			}
		};

		// console.log(data);
	},

	createC3MultipleLineChart : function(elem, objList) {
		console.log('asem!!!');
		console.log(objList.xAxis.data);
		var chart = c3.generate({
			bindto : elem,
			grid : {
				y : {
					show : true
				}
			},
			padding : objList.padding,
			axis : {
				x : {
					label : objList.xAxis.name,
					tick : {
						format : d3.format(',.1f'),
						count : 20
					},
				},
				y : {
					label : objList.yAxis.name,
					tick : {
						count : 5
					}
				}
			},
			data : {
				type : 'bar',
				x : objList.yAxis.name,
				columns : objList.xAxis.data
			},
			tooltip : {
				contents : function(d) {
					var table = '<table class="table">';
					table += '<thead><tr class="info"><th>'
							+ objList.xAxis.name + '</th><th>' + d[0].x
							+ '</th></tr></thead>';
					table += '<tbody><tr class="active"><td>'
							+ objList.yAxis.name + '</td><td>' + d[0].value
							+ '</td></tr></tbody>';
					return table;
				}
			}
		});
	},

	/*
	 * args needed is : { objList:{ xAxis: {type, name, format, count, data}
	 * yAxis: {type, name, format, count, data} }
	 */
	createC3BarChart : function(args) {
		console.log('barchart');
		console.log(args.objList);
		// console.log(args.objList.xAxis);
		// console.log(args.objList);

		if (args.objList.isObject) {
			//var logminerHelper = new LogminerHelper();
			var chart = c3.generate({
				bindto : args.elem,
				grid : {
					y : {
						show : true
					}
				},
				padding : args.objList.padding,
				size : {
					width : args.objList.size.width,
					height : args.objList.size.height
				},
				axis : {
					x : {
						type : args.objList.xAxis.type,
						label : args.objList.xAxis.name,
						tick : {
							format : args.objList.xAxis.format,
							count : args.objList.xAxis.count
						},
					},
					y : {
						type : args.objList.xAxis.type,
						label : args.objList.yAxis.name,
						tick : {
							format : args.objList.yAxis.format,
							count : args.objList.yAxis.count
						}
					}
				},
				bar : {
					width : {
						ratio : args.objList.ratio
					}
				},
				data : {
					type : 'bar',
					x : args.objList.yAxis.name,
					columns : [ args.objList.xAxis.data,
							args.objList.yAxis.data ]
				},
				tooltip : {
					format : {
						data : null,
						title : function(d) {
							var format = args.objList.xAxis.format;
							var result;
							if (format == undefined)
								result = d;
							else
								result = format(d);
							return args.objList.xAxis.name + ' ' + result;
						},
						name : function(value, id, ratio) {
							return args.objList.yAxis.name
						},
						value : function(value, id, ratio) {
							var format = args.objList.yAxis.format;
							if (format == undefined)
								result = value;
							else
								result = format(value);
							return result;
						}
					},
				// contents: function (d){
				// var table = '<table class="table bar-info">';
				// if(args.objList.xAxis.type=='timeseries'){
				// //var newSeconds = moment(Number(d[0].x)).format('hh:mm:ss');
				// var newSeconds =
				// logminerHelper.printTimeDuration(Number(d[0].x));
				// table+= '<thead><tr
				// class="info"><th>'+args.objList.xAxis.name+'</th><th
				// class="info-value">up to '+newSeconds+'</th></tr></thead>';
				// }
				// else{
				// table+= '<thead><tr
				// class="info"><th>'+args.objList.xAxis.name+'</th><th
				// class="info-value">'+
				// logminerHelper.printNumber(d[0].x)+'</th></tr></thead>';
				// }
				// table+= '<tbody><tr
				// class="active"><td>'+args.objList.yAxis.name+'</td><td
				// class="active-value">'+logminerHelper.printNumber(d[0].value)+'</td></tr></tbody>';
				// return table;
				// }
				}
			});

			// if(args.objList.xAxis.timeseries){
			// var theData = args.objList.yAxis.data;
			// theData.shift();
			// //console.log(theData);
			// var minDuration = theData[0]; console.log('max : '+minDuration);
			// var maxDuration = Math.max.apply(null, theData); console.log('max
			// : '+maxDuration);
			// d3.selectAll('table.info').remove();
			// d3.selectAll('.log-summary-table').insert('table',
			// ":first-child").attr('class','table table-striped table-bordered
			// table-hover col-md-6 info')
			// .each(function(){
			// var table = d3.select(this);
			// var tr = table.append('tr');
			// tr.append('td').text('Max Duration');
			// tr.append('td').text(logminerHelper.printTimeDuration(Number(maxDuration)));
			// var tr2 = table.append('tr');
			// tr2.append('td').text('Min Duration');
			// tr2.append('td').text(logminerHelper.printTimeDuration(Number(minDuration)));
			// });
			// }

		} else {
			// console.log(args.elem);
			d3.select(args.elem).text('');
			d3.select(args.elem).append('div').classed('no-data', true).append(
					'span').text('sorry, no data available')

			chart = null;
		}
		return chart;
	},

	/*
	 * args needed is { elem : objList: }
	 */
	createC3PieChart : function(args) {
		console.log('piehart');
		console.log(args);

		var isShow = args.legend.custom ? false : true;
		//var logminerHelper = new LogminerHelper();
		var chart = c3.generate({
			bindto : args.elem,
			padding : {
				top : 20,
				right : 20,
				bottom : 20,
				left : 0
			},
			size : {
				width : args.width
			},
			data : {
				type : 'pie',
				columns : args.objList
			},
			legend : {
				position : args.legend.position,
				show : isShow
			},
			tooltip : {
				format : {
					value : function(value, ratio, id) {
						return BabHelper.printNumber(value, 0);
					}
				}
			},
			pie : {
				label : {
					format : function(value, ratio, id) {
						// var labelText = '<ul
						// class="list-unstyled"><li>'+numeral(ratio).format('0%')+'</li><li>'+numeral(value).format('0,0')+'</li></ul>';
						// return $.parseHTML( labelText );
						// console.log($.parseHTML( labelText ));
						return numeral(ratio).format('0%') + ' '
								+ numeral(value).format('0,0');
					}
				}
			}
		});

		/* print custom legend below chart */
		if (args.legend.custom) {
			var objLegendList = [];

			console.log('custom legend');
			d3.selectAll(args.elem + ' .c3-chart-arc').each(
					function(k, v) {
						console.log(k);
						var theClass = d3.select(this).attr('class').split(
								'c3-target');
						objLegendList.push({
							label : k.data.id,
							value : k.value,
							dataId : theClass[2]
						}); // set value to array
					});
			console.log('end of custom legend');

			var divLegend = d3.select(args.elem).append('div').attr('class',
					'custom-legend col-md-12').append('ul').attr('class',
					'row list-unstyled');

			d3.select(args.elem + ' .custom-legend ul').selectAll(' li').data(
					objLegendList).enter().append('li').attr('class',
					'legend-item col-md-3').attr('data-id', function(obj) {
				console.log(obj);
				return obj.dataId;
			}).html(
					function(obj) {
						return '<span style="background-color:'
								+ chart.color(obj.dataId) + '"></span>'
								+ obj.label;
					}).on('mouseover', function(obj) {
				console.log(obj);
				chart.focus(obj.dataId);
			}).on('mouseout', function(obj) {
				chart.revert();
			}).on('click', function(obj) {
				chart.toggle(obj.dataId);
			});

		}
	},

	createC3DonutChart : function(elem, objList, donutTitle) {
		var myColumns = [];

		Object.keys(objList).forEach(function(row) {
			/* mapping your object here */
			myColumns.push([ row, objList[row] ]);
		});

		var chart = c3.generate({
			bindto : elem,
			data : {
				columns : myColumns,
				type : 'donut'
			},
			donut : {
				title : donutTitle,
				onclick : function(d, i) {
					console.log(d, i);
				},
				onmouseover : function(d, i) {
					console.log(d, i);
				},
				onmouseout : function(d, i) {
					console.log(d, i);
				}
			}
		})
	}
}
