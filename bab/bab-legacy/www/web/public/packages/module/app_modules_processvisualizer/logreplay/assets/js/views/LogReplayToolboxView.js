var LogReplayToolboxView = Backbone.View.extend({

	events: {
		'click button#logreplay-defaultcolor': 'changeDefaultColorState',
		'click button#logreplay-showcaseid': 'changeShowCaseIdState',
		'click button#logreplay-enableglobalkpi': 'enableGlobalKPI',
		'click button#logreplay-enablearcnodekpi': 'enableArcNodeKPI',
		'change select#logreplay-selectattribute': 'printCssRelatedToAttribute',
		'keyup input#logreplay-searchattribute': 'showAndHideAttribute',
		'click button#logreplay-hideunselectedattribute': 'printSelectedAttributesCss',
		'click button#logreplay-enabletokenfadeout': 'enableTokenFadeout'
	},

	initialize: function(){
		this.$attributeListEl = this.$el.find('#logreplay-toolbox-legend-body .list-group');
		this.attributeCollection = new AttributeCollection();
		this.cssTemplate = Handlebars.compile($('#css-template').html());

		this.renderSelectAttributes();
		this.initializeAttributes()
		this.$buttonDefaultColorEl = this.$el.find('button#logreplay-defaultcolor');
		this.$buttonShowCaseIdEl = this.$el.find('button#logreplay-showcaseid');
		this.$buttonEnableGlobalKPIEl = this.$el.find('button#logreplay-enableglobalkpi');
		this.$buttonEnableArcNodeKPIEl = this.$el.find('button#logreplay-enablearcnodekpi');
		this.$buttonEnableTokenFadeOutEl = this.$el.find('button#logreplay-enabletokenfadeout');

		this.model.on('change:enableTokenFadeOut', this.changeButtonTokenFadeOut, this);
		this.model.on('change:enableGlobalKPI', this.changeButtonColorEnableGlobalKPI, this);
		this.model.on('change:enableArcNodeKPI', this.changeButtonColorEnableArcNodeKPI, this);
		this.model.on('change:isShowTokenCaseId', this.changeButtonColorShowTokenCaseId, this);
		this.model.on('change:isShowTokenDefaultColor', this.changeButtonColorShowTokenDefaultColor, this);
		
		this.changeButtonTokenFadeOut();
		this.changeButtonColorEnableGlobalKPI();
		this.changeButtonColorEnableArcNodeKPI();
		this.changeButtonColorShowTokenCaseId();
		this.changeButtonColorShowTokenDefaultColor();

		// var fps = document.getElementById("fps"),
		//     startTime = Date.now(),
		//     frame = 0;

		// tick = function() {
		//   var time = Date.now();
		//   frame++;
		//   if (time - startTime > 1000) {
		//       fps.innerHTML = (frame / ((time - startTime) / 1000)).toFixed(1);
		//       startTime = time;
		//       frame = 0;
		// }
		//   window.requestAnimationFrame(tick);
		// }
		// tick();
	}, 

	initializeAttributes: function(){
		var self = this;
		var css = '';

		arrAttribute = _.where(this.model.get('tokenAttributes'), { type:'array' });
		currentAttribute = _.findWhere(this.model.get('tokenAttributes'), { show:true, type:'array' });

		arrAttribute.forEach(function(v, k){
			colors = randomColor({ count: v.options.length });

			v.options.forEach(function(value, key){
				// console.log('create attribute view');
				attributeModel = new AttributeModel({
					name : v.name,
					value : value,
					class : 'attr-'+S(v.name).slugify().s+'-'+S(value).slugify().s,
					color: colors[key]
				});

				self.attributeCollection.push(attributeModel);

				attributeView = new AttributeView({
					id: attributeModel.get('class'),
					model: attributeModel
				});
				self.$attributeListEl.append(attributeView.render().el);
			});
		});

		this.attributeCollection.where({ name: currentAttribute.name }).forEach(function(v, k){
			v.set({ visible: true, selected: true });
			css+= self.cssTemplate(v.attributes);
		});
        d3.select('head').append('style').attr('type', 'text/css').text(css);
	},

	showAndHideAttribute: function(ev){
		console.log(ev.currentTarget.value);
		currentAttribute = _.findWhere(this.model.get('tokenAttributes'), { show:true, type:'array' });
		
		var a = this.attributeCollection
			.filter(function(val, key){
				return val.get('name') == currentAttribute.name && S(val.get('value').toLowerCase()).contains(ev.currentTarget.value);
			});
		a.forEach(function(val, key){
			val.set({ visible: true });
		});
		console.log(a);

		var b = this.attributeCollection
			.filter(function(val, key){
				return val.get('name') == currentAttribute.name && !S(val.get('value').toLowerCase()).contains(ev.currentTarget.value);
			});
		b.forEach(function(val, key){
			val.set({ visible: false });
		});
		console.log(b);
	},

	// get only variable which type is array
	printCssRelatedToAttribute: function(ev){
		var self = this;
		var css = '';
		
		currentAttribute = _.findWhere(this.model.get('tokenAttributes'), { type:'array', name: ev.currentTarget.value });
		previousAttribute = _.findWhere(this.model.get('tokenAttributes'), { type:'array', show: true });
		currentAttribute.show = true;
		previousAttribute.show = false;

		this.attributeCollection.where({ name: previousAttribute.name }).forEach(function(v, k){
			v.set({ visible: false, selected: false });
		});
		this.attributeCollection.where({ name: currentAttribute.name }).forEach(function(v, k){
			v.set({ visible: true, selected: true });				
		});
    	
		this.printSelectedAttributesCss();

		console.log(this.attributeCollection);		
	},

	printSelectedAttributesCss: function(){
		var self 	= this;
		var css = "";
		console.log('is hide? '+this.model.get('isHideTokenUnselectedAttributes'));
		// this.model.set({ isHideTokenUnselectedAttributes: !this.model.get('isHideTokenUnselectedAttributes') });
		
		currentAttribute = _.findWhere(this.model.get('tokenAttributes'), { show:true, type:'array' });

		this.attributeCollection.where({ selected: true, name:currentAttribute.name }).forEach(function(v, k){

			color = _.isUndefined(v.get('customColor'))? v.get('color'):v.get('customColor');

			css+= self.cssTemplate({ 
				class: v.get('class'), 
				color: color,
				opacity: 1 
			});
		});

		this.attributeCollection.where({ selected: false, name:currentAttribute.name }).forEach(function(v, k){
			css+= self.cssTemplate({ 
				class: v.get('class'), 
				color: color, 
				opacity: 0 
			});
		});

		d3.select('head style').remove();
    	d3.select('head').append('style').attr('type', 'text/css').text(css);
	},

	renderSelectAttributes: function(){
		html='';
		_.where(this.model.get('tokenAttributes'), {type:'array'}).forEach(function(val, key){
			html += '<option value="'+val.name+'">'+val.name+'</option>';
		});
		this.$el.find('select#logreplay-selectattribute').append(html);
	},

	changeDefaultColorState: function(ev){
		console.log('change show default color state');
		var self = this;
		
		this.model.set({ isShowTokenDefaultColor: !this.model.get('isShowTokenDefaultColor') });
		
		var filtered = this.collection.filter(function(model){
		    return model.get('state')=='stop-initial' || model.get('state')=='running';
		});
		filtered.forEach(function(model, key){
			model.set({ isShowTokenDefaultColor: self.model.get('isShowTokenDefaultColor') });
		});
	},

	changeShowCaseIdState: function(ev){
		console.log('change show case id state');
		var self = this;
		
		this.model.set({ isShowTokenCaseId: !this.model.get('isShowTokenCaseId') });
		
		var filtered = this.collection.filter(function(model){
		    return model.get('state')=='stop-initial' || model.get('state')=='running';
		});
		
		filtered.forEach(function(model, key){
			model.set({ isShowTokenCaseId: self.model.get('isShowTokenCaseId')});
		});
	},

	enableTokenFadeout: function(ev){
		var self = this;
		this.model.set({ enableTokenFadeOut: !this.model.get('enableTokenFadeOut') });
		this.collection.forEach(function(model, key){
			model.set({ isEnableTokenFadeOut: self.model.get('enableTokenFadeOut') });
		})
	},

	enableGlobalKPI: function(ev){
		this.model.set({ enableGlobalKPI: !this.model.get('enableGlobalKPI') });
	},

	enableArcNodeKPI: function(ev){
		this.model.set({ enableArcNodeKPI: !this.model.get('enableArcNodeKPI') });
	},

	changeButtonColorShowTokenDefaultColor: function(){
		if(this.model.get('isShowTokenDefaultColor')){
			this.$buttonDefaultColorEl.addClass('btn-success');
		}
		else{
			this.$buttonDefaultColorEl.removeClass('btn-success');
		}
	},

	changeButtonColorShowTokenCaseId: function(){
		if(this.model.get('isShowTokenCaseId')){
			this.$buttonShowCaseIdEl.addClass('btn-success');
		}
		else{
			this.$buttonShowCaseIdEl.removeClass('btn-success');
		}
	},

	changeButtonColorEnableGlobalKPI: function(){
		if(this.model.get('enableGlobalKPI')){
			this.$buttonEnableGlobalKPIEl.addClass('btn-success');
		}
		else{
			this.$buttonEnableGlobalKPIEl.removeClass('btn-success');	
		}		
	},

	changeButtonColorEnableArcNodeKPI: function(){
		if(this.model.get('enableArcNodeKPI')){
			this.$buttonEnableArcNodeKPIEl.addClass('btn-success');
		}
		else{
			this.$buttonEnableArcNodeKPIEl.removeClass('btn-success');
		}
	},

	changeButtonTokenFadeOut: function(){
		if(this.model.get('enableTokenFadeOut')){
			this.$buttonEnableTokenFadeOutEl.addClass('btn-success');
		}
		else{
			this.$buttonEnableTokenFadeOutEl.removeClass('btn-success');
		}
	}
})