AttributeModel = Backbone.RelationalModel.extend({
    defaults: {
        name: 'name',
        value: 'value',
        class: 'class',
        color: 'color',
        customColor: undefined,
    	selected: false,   // if user select
    	visible: false,
    	destroy: false,
    }
});