// Filename: view/CircleViewB
var StatisticsNodesView = Backbone.View.extend({
	el:$('#table-statistics tbody'), 
	initialize: function(){
		
	}, 
	render: function(){
		var thead = d3.select('#table-statistics tbody');
		this.collection.forEach(function(objNode){
			var tr = thead.append('tr');
			tr.append('td').text(objNode.get('name'));
			tr.append('td').attr('class', objNode.get('id')).text(0);
		});
	},
	showCurrentEvents: function(args){
		var currentEventsLength = args.get('arc').get('nodeTarget').get('currentEvents').length;
		d3.select('#table-statistics tbody td.'+args.get('arc').get('nodeTarget').get('id')).text(currentEventsLength);
	}
});