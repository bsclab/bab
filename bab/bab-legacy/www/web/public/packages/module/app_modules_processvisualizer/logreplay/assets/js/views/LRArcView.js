LRArcView= Backbone.View.extend({
	initialize: function(options){
		this.options = options || {};
		this.$indicatorA = this.$el.find(".indicator-1");
		this.render();
	},

	render: function(){
		this.$indicatorA.html('0');
	}, 

	showIndicatorA: function(model){
		if(model.get('syncIndicator')){
			filtered = this.collection.filter(function(value, key){
					return value.get('arc').get('nodeSource').get('label') == model.get('arc').get('nodeSource').get('label') && value.get('arc').get('nodeTarget').get('label') == model.get('arc').get('nodeTarget').get('label') && value.get('state')=='running';
				});
			this.$indicatorA.html(filtered.length);
		}
	}
});