var TableRowPartitionView = Backbone.View.extend({
	
	initialize: function(options){
		this.options = options || {};
		_.bindAll(this, 'render');
		this.template = Handlebars.compile($('#table-row-template').html());
		this.model.on('change', this.render, this)
	}, 

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
})

var PartitionInfoView = Backbone.View.extend({
	initialize: function(options){
		_.bindAll(this, "render");
		this.options = options || {};
		this.render();
	},

	render: function(){
		var self = this;
	    console.log(this.collection);
	    this.collection.forEach(function(model, key){
	    	console.log(model);
		    var rowView = new TableRowPartitionView({
		    	model: model,
		    });
	    	self.$el.append(rowView.render().el);
	    })
	}
});