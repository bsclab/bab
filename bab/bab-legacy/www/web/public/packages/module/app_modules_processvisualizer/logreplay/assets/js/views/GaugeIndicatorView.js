GaugeIndicatorView = Backbone.View.extend({
	initialize: function(options){
		var self = this;
		this.options = options || {};
		this.$finishEl = this.$el.find('#gauge-chart-events-finish .progress');
		this.$runningEl = this.$el.find('#gauge-chart-events-running .progress');
		this.template = Handlebars.compile($('#progressbar-template').html());
		this.templateMinMax = Handlebars.compile($('#progressbar-minmax-template').html());
		this.render();
	}, 
	render: function(){
		this.$el.find('#gauge-chart-events-finish').append(this.templateMinMax({valueMax:numeral(this.model.get('totalEvent')).format('0,0')}));
		this.$el.find('#gauge-chart-events-running').append(this.templateMinMax({valueMax:numeral(this.model.get('totalEvent')).format('0,0')}));
		this.$finishEl.html(this.template({ value:0, percentValue:0 }));
		this.$runningEl.html(this.template({ value:0, percentValue:0 }));
	},

	updateIndicator: function(model, logReplayModel){
		if(logReplayModel.get('enableGlobalKPI')){
			// console.log(model.get('tokenId')+'-'+model.get('state'));
			finishTokens = this.collection.where({ state: 'finish' });
			this.$finishEl.html(this.template({value:finishTokens.length, percentValue:(finishTokens.length/this.model.get('totalEvent'))*100 }));
			// this.$finishEl.html(this.template({value:numeral(finishTokens.length).format('0,0'), percentValue:finishTokens.length/this.model.get('totalEvent') }));
			// this.finishTokensGaugeChart.refresh(finishTokens.length);

			runningTokens = this.collection.where({ state: 'running' });
			this.$runningEl.html(this.template({value:runningTokens.length, percentValue:(runningTokens.length/this.model.get('totalEvent'))*100 }));
			// this.$runningEl.html(this.template({value:numeral(runningTokens.length).format('0,0'), percentValue:runningTokens.length/this.model.get('totalEvent') }));
			// this.runningTokensGaugeChart.refresh(runningTokens.length);
		}
	}
});