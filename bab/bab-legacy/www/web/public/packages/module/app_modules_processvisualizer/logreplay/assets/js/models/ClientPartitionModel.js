ClientPartitionModel = Backbone.Model.extend({
	defaults: {
		start: 0,
		complete:0
	},
	relations:[
    	{
	    	type: Backbone.HasMany,
			key: 'allEvents',
			relatedModel: 'EventRelationalModel',
			collectionType: 'EventCollection',
			reverseRelation: {
				key: 'inClientPartition',
				includeInJSON: 'id'
				// 'relatedModel' is automatically set to 'Zoo'; the 'relationType' to 'HasOne'.
			}
	    }, 
	],

	intiialize: function(){
		this.set({ start: this.get('start')/1000 });
	},
});
