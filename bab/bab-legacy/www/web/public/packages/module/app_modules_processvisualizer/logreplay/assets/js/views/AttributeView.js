AttributeView = Backbone.View.extend({
	tagName: 'li',
	className: 'list-group-item hidden',
	
	initialize: function () {
		var self = this;
		this.template = Handlebars.compile($('#attribute-template').html());
		this.model.on('change:selected', this.showSelected, this);
		this.model.on('change:visible', this.showHide, this);
		this.model.on('change:destroy', this.removeView, this);
	},
	
	events:{
		'click div': 'changeSelected'
	},
	
	beforeRender: function() { 
	    // console.log('before render'); 
  	},

	render: function(){
		var self = this;
		this.$el.html(this.template(this.model.attributes));
		this.$selectedEl = this.$el.find('.checked');
		this.$colorpickerEl = this.$el.find('input');

		this.$colorpickerEl.colorpicker({
			color: this.model.get('color')
		});

		this.$colorpickerEl.on("change.color", function(event, color){
		    self.model.set({ customColor: color });
		});
		return this;
	},

	showHide: function(){
		if(this.model.get('visible')){
			this.$el.removeClass('hidden');
		}
		else{
			this.$el.addClass('hidden')
		}
	},

	showSelected: function(){
		if(this.model.get('selected')){
			this.$selectedEl.removeClass('hidden');
		}
		else{
			this.$selectedEl.addClass('hidden');
		}
	},

	changeSelected: function(ev){
		this.model.set({ selected: !this.model.get('selected') });
	},

	removeView: function(){
		this.remove();
		// this.model.destroy();
	}
})