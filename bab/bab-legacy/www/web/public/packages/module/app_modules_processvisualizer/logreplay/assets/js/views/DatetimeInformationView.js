// Filename: view/CircleViewB
var DateTimeInformationView = Backbone.View.extend({
	
	initialize: function(options){
		this.options = options || {};
		this.model.on('change:current', this.render, this);
		this.$date = this.$el.find('.date');
		this.$time = this.$el.find('.time');
		this.render();
	}, 	 

	render: function(){
		this.$date.text(moment( Number((this.model.get('current')+this.model.get('start'))*1000 )).format('dddd, MMMM Do YYYY'));
		this.$time.text(moment( Number((this.model.get('current')+this.model.get('start'))*1000 )).format('HH:mm:ss'));
	},
});