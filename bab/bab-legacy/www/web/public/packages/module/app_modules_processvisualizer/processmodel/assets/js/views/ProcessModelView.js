ProcessModelView = Backbone.View.extend({
        
    initialize: function(){
        
        // initiate arc and node collection
        this.nodeCollection = new NodeCollection();
        this.arcCollection = new ArcCollection();
        this.nodeViews = [];
        this.arcViews = [];

        this.arcsFrequencyRange = [];
        this.arcsDependencyRange = [];
        this.nodesFrequencyRange = [];

        this.nodeFreqClass = this.makeArray(5, 'vertex-freq-', ''); 
        this.arcFreqClass = this.makeArray(5, 'edgePath-freq-', '');
        this.arcDepClass = this.makeArray(5, 'edgePath-dep-', '');
        this.nodeTemplate = Handlebars.compile($('#node-template').html());
        this.arcTemplate = Handlebars.compile($('#arc-template').html());

        this.g = new dagreD3.graphlib.Graph()
                    .setGraph({})
                    .setDefaultEdgeLabel(function() { return {}; });

        this.svg = d3.select(this.el).append('svg'),
        this.inner = this.svg.append('g');
        // Create the renderer
        this.renderer = new dagreD3.render();

        this.initializeGraph();

        this.model.on('change:jsonProcessModel', this.initializeGraph, this);
    },

    events:{
        'click #button-graph-fit-window' : 'graphScaleToFit',
        'click #button-graph-fit-actual' : 'graphScaleToActual',

        'click #button-graph-font-inc' : 'fontIncrease',
        'click #button-graph-font-dec' : 'fontDecrease',
        
        'click #button-graph-arctype-rounded' : 'changeArcTypeTo',
        'click #button-graph-arctype-linear' : 'changeArcTypeTo',
        'click #button-graph-arctype-linearsmooth' : 'changeArcTypeTo',
        'click #button-graph-arctype-rectangular' : 'changeArcTypeTo',
        
        'click #button-graph-arcdirection-tb' : 'changeArcDirectionTo',
        'click #button-graph-arcdirection-lr' : 'changeArcDirectionTo',

        'keyup #node-name' : 'searchNodeName',
        'keyup #node-frequency' : 'searchNodeFrequency',
        'keyup #arc-frequency' : 'searchArcFrequency',
        'keyup #arc-dependency' : 'searchArcDependency',
        'keyup #arc-significance' : 'searchArcSignificance',

        'click #button-graph-showartificial' : 'showArtificial'
    },

    renderRangeLegends: function(){
        var Model = Backbone.Model.extend();
        var rangeModel = new Model({ 
                label: 'Node Range',
                ranges: this.nodesFrequencyRange.reverse()
            });

        var rangeView = new RangeLegendsView({
            el: '#range-nodes',
            model: rangeModel
        });

        var rangeModel = new Model({ 
                label: 'Arc Range',
                ranges: this.arcsFrequencyRange.reverse()
            });

        var rangeView = new RangeLegendsView({
            el: '#range-arcs',
            model: rangeModel
        });
    },

    emptyDigraph: function(){
        var self = this;
        
        if(!_.isUndefined(this.g)){
            if(this.g.nodes().length > 0){
                this.g.nodes().forEach(function(value, key) {
                    self.g.removeNode(value);
                });
            }            
        }
        if(!_.isUndefined(this.g)){
            if(this.g.edges().length > 0){
                this.g.edges().forEach(function(value, key) {
                    self.g.removeEdge(value.w, value.w);
                });
            }
        }
        
        if(this.nodeViews.length > 0){
            this.nodeViews.forEach(function(view, key){
                self.nodeViews.pop();
                view.remove();
            })
        }

        if(this.arcViews.length > 0){
            this.arcViews.forEach(function(view, key){
                self.arcViews.pop();
                view.remove();
            })
        }
    },

    initializeBackboneModels: function(){
        // remove current collection
        if(this.nodeCollection.length > 0){
            this.nodeCollection.remove(this.nodeCollection.models);
        }
        if(this.arcCollection.length > 0){
            this.arcCollection.remove(this.arcCollection.models);
        }
        
        for (var row in this.model.get('nodes')){
            theNode = this.model.get('nodes')[row];
            nodeModel = new NodeModel({
                label: theNode.label, 
                artificial: false,
                attribute: theNode,
                type: this.model.get('type'),
                isLogReplay: this.model.get('isLogReplay'),
                rx: 10,
                ry: 10,
                padding: 10
            }); 

            if(this.model.get('type') === 'delta'){
                if(this.model.get('jsonProcessModelDifferences').nodes[nodeModel.get('label')] ==1){
                    nodeModel.set({ isDifferent: true });   
                }
            }

            this.nodeCollection.push(nodeModel);
        }

        for (var row in this.model.get('arcs')){
            theArc = this.model.get('arcs')[row];

            arcModel = new ArcModel({
                label: row,
                artificial: false,
                attribute: theArc,
                type: this.model.get('type'),
                isLogReplay: this.model.get('isLogReplay')
            });               
            arcModel.set({
                nodeSource: this.nodeCollection.findWhere({label: theArc.source }),
                nodeTarget: this.nodeCollection.findWhere({label: theArc.target }),
            });

            if(this.model.get('type') === 'delta'){
                if(this.model.get('jsonProcessModelDifferences').arcs[arcModel.get('label')] ==1){
                    arcModel.set({ isDifferent: true });
                }
            }
            this.arcCollection.push(arcModel);
        }
    },

    initializeBackboneViews: function(){
        var self = this;
        
        //only load to digraph if node collection is not empty
        if(this.nodeCollection.length > 0){
            this.nodeCollection.forEach(function(nodeModel, key){
                newView = new NodeView({
                    model: nodeModel
                });
                self.nodeViews.push(newView);
            });
        }            

        if(this.arcCollection.length >0){            
            this.arcCollection.forEach(function(arcModel, key){
                newView = new ArcView({
                    model: arcModel
                }); 
                self.arcViews.push(newView);
            });
        }
    },

    deleteAllGraphElement : function() {            
        this.svg.select("g.edgePaths").selectAll("*").remove();
        this.svg.select("g.edgeLabels").selectAll("*").remove();
        this.svg.select("g.nodes").selectAll("*").remove();
        this.svg.select("defs").selectAll("*").remove();
    },

    modifyGraph: function(){
        var self = this;

        // if(this.model.get('isLogReplay')){
            this.svg.select('g.output').append('g').attr('class', 'events');
        // }

        // // set the marker smaller 
        d3.selectAll('marker').attr('markerWidth', 4).attr('markerHeight', 2);
    },

    initializeGraph: function(){
        this.initializeBackboneModels();
        // this.initializeArtificialModels();
        this.renderGraph();
        this.updateGraph(false);
        this.initializeBackboneViews();

        if(this.nodesFrequencyRange.length>0){
            this.renderRangeLegends();
        }
    },

    renderGraph: function(){
        var self = this;
        this.emptyDigraph();

        this.deleteAllGraphElement();

        if(this.nodeCollection.length > 0){
            this.nodesFrequencyRange = this.setClassRange('node', this.nodeFreqClass, true);
            this.nodeCollection.forEach(function(val, key){
                val.setLabelData(self.model.get('type'));
                val.setClass(self.model.get('type'), self.nodesFrequencyRange);
                
                self.g.setNode(
                    val.cid, 
                    { 
                        id: val.cid,
                        labelType: "html",
                        label: self.nodeTemplate(val.get('labelData')),
                        rx: val.get('rx'),
                        ry: val.get('ry'),
                        padding: val.get('padding'),
                        shape: val.get('shape')
                    }
                );            
            });
        }   

        if(this.arcCollection.length > 0){
            this.arcsFrequencyRange = this.setClassRange('arc', this.arcFreqClass, true);
            this.arcsDependencyRange = this.setClassRange('arc', this.arcDepClass, false);
            
            this.arcCollection.forEach(function(val, key){
                val.setLabelData(self.model.get('type'));
                val.setClass(self.model.get('type'), self.arcsFrequencyRange, self.arcsDependencyRange);
                
                self.g.setEdge(
                    val.get('nodeSource').cid,
                    val.get('nodeTarget').cid, 
                    { 
                        id: val.cid,
                        labelType: "html",
                        label: self.arcTemplate(val.get('labelData')), 
                        lineInterpolate: 'basis', 
                        labelpos: 'c',
                        labelId: 'label-'+val.cid
                    }
                );
            });
        }        
    }, 

    updateGraph: function(isUpdate){
        var self = this;
        // Set up zoom support
        if(isUpdate){
            this.nodeCollection.forEach(function(val, key){
                val.set({ destroy: true });
                val.set({ destroy: false });
            });
            this.arcCollection.forEach(function(val, key){
                val.set({ destroy: true });
                val.set({ destroy: false });
            });
        }

        this.zoom = d3.behavior.zoom().on("zoom", function() {
            self.inner.attr("transform", "translate(" + d3.event.translate + ")" + "scale(" + d3.event.scale + ")");
        });
        this.svg.call(this.zoom);

        this.g.setGraph(this.model.get('dagreParam'));

        // Run the renderer. This is what draws the final graph.
        this.renderer(this.inner, this.g);

        this.graphScaleToFit();
        this.modifyGraph();
    },

    // Zoom and scale to fit
    graphScaleToFit: function(){
        var isUpdate = true;            
        var zoomScale = this.zoom.scale();
        var graphWidth = this.g.graph().width + 80;
        var graphHeight = this.g.graph().height + 40;
        var width = parseInt(this.svg.style("width").replace(/px/, ""));
        var height = parseInt(this.svg.style("height").replace(/px/, ""));
        zoomScale = Math.min(width / graphWidth, height / graphHeight);
        var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
        this.zoom.translate(translate);
        this.zoom.scale(zoomScale);
        this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },

    graphScaleToActual: function(){
        var isUpdate = true;            
        var graphWidth = this.g.graph().width + 80;
        var graphHeight = this.g.graph().height + 40;
        var width = parseInt(this.svg.style("width").replace(/px/, ""));
        var height = parseInt(this.svg.style("height").replace(/px/, ""));
        var zoomScale = 1;
        var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
        this.zoom.translate(translate);
        this.zoom.scale(zoomScale);
        this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },

    makeArray : function(num, prefix, sufix) {
        var arr = [];
        for (i = 1; i <= num; i++) {
            arr.push(prefix + i + sufix);
        }
        return arr.reverse();
    },

    /*  make range like this : 
        [ 
          {max: 627, min: 405, theClass: "vertex-freq-1"},
          {max: 848, min: 626, theClass: "vertex-freq-2"},
          {max: 1069, min: 847, theClass: "vertex-freq-3"} 
        ]
     */
    setClassRange: function(type, arrClass, isFrequency){
        var maxValue = undefined;
        var minValue = undefined;
        var result = [];

        if(type=='node' ){
            switch(this.model.get('type')){
                case 'heuristic':
                case 'fuzzy':
                    minValue = _.min(this.nodeCollection.where({artificial:false}), function(val, key){
                        return val.get('attribute').frequency.absolute;
                    }).get('attribute').frequency.absolute;
                    maxValue = _.max(this.nodeCollection.where({artificial:false}), function(val, key){
                        return val.get('attribute').frequency.absolute;
                    }).get('attribute').frequency.absolute;
                    break;
                case 'timegap':
                case 'mtga':
                case 'bayesian':
                case 'delta':
                default:                
                    break;
            }
        }
        else if(type=='arc'){
            if(isFrequency){
                switch(this.model.get('type')){
                    case 'heuristic':
                    case 'fuzzy':
                        minValue = _.min(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').frequency.absolute;
                        }).get('attribute').frequency.absolute;
                        maxValue = _.max(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').frequency.absolute;
                        }).get('attribute').frequency.absolute;
                        break;
                    case 'timegap':
                    case 'mtga':
                    case 'bayesian':
                    case 'delta':
                    default:                
                        break;
                }
            }
            else{
                switch(this.model.get('type')){
                    case 'heuristic':
                        minValue = _.min(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').dependency;
                        }).get('attribute').dependency;
                        maxValue = _.max(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').dependency;
                        }).get('attribute').dependency;
                        break;
                    case 'fuzzy':
                        minValue = _.min(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').significance;
                        }).get('attribute').significance;
                        maxValue = _.max(this.arcCollection.where({artificial: false}), function(val, key){
                            return val.get('attribute').significance;
                        }).get('attribute').significance;
                        break;
                    case 'timegap':
                    case 'mtga':
                    case 'bayesian':
                    case 'delta':
                    default:                
                        break;
                }
            }
        }

        if(!_.isUndefined(minValue) || !_.isUndefined(maxValue)){
            step = (maxValue - minValue)/(arrClass.length);
            var arrRange = _.range(minValue, maxValue, step);
            arrRange.push(maxValue);
            
            if(arrRange.length > arrClass.length){
                arrClass.forEach(function(value, key){
                    result.push({ min: arrRange[key], max: Math.ceil(arrRange[key+1]), class: value });
                });                
            }
            else{
                // if the range value less than class range
                result.push({ min: arrRange[0], max: arrRange[0]+0.01, class: arrClass[0] });
            }
            return result;            
        }
        else{
            return result;
        }
    },

    /* change all node shape 
        model = model of this view
    */
    changeNodeShape: function(model){
        for(var row in this.g._nodes){
            var node = this.g._nodes[row];
            node.shape = model.get('nodeShape');
        }
        this.renderGraph();
        this.initializeBackboneViews();
    },

    changeArcDirectionTo: function(ev){
        this.model.get('dagreParam').rankdir = $(ev.currentTarget).data('arcdirection')
        // this.renderGraph();
        this.updateGraph(true);
        this.initializeBackboneViews();
    },

    /* passing parameter backbone events http://stackoverflow.com/questions/7823556/passing-parameters-into-the-backbone-events-object-of-a-backbone-view */
    changeArcTypeTo: function(ev){
        var arcType = $(ev.currentTarget).data('arctype');
        
        for (var row in this.g._edgeLabels){
            var edge = this.g._edgeLabels[row];
            edge.lineInterpolate = arcType;
        }
        this.updateGraph(true);
        this.initializeBackboneViews();
    },

    fontIncrease: function(){
        var elem = ".vertex li", maxFont = 10, minFont = 6;
        var curSize = parseInt($(elem).css('font-size')) + 1;
        if (curSize <= maxFont) {
            $(elem).css('font-size', curSize);
            $(elem + ' h5').css('font-size', curSize);
        }
    },

    fontDecrease: function(){
        var elem = ".vertex li", maxFont = 10, minFont = 6;
        curSize = parseInt($(elem).css('font-size')) - 1;
        if (curSize >= minFont) {
            $(elem).css('font-size', curSize);
            $(elem + ' h5').css('font-size', curSize);
        }
    },

    searchNodeName: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.nodeCollection.models, 'label');
    },

    searchNodeFrequency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.nodeCollection.models, 'attribute.frequency.absolute');
    },

    searchArcFrequency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'attribute.frequency.absolute');
    },

    searchArcDependency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'dependency');
    },

    searchArcSignificance: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'significance');
    },

    /* function to search element based on search value on sidebar */
    filterModelInCollection: function(text, models, attribute){
        var theText = text.trim().toLowerCase();
        var filtered = [];
        // reset status of each model
        models.forEach(function(model, key){
            model.set({isFound: false});
        });
        
        if(theText!=''){
            var attrs = attribute.split('.'); 
            var filteredModels = [], 
                excludeModels = [];

            if(attrs.length == 1){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }
            else if(attrs.length == 2){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])[attrs[1]]).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }
            else if(attrs.length == 3){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])[attrs[1]][attrs[2]]).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }

            filteredModels.forEach(function(model, key){
                model.set({isFound: true});
            });
                
            // for another models which is not found
            excludeModels = _.difference(models, filteredModels);
            excludeModels.forEach(function(model, key){
                model.set({isFound: false});
            });                    
        }     
    },

    showArtificial: function(){
        var self = this; 
        console.log('show artificial');

        this.model.set({ showArtificial : !this.model.get('showArtificial') })

        if(this.model.get('showArtificial')){
            d3.select('#button-graph-showartificial').classed('btn-success', true);
        }
        else{
            d3.select('#button-graph-showartificial').classed('btn-success', false);
        }

        var filteredArcs = this.arcCollection.where({ artificial: true });
        var filteredNodes = this.nodeCollection.where({ artificial: true });

        filteredArcs.forEach(function(value, key){
            value.set({ isShow: self.model.get('showArtificial') });
        });

        filteredNodes.forEach(function(value, key){
            value.set({ isShow: self.model.get('showArtificial') });
        });
    },

    initializeArtificialModels: function(){
        var self = this;

        var startNodes = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length == 0;
        });

        var endNodes = this.nodeCollection.filter(function(val, key){
            return val.get('nextNodes').models.length == 0;
        });
        
        var repeatedNodes = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length ==1 && val.get('nextNodes').models.length ==1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });
        
        startNodes = startNodes.concat(repeatedNodes);
        endNodes = endNodes.concat(repeatedNodes);

        var repeatedNodesStart = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length ==1 && val.get('nextNodes').models.length > 1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });

        var repeatedNodesEnd = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length > 1 && val.get('nextNodes').models.length == 1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });

        startNodes = startNodes.concat(repeatedNodesStart);
        endNodes = endNodes.concat(repeatedNodesEnd);
        
        var artificialNode = ['Start', 'End'];

        // create artificial node model
        artificialNode.forEach(function(value, key){
            newModel = new NodeModel({
                label: value,
                artificial: true, 
                type: 'circle',
                isShow: true,
            });
            self.nodeCollection.push(newModel);
        });

        // create artificial arc model
        startNodes.forEach(function(value, key){
            newModel = new ArcModel({
                artificial: true,
                isShow: true, 
                label: 'Start | '+value.get('label')
            });               
            newModel.set({
                nodeSource: self.nodeCollection.findWhere({label: 'Start' }),
                nodeTarget: self.nodeCollection.findWhere({label: value.get('label') })
            });
            self.arcCollection.push(newModel);
        });

        endNodes.forEach(function(value, key){
            newModel = new ArcModel({
                artificial: true,
                isShow: true, 
                label: value.get('label')+' | End'
            });               
            newModel.set({
                nodeSource: self.nodeCollection.findWhere({label: value.get('label') }),
                nodeTarget: self.nodeCollection.findWhere({label: 'End' })
            });
            self.arcCollection.push(newModel);
        });
    }
});