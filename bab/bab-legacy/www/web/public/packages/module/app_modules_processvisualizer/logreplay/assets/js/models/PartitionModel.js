var PartitionModel = Backbone.RelationalModel.extend({
	defaults: {
		start: 0, 
		complete: 0, 
		url: '#',
		state: 'state',
		stateClass: 'label label-default',
		timeline: null,
		eventsNumber: 0
	}, 

	relations: [{
		type: Backbone.HasMany,
		key: 'events',
		relatedModel: 'EventRelationalModel',
		collectionType: 'EventCollection',
		reverseRelation: {
			key: 'inPartition',
			includeInJSON: 'id'
			// 'relatedModel' is automatically set to 'Zoo'; the 'relationType' to 'HasOne'.
		}
	}],

	initialize: function(){
		this.set({ 
            start: this.get('start')/1000,
            complete: this.get('complete')/1000
        });
		this.set({ 
			duration: (this.get('complete')-this.get('start')),
			offset: this.get('start')-this.get('startMaster')
		});
		this.on('change:state', this.changeClass, this);
	},

	setStartClientPartition: function(id, start){
		clientPartition = _.findWhere(this.get('clientPartition'),{ id:id });
		clientPartition.start = start/1000;
	},

	setCompleteClientPartition: function(id, complete){
		clientPartition = _.findWhere(this.get('clientPartition'),{ id:id });
		clientPartition.complete = complete/1000;
	},

	changeClass: function(){
		switch(this.get('state')){
			case 'IN': 
				result = 'label label-primary';
				break;
			case 'RD':
				result = 'label label-success';
				break;
			case 'PL':
				result = 'label label-success';
				break;
			case 'ST':
				result = 'label label-info';
				break;
			default:
				result = 'label label-default';
				break;
		};
		this.set({ class: result });
	},

	isStateNotAvailable: function(){
		if(this.get('state') == 'NA')
			return true;
		else
			return false;
	},


	isStateStored: function(){
		if(this.get('state') == 'ST')
			return true;
		else
			return false;
	},
	
	isStateInitialized: function(){
		if(this.get('state') == 'IN')
			return true;
		else
			return false;
	},

	isStatePlay: function(){
		if(this.get('state') == 'PL')
			return true;
		else
			return false;
	},
	
});