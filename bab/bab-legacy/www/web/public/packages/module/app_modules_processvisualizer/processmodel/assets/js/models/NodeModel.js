var NodeModel = Backbone.RelationalModel.extend({
    defaults: {
        label: 'The Name',
        shape: 'rect',
        isFound: false,
        isDifferent: false,
        isClustered: false,
        attribute: {},
        destroy: false
    },

    relations:[
        {
            type: Backbone.HasMany,
            key: 'nextNodes',
            relatedModel: 'NodeModel',
            collectionType: 'NodeCollection',
        },
        {
            type: Backbone.HasMany,
            key: 'prevNodes',
            relatedModel: 'NodeModel',
            collectionType: 'NodeCollection',
        }, 
    ], 

    initialize: function(){
        this.setShape();

        // add additional attribute if 
        if(this.get('isLogReplay')){
            this.set({
                tokensCreated: 0,
                tokensFinished: 0                
            })
        }
    },

    setShape: function(){
        if(this.get('label').toLowerCase().indexOf('cluster') > -1){
            this.set({shape: 'circle'});
        }
        else{
            this.set({shape: 'rect'});
        }
    },

    getClassRange: function(classRange, number){
        return _.find(classRange, function(value){
            return number >= value.min && number <= value.max;
        });
    },

    setClass: function(processModelType, classRange){
        var cssClass = undefined ;
        if(this.get('artificial')){
            cssClass = 'vertex-artificial';
        }
        else{
            switch(processModelType){
                case 'heuristic':
                    var result = this.getClassRange(classRange, this.get('attribute').frequency.absolute);
                    cssClass = result.class ;
                    break;
                case 'fuzzy':
                    var result = this.getClassRange(classRange, this.get('attribute').frequency.absolute);
                    if (S(this.get('label')).contains('cluster')){
                        cssClass = 'vertex-cluster ' + result.class;
                    } 
                    else {
                        cssClass = result.class;
                    }
                    break;
                case 'timegap':
                    cssClass = 'vertex-timegap';
                    break;
                case 'bayesian':
                    cssClass = 'vertex-bayesian';
                    break;
                case 'mtga':
                    cssClass = 'color-' + this.get('attribute').color;
                    break;
                case 'delta':
                    cssClass = 'vertex-delta'
                    break;
            }

            if(this.get('isLogReplay')){
                cssClass += ' vertex-logreplay';
            }
        }

        if(this.get('isDifferent')){
            cssClass += ' found';
        }

        this.set({ class: 'vertex enter ' + cssClass });
    },

    setLabelData: function(processModelType){
        var data = {
                label: this.get('label'),
                type: this.get('type'),
                isLogReplay: this.get('isLogReplay'),
                attributes: []
            }

        if(!this.get('artificial')){
            switch(processModelType){
                case 'delta':
                case 'heuristic':
                    data.attributes = [                        
                            { key:'frequency', value: numeral(this.get('attribute').frequency.absolute).format('0,0') }
                        ];
                    break;
                case 'fuzzy':
                    data.attributes= [
                            { key:'significance', value: numeral(this.get('attribute').significance).format('0,0.0') }
                        ];
                    break;
                case 'bayesian':
                    var posterior = this.get('attribute').posteriorProbability;
                    data.attributes.push({ key: '-', value: '---' });
                    for(var key in posterior){
                        data.attributes.push({ key: key, value: key+': '+posterior[key] });
                    }
                    break;
                case 'mtga':
                    var temp = this.get('attribute').dimension.split('|');
                    var attributes = [{key:'eventName', value:this.get('attribute').eventName }];
                    if(temp.length > 1){
                        temp.forEach(function(val, key){
                            attributes.push({ key:'dimension'+key, value:val });
                        })
                    }
                    else{
                        attributes.push({ key: 'dimension1', value: temp[0] });
                    }
                    attributes.push({key:'timestamp', value:this.get('attribute').timestamp });

                    data.label = this.get('attribute').caseID;
                    data.attributes = attributes;
                    break ;
                case 'timegap':
                default:
                    data.attributes = [];
                    break;
            }
        }
        this.set({ labelData : data });
    }
});