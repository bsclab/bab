/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;

import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;

public class HdfsUtil {
	
	public static boolean isFileExists(SparkExecutor se, String uri) {
		try {
			return (se.getHdfsFileSystem().exists(new Path(uri)));
		} catch (Exception e) {

		}
		return false;
	}

	public static void saveAsTextFile(SparkExecutor se, String uri, String data) {
		try {
			FSDataOutputStream out = se.getHdfsFileSystem().create(
					new Path(uri));
			out.write(data.getBytes("UTF-8"));
			out.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String loadTextFile(SparkExecutor se, String uri) {
		try {
			FSDataInputStream is = se.getHdfsFileSystem().open(new Path(uri));
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {

		}
		return null;
	}
}
