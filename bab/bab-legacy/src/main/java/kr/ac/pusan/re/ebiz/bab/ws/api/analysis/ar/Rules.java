/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar;

import java.io.Serializable;
import java.util.Set;

public class Rules implements Serializable, Comparable<Rules> {
	private static final long serialVersionUID = 1L;

	public Set<String> getLeft() {
		return left;
	}

	public void setLeft(Set<String> left) {
		this.left = left;
	}

	public Set<String> getRight() {
		return right;
	}

	public void setRight(Set<String> right) {
		this.right = right;
	}

	private double confidence;
	private double support;
	private Set<String> left;
	private Set<String> right;
	
	public Rules() {
		
	}
	
	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	public double getSupport() {
		return support;
	}

	public void setSupport(double support) {
		this.support = support;
	}

	public Rules(double confidence, double support) {
		this.confidence = confidence;
		this.support = support;
	}

	@Override
	public int compareTo(Rules o) {
		if (confidence == o.confidence) {
			if (support == o.support) {
				return 0;
			} else if (support > o.support) {
				return -1;
			} else {
				return 1;
			}
		} else if (confidence > o.confidence){
			return -1;
		} else {
			return 1;
		}
	}
}