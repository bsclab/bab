/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config;

import java.io.Serializable;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config.Option;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config.Threshold;

public class HeuristicMinerJobConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;

	private String repositoryURI;
	private Threshold threshold = new Threshold();
	private int positiveObservation;
	private Option option = new Option();
	private String[] cases = new String[0];

	public HeuristicMinerJobConfiguration() {

	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public Threshold getThreshold() {
		return threshold;
	}

	public void setThreshold(Threshold threshold) {
		this.threshold = threshold;
	}

	public int getPositiveObservation() {
		return positiveObservation;
	}

	public void setPositiveObservation(int positiveObservation) {
		this.positiveObservation = positiveObservation;
	}

	public Option getOption() {
		return option;
	}

	public void setOption(Option option) {
		this.option = option;
	}

	public String[] getCases() {
		return cases;
	}

	public void setCases(String[] cases) {
		this.cases = cases;
	}

}
