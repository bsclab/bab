/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HeatCasesModel implements Serializable{
	private Map<String, String> caseList = new TreeMap<String, String>();
	private Map<String, Set<String>> caseMap = new TreeMap<String, Set<String>>();
	
	public Map<String, String> getCaseList() {
		return caseList;
	}
	public void setCaseList(Map<String, String> caseList) {
		this.caseList = caseList;
	}
	
	public HeatCasesModel putCaseList(Map<String, String> e){
		caseList.putAll(e);
		return this;
	}
	
	public Map<String, Set<String>> getCaseMap() {
		return caseMap;
	}
	public void setCaseMap(Map<String, Set<String>> caseMap) {
		this.caseMap = caseMap;
	}
	public HeatCasesModel putCaseMap(Map<String, Set<String>> e){
		caseMap.putAll(e);
		return this;
	}
}