/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public abstract class AbstractMetric implements Serializable, Metric {
	private static final long serialVersionUID = 1L;

	protected Map<String, Double> measured = new TreeMap<String, Double>();

	@Override
	public Set<String> getLabels() {
		return measured.keySet();
	}
	
	@Override
	public double getSignificance(String label) {
		return measured.containsKey(label) ? measured.get(label) : 0;
	}
	
	@Override
	public void aggregate(Metric metric) {
		for (String s : metric.getLabels()) {
			measured.put(s, getSignificance(s) + metric.getSignificance(s));
		}
	}
	
	@Override
	public void divideAll(double denom) {
		for (String s : getLabels()) {
			measured.put(s, getSignificance(s) / denom);
		}
	}

	@Override
	public void normalize(double max) {
		double m = Double.MIN_VALUE;
		for (String s : measured.keySet()) {
			double v = measured.get(s);
			if (v > m) m = v;
		}
		if (m == 0) return;
		for (String s : measured.keySet()) {
			double v = measured.get(s);
			v = v / m * max;
			measured.put(s, v);
		}
	}
}