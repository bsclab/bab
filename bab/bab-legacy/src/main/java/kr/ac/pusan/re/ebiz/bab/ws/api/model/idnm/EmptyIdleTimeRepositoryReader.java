/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.idnm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
//import org.apache.spark.storage.StorageLevel;

import com.google.common.base.Strings;

import scala.Tuple2;
import flexjson.ClassLocator;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.ObjectBinder;
import flexjson.ObjectFactory;
import flexjson.factories.ClassLocatorObjectFactory;
import flexjson.factories.MapObjectFactory;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;

public class EmptyIdleTimeRepositoryReader extends IdleTimeRepositoryReader implements
		Serializable {
	private static final long serialVersionUID = 1L;


	private SparkExecutor executor;
	private IRepository repository;
	private JavaPairRDD<String, ICase> casesRDD;
	private Map<String, ICase> cases;
	private String repositoryURI;

	public EmptyIdleTimeRepositoryReader(SparkExecutor se, String repositoryURI) {
		super(se, repositoryURI);
		this.executor = se;
		try {
			this.repositoryURI = repositoryURI;
			ClassLocatorObjectFactory objectFactory = new ClassLocatorObjectFactory(
					new ClassLocator() {
						@Override
						public Class<?> locate(ObjectBinder arg0,
								flexjson.Path arg1)
								throws ClassNotFoundException {
							return String.class;
						}
					});
			String repo = HdfsUtil.loadTextFile(se,
					se.getHdfsURI(repositoryURI + ".brepo"));
			this.repository = new BRepository();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IRepository getRepository() {
		return repository;
	}

	@Override
	public JavaPairRDD<String, ICase> getCasesRDD() {

		if (casesRDD == null) {
			JavaSparkContext jsc = executor.getContext();
			List<Tuple2<String, ICase>> emptyList = new ArrayList<Tuple2<String,ICase>>();
			casesRDD = jsc.parallelizePairs(emptyList);
		}
		return casesRDD;
	}

	@Override
	public Map<String, ICase> getCases() {
		if (cases == null) {
			cases = getCasesRDD().collectAsMap();
		}
		return cases;
	}

}
