/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="process")
public class Process {
	
	private String id;
	private String name;
	private boolean isClosed;
	private boolean isExecutable;
	private String processType = "None";
	
	public String getId() {
		return id;
	}
	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}
	public boolean isClosed() {
		return isClosed;
	}
	@XmlAttribute(name="isClosed")
	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}
	public boolean isExecutable() {
		return isExecutable;
	}
	@XmlAttribute(name="isExecutable")
	public void setExecutable(boolean isExecutable) {
		this.isExecutable = isExecutable;
	}
	public String getProcessType() {
		return processType;
	}
	@XmlAttribute(name="processType")
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	
	private final List<Element> elements = new ArrayList<Element>();

	@XmlElementRef
	public List<Element> getElements() {
		return elements;
	}

	
}
