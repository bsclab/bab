/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar;

import java.util.*;
import java.util.TreeMap;

public class AssociationRuleMiner {
	private Map<String, Rules> associationRules = new TreeMap<String, Rules>();
	private long totalTransactions = 0;
	private int noOfRelation = 0;

	public Map<String, Rules> getAssociationRules() {
		return associationRules;
	}

	public void setAssociationRules(Map<String, Rules> associationRules) {
		this.associationRules = associationRules;
	}

	public long getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(long totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public int getNoOfRelation() {
		return noOfRelation;
	}

	public void setNoOfRelation(int noOfRelation) {
		this.noOfRelation = noOfRelation;
	}
	
}
