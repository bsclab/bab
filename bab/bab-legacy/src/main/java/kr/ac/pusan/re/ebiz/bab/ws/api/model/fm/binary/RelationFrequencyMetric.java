/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.binary;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.AbstractMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.BinaryMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.Metric;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;

public class RelationFrequencyMetric extends AbstractMetric implements BinaryMetric {
	private static final long serialVersionUID = 1L;


	@Override
	public double measure(IEvent refEvent, int refIndex, IEvent folEvent,
			int folIndex) {
		double result = 0;
		String refLabel = refEvent.getLabel() + " (" + refEvent.getType() + ")";
		String folLabel = folEvent.getLabel() + " (" + folEvent.getType() + ")";
		String label = refLabel + "=>" + folLabel;
		measured.put(label, getSignificance(label) + 1);
		return result;
	}


	@Override
	public Metric cloneMetric() {
		Metric result = new RelationFrequencyMetric();
		result.aggregate(this);
		return result;
	}
}
