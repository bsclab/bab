/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.binary.RelationFrequencyMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.binary.RelationProximityMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.AggregationMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.BinaryMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.Metric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.UnaryMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.unary.ActivityFrequencyMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.FuzzyModel;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.FuzzyModel.Arc;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.FuzzyModel.Node;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;


public class FuzzyMinerJob extends AbstractModelJob {
	private static final long serialVersionUID = 1L;


	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			FuzzyMinerJobConfiguration config = new JSONDeserializer<FuzzyMinerJobConfiguration>().deserialize(json, FuzzyMinerJobConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());

			SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());
			IRepository repository = reader.getRepository();

			JavaPairRDD<String, Metric> metricsRDD = reader.getCasesRDD().mapPartitionsToPair(new PairFlatMapFunction<Iterator<Tuple2<String,ICase>>, String, Metric>() {
				@Override
				public Iterable<Tuple2<String, Metric>> call(
						Iterator<Tuple2<String, ICase>> arg0) throws Exception {
					List<Tuple2<String, Metric>> result = new ArrayList<Tuple2<String, Metric>>();
					result.add(new Tuple2<String, Metric>("afm", (Metric) new ActivityFrequencyMetric()));
					result.add(new Tuple2<String, Metric>("rfm", (Metric) new RelationFrequencyMetric()));
					//result.add(new Tuple2<String, Metric>("rpm", (Metric) new RelationProximityMetric()));
					int lookBackSize = 5;
					while (arg0.hasNext()) {
						ICase c = arg0.next()._2();
						List<IEvent> lookBack = new ArrayList<IEvent>(lookBackSize); 
						int i = 0;
						IEvent r = null;
						for (IEvent e : c.getEvents().values()) {
							for (Tuple2<String, Metric> calculator : result) {
								Metric metric = calculator._2();
								if (metric instanceof UnaryMetric) {
									((UnaryMetric) metric).measure(e, i);
								} else if (metric instanceof BinaryMetric) {
									if (r != null) {
										((BinaryMetric) metric).measure(r, i - 1, e, i);
									}
									r = e;
									/*
									int j = 0;
									for (IEvent r : lookBack) {
										((BinaryMetric) metric).measure(r, i - j, e, i);
										j++;
									}
									lookBack.add(0, e);
									lookBack.remove(lookBack.size() - 1);
									*/
								}
							}
							i++;
						}
					}
					return result;
				}
			}).reduceByKey(new Function2<Metric, Metric, Metric>() {
				@Override
				public Metric call(Metric arg0, Metric arg1) throws Exception {
					Metric r = arg0.cloneMetric();
					r.aggregate(arg1);
					return r;
				}
			});
			
			List<Tuple2<String, Metric>> metrics = metricsRDD.collect();
			ActivityFrequencyMetric afm = null;
			for (Tuple2<String, Metric> met : metrics) {
				if (met._1().compareTo("afm") == 0) {
					afm = (ActivityFrequencyMetric) met._2();
					break;
				}
			}

			AggregationMetric nodeMetricFreq = new AggregationMetric();
			AggregationMetric arcMetricFreq = new AggregationMetric();
			for (Tuple2<String, Metric> met : metrics) {
				Metric m = met._2();
				if (m instanceof UnaryMetric) {
					nodeMetricFreq.aggregate(m);
				} else if (m instanceof BinaryMetric){
					arcMetricFreq.aggregate(m);
				}
			}

			FuzzyModel model = new FuzzyModel();
			for (String s : nodeMetricFreq.getLabels()) {
				Node n = model.getOrAddNode(s);
				n.getFrequency().setAbsolute((int) nodeMetricFreq.getSignificance(s));
			}
			
			for (String s : arcMetricFreq.getLabels()) {
				String[] ss = s.split("=>");
				String source = ss[0];
				String target = ss[1];
				Arc a = model.getOrAddArc(source, target);
				a.getFrequency().setAbsolute((int) arcMetricFreq.getSignificance(s));
			}
			
			double totalEvents = 0;
			for (String label : afm.getLabels()) {
				totalEvents += afm.getSignificance(label);
			}			
			afm.divideAll(totalEvents);
			afm.normalize(1);
			RelationFrequencyMetric rfm = null;
			for (Tuple2<String, Metric> met : metrics) {
				if (met._1().compareTo("rfm") == 0) {
					rfm = (RelationFrequencyMetric) met._2();
					break;
				}
			}
			double totalRelations = 0;
			for (String label : rfm.getLabels()) {
				totalRelations += rfm.getSignificance(label);
			}
			rfm.divideAll(totalRelations);
			rfm.normalize(1);


			AggregationMetric nodeMetric = new AggregationMetric();
			AggregationMetric arcMetric = new AggregationMetric();
			for (Tuple2<String, Metric> met : metrics) {
				Metric m = met._2();
				if (m instanceof UnaryMetric) {
					nodeMetric.aggregate(m);
				} else if (m instanceof BinaryMetric){
					arcMetric.aggregate(m);
				}
			}
			nodeMetric.completeAggregation();
			arcMetric.completeAggregation();

			for (String s : nodeMetric.getLabels()) {
				Node n = model.getOrAddNode(s);
				n.setSignificance(nodeMetric.getSignificance(s));
			}
			
			for (String s : arcMetric.getLabels()) {
				String[] ss = s.split("=>");
				String source = ss[0];
				String target = ss[1];
				Arc a = model.getOrAddArc(source, target);
				a.setSignificance(arcMetric.getSignificance(s));
			}
			
			RawJobResult result = new RawJobResult("model.Fuzzy", outputURI, outputURI, serializer.exclude("*.class").serialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".fmodel", result.getResponse());		
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
