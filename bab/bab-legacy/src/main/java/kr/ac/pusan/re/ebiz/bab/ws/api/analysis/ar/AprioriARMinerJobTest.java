//
//  @ Project : Log Miner Web Service
//  @ File Name : AprioriARMinerJob.java
//  @ Date : 7/14/2014
//  @ Author : Iq Reviessay Pulshashi
//



package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;


public class AprioriARMinerJobTest extends AssociationRuleMinerJob {
	private static final long serialVersionUID = 1L;


	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		JavaSparkContext sc = se.getContext();
		FileSystem fs = se.getHdfsFileSystem();
		JSONSerializer serializer = new JSONSerializer();
		String outputURI = se.getHdfsURI(res.getUri());
		AssociationRuleMinerConfiguration config = new JSONDeserializer<AssociationRuleMinerConfiguration>().deserialize(json, AssociationRuleMinerConfiguration.class);
		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());

		
		List<Integer> a = IntStream.range(1, 1000).parallel().boxed().collect(Collectors.toList());
		
		JavaRDD<String> aR = sc.parallelize(a, 5)
		.mapPartitions(new FlatMapFunction<Iterator<Integer>, String>() {
			private static final long serialVersionUID = 1L;


			@Override
			public Iterable<String> call(Iterator<Integer> ints)
				throws Exception {
				List<String> result = new ArrayList<String>();
				ints.forEachRemaining(i -> result.add(String.valueOf(i)));
				return result;
			}
		});

		aR.collect().forEach(i -> System.err.println(i));
				
		return null;
	}

	
}
