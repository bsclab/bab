/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicModel.Arc;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicModel.Node;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.model.RepositoryResource;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaSparkContext;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;


public class LinearHeuristicMinerJob extends AbstractModelJob {
	private static final long serialVersionUID = 1L;

	
	private HeuristicModel model;
	
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			HeuristicMinerJobConfiguration config = new JSONDeserializer<HeuristicMinerJobConfiguration>().deserialize(json, HeuristicMinerJobConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());

			IRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI()); 
			if (res instanceof RepositoryResource) {
				reader = ((RepositoryResource) res).getRepositoryReader();
			}
			IRepository repository = reader.getRepository();
			Map<String, ICase> cases = reader.getCases();
			model = new HeuristicModel();
			
			Map<String, Integer> modelElements = new LinkedHashMap<String, Integer>();
			for (String caseId : cases.keySet()) {
				ICase icase = cases.get(caseId);
				for (String eventId : icase.getEvents().keySet()) {
					IEvent ievent = icase.getEvents().get(eventId);
					String modelElement = ievent.getLabel() + " (" + ievent.getType() + ")";
					if (!modelElements.containsKey(modelElement)) 
						modelElements.put(modelElement, modelElements.size());
				}
			}
			
			int size = modelElements.size();
			int[] events = new int[size];
			int[] startCounts = new int[size];
			int[] endCounts = new int[size];
			double[][] directSuccessions = new double[size][size];
			double[][] successions2 = new double[size][size];
			for (String caseId : cases.keySet()) {
				ICase icase = cases.get(caseId);
				String prev = null;
				String start = null;
				String end = null;
				for (String eventId : icase.getEvents().keySet()) {
					IEvent ievent = icase.getEvents().get(eventId);
					String modelElement = ievent.getLabel() + " (" + ievent.getType() + ")";
					events[modelElements.get(modelElement)]++;
					if (start == null) start = modelElement;
					end = modelElement;
					if (prev == null) {
						prev = modelElement;
						continue;
					}
					int from = modelElements.get(prev);
					int to = modelElements.get(modelElement);
					directSuccessions[from][to]++;
					prev = modelElement;
				}
				if (start != null) startCounts[modelElements.get(start)]++;
				if (end != null) endCounts[modelElements.get(end)]++;
			}
			for (String caseId : cases.keySet()) {
				ICase icase = cases.get(caseId);
				int[] memory = new int[2];
				
				int i = 0;
				if (icase.getEvents().size() == 0) continue;
				Iterator<IEvent> ateit = icase.getEvents().values().iterator();
				while (ateit.hasNext()) {
					IEvent ate = ateit.next();
					int index = modelElements.get(ate.getLabel() + " (" + ate.getType() + ")");
					if (i < 2) {
						memory[i] = index;
						i++;
						continue;
					}
					if (memory[0] == index) {
						for (int j = 0; j < 2; j++) {
							successions2[index][memory[j]]++;
						}
					}
					for (int j = 0; j < 2 - 1; j++) {
						memory[j] = memory[j + 1];
					}
					memory[1] = index;
				}
			}
			
			double[][] longRangeSuccessions = new double[size][size];
			double[][] casualSuccessions = new double[size][size];
			double[][] longRangeDependencyMeasures = new double[size][size];
			for (String caseId : cases.keySet()) {
				ICase icase = cases.get(caseId);
				int i = 0;
				boolean terminate = false;
				while (!terminate) {
					Iterator<IEvent> ate = icase.getEvents().values().iterator();
					for (int j = 0; j < i; j++) {
						ate.next();
					}
					if (!ate.hasNext()) break;
					IEvent begin = ate.next();
					int from = modelElements.get(begin.getLabel() + " (" + begin.getType() + ")");
					int distance = 0;
					boolean foundSelf = false;
					Set<Integer> done = new LinkedHashSet<Integer>();
					terminate = !ate.hasNext();
					while (ate.hasNext() && !foundSelf) {
						IEvent end = ate.next();
						int to = modelElements.get(end.getLabel() + " (" + end.getType() + ")");
						foundSelf = (from == to);
						distance++;
						if (done.contains(to)) continue;
						done.add(to);
						longRangeSuccessions[from][to]++;
						casualSuccessions[from][to] += Math.pow(0.8, distance - 1);
					}
					i++;
				}
			}

			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (casualSuccessions[i][j] != 0) casualSuccessions[i][j] = casualSuccessions[i][j] / longRangeSuccessions[i][j];
					if (events[i] != 0) longRangeDependencyMeasures[i][j] = (longRangeSuccessions[i][j] / (events[i] + config.getThreshold().getDependencyDivisor())) - (5.0 * (Math.abs(events[i] - events[j])) / events[i]); 
				}
			}
			
			double[][] dependency = new double[size][size];
			
			int bestStart = 0;
			int bestEnd = 0;
			double[] bestInputMeasure = new double[size];
			double[] bestOutputMeasure = new double[size];
			int[] bestInputEvent = new int[size];
			int[] bestOutputEvent = new int[size];

			boolean[] l1lRelations = new boolean[size];
			int[] l2lRelations = new int[size];
			boolean[] alwaysVisited = new boolean[size];
			
			double[] l1ldependency = new double[size];
			for (int i = 0; i < size; i++) {
				if (startCounts[i] > startCounts[bestStart]) bestStart = i;
				if (endCounts[i] > endCounts[bestEnd]) bestEnd = i;
				bestInputMeasure[i] = -10.0;
				bestOutputMeasure[i] = -10.0;
				bestInputEvent[i] = -1;
				bestOutputEvent[i] = -1;

				double l1lmeasure = (double)directSuccessions[i][i] / (double)(directSuccessions[i][i] + config.getThreshold().getDependencyDivisor());
				l1ldependency[i] = l1lmeasure;
				if (l1lmeasure >= config.getThreshold().getL1Loop() && directSuccessions[i][i] >= config.getPositiveObservation()) {
					dependency[i][i] = l1lmeasure;
					l1lRelations[i] = true;
				}
			}
			
			double[][] l2ldependency = new double[size][size];
			double[][] abdependency = new double[size][size];
			double[][] noiseCounters = new double[size][size];
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					double l2lmeasure = ((l1lRelations[i] && successions2[i][j] >= config.getPositiveObservation()) || (l1lRelations[j] && successions2[j][i] >= config.getPositiveObservation())) 
						? 0.0
						: ((double)(successions2[i][j] + successions2[j][i]) / (double)(successions2[i][j] + successions2[j][i] + config.getThreshold().getDependencyDivisor()));
					l2ldependency[i][j] = l2lmeasure;
					l2ldependency[j][i] = l2lmeasure;
					if (i != j) {
						if (l2lmeasure >= config.getThreshold().getL2Loop() && successions2[i][j] + successions2[j][i] >= config.getPositiveObservation()) {
							dependency[i][j] = l2lmeasure;
							dependency[j][i] = l2lmeasure;
							l2lRelations[i] = j;
							l2lRelations[j] = i;
						}
						double abmeasure = ((double)directSuccessions[i][j] - directSuccessions[j][i]) / (double)(directSuccessions[i][j] + directSuccessions[j][i] + config.getThreshold().getDependencyDivisor());
						abdependency[i][j] = abmeasure;
						if (abmeasure > bestInputMeasure[i]) {
							bestInputMeasure[i] = abmeasure;
							bestInputEvent[i] = j;
						}
						if (abmeasure > bestOutputMeasure[i]) {
							bestOutputMeasure[i] = abmeasure;
							bestOutputEvent[i] = j;
						}
					}
				}
				if (i != bestStart && i != bestEnd) {
					for (int j = 0; j < size; j++) {
						if (l2ldependency[i][j] > bestInputMeasure[i]) {
							dependency[i][j] = l2ldependency[i][j];
							dependency[j][i] = l2ldependency[j][i];
							l2lRelations[i] = j;
							l2lRelations[j] = i;
						}
					}
				}
			}
			
			if (config.getOption().isAllConnected()) {
				for (int i = 0; i < size; i++) {
					int j = l2lRelations[i];
					if (i != bestStart) {
						if (j > -1 && bestInputMeasure[j] > bestInputMeasure[i]) {
							// Do nothing
						} else {
							dependency[bestInputEvent[i]][i] = bestInputMeasure[i];
							noiseCounters[bestInputEvent[i]][i] = directSuccessions[i][bestInputEvent[i]];
						}
					}
					if (i != bestEnd) {
						if (j > -1 && bestOutputMeasure[j] > bestOutputMeasure[i]) {
							// Do nothing
						} else {
							dependency[bestOutputEvent[i]][i] = bestOutputMeasure[i];
							noiseCounters[bestOutputEvent[i]][i] = directSuccessions[bestOutputEvent[i]][i];
						}
					}
				}
			}
			
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (dependency[i][j] <= 0.0001) {
						if (bestOutputMeasure[i] - abdependency[i][j] <= config.getThreshold().getRelativeToBest()
							&& directSuccessions[i][j] >= config.getPositiveObservation() 
							&& abdependency[i][j] >= config.getThreshold().getDependency()) {
							dependency[i][j] = abdependency[i][j];
							noiseCounters[i][j] = directSuccessions[j][i];
						}
					}
				}
			}
			
			if (config.getOption().isLongDistance()) {
				alwaysVisited[bestStart] = false;
				for (int i = 1; i < size; i++) {
					// NOT FINISH YET
				}
			}
			
			String[] nodes = new String[size];
			for (String s : modelElements.keySet()) {
				nodes[modelElements.get(s)] = s;
				Node n = model.getOrAddNode(s);
				n.getFrequency().setAbsolute(events[modelElements.get(s)]); 
			}
			
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (dependency[i][j] > 0 && directSuccessions[i][j] > 0) {
						String from = nodes[i];
						String to = nodes[j];
						Arc a = model.getOrAddArc(from, to);
						a.setDependency(dependency[i][j]);
						a.getFrequency().setAbsolute((int) directSuccessions[i][j]); 
					}
				}
			}			
			
			RawJobResult result = new RawJobResult("model.Heuristic", outputURI, outputURI, serializer.exclude("*.class").serialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".hmodel", result.getResponse());		
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public HeuristicModel getModel() {
		return model;
	}
}
