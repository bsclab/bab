/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import scala.Tuple2;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader2;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawResource;

public class HeuristicMinerJob2 extends AbstractModelJob {
	private static final long serialVersionUID = 1L;


	private HeuristicModel model;
	
	public HeuristicModel getModel() {
		return model;
	}
	
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			HeuristicMinerJobConfiguration config = new JSONDeserializer<HeuristicMinerJobConfiguration>().deserialize(json, HeuristicMinerJobConfiguration.class);
			
			SparkRepositoryReader2 		reader					= new SparkRepositoryReader2(se, config.getRepositoryURI());
				IRepository 				repository 				= reader.getRepository();
				JavaPairRDD<String, ICase> 	traces 					= reader.getCasesRDD();
			
			Algorithm1ActivityMapping 	step1ActivityMapping 	= new Algorithm1ActivityMapping(repository);
				Map<String, Integer> 		activitySet 			= step1ActivityMapping.getActivities();
				
			Algorithm2FrequencyMiner 	step2FrequencyMiner 	= new Algorithm2FrequencyMiner(activitySet);
				JavaPairRDD<String, Vector> frequencySet 			= step2FrequencyMiner.mine(traces);
			
			Algorithm3DependencyMiner	step3DependencyMiner	= new Algorithm3DependencyMiner(config);
				JavaPairRDD<Integer, Activity> 	dependencySet		= step3DependencyMiner.mine(frequencySet);
				
			Algorithm4DependencyFilter 	step4DependencyFilter	= new Algorithm4DependencyFilter(config);
				JavaPairRDD<Integer, Activity> 	filteredDependencySet 	= step4DependencyFilter.mine(dependencySet);
				
			Map<Integer, Activity> dependencyGraph = filteredDependencySet.collectAsMap();
			
			model = new HeuristicModel();
			String[] activityLabels = activitySet.keySet().toArray(new String[0]);
			for (String a : activityLabels) {
				model.getOrAddNode(a);
			}
			for (int ai : dependencyGraph.keySet()) {
				String as 	= activityLabels[ai];
				Activity a	= dependencyGraph.get(ai);
				Dependency d = a.getL1();
				if (d != null && d.isAccepted()) {
					HeuristicModel.Arc r = model.getOrAddArc(as, as, d.getFrequency(), d.getDependency());
				}
				for (int ri : a.getInput().keySet()) {
					String rs = activityLabels[ri];
					d = a.getInput().get(ri);
					if (d != null && d.isAccepted()) {
						HeuristicModel.Arc r = model.getOrAddArc(rs, as, d.getFrequency(), d.getDependency());
					}
				}
				for (int ri : a.getOutput().keySet()) {
					String rs = activityLabels[ri];
					d = a.getOutput().get(ri);
					if (d != null && d.isAccepted()) {
						HeuristicModel.Arc r = model.getOrAddArc(as, rs, d.getFrequency(), d.getDependency());
					}
				}
				for (int ri : a.getInputL2().keySet()) {
					String rs = activityLabels[ri];
					d = a.getInputL2().get(ri);
					if (d != null && d.isAccepted()) {
						HeuristicModel.Arc r = model.getOrAddArc(rs, as, d.getFrequency(), d.getDependency());
					}
				}
				for (int ri : a.getOutputL2().keySet()) {
					String rs = activityLabels[ri];
					d = a.getOutputL2().get(ri);
					if (d != null && d.isAccepted()) {
						HeuristicModel.Arc r = model.getOrAddArc(as, rs, d.getFrequency(), d.getDependency());
					}
				}
			}
			
			String outputURI 	= se.getHdfsURI(res.getUri());
			RawJobResult result = new RawJobResult("model.Heuristic2", outputURI, outputURI, new JSONSerializer().exclude("*.class").serialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".hmodel2", result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public class Algorithm1ActivityMapping {
		public static final String STRING_DIVIDER 			= "|";
		public static final String STRING_START_ACTIVITY 	= "Start" 	+ STRING_DIVIDER + "Artificial";
		public static final String STRING_END_ACTIVITY 		= "End" 	+ STRING_DIVIDER + "Artificial";
		private final Map<String, Integer> activities;

		public Algorithm1ActivityMapping(IRepository repository) {
			activities = new LinkedHashMap<String, Integer>();
			activities.put(STRING_START_ACTIVITY, activities.size());
			for (String t : repository.getActivityTypes().keySet()) {
				for (String a : repository.getActivities().keySet()) {
					activities.put(a + STRING_DIVIDER + t, activities.size());
				}
			}
			activities.put(STRING_END_ACTIVITY, activities.size());
		}

		public Map<String, Integer> getActivities() {
			return activities;
		}
	}

	public class Algorithm2FrequencyMiner implements
			PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>,
			Function2<Vector, Vector, Vector> {

		public static final int AFINDEX_START 	= 0;
		public static final int AFINDEX_OCCUR 	= 1;
		public static final int AFINDEX_END 	= 2;
		public static final int AFINDEX_L1	 	= 3;
		public static final int RFINDEX_AB 		= 0;
		public static final int RFINDEX_BA 		= 1;
		public static final int RFINDEX_ABL2 	= 2;
		public static final int RFINDEX_BAL2 	= 3;
		
		private final Map<String, Integer> activities;

		public Algorithm2FrequencyMiner(Map<String, Integer> activities) {
			this.activities = activities;
		}

		@Override
		public Iterable<Tuple2<String, Vector>> call(Tuple2<String, ICase> arg0)
				throws Exception {
			String 	traceId = arg0._1();
			ICase 	trace 	= arg0._2();
			// Previous activity 2
			int		pa2	= -1;
			// Previous activity
			int 	pa	= -1;
			
			// A 	=> AAsStartFreq,AFreq,AAsEndFreq,AL1LoopFreq
				// A	=> AL1LDependency
			// A|B 	=> AtoBFreq,BtoAFreq,AtoBL2LoopFreq,BtoAL2LoopFreq
				// 
			
			Map<Integer, Double[]> eF = new LinkedHashMap<Integer, Double[]>();
			Map<String, Double[]> erF = new LinkedHashMap<String, Double[]>();
			Double[] aFreqs = null, rFreqs = null;
			int a = -1;
			for (IEvent e : trace.getEvents().values()) {
				String aStr = e.getLabel() + Algorithm1ActivityMapping.STRING_DIVIDER + e.getType();
				if (!activities.containsKey(aStr)) continue;
				a = activities.get(aStr);
				if (!eF.containsKey(a)) eF.put(a, ArrayUtils.toObject(new double[4]));
				aFreqs = eF.get(a);
				aFreqs[AFINDEX_OCCUR]++;
				if (pa == -1) {
					aFreqs[AFINDEX_START]++;
					pa = a;
					String r = activities.get(Algorithm1ActivityMapping.STRING_START_ACTIVITY) + Algorithm1ActivityMapping.STRING_DIVIDER + a; 
					if (!erF.containsKey(r)) erF.put(r, ArrayUtils.toObject(new double[4]));
					rFreqs = erF.get(r);
					rFreqs[RFINDEX_AB]++;
					continue;
				}
				// L1Loop
				if (pa == a) {
					aFreqs[AFINDEX_L1]++;
					continue;
				} else {
					// A => B or B => A
					String r = (pa < a) 
						? pa + Algorithm1ActivityMapping.STRING_DIVIDER + a 
						: a + Algorithm1ActivityMapping.STRING_DIVIDER + pa;
					int dr = (pa < a) ? RFINDEX_AB : RFINDEX_BA;
					if (!erF.containsKey(r)) erF.put(r, ArrayUtils.toObject(new double[4]));
					rFreqs = erF.get(r);
					rFreqs[dr]++;
				}

				if (pa2 == -1) {
					pa2 = pa;
					pa = a;
					continue;
				}
				//L2Loop
				if (pa2 == a) {
					String r2 = (a < pa) 
							? a + Algorithm1ActivityMapping.STRING_DIVIDER + pa 
							: pa + Algorithm1ActivityMapping.STRING_DIVIDER + a;
					int dr = (a < pa) ? RFINDEX_ABL2 : RFINDEX_BAL2;
					if (!erF.containsKey(r2)) erF.put(r2, ArrayUtils.toObject(new double[4]));
					rFreqs = erF.get(r2);
					rFreqs[dr]++;
				}
				pa2 = pa;
				pa = a;
			}
			if (aFreqs != null) {
				String r = a + Algorithm1ActivityMapping.STRING_DIVIDER + activities.get(Algorithm1ActivityMapping.STRING_END_ACTIVITY); 
				if (!erF.containsKey(r)) erF.put(r, ArrayUtils.toObject(new double[4]));
				rFreqs = erF.get(r);
				rFreqs[RFINDEX_AB]++;
				aFreqs[AFINDEX_END]++;		
			}
			List<Tuple2<String, Vector>> result = new ArrayList<Tuple2<String,Vector>>();
			for (Integer e : eF.keySet()) {
				result.add(new Tuple2<String, Vector>(String.valueOf(e), Vectors.dense(ArrayUtils.toPrimitive(eF.get(e)))));
			}
			for (String r : erF.keySet()) {
				result.add(new Tuple2<String, Vector>(r, Vectors.dense(ArrayUtils.toPrimitive(erF.get(r)))));
			}
			return result;
		}

		@Override
		public Vector call(Vector arg0, Vector arg1) throws Exception {
			double[] a0 = arg0.toArray();
			double[] a1 = arg1.toArray();
			if (a0 == null && a1 != null) return Vectors.dense(a1);
			if (a1 == null) return Vectors.dense(a0);
			for (int i = 0; i < a0.length; i++) {
				a0[i] += a1[i];
			}
			return Vectors.dense(a0);
		}

		public JavaPairRDD<String, Vector> mine(JavaPairRDD<String, ICase> cases) {
			JavaPairRDD<String, Vector> result = cases
					.flatMapToPair(this)
					.reduceByKey(this)
					.cache();
			return result;
		}
	}

	public class Algorithm3DependencyMiner implements PairFlatMapFunction<Tuple2<String,Vector>, Integer, Activity>, Function2<Activity, Activity, Activity> {
		private static final long serialVersionUID = 1L;

		public static final int TYPE_DEP	 	= 0;
		public static final int TYPE_L1DEP	 	= 1;
		public static final int TYPE_L2DEP	 	= 2;

		public static final int INDEX_A	 		= 0;
		public static final int INDEX_B	 		= 1;
		public static final int INDEX_TYPE	 	= 2;
		public static final int INDEX_FREQ	 	= 3;
		public static final int INDEX_DEP	 	= 4;
		public static final int INDEX_FLAG	 	= 5;

		private final int dependencyDivisor;
		
		public Algorithm3DependencyMiner(HeuristicMinerJobConfiguration config) {
			dependencyDivisor 			= config.getThreshold().getDependencyDivisor();
		}
		
		public double calculateDependency(double ab, double ba) {
			return (ab - ba) / (ab + ba + dependencyDivisor);
		}
		
		public double calculateL1Dependency(double count) {
			return count / (count + dependencyDivisor);
		}
		
		public double calculateL2Dependency(double ab, double ba) {
			return (ab + ba) / (ab + ba + dependencyDivisor);
		}

		@Override
		public Iterable<Tuple2<Integer, Activity>> call(Tuple2<String, Vector> arg0)
				throws Exception {
			String ab = arg0._1();
			double[] v = arg0._2().toArray();
			double[] r = null; // a, b, type, freq, dependency
			boolean isRelation = ab.contains(Algorithm1ActivityMapping.STRING_DIVIDER);
			List<Tuple2<Integer, Activity>> result = new ArrayList<Tuple2<Integer, Activity>>();
			if (v == null) return result; 
			Activity ao = null;
			if (isRelation) {
				String[] abs 		= ab.split("\\" + Algorithm1ActivityMapping.STRING_DIVIDER);
				int a 				= Integer.valueOf(abs[0]);
				int b				= Integer.valueOf(abs[1]);
				double abCount 		= v[Algorithm2FrequencyMiner.RFINDEX_AB];
				double baCount 		= v[Algorithm2FrequencyMiner.RFINDEX_BA];
				double abDep		= calculateDependency(abCount, baCount);
				double baDep		= calculateDependency(baCount, abCount);
				ao = new Activity();
				ao.setId(a);
				if (baCount > 0) ao.getInput().put(b, new Dependency(b, baCount, baDep, false));
				if (abCount > 0) ao.getOutput().put(b, new Dependency(b, abCount, abDep, false));
				result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
				ao = new Activity();
				ao.setId(b);
				if (baCount > 0) ao.getOutput().put(a, new Dependency(a, baCount, baDep, false));
				if (abCount > 0) ao.getInput().put(a, new Dependency(a, abCount, abDep, false));
				result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
				
				double abL2Count = v[Algorithm2FrequencyMiner.RFINDEX_ABL2];
				double baL2Count = v[Algorithm2FrequencyMiner.RFINDEX_BAL2];
				double abL2Dep		= calculateL2Dependency(abL2Count, baL2Count);
				double baL2Dep		= calculateL2Dependency(baL2Count, abL2Count);
				ao = new Activity();
				ao.setId(a);
				if (baL2Count > 0) ao.getInputL2().put(b, new Dependency(b, baL2Count, baL2Dep, false));
				if (abL2Count > 0) ao.getOutputL2().put(b, new Dependency(b, abL2Count, abL2Dep, false));
				result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
				ao = new Activity();
				ao.setId(b);
				if (baL2Count > 0) ao.getOutputL2().put(a, new Dependency(a, baL2Count, baL2Dep, false));
				if (abL2Count > 0) ao.getInputL2().put(a, new Dependency(a, abL2Count, abL2Dep, false));
				result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
			} else {
				int a = Integer.parseInt(ab);
				double aL1Count = v[Algorithm2FrequencyMiner.AFINDEX_L1];
				double aL1Dep 	= calculateL1Dependency(aL1Count);
				ao = new Activity();
				ao.setId(a);
				ao.setL1(new Dependency(a, aL1Count, aL1Dep, false));
				result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
			}
			return result;
		}

		@Override
		public Activity call(Activity arg0, Activity arg1) throws Exception {
			Activity r = new Activity();
			if (arg0 == null && arg1 != null) {
				arg0 = arg1;
				arg1 = null;
			}
			r.setId(arg0.getId());
			if (arg0.getL1() != null) r.setL1(arg0.getL1());
			r.getInput().putAll(arg0.getInput());
			r.getOutput().putAll(arg0.getOutput());
			r.getInputL2().putAll(arg0.getInputL2());
			r.getOutputL2().putAll(arg0.getOutputL2());
			if (arg1 != null) {
				if (arg1.getL1() != null) r.setL1(arg1.getL1());
				r.getInput().putAll(arg1.getInput());
				r.getOutput().putAll(arg1.getOutput());
				r.getInputL2().putAll(arg1.getInputL2());
				r.getOutputL2().putAll(arg1.getOutputL2());
			}
			return r;
		}
		
		public JavaPairRDD<Integer, Activity> mine(JavaPairRDD<String, Vector> frequencySet) {
			return frequencySet
				.flatMapToPair(this)
				.reduceByKey(this)
				.cache();
		}
	}
	
	public class Dependency implements Serializable {
		private static final long serialVersionUID = 1L;

		private int		related;
		private double 	frequency;
		private double 	dependency;
		private boolean accepted;
		
		public Dependency() {
			
		}
		
		public Dependency(int related, double frequency, double dependency, boolean accepted) {
			setRelated(related);
			setFrequency(frequency);
			setDependency(dependency);
			setAccepted(accepted);
		}
		
		public int getRelated() {
			return related;
		}

		public void setRelated(int related) {
			this.related = related;
		}
		
		public double getFrequency() {
			return frequency;
		}
		public void setFrequency(double frequency) {
			this.frequency = frequency;
		}
		public double getDependency() {
			return dependency;
		}
		public void setDependency(double dependency) {
			this.dependency = dependency;
		}
		public boolean isAccepted() {
			return accepted;
		}
		public void setAccepted(boolean accepted) {
			this.accepted = accepted;
		}
		
		@Override
		public String toString() {
			return getDependency() + " (" + getFrequency() + " x) => " + isAccepted();
		}
	}
	
	public class Activity implements Serializable {
		private static final long serialVersionUID = 1L;

		
		private int id;
		private Dependency l1;
		private Map<Integer, Dependency> input;
		private Map<Integer, Dependency> output;
		private Map<Integer, Dependency> inputL2;
		private Map<Integer, Dependency> outputL2;
		
		public Activity() {
		}

		public Map<Integer, Dependency> getInputL2() {
			if (inputL2 == null) {
				inputL2 = new LinkedHashMap<Integer, Dependency>();
			}
			return inputL2;
		}

		public void setInputL2(Map<Integer, Dependency> inputL2) {
			this.inputL2 = inputL2;
		}

		public Map<Integer, Dependency> getOutputL2() {
			if (outputL2 == null) {
				outputL2 = new LinkedHashMap<Integer, Dependency>();
			}
			return outputL2;
		}

		public void setOutputL2(Map<Integer, Dependency> outputL2) {
			this.outputL2 = outputL2;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Dependency getL1() {
			return l1;
		}

		public void setL1(Dependency l1) {
			this.l1 = l1;
		}

		public Map<Integer, Dependency> getInput() {
			if (input == null) {
				input = new LinkedHashMap<Integer, Dependency>();
			}
			return input;
		}

		public void setInput(Map<Integer, Dependency> input) {
			this.input = input;
		}

		public Map<Integer, Dependency> getOutput() {
			if (output == null) {
				output = new LinkedHashMap<Integer, Dependency>();
			}
			return output;
		}

		public void setOutput(Map<Integer, Dependency> output) {
			this.output = output;
		}
		
	}
	
	public class Algorithm4DependencyFilter implements PairFunction<Tuple2<Integer,Activity>, Integer, Activity> {
		private static final long serialVersionUID = 1L;

		private final boolean useAllConnected;
		private final double relativeToBest;
		private final int positiveObservations;
		private final double dependencyThreshold;
		private final double dependencyThresholdL1Loop;
		private final double dependencyThresholdL2Loop;
		private final double dependencyDivisor;
		
		public Algorithm4DependencyFilter(HeuristicMinerJobConfiguration config) {
			positiveObservations		= config.getPositiveObservation();
			dependencyThreshold 		= config.getThreshold().getDependency();
			dependencyThresholdL1Loop 	= config.getThreshold().getL1Loop();
			dependencyThresholdL2Loop 	= config.getThreshold().getL2Loop();
			dependencyDivisor 			= config.getThreshold().getDependencyDivisor();
			useAllConnected				= config.getOption().isAllConnected();
			relativeToBest				= config.getThreshold().getRelativeToBest();
		}

//		@Override
//		public Boolean call(Activity arg0) throws Exception {
//			double[] v 		= arg0.toArray();
//			int type 		= (int) v[Algorithm3DependencyMiner.INDEX_TYPE];
//			double freq 	= v[Algorithm3DependencyMiner.INDEX_FREQ];
//			double dep 		= v[Algorithm3DependencyMiner.INDEX_DEP];
//			boolean result 	= false;
//			switch (type) {
//			case Algorithm3DependencyMiner.TYPE_DEP:
//				result = freq >= positiveObservations && dep > dependencyThreshold;
//				break;
//			case Algorithm3DependencyMiner.TYPE_L1DEP:
//				result = freq >= positiveObservations && dep > dependencyThresholdL1Loop;
//				break;
//			case Algorithm3DependencyMiner.TYPE_L2DEP:
//				result = freq >= positiveObservations && dep > dependencyThresholdL2Loop;
//				break;
//			}
//			return result;
//		}
//	

		@Override
		public Tuple2<Integer, Activity> call(Tuple2<Integer, Activity> arg0)
				throws Exception {
			int 		ai 	= arg0._1();
			Activity 	a 	= arg0._2();
			Activity 	r 	= new Activity();
			r.setId(a.getId());
			// Check L1Loop Dependency
			Dependency 	aL1 = a.getL1();
			if (aL1 != null && aL1.getFrequency() >= positiveObservations && aL1.getDependency() >= dependencyThresholdL1Loop) {
				aL1.setAccepted(true);
				r.setL1(aL1);
			}
			// Check Dependency
			int ia = 0, oa = 0;
			Dependency bestInput = null, bestOutput = null;
			for (Integer i : a.getInput().keySet()) {
				Dependency d = a.getInput().get(i);
				if (bestInput == null || d.getDependency() > bestInput.getDependency()) bestInput = d;
				if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThreshold) {
					d.setAccepted(true);
					r.getInput().put(i, d);
					ia++;
				}
			}
			for (Integer i : a.getOutput().keySet()) {
				Dependency d = a.getOutput().get(i);
				if (bestOutput == null || d.getDependency() > bestOutput.getDependency()) bestOutput = d;
				if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThreshold) {
					d.setAccepted(true);
					r.getOutput().put(i, d);
					oa++;
				}
			}
			// Check L2Dependency
			Dependency bestInputL2 = null, bestOutputL2 = null;
			for (Integer i : a.getInputL2().keySet()) {
				Dependency d = a.getInputL2().get(i);
				if (bestInputL2 == null || d.getDependency() > bestInputL2.getDependency()) bestInputL2 = d;
				if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThresholdL2Loop) {
					d.setAccepted(true);
					r.getInputL2().put(i, d);
					ia++;
				}
			}
			for (Integer i : a.getOutputL2().keySet()) {
				Dependency d = a.getOutputL2().get(i);
				if (bestOutputL2 == null || d.getDependency() > bestOutputL2.getDependency()) bestOutputL2 = d;
				if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThresholdL2Loop) {
					d.setAccepted(true);
					r.getOutputL2().put(i, d);
					oa++;
				}
			}
			if (ia == 0) {
				if (bestInput != null) {
					bestInput.setAccepted(true);
					r.getInput().put(bestInput.getRelated(), bestInput);
				} else if (bestInputL2 != null) {
					bestInputL2.setAccepted(true);
					r.getInput().put(bestInputL2.getRelated(), bestInputL2);
				}
			}
			if (oa == 0) {
				if (bestOutput != null) {
					bestOutput.setAccepted(true);
					r.getOutput().put(bestOutput.getRelated(), bestOutput);
				} else if (bestOutputL2 != null) {
					bestOutputL2.setAccepted(true);
					r.getOutput().put(bestOutputL2.getRelated(), bestOutputL2);
				}
			}
			return new Tuple2<Integer, Activity>(ai, r);
		}
		
		
		public JavaPairRDD<Integer, Activity> mine(JavaPairRDD<Integer, Activity> dependencies) {
			JavaPairRDD<Integer, Activity> result = dependencies
				.mapToPair(this)
				.cache();
			return result;
		}
	}
}
