/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicModel.Node;

public class HeuristicModel {

	private String id;
	private Map<String, Node> nodes = new LinkedHashMap<String, HeuristicModel.Node>();
	private Map<String, Arc> arcs = new LinkedHashMap<String, HeuristicModel.Arc>();
	private String resourceId;
	
	public HeuristicModel() {
		
	}
	
	public HeuristicModel(String outputURI) {
		this.resourceId = outputURI;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Node getOrAddNode(String label) {
		Node n = nodes.get(label);
		if (n == null) {
			n = new Node();
			n.setLabel(label);
			nodes.put(label, n);
		}
		return n;
	}
	
	public Arc getOrAddArc(String source, String target) {
		Arc n = arcs.get(source + "|" + target);
		if (n == null) {
			n = new Arc();
			n.setSource(source);
			n.setTarget(target);
			arcs.put(source + "|" + target, n);
		}
		return n;
	}
	
	public Arc getOrAddArc(String source, String target, double frequency, double dependency) {
		Arc n = getOrAddArc(source, target);
		n.getFrequency().setAbsolute((int) frequency);
		n.setDependency(dependency);
		return n;
	}

	public class Node {
		private String label;
		private Frequency frequency = new Frequency();
		private Duration duration = new Duration();
		
		public class Frequency {
			private int absolute = 0;
			private double relative = 0;
			private Cases cases = new Cases();
			private int maxRepetition = 0;
			
			class Cases {
				private int include = 0;
				private int asStartEvent = 0;
				private int asEndEvent = 0;
				
				public int getInclude() {
					return include;
				}
				public void setInclude(int include) {
					this.include = include;
				}
				public int getAsStartEvent() {
					return asStartEvent;
				}
				public void setAsStartEvent(int asStartEvent) {
					this.asStartEvent = asStartEvent;
				}
				public int getAsEndEvent() {
					return asEndEvent;
				}
				public void setAsEndEvent(int asEndEvent) {
					this.asEndEvent = asEndEvent;
				}
				public void increaseInclude() {
					include++;
				}
				public void increaseAsStartEvent() {
					asStartEvent++;
				}
				public void increaseAsEndEvent() {
					asEndEvent++;
				}
			}

			public int getAbsolute() {
				return absolute;
			}

			public void setAbsolute(int absolute) {
				this.absolute = absolute;
			}

			public double getRelative() {
				return relative;
			}

			public void setRelative(double relative) {
				this.relative = relative;
			}

			public Cases getCases() {
				return cases;
			}

			public void setCases(Cases cases) {
				this.cases = cases;
			}

			public int getMaxRepetition() {
				return maxRepetition;
			}

			public void setMaxRepetition(int maxRepetition) {
				this.maxRepetition = maxRepetition;
			}
			public void increaseAbsolute() {
				absolute++;
			}
			public void calculateRelative(double denominator) {
				if (denominator == 0) {
					relative = 0;
				} else {
					relative = relative / denominator; 
				}
			}
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public Frequency getFrequency() {
			return frequency;
		}

		public void setFrequency(Frequency frequency) {
			this.frequency = frequency;
		}

		public Duration getDuration() {
			return duration;
		}

		public void setDuration(Duration duration) {
			this.duration = duration;
		}
	}
	
	public class Arc {
		private String source;
		private String target;
		private Frequency frequency = new Frequency();
		private Duration duration = new Duration();
		private double dependency;
		
		public class Frequency {
			private int absolute = 0;
			private double relative = 0;
			private int cases = 0;
			private int maxRepetition = 0;
			
			public int getAbsolute() {
				return absolute;
			}
			public void setAbsolute(int absolute) {
				this.absolute = absolute;
			}
			public double getRelative() {
				return relative;
			}
			public void setRelative(double relative) {
				this.relative = relative;
			}
			public int getCases() {
				return cases;
			}
			public void setCases(int cases) {
				this.cases = cases;
			}
			public int getMaxRepetition() {
				return maxRepetition;
			}
			public void setMaxRepetition(int maxRepetition) {
				this.maxRepetition = maxRepetition;
			}
			public void increaseAbsolute() {
				absolute++;
			}
			public void calculateRelative(double denominator) {
				if (denominator == 0) {
					relative = 0;
				} else {
					relative = relative / denominator; 
				}
			}
			public void increaseCases() {
				cases++;
			}
			public void increaseMaxRepetition() {
				maxRepetition++;
			}
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}

		public Frequency getFrequency() {
			return frequency;
		}

		public void setFrequency(Frequency frequency) {
			this.frequency = frequency;
		}

		public Duration getDuration() {
			return duration;
		}

		public void setDuration(Duration duration) {
			this.duration = duration;
		}
		
		public double getDependency() {
			return dependency;
		}
		
		public void setDependency(double dependency) {
			this.dependency = dependency;
		}
	}
	
	public class Duration {
		private long total = 0;
		private long min = Long.MAX_VALUE;
		private long max = Long.MIN_VALUE;
		private double mean = 0;
		private long median = 0;
		
		public long getTotal() {
			return total;
		}
		public void setTotal(long total) {
			this.total = total;
		}
		public long getMin() {
			return min;
		}
		public void setMin(long min) {
			this.min = min;
		}
		public long getMax() {
			return max;
		}
		public void setMax(long max) {
			this.max = max;
		}
		public double getMean() {
			return mean;
		}
		public void setMean(double mean) {
			this.mean = mean;
		}
		public long getMedian() {
			return median;
		}
		public void setMedian(long median) {
			this.median = median;
		}
	}
	
	public Map<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String, Node> nodes) {
		this.nodes = nodes;
	}

	public Map<String, Arc> getArcs() {
		return arcs;
	}

	public void setArcs(Map<String, Arc> arcs) {
		this.arcs = arcs;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

}
