//
//  @ Project : Log Miner Web Service
//  @ File Name : AprioriARMinerJob.java
//  @ Date : 7/14/2014
//  @ Author : Iq Reviessay Pulshashi
//



package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar;

import java.util.*;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import flexjson.JSONDeserializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;


public class AprioriARMiner2Job extends AssociationRuleMinerJob {
	private static final long serialVersionUID = 1L;


	@SuppressWarnings("serial")
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		JavaSparkContext sc = se.getContext();
		//FileSystem fs = se.getHdfsFileSystem();
//		JSONSerializer serializer = new JSONSerializer();
//		String outputURI = se.getHdfsURI(res.getUri());
		AssociationRuleMinerConfiguration config = new JSONDeserializer<AssociationRuleMinerConfiguration>().deserialize(json, AssociationRuleMinerConfiguration.class);
		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());
		
		// Collect dimensional states
		long start = System.currentTimeMillis();
		System.out.println("Calculating dimensional states ... ");
		Map<String, Integer> raw = reader.getCasesRDD().flatMapToPair(new PairFlatMapFunction<Tuple2<String,ICase>, String, Integer>() {
			@Override
			public Iterable<Tuple2<String, Integer>> call(
					Tuple2<String, ICase> arg0) throws Exception {
				Map<String, Tuple2<String, Integer>> dsr = new TreeMap<String, Tuple2<String, Integer>>();
				ICase c = arg0._2();
				for (IEvent e : c.getEvents().values()) {
					String ds = e.getLabel();
					if (!dsr.containsKey(ds))
						dsr.put(ds, new Tuple2<String, Integer>(ds, 1));
				}
				return dsr.values();
			}
		}).reduceByKey(new Function2<Integer, Integer, Integer>() {
			@Override
			public Integer call(Integer arg0, Integer arg1) throws Exception {
				return 1;
			}
		}).collectAsMap();
		int dsi = 0;
		final Map<String, Integer> dimensionalStates = new LinkedHashMap<String, Integer>();
		for (String s : raw.keySet()) {
			dimensionalStates.put(s, dsi);
			System.out.println("- " + dsi + ": " + s);
			dsi++;
		}
		final int traceLength = dimensionalStates.size();
		System.out.println("Calculating dimensional states ... " + (System.currentTimeMillis() - start) + " ms");
		
		System.out.println("Convert to trace ... ");
		final JavaPairRDD<String, BitSet> traces = reader.getCasesRDD().mapToPair(new PairFunction<Tuple2<String,ICase>, String, BitSet>() {
			@Override
			public Tuple2<String, BitSet> call(Tuple2<String, ICase> arg0)
					throws Exception {
				BitSet r = new BitSet(traceLength);
				ICase c = arg0._2();
				for (IEvent e : c.getEvents().values()) {
					String ds = e.getLabel();
					if (dimensionalStates.containsKey(ds)) 
						r.set(dimensionalStates.get(ds), true);
				}
				return new Tuple2<String, BitSet>(arg0._1(), r);
			}
		}).cache();
		
		List<BitSet> test = new ArrayList<BitSet>();
		for (Integer i : dimensionalStates.values()) {
			BitSet b = new BitSet(traceLength);
			b.set(i, true);
			test.add(b);
		}
		System.out.println(
		sc.parallelize(test).mapToPair(new PairFunction<BitSet, BitSet, Double>() {

			@Override
			public Tuple2<BitSet, Double> call(BitSet arg0) throws Exception {
				final BitSet cs = arg0;
				double r = traces.mapToDouble(new DoubleFunction<Tuple2<String,BitSet>>() {
					
					@Override
					public double call(Tuple2<String, BitSet> arg0) throws Exception {
						BitSet t = (BitSet) arg0._2().clone();
						t.and(cs);
						t.xor(cs);
						return t.isEmpty() ? 1 : 0;
					}
				}).sum();
				return new Tuple2<BitSet, Double>(arg0, r);
			}
		}).collectAsMap()
		);
		
		
		System.out.println("Convert to trace ... " + (System.currentTimeMillis() - start) + " ms");
		
		return null;
	}

}
