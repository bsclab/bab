/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.base.model;

public interface IEvent extends IAttributable, IResource {
	public static final String MAP_EVENT_CASE = "event.case";
	public static final String MAP_EVENT_ORDER_TIME = "event.order.time";
	public static final String MAP_EVENT_ORDER_TIME_START = "event.order.time.start";
	public static final String MAP_EVENT_ORDER_TIME_END = "event.order.time.end";

	public static final String MAP_EVENT_ACTIVITY = "event.activity";
	public static final String MAP_EVENT_TYPE = "event.type";
	public static final String MAP_EVENT_TIMESTAMP = "event.timestamp";
	public static final String MAP_EVENT_ORIGINATOR = "event.originator";
	public static final String MAP_EVENT_RESOURCE = "event.resource";
	public static final String MAP_EVENT_OTHER = "event.other";

	public static final String DIM_EVENT_ACTIVITY_TYPE = "event.activity+event.type";
	public static final String DIM_EVENT_ACTIVITY = "event.activity";
	public static final String DIM_EVENT_TYPE = "event.type";
	public static final String DIM_EVENT_TIMESTAMP = "event.timestamp";
	public static final String DIM_EVENT_ORIGINATOR = "event.originator";
	public static final String DIM_EVENT_RESOURCE = "event.resource";
	public static final String DIM_EVENT_OTHER = "event.other";
	public static final String DIM_EVENT_TYPE_START = "start";
	public static final String DIM_EVENT_TYPE_COMPLETE = "complete";

	public String getLabel();

	public String getType();

	public String getOriginator();

	public Long getTimestamp();

	public String getResource();
}
