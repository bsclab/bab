/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar;

import java.util.*;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;
import scala.Tuple2;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class AprioriARMinerJob extends AssociationRuleMinerJob {
	private static final long serialVersionUID = 1L;

	class Node {
		String name;
		double frequency;
		Node parent;
		Map<String, Node> childs;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getFrequency() {
			return frequency;
		}
		public void setFrequency(double frequency) {
			this.frequency = frequency;
		}
		public Node getParent() {
			return parent;
		}
		public void setParent(Node parent) {
			this.parent = parent;
		}
		public Map<String, Node> getChilds() {
			if (childs == null) 
				childs = new TreeMap<String, Node>();
			return childs;
		}
		public boolean isLeaf() {
			return childs == null;
		}

	}
	
	@SuppressWarnings("serial")
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		//JavaSparkContext sc = se.getContext();
		//FileSystem fs = se.getHdfsFileSystem();
		JSONSerializer serializer = new JSONSerializer();
		String outputURI = se.getHdfsURI(res.getUri());
		AssociationRuleMinerConfiguration config = new JSONDeserializer<AssociationRuleMinerConfiguration>().deserialize(json, AssociationRuleMinerConfiguration.class);
		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());
		
		try {
		
			JavaRDD<Set<String>> transactions;
			
			if (config.getNoOfRelation() == 0) {
				transactions = reader.getCasesRDD().map(new Function<Tuple2<String,ICase>, Set<String>>() {
					
					@Override
					public Set<String> call(Tuple2<String, ICase> arg0)
							throws Exception {
						ICase icase = arg0._2();
						Set<String> events = new HashSet<String>(icase.getEvents().size());
						for (IEvent e : icase.getEvents().values()) {
							String label = e.getLabel() + " (" + e.getType() + ")";
							events.add(label);
						}
						return new TreeSet<String>(events);
					}
				});
			} else {
				transactions = reader.getCasesRDD().map(new Function<Tuple2<String,ICase>, Set<String>>() {
					
					@Override
					public Set<String> call(Tuple2<String, ICase> arg0)
							throws Exception {
						ICase icase = arg0._2();
						Set<String> events = new HashSet<String>(icase.getEvents().size());
						String prev = null;
						for (IEvent e : icase.getEvents().values()) {
							String label = e.getLabel() + " (" + e.getType() + ")";
							if (prev == null) {
								prev = label;
								continue;
							}
							events.add(prev + "-->" + label);
							prev = label;
						}
						return new TreeSet<String>(events);
					}
				});
			}
			
			AssociationRuleMiner model = new AssociationRuleMiner();
			model.setTotalTransactions(transactions.count());
			model.setNoOfRelation(config.getNoOfRelation());
			
			FPGrowth fpg = new FPGrowth()
				.setMinSupport(config.getThreshold().getSupport().getMin())
				.setNumPartitions(10);
			FPGrowthModel<String> fpmodel = fpg.run(transactions);
			
			final Node fpTree = new Node();
			fpTree.setName("");
			List<Node> leafs = new ArrayList<Node>();
			
			//Map<String, Long> freqPatterns = new TreeMap<String, Long>();
			for (FPGrowth.FreqItemset<String> itemset: fpmodel.freqItemsets().toJavaRDD().collect()) {
				Set<String> sorter = new TreeSet<String>(itemset.javaItems());
				double f = itemset.freq();
				Node current = fpTree;
				for (String s : sorter) {
					if (current.getChilds().containsKey(s)) {
						if (f > current.getFrequency())
							current.setFrequency(f);
						current = current.getChilds().get(s);
					} else if (current == fpTree) {
						Node n = new Node();
						n.setName(s);
						n.setFrequency(f);
						n.setParent(current);
						current.getChilds().put(s, n);
						current = n;
					} else {
						Node n = new Node();
						n.setName(s);
						n.setFrequency(f);
						n.setParent(current);
						current.getChilds().put(s, n);
						current = n;
						Node cf = current;
						while (cf != null) {
							if (cf.getParent() != null && cf.getFrequency() > cf.getParent().getFrequency()) {
								cf.getParent().setFrequency(cf.getFrequency());
							}
							cf = cf.getParent();
						}
					}
				}
				if (current != fpTree)
					leafs.add(current);
			}
			//
			
			double minConfidence = config.getThreshold().getConfidence().getMin();
			Map<String, Rules> confidenceList = new TreeMap<String, Rules>();

			for (Node leaf : leafs) {
				Set<String> rightHandSet = new TreeSet<String>();
				String rightHand = "";
				Node current = leaf;
				while (current != fpTree) {
					rightHand = rightHand + "|" + current.getName();
					rightHandSet.add(current.getName());
					if (current.getFrequency() != current.getParent().getFrequency()) {
						Node currentLeft = current.getParent();
						Set<String> leftHandSet = new TreeSet<String>();
						leftHandSet.add(currentLeft.getName());
						String leftHand = currentLeft.getName();
						if (currentLeft != fpTree) {
							double fx = currentLeft.getFrequency();
							while (currentLeft.getParent() != fpTree) {
								leftHandSet.add(currentLeft.getParent().getName());
								leftHand = currentLeft.getParent().getName() + "|" + leftHand;
								currentLeft = currentLeft.getParent();
							}
							double fxuy = current.getFrequency();
							double fconf = fxuy / fx;
							if (fconf >= minConfidence) {
							    Rules r = new Rules(fconf, fxuy / (double) model.getTotalTransactions());
							    r.setLeft(leftHandSet);
							    r.setRight(rightHandSet);
							    String k = fconf + "" + fxuy;
							    if (confidenceList.containsKey(k)) {
							    	Rules c = confidenceList.get(k);
							    	for (String s : leftHandSet) {
							    		c.getLeft().add(s);
							    	}
							    	for (String s : rightHandSet) {
							    		c.getRight().add(s);
							    	}
							    } else {
							    	confidenceList.put(k, r);
							    }
								//System.err.println(leftHand.substring(1) + " => " + rightHand.substring(1) + " : " + fxuy + " " + fx + " " + fconf);
							}
						}
					}
					current = current.getParent();
				}
			}
			
			Map<Set<String>, Double> setFreqs = new HashMap<Set<String>, Double>(); 
			
			for (String k : confidenceList.keySet()) {
				Rules c = confidenceList.get(k);
				setFreqs.put(c.getLeft(), 0d);
				setFreqs.put(c.getRight(), 0d);
			    for (String s : c.getLeft()) {
			    	if (c.getRight().contains(s)) {
			    		c.getRight().remove(s);
			    	}
			    }
				model.getAssociationRules().put(k, c);
			}
				
			RawJobResult result = new RawJobResult("analysis.AssociationRules", outputURI, outputURI, serializer.exclude("*.class").deepSerialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".arans", result.getResponse());		
			return result;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
}
