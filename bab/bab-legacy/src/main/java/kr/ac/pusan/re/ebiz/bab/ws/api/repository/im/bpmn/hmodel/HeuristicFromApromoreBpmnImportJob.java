/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.bpmn.hmodel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;














//import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import scala.Tuple2;

import com.google.common.base.Strings;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.IterableTransformer;
import flexjson.transformer.MapTransformer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicModel.Node;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Element;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Event;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Task;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class HeuristicFromApromoreBpmnImportJob extends ImportJob {
	private static final long serialVersionUID = 1L;


	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	
	Map<String, HeuristicModel.Node> nodes = new LinkedHashMap<String, HeuristicModel.Node>();
	Map<String, String[]> flows = new LinkedHashMap<String, String[]>();
    Map<String, Gateway> gateways = new LinkedHashMap<String, Gateway>();

	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			ImportJobConfiguration config = new JSONDeserializer<ImportJobConfiguration>()
					.deserialize(json, ImportJobConfiguration.class);
			String outputURI = se.getHdfsURI(config.getRepositoryURI());
			String[] repos = config.getRepositoryURI().split("/");
			String repositoryId = "/bab/workspaces/tmp/"
					+ repos[repos.length - 1] + ".bpmn";
			

			JAXBContext jaxbContext = JAXBContext.newInstance(Model.class);
			String xml = HdfsUtil.loadTextFile(se, repositoryId);

			System.err.println(xml);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xml);
			Model mod = (Model) jaxbUnmarshaller.unmarshal(reader);
			kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.model.Process process = mod.getProcess();
			
			// 1. Convert Task / Activities / Event to Node
			HeuristicModel model = new HeuristicModel(outputURI);
			for (Element element : process.getElements()) {
				if (!(element instanceof SequenceFlow) && !(element instanceof Gateway) && (element.getName().trim().length() > 0)) {
					nodes.put(element.getId(), model.getOrAddNode(element.getName()));
					System.err.println("ADD NODE " + element.getId() + " : " + element.getName());
				} else if (element instanceof Gateway) {
					gateways.put(element.getId(), (Gateway) element);
					System.err.println("FOUND GATEWAY  : " + element.getId());
				}
			}
			// 2. Register Arc for Node to Node
			for (Element element : process.getElements()) {
				if (element instanceof SequenceFlow) {
					SequenceFlow flow = (SequenceFlow) element;
					if (nodes.containsKey(flow.getSourceRef()) && nodes.containsKey(flow.getTargetRef())) {
						String source = nodes.get(flow.getSourceRef()).getLabel();
						String target = nodes.get(flow.getTargetRef()).getLabel();
						model.getOrAddArc(source, target);
						System.err.println("ADD ARC N TO N : " + source + " => " + target);
					}
					flows.put(flow.getId(), new String[] { flow.getSourceRef(), flow.getTargetRef() });
				}
			}
			// 3. Register Arcs from Gateway
			for (String gid : gateways.keySet()) {
				Gateway g = gateways.get(gid);
				Set<String> sources = getSources(g, new TreeSet<String>());
				Set<String> targets = getTargets(g, new TreeSet<String>());
				System.err.println("LOOKING FOR GATEWAY " + gid + " : " + sources.size() + " - " + targets.size());
				for (String source : sources) {
					for (String target : targets) {
						model.getOrAddArc(source, target);
						System.err.println("ADD ARC FROM G " + g.getId() + " : " + source + " => " + target);
					}
				}
			}
//			
//			// Problem when gateway meet gateway
//			
//			// 3. Delete gateway, connect in to out node
//			for (Element element : process.getElements()) {
//				if (element instanceof Gateway) {
//					Gateway gateway = (Gateway) element;
//					Set<Node> ins = new LinkedHashSet<Node>();
//					Set<Node> outs = new LinkedHashSet<Node>();
//					for (String flowId : gateway.getIncoming()) {
//						String[] flow = flows.get(flowId); 
//						for (String nodeId : flow) {
//							if (nodes.containsKey(nodeId)) {
//								ins.add(nodes.get(nodeId));
//							}
//						}
//					}
//					for (String flowId : gateway.getOutgoing()) {
//						String[] flow = flows.get(flowId);
//						for (String nodeId : flow) {
//							if (nodes.containsKey(nodeId)) {
//								outs.add(nodes.get(nodeId));
//							}
//						}
//					}
//					for (Node i : ins) {
//						for (Node o : outs) {
//							model.getOrAddArc(i.getLabel(), o.getLabel());
//						}
//					}
//				}
//			}
			
			RawJobResult result = new RawJobResult("repository.Repository",
					outputURI, outputURI, serializer.exclude("*.class")
			.serialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".hbmodel",
					result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Set<String> getSources(Gateway gateway, Set<String> visitedGateway) {
		visitedGateway.add(gateway.getId());
		Set<String> result = new LinkedHashSet<String>();
		for (String flowId : gateway.getIncoming()) {
			String[] flow = flows.get(flowId);
			String source = flow[0];
			if (nodes.containsKey(source)) {
				result.add(nodes.get(source).getLabel());
			} else if (gateways.containsKey(source) && !visitedGateway.contains(source)) {
				result.addAll(getSources(gateways.get(source), visitedGateway));
			}
 		}	
		return result;
	}

	public Set<String> getTargets(Gateway gateway, Set<String> visitedGateway) {
		visitedGateway.add(gateway.getId());
		Set<String> result = new LinkedHashSet<String>();
		for (String flowId : gateway.getOutgoing()) {
			String[] flow = flows.get(flowId);
			String target = flow[1];
			if (nodes.containsKey(target)) {
				result.add(nodes.get(target).getLabel());
			} else if (gateways.containsKey(target) && !visitedGateway.contains(target)) {
				result.addAll(getTargets(gateways.get(target), visitedGateway));
			}
 		}	
		return result;
	}
}
