/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.ls;

import java.util.*;

import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.controller.DateUtil;

public class LogSummary {

	private Case caseTab = new Case();
	private Timeline timelineTab = new Timeline();
	private Dimension activityTab = new Dimension();
	private Dimension originatorTab = new Dimension();
	private Dimension resourceTab = new Dimension();
	private Map<String, Dimension> otherTab = new LinkedHashMap<String, LogSummary.Dimension>();

	public LogSummary() {
		activityTab.setDimension(IEvent.MAP_EVENT_ACTIVITY);
		originatorTab.setDimension(IEvent.MAP_EVENT_ORIGINATOR);
		resourceTab.setDimension(IEvent.MAP_EVENT_RESOURCE);
	}

	class Case {
		private Map<String, DCase> cases = new LinkedHashMap<String, DCase>();
		private Map<Integer, Integer> eventPerCase = new LinkedHashMap<Integer, Integer>();
		private Map<Long, Integer> caseDurations = new LinkedHashMap<Long, Integer>();

		private Map<Double, Integer> meanActivityDurations = new LinkedHashMap<Double, Integer>();

		private Map<Double, Integer> meanWaitingTimes = new LinkedHashMap<Double, Integer>();
		private Map<Double, Integer> caseUtilizations = new LinkedHashMap<Double, Integer>();

		public DCase getCase(String caseId) {
			if (cases.containsKey(caseId)) {
				return cases.get(caseId);
			}
			DCase nc = new DCase();
			nc.setCaseId(caseId);
			cases.put(caseId, nc);
			return nc;
		}

		public void increaseEventPerCase(int noOfEvents) {
			if (!eventPerCase.containsKey(noOfEvents))
				eventPerCase.put(noOfEvents, 0);
			eventPerCase.put(noOfEvents, eventPerCase.get(noOfEvents) + 1);
		}

		public void increaseCaseDurations(long caseDuration) {
			if (!caseDurations.containsKey(caseDuration))
				caseDurations.put(caseDuration, 0);
			caseDurations
					.put(caseDuration, caseDurations.get(caseDuration) + 1);
		}

		public void increaseCaseUtilizations(double caseUtilization) {
			if (!caseUtilizations.containsKey(caseUtilization))
				caseUtilizations.put(caseUtilization, 0);
			caseUtilizations.put(caseUtilization,
					caseUtilizations.get(caseUtilization) + 1);
		}

		public void increaseMeanActivityDurations(double meanActivityDuration) {
			if (!meanActivityDurations.containsKey(meanActivityDuration))
				meanActivityDurations.put(meanActivityDuration, 0);
			meanActivityDurations.put(meanActivityDuration,
					meanActivityDurations.get(meanActivityDuration) + 1);
		}

		public void increaseMeanWaitingTimes(double meanWaitingTime) {
			if (!meanWaitingTimes.containsKey(meanWaitingTime))
				meanWaitingTimes.put(meanWaitingTime, 0);
			meanWaitingTimes.put(meanWaitingTime,
					meanWaitingTimes.get(meanWaitingTime) + 1);
		}

		class DCase {
			private String caseId;
			private int noOfEvents;
			private long started;
			private long finished;
			private long duration;

			public String getCaseId() {
				return caseId;
			}

			public void setCaseId(String caseId) {
				this.caseId = caseId;
			}

			public int getNoOfEvents() {
				return noOfEvents;
			}

			public void setNoOfEvents(int noOfEvents) {
				this.noOfEvents = noOfEvents;
			}

			public long getStarted() {
				return started;
			}

			public void setStarted(long started) {
				this.started = started;
			}

			public long getFinished() {
				return finished;
			}

			public void setFinished(long finished) {
				this.finished = finished;
			}

			public long getDuration() {
				return duration;
			}

			public void setDuration(long duration) {
				this.duration = duration;
			}

			public String getStartedStr() {
				return DateUtil.toDateTimeString(started);
			}

			public String getFinishedStr() {
				return DateUtil.toDateTimeString(finished);
			}

			public String getDurationStr() {
				return DateUtil.toDurationString(duration);
			}
		}

		public Map<String, DCase> getCases() {
			return cases;
		}

		public void setCases(Map<String, DCase> cases) {
			this.cases = cases;
		}

		public Map<Integer, Integer> getEventPerCase() {
			return eventPerCase;
		}

		public void setEventPerCase(Map<Integer, Integer> eventPerCase) {
			this.eventPerCase = eventPerCase;
		}

		public Map<Long, Integer> getCaseDurations() {
			return caseDurations;
		}

		public void setCaseDurations(Map<Long, Integer> caseDurations) {
			this.caseDurations = caseDurations;
		}

		public Map<Double, Integer> getCaseUtilizations() {
			return caseUtilizations;
		}

		public void setCaseUtilizations(Map<Double, Integer> caseUtilizations) {
			this.caseUtilizations = caseUtilizations;
		}

		public Map<Double, Integer> getMeanActivityDurations() {
			return meanActivityDurations;
		}

		public void setMeanActivityDurations(
				Map<Double, Integer> meanActivityDurations) {
			this.meanActivityDurations = meanActivityDurations;
		}

		public Map<Double, Integer> getMeanWaitingTimes() {
			return meanWaitingTimes;
		}

		public void setMeanWaitingTimes(Map<Double, Integer> meanWaitingTimes) {
			this.meanWaitingTimes = meanWaitingTimes;
		}
	}

	class Timeline {
		private Map<Long, Integer> caseStartOverTime = new TreeMap<Long, Integer>();
		private Map<Long, Integer> caseCompleteOverTime = new TreeMap<Long, Integer>();
		private Map<Long, Integer> eventsOverTime = new TreeMap<Long, Integer>();
		private Map<Long, Integer> activeCasesOverTime;
		private Map<Long, Set<String>> activeCasesOverTimeDetail = new TreeMap<Long, Set<String>>();

		public void increaseCaseStartOverTime(long time) {
			if (!caseStartOverTime.containsKey(time))
				caseStartOverTime.put(time, 0);
			caseStartOverTime.put(time, caseStartOverTime.get(time) + 1);
		}

		public void increaseCaseCompleteOverTime(long time) {
			if (!caseCompleteOverTime.containsKey(time))
				caseCompleteOverTime.put(time, 0);
			caseCompleteOverTime.put(time, caseCompleteOverTime.get(time) + 1);
		}

		public void increaseEventOverTime(long time) {
			if (!eventsOverTime.containsKey(time))
				eventsOverTime.put(time, 0);
			eventsOverTime.put(time, eventsOverTime.get(time) + 1);
		}

		public void increaseActiveCasesOverTime(long time, String caseId) {
			if (!activeCasesOverTimeDetail.containsKey(time))
				activeCasesOverTimeDetail
						.put(time, new LinkedHashSet<String>());
			activeCasesOverTimeDetail.get(time).add(caseId);
		}

		public Map<Long, Integer> getEventsOverTime() {
			return eventsOverTime;
		}

		public void setEventsOverTime(Map<Long, Integer> eventsOverTime) {
			this.eventsOverTime = eventsOverTime;
		}

		public Map<Long, Integer> getActiveCasesOverTime() {
			if (activeCasesOverTime != null)
				return activeCasesOverTime;
			activeCasesOverTime = new TreeMap<Long, Integer>();
			Map<Long, Integer> times = new TreeMap<Long, Integer>();
			for (Long l : caseStartOverTime.keySet()) times.put(l, caseStartOverTime.get(l));
			for (Long l : caseCompleteOverTime.keySet()) times.put(l, (times.containsKey(l) ? times.get(l) : 0) - caseCompleteOverTime.get(l));
			int activeCases = 0;
			for (Long l : times.keySet()) {
				activeCases += times.get(l);
				activeCasesOverTime.put(l, activeCases);
			}
//			for (Long l : activeCasesOverTimeDetail.keySet()) {
//				activeCasesOverTime.put(l, activeCasesOverTimeDetail.get(l)
//						.size());
//			}
			return activeCasesOverTime;
		}

		public Map<Long, Set<String>> getActiveCasesOverTimeDetail() {
			return activeCasesOverTimeDetail;
		}

		public void setActiveCasesOverTime(
				Map<Long, Integer> activeCasesOverTime) {
			this.activeCasesOverTime = activeCasesOverTime;
		}

		public void setActiveCasesOverTimeDetail(
				Map<Long, Set<String>> activeCasesOverTimeDetail) {
			this.activeCasesOverTimeDetail = activeCasesOverTimeDetail;
		}

		public Map<Long, Integer> getCaseStartOverTime() {
			return caseStartOverTime;
		}

		public void setCaseStartOverTime(Map<Long, Integer> caseStartOverTime) {
			this.caseStartOverTime = caseStartOverTime;
		}

		public Map<Long, Integer> getCaseCompleteOverTime() {
			return caseCompleteOverTime;
		}

		public void setCaseCompleteOverTime(Map<Long, Integer> caseCompleteOverTime) {
			this.caseCompleteOverTime = caseCompleteOverTime;
		}

	}

	class Dimension {
		private String dimension = "";
		private Map<String, Integer> frequency;
		private Map<String, Double> medianDuration;
		private Map<String, Double> meanDuration;
		private Map<String, Double> durationRange;
		private Map<String, Double> aggregateDuration;
		private Map<String, State> states = new LinkedHashMap<String, LogSummary.Dimension.State>();

		public Map<String, Integer> getFrequency() {
			if (frequency == null) {
				frequency = new TreeMap<String, Integer>();
				for (String state : states.keySet()) {
					frequency.put(state, states.get(state).getFrequency());
				}
			}
			return frequency;
		}

		public Map<String, Double> getMedianDuration() {
			if (medianDuration == null) {
				medianDuration = new TreeMap<String, Double>();
				for (String state : states.keySet()) {
					medianDuration.put(state, states.get(state)
							.getDurationRange());
				}
			}
			return medianDuration;
		}

		public Map<String, Double> getMeanDuration() {
			if (meanDuration == null) {
				meanDuration = new TreeMap<String, Double>();
				for (String state : states.keySet()) {
					meanDuration
							.put(state, states.get(state).getMeanDuration());
				}
			}
			return meanDuration;
		}

		public Map<String, Double> getDurationRange() {
			if (durationRange == null) {
				durationRange = new TreeMap<String, Double>();
				for (String state : states.keySet()) {
					durationRange.put(state, states.get(state)
							.getDurationRange());
				}
			}
			return durationRange;
		}

		public Map<String, Double> getAggregateDuration() {
			if (aggregateDuration == null) {
				aggregateDuration = new TreeMap<String, Double>();
				for (String state : states.keySet()) {
					aggregateDuration.put(state, states.get(state)
							.getSumDuration());
				}
			}
			return aggregateDuration;
		}

		public void setMedianDuration(Map<String, Double> medianDuration) {
			this.medianDuration = medianDuration;
		}

		public void setMeanDuration(Map<String, Double> meanDuration) {
			this.meanDuration = meanDuration;
		}

		public void setDurationRange(Map<String, Double> durationRange) {
			this.durationRange = durationRange;
		}

		public void setAggregateDuration(Map<String, Double> aggregateDuration) {
			this.aggregateDuration = aggregateDuration;
		}

		public State getState(String state) {
			if (states.containsKey(state)) {
				return states.get(state);
			}
			State ns = new State();
			ns.setLabel(state);
			states.put(state, ns);
			return ns;
		}

		class State {
			private String label;
			private int frequency;
			private double relativeFrequency;

			private double sumDuration;
			private double meanDuration;

			private double minDuration;
			private double maxDuration;
			private double durationRange;

			public void increaseFrequency() {
				frequency++;
			}

			public void addNewDuration(double duration) {
				sumDuration += duration;
				if (frequency > 0)
					meanDuration = sumDuration / frequency;
				if (duration < minDuration)
					minDuration = duration;
				if (duration > maxDuration)
					maxDuration = duration;
				durationRange = maxDuration - minDuration;
			}

			public String getLabel() {
				return label;
			}

			public void setLabel(String label) {
				this.label = label;
			}

			public int getFrequency() {
				return frequency;
			}

			public void setFrequency(int frequency) {
				this.frequency = frequency;
			}

			public double getRelativeFrequency() {
				return relativeFrequency;
			}

			public void setRelativeFrequency(double relativeFrequency) {
				this.relativeFrequency = relativeFrequency;
			}

			public double getSumDuration() {
				return sumDuration;
			}

			public void setSumDuration(double sumDuration) {
				this.sumDuration = sumDuration;
			}

			public double getMeanDuration() {
				return meanDuration;
			}

			public void setMeanDuration(double meanDuration) {
				this.meanDuration = meanDuration;
			}

			public double getMinDuration() {
				return minDuration;
			}

			public void setMinDuration(double minDuration) {
				this.minDuration = minDuration;
			}

			public double getMaxDuration() {
				return maxDuration;
			}

			public void setMaxDuration(double maxDuration) {
				this.maxDuration = maxDuration;
			}

			public double getDurationRange() {
				return durationRange;
			}

			public void setDurationRange(double durationRange) {
				this.durationRange = durationRange;
			}

			public String getSumDurationStr() {
				return DateUtil.toDurationString((long) sumDuration);
			}

			public String getMeanDurationStr() {
				return DateUtil.toDurationString((long) meanDuration);
			}

			public String getMinDurationStr() {
				return DateUtil.toDurationString((long) minDuration);
			}

			public String getMaxDurationStr() {
				return DateUtil.toDurationString((long) maxDuration);
			}

			public String getDurationRangeStr() {
				return DateUtil.toDurationString((long) durationRange);
			}
		}

		public String getDimension() {
			return dimension;
		}

		public void setDimension(String dimension) {
			this.dimension = dimension;
		}

		public Map<String, State> getStates() {
			return states;
		}

		public void setStates(Map<String, State> states) {
			this.states = states;
		}

		public void calculateRelativeFrequency(int totalEvents) {
			for (State s : states.values()) {
				if (totalEvents != 0)
					s.setRelativeFrequency(s.getFrequency() / totalEvents);
			}

		}

		public void setFrequency(Map<String, Integer> frequencies) {
			this.frequency = frequencies;
		}

	}

	public Case getCaseTab() {
		return caseTab;
	}

	public void setCaseTab(Case caseTab) {
		this.caseTab = caseTab;
	}

	public Timeline getTimelineTab() {
		return timelineTab;
	}

	public void setTimelineTab(Timeline timelineTab) {
		this.timelineTab = timelineTab;
	}

	public Dimension getActivityTab() {
		return activityTab;
	}

	public void setActivityTab(Dimension activityTab) {
		this.activityTab = activityTab;
	}

	public Dimension getOriginatorTab() {
		return originatorTab;
	}

	public void setOriginatorTab(Dimension originatorTab) {
		this.originatorTab = originatorTab;
	}

	public Dimension getResourceTab() {
		return resourceTab;
	}

	public void setResourceTab(Dimension resourceTab) {
		this.resourceTab = resourceTab;
	}

	public Map<String, Dimension> getOtherTab() {
		return otherTab;
	}

	public void setOtherTab(Map<String, Dimension> otherTab) {
		this.otherTab = otherTab;
	}

	public void calculateRelativeFrequency(int totalEvents) {
		List<Dimension> dimensions = new ArrayList<Dimension>();
		for (Dimension dimension : dimensions) {
			dimension.calculateRelativeFrequency(totalEvents);
		}
	}

}
