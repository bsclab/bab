/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;

public class TimeGapAnalysisJobModel implements Serializable{
	private static final long serialVersionUID = 1L;


	public Map<String, Transition> transitions = new HashMap<String, Transition>(); //key=> 'start activity (type) | end activity (type)'
	public Map<String, Node> nodes = new HashMap<String, Node>(); //key=> 'start activity (type) | end activity (type)'
	public Map<Long, Integer> timelineWhole = new TreeMap<Long, Integer>();
	public Map<Long, Integer> timelineFiltered = new TreeMap<Long, Integer>();
	public BRepository repository;
	public int minGap;
	public int maxGap;
	public String repositoryURI;
	
	public Node setOrGetNode(String label){
		Node n = nodes.get(label);
		if(n == null){
			n = new Node();
			n.setLabel(label);
		}
		
		nodes.put(label, n);
		
		return n;
	}
	
	public Transition setOrGetTransition(String startActivity, String endActivity){
		Transition t = transitions.get(startActivity+"|"+endActivity);
		if(t == null){
			t = new Transition();
			t.setTarget(endActivity);
			t.setSource(startActivity);
		}
		
		transitions.put(startActivity+"|"+endActivity, t);
		
		setOrGetNode(startActivity);
		setOrGetNode(endActivity);
		
		return t;
	}
	
	
	
	public Map<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String, Node> nodes) {
		this.nodes = nodes;
	}

	public Map<String, Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Map<String, Transition> transitions) {
		this.transitions = transitions;
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}
	
	public double getMinGap() {
		return minGap;
	}

	public void setMinGap(int minGap) {
		this.minGap = minGap;
	}

	public int getMaxGap() {
		return maxGap;
	}

	public void setMaxGap(int maxGap) {
		this.maxGap = maxGap;
	}

	public class Node{
		private String label;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}
		
	}
	
	public class Transition{
		public String source; //concatenate of 'Activity Name (Event type)'
		public String target; //concatenate of 'Activity Name (Event type)'
		public Map<String, Case> caseIDSuccessive = new HashMap<String, Case>();
		public Map<String, Case> caseIDNonSuccessive = new HashMap<String, Case>();
		public int noOfSuccessiveCases;
		public int noOfNonSuccessiveCases;
		public String meanSuccessive;
		public String stdDevSuccessive;
		public String meanNonSuccessive;
		public String stdDevNonSuccessive;
		public int meanRaw;
		

		public Case setOrGetSuccessive(String caseID){
			Case c = caseIDSuccessive.get(caseID);
			if(c == null){
				c = new Case();
				c.setLabel(caseID);
			}
			
			caseIDSuccessive.put(caseID, c);
			return c;
		}
		
		public Case setOrGetNonSuccessive(String caseID){
			Case c = caseIDNonSuccessive.get(caseID);
			if(c == null){
				c = new Case();
				c.setLabel(caseID);
			}
			
			caseIDNonSuccessive.put(caseID, c);
			return c;
		}
		
		
		
		public String getMeanSuccessive() {
			return meanSuccessive;
		}

		public void setMeanSuccessive(String meanSuccessive) {
			this.meanSuccessive = meanSuccessive;
		}

		public String getStdDevSuccessive() {
			return stdDevSuccessive;
		}

		public void setStdDevSuccessive(String stdDevSuccessive) {
			this.stdDevSuccessive = stdDevSuccessive;
		}

		public String getMeanNonSuccessive() {
			return meanNonSuccessive;
		}

		public void setMeanNonSuccessive(String meanNonSuccessive) {
			this.meanNonSuccessive = meanNonSuccessive;
		}

		public String getStdDevNonSuccessive() {
			return stdDevNonSuccessive;
		}

		public void setStdDevNonSuccessive(String stdDevNonSuccessive) {
			this.stdDevNonSuccessive = stdDevNonSuccessive;
		}
		
		public String getSource() {
			return source;
		}
		
		public void setSource(String startActivity) {
			this.source = startActivity;
		}
		
		public String getTarget() {
			return target;
		}
		
		public void setTarget(String endActivity) {
			this.target = endActivity;
		}
		
		public Map<String, Case> getCaseIDSuccessive() {
			return caseIDSuccessive;
		}
		
		public void setCaseIDSuccessive(Map<String, Case> caseIDSuccessive) {
			this.caseIDSuccessive = caseIDSuccessive;
		}
		
		public Map<String, Case> getCaseIDNonSuccessive() {
			return caseIDNonSuccessive;
		}
		
		public void setCaseIDNonSuccessive(Map<String, Case> caseIDNonSuccessive) {
			this.caseIDNonSuccessive = caseIDNonSuccessive;
		}

		public int getNoOfSuccessiveCases() {
			//noOfSuccessiveCases = caseIDSuccessive.size();
			return noOfSuccessiveCases;
		}

		public void setNoOfSuccessiveCases(int noOfSuccessiveCases) {
			this.noOfSuccessiveCases = noOfSuccessiveCases;
		}

		public int getNoOfNonSuccessiveCases() {
			//noOfNonSuccessiveCases = caseIDNonSuccessive.size() + caseIDSuccessive.size();
			return noOfNonSuccessiveCases;
		}

		public void setNoOfNonSuccessiveCases(int noOfNonSuccessiveCases) {
			this.noOfNonSuccessiveCases = noOfNonSuccessiveCases;
		}

		public int getMeanRaw() {
			return meanRaw;
		}

		public void setMeanRaw(int meanRaw) {
			this.meanRaw = meanRaw;
		}
		
	}
	
	public class Case{
		public String label; //CaseID value
		public String gap;
		
		public String getLabel() {
			return label;
		}
		
		public void setLabel(String label) {
			this.label = label;
		}
		
		public String getGap() {
			return gap;
		}

		public void setGap(String gap) {
			this.gap = gap;
		}
		
	}

	public BRepository getRepository() {
		return repository;
	}

	public void setRepository(BRepository repository) {
		this.repository = repository;
	}

	public Map<Long, Integer> getTimelineWhole() {
		return timelineWhole;
	}

	public void setTimelineWhole(Map<Long, Integer> timelineWhole) {
		this.timelineWhole = timelineWhole;
	}

	public Map<Long, Integer> getTimelineFiltered() {
		return timelineFiltered;
	}

	public void setTimelineFiltered(Map<Long, Integer> timelineFiltered) {
		this.timelineFiltered = timelineFiltered;
	}
}
