<!-- 
/******************************************************************************
*       This file is part of Best Analytics of Big Data (BAB) v2 Package.      *
* ============================================================================ *
*                                                                              * 
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
 ******************************************************************************/
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  	<!-- Identification -->
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>kr.ac.pusan.bsclab</groupId>
		<artifactId>bab</artifactId>
		<version>2.2.0-SNAPSHOT</version>
	</parent>
	<artifactId>bab-common-spark</artifactId>
	<name>BAB v2 - Common Analytics Algorithm on Spark</name>

	<!-- License -->
	<licenses>
		<license>
			<name>GNU Lesser General Public License, Version 3.0</name>
			<url>http://www.gnu.org/licenses/lgpl-3.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<!-- Configuration -->
	<properties>
		<!-- Application Configuration -->
	</properties>

	<!-- Dependencies -->
	<dependencies>
		<!-- BAB Module Dependencies -->
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-core</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<dependency>
			<groupId>kr.ac.pusan.bsclab</groupId>
			<artifactId>bab-common</artifactId>
			<version>${bab.version}</version>
		</dependency>
		<!-- Apache Hadoop Dependencies -->
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-annotations</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-assemblies</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-common</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-hdfs</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-nfs</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-auth</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-client</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-api</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-common</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-yarn-client</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- Apache Spark Dependencies -->
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-core_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-sql_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-streaming_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-mllib_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-graphx_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-yarn_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-hive_2.10</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- Jackson Dependencies -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
		    <groupId>junit</groupId>
		    <artifactId>junit</artifactId>
		    <scope>test</scope>
		</dependency>
		<!-- Database Drivers Dependencies -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>
		<dependency>
			<groupId>com.facebook.presto</groupId>
			<artifactId>presto-jdbc</artifactId>
			<version>0.194</version>
		</dependency>
	</dependencies>

	<!-- Build Configuration -->
	<build>
		<!-- Plugins -->
	    <plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<environmentVariables>
						<HADOOP_HOME>Y:\0Cloud\Dropbox
							(Personal)\1Ideas\2013-IEPNU\2018\BABv3\app\babv3\bab\lib\hadoop</HADOOP_HOME>
					</environmentVariables>
					<systemPropertyVariables>
						<HADOOP_HOME>Y:\0Cloud\Dropbox
							(Personal)\1Ideas\2013-IEPNU\2018\BABv3\app\babv3\bab\lib\hadoop</HADOOP_HOME>
					</systemPropertyVariables>
					<!-- <skipTests>true</skipTests> -->
					<testFailureIgnore>true</testFailureIgnore>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<configuration>
							<minimizeJar>true</minimizeJar>
							<artifactSet>
								<excludes>
									<exclude>com*:*</exclude>
									<exclude>org*:*</exclude>
									<exclude>java*:*</exclude>
									<exclude>xml*:*</exclude>
									<exclude>dom*:*</exclude>
									<exclude>ch*:*</exclude>
									<exclude>ao*:*</exclude>
									<exclude>my*:*</exclude>
									<exclude>ant*:*</exclude>
									<exclude>xom*:*</exclude>
								</excludes>
							</artifactSet>
							<filters>
								<filter>
									<artifact>kr.ac.pusan.bsclab:bab-core</artifact>
									<includes>
										<include>kr/ac/pusan/bsclab/bab/**</include>
									</includes>
								</filter>
								<filter>
									<artifact>kr.ac.pusan.bsclab:bab-common</artifact>
									<includes>
										<include>kr/ac/pusan/bsclab/bab/**</include>
									</includes>
								</filter>
							</filters>
						</configuration>
					</execution>
				</executions>
			</plugin>
	     	<plugin>
	            <groupId>org.apache.maven.plugins</groupId>
	            <artifactId>maven-antrun-plugin</artifactId>
	            <executions>
	                <execution>
	                    <phase>install</phase>
	                    <configuration>
	                        <target>
	                            <!-- copy file="target/${project.artifactId}-${project.version}.jar" tofile="../bab-web-common/jars/${project.artifactId}-${project.version}.jar"/-->
	                            <copy file="target/${project.artifactId}-${project.version}.jar" tofile="X:/opt/spark/app/hdfs/bab/v2/packages/${project.artifactId}-${project.version}.jar"/>
	                        </target>
	                    </configuration>
	                    <goals>
	                        <goal>run</goal>
	                    </goals>
	                </execution>
	            </executions>
	        </plugin>			
		</plugins>
	</build>
	<description>BAB Framework 2nd Version - Implementation of Common Analytics Algorithm using Apache Spark Ecosystem</description>
</project>