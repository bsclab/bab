/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.EndEvent;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.ExclusiveGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.InclusiveGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.ParallelGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.StartEvent;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Task;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Process discovery based on heuristic miner algorithm <br>
 * <br>
 * Config class: {@link HeuristicMinerJobConfiguration}<br>
 * Result class: {@link HeuristicModel}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */

public class SparkHeuristicBpmnMinerJob extends SparkHeuristicMinerJob {
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(
		name = "ModelHeuristicBpmnJob", 
		title = "Heuristic Miner with BPMN Representation",
		requestClass = HeuristicMinerJobConfiguration.class, 
		responseClass = Model.class,
		legacyJobExtension = ".hbmodel"
	)
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			IJobResult sresult = super.run(json, res, se);
			if (sresult != null) {
			      ObjectMapper mapper = new ObjectMapper();
			      String outputURI = se.getContextUri(res.getUri());
				HeuristicModel hm = getModel();
				 Model bpmn = new Model();
			      Map<String, Integer> freqs = new LinkedHashMap<String, Integer>();
	
			      // 1. Turn node into task
			      Map<String, Task> nmap = new LinkedHashMap<String, Task>();
			      int n = 1;
			      for (String node : hm.getNodes().keySet()) {
			        String id = "node-" + n;
			        Task t = new Task(id, node);
			        bpmn.getProcess().getElements().add(t);
			        nmap.put(node, t);
			        int f = hm.getNodes().get(node).getFrequency().getAbsolute();
			        freqs.put(id, f);
			        n++;
			      }
			      // 2. Turn arc into sequence flow
			      Map<String, SequenceFlow> smap = new LinkedHashMap<String, SequenceFlow>();
			      int sq = 1;
			      for (String arc : hm.getArcs().keySet()) {
			        Arc a = hm.getArcs().get(arc);
			        String id = "sequenceFlow-" + sq;
			        Task source = nmap.get(a.getSource());
			        Task target = nmap.get(a.getTarget());
			        SequenceFlow flow = new SequenceFlow(id, "", source.getId(), target.getId());
			        smap.put(id, flow);
			        source.getOutgoing().add(id);
			        target.getIncoming().add(id);
			        int f = a.getFrequency().getAbsolute();
			        freqs.put(id, f);
			        sq++;
			      }
			      // 3. Create gateway
			      int gwi = 1;
			      for (Task t : nmap.values()) {
			        String gid = "gateway-" + gwi;
			        String sid = "sequenceFlow-" + sq;
			        // Split
			        if (t.getIncoming().size() == 1 && t.getOutgoing().size() > 1) {
			          int nfreq = freqs.get(t.getId());
			          List<Integer> afreqs = new ArrayList<Integer>();
			          for (String aid : t.getOutgoing()) {
			            afreqs.add(freqs.get(aid));
			          }
			          Gateway g = getGateway(gid, Gateway.DIRECTION_DIVERGING, nfreq, afreqs);
			          for (String aid : t.getOutgoing()) {
			            SequenceFlow iarc = smap.get(aid);
			            iarc.setSourceRef(gid);
			            g.getOutgoing().add(aid);
			          }
			          t.getOutgoing().clear();
			          bpmn.getProcess().getElements().add(g);
			          SequenceFlow nflow = new SequenceFlow(sid, "", t.getId(), gid);
			          smap.put(sid, nflow);
			          t.getOutgoing().add(sid);
			          g.getIncoming().add(sid);
			          sq++;
			          gwi++;
			          // Join
			        } else if (t.getOutgoing().size() == 1 && t.getIncoming().size() > 1) {
			          int nfreq = freqs.get(t.getId());
			          List<Integer> afreqs = new ArrayList<Integer>();
			          for (String aid : t.getIncoming()) {
			            afreqs.add(freqs.get(aid));
			          }
			          Gateway g = getGateway(gid, Gateway.DIRECTION_CONVERGING, nfreq, afreqs);
			          for (String aid : t.getIncoming()) {
			            SequenceFlow iarc = smap.get(aid);
			            iarc.setTargetRef(gid);
			            g.getIncoming().add(aid);
			          }
			          t.getIncoming().clear();
			          bpmn.getProcess().getElements().add(g);
			          SequenceFlow nflow = new SequenceFlow(sid, "", gid, t.getId());
			          smap.put(sid, nflow);
			          t.getIncoming().add(sid);
			          g.getOutgoing().add(sid);
			          sq++;
			          gwi++;
			        }
			      }
			      // 4. Assign start (node with no incoming) / end event (node with no outgoing)
			      int sei = 1;
			      int eei = 1;
			      for (Task t : nmap.values()) {
			        if (t.getIncoming().size() == 0) {
			          StartEvent sev =
			              new StartEvent("startEvent-" + sei, (sei == 1 ? "Start" : "Start " + sei));
			          String id = "sequenceFlow-" + sq;
			          bpmn.getProcess().getElements().add(sev);
			          smap.put(id, new SequenceFlow(id, "", sev.getId(), t.getId()));
			          t.getIncoming().add(id);
			          sei++;
			          sq++;
			        }
			        if (t.getOutgoing().size() == 0) {
			          EndEvent eev = new EndEvent("endEvent-" + eei, (eei == 1 ? "End" : "End " + sei));
			          String id = "sequenceFlow-" + sq;
			          bpmn.getProcess().getElements().add(eev);
			          smap.put(id, new SequenceFlow(id, "", t.getId(), eev.getId()));
			          t.getOutgoing().add(id);
			          eei++;
			          sq++;
			        }
			      }
			      // Register Flow
			      for (SequenceFlow flow : smap.values()) {
			        bpmn.getProcess().getElements().add(flow);
			      }
			      RawJobResult result = new RawJobResult("model.HeuristicBpmn", outputURI, outputURI,
			              mapper.writeValueAsString(bpmn));
			          se.getFileUtil().saveAsTextFile(se, outputURI + ".hbmodel", result.getResponse());
			          return result;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	  public Gateway getGateway(String id, String direction, int nodeFreq, List<Integer> arcFreqs) {
	    for (Integer arcFreq : arcFreqs) {
	      if (arcFreq != nodeFreq) {
	        int sumArcFreq = 0;
	        for (Integer arcFreq2 : arcFreqs) {
	          sumArcFreq += arcFreq2;
	        }
	        // It was XOR if sum of all arc freq = node freq
	        if (sumArcFreq == nodeFreq) {
	          return new ExclusiveGateway(id, "", direction);
	          // Otherwise it was OR
	        } else {
	          return new InclusiveGateway(id, "", direction);
	        }
	      }
	    }
	    // By default is AND if all arc freq = node freq
	    return new ParallelGateway(id, "", direction);
	  }
}
