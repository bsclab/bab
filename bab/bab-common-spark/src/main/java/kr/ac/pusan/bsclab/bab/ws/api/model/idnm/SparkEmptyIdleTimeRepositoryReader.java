/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.idnm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ISparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class SparkEmptyIdleTimeRepositoryReader extends SparkIdleTimeRepositoryReader
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private ISparkExecutor executor;
  private IRepository repository;
  private JavaPairRDD<String, ICase> casesRDD;

  public SparkEmptyIdleTimeRepositoryReader(ISparkRepositoryReader nativeReader, IExecutor se,
      Configuration config) {
    super(nativeReader, config);
    this.executor = (ISparkExecutor) se;
    try {
      this.repository = new BRepository();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public IRepository getRepository() {
    return repository;
  }

  @Override
  public JavaPairRDD<String, ICase> getCasesRDD() {

    if (casesRDD == null) {
      JavaSparkContext jsc = executor.getContext();
      List<Tuple2<String, ICase>> emptyList = new ArrayList<Tuple2<String, ICase>>();
      casesRDD = jsc.parallelizePairs(emptyList);
    }
    return casesRDD;
  }

}
