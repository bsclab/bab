/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.ar;

import java.io.Serializable;
import java.util.*;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Association rule miner based on FP Growth algorithm <br>
 * <br>
 * Config class: {@link AssociationRuleMinerConfiguration}<br>
 * Result class: {@link AssociationRuleMiner}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkFpGrowthArMinerJob extends FpGrowthArMinerJob implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  @SuppressWarnings("serial")
	@Override
	@BabService(
		name = "AnalysisAssociationRuleJob", 
		title = "Association Rule Analysis",
		requestClass = AssociationRuleMinerConfiguration.class, 
		responseClass = AssociationRuleMiner.class,
		legacyJobExtension = ".arans"
	)
  public IJobResult run(String json, IResource res, IExecutor se) {
    // JavaSparkContext sc = se.getContext();
    // FileSystem fs = se.getHdfsFileSystem();
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    String outputURI = se.getContextUri(res.getUri());

    try {
      AssociationRuleMinerConfiguration config =
          mapper.readValue(json, AssociationRuleMinerConfiguration.class);
      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);

      JavaRDD<Set<String>> transactions;

      if (config.getNoOfRelation() == 0) {
        transactions = reader.getCasesRDD().map(new Function<Tuple2<String, ICase>, Set<String>>() {

          @Override
          public Set<String> call(Tuple2<String, ICase> arg0) throws Exception {
            ICase icase = arg0._2();
            Set<String> events = new HashSet<String>(icase.getEvents().size());
            for (IEvent e : icase.getEvents().values()) {
              String label = e.getLabel() + " (" + e.getType() + ")";
              events.add(label);
            }
            return new TreeSet<String>(events);
          }
        });
      } else {
        transactions = reader.getCasesRDD().map(new Function<Tuple2<String, ICase>, Set<String>>() {

          @Override
          public Set<String> call(Tuple2<String, ICase> arg0) throws Exception {
            ICase icase = arg0._2();
            Set<String> events = new HashSet<String>(icase.getEvents().size());
            String prev = null;
            for (IEvent e : icase.getEvents().values()) {
              String label = e.getLabel() + " (" + e.getType() + ")";
              if (prev == null) {
                prev = label;
                continue;
              }
              events.add(prev + "-->" + label);
              prev = label;
            }
            return new TreeSet<String>(events);
          }
        });
      }

      AssociationRuleMiner model = new AssociationRuleMiner();
      model.setTotalTransactions(transactions.count());
      model.setNoOfRelation(config.getNoOfRelation());

      FPGrowth fpg = new FPGrowth().setMinSupport(config.getThreshold().getSupport().getMin())
          .setNumPartitions(10);
      FPGrowthModel<String> fpmodel = fpg.run(transactions);

      final Node fpTree = new Node();
      fpTree.setName("");
      List<Node> leafs = new ArrayList<Node>();

      for (FPGrowth.FreqItemset<String> itemset : fpmodel.freqItemsets().toJavaRDD().collect()) {
        Set<String> sorter = new TreeSet<String>(itemset.javaItems());
        double f = itemset.freq();
        Node current = fpTree;
        for (String s : sorter) {
          if (current.getChilds().containsKey(s)) {
            if (f > current.getFrequency())
              current.setFrequency(f);
            current = current.getChilds().get(s);
          } else if (current == fpTree) {
            Node n = new Node();
            n.setName(s);
            n.setFrequency(f);
            n.setParent(current);
            current.getChilds().put(s, n);
            current = n;
          } else {
            Node n = new Node();
            n.setName(s);
            n.setFrequency(f);
            n.setParent(current);
            current.getChilds().put(s, n);
            current = n;
            Node cf = current;
            while (cf != null) {
              if (cf.getParent() != null && cf.getFrequency() > cf.getParent().getFrequency()) {
                cf.getParent().setFrequency(cf.getFrequency());
              }
              cf = cf.getParent();
            }
          }
        }
        if (current != fpTree)
          leafs.add(current);
      }
      //

      double minConfidence = config.getThreshold().getConfidence().getMin();
      Map<String, Rules> confidenceList = new TreeMap<String, Rules>();

      for (Node leaf : leafs) {
        Set<String> rightHandSet = new TreeSet<String>();
        String rightHand = "";
        Node current = leaf;
        while (current != fpTree) {
          rightHand = rightHand + "|" + current.getName();
          rightHandSet.add(current.getName());
          if (current.getFrequency() != current.getParent().getFrequency()) {
            Node currentLeft = current.getParent();
            Set<String> leftHandSet = new TreeSet<String>();
            leftHandSet.add(currentLeft.getName());
            String leftHand = currentLeft.getName();
            if (currentLeft != fpTree) {
              double fx = currentLeft.getFrequency();
              while (currentLeft.getParent() != fpTree) {
                leftHandSet.add(currentLeft.getParent().getName());
                leftHand = currentLeft.getParent().getName() + "|" + leftHand;
                currentLeft = currentLeft.getParent();
              }
              double fxuy = current.getFrequency();
              double fconf = fxuy / fx;
              if (fconf >= minConfidence) {
                Rules r = new Rules(fconf, fxuy / (double) model.getTotalTransactions());
                r.setLeft(leftHandSet);
                r.setRight(rightHandSet);
                String k = fconf + "" + fxuy;
                if (confidenceList.containsKey(k)) {
                  Rules c = confidenceList.get(k);
                  for (String s : leftHandSet) {
                    c.getLeft().add(s);
                  }
                  for (String s : rightHandSet) {
                    c.getRight().add(s);
                  }
                } else {
                  confidenceList.put(k, r);
                }
                // System.err.println(leftHand.substring(1) + " => " + rightHand.substring(1) + " :
                // " + fxuy + " " + fx + " " + fconf);
              }
            }
          }
          current = current.getParent();
        }
      }

      Map<Set<String>, Double> setFreqs = new HashMap<Set<String>, Double>();

      for (String k : confidenceList.keySet()) {
        Rules c = confidenceList.get(k);
        setFreqs.put(c.getLeft(), 0d);
        setFreqs.put(c.getRight(), 0d);
        for (String s : c.getLeft()) {
          if (c.getRight().contains(s)) {
            c.getRight().remove(s);
          }
        }
        model.getAssociationRules().put(k, c);
      }

      RawJobResult result = new RawJobResult("analysis.AssociationRules", outputURI, outputURI,
          mapper.writeValueAsString(model));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".arans", result.getResponse());
      return result;

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

}
