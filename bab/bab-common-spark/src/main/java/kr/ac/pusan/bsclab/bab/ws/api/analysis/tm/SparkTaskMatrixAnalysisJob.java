/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.AbstractAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Task matrix algorithm <br>
 * <br>
 * Config class: {@link TaskMatrixConfiguration}<br>
 * Result class: {@link HeatMapModel} and {@link HeatCasesModel}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 * @author Wahyu Andy (wanprabu@gmail.com)
 */
public class SparkTaskMatrixAnalysisJob extends AbstractAnalysisJob {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

	@Override
	@BabService(
		name = "AnalysisTaskMatrixJob", 
		title = "Task Matrix Analysis",
		requestClass = TaskMatrixConfiguration.class, 
		responseClass = HeatMapModel.class,
		legacyJobExtension = ".tmans"
	)
  public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      // JavaSparkContext sc = se.getContext();
      // FileSystem fs = se.getHdfsFileSystem();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      String outputURI = se.getContextUri(res.getUri());
      final TaskMatrixConfiguration config = mapper.readValue(json, TaskMatrixConfiguration.class);
      final String row = config.getRow();
      final String column = config.getColumn();

      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      JavaPairRDD<String, Link> relationsRDD = reader.getCasesRDD().mapPartitionsToPair(
          new PairFlatMapFunction<Iterator<Tuple2<String, ICase>>, String, Link>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Link>> call(Iterator<Tuple2<String, ICase>> arg0)
                throws Exception {
              Map<String, Tuple2<String, Link>> result =
                  new LinkedHashMap<String, Tuple2<String, Link>>();
              while (arg0.hasNext()) {
                ICase icase = arg0.next()._2();
                IEvent pe = null;
                Set<String> caseSet = new LinkedHashSet<String>();
                caseSet.add(icase.getId());

                for (IEvent e : icase.getEvents().values()) {
                  String source = "", target = "";
                  if (pe == null)
                    pe = e;

                  // set source value
                  if (row.compareTo(IEvent.DIM_EVENT_ACTIVITY) == 0)
                    source = pe.getLabel();
                  else if (row.compareTo(IEvent.DIM_EVENT_ACTIVITY_TYPE) == 0) {
                    source = pe.getLabel();
                    source += " (" + pe.getType() + ")";
                  } else if (row.compareTo(IEvent.DIM_EVENT_TYPE) == 0)
                    source = pe.getType();
                  else if (row.compareTo(IEvent.DIM_EVENT_ORIGINATOR) == 0)
                    source = pe.getOriginator();
                  else if (row.compareTo(IEvent.DIM_EVENT_RESOURCE) == 0)
                    source = pe.getResource();

                  // set target value
                  if (column.compareTo(IEvent.DIM_EVENT_ACTIVITY) == 0)
                    target = e.getLabel();
                  else if (column.compareTo(IEvent.DIM_EVENT_ACTIVITY_TYPE) == 0) {
                    target = e.getLabel();
                    target += " (" + e.getType() + ")";
                  } else if (column.compareTo(IEvent.DIM_EVENT_TYPE) == 0)
                    target = e.getType();
                  else if (column.compareTo(IEvent.DIM_EVENT_ORIGINATOR) == 0)
                    target = e.getOriginator();
                  else if (column.compareTo(IEvent.DIM_EVENT_RESOURCE) == 0)
                    target = e.getResource();

                  String key = source + "=>" + target;
                  if (!result.containsKey(key))
                    result.put(key, new Tuple2<String, Link>(key, new Link(source, target)));

                  Link r = result.get(key)._2();
                  r.addCase(icase.getId());
                  if (config.getUnit().compareTo(TaskMatrixConfiguration.FREQUENCY) == 0)
                    r.setValue(r.getValue() + 1);
                  else {
                    long duration = e.getTimestamp() - pe.getTimestamp();
                    r.setValue(duration);
                  }

                  pe = e;
                }
              }
              return result.values();
            }

          }).reduceByKey(new Function2<Link, Link, Link>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Link call(Link arg0, Link arg1) throws Exception {
              Link nr = new Link(arg0.getSource(), arg0.getTarget());

              nr.setValue(arg0.getValue() + arg1.getValue());
              nr.setCases(arg0.getCases());
              // nr.getFrequency().setRelative(arg0.getFrequency().getRelative() +
              // arg1.getFrequency().getRelative());
              /*
               * nr.getDuration().setMax(arg0.getDuration().getMax() > arg1.getDuration().getMax() ?
               * arg0.getDuration().getMax() : arg1.getDuration().getMax());
               * nr.getDuration().setMin(arg0.getDuration().getMin() < arg1.getDuration().getMin() ?
               * arg0.getDuration().getMin() : arg1.getDuration().getMin());
               * nr.getDuration().setTotal(arg0.getDuration().getTotal() +
               * arg1.getDuration().getTotal());
               */
              return nr;
            }
          });

      Map<String, Link> relations = relationsRDD.collectAsMap();
      // Map<String, Link> heatList = relationsRDD.collectAsMap();
      // List<Tuple2<String, Link>> relations2 = relationsRDD.collect();
      Set<String> rowSet = new TreeSet<String>();
      Set<String> colSet = new TreeSet<String>();

      for (Link m : relations.values()) {
        rowSet.add(m.getSource());
        colSet.add(m.getTarget());
      }

      List<String> rowList = new ArrayList<String>(rowSet);
      List<String> colList = new ArrayList<String>(colSet);
      List<Integer> hcrowList = new ArrayList<Integer>();
      List<Integer> hccolList = new ArrayList<Integer>();
      int i = 1;
      for (String s : rowSet) {
        s.toString();
        hcrowList.add(i++);
      }
      i = 1;
      for (String s : colSet) {
        s.toString();
        hccolList.add(i++);
      }

      double min = Integer.MIN_VALUE, max = Integer.MAX_VALUE;
      double mean = 0, count = 0, sum = 0;
      HeatMapModel taskMatrix = new HeatMapModel();
      HeatCasesModel caseModel = new HeatCasesModel();
      Map<String, String> caseList = new TreeMap<String, String>();
      Map<String, Set<String>> caseMap = new TreeMap<String, Set<String>>();
      for (Link m : relations.values()) {
        String source = rowList.indexOf(m.getSource()) + "";
        String target = colList.indexOf(m.getTarget()) + "";
        Link l = new Link(source, target);
        l.setValue(m.getValue());
        l.setCasesNumber(m.getCases().size());

        if (count == 0) {
          min = m.getValue();
          max = m.getValue();
        }
        if (m.getValue() < min)
          min = m.getValue();
        if (m.getValue() > max)
          max = m.getValue();

        sum += m.getValue();
        count++;
        caseList.put(source + "|" + target, m.getCases().toString());
        caseMap.put(source + "|" + target, m.getCases());

        taskMatrix.addHeatList(l);
        // caseModel.putCaseList(caseList);
        caseModel.putCaseMap(caseMap);
      }
      mean = sum / count;// (rowList.size()*colList.size());

      /*
       * for (String key : heatList.keySet()) { String source = heatList.get(key).getSource();
       * String target = heatList.get(key).getTarget();
       * heatList.get(key).setSource(rowList.indexOf(source)+"");
       * heatList.get(key).setTarget(colList.indexOf(target)+""); }
       */

      for (String s : rowList) {
        for (String t : colList) {
          String source = rowList.indexOf(s) + "";
          String target = colList.indexOf(t) + "";
          if (!taskMatrix.getHeatList().contains(new Link(source, target))) {
            Link l = new Link(source, target);
            l.setValue(0);
            l.setEmpty(true);
            taskMatrix.addHeatList(l);
          }
        }
      }

      taskMatrix.getStatistics().setMax(max);
      taskMatrix.getStatistics().setMin(min);
      taskMatrix.getStatistics().setMean(mean);
      taskMatrix.getStatistics().setTotal(sum);
      taskMatrix.getStatistics().setCount(count);
      taskMatrix.setUnit(config.getUnit());

      taskMatrix.setHcrow(hcrowList);
      taskMatrix.setHccol(hccolList);
      taskMatrix.setRowLabel(rowList);
      taskMatrix.setColLabel(colList);
      taskMatrix.setCasesUrl(res.getUri() + "-cases.tmans");

      // cases map output
      RawJobResult caseOutput = new RawJobResult("analysis.TaskMatrix", outputURI, outputURI,
          mapper.writeValueAsString(caseModel));
      se.getFileUtil().saveAsTextFile(se, outputURI + "-cases.tmans", caseOutput.getResponse());

      RawJobResult result = new RawJobResult("analysis.TaskMatrix", outputURI, outputURI,
          mapper.writeValueAsString(taskMatrix));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".tmans", result.getResponse());

      // return caseOutput;
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
