/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository;

import java.io.*;
import java.util.*;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.PairFunction;
// import org.apache.spark.storage.StorageLevel;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.model.BCase;
import kr.ac.pusan.bsclab.bab.ws.model.BEvent;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class SparkRepositoryReaderWithArtificial implements ISparkRepositoryReader, Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private SparkExecutor executor;
  private IRepository repository;
  private JavaPairRDD<String, ICase> casesRDD;
  private Map<String, ICase> cases;
  private String repositoryURI;

  public SparkRepositoryReaderWithArtificial(SparkExecutor se, String repositoryURI,
      Configuration config) {
    this.executor = se;
    try {
      this.repositoryURI = repositoryURI;
      String repo = se.getFileUtil().loadTextFile(se, se.getHdfsURI(repositoryURI + ".brepo"));
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      this.repository = mapper.readValue(repo, BRepository.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public IRepository getRepository() {
    return repository;
  }

  @Override
  public JavaPairRDD<String, ICase> getCasesRDD() {

    if (casesRDD == null) {
      JavaRDD<String> caseStrRdd = executor.getSparkFileUtil().loadRdd(executor, executor.getHdfsURI(repositoryURI + ".trepo"));
      casesRDD = caseStrRdd.mapToPair(new PairFunction<String, String, ICase>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;
        ObjectMapper mapper;

        @Override
        public Tuple2<String, ICase> call(String caseJson) throws Exception {
          if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SimpleModule module =
                new SimpleModule("EnhancedDatesModule", new Version(0, 0, 0, "0", "0", "0"));
            module.addDeserializer(IEvent.class, new JsonDeserializer<IEvent>() {

              @Override
              public IEvent deserialize(JsonParser jp, DeserializationContext dc)
                  throws IOException, JsonProcessingException {
                return jp.readValueAs(BEvent.class);
              }
            });
            mapper.registerModule(module);
          }
          BCase bcase = mapper.readValue(caseJson, BCase.class);
          IEvent[] events = bcase.getEvents().values().toArray(new IEvent[0]);

          if (events.length > 0) {
            IEvent se = events[0];
            IEvent ee = events[events.length - 1];
            BEvent ase = new BEvent("-1", se.getUri(), "Start", "Artificial", se.getOriginator(),
                se.getTimestamp(), se.getResource());
            BEvent aee = new BEvent(String.valueOf(events.length), ee.getUri(), "End", "Artificial",
                ee.getOriginator(), ee.getTimestamp(), ee.getResource());
            bcase.getEvents().clear();
            bcase.getEvents().put(ase.getId(), ase);
            for (IEvent e : events) {
              bcase.getEvents().put(e.getId(), e);
            }
            bcase.getEvents().put(aee.getId(), aee);
          }
          return new Tuple2<String, ICase>(bcase.getId(), bcase);
        }

      });
    }
    return casesRDD;
  }

  @Override
  public Map<String, ICase> getCases() {
    if (cases == null) {
      cases = getCasesRDD().collectAsMap();
    }
    return cases;
  }

}
