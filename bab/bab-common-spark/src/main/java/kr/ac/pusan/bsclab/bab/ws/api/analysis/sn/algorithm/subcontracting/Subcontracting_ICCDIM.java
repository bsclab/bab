/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.subcontracting;

import java.util.Iterator;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class Subcontracting_ICCDIM extends SubcontractingAlgorithm {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private final List<String> originatorNameList;
  private final JavaPairRDD<String, ICase> rdd;

  public Subcontracting_ICCDIM(final List<String> originatorNameList,
      final JavaPairRDD<String, ICase> rdd) {
    this.originatorNameList = originatorNameList;
    this.rdd = rdd;
  }

  @Override
  public SocialMatrix calculate(final double beta, final int depth) {
    List<Tuple2<String, ICase>> cases = rdd.collect();
    int originatorNum = originatorNameList.size();
    SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);

    int count = 0;
    // double normalVal = 0.0d;

    // Iterator<Tuple2<String,ICase>> casesIter = cases.iterator();
    for (Tuple2<String, ICase> t : cases) {
      Iterator<IEvent> entIter = t._2.getEvents().values().iterator();
      IEvent ent1, ent2, ent3;
      SocialMatrix m = new SocialMatrix(originatorNum, originatorNum, 0.0d);

      if (entIter.hasNext()) {
        ent1 = entIter.next();
        if (entIter.hasNext()) {
          ent2 = entIter.next();
          if (entIter.hasNext()) {
            count++;
          }

          while (entIter.hasNext()) {
            int row, col;
            ent3 = entIter.next();
            if (ent1.getOriginator() != null && ent2.getOriginator() != null
                && ent3.getOriginator() != null) {
              if (ent1.getOriginator().equalsIgnoreCase(ent3.getOriginator())) {
                row = D.findIndexBy(ent1.getOriginator());
                col = D.findIndexBy(ent2.getOriginator());
                m.set(row, col, 1);
              }
            }
            ent1 = ent2;
            ent2 = ent3;
          }
        }
      }

      for (int i = 0; i < originatorNum; i++) {
        for (int j = 0; j < originatorNum; j++) {
          D.set(i, j, D.get(i, j) + m.get(i, j));
        }
      }
    }

    // for(Tuple2<String, ICase> t : cases){
    // ICase icase = t._2;
    // List<IEvent> events = new
    // ArrayList<IEvent>(icase.getEvents().values());
    //
    // if(events.size() < 3) continue;
    //
    // int minK = 0;
    // if(events.size() < depth)
    // minK = events.size();
    // else
    // minK = depth + 1;
    //
    // if(minK < 3) minK = 3;
    //
    // for(int k = 2; k < minK; k++){
    // normalVal += Math.pow(beta, k-2) * (events.size() - k) * (k - 1);
    // SocialMatrix m = new SocialMatrix(originatorNum, originatorNum,
    // 0.0d);
    //
    // for(int i=0; i<events.size() - k; i++){
    // try{
    // IEvent ent = events.get(i);
    // IEvent ent3 = events.get(i+k);
    //
    // if(ent.getOriginator() != null && ent3.getOriginator() != null){
    // if(ent.getOriginator().equalsIgnoreCase(ent3.getOriginator())){
    // for(int j= i + 1; j < i + k; j++){
    // IEvent ent2 = events.get(j);
    // if(ent2.getOriginator() == null) continue;
    // int row = D.findIndexBy(ent.getOriginator());
    // int col = D.findIndexBy(ent2.getOriginator());
    // m.set(row, col, 1);
    // }
    // }
    // }
    // }catch(IndexOutOfBoundsException ee){};
    // }
    //
    // for(int i=0; i<originatorNum; i++){
    // for(int j=0; j<originatorNum; j++){
    // D.set(i, j, D.get(i, j) + m.get(i, j) * Math.pow(beta, k - 2));
    // }
    // }
    // }
    // }
    //
    // for(int i=0; i<D.originatorSize(); i++){
    // for(int j=0; j<D.originatorSize(); j++){
    // D.set(i, j, D.get(i, j)/count);
    // }
    // }

    return UtilOperation.normalize(D, count);
  }

}
