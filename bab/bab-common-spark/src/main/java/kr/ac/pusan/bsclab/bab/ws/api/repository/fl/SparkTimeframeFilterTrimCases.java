package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.spark.api.java.function.PairFunction;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class SparkTimeframeFilterTrimCases
    implements PairFunction<Tuple2<String, ICase>, String, ICase> {

  private static final long serialVersionUID = 1L;
  private final SparkTimeframeFilter filter;

  public SparkTimeframeFilterTrimCases(SparkTimeframeFilter f) {
    filter = f;
  }

  @Override
  public Tuple2<String, ICase> call(Tuple2<String, ICase> bcase) throws Exception {
    Map<String, IEvent> events = new LinkedHashMap<String, IEvent>();
    for (IEvent e : bcase._2().getEvents().values()) {
      if (e.getTimestamp() >= filter.getMin() && e.getTimestamp() <= filter.getMax()) {
        events.put(e.getId(), e);
      }
    }
    if (events.size() < 1) {
      return null;
    }
    bcase._2.getEvents().clear();
    bcase._2.getEvents().putAll(events);
    return bcase;
  }

}
