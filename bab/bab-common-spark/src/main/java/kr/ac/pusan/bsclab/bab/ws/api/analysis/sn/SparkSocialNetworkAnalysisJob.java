/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn;

import java.util.HashMap;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkHandoverOfWork;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkReassignment;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkSimilarTask;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkSocialNetworkAlgorithm;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkSubcontracting;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.SparkWorkingTogether;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Social network analysis <br>
 * <br>
 * Config class: {@link SocialNetworkAnalysisConfiguration}<br>
 * Result class: {@link SocialNetworkAnalysis}
 * 
 * @author Choi Deokil (cdi1318@gmail.com)
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkSocialNetworkAnalysisJob extends SocialNetworkAnalysisJob {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private static final String subconstracting = "subcontracting";
  private static final String handoverofwork = "handoverofwork";
  private static final String workingtogether = "workingtogether";
  private static final String similartask = "similartask";
  private static final String reassignment = "reassignment";

  private static Map<String, SparkSocialNetworkAlgorithm> algorithms =
      new HashMap<String, SparkSocialNetworkAlgorithm>();

  private static void initialize() {
    algorithms.put(subconstracting, new SparkSubcontracting());
    algorithms.put(handoverofwork, new SparkHandoverOfWork());
    algorithms.put(workingtogether, new SparkWorkingTogether());
    algorithms.put(similartask, new SparkSimilarTask()); // not implemented
    algorithms.put(reassignment, new SparkReassignment());
  }

  @Override
	@BabService(
		name = "AnalysisSocialNetworkJob", 
		title = "Social Network Analysis",
		requestClass = SocialNetworkAnalysisConfiguration.class, 
		responseClass = SocialNetworkAnalysis.class,
		legacyJobExtension = ".snans"
	)
  public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      /* Receive Request */
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      SocialNetworkAnalysisConfiguration config =
          mapper.readValue(json, SocialNetworkAnalysisConfiguration.class);
      String outputURI = se.getContextUri(res.getUri());

      /* Initialize Response */
      SocialNetworkAnalysis analysis = new SocialNetworkAnalysis();
      analysis.setId(System.currentTimeMillis() + "snans");

      /* Run Social Network Algorithm */
      initialize();
      SparkSocialNetworkAlgorithm socialNetworkAlgorithm =
          algorithms.get(config.getMethod().getAlgorithm().toLowerCase());
      analysis = socialNetworkAlgorithm.run(config, analysis, se);

      /* Make Response */
      RawJobResult result = new RawJobResult("analysis.SocialNetwork", outputURI, outputURI,
          mapper.writeValueAsString(analysis));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".snans", result.getResponse());

      /* Return Result */
      return result;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
}
