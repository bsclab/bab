//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.AbstractRepositoryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.BCase;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import scala.Tuple2;

public class SparkFilteringJob extends AbstractRepositoryJob {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Configuration
   */
  private FilteringJobConfiguration config;

  @Override
  public IJobResult run(String json, IResource res, IExecutor se) throws Exception {
    try {
      SparkExecutor.debug("test");
      ObjectMapper mapper = new ObjectMapper();
      config = mapper.readValue(json, FilteringJobConfiguration.class);
      String outputURI = se.getContextUri(res.getUri());
      SparkExecutor spe = (SparkExecutor) se;

      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      JavaPairRDD<String, ICase> casesRDD = reader.getCasesRDD();
      // availableFilters.put(TimeFrameFilter.NAME, SparkTimeframeFilter.class);
      // availableFilters.put(PerformanceFilter.NAME, PerformanceFilter.class);
      // availableFilters.put(AttributeFilter.NAME, AttributeFilter.class);
      // availableFilters.put(EndpointFilter.NAME, EndpointFilter.class);
      // availableFilters.put(VariationFilter.NAME, VariationFilter.class);

      List<SparkFilter> sparkFilter = new ArrayList<SparkFilter>();

      for (Map<String, String> filter : config.getFilters()) {
        if (filter.containsKey("type")) {
          String filterType = filter.get("type");
          if (filterType.equalsIgnoreCase(TimeFrameFilter.NAME)) {
            SparkTimeframeFilter stf = new SparkTimeframeFilter();
            stf.getOptions().putAll(filter);
            sparkFilter.add(stf);
          } else if (filterType.equalsIgnoreCase(PerformanceFilter.NAME)) {

          }
        }
      }

      Broadcast<List<SparkFilter>> bsparkFilter = spe.getContext().broadcast(sparkFilter);
      sparkFilter = bsparkFilter.value();

      for (SparkFilter sf : sparkFilter) {
        casesRDD = sf.applyFilter(casesRDD);
      }

      casesRDD.map(new Function<Tuple2<String, ICase>, String>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;
        ObjectMapper jsonMapper = new ObjectMapper();

        @Override
        public String call(Tuple2<String, ICase> arg0) throws Exception {
          return jsonMapper.writeValueAsString((BCase) arg0._2());
        }
      }).saveAsTextFile(outputURI + ".trepo");


      if (casesRDD.count() < 1) {
        return null;
      }
      Function2<Integer, Integer, Integer> integerReducer =
          new Function2<Integer, Integer, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(Integer arg0, Integer arg1) throws Exception {
              return arg0 + arg1;
            }
          };

      Map<String, Integer> cases =
          casesRDD.mapToPair(new PairFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> call(Tuple2<String, ICase> arg0) throws Exception {
              return new Tuple2<String, Integer>(arg0._1(), 1);
            }
          }).reduceByKey(integerReducer).collectAsMap();

      double eventCounts = casesRDD.mapToDouble(new DoubleFunction<Tuple2<String, ICase>>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public double call(Tuple2<String, ICase> arg0) throws Exception {
          return arg0._2().getEvents().size();
        }
      }).sum();
      Map<String, Integer> activities =
          casesRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Integer>> call(Tuple2<String, ICase> arg0)
                throws Exception {
              Map<String, Integer> r = new LinkedHashMap<String, Integer>();
              for (IEvent e : arg0._2().getEvents().values()) {
                String k = e.getLabel();
                if (!r.containsKey(k))
                  r.put(k, 0);
                r.put(k, r.get(k) + 1);
              }
              List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
              for (String k : r.keySet()) {
                Integer f = r.get(k);
                result.add(new Tuple2<String, Integer>(k, f));
              }
              return result;
            }
          }).reduceByKey(integerReducer).collectAsMap();
      Map<String, Integer> activityTypes =
          casesRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Integer>> call(Tuple2<String, ICase> arg0)
                throws Exception {
              Map<String, Integer> r = new LinkedHashMap<String, Integer>();
              for (IEvent e : arg0._2().getEvents().values()) {
                String k = e.getType();
                if (!r.containsKey(k))
                  r.put(k, 0);
                r.put(k, r.get(k) + 1);
              }
              List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
              for (String k : r.keySet()) {
                Integer f = r.get(k);
                result.add(new Tuple2<String, Integer>(k, f));
              }
              return result;
            }
          }).reduceByKey(integerReducer).collectAsMap();
      Map<String, Integer> originators =
          casesRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Integer>> call(Tuple2<String, ICase> arg0)
                throws Exception {
              Map<String, Integer> r = new LinkedHashMap<String, Integer>();
              for (IEvent e : arg0._2().getEvents().values()) {
                String k = e.getOriginator();
                if (!r.containsKey(k))
                  r.put(k, 0);
                r.put(k, r.get(k) + 1);
              }
              List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
              for (String k : r.keySet()) {
                Integer f = r.get(k);
                result.add(new Tuple2<String, Integer>(k, f));
              }
              return result;
            }
          }).reduceByKey(integerReducer).collectAsMap();
      Map<String, Integer> resources =
          casesRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Integer>> call(Tuple2<String, ICase> arg0)
                throws Exception {
              Map<String, Integer> r = new LinkedHashMap<String, Integer>();
              for (IEvent e : arg0._2().getEvents().values()) {
                String k = e.getResource();
                if (!r.containsKey(k))
                  r.put(k, 0);
                r.put(k, r.get(k) + 1);
              }
              List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
              for (String k : r.keySet()) {
                Integer f = r.get(k);
                result.add(new Tuple2<String, Integer>(k, f));
              }
              return result;
            }
          }).reduceByKey(integerReducer).collectAsMap();
      Map<String, Integer> attributes =
          casesRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Integer>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, Integer>> call(Tuple2<String, ICase> arg0)
                throws Exception {
              Map<String, Integer> r = new LinkedHashMap<String, Integer>();
              Map<String, Object> attributes = arg0._2().getAttributes();
              for (String attr : attributes.keySet()) {
                String k = "C|" + attr + "|" + attributes.get(attr);
                if (!r.containsKey(k))
                  r.put(k, 0);
                r.put(k, r.get(k) + 1);
              }
              for (IEvent e : arg0._2().getEvents().values()) {
                attributes = e.getAttributes();
                for (String attr : attributes.keySet()) {
                  String k = "E|" + attr + "|" + attributes.get(attr);
                  if (!r.containsKey(k))
                    r.put(k, 0);
                  r.put(k, r.get(k) + 1);
                }
              }
              List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
              for (String k : r.keySet()) {
                Integer f = r.get(k);
                result.add(new Tuple2<String, Integer>(k, f));
              }
              return result;
            }

          }).reduceByKey(integerReducer).collectAsMap();

      Long startTime = casesRDD.map(new Function<Tuple2<String, ICase>, Long>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public Long call(Tuple2<String, ICase> arg0) throws Exception {
          Long result = null;
          for (IEvent e : arg0._2().getEvents().values()) {
            if (result == null && e.getTimestamp() > 0) {
              result = e.getTimestamp();
            }
            if (e.getTimestamp() > 0 && result > e.getTimestamp())
              result = e.getTimestamp();
          }
          return result;
        }
      }).reduce(new Function2<Long, Long, Long>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public Long call(Long arg0, Long arg1) throws Exception {
          return arg0 < arg1 ? arg0 : arg1;
        }
      });

      Long endTime = casesRDD.map(new Function<Tuple2<String, ICase>, Long>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public Long call(Tuple2<String, ICase> arg0) throws Exception {
          Long result = null;
          for (IEvent e : arg0._2().getEvents().values()) {
            if (result == null && e.getTimestamp() > 0) {
              result = e.getTimestamp();
            }
            if (e.getTimestamp() > 0 && result < e.getTimestamp())
              result = e.getTimestamp();
          }
          return result;
        }
      }).reduce(new Function2<Long, Long, Long>() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public Long call(Long arg0, Long arg1) throws Exception {
          return arg0 > arg1 ? arg0 : arg1;
        }
      });

      BRepository repository = new BRepository("BRepository@" + System.currentTimeMillis(),
          res.getUri(), config.getRepositoryURI(), System.currentTimeMillis(), cases.size(),
          (int) eventCounts, activities.size(), activityTypes.size(), originators.size(),
          resources.size());
      repository.setTimestampStart(startTime);
      repository.setTimestampEnd(endTime);
      toString();

      BRepository urepository = (BRepository) reader.getRepository();

      repository.setName(urepository.getName());
      repository.setDescription(urepository.getDescription());
      repository.setMapping(urepository.getMapping());

      repository.getCases().putAll(cases);
      repository.getActivities().putAll(activities);
      repository.getActivityTypes().putAll(activityTypes);
      repository.getOriginators().putAll(originators);
      repository.getResources().putAll(resources);

      for (String k : attributes.keySet()) {
        String[] keys = k.split("\\|", 3);
        String type = keys[0];
        String key = keys[1];
        String value = keys[2];
        if (keys.length > 2) {
          if (type.equalsIgnoreCase("C")) {
            if (!repository.getNoOfCaseAttributes().containsKey(key))
              repository.getNoOfCaseAttributes().put(key, 0);
            repository.getNoOfCaseAttributes().put(key,
                repository.getNoOfCaseAttributes().get(key) + 1);
            if (!repository.getCaseAttributes().containsKey(key))
              repository.getCaseAttributes().put(key, new TreeMap<String, Integer>());
            if (!repository.getCaseAttributes().get(key).containsKey(value))
              repository.getCaseAttributes().get(key).put(value, 0);
            repository.getCaseAttributes().get(key).put(value,
                repository.getCaseAttributes().get(key).get(value) + 1);
          } else {
            if (!repository.getNoOfEventAttributes().containsKey(key))
              repository.getNoOfEventAttributes().put(key, 0);
            repository.getNoOfEventAttributes().put(key,
                repository.getNoOfEventAttributes().get(key) + 1);
            if (!repository.getEventAttributes().containsKey(key))
              repository.getEventAttributes().put(key, new TreeMap<String, Integer>());
            if (!repository.getEventAttributes().get(key).containsKey(value))
              repository.getEventAttributes().get(key).put(value, 0);
            repository.getEventAttributes().get(key).put(value,
                repository.getEventAttributes().get(key).get(value) + 1);
          }
        }
      }


      RawJobResult result = new RawJobResult("repository.Filtered", outputURI, outputURI,
          mapper.writeValueAsString(repository));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".brepo", result.getResponse());
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
