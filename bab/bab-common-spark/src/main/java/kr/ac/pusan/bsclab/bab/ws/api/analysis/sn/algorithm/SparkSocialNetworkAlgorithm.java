/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Frequency;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Node;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public abstract class SparkSocialNetworkAlgorithm implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  public abstract SocialNetworkAnalysis run(SocialNetworkAnalysisConfiguration config,
      SocialNetworkAnalysis analysis, IExecutor se);

  private static final String deokilDebug = "************DEOKIL DEBUGGER************ : ";

  public static void printDebug(String str) {
    System.err.println(deokilDebug + str);
  }

  public Map<String, Arc> makeArcs(SocialMatrix D) {
    Map<String, Arc> arcs = new TreeMap<String, Arc>();
    for (int i = 0; i < D.getRow(); i++) {
      for (int j = 0; j < D.getCol(); j++) {
        String originator1 = D.findOriginatorBy(i);
        String originator2 = D.findOriginatorBy(j);
        Arc arc = new Arc();
        arc.setSource(originator1);
        arc.setTarget(originator2);
        Frequency frequency = new Frequency();
        frequency.setRelative(D.get(i, j));
        arc.setFrequency(frequency);
        arcs.put(originator1 + "->" + originator2, arc);
      }
    }
    return arcs;
  }

  public Map<String, Arc> makeArcs(JavaPairRDD<String, Double> resultRDD,
      Map<String, Double> originatorNameFrequencyPerCaseMap) {
    Map<String, Arc> arcs = new TreeMap<String, Arc>();
    List<Tuple2<String, Double>> l = resultRDD.collect();
    for (Tuple2<String, Double> t : l) {
      String[] originators = t._1.split("->");
      String originator1 = originators[0];
      Double originator1TotalCount = originatorNameFrequencyPerCaseMap.get(originator1);
      if (originator1TotalCount == null)
        continue;
      double value = t._2 / (double) originator1TotalCount;

      Arc arc = new Arc();
      arc.setSource(originators[0]);
      arc.setTarget(originators[1]);
      arc.getFrequency().setRelative(value);
      arcs.put(t._1, arc);
    }
    return arcs;
  }

  public Map<String, Double> getMapOfOriginatorNameFrequencyPerCase(SparkRepositoryReader reader) {
    return getMapOfOriginatorNameFrequencyPerCase(reader, null);
  }

  public Map<String, Double> getMapOfOriginatorNameFrequencyPerCase(SparkRepositoryReader reader,
      List<String> selectedOriginators) {
    OriginatorCounter counter = (selectedOriginators == null || selectedOriginators.size() < 1)
        ? new AllOriginatorCounter() : new SelectedOriginatorCounter(selectedOriginators);

    JavaPairRDD<String, Double> originatorPairRDD =
        reader.getCasesRDD().mapPartitionsToPair(counter).reduceByKey(counter);

    Map<String, Double> originatorNameFrequencyPerCaseMap = originatorPairRDD.collectAsMap();
    return originatorNameFrequencyPerCaseMap;
  }

  public Map<String, Node> makeNodes(List<String> originatorNameList,
      Map<String, Double> originatorNameFrequencyPerCaseMap) {
    Map<String, Node> nodes = new TreeMap<String, Node>();
    for (String s : originatorNameList) {
      Node node = new Node();
      Frequency frequency = new Frequency();
      frequency.absolute = originatorNameFrequencyPerCaseMap.get(s).intValue();
      node.setFrequency(frequency);
      nodes.put(s, node);
    }
    return nodes;
  }

  static abstract class OriginatorCounter
      implements Serializable, PairFlatMapFunction<Iterator<Tuple2<String, ICase>>, String, Double>,
      Function2<Double, Double, Double> {
    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;

    public Double call(Double arg0, Double arg1) throws Exception {
      return arg0 + arg1;
    }
  }

  static class SelectedOriginatorCounter extends OriginatorCounter {

    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;
    private List<String> selectedOriginators;

    public SelectedOriginatorCounter(List<String> selectedOriginators) {
      this.selectedOriginators = selectedOriginators;
    }

    @Override
    public Iterable<Tuple2<String, Double>> call(Iterator<Tuple2<String, ICase>> arg0)
        throws Exception {
      Map<String, Tuple2<String, Double>> tempMap = new TreeMap<String, Tuple2<String, Double>>();
      while (arg0.hasNext()) {
        ICase icase = arg0.next()._2;
        for (IEvent event : icase.getEvents().values()) {
          String originatorName = event.getOriginator();
          if (selectedOriginators.contains(originatorName)) {
            if (tempMap.containsKey(originatorName)) {
              Tuple2<String, Double> t = tempMap.get(originatorName);
              tempMap.put(originatorName, new Tuple2<String, Double>(originatorName, t._2 + 1.0d));
            } else {
              tempMap.put(originatorName, new Tuple2<String, Double>(originatorName, 1.0d));
            }
          }
        }
      }
      return tempMap.values();
    }
  }

  static class AllOriginatorCounter extends OriginatorCounter {

    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Iterable<Tuple2<String, Double>> call(Iterator<Tuple2<String, ICase>> arg0)
        throws Exception {
      Map<String, Tuple2<String, Double>> tempMap = new TreeMap<String, Tuple2<String, Double>>();
      while (arg0.hasNext()) {
        Set<String> tempSet = new TreeSet<String>();

        ICase icase = arg0.next()._2;
        for (IEvent ievent : icase.getEvents().values())
          tempSet.add(ievent.getOriginator());

        Iterator<String> iter = tempSet.iterator();
        while (iter.hasNext()) {
          String originatorName = iter.next();
          if (tempMap.containsKey(originatorName)) {
            Tuple2<String, Double> t = tempMap.get(originatorName);
            tempMap.put(originatorName, new Tuple2<String, Double>(originatorName, t._2 + 1.0d));
          } else {
            tempMap.put(originatorName, new Tuple2<String, Double>(originatorName, 1.0d));
          }
        }
      }
      return tempMap.values();
    }
  }
}
