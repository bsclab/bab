package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;
import java.util.Comparator;
import scala.Tuple2;

public class TupleComparator implements Comparator<Tuple2<String, LEvent>>, Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  @Override
  public int compare(Tuple2<String, LEvent> o1, Tuple2<String, LEvent> o2) {
    return (o1._2().getStart() < o2._2().getStart()) ? -1
        : (o1._2().getStart() > o2._2().getStart()) ? 1 : 0;
  }

}
