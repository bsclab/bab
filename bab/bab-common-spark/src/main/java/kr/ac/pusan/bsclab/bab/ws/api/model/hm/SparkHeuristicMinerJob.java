/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.*;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Process discovery based on heuristic miner algorithm <br>
 * <br>
 * Config class: {@link HeuristicMinerJobConfiguration}<br>
 * Result class: {@link HeuristicModel}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */

public class SparkHeuristicMinerJob extends HeuristicMinerJob {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Configuration
	 */
	private HeuristicMinerJobConfiguration config;

	/**
	 * Result
	 */
	private HeuristicModel model;

	public SparkHeuristicMinerJob() {

	}

	public HeuristicMinerJobConfiguration getConfig() {
		return config;
	}

	public HeuristicModel getModel() {
		return model;
	}

	@Override
	@BabService(name = "ModelHeuristicJob", title = "Heuristic Miner", requestClass = HeuristicMinerJobConfiguration.class, responseClass = HeuristicModel.class, legacyJobExtension = ".hmodel")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			// JavaSparkContext sc = se.getContext();
			// FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper mapper = new ObjectMapper();
			// JSONSerializer serializer = new JSONSerializer();
			// config = new JSONDeserializer<HeuristicMinerJobConfiguration>()
			// .deserialize(json, HeuristicMinerJobConfiguration.class);
			config = mapper.readValue(json, HeuristicMinerJobConfiguration.class);
			String outputURI = se.getContextUri(res.getUri());

			SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
			// IRepository repository = reader.getRepository();

			System.out.println(reader.getCasesRDD().count());
			// Step 1. Count Nodes
			Map<String, NodeCount> nodeRdds = reader.getCasesRDD()
					// 1.1. Calculate arc frequency
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, NodeCount>() {
						/**
						 * Default serial version ID
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public Iterable<Tuple2<String, NodeCount>> call(Tuple2<String, ICase> trace) throws Exception {
							Map<String, Tuple2<String, NodeCount>> counts = new LinkedHashMap<String, Tuple2<String, NodeCount>>();
							Iterable<IEvent> events = trace._2().getEvents().values();
							for (IEvent event : events) {
								String node = event.getLabel() + " (" + event.getType() + ")";
								if (!counts.containsKey(node))
									counts.put(node, new Tuple2<String, NodeCount>(node, new NodeCount(node)));
								NodeCount arc = counts.get(node)._2();
								arc.getCases().add(trace._2().getId());
								arc.increase(0);
							}
							return counts.values();
						}
					}).reduceByKey(new Function2<NodeCount, NodeCount, NodeCount>() {
						/**
						 * Default serial version ID
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public NodeCount call(NodeCount arg0, NodeCount arg1) throws Exception {
							NodeCount reduced = new NodeCount(arg0.getNode());
							reduced.add(arg0);
							reduced.add(arg1);
							return reduced;
						}
					}).collectAsMap();

			// Step 1. Count arcs
			Map<String, ArcCount> arcs = reader.getCasesRDD()
					// 1.1. Calculate arc frequency
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, ArcCount>() {
						/**
						 * Default serial version ID
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public Iterable<Tuple2<String, ArcCount>> call(Tuple2<String, ICase> trace) throws Exception {
							Map<String, Tuple2<String, ArcCount>> counts = new LinkedHashMap<String, Tuple2<String, ArcCount>>();
							Iterable<IEvent> events = trace._2().getEvents().values();
							IEvent prev = null;
							for (IEvent event : events) {
								String from;
								if (prev == null) {
									from = ArcCount.NULLSTR;
								} else {
									from = prev.getLabel() + " (" + prev.getType() + ")";
								}
								String to = event.getLabel() + " (" + event.getType() + ")";
								String transition = from + "=>" + to;
								if (!counts.containsKey(transition))
									counts.put(transition,
											new Tuple2<String, ArcCount>(transition, new ArcCount(from, to)));
								ArcCount arc = counts.get(transition)._2();
								arc.getCases().add(trace._2().getId());
								if (prev == null) {
									arc.increase(0);
								} else {
									arc.increase(event.getTimestamp() - prev.getTimestamp());
								}
								prev = event;
							}
							if (prev != null) {
								String from = prev.getLabel() + " (" + prev.getType() + ")";
								String to = ArcCount.NULLSTR;
								String transition = from + "=>" + to;
								if (!counts.containsKey(transition))
									counts.put(transition,
											new Tuple2<String, ArcCount>(transition, new ArcCount(from, to)));
								ArcCount arc = counts.get(transition)._2();
								arc.getCases().add(trace._2().getId());
								arc.increase(0);
							}
							return counts.values();
						}
					}).reduceByKey(new Function2<ArcCount, ArcCount, ArcCount>() {
						/**
						 * Default serial version ID
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public ArcCount call(ArcCount arg0, ArcCount arg1) throws Exception {
							ArcCount reduced = new ArcCount(arg0.getFrom(), arg0.getTo());
							reduced.add(arg0);
							reduced.add(arg1);
							return reduced;
						}
					}).collectAsMap();

			// Step 2. CalculateDependency
			// 2.1. Calculate Dependency and Build Nodes
			Map<String, Integer> nodes = new LinkedHashMap<String, Integer>();
			String node;
			double ab, ba;
			double total = 0d;
			for (NodeCount nc : nodeRdds.values()) {
				node = nc.getNode();
				if (!nodes.containsKey(node))
					nodes.put(node, 0);
				total += nc.getFrequency();
				nodes.put(node, nodes.get(node) + nc.getFrequency());
			}
			for (ArcCount arc : arcs.values()) {
				if (arc.isSelfLoop()) {
					ab = arc.getFrequency();
					arc.setDependency(ab / (ab + 1));
				} else {
					ab = arc.getFrequency();
					String rtrans = arc.getTo() + "=>" + arc.getFrom();
					ba = arcs.containsKey(rtrans) ? arcs.get(rtrans).getFrequency() : 0d;
					arc.setDependency((ab - ba) / (ab + ba + 1));
				}
			}
			// 2.2. Prune arcs
			double positiveObservations = config.getPositiveObservation();
			double minimumThreshold = config.getThreshold().getDependency();
			// boolean useAllConnectedHeuristics =
			// config.getOption().isAllConnected();
			boolean accepted;
			model = new HeuristicModel(outputURI);
			// Node modelNode;
			Arc modelArc;
			for (String k : nodeRdds.keySet()) {
				NodeCount nc = nodeRdds.get(k);
				if (total != 0) {
					model.getOrAddNode(k).getFrequency()
							.setRelative(model.getOrAddNode(k).getFrequency().getAbsolute() / total);
				}
				model.getOrAddNode(k).getFrequency().setCases(nc.getCases().size());
			}
			double totala = 0d;
			for (ArcCount arc : arcs.values()) {
				accepted = arc.getFrequency() >= positiveObservations && arc.getDependency() > 0
						&& arc.getDependency() >= minimumThreshold && !arc.getFrom().equals(ArcCount.NULLSTR)
						&& !arc.getTo().equals(ArcCount.NULLSTR);
				if (accepted) {
					totala += arc.getFrequency();
				}
			}
			for (ArcCount arc : arcs.values()) {
				accepted = arc.getFrequency() >= positiveObservations && arc.getDependency() > 0
						&& arc.getDependency() >= minimumThreshold && !arc.getFrom().equals(ArcCount.NULLSTR)
						&& !arc.getTo().equals(ArcCount.NULLSTR);
				if (!accepted) {
					node = arc.getFrom();
					node = arc.getTo();
				} else {
					node = arc.getFrom();
					model.getOrAddNode(node).getFrequency().setAbsolute(nodes.get(node));
					node = arc.getTo();
					model.getOrAddNode(node).getFrequency().setAbsolute(nodes.get(node));
					modelArc = model.getOrAddArc(arc.getFrom(), arc.getTo());
					modelArc.getFrequency().setAbsolute(arc.getFrequency());
					if (totala != 0) {
						modelArc.getFrequency().setRelative(arc.getFrequency() / totala);
					}
					modelArc.getFrequency().setCases(arc.getCases().size());
					modelArc.getDuration().setTotal(arc.getDuration());
					if (arc.getDuration() < modelArc.getDuration().getMin()) {
						modelArc.getDuration().setMin(arc.getDuration());
					}
					if (arc.getDuration() > modelArc.getDuration().getMax()) {
						modelArc.getDuration().setMax(arc.getDuration());
					}
					if (modelArc.getFrequency().getAbsolute() != 0) {
						modelArc.getDuration()
								.setMean(modelArc.getDuration().getTotal() / modelArc.getFrequency().getAbsolute());
					}
					modelArc.setDependency(arc.getDependency());
				}
			}
			model.getNodes().remove(ArcCount.NULLSTR);

			RawJobResult result = new RawJobResult("model.Heuristic", outputURI, outputURI,
					mapper.writeValueAsString(model));
			se.getFileUtil().saveAsTextFile(se, outputURI + ".hmodel", result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
