/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.HandOverAlgorithm;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_ICIDIM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.workingtogether.*;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.*;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;

public class SparkWorkingTogether extends SparkSocialNetworkAlgorithm implements Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private static final String SAR = "simultaneousratio";
  private static final String DWTC = "withoutdistancecasuality";
  private static final String DWC = "withdistancecasuality";

  @Override
  public SocialNetworkAnalysis run(SocialNetworkAnalysisConfiguration config,
      SocialNetworkAnalysis analysis, IExecutor se) {

    JavaSparkContext sc = ((ISparkExecutor) se).getContext();
    SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);

    /* Originator Model for all of algorithm from Social Network */
    // if(config.getDimension().equalsIgnoreCase("event.originator")) {}
    // else if(config.getDimension().equalsIgnoreCase("event.resource") { }

    /* <Originator Name, Occurrences Count of Originator> */
    Map<String, Double> originatorNameFrequencyPerCaseMap =
        getMapOfOriginatorNameFrequencyPerCase(reader, config.getStates());
    List<String> originatorNameList = new ArrayList<String>();
    originatorNameList.addAll(originatorNameFrequencyPerCaseMap.keySet());
    Broadcast<List<String>> broadNameList = sc.broadcast(originatorNameList);

    /* Make Nodes */
    Map<String, Node> nodes = makeNodes(originatorNameList, originatorNameFrequencyPerCaseMap);
    analysis.setNodes(nodes);
    analysis
        .setOriginators(new LinkedList<String>(reader.getRepository().getOriginators().keySet()));

    /* Variables for Social Network */
    String workingTogetherBy = config.getMethod().getWorkingTogether();
    JavaPairRDD<String, Double> resultRDD = null;
    JavaPairRDD<String, ICase> rdd = reader.getCasesRDD();
    Map<String, Arc> arcs = null;

    if (workingTogetherBy.equalsIgnoreCase(SAR)) {
      WorkingTogether_SAR sar = new WorkingTogether_SAR(broadNameList.value());
      resultRDD = reader.getCasesRDD().mapPartitionsToPair(sar).reduceByKey(sar);
      arcs = makeArcs(resultRDD, originatorNameFrequencyPerCaseMap);
    }

    else if (workingTogetherBy.equalsIgnoreCase(DWTC)) {
      // not implemented
    }

    else if (workingTogetherBy.equalsIgnoreCase(DWC)) {
      HandOverAlgorithm handover = new Handover_ICIDIM(originatorNameList, rdd);
      SocialMatrix m = handover.calculate(0.5, 10);
      SocialMatrix D = new SocialMatrix(originatorNameList.size(), originatorNameList.size(), 0);
      for (int i = 0; i < originatorNameList.size(); i++) {
        for (int j = i; j < originatorNameList.size(); j++) {
          D.set(i, j, (m.get(i, j) + m.get(j, i)) / 2);
          D.set(j, i, D.get(i, j));
        }
      }
      arcs = makeArcs(D);
    }

    if (resultRDD == null) {
      WorkingTogether_SAR sar = new WorkingTogether_SAR(broadNameList.value());
      resultRDD = reader.getCasesRDD().mapPartitionsToPair(sar).reduceByKey(sar);
    }

    /* Make Arcs */
    analysis.setArcs(arcs);
    return analysis;
  }

}
