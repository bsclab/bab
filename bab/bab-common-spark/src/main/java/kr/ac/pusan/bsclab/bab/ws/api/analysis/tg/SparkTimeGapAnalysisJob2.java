/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

public class SparkTimeGapAnalysisJob2 extends TimeGapAnalysisJob2 {

  /**
   * Returns an JSON object as an output for time gap model with input parameters containing arcs,
   * nodes, and time gaps between arcs .
   *
   * @param json an absolute URL giving the base location of the image
   * @param res the location of the image, relative to the url argument
   * @param se the spark executor configuration
   * @return JSON object
   */
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  @Override
  public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      // JavaSparkContext sc = se.getContext();
      // FileSystem fs = se.getHdfsFileSystem();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      TimeGapAnalysisJobConfiguration config =
          mapper.readValue(json, TimeGapAnalysisJobConfiguration.class);
      String outputURI = se.getContextUri(res.getUri());

      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      // IRepository repository = reader.getRepository();

      /**
       * get the config value
       */
      String startActivity = "";
      String startActivityType = "";
      String endActivity = "";
      String endActivityType = "";

      if (config.getStartActivity() != null && config.getEndActivity() != null) {
        String[] temp = config.getStartActivity().split("\\(");
        startActivity = temp[0].trim();
        String[] startActivityTypes = temp[1].split("\\)");
        startActivityType = startActivityTypes[0];

        String[] temp2 = config.getEndActivity().split("\\(");
        endActivity = temp2[0].trim();
        String[] endActivityTypes = temp2[1].split("\\)");
        endActivityType = endActivityTypes[0];
      }


      /**
       * if selected PIs are not empty then process the additional information
       */
      System.err.println("case Ids Exist" + config.getCaseIds());
      // public BRepository(String id, String uri, String originalName,
      // long createdDate, int noOfCases, int noOfEvents,
      // int noOfActivities, int noOfActivityTypes, int noOfOriginator,
      // int noOfResourceClasses) {

      Map<String, Integer> cases = new TreeMap<String, Integer>();
      Map<String, Integer> activities = new TreeMap<String, Integer>();
      Map<String, Integer> activityTypes = new TreeMap<String, Integer>();
      Map<String, Integer> originators = new TreeMap<String, Integer>();
      Map<String, Integer> resources = new TreeMap<String, Integer>();
      Map<Long, Integer> timelineWhole = new TreeMap<Long, Integer>();
      Map<Long, Integer> timelineFiltered = new TreeMap<Long, Integer>();

      Long t;
      int divider = 24 * 3600 * 1000;
      for (ICase c : reader.getCases().values()) {
        for (IEvent e : c.getEvents().values()) {
          t = e.getTimestamp() / divider;
          if (!timelineWhole.containsKey(t)) {
            timelineWhole.put(t, 0);
            timelineFiltered.put(t, 0);
          }
          timelineWhole.put(t, timelineWhole.get(t) + 1);
        }
      }

      List<List<IEvent>> eventGaps = null;
      for (String pi : config.getCaseIds()) {
        String a = pi.replace("Case_", "");
        ICase c = reader.getCases().get(a);
        cases.put(a, c.getEvents().size());
        List<IEvent> events = null;
        for (IEvent currEvent : c.getEvents().values()) {
          if (events == null && currEvent.getLabel().equalsIgnoreCase(startActivity)
              && currEvent.getType().equalsIgnoreCase(startActivityType)) {
            events = new ArrayList<IEvent>();
            events.add(currEvent);
            if (eventGaps == null)
              eventGaps = new ArrayList<List<IEvent>>();
          } else if (currEvent.getLabel().equalsIgnoreCase(endActivity)
              && currEvent.getType().equalsIgnoreCase(endActivityType)) {
            events.add(currEvent);
            eventGaps.add(events);
            events = null;
          } else if (eventGaps != null && events != null) {
            events.add(currEvent);
          }
        }
      }

      int noOfEvents = 0;
      for (List<IEvent> events : eventGaps) {
        for (IEvent e : events) {
          t = e.getTimestamp() / divider;
          timelineFiltered.put(t, timelineFiltered.get(t) + 1);
          noOfEvents++;
          if (!activities.containsKey(e.getLabel()))
            activities.put(e.getLabel(), 0);
          activities.put(e.getLabel(), activities.get(e.getLabel()) + 1);
          if (!activityTypes.containsKey(e.getType()))
            activityTypes.put(e.getType(), 0);
          activityTypes.put(e.getType(), activityTypes.get(e.getType()) + 1);
          if (!originators.containsKey(e.getOriginator()))
            originators.put(e.getOriginator(), 0);
          originators.put(e.getOriginator(), originators.get(e.getOriginator()) + 1);
          if (!resources.containsKey(e.getResource()))
            resources.put(e.getResource(), 0);
          resources.put(e.getResource(), resources.get(e.getResource()) + 1);
        }
      }

      BRepository filteredRepository = new BRepository(reader.getRepository().getId(),
          reader.getRepository().getUri(), reader.getRepository().getOriginalName(),
          reader.getRepository().getCreatedDate(), cases.size(), noOfEvents, activities.size(),
          activityTypes.size(), originators.size(), resources.size());
      filteredRepository.getCases().putAll(cases);
      filteredRepository.getActivities().putAll(activities);
      filteredRepository.getActivityTypes().putAll(activityTypes);
      filteredRepository.getOriginators().putAll(originators);
      filteredRepository.getResources().putAll(resources);

      /**
       * Populate the model
       */
      TimeGapAnalysisJobModel model = new TimeGapAnalysisJobModel();
      model.setRepositoryURI(config.getRepositoryURI());
      model.setRepository(filteredRepository);
      model.setTimelineWhole(timelineWhole);
      model.setTimelineFiltered(timelineFiltered);
      // NumberFormat nf = new DecimalFormat("#0.00");
      // int max = 0, min = 0;

      /**
       * Return result
       */
      // System.err.println("masuk sini");
      IJobResult result = new RawJobResult("analysis.TimeGap", outputURI, outputURI,
          mapper.writeValueAsString(model));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".tgans", result.getResponse());
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
