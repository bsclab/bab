/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.*;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.mllib.linalg.Vector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * 
 * <br>
 * <br>
 * Config class: {@link }<br>
 * Result class: {@link }
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkHeuristicMinerJob2 extends HeuristicMinerJob2 {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private HeuristicModel model;

  public HeuristicModel getModel() {
    return model;
  }

	@Override
	@BabService(
		name = "ModelHeuristicJob2", 
		title = "Heuristic Miner 2",
		requestClass = HeuristicMinerJobConfiguration.class, 
		responseClass = HeuristicModel.class,
		legacyJobExtension = ".hmodel2"
	)
public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      HeuristicMinerJobConfiguration config =
          mapper.readValue(json, HeuristicMinerJobConfiguration.class);

//      SparkRepositoryReader2 reader =
//          new SparkRepositoryReader2((IExecutor) se, config.getRepositoryURI(), config);
	SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      IRepository repository = reader.getRepository();
      JavaPairRDD<String, ICase> traces = reader.getCasesRDD();

      Algorithm1ActivityMapping step1ActivityMapping = new Algorithm1ActivityMapping(repository);
      Map<String, Integer> activitySet = step1ActivityMapping.getActivities();

      Algorithm2FrequencyMiner step2FrequencyMiner = new Algorithm2FrequencyMiner(activitySet);
      JavaPairRDD<String, Vector> frequencySet = step2FrequencyMiner.mine(traces);

      Algorithm3DependencyMiner step3DependencyMiner = new Algorithm3DependencyMiner(config);
      JavaPairRDD<Integer, Activity> dependencySet = step3DependencyMiner.mine(frequencySet);

      Algorithm4DependencyFilter step4DependencyFilter = new Algorithm4DependencyFilter(config);
      JavaPairRDD<Integer, Activity> filteredDependencySet =
          step4DependencyFilter.mine(dependencySet);

      Map<Integer, Activity> dependencyGraph = filteredDependencySet.collectAsMap();

      model = new HeuristicModel();
      String[] activityLabels = activitySet.keySet().toArray(new String[0]);
      for (String a : activityLabels) {
        model.getOrAddNode(a);
      }
      for (int ai : dependencyGraph.keySet()) {
        String as = activityLabels[ai];
        Activity a = dependencyGraph.get(ai);
        Dependency d = a.getL1();
        if (d != null && d.isAccepted()) {
          model.getOrAddArc(as, as, d.getFrequency(), d.getDependency());
        }
        for (int ri : a.getInput().keySet()) {
          String rs = activityLabels[ri];
          d = a.getInput().get(ri);
          if (d != null && d.isAccepted()) {
            model.getOrAddArc(rs, as, d.getFrequency(), d.getDependency());
          }
        }
        for (int ri : a.getOutput().keySet()) {
          String rs = activityLabels[ri];
          d = a.getOutput().get(ri);
          if (d != null && d.isAccepted()) {
            model.getOrAddArc(as, rs, d.getFrequency(), d.getDependency());
          }
        }
        for (int ri : a.getInputL2().keySet()) {
          String rs = activityLabels[ri];
          d = a.getInputL2().get(ri);
          if (d != null && d.isAccepted()) {
            model.getOrAddArc(rs, as, d.getFrequency(), d.getDependency());
          }
        }
        for (int ri : a.getOutputL2().keySet()) {
          String rs = activityLabels[ri];
          d = a.getOutputL2().get(ri);
          if (d != null && d.isAccepted()) {
            model.getOrAddArc(as, rs, d.getFrequency(), d.getDependency());
          }
        }
      }

      String outputURI = se.getContextUri(res.getUri());
      RawJobResult result = new RawJobResult("model.Heuristic2", outputURI, outputURI,
          mapper.writeValueAsString(model));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".hmodel2", result.getResponse());
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
