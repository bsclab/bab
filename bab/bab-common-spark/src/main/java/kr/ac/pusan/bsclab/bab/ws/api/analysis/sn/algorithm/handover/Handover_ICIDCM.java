/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

public class Handover_ICIDCM extends HandOverAlgorithm {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private final List<String> originatorNameList;
  private final JavaPairRDD<String, ICase> rdd;

  public Handover_ICIDCM(final List<String> originatorNameList,
      final JavaPairRDD<String, ICase> rdd) {
    this.originatorNameList = originatorNameList;
    this.rdd = rdd;
  }

  @Override
  public SocialMatrix calculate(double beta, int depth) {
    List<Tuple2<String, ICase>> cases = rdd.collect();
    int originatorNum = originatorNameList.size();

    SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);
    double normalVal = 0.0d;

    for (Tuple2<String, ICase> t : cases) {
      Iterator<IEvent> entIter = t._2.getEvents().values().iterator();
      IEvent ent, ent2;

      if (entIter.hasNext()) {
        int flag = 0;
        ent = entIter.next();

        // SocialMatrix m = new SocialMatrix(originatorNum, originatorNum,0);
        ArrayList<String> userList = new ArrayList<String>();

        while (entIter.hasNext()) {
          userList.add(ent.getOriginator());

          int row, col;
          ent2 = entIter.next();

          flag++;
          normalVal++;

          if (ent.getOriginator() != null && ent2.getOriginator() != null) {
            row = D.findIndexBy(ent.getOriginator());
            col = D.findIndexBy(ent2.getOriginator());
            D.set(row, col, D.get(row, col) + 1.0);
          }

          if (ent2.getOriginator() != null) {
            col = D.findIndexBy(ent2.getOriginator());
            for (int i = 0; i < depth - 1 && i < flag - 1; i++) {
              if (userList.get(userList.size() - i - 2) != null) {
                row = D.findIndexBy(userList.get(userList.size() - i - 2));
                D.set(row, col, D.get(row, col) + Math.pow(beta, i + 1));
              }
              normalVal += Math.pow(beta, i + 1);
            }
          }
          ent = ent2;
        }
      }
    }
    return UtilOperation.normalize(D, normalVal);
  }
}
