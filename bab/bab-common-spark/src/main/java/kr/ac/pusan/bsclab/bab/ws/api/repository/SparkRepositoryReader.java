/*
 *
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.spark.api.java.JavaPairRDD;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

public class SparkRepositoryReader implements ISparkRepositoryReader,
    IFilteredRepositoryReader<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>,
    Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private static Map<String, Class<? extends ISparkRepositoryReader>> factories;

  private ISparkExecutor executor;
  private ISparkRepositoryReader reader;

  private IRepository repository;
  private JavaPairRDD<String, ICase> casesRDD;
  private Map<String, ICase> cases;
  private List<IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>> filters;

  private static Class<? extends ISparkRepositoryReader> getReader(String type) {
    if (factories == null) {
      factories = new LinkedHashMap<String, Class<? extends ISparkRepositoryReader>>();
      factories.put("hdfs", SparkHdfsRepositoryReader.class);
      factories.put("mysql", SparkMysqlRepositoryReader.class);
      factories.put("presto", SparkPrestoRepositoryReader.class);
      factories.put("presto2", SparkPrestoRepositoryReader2.class);
    }
    return factories.get(type);
  }

  public SparkRepositoryReader(IExecutor se, String repositoryURI, Configuration config) {
    this.executor = (ISparkExecutor) se;
    executor.getClass();
    try {
      this.reader =
          (ISparkRepositoryReader) getReader(config.getRepositoryType())
                                                                        .getConstructors()[0].newInstance(
                                                                            new Object[] {se,
                                                                                repositoryURI,
                                                                                config});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public IRepository getRepository() {
    if (repository == null) {
      repository = reader.getRepository();
    }
    return repository;
  }

  @Override
  public JavaPairRDD<String, ICase> getCasesRDD() {
    if (casesRDD == null) {
      casesRDD = reader.getCasesRDD();
    }
    return casesRDD;
  }

  @Override
  public Map<String, ICase> getCases() {
    if (cases == null) {
      cases = getCasesRDD().collectAsMap();
    }
    return cases;
  }

  @Override
  public List<IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>> getFilters() {
    return filters;
  }

  @Override
  public void applyFilters() {
    if (getFilters() != null) {
      for (IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>> filter : getFilters()) {
        casesRDD = filter.apply(casesRDD);
      }
    }
  }

}
