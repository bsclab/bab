package kr.ac.pusan.bsclab.bab.ws.api.repository.im.csv.brepo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Import immediate repository from CSV file format <br>
 * <br>
 * Config class: {@link ImportJobConfiguration}<br>
 * Result class: {@link ImportJobResult}
 *
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkCsvImportJob extends CsvImportJob { 

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  @Override
  public IJobResult run(String json, IResource res, IExecutor se) {
    try {
        FileSystem fs = ((ISparkExecutor) se).getHdfsFileSystem();
      ObjectMapper jsonMapper = new ObjectMapper();
      ImportJobConfiguration config = jsonMapper.readValue(json, ImportJobConfiguration.class);
      String outputURI = se.getContextUri(config.getRepositoryURI());
      int bufferSize = 0;

      BufferedReader br = new BufferedReader(
              new InputStreamReader(fs.open(new Path(se.getContextUri(config.getRawPath()))), "UTF-8"));
      String delimeter = ",";

      String metadataLine = br.readLine();
      if (metadataLine != null) {
        ImportJobResult result = new ImportJobResult();
        result.setOriginalFormat("CSV");
        result.setRawPath(config.getRawPath());
        result.setRepositoryURI(config.getRepositoryURI());

        // JavaRDD<String> dataRDD = sc.parallelize(new ArrayList<String>());
        int partNumber = 0;
        List<String> columns = new ArrayList<String>();
        List<String> dataStringBuffer = new ArrayList<String>();
        StringTokenizer csvTokenizer = new StringTokenizer(metadataLine, delimeter);
        while (csvTokenizer.hasMoreElements()) {
          String column = csvTokenizer.nextToken();
          columns.add(column);
          result.getOrAddDimension(column);
        }
        String dataLine;
        int lineCount = 0;
        while ((dataLine = br.readLine()) != null) {
          lineCount++;
          csvTokenizer = new StringTokenizer(dataLine, delimeter);
          int i = 0;
          Map<String, String> data = new TreeMap<String, String>();
          while (csvTokenizer.hasMoreElements() && i < columns.size()) {
            String column = columns.get(i);
            String value = csvTokenizer.nextToken();
            data.put(column, value);
            result.increaseDimensionalStateFrequency(column, value);
            i++;
          }
          if (i < columns.size()) {
            for (int j = i; j < columns.size(); j++) {
              data.put(columns.get(j), null);
            }
          }
          String dataJson = jsonMapper.writeValueAsString(data);
          dataStringBuffer.add(dataJson);
          bufferSize += dataJson.length();
          if (lineCount >= 262144 || bufferSize >= HDFS_BLOCK_SIZE) {
            lineCount = 0;
            StringBuilder sb = new StringBuilder();
            for (String l : dataStringBuffer) {
              sb.append(l)
                .append("\n");
            }
            se.getFileUtil()
              .saveAsTextFile(se, outputURI + ".irepo/part-0000" + String.valueOf(partNumber),
                  sb.toString());

            // dataRDD = dataRDD.union(sc.parallelize(dataStringBuffer));
            dataStringBuffer = new ArrayList<String>();
            bufferSize = 0;
            partNumber++;
          }
        }
        if (dataStringBuffer.size() > 0) {
          StringBuilder sb = new StringBuilder();
          for (String l : dataStringBuffer) {
            sb.append(l)
              .append("\n");
          }
          se.getFileUtil()
            .saveAsTextFile(se, outputURI + ".irepo/part-0000" + String.valueOf(partNumber),
                sb.toString());
          // dataRDD = dataRDD.union(sc.parallelize(dataStringBuffer));
        }
        // dataRDD.saveAsTextFile(outputURI + ".irepo");

        String metadataJson = jsonMapper.writeValueAsString(result);
        RawJobResult response =
            new RawJobResult("repository.Repository", outputURI, outputURI, metadataJson);
        se.getFileUtil()
          .saveAsTextFile(se, outputURI + ".mrepo", response.getResponse());
        return response;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}

