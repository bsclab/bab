/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.reassign;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

public class Reassignment_CM extends ReassignmentAlgorithm {

  private final List<String> originatorNameList;
  private final JavaPairRDD<String, ICase> rdd;

  public Reassignment_CM(final List<String> originatorNameList,
      final JavaPairRDD<String, ICase> rdd) {
    this.originatorNameList = originatorNameList;
    this.rdd = rdd;
  }

  @Override
  public SocialMatrix calculate() {
    List<Tuple2<String, ICase>> cases = rdd.collect();
    int originatorNum = originatorNameList.size();
    int count = 0;
    SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);

    for (Tuple2<String, ICase> t : cases) {
      Iterator<IEvent> entIter = t._2.getEvents().values().iterator();
      List<IEvent> events = new ArrayList<IEvent>(t._2.getEvents().values());

      IEvent ent1 = null, ent2 = null;
      int index = 0;
      count += events.size() - 1;

      if (entIter.hasNext()) {
        ent1 = entIter.next();
        index++;

        while (entIter.hasNext()) {
          int row, col;
          if (ent1.getOriginator() != null) {
            if (ent1.getType() != null && ent1.getType().equals("reassign")) {
              for (int i = index; i < events.size(); i++) {
                try {
                  ent2 = events.get(i);
                } catch (IndexOutOfBoundsException ee) {
                } ;

                if (ent2.getOriginator() != null) {
                  if (ent1.getLabel().equals(ent2.getLabel())) {
                    row = D.findIndexBy(ent1.getOriginator());
                    col = D.findIndexBy(ent2.getOriginator());
                    D.set(row, col, D.get(row, col) + 1.0d);
                    break;
                  }
                }
              }
            }
          }
          ent1 = entIter.next();
          index++;
        }
      }
    }

    return UtilOperation.normalize(D, count);
  }
}
