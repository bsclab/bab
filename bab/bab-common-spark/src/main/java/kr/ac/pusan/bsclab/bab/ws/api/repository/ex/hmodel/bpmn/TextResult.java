package kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

public class TextResult extends Result {

	private static final long serialVersionUID = 1L;
	
	protected String text;

	public TextResult(String text) {
		this.text = text;
	}
	
	public TextResult() {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return getText();
	}

}
