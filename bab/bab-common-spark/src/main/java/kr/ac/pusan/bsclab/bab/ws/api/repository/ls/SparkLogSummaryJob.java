//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.AbstractRepositoryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Generate detailed log summary algorithm <br>
 * <br>
 * Config class: {@link LogSummaryJobConfiguration}<br>
 * Result class: {@link LogSummary}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkLogSummaryJob extends AbstractRepositoryJob {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

	@Override
	@BabService(
		name = "RepositoryLogSummary", 
		title = "Log Summary",
		requestClass = LogSummaryJobConfiguration.class, 
		responseClass = LogSummary.class,
		legacyJobExtension = ".blsum"
	)
	public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      // JavaSparkContext sc = se.getContext();
      // FileSystem fs = se.getHdfsFileSystem();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      System.out.println("#######");
      System.out.println(json);
      System.out.println("#######");
      LogSummaryJobConfiguration config = mapper.readValue(json, LogSummaryJobConfiguration.class);
      String outputURI = se.getContextUri(res.getUri());

      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      reader.getRepository().getId();
      
      LogSummary summary = new LogSummary();
      Map<String, ICase> cases = reader.getCases();
      int totalEvents = 0;
      for (String caseId : cases.keySet()) {
        ICase icase = cases.get(caseId);
        DCase dcase = summary.getCaseTab().getCase(caseId);
        dcase.setNoOfEvents(icase.getEvents().size());
        summary.getCaseTab().increaseEventPerCase(dcase.getNoOfEvents());
        Map<String, Set<String>> attributes = new TreeMap<String, Set<String>>();
        if (dcase.getNoOfEvents() > 0) {
          IEvent start = null;
          IEvent end = null;
          Map<String, Stack<IEvent>> activeActivities = new LinkedHashMap<String, Stack<IEvent>>();
          Map<String, IEvent> lastComplete = new LinkedHashMap<String, IEvent>();
          double sumActivityDuration = 0;
          int countActivityDuration = 0;
          double sumActivityWaiting = 0;
          int countActivityWaiting = 0;
          IEvent startEvent = null;
          IEvent completeEvent = null;
          for (String eventId : icase.getEvents().keySet()) {
            IEvent current = icase.getEvents().get(eventId);
            for (String attr : current.getAttributes().keySet()) {
              Object value = current.getAttributes().get(attr);
              if (value != null) {
                if (!attributes.containsKey(attr))
                  attributes.put(attr, new HashSet<String>());
                attributes.get(attr).add(value.toString());
              }
            }
            if (startEvent == null)
              startEvent = current;
            completeEvent = current;
            summary.getTimelineTab().increaseEventOverTime(current.getTimestamp());

            totalEvents++;
            if (start == null || current.getTimestamp() < start.getTimestamp())
              start = current;
            if (end == null || current.getTimestamp() > end.getTimestamp())
              end = current;

            String activity = current.getLabel();
            State as = summary.getActivityTab().getState(activity);
            as.increaseFrequency();

            String originator = current.getOriginator();
            State os = summary.getOriginatorTab().getState(originator);
            os.increaseFrequency();

            String resource = current.getResource();
            State rs = summary.getResourceTab().getState(resource);
            rs.increaseFrequency();

            String type = current.getType();
            if (type.compareTo(IEvent.DIM_EVENT_TYPE_START) == 0) {
              if (!activeActivities.containsKey(activity))
                activeActivities.put(activity, new Stack<IEvent>());
              activeActivities.get(activity).push(current);
              if (lastComplete.containsKey(activity)) {
                double waiting = current.getTimestamp() - lastComplete.get(activity).getTimestamp();
                if (waiting < 0)
                  waiting *= -1;
                sumActivityWaiting += waiting;
                countActivityWaiting++;
              }
            } else if (type.compareTo(IEvent.DIM_EVENT_TYPE_COMPLETE) == 0
                && activeActivities.containsKey(activity)
                && activeActivities.get(activity).size() > 0) {
              IEvent astart = activeActivities.get(activity).pop();
              double duration = current.getTimestamp() - astart.getTimestamp();
              if (duration < 0)
                duration *= -1;
              as.addNewDuration(duration);
              os.addNewDuration(duration);
              rs.addNewDuration(duration);
              sumActivityDuration += duration;
              countActivityDuration++;
              lastComplete.put(activity, current);
            }

          }
          if (startEvent != null)
            summary.getTimelineTab().increaseCaseStartOverTime(startEvent.getTimestamp());
          if (completeEvent != null)
            summary.getTimelineTab().increaseCaseCompleteOverTime(completeEvent.getTimestamp());
          dcase.setStarted(start.getTimestamp());
          dcase.setFinished(end.getTimestamp());
          long caseDuration = end.getTimestamp() - start.getTimestamp();
          if (caseDuration < 0)
            caseDuration *= -1;
          summary.getCaseTab().increaseCaseDurations(caseDuration);
          dcase.setDuration(caseDuration);
          double meanActivityDuration =
              (countActivityDuration != 0) ? sumActivityDuration / countActivityDuration : 0;
          summary.getCaseTab().increaseMeanActivityDurations(meanActivityDuration);
          double meanActivityWaiting =
              (countActivityWaiting != 0) ? sumActivityWaiting / countActivityWaiting : 0;
          summary.getCaseTab().increaseMeanWaitingTimes(meanActivityWaiting);
          double caseUtilization =
              (meanActivityWaiting != 0) ? meanActivityDuration / meanActivityWaiting : 0;
          summary.getCaseTab().increaseCaseUtilizations(caseUtilization);
        }
        for (String attr : attributes.keySet()) {
          dcase.getNoOfAttributes().put(attr, String.valueOf(attributes.get(attr).size()));
        }
        for (String attr : icase.getAttributes().keySet()) {
          Object value = icase.getAttributes().get(attr);
          if (value != null) {
            dcase.getNoOfAttributes().put(attr, value.toString());
          }
        }
      }

      summary.calculateRelativeFrequency(totalEvents);
      if (config.isRepositoryTab()) {
    	  summary.setRepository((BRepository) reader.getRepository());
      }
      if (!config.isCaseTab()) {
        summary.setCaseTab(null);
      }
      if (config.isActivityTab()) {
        summary.getActivityTab().getFrequency();
        summary.getActivityTab().getMedianDuration();
        summary.getActivityTab().getMeanDuration();
        summary.getActivityTab().getDurationRange();
        summary.getActivityTab().getAggregateDuration();
      } else {
        summary.setActivityTab(null);
      }
      if (config.isOriginatorTab()) {
        summary.getOriginatorTab().getFrequency();
        summary.getOriginatorTab().getMedianDuration();
        summary.getOriginatorTab().getMeanDuration();
        summary.getOriginatorTab().getDurationRange();
        summary.getOriginatorTab().getAggregateDuration();
      } else {
        summary.setOriginatorTab(null);
      }
      if (config.isResourceTab()) {
        summary.getResourceTab().getFrequency();
        summary.getResourceTab().getMedianDuration();
        summary.getResourceTab().getMeanDuration();
        summary.getResourceTab().getDurationRange();
        summary.getResourceTab().getAggregateDuration();
      } else {
        summary.setResourceTab(null);
      }
      if (config.isTimelineTab()) {
        summary.getTimelineTab().getActiveCasesOverTime();
        int started = 0;
        for (Long t : summary.getTimelineTab().getCaseStartOverTime().keySet()) {
          started += summary.getTimelineTab().getCaseStartOverTime().get(t);
          summary.getTimelineTab().getCaseStartOverTime().put(t, started);
        }
        int completed = 0;
        for (Long t : summary.getTimelineTab().getCaseCompleteOverTime().keySet()) {
          completed += summary.getTimelineTab().getCaseCompleteOverTime().get(t);
          summary.getTimelineTab().getCaseCompleteOverTime().put(t, completed);
        }
        int event = 0;
        for (Long t : summary.getTimelineTab().getEventsOverTime().keySet()) {
          event += summary.getTimelineTab().getEventsOverTime().get(t);
          summary.getTimelineTab().getEventsOverTime().put(t, event);
        }
      } else {
        summary.setTimelineTab(null);
      }
      RawJobResult result = new RawJobResult("repository.LogSummary", outputURI, outputURI,
          mapper.writeValueAsString(summary));
      System.out.println("######################");
      System.out.println(outputURI + ".blusm");
      System.out.println("######################");

      se.getFileUtil().saveAsTextFile(se, outputURI + ".blsum", result.getResponse());
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
