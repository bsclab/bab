/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.reassign.ReassignmentAlgorithm;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.reassign.Reassignment_CM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.reassign.Reassignment_IM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.*;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import org.apache.spark.api.java.JavaPairRDD;

public class SparkReassignment extends SparkSocialNetworkAlgorithm implements Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  @Override
  public SocialNetworkAnalysis run(SocialNetworkAnalysisConfiguration config,
      SocialNetworkAnalysis analysis, IExecutor se) {

    // JavaSparkContext sc = ((ISparkExecutor) se).getContext();

    SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
    final boolean CONSIDER_MULTIPLE_TRANSFER = config.getMethod().isMultipleTransfer();

    /* <Originator Name, Occurrences Count of Originator> */
    Map<String, Double> originatorNameFrequencyPerCaseMap =
        getMapOfOriginatorNameFrequencyPerCase(reader, config.getStates());
    List<String> originatorNameList =
        new ArrayList<String>(originatorNameFrequencyPerCaseMap.keySet());
    Collections.sort(originatorNameList);

    /* Make Nodes */
    Map<String, Node> nodes = makeNodes(originatorNameList, originatorNameFrequencyPerCaseMap);
    analysis.setNodes(nodes);
    analysis
        .setOriginators(new ArrayList<String>(reader.getRepository().getOriginators().keySet()));

    /* Ready to run HandoverOfWork Algorithm */
    JavaPairRDD<String, ICase> rdd = reader.getCasesRDD();
    ReassignmentAlgorithm reassignmentAlgorithm = null;

    if (CONSIDER_MULTIPLE_TRANSFER) {
      reassignmentAlgorithm = new Reassignment_CM(originatorNameList, rdd);
      SparkSocialNetworkAlgorithm.printDebug("CM");
    } else {
      reassignmentAlgorithm = new Reassignment_IM(originatorNameList, rdd);
      SparkSocialNetworkAlgorithm.printDebug("IM");
    }

    SocialMatrix D = reassignmentAlgorithm.calculate();

    Map<String, Arc> arcs = makeArcs(D);
    analysis.setArcs(arcs);

    return analysis;
  }
}
