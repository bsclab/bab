/*
 * 
 * Copyright 筌욑옙 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;

/**
 * Process model animation using Log Replay <br>
 * <br>
 * Config class: {@link LogReplayConfiguration}<br>
 * Result class: {@link LogReplayModel}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 * @author Wahyu Andy (wanprabu@gmail.com)
 * @author Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 */
public class SparkLogReplayJob extends AbstractModelJob implements Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L; 

	@Override
	@BabService(
		name = "ModelLogReplayJob", 
		title = "Log Replay Miner",
		requestClass = LogReplayConfiguration.class, 
		responseClass = LogReplayModel.class,
		legacyJobExtension = ".lrmodel"
	)
public IJobResult run(String json, IResource res, IExecutor se) {
    try {
      // JavaSparkContext sc = se.getContext();
      // FileSystem fs = se.getHdfsFileSystem();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

      LogReplayConfiguration config = mapper.readValue(json, LogReplayConfiguration.class);
      SparkRepositoryReader reader =
          new SparkRepositoryReader(se, config.getRepositoryURI(), config);
      LogReplayModel model = new LogReplayModel();

      JavaPairRDD<String, LEvent> eventListRdd = reader.getCasesRDD().mapPartitionsToPair(
          new PairFlatMapFunction<Iterator<Tuple2<String, ICase>>, String, LEvent>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Iterable<Tuple2<String, LEvent>> call(Iterator<Tuple2<String, ICase>> arg0)
                throws Exception {
              Map<String, Tuple2<String, LEvent>> map =
                  new TreeMap<String, Tuple2<String, LEvent>>();

              int eventId = 0;
              while (arg0.hasNext()) {
                ICase cases = arg0.next()._2();
                IEvent event = null;
                int eventSize = cases.getEvents().size();
                int count = 1;

                for (IEvent e : cases.getEvents().values()) {
                  if (event == null) {
                    event = e;
                    // continue;
                  }

                  String state = "";
                  String s = event.getLabel() + " (" + event.getType() + ")";
                  String o = e.getLabel() + " (" + e.getType() + ")";
                  String a = s + "|" + o;
                  eventId++;

                  if (count == 1)
                    state = "first";
                  else if (count == eventSize)
                    state = "last";

                  String key = cases.getId() + "." + eventId + "." + e.getTimestamp();
                  if (!map.containsKey(key)
                      && event.getTimestamp().compareTo(e.getTimestamp()) != 0)
                    map.put(key, new Tuple2<String, LEvent>(key, new LEvent(cases.getId(),
                        eventSize + "", a, s, o, event.getTimestamp(), e.getTimestamp(), state)));

                  event = e;
                  count++;
                }
              }
              return map.values();
            }
          }).reduceByKey(new Function2<LEvent, LEvent, LEvent>() {
            /**
             * Default serial version ID
             */
            private static final long serialVersionUID = 1L;

            @Override
            public LEvent call(LEvent arg0, LEvent arg1) throws Exception {
              long start = arg0.getStart() < arg1.getStart() ? arg0.getStart() : arg1.getStart();
              long complete =
                  arg0.getComplete() > arg1.getComplete() ? arg0.getComplete() : arg1.getComplete();
              LEvent r = new LEvent(arg0.getCaseId(), arg0.getEventId(), arg0.getEventName(),
                  arg0.getSource(), arg0.getTarget(), start, complete, arg0.getEventState());

              return r;
            }
          });

      List<Tuple2<String, LEvent>> eventListFromRdd = eventListRdd.collect();
      List<Tuple2<String, LEvent>> eventList = new ArrayList<Tuple2<String, LEvent>>();
      eventList.addAll(eventListFromRdd);
      int eventSize = eventList.size();

      Collections.sort(eventList, new TupleComparator());

      int limit = config.getLimit();
      int partCount = 0;
      String outputURI = se.getContextUri(res.getUri());

      List<List<Tuple2<String, LEvent>>> parts = chopped(eventList, limit);
      Map<Long, Integer> eventCount = new TreeMap<Long, Integer>();
      for (List<Tuple2<String, LEvent>> e : parts) {
        long start = Long.MAX_VALUE;
        long end = Long.MIN_VALUE;
        EventListModel elModel = new EventListModel();
        for (Tuple2<String, LEvent> tuple : e) {
          LEvent le = tuple._2();
          if (le.getStart() < start)
            start = le.getStart();
          if (le.getComplete() > end)
            end = le.getComplete();

          if (!eventCount.containsKey(le.getStart())) {
            eventCount.put(le.getStart(), 1);
          } else {
            int value = eventCount.get(le.getStart()) + 1;
            eventCount.put(le.getStart(), value);
          }

          elModel.addLEvent(le);
        }
        elModel.setEventsNumber(e.size());
        elModel.setPart(partCount);
        elModel.setStart(start);
        elModel.setComplete(end);

        RawJobResult result = new RawJobResult("model.LogReplay", outputURI, outputURI,
            mapper.writeValueAsString(elModel));
        String url = outputURI + "/part" + partCount + ".lrmodel";
        // �뜝�럩�쐸�솻洹⑥삕
        // String urlPartition = res.getUri() + "/part"+partCount+".lrmodel";
        // for dongkuk
        String urlPartition =
            res.getUri() + "/" + res.getUri().split("/")[res.getUri().split("/").length - 1]
                + "/part" + partCount + ".lrmodel";

        se.getFileUtil().saveAsTextFile(se, url, result.getResponse());

        model.addPartition(new EventPartition(partCount++, urlPartition, start, end));
      }

      long outerStart = model.getEventPartitions().get(0).getStart();
      long outerEnd =
          model.getEventPartitions().get(model.getEventPartitions().size() - 1).getComplete();

      model.setStartTime(outerStart);
      model.setEndTime(outerEnd);
      model.setDuration(outerEnd - outerStart);
      model.setCasesNumber(reader.getCases().size());
      model.setEventsNumber(eventSize);
      model.setRequestURI(config.getRepositoryURI());
      // model.setEventCount(eventCount);

      // System.out.println(
      // serializer.exclude("*.class").include("eventPartitions").serialize(model));

      RawJobResult result = new RawJobResult("model.LogReplay", outputURI, outputURI,
          mapper.writeValueAsString(model));
      se.getFileUtil().saveAsTextFile(se, outputURI + ".lrmodel", result.getResponse());
      return result;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  public static <T> List<List<T>> chopped(List<T> list, final int L) {
    List<List<T>> parts = new ArrayList<List<T>>();
    final int N = list.size();
    for (int i = 0; i < N; i += L) {
      parts.add(new ArrayList<T>(list.subList(i, Math.min(N, i + L))));
    }

    int i = parts.size();
    double limit = 0.3 * L;
    if (i > 1 && parts.get(i - 1).size() <= limit) {
      parts.get(i - 2).addAll(parts.get(i - 1));
      parts.remove(i - 1);
    }

    return parts;
  }

}
