/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.ws.api.Result;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class TimeGapAnalysisJobModel extends Result implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Map of concatenation of start and end activities, and object Transitions, i.e. key= 'start
   * activity (type) | end activity (type)'
   */
  public Map<String, Transition> transitions = new HashMap<String, Transition>();

  /**
   * Map of concatenation of start and end activities, and object Nodes, i.e. key= 'start activity
   * (type) | end activity (type)'
   */
  public Map<String, Node> nodes = new HashMap<String, Node>();

  /**
   * Overall timeline
   */
  public Map<Long, Integer> timelineWhole = new TreeMap<Long, Integer>();

  /**
   * Filtered timeline
   */
  public Map<Long, Integer> timelineFiltered = new TreeMap<Long, Integer>();

  /**
   * Repository summary
   */
  public BRepository repository;

  /**
   * Minimum time gap
   */
  public int minGap;

  /**
   * Maximum time gap
   */
  public int maxGap;

  /**
   * Repository URI
   */
  public String repositoryURI;

  public Node setOrGetNode(String label) {
    Node n = nodes.get(label);
    if (n == null) {
      n = new Node();
      n.setLabel(label);
    }

    nodes.put(label, n);

    return n;
  }

  public Transition setOrGetTransition(String startActivity, String endActivity) {
    Transition t = transitions.get(startActivity + "|" + endActivity);
    if (t == null) {
      t = new Transition();
      t.setTarget(endActivity);
      t.setSource(startActivity);
    }

    transitions.put(startActivity + "|" + endActivity, t);

    setOrGetNode(startActivity);
    setOrGetNode(endActivity);

    return t;
  }

  public Map<String, Node> getNodes() {
    return nodes;
  }

  public void setNodes(Map<String, Node> nodes) {
    this.nodes = nodes;
  }

  public Map<String, Transition> getTransitions() {
    return transitions;
  }

  public void setTransitions(Map<String, Transition> transitions) {
    this.transitions = transitions;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public double getMinGap() {
    return minGap;
  }

  public void setMinGap(int minGap) {
    this.minGap = minGap;
  }

  public int getMaxGap() {
    return maxGap;
  }

  public void setMaxGap(int maxGap) {
    this.maxGap = maxGap;
  }

  public BRepository getRepository() {
    return repository;
  }

  public void setRepository(BRepository repository) {
    this.repository = repository;
  }

  public Map<Long, Integer> getTimelineWhole() {
    return timelineWhole;
  }

  public void setTimelineWhole(Map<Long, Integer> timelineWhole) {
    this.timelineWhole = timelineWhole;
  }

  public Map<Long, Integer> getTimelineFiltered() {
    return timelineFiltered;
  }

  public void setTimelineFiltered(Map<Long, Integer> timelineFiltered) {
    this.timelineFiltered = timelineFiltered;
  }
}
