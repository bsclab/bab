package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.io.Serializable;

public class Dependency implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private int related;
  private double frequency;
  private double dependency;
  private boolean accepted;

  public Dependency() {

  }

  public Dependency(int related, double frequency, double dependency, boolean accepted) {
    setRelated(related);
    setFrequency(frequency);
    setDependency(dependency);
    setAccepted(accepted);
  }

  public int getRelated() {
    return related;
  }

  public void setRelated(int related) {
    this.related = related;
  }

  public double getFrequency() {
    return frequency;
  }

  public void setFrequency(double frequency) {
    this.frequency = frequency;
  }

  public double getDependency() {
    return dependency;
  }

  public void setDependency(double dependency) {
    this.dependency = dependency;
  }

  public boolean isAccepted() {
    return accepted;
  }

  public void setAccepted(boolean accepted) {
    this.accepted = accepted;
  }

  @Override
  public String toString() {
    return getDependency() + " (" + getFrequency() + " x) => " + isAccepted();
  }
}
