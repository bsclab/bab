package kr.ac.pusan.bsclab.bab.ws.api.analysis.ar;

import java.util.Map;
import java.util.TreeMap;

public class Node {
  String name;
  double frequency;
  Node parent;
  Map<String, Node> childs;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getFrequency() {
    return frequency;
  }

  public void setFrequency(double frequency) {
    this.frequency = frequency;
  }

  public Node getParent() {
    return parent;
  }

  public void setParent(Node parent) {
    this.parent = parent;
  }

  public Map<String, Node> getChilds() {
    if (childs == null)
      childs = new TreeMap<String, Node>();
    return childs;
  }

  public boolean isLeaf() {
    return childs == null;
  }

}
