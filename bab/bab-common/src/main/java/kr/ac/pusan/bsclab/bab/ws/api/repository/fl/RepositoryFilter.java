package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.repository.IRepositoryFilter;

public abstract class RepositoryFilter<I, O> implements IRepositoryFilter<I, O>, Serializable {

  private static final long serialVersionUID = 1L;

  public static final String NAME = "TIMEFRAME";
  protected Map<String, String> options;

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public Map<String, String> getOptions() {
    if (options == null) {
      options = new LinkedHashMap<String, String>();
    }
    return options;
  }

}
