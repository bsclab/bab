/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config;

import java.io.Serializable;

public class Method implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private String algorithm = "workingTogether";
  private boolean casuality = true;
  private DirectSubContract directSubcontract = new DirectSubContract();
  private String workingTogether = "simultaneousRatio";
  private String distance = "euclidean";
  private boolean multipleTransfer = true;

  public String getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }

  public boolean isCasuality() {
    return casuality;
  }

  public void setCasuality(boolean casuality) {
    this.casuality = casuality;
  }

  public DirectSubContract getDirectSubcontract() {
    return directSubcontract;
  }

  public void setDirectSubcontract(DirectSubContract directSubcontract) {
    this.directSubcontract = directSubcontract;
  }

  public String getWorkingTogether() {
    return workingTogether;
  }

  public void setWorkingTogether(String workingTogether) {
    this.workingTogether = workingTogether;
  }

  public String getDistance() {
    return distance;
  }

  public void setDistance(String distance) {
    this.distance = distance;
  }

  public boolean isMultipleTransfer() {
    return multipleTransfer;
  }

  public void setMultipleTransfer(boolean multipleTransfer) {
    this.multipleTransfer = multipleTransfer;
  }

}
