/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;

public class LEvent implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private String caseId;
  private String eventId;
  private String eventName;
  private String source;
  private String target;
  private Long start;
  private Long complete;
  private String eventState;

  public LEvent() {

  }

  public LEvent(String cid, String eid, String enm, String src, String tar, Long st, Long comp,
      String state) {
    setCaseId(cid);
    setEventId(eid);
    setEventName(enm);
    setSource(src);
    setTarget(tar);
    setStart(st);
    setComplete(comp);
    setEventState(state);
  }

  public String getEventState() {
    return eventState;
  }

  public void setEventState(String eventState) {
    this.eventState = eventState;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getEventName() {
    return eventName;
  }

  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public Long getStart() {
    return start;
  }

  public void setStart(Long start) {
    this.start = start;
  }

  public Long getComplete() {
    return complete;
  }

  public void setComplete(Long complete) {
    this.complete = complete;
  }

  public String getSource() {
    return source;
  }

  public String getTarget() {
    return target;
  }

  public String getCaseId() {
    return caseId;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public void setCaseId(String caseId) {
    this.caseId = caseId;
  }


}
