/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@XmlRootElement(name = "element")
@XmlSeeAlso({ Task.class, Event.class, Gateway.class, SequenceFlow.class })
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
        property = "type")
@JsonSubTypes({
	    @JsonSubTypes.Type(value = Task.class, name = Task.TYPE),
	    @JsonSubTypes.Type(value = SequenceFlow.class, name = SequenceFlow.TYPE),
	    @JsonSubTypes.Type(value = Event.class, name = Event.TYPE),
	    @JsonSubTypes.Type(value = Gateway.class, name = Gateway.TYPE)
    })
public abstract class Element {

	public Element() {
		
	}
	
	public Element(String id, String name) {
		this.id = id;
		this.name = name;
	}

	private String type;

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

}
