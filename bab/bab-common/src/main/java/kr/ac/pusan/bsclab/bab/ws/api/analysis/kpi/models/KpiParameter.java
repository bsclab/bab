package kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.models;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Example result class from Test Algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class KpiParameter implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  public static final String KPI_TYPE_NUMERICAL = "NUMERICAL";
  public static final String KPI_TYPE_CAT_ORDINAL = "CATORDINAL";
  public static final String KPI_TYPE_CAT_NOMINAL = "CATNOMINAL";

  public static final String KPI_OPERATION_ADD = "ADD";
  public static final String KPI_OPERATION_SUB = "SUB";
  public static final String KPI_OPERATION_MUL = "MUL";
  public static final String KPI_OPERATION_DIV = "DIV";

  public static final String KPI_AGGREGATION_COUNT = "COUNT";
  public static final String KPI_AGGREGATION_COUNT_DISTINCT = "COUNTD";
  public static final String KPI_AGGREGATION_SUM = "SUM";
  public static final String KPI_AGGREGATION_AVERAGE = "AVERAGE";
  public static final String KPI_AGGREGATION_MIN = "MIN";
  public static final String KPI_AGGREGATION_MAX = "MAX";

  public static final String KPI_TARGET_GLOBAL = "MODEL";
  public static final String KPI_TARGET_ACTIVITY = "ACTIVITY";
  public static final String KPI_TARGET_TRANSITION = "TRANSITION";

  // Analysis group
  private String group;

  // Analysis name
  private String name;

  // Data type
  private String type;

  // Classifier field (e.g. case.id, event.id, @case.attr, @event.attr
  private Set<String> classifiers;

  // Aggregation type
  private Set<String> aggregation;

  // Analysis target
  private String target;

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Set<String> getClassifiers() {
    if (classifiers == null) {
      classifiers = new LinkedHashSet<String>();
    }
    return classifiers;
  }

  public void setClassifiers(Set<String> classifiers) {
    this.classifiers = classifiers;
  }

  public Set<String> getAggregation() {
    if (aggregation == null) {
      aggregation = new LinkedHashSet<String>();
    }
    return aggregation;
  }

  public void setAggregation(Set<String> aggregation) {
    this.aggregation = aggregation;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  @Override
  public String toString() {
    return this.getGroup() + "+" + this.getName();
  }

}
