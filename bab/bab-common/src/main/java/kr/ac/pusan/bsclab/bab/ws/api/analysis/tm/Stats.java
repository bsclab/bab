package kr.ac.pusan.bsclab.bab.ws.api.analysis.tm;

import java.io.Serializable;

public class Stats implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private double total = 0;
  private double count = 0;
  private double min = Long.MAX_VALUE;
  private double max = Long.MIN_VALUE;
  private double mean = 0;
  private long median = 0;

  public double getCount() {
    return count;
  }

  public void setCount(double count) {
    this.count = count;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public double getMin() {
    return min;
  }

  public void setMin(double min) {
    this.min = min;
  }

  public double getMax() {
    return max;
  }

  public void setMax(double max) {
    this.max = max;
  }

  public double getMean() {
    return mean;
  }

  public void setMean(double mean) {
    this.mean = mean;
  }

  public long getMedian() {
    return median;
  }

  public void setMedian(long median) {
    this.median = median;
  }
}
