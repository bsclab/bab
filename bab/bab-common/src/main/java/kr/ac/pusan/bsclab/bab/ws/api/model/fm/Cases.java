/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.fm;

public class Cases {
	private int include = 0;
	private int asStartEvent = 0;
	private int asEndEvent = 0;

	public int getInclude() {
		return include;
	}

	public void setInclude(int include) {
		this.include = include;
	}

	public int getAsStartEvent() {
		return asStartEvent;
	}

	public void setAsStartEvent(int asStartEvent) {
		this.asStartEvent = asStartEvent;
	}

	public int getAsEndEvent() {
		return asEndEvent;
	}

	public void setAsEndEvent(int asEndEvent) {
		this.asEndEvent = asEndEvent;
	}

	public void increaseInclude() {
		include++;
	}

	public void increaseAsStartEvent() {
		asStartEvent++;
	}

	public void increaseAsEndEvent() {
		asEndEvent++;
	}
}

