package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;


public class Node {

  /**
   * contains node's label
   */
  private String label;

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

}
