/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

/**
 * Configuration class for Log Summary job (detailed)
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class LogSummaryJobConfiguration extends Configuration implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Repository URI
   */
  private String repositoryURI;

  /**
   * Generate repository
   */
  private boolean repositoryTab = true;

  /**
   * Generate case summary
   */
  private boolean caseTab = false;

  /**
   * Generate timeline summary
   */
  private boolean timelineTab = false;

  /**
   * Generate activity summary
   */
  private boolean activityTab = false;

  /**
   * Generate originator summary
   */
  private boolean originatorTab = false;

  /**
   * Generate resource summary
   */
  private boolean resourceTab = false;

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

public boolean isRepositoryTab() {
	return repositoryTab;
}

public void setRepositoryTab(boolean repositoryTab) {
	this.repositoryTab = repositoryTab;
}

  public boolean isTimelineTab() {
    return timelineTab;
  }

  public void setTimelineTab(boolean timelineTab) {
    this.timelineTab = timelineTab;
  }

  public boolean isActivityTab() {
    return activityTab;
  }

  public void setActivityTab(boolean activityTab) {
    this.activityTab = activityTab;
  }

  public boolean isOriginatorTab() {
    return originatorTab;
  }

  public void setOriginatorTab(boolean originatorTab) {
    this.originatorTab = originatorTab;
  }

  public boolean isResourceTab() {
    return resourceTab;
  }

  public void setResourceTab(boolean resourceTab) {
    this.resourceTab = resourceTab;
  }

  public boolean isCaseTab() {
    return caseTab;
  }

  public void setCaseTab(boolean caseTab) {
    this.caseTab = caseTab;
  }

}
