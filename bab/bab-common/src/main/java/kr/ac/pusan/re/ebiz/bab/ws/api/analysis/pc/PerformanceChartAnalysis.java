/*
 * 
 * Copyright © 2013-2015 Park Chanho (cksgh4178@naver.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc;

import java.util.HashMap;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

import java.util.List;
import java.util.ArrayList;

public class PerformanceChartAnalysis extends Result {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String id = "Try";
  private String series;
  private Map<Integer, Event> matrix = new HashMap<Integer, Event>();
  private List<String> labels = new ArrayList<String>();


  public String getSeries() {
    return series;
  }

  public void setSeries(String series) {
    this.series = series;
  }

  public Event getevent(int index) {
    Event e = new Event();
    matrix.put(index, e);
    return e;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Map<Integer, Event> getMatrix() {
    return matrix;
  }

  public void setMatrix(Map<Integer, Event> matrix) {
    this.matrix = matrix;
  }

  public List<String> getlabels() {
    return labels;
  }

  public void setSeries(List<String> labels) {
    this.labels = labels;
  }
}
