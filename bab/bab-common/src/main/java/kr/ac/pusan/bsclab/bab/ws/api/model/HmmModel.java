/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public class HmmModel implements IHmmModel, Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private Map<String, Set<String>> outputSets = new TreeMap<String, Set<String>>();

  public void enableTransition(String from, String to) {
    if (hasTransition(from, to))
      return;
    if (!outputSets.containsKey(from))
      outputSets.put(from, new TreeSet<String>());
    outputSets.get(from).add(to);
  }

  @Override
  public boolean hasTransition(String from, String to) {
    return outputSets.containsKey(from) && outputSets.get(from).contains(to);
  }

  @Override
  public boolean canReplay(ICase icase) {
    String prev = null;
    for (IEvent e : icase.getEvents().values()) {
      String act = e.getLabel() + " (" + e.getType() + ")";
      if (prev == null) {
        prev = act;
        continue;
      }
      if (!hasTransition(prev, act))
        return false;
      prev = act;
    }
    return true;
  }

}
