/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ElementDeserializer extends JsonDeserializer<Element> {

	@Override
	public Element deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		try {
			ObjectMapper mapper = (ObjectMapper) jp.getCodec();
			Object n = mapper.readTree(jp);
			if (n instanceof ObjectNode) {
				ObjectNode node = (ObjectNode) n;
				if (jp.readValueAsTree() instanceof ObjectNode && node != null && node.get("type") != null) {
					String type = node.get("type").textValue();
					Class<? extends Element> concreteType = Task.class;
					if (type.equalsIgnoreCase(Event.TYPE)) {
						concreteType = Event.class;
					} else if (type.equalsIgnoreCase(Gateway.TYPE)) {
						concreteType = Gateway.class;
					} else if (type.equalsIgnoreCase(SequenceFlow.TYPE)) {
						concreteType = SequenceFlow.class;
					}
					Element result = mapper.readValue(node.toString(), concreteType);
					return result;
				}
			} else {
				toString();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
