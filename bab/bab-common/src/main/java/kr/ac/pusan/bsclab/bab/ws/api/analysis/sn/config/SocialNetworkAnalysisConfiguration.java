/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

/**
 * Social network analysis configuration file
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SocialNetworkAnalysisConfiguration extends Configuration implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Repository URI
   */
  private String repositoryURI = "";

  /**
   * Selected dimension for analysis
   */
  private String dimension = "event.originator";

  /**
   * Selected states
   */
  private List<String> states = new ArrayList<String>();

  /**
   * Selected method for analysis and its parameters
   */
  private Method method = new Method();

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public String getDimension() {
    return dimension;
  }

  public void setDimension(String dimension) {
    this.dimension = dimension;
  }

  public List<String> getStates() {
    return states;
  }

  public void setStates(List<String> states) {
    this.states = states;
  }

  public Method getMethod() {
    return method;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

}
