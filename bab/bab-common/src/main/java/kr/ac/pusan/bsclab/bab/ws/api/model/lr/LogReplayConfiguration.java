/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

public class LogReplayConfiguration extends Configuration implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Repository URI
   */
  private String repositoryURI;

  /**
   * Event limit per partition
   */
  private int limit = 1000;

  /**
   * Event offset (skip events)
   */
  private int offset = 0;

  public LogReplayConfiguration() {

  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

}
