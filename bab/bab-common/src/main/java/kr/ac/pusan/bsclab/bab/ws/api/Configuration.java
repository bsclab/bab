package kr.ac.pusan.bsclab.bab.ws.api;

import java.io.Serializable;

import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceRequest;

public class Configuration extends ServiceRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  private String repositoryURI = "";
  private String workspaceId = "";
  private String datasetId = "";
  private String repositoryId = "";
  private String filterId = "";

  /*/ hdfs 
  private String repositoryType = "hdfs";
  private String repositoryDbString = ""; 
  private String repositoryDbUsername = "";
  private String repositoryDbPassword = "";
  //*/
  //*/ mysql 
  private String repositoryType = "mysql";
  private String repositoryDbString = "jdbc:mysql://192.168.178.45:3306/smartfactory_test";
  private String repositoryDbUsername = "ntreef"; 
  private String repositoryDbPassword = "ntreef";
  //*/
  /*/ presto
  private String repositoryType = "presto";
  private String repositoryDbString = "jdbc:presto://premaster.dev.iochord.co.kr:8080/hive/bab";
  private String repositoryDbUsername = "test";
  private String repositoryDbPassword = "";
  //*/

  private String repositorySqlSelectLog = "";
  private String repositorySqlSelectCases = "SELECT * FROM bab_log_cases WHERE case_log_id=?";
  private String repositorySqlSelectEventPerCase =
      "SELECT * FROM bab_log_events WHERE event_case_id=?";

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public String getWorkspaceId() {
    return workspaceId;
  }

  public void setWorkspaceId(String workspaceId) {
    this.workspaceId = workspaceId;
  }

  public String getDatasetId() {
    return datasetId;
  }

  public void setDatasetId(String datasetId) {
    this.datasetId = datasetId;
  }

  public String getRepositoryId() {
    return repositoryId;
  }

  public void setRepositoryId(String repositoryId) {
    this.repositoryId = repositoryId;
  }

  public String getFilterId() {
    return filterId;
  }

  public void setFilterId(String filterId) {
    this.filterId = filterId;
  }

  public String getRepositoryType() {
    return repositoryType;
  }

  public void setRepositoryType(String repositoryType) {
    this.repositoryType = repositoryType;
  }

  public String getRepositoryDbString() {
    return repositoryDbString;
  }

  public void setRepositoryDbString(String repositoryDbString) {
    this.repositoryDbString = repositoryDbString;
  }

  public String getRepositoryDbUsername() {
    return repositoryDbUsername;
  }

  public void setRepositoryDbUsername(String repositoryDbUsername) {
    this.repositoryDbUsername = repositoryDbUsername;
  }

  public String getRepositoryDbPassword() {
    return repositoryDbPassword;
  }

  public void setRepositoryDbPassword(String repositoryDbPassword) {
    this.repositoryDbPassword = repositoryDbPassword;
  }

  public String getRepositorySqlSelectLog() {
    return repositorySqlSelectLog;
  }

  public void setRepositorySqlSelectLog(String repositorySqlSelectLog) {
    this.repositorySqlSelectLog = repositorySqlSelectLog;
  }

  public String getRepositorySqlSelectCases() {
    return repositorySqlSelectCases;
  }

  public void setRepositorySqlSelectCases(String repositorySqlSelectCases) {
    this.repositorySqlSelectCases = repositorySqlSelectCases;
  }

  public String getRepositorySqlSelectEventPerCase() {
    return repositorySqlSelectEventPerCase;
  }

  public void setRepositorySqlSelectEventPerCase(String repositorySqlSelectEventPerCase) {
    this.repositorySqlSelectEventPerCase = repositorySqlSelectEventPerCase;
  }

}
