package kr.ac.pusan.bsclab.bab.ws.api.analysis;

import java.io.Serializable;

public abstract class Configuration extends kr.ac.pusan.bsclab.bab.ws.api.Configuration
    implements Serializable {

  private static final long serialVersionUID = 1L;

}
