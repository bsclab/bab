package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

public class NodeCount implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  public static final String NULLSTR = "null (null)";
  private final String node;
  private int frequency;
  private long duration;
  private final Set<String> cases = new TreeSet<String>();

  public Set<String> getCases() {
	return cases;
}


public NodeCount() {
    node = NULLSTR;
  }

  public NodeCount(String node) {
    this.node = node;
  }

  public NodeCount(NodeCount copy) {
    this.node = copy.node;
    add(copy);
  }

  public int getFrequency() {
    return frequency;
  }

  public long getDuration() {
    return duration;
  }

  public String getNode() {
    return node;
  }

  public void increase(long duration) {
    frequency++;
    this.duration += duration;
  }

  public void add(NodeCount add) {
    this.frequency += add.getFrequency();
    this.duration += add.getDuration();
    this.cases.addAll(add.getCases());
  }

}
