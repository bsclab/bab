package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Timeline {
  private Map<Long, Integer> caseStartOverTime = new TreeMap<Long, Integer>();
  private Map<Long, Integer> caseCompleteOverTime = new TreeMap<Long, Integer>();
  private Map<Long, Integer> eventsOverTime = new TreeMap<Long, Integer>();
  private Map<Long, Integer> activeCasesOverTime;
  private Map<Long, Set<String>> activeCasesOverTimeDetail = new TreeMap<Long, Set<String>>();

  public void increaseCaseStartOverTime(long time) {
    if (!caseStartOverTime.containsKey(time))
      caseStartOverTime.put(time, 0);
    caseStartOverTime.put(time, caseStartOverTime.get(time) + 1);
  }

  public void increaseCaseCompleteOverTime(long time) {
    if (!caseCompleteOverTime.containsKey(time))
      caseCompleteOverTime.put(time, 0);
    caseCompleteOverTime.put(time, caseCompleteOverTime.get(time) + 1);
  }

  public void increaseEventOverTime(long time) {
    if (!eventsOverTime.containsKey(time))
      eventsOverTime.put(time, 0);
    eventsOverTime.put(time, eventsOverTime.get(time) + 1);
  }

  public void increaseActiveCasesOverTime(long time, String caseId) {
    if (!activeCasesOverTimeDetail.containsKey(time))
      activeCasesOverTimeDetail.put(time, new LinkedHashSet<String>());
    activeCasesOverTimeDetail.get(time).add(caseId);
  }

  public Map<Long, Integer> getEventsOverTime() {
    return eventsOverTime;
  }

  public void setEventsOverTime(Map<Long, Integer> eventsOverTime) {
    this.eventsOverTime = eventsOverTime;
  }

  public Map<Long, Integer> getActiveCasesOverTime() {
    if (activeCasesOverTime != null)
      return activeCasesOverTime;
    activeCasesOverTime = new TreeMap<Long, Integer>();
    Map<Long, Integer> times = new TreeMap<Long, Integer>();
    for (Long l : caseStartOverTime.keySet())
      times.put(l, caseStartOverTime.get(l));
    for (Long l : caseCompleteOverTime.keySet())
      times.put(l, (times.containsKey(l) ? times.get(l) : 0) - caseCompleteOverTime.get(l));
    int activeCases = 0;
    for (Long l : times.keySet()) {
      activeCases += times.get(l);
      activeCasesOverTime.put(l, activeCases);
    }
    // for (Long l : activeCasesOverTimeDetail.keySet()) {
    // activeCasesOverTime.put(l, activeCasesOverTimeDetail.get(l)
    // .size());
    // }
    return activeCasesOverTime;
  }

  public Map<Long, Set<String>> getActiveCasesOverTimeDetail() {
    return activeCasesOverTimeDetail;
  }

  public void setActiveCasesOverTime(Map<Long, Integer> activeCasesOverTime) {
    this.activeCasesOverTime = activeCasesOverTime;
  }

  public void setActiveCasesOverTimeDetail(Map<Long, Set<String>> activeCasesOverTimeDetail) {
    this.activeCasesOverTimeDetail = activeCasesOverTimeDetail;
  }

  public Map<Long, Integer> getCaseStartOverTime() {
    return caseStartOverTime;
  }

  public void setCaseStartOverTime(Map<Long, Integer> caseStartOverTime) {
    this.caseStartOverTime = caseStartOverTime;
  }

  public Map<Long, Integer> getCaseCompleteOverTime() {
    return caseCompleteOverTime;
  }

  public void setCaseCompleteOverTime(Map<Long, Integer> caseCompleteOverTime) {
    this.caseCompleteOverTime = caseCompleteOverTime;
  }

}
