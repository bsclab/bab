package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

public class ArcCount implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  public static final String NULLSTR = "null (null)";
  private final String from;
  private final String to;
  private int frequency;
  private long duration;
  private double dependency;
  private final Set<String> cases = new TreeSet<String>();

  public Set<String> getCases() {
	return cases;
}

  public ArcCount() {
    from = NULLSTR;
    to = NULLSTR;
  }

  public double getDependency() {
    return dependency;
  }

  public ArcCount(String from, String to) {
    this.from = from;
    this.to = to;
  }

  public ArcCount(ArcCount copy) {
    this.from = copy.getFrom();
    this.to = copy.getTo();
    add(copy);
  }

  public int getFrequency() {
    return frequency;
  }

  public long getDuration() {
    return duration;
  }

  public String getFrom() {
    return from;
  }

  public String getTo() {
    return to;
  }

  public boolean isStartNode() {
    return from.equalsIgnoreCase(NULLSTR);
  }

  public boolean isEndNode() {
    return to.equalsIgnoreCase(NULLSTR);
  }

  public boolean isSelfLoop() {
    return from.equalsIgnoreCase(to);
  }

  public void increase(long duration) {
    frequency++;
    this.duration += duration;
  }

  public void add(ArcCount add) {
    this.frequency += add.getFrequency();
    this.duration += add.getDuration();
    this.cases.addAll(add.getCases());
  }

  public void setDependency(double dependency) {
    this.dependency = dependency;
  }
}
