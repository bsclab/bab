package kr.ac.pusan.bsclab.bab.ws.api.analysis.dc;

import java.io.Serializable;

public class Dot implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private long x;
  private String y;
  private String name;
  private String type;

  public long getX() {
    return x;
  }

  public void setX(long x) {
    this.x = x;
  }

  public String getY() {
    return y;
  }

  public void setY(String y) {
    this.y = y;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}

