/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

public abstract class TimeGapAnalysisJob1 extends ATimeGapAnalysisJob {

  /**
   * Returns an JSON object as an output for time gap model with no input parameters containing
   * arcs, nodes, and time gaps between arcs.
   *
   * @param json an absolute URL giving the base location of the image
   * @param res the location of the image, relative to the url argument
   * @param se the spark executor configuration
   * @return JSON object
   */
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

}
