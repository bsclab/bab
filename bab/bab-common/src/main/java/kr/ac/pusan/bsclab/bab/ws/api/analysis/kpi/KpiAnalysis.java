package kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi;

import kr.ac.pusan.bsclab.bab.ws.api.AbstractJob;

/**
 * Example job class for Algorithm Test
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public abstract class KpiAnalysis extends AbstractJob {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
}
