/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Heatmap model as a result of task matrix
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class HeatMapModel extends Result implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Row order
   */
  private List<Integer> hcrow = new ArrayList<Integer>();

  /**
   * Column order
   */
  private List<Integer> hccol = new ArrayList<Integer>();

  /**
   * Row label
   */
  private List<String> rowLabel = new ArrayList<String>();

  /**
   * Column label
   */
  private List<String> colLabel = new ArrayList<String>();

  /**
   * Heatmap data
   */
  private List<Link> heatList = new ArrayList<Link>();

  /**
   * Statistics
   */
  private Stats statistics = new Stats();

  /**
   * Analyzed unit
   */
  private String unit;

  /**
   * Case URL
   */
  private String casesUrl;

  public String getCasesUrl() {
    return casesUrl;
  }

  public void setCasesUrl(String casesUrl) {
    this.casesUrl = casesUrl;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public Stats getStatistics() {
    return statistics;
  }

  public void setStatistics(Stats statistics) {
    this.statistics = statistics;
  }

  public List<Link> getHeatList() {
    return heatList;
  }

  public void setHeatList(List<Link> heatList) {
    this.heatList = heatList;
  }

  public List<Integer> getHcrow() {
    return hcrow;
  }

  public void setHcrow(List<Integer> hcrow) {
    this.hcrow = hcrow;
  }

  public List<Integer> getHccol() {
    return hccol;
  }

  public void setHccol(List<Integer> hccol) {
    this.hccol = hccol;
  }

  public List<String> getRowLabel() {
    return rowLabel;
  }

  public void setRowLabel(List<String> rowLabel) {
    this.rowLabel = rowLabel;
  }

  public List<String> getColLabel() {
    return colLabel;
  }

  public void setColLabel(List<String> colLabel) {
    this.colLabel = colLabel;
  }

  public HeatMapModel addHcrow(Integer e) {
    hcrow.addAll(Arrays.asList(e));
    return this;
  }

  public HeatMapModel addHccol(Integer e) {
    hccol.addAll(Arrays.asList(e));
    return this;
  }

  public HeatMapModel addRowLabel(String e) {
    rowLabel.addAll(Arrays.asList(e));
    return this;
  }

  public HeatMapModel addColLabel(String e) {
    colLabel.addAll(Arrays.asList(e));
    return this;
  }

  public HeatMapModel addHeatList(Link e) {
    heatList.addAll(Arrays.asList(e));
    return this;
  }

  public class Stats implements Serializable {
    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;
    private double total = 0;
    private double count = 0;
    private double min = Long.MAX_VALUE;
    private double max = Long.MIN_VALUE;
    private double mean = 0;
    private long median = 0;

    public double getCount() {
      return count;
    }

    public void setCount(double count) {
      this.count = count;
    }

    public double getTotal() {
      return total;
    }

    public void setTotal(double total) {
      this.total = total;
    }

    public double getMin() {
      return min;
    }

    public void setMin(double min) {
      this.min = min;
    }

    public double getMax() {
      return max;
    }

    public void setMax(double max) {
      this.max = max;
    }

    public double getMean() {
      return mean;
    }

    public void setMean(double mean) {
      this.mean = mean;
    }

    public long getMedian() {
      return median;
    }

    public void setMedian(long median) {
      this.median = median;
    }
  }
}
