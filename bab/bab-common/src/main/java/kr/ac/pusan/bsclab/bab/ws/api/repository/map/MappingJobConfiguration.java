/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.map;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

/**
 * Mapping job configuration file
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class MappingJobConfiguration extends Configuration implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Repository name
   */
  private String name;

  /**
   * Repository description
   */
  private String description;

  /**
   * Mapping parameters
   */
  private Map<String, Map<String, String>> mapping = new TreeMap<String, Map<String, String>>();

  /**
   * Mapping filters
   */
  private Map<String, Map<String, String>> filter = new TreeMap<String, Map<String, String>>();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Map<String, Map<String, String>> getMapping() {
    return mapping;
  }

  public void setMapping(Map<String, Map<String, String>> mapping) {
    this.mapping = mapping;
  }

  public Map<String, Map<String, String>> getFilter() {
    return filter;
  }

  public void setFilter(Map<String, Map<String, String>> filter) {
    this.filter = filter;
  }

}
