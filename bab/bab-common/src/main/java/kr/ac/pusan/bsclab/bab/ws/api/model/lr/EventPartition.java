/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;

public class EventPartition implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private int part;
  private String url;
  private Long start;
  private Long complete;

  public EventPartition() {

  }

  public EventPartition(int part, String url, Long st, Long comp) {
    setPart(part);
    setUrl(url);
    setStart(st);
    setComplete(comp);
  }

  public int getPart() {
    return part;
  }

  public void setPart(int part) {
    this.part = part;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getStart() {
    return start;
  }

  public void setStart(Long start) {
    this.start = start;
  }

  public Long getComplete() {
    return complete;
  }

  public void setComplete(Long complete) {
    this.complete = complete;
  }
}
