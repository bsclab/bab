/*
 * 
 * Copyright © 2013-2015 Park Chanho (cksgh4178@naver.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc;

import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

public class PerformanceChartAnalysisJobConfiguration extends Configuration {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String series = "Task";
  private String y = "Sum";
  private String z = "Weekly";
  private String unit = "seconds";
  private String calculation = "Working Time";
  private String repositoryURI;

  public String getSeries() {
    return series;
  }

  public void setSeries(String series) {
    this.series = series;
  }

  public String getY() {
    return y;
  }

  public void setY(String y) {
    this.y = y;
  }

  public String getZ() {
    return z;
  }

  public void setZ(String z) {
    this.z = z;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getCalculation() {
    return calculation;
  }

  public void setCalculation(String calculation) {
    this.calculation = calculation;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

}
