/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

/**
 * Configuration class for association rule algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class AssociationRuleMinerConfiguration extends Configuration implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Repository URI
   */
  private String repositoryURI;

  /**
   * Selected algorithm
   */
  private String algorithm = "apriori";

  /**
   * Association rule threshold
   */
  private Threshold threshold = new Threshold();

  /**
   * No of rules
   */
  private int noOfRules = 10;

  /**
   * No of relation
   */
  private int noOfRelation = 0;

  /**
   * Rule profiles
   */
  private List<String> profiles = new ArrayList<String>();

  public String getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }

  public List<String> getProfiles() {
    return profiles;
  }

  public void setProfiles(List<String> profiles) {
    this.profiles = profiles;
  }

  public Threshold getThreshold() {
    return threshold;
  }

  public void setThreshold(Threshold threshold) {
    this.threshold = threshold;
  }

  public int getNoOfRules() {
    return noOfRules;
  }

  public void setNoOfRules(int noOfRules) {
    this.noOfRules = noOfRules;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public int getNoOfRelation() {
    return noOfRelation;
  }

  public void setNoOfRelation(int noOfRelation) {
    this.noOfRelation = noOfRelation;
  }
}
