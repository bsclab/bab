/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.ar;

import java.util.*;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Associatio rule miner model as a result of association rule miner algorithm On next version it
 * will be renamed as AssociationRuleModel
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class AssociationRuleMiner extends Result {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/**
   * Mined association rules and its properties (support, confidence)
   */
  private Map<String, Rules> associationRules = new TreeMap<String, Rules>();

  /**
   * Total transactions
   */
  private long totalTransactions = 0;

  /**
   * No of relations
   */
  private int noOfRelation = 0;

  public Map<String, Rules> getAssociationRules() {
    return associationRules;
  }

  public void setAssociationRules(Map<String, Rules> associationRules) {
    this.associationRules = associationRules;
  }

  public long getTotalTransactions() {
    return totalTransactions;
  }

  public void setTotalTransactions(long totalTransactions) {
    this.totalTransactions = totalTransactions;
  }

  public int getNoOfRelation() {
    return noOfRelation;
  }

  public void setNoOfRelation(int noOfRelation) {
    this.noOfRelation = noOfRelation;
  }

}
