package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;

public class TEvent implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private String name;
  private long start;
  private long end;
  private int count = 0;

  public TEvent(String name, long start, long end) {
    this.name = name;
    this.start = start;
    this.end = end;
  }

  public int getCount() {
    return count;
  }

  public String getName() {
    return name;
  }

  public long getStart() {
    return start;
  }

  public long getEnd() {
    return end;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStart(long start) {
    this.start = start;
  }

  public void setEnd(long end) {
    this.end = end;
  }

  public void doAverage() {
    if (this.count != 0) {
      this.start /= this.count;
      this.end /= this.count;
    }
  }

}
