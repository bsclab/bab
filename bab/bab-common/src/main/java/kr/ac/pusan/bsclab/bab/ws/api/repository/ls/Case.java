package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.LinkedHashMap;
import java.util.Map;

public class Case {
  private Map<String, DCase> cases = new LinkedHashMap<String, DCase>();
  private Map<Integer, Integer> eventPerCase = new LinkedHashMap<Integer, Integer>();
  private Map<Long, Integer> caseDurations = new LinkedHashMap<Long, Integer>();

  private Map<Double, Integer> meanActivityDurations = new LinkedHashMap<Double, Integer>();

  private Map<Double, Integer> meanWaitingTimes = new LinkedHashMap<Double, Integer>();
  private Map<Double, Integer> caseUtilizations = new LinkedHashMap<Double, Integer>();

  public DCase getCase(String caseId) {
    if (cases.containsKey(caseId)) {
      return cases.get(caseId);
    }
    DCase nc = new DCase();
    nc.setCaseId(caseId);
    cases.put(caseId, nc);
    return nc;
  }

  public void increaseEventPerCase(int noOfEvents) {
    if (!eventPerCase.containsKey(noOfEvents))
      eventPerCase.put(noOfEvents, 0);
    eventPerCase.put(noOfEvents, eventPerCase.get(noOfEvents) + 1);
  }

  public void increaseCaseDurations(long caseDuration) {
    if (!caseDurations.containsKey(caseDuration))
      caseDurations.put(caseDuration, 0);
    caseDurations.put(caseDuration, caseDurations.get(caseDuration) + 1);
  }

  public void increaseCaseUtilizations(double caseUtilization) {
    if (!caseUtilizations.containsKey(caseUtilization))
      caseUtilizations.put(caseUtilization, 0);
    caseUtilizations.put(caseUtilization, caseUtilizations.get(caseUtilization) + 1);
  }

  public void increaseMeanActivityDurations(double meanActivityDuration) {
    if (!meanActivityDurations.containsKey(meanActivityDuration))
      meanActivityDurations.put(meanActivityDuration, 0);
    meanActivityDurations.put(meanActivityDuration,
        meanActivityDurations.get(meanActivityDuration) + 1);
  }

  public void increaseMeanWaitingTimes(double meanWaitingTime) {
    if (!meanWaitingTimes.containsKey(meanWaitingTime))
      meanWaitingTimes.put(meanWaitingTime, 0);
    meanWaitingTimes.put(meanWaitingTime, meanWaitingTimes.get(meanWaitingTime) + 1);
  }

  public Map<String, DCase> getCases() {
    return cases;
  }

  public void setCases(Map<String, DCase> cases) {
    this.cases = cases;
  }

  public Map<Integer, Integer> getEventPerCase() {
    return eventPerCase;
  }

  public void setEventPerCase(Map<Integer, Integer> eventPerCase) {
    this.eventPerCase = eventPerCase;
  }

  public Map<Long, Integer> getCaseDurations() {
    return caseDurations;
  }

  public void setCaseDurations(Map<Long, Integer> caseDurations) {
    this.caseDurations = caseDurations;
  }

  public Map<Double, Integer> getCaseUtilizations() {
    return caseUtilizations;
  }

  public void setCaseUtilizations(Map<Double, Integer> caseUtilizations) {
    this.caseUtilizations = caseUtilizations;
  }

  public Map<Double, Integer> getMeanActivityDurations() {
    return meanActivityDurations;
  }

  public void setMeanActivityDurations(Map<Double, Integer> meanActivityDurations) {
    this.meanActivityDurations = meanActivityDurations;
  }

  public Map<Double, Integer> getMeanWaitingTimes() {
    return meanWaitingTimes;
  }

  public void setMeanWaitingTimes(Map<Double, Integer> meanWaitingTimes) {
    this.meanWaitingTimes = meanWaitingTimes;
  }
}
