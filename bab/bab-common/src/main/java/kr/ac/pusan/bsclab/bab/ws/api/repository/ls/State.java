package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;

public class State {
  private String label;
  private int frequency;
  private double relativeFrequency;

  private double sumDuration;
  private double meanDuration;

  private double minDuration;
  private double maxDuration;
  private double durationRange;

  public void increaseFrequency() {
    frequency++;
  }

  public void addNewDuration(double duration) {
    sumDuration += duration;
    if (frequency > 0)
      meanDuration = sumDuration / frequency;
    if (duration < minDuration)
      minDuration = duration;
    if (duration > maxDuration)
      maxDuration = duration;
    durationRange = maxDuration - minDuration;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public int getFrequency() {
    return frequency;
  }

  public void setFrequency(int frequency) {
    this.frequency = frequency;
  }

  public double getRelativeFrequency() {
    return relativeFrequency;
  }

  public void setRelativeFrequency(double relativeFrequency) {
    this.relativeFrequency = relativeFrequency;
  }

  public double getSumDuration() {
    return sumDuration;
  }

  public void setSumDuration(double sumDuration) {
    this.sumDuration = sumDuration;
  }

  public double getMeanDuration() {
    return meanDuration;
  }

  public void setMeanDuration(double meanDuration) {
    this.meanDuration = meanDuration;
  }

  public double getMinDuration() {
    return minDuration;
  }

  public void setMinDuration(double minDuration) {
    this.minDuration = minDuration;
  }

  public double getMaxDuration() {
    return maxDuration;
  }

  public void setMaxDuration(double maxDuration) {
    this.maxDuration = maxDuration;
  }

  public double getDurationRange() {
    return durationRange;
  }

  public void setDurationRange(double durationRange) {
    this.durationRange = durationRange;
  }

  public String getSumDurationStr() {
    return DateUtil.getInstance().toDurationString((long) sumDuration);
  }

  public String getMeanDurationStr() {
    return DateUtil.getInstance().toDurationString((long) meanDuration);
  }

  public String getMinDurationStr() {
    return DateUtil.getInstance().toDurationString((long) minDuration);
  }

  public String getMaxDurationStr() {
    return DateUtil.getInstance().toDurationString((long) maxDuration);
  }

  public String getDurationRangeStr() {
    return DateUtil.getInstance().toDurationString((long) durationRange);
  }
}
