/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.fl.models;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

/**
 * 
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class Filter implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  protected String name = "Filter";
  protected Map<String, String> args;
  Map<String, List<String>> params;

  public String getName() {
    return name;
  }

  public Map<String, String> getArgs() {
    return args;
  }

  public void setArgs(Map<String, String> args) {
    this.args = args;
  }

  public void setParameters(Map<String, List<String>> params) {
    if (this.params == null) {
      this.params = new LinkedHashMap<String, List<String>>();
    }
    this.params.putAll(params);
  }

  public Map<String, List<String>> getParameters() {
    if (this.params == null) {
      this.params = new LinkedHashMap<String, List<String>>();
    }
    return this.params;
  }

  public Iterable<String> getParameter(String name) {
    return (getParameters().containsKey(name) ? getParameters().get(name) : null);
  }

  public boolean isValid() {
    return true;
  }

  public Iterable<Object> filterObjects(Iterable<Object> data) {
    return data;
  }

  public Iterable<IRepository> filterRepository(Iterable<IRepository> repository) {
    return repository;
  }

  public Iterable<ICase> filterCases(Iterable<ICase> cases) {
    return cases;
  }

  public Iterable<IEvent> filterEvents(Iterable<IEvent> events) {
    return events;
  }
}
