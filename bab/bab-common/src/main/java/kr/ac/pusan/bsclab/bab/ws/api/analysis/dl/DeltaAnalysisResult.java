/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.util.Map;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Delta analysis result
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class DeltaAnalysisResult extends Result {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Corresponding configuration
	 */
	private DeltaAnalysisJobConfiguration config;

	/**
	 * Expected process model with difference
	 */
	private Model asisModel = new Model();

	/**
	 * Observed process model with difference
	 */
	private Model tobeModel = new Model();

	/**
	 * Chi square P value
	 */
	private double chiPValue;

	/**
	 * Chi square degree of freedom
	 */
	private int chiDegreeOfFreedom;

	/**
	 * Chi square statistic
	 */
	private double chiStatistic;

	/**
	 * Chi square null hypothesis
	 */
	private String chiNullHypothesis;

	/**
	 * Overall trace TPI
	 */
	private Double overallTraceTPI;

	/**
	 * Overall trace speedup
	 */
	private Double overallTraceSpeedup;

	/**
	 * Overall activity TPI
	 */
	private Double overallActivityTPI;

	/**
	 * Overall activity speedup
	 */
	private Double overallActivitySpeedup;

	private Map<String, Double> traceTPIs;
	private Map<String, Double> activityTPIs;

	public DeltaAnalysisResult() {

	}

	public DeltaAnalysisResult(DeltaAnalysisJobConfiguration config) {
		this.config = config;
	}

	public DeltaAnalysisJobConfiguration getConfig() {
		return config;
	}

	public void setConfig(DeltaAnalysisJobConfiguration config) {
		this.config = config;
	}

	public Model getAsisModel() {
		return asisModel;
	}

	public void setAsisModel(Model asisModel) {
		this.asisModel = asisModel;
	}

	public Model getTobeModel() {
		return tobeModel;
	}

	public void setTobeModel(Model tobeModel) {
		this.tobeModel = tobeModel;
	}

	public double getChiPValue() {
		return chiPValue;
	}

	public int getChiDegreeOfFreedom() {
		return chiDegreeOfFreedom;
	}

	public double getChiStatistic() {
		return chiStatistic;
	}

	public String getChiNullHypothesis() {
		return chiNullHypothesis;
	}

	public void setChiPValue(double chiPValue) {
		this.chiPValue = chiPValue;
	}

	public void setChiDegreeOfFreedom(int chiDegreeOfFreedom) {
		this.chiDegreeOfFreedom = chiDegreeOfFreedom;
	}

	public void setChiStatistic(double chiStatistic) {
		this.chiStatistic = chiStatistic;
	}

	public void setChiNullHypothesis(String chiNullHypothesis) {
		this.chiNullHypothesis = chiNullHypothesis;
	}

	public Double getOverallTraceTPI() {
		return overallTraceTPI;
	}

	public void setOverallTraceTPI(Double overallTraceTPI) {
		this.overallTraceTPI = overallTraceTPI;
	}

	public Double getOverallTraceSpeedup() {
		return overallTraceSpeedup;
	}

	public void setOverallTraceSpeedup(Double overallTraceSpeedup) {
		this.overallTraceSpeedup = overallTraceSpeedup;
	}

	public Double getOverallActivityTPI() {
		return overallActivityTPI;
	}

	public void setOverallActivityTPI(Double overallActivityTPI) {
		this.overallActivityTPI = overallActivityTPI;
	}

	public Double getOverallActivitySpeedup() {
		return overallActivitySpeedup;
	}

	public void setOverallActivitySpeedup(Double overallActivitySpeedup) {
		this.overallActivitySpeedup = overallActivitySpeedup;
	}

	public Map<String, Double> getTraceTPIs() {
		return traceTPIs;
	}

	public void setTraceTPIs(Map<String, Double> traceTPIs) {
		this.traceTPIs = traceTPIs;
	}

	public Map<String, Double> getActivityTPIs() {
		return activityTPIs;
	}

	public void setActivityTPIs(Map<String, Double> activityTPIs) {
		this.activityTPIs = activityTPIs;
	}
}
