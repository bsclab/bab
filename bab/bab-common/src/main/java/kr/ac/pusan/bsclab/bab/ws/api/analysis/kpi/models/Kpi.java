package kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.models;

import java.io.Serializable;

/**
 * Example result class from Test Algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class Kpi implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

}
