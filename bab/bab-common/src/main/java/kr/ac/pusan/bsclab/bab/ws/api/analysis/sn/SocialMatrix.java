/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SocialMatrix implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  final private int row;
  final private int col;
  private double mat[][];
  private Map<String, Integer> map;

  public SocialMatrix(int row, int col, double initVal) {
    this.row = row;
    this.col = col;
    this.mat = new double[row][col];
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < col; j++) {
        mat[i][j] = initVal;
      }
    }
  }

  public SocialMatrix(int row, int col, double initVal, List<String> orginators) {
    this(row, col, initVal);
    map = new TreeMap<String, Integer>();
    int cnt = 0;
    // System.err.println("Number and Originator");
    for (String originator : orginators) {
      // System.err.print(originator + " is " + cnt + ", ");
      map.put(originator, cnt++);
    }
  }

  public int originatorSize() {
    return map.size();
  }

  public int findIndexBy(String originator) {
    return map.get(originator);
  }

  public String findOriginatorBy(int idx) {
    for (String org : map.keySet()) {
      if (idx-- == 0)
        return org;
    }
    return null;
  }

  public int getRow() {
    return row;
  }

  public int getCol() {
    return col;
  }

  public double get(int row, int col) {
    return mat[row][col];
  }

  public void set(int row, int col, double val) {
    mat[row][col] = val;
  }

  public void set(String org1, String org2, double val) {
    int i = findIndexBy(org1);
    int j = findIndexBy(org2);
    set(i, j, val);
  }

  public void print() {
    for (String org : map.keySet())
      System.err.print(org + " ");
    System.err.println();

    for (int i = 0; i < row; i++) {
      System.err.print(findOriginatorBy(i) + " ");
      for (int j = 0; j < col; j++) {
        System.err.format("%.4f ", mat[i][j]);
      }
      System.err.println();
    }
  }

  public void printOriginators() {
    System.err.println("Originator");
    for (String org : map.keySet()) {
      System.err.print(org + " ");
    }
    System.err.println();
  }
}
