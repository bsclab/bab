package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.util.LinkedHashMap;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.IdleTimeNetwork;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class Model {
  private int noOfCases;
  private int noOfEvents;
  private int noOfNodes;
  private int noOfArcs;

  private double fitness;
  private double crossFitness;
  private double averageExecutionTime;

  private int noOfSerialBlock;
  private int noOfParalelBlock;
  private double serialization;
  private double paralelization;

  private Map<String, Integer> nodeDifferences = new LinkedHashMap<String, Integer>();
  private Map<String, Integer> arcDifferences = new LinkedHashMap<String, Integer>();

  private HeuristicModel model;
  private IdleTimeNetwork idnNetwork;
  private BRepository repository;

  public BRepository getRepository() {
    return repository;
  }

  public void setRepository(BRepository repository) {
    this.repository = repository;
  }

  public int getNoOfCases() {
    return noOfCases;
  }

  public void setNoOfCases(int noOfCases) {
    this.noOfCases = noOfCases;
  }

  public int getNoOfEvents() {
    return noOfEvents;
  }

  public void setNoOfEvents(int noOfEvents) {
    this.noOfEvents = noOfEvents;
  }

  public int getNoOfNodes() {
    return noOfNodes;
  }

  public void setNoOfNodes(int noOfNodes) {
    this.noOfNodes = noOfNodes;
  }

  public int getNoOfArcs() {
    return noOfArcs;
  }

  public void setNoOfArcs(int noOfArcs) {
    this.noOfArcs = noOfArcs;
  }

  public double getFitness() {
    return fitness;
  }

  public void setFitness(double fitness) {
    this.fitness = fitness;
  }

  public double getCrossFitness() {
    return crossFitness;
  }

  public void setCrossFitness(double crossFitness) {
    this.crossFitness = crossFitness;
  }

  public double getAverageExecutionTime() {
    return averageExecutionTime;
  }

  public void setAverageExecutionTime(double averageExecutionTime) {
    this.averageExecutionTime = averageExecutionTime;
  }

  public HeuristicModel getModel() {
    return model;
  }

  public void setModel(HeuristicModel model) {
    this.model = model;
  }

  public Map<String, Integer> getNodeDifferences() {
    return nodeDifferences;
  }

  public Map<String, Integer> getArcDifferences() {
    return arcDifferences;
  }

  public void setNodeDifferences(Map<String, Integer> nodeDifferences) {
    this.nodeDifferences = nodeDifferences;
  }

  public void setArcDifferences(Map<String, Integer> arcDifferences) {
    this.arcDifferences = arcDifferences;
  }

  public int getNoOfSerialBlock() {
    return noOfSerialBlock;
  }

  public int getNoOfParalelBlock() {
    return noOfParalelBlock;
  }

  public double getSerialization() {
    return serialization;
  }

  public double getParalelization() {
    return paralelization;
  }

  public void setNoOfSerialBlock(int noOfSerialBlock) {
    this.noOfSerialBlock = noOfSerialBlock;
  }

  public void setNoOfParalelBlock(int noOfParalelBlock) {
    this.noOfParalelBlock = noOfParalelBlock;
  }

  public void setSerialization(double serialization) {
    this.serialization = serialization;
  }

  public void setParalelization(double paralelization) {
    this.paralelization = paralelization;
  }

  public IdleTimeNetwork getIdnNetwork() {
    return idnNetwork;
  }

  public void setIdnNetwork(IdleTimeNetwork idnNetwork) {
    this.idnNetwork = idnNetwork;
  }

}
