package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

public class Cases {
  private int include = 0;
  private int asStartEvent = 0;
  private int asEndEvent = 0;

  public int getInclude() {
    return include;
  }

  public void setInclude(int include) {
    this.include = include;
  }

  public int getAsStartEvent() {
    return asStartEvent;
  }

  public void setAsStartEvent(int asStartEvent) {
    this.asStartEvent = asStartEvent;
  }

  public int getAsEndEvent() {
    return asEndEvent;
  }

  public void setAsEndEvent(int asEndEvent) {
    this.asEndEvent = asEndEvent;
  }

  public void increaseInclude() {
    include++;
  }

  public void increaseAsStartEvent() {
    asStartEvent++;
  }

  public void increaseAsEndEvent() {
    asEndEvent++;
  }
}
