/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tm;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public class TaskMatrixConfiguration extends Configuration implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  public static final String FREQUENCY = "frequency";
  public static final String DURATION = "duration";

  /**
   * Repository URI
   */
  private String repositoryURI;

  /**
   * Row dimension
   */
  private String row = IEvent.DIM_EVENT_ACTIVITY_TYPE;

  /**
   * Column dimension
   */
  private String column = IEvent.DIM_EVENT_ACTIVITY_TYPE;// DIM_EVENT_ORIGINATOR;

  /**
   * Unit analyzed
   */
  private String unit = TaskMatrixConfiguration.FREQUENCY;

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public String getRow() {
    return row;
  }

  public void setRow(String row) {
    this.row = row;
  }

  public String getColumn() {
    return column;
  }

  public void setColumn(String column) {
    this.column = column;
  }

}
