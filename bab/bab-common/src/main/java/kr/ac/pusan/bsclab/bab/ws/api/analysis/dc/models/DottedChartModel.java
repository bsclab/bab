/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Dotted chart model as a result of dotted chart algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class DottedChartModel extends Result implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  /**
   * Time dimension and its frequency
   */
  private Map<Long, Integer> times = new TreeMap<Long, Integer>();

  /**
   * Legend dimension and its frequency
   */
  private Map<String, Integer> legend = new LinkedHashMap<String, Integer>();

  /**
   * Color dimension and its frequency
   */
  private Map<String, Integer> colors = new LinkedHashMap<String, Integer>();

  /**
   * Dotted chart data, its time position and data properties
   */
  private Map<String, Map<Long, Data>> data = new TreeMap<String, Map<Long, Data>>();

  public DottedChartModel() {

  }

  public Map<String, Map<Long, Data>> getData() {
    return data;
  }

  public void setData(Map<String, Map<Long, Data>> data) {
    this.data = data;
  }

  public Map<Long, Integer> getTimes() {
    return times;
  }

  public void setTimes(Map<Long, Integer> times) {
    this.times = times;
  }

  public Map<String, Integer> getLegend() {
    return legend;
  }

  public void setLegend(Map<String, Integer> legend) {
    this.legend = legend;
  }

  public Map<String, Integer> getColors() {
    return colors;
  }

  public void setColors(Map<String, Integer> colors) {
    this.colors = colors;
  }

}
