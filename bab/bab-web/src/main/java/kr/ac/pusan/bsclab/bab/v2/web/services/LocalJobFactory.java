package kr.ac.pusan.bsclab.bab.v2.web.services;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;

@Service
@ConditionalOnProperty(name = "bab.serviceJobFactory", havingValue = "local", matchIfMissing = true)
public class LocalJobFactory implements IJobFactory {

  @Override
  public Job create(ServiceEndpoint endpoint, IServiceRequest request) {
    return new Job();
  }

}
