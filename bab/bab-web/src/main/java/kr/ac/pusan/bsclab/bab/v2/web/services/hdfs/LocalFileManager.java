package kr.ac.pusan.bsclab.bab.v2.web.services.hdfs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "bab.serviceFileManager", havingValue = "local", matchIfMissing = true)
public class LocalFileManager implements IFileManager {

	@Override
	public boolean isExists(String uri) {
		File file = new File(uri);
		return file.exists();
	}

	@Override
	public void saveAsTextFile(String uri, String data) {
		try {
			File file = new File(uri);
			File parentDir = file.getParentFile();
			if (parentDir != null && !parentDir.exists()) {
				parentDir.mkdirs();
			}
			FileWriter fw = new FileWriter(uri);
			BufferedWriter out = new BufferedWriter(fw);
			out.write(data);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String loadFromTextFile(String uri) {
		try {
			FileReader fileReader = new FileReader(uri);
			BufferedReader br = new BufferedReader(fileReader);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
