package kr.ac.pusan.bsclab.bab.v2.web.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.BabWeb;
import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.IJobManager;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.IFileManager;

@Controller
public abstract class AController {

	public static final String BASE_URL = "/bab/v2";
	
	@Autowired
	protected BabConfiguration config;

	@Autowired
	protected RequestMappingHandlerMapping handlerMapping;
	
	@Autowired
	protected IJobManager jobManager;
	
	@Autowired
	protected IFileManager fileManager;
	
	protected ObjectMapper mapper;

	public BabConfiguration getConfig() {
		return config;
	}

	public RequestMappingHandlerMapping getHandlerMapping() {
		return handlerMapping;
	}

	public IJobManager getJobManager() {
		return jobManager;
	}

	public IFileManager getFileManager() {
		return fileManager;
	}
	
	public ObjectMapper getMapper() {
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper;
	}

	public Logger log(Object caller) {
		return BabWeb.log(caller);
	}
}

