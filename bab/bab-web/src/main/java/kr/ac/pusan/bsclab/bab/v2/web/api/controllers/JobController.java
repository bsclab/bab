package kr.ac.pusan.bsclab.bab.v2.web.api.controllers;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;

@Controller
public class JobController extends AServiceController {

  public static final String BASE_URL = AServiceController.BASE_URL + "/job";

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/manager")
  public ModelAndView getManager() {
    return getManager(0);
  }

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/manager/{refresh}")
  public ModelAndView getManager(@PathVariable(value = "refresh", required = false) int refresh) {
    if (refresh > 0 && refresh < 5) {
      refresh = 5;
    }
    Map<String, String> services = new LinkedHashMap<String, String>();
    Map<String, Map<String, ServiceEndpoint>> babServices = getJobManager().getServices(0);
    for (String packageId : babServices.keySet()) {
      for (String serviceId : babServices.get(packageId)
                                         .keySet()) {
        // services.put(AServiceController.BASE_URL + "/job/submit/" + packageId + "/" + serviceId,
        // "[GET][POST]");
        services.put(AServiceController.BASE_URL + "/spark/submit/" + packageId + "/" + serviceId
            + "/{workspaceId}/{datasetId}/{repositoryId}", "[GET][POST]");
      }
    }
    Set<RequestMappingInfo> mappings = this.handlerMapping.getHandlerMethods()
                                                          .keySet();
    for (RequestMappingInfo map : mappings) {
      StringBuilder reqMethod = new StringBuilder();
      Set<RequestMethod> methods = map.getMethodsCondition()
                                      .getMethods();
      for (RequestMethod method : methods) {
        reqMethod.append("[");
        reqMethod.append(method);
        reqMethod.append("]");
      }
      Set<String> patterns = map.getPatternsCondition()
                                .getPatterns();
      for (String pattern : patterns) {
        if (pattern.startsWith(AServiceController.BASE_URL)) {
          services.put(pattern, reqMethod.toString());
        }
      }
    }

    ModelAndView view = new ModelAndView("api/job/manager");
    view.addObject("refresh", refresh);
    view.addObject("jobmanager", getJobManager());
    view.addObject("services", services);
    view.addObject("config", getConfig());
    return view;
  }
  //
  // @CrossOrigin
  // @RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
  // + "/submit/{packageId}/{serviceId}")
  // @ResponseBody
  // public ServiceResponse postSubmit(@PathVariable(value = "packageId") String packageId,
  // @PathVariable(value = "serviceId") String serviceId, ServiceRequest request) {
  // if (getJobManager().getServices(0).containsKey(packageId)
  // && getJobManager().getServices(0).get(packageId).containsKey(serviceId)) {
  // ServiceEndpoint endpoint = getJobManager().getServices(0).get(packageId).get(serviceId);
  // Job job = getJobManager().getJobFactory().create(endpoint, request);
  // if (job != null) {
  // job = getJobManager().submit(job);
  // return job.getResponse();
  // }
  // }
  // return null;
  // }

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/queue")
  @ResponseBody
  public Map<String, Collection<Job>> getQueue() {

    Map<String, Collection<Job>> result = new LinkedHashMap<String, Collection<Job>>();
    result.put("active", getJobManager().getRunning()
                                        .values());
    result.put("queue", getJobManager().getQueue()
                                       .values());
    result.put("completed", getJobManager().getCompleted()
                                           .values());
    return result;
  }

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/queue/{status}")
  @ResponseBody
  public Map<String, Collection<Job>> getQueueByStatus(@PathVariable("status") String status) {

    Map<String, Collection<Job>> result = new LinkedHashMap<String, Collection<Job>>();
    result.put(status, getJobManager().getRunning()
                                      .values());
    return result;
  }

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/services/{reload}")
  @ResponseBody
  public Map<String, Map<String, ServiceEndpoint>> getServices(
      @PathVariable(value = "reload", required = false) int reload) {
    Map<String, Map<String, ServiceEndpoint>> result = getJobManager().getServices(reload);
    return result;
  }

  @CrossOrigin
  @RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/pull")
  @ResponseBody
  public Job getPull() {
    Job job = getJobManager().pullAndRun();
    return job;
  }

  @CrossOrigin
  @RequestMapping(method = {RequestMethod.GET}, path = BASE_URL + "/report/{jobId}")
  @ResponseBody
  public Job postReport(@PathVariable(value = "jobId") String jobId) {
    Job job = getJobManager().status(jobId);
    return job;
  }

  @CrossOrigin
  @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST},
      path = BASE_URL + "/update/{jobId}")
  @ResponseBody
  public Job postUpdate(@PathVariable(value = "jobId") String jobId,
      @RequestBody(required = false) String remarks) {
    if (remarks == null) {
      remarks = "";
    }
    Job job = getJobManager().update(jobId, remarks);
    return job;
  }

  @CrossOrigin
  @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST},
      path = BASE_URL + "/report/{jobId}/{status}")
  @ResponseBody
  public Job postReport(@PathVariable(value = "jobId") String jobId,
      @PathVariable(value = "status", required = false) String status,
      @RequestBody(required = false) String remarks) {
    Job job = getJobManager().finish(jobId, status, remarks);
    return job;
  }

  @CrossOrigin
  @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST},
      path = BASE_URL + "/cancel/{jobId}")
  @ResponseBody
  public Job postCancel(@PathVariable(value = "jobId") String jobId,
      @RequestBody(required = false) String remarks) {
    Job job = getJobManager().cancel(jobId, remarks);
    return job;
  }

  @CrossOrigin
  @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST},
      path = BASE_URL + "/kill/{jobId}")
  @ResponseBody
  public Job postKill(@PathVariable(value = "jobId") String jobId, @RequestBody String remarks) {
    Job job = getJobManager().kill(jobId, remarks);
    return job;
  }
  //
  // @CrossOrigin
  // @RequestMapping(method = RequestMethod.GET, path = BASE_URL +
  // "/test/submit")
  // public void getTestSubmit() {
  // Job job = getJobManager().getJobFactory().create();
  // getJobManager().submit(job);
  // }

}
