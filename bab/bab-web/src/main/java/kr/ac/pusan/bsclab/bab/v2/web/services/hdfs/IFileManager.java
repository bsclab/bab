package kr.ac.pusan.bsclab.bab.v2.web.services.hdfs;

import org.springframework.stereotype.Service;

@Service
public interface IFileManager {
	boolean isExists(String uri);
	void saveAsTextFile(String uri, String data);
	String loadFromTextFile(String uri);
}
