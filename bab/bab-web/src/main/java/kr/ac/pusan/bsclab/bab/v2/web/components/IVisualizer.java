package kr.ac.pusan.bsclab.bab.v2.web.components;

public interface IVisualizer<T> {
	T visualize();
}
