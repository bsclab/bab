package kr.ac.pusan.bsclab.bab.v2.web.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("bab")
public class BabConfiguration extends AConfiguration {

  protected String name;
  protected String title;
  protected String version;
  protected String serviceJobManager;
  protected String serviceJobFactory;
  protected String serviceJobExecutor;
  protected String serviceFileManager;
  protected String packageDirectory;
  protected String serverIp;
  protected String url;

  public String getPackageDirectory() {
    return packageDirectory;
  }

  public void setPackageDirectory(String packageDirectory) {
    this.packageDirectory = packageDirectory;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getServiceJobManager() {
    return serviceJobManager;
  }

  public void setServiceJobManager(String serviceJobManager) {
    this.serviceJobManager = serviceJobManager;
  }

  public String getServiceJobFactory() {
    return serviceJobFactory;
  }

  public void setServiceJobFactory(String serviceJobFactory) {
    this.serviceJobFactory = serviceJobFactory;
  }

  public String getServiceJobExecutor() {
    return serviceJobExecutor;
  }

  public void setServiceJobExecutor(String serviceJobExecutor) {
    this.serviceJobExecutor = serviceJobExecutor;
  }

  public String getServiceFileManager() {
    return serviceFileManager;
  }

  public void setServiceFileManager(String serviceFileManager) {
    this.serviceFileManager = serviceFileManager;
  }

  public String getServerIp() {
    return serverIp;
  }

  public void setServerIp(String serverIp) {
    this.serverIp = serverIp;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
