package kr.ac.pusan.bsclab.bab.v2.web.components;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

public class HtmlPage extends ModelAndView implements IVisualizer<String> {
	
	// e.g. V=Name, K=URI
	protected final Map<String, String> js = new LinkedHashMap<String, String>();
	protected final Map<String, String> css = new LinkedHashMap<String, String>();
	
	public HtmlPage() {
		super();
	}
	
	public HtmlPage(String viewName) {
		super(viewName);
	}	
	
	public Map<String, String> getJs() {
		return js;
	}

	public Map<String, String> getCss() {
		return css;
	}

	@Override
	public String visualize() {
		return null;
	}
	
}
