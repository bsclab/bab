package kr.ac.pusan.bsclab.bab.v2.web;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;

@ComponentScan(basePackages = "kr.ac.pusan.bsclab.bab.v2.web")
@EnableScheduling
@SpringBootApplication
public class BabWeb extends WebMvcConfigurerAdapter {
  public static final String BASE_URL = "/bab";

  public static void main(String[] args) throws Exception {
    SpringApplication.run(BabWeb.class, args);

  }
	private static boolean firstInit = true;
	
  @Autowired
  protected BabConfiguration config;

  private static Map<Class<?>, Logger> loggers;

  public static Logger log(Object caller) {
    if (loggers == null) {
      loggers = new LinkedHashMap<Class<?>, Logger>();
    }
    if (!loggers.containsKey(caller.getClass())) {
      loggers.put(caller.getClass(), LoggerFactory.getLogger(caller.getClass()));
    }
    return loggers.get(caller.getClass());
  }

  @PostConstruct
  public void init() {
  	if (firstInit) {
      	log(this).info("\r\n\r\n\r\n" 
			+ "                            @@@@@@                          \r\n"
			+ "       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@                        \r\n"
			+ "    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                      \r\n"
			+ "  @@@@4@@   @@@@@@@@@   @@@@@@   @@@I@@             @@@@    \r\n"
			+ " @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@    @    @       @   \r\n"
			+ " @@@@@n@@               @@@@@@   @@@d@@    @    @   @@@@    \r\n"
			+ "  @@@@@@@               @@@@@@   @@@@@@     @  @    @       \r\n"
			+ "  @@@@@@@   @@@@@@@@@   @@@@@@      @@@      @@   @  @@@@   \r\n"
			+ "  @@@@o@@   @@@@@@@@@   @@@@@@      @@@                     \r\n"
			+ "  @@@@@@@               @@@@@@   @@@@@@@   by               \r\n"
			+ "  @@@@@@@               @@@@@@   @@@@@@@                    \r\n"
			+ "  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@n@@@   BSCLab @ IEPNU   \r\n"
			+ "   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@     2015 - 2017    \r\n"
			+ "   @@@@@@                        @@@@@@@        Busan       \r\n"
			+ "   @@@e@@                        @@@s@@@     South Korea    \r\n"
			+ "   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@                   \r\n"
			+ "   @@@@@@                        @@@@@@@@  S T A R T I N G  \r\n"
			+ "   @@@i@@                        @@@a@@@       . . . .      \r\n"
			+ "    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                      \r\n"
			+ "      @@@@@@@@@@@@@@@@@@@@@@@@@@@                           \r\n"
			+ "          @@@@@@@@@                                         \r\n"
			+ "\r\n");
  		firstInit = false;
  	}
  }
}
