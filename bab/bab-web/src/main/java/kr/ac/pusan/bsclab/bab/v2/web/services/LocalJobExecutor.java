package kr.ac.pusan.bsclab.bab.v2.web.services;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "bab.serviceJobExecutor", havingValue = "local",
    matchIfMissing = true)
public class LocalJobExecutor implements IJobExecutor {

  @Override
  public Job run(Job job) {
    return job;
  }

  @Override
  public Job status(Job job) {
    return job;
  }

  @Override
  public Job kill(Job job) {
    return job;
  }

  @Override
  public boolean isBusy() {
    return false;
  }

}
