package kr.ac.pusan.bsclab.bab.v2.web.services;

import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;

@Service
public interface IJobManager {
  IJobFactory getJobFactory();

  IJobExecutor getJobExecutor();

  String getSessionId();

  int getMaxSimultaneousRun();

  void setMaxSimultaneousRun(int maxSimultaneousRun);

  Map<String, Job> getAll();

  Map<String, Job> getQueue();

  Map<String, Job> getRunning();

  Map<String, Job> getCompleted();

  Map<String, Map<String, ServiceEndpoint>> getServices(int reload);

  Map<String, Map<String, Set<ServiceEndpoint>>> getJars(int reload);

  Job register(Job job);

  Job submit(Job job);

  Job pullAndRun();

  Job start(String jobId);

  Job update(String jobId, String status);

  Job finish(String jobId, String status, String remarks);

  Job kill(String jobId, String remarks);

  Job cancel(String jobId, String remarks);

  Job status(String jobId);

}
