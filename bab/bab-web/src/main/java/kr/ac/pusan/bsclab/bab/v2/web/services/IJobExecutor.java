package kr.ac.pusan.bsclab.bab.v2.web.services;

import org.springframework.stereotype.Service;

@Service
public interface IJobExecutor {

  Job run(Job job);

  Job status(Job job);

  Job kill(Job job);

  boolean isBusy();

}
