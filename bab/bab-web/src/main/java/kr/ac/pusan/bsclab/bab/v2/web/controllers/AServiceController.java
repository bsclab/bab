package kr.ac.pusan.bsclab.bab.v2.web.controllers;

import org.springframework.stereotype.Controller;

@Controller
public abstract class AServiceController extends AController {

  public static final String BASE_URL = AController.BASE_URL + "/api";

}
