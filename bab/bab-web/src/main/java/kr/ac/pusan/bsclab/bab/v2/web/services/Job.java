package kr.ac.pusan.bsclab.bab.v2.web.services;

import java.util.Date;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceResponse;

public class Job implements IServiceResponse {

  public static final String STATUS_QUEUED = "QUEUE";
  public static final String STATUS_CANCELED = "CANCELED";
  public static final String STATUS_RUNNING = "RUNNING";
  public static final String STATUS_COMPLETED = "COMPLETED";
  public static final String STATUS_FINISHED = "FINISHED";
  public static final String STATUS_SUBMITTED = "SUBMITTED";
  public static final String STATUS_WAITING = "WAITING";
  public static final String STATUS_KILLED = "KILLED";
  public static final String STATUS_FAILED = "FAILED";
  public static final String STATUS_ERROR = "ERROR";

  protected String uri;
  protected String id;
  protected String hash;
  protected String name = "UNTITLED";
  protected long created = new Date().getTime();
  protected long started = 0l;
  protected long finished = 0l;
  protected String author = "ANONYMOUS";
  protected String status = STATUS_QUEUED;
  protected String remarks = "";
  protected ServiceRequest request;
  protected ServiceResponse response;

  public boolean isRunning() {
    return status.equalsIgnoreCase(STATUS_RUNNING) || status.equalsIgnoreCase(STATUS_WAITING)
        || status.equalsIgnoreCase(STATUS_SUBMITTED);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getCreated() {
    return created;
  }

  public void setCreated(long created) {
    this.created = created;
  }

  public long getStarted() {
    return started;
  }

  public void setStarted(long started) {
    this.started = started;
  }

  public long getFinished() {
    return finished;
  }

  public void setFinished(long finished) {
    this.finished = finished;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public ServiceRequest getRequest() {
    return request;
  }

  public void setRequest(ServiceRequest request) {
    this.request = request;
  }

  public ServiceResponse getResponse() {
    return response;
  }

  public void setResponse(ServiceResponse response) {
    this.response = response;
  }

  @Override
  public void setUri(String uri) {
    this.uri = uri;
  }

  @Override
  public String getUri() {
    return uri;
  }

}
