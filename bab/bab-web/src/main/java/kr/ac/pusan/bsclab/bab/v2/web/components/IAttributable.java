package kr.ac.pusan.bsclab.bab.v2.web.components;

import java.util.Map;

public interface IAttributable<K, V> {
	public Map<K, V> getAttributes();
}
