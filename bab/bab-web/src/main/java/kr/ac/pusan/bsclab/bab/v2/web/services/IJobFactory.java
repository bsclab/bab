package kr.ac.pusan.bsclab.bab.v2.web.services;

import org.springframework.stereotype.Service;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;

@Service
public interface IJobFactory {
  Job create(ServiceEndpoint endpoint, IServiceRequest request);
}
