/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.ws.api.AbstractJob;
import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkLocalExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.SparkFpGrowthArMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.SparkDottedChartAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.SparkDeltaAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.SparkKpiAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SparkSocialNetworkAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.SparkTimeGapAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.SparkTaskMatrixAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models.SparkYmkpiJob;
import kr.ac.pusan.bsclab.bab.ws.api.hello.WorldJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob2;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.SparkIdleTimeNetworkMiner;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.SparkLogReplayJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.SparkHNetToBPMNMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.fl.SparkFilteringJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.csv.brepo.SparkCsvImportJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.mxml.brepo.SparkMxmlGzImportJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.mxml.brepo.SparkMxmlImportJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.SparkLogSummaryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.SparkIntermediateMappingJob;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.model.RawResource;

/**
 * 
 * BAB main class, this class will be included in bab.jar file as an entry point class
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class Bab2 {
	private static String driverUri = null;
	
	/**
	 * BAB entry point, it will run as spark driver program or run as standalone program
	 * (not recommended except for debugging)
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		long startup = System.currentTimeMillis();
//		try {
			SparkExecutor.debug("STARTUP @ " + startup);

			ObjectMapper mapper = new ObjectMapper();
			System.out.println(mapper.writeValueAsString(args));
			
			Map<String, Class<?>> jobClasses = new TreeMap<String, Class<?>>();

			
			jobClasses.put("RepositoryCsvImportJob", SparkCsvImportJob.class);
			jobClasses.put("RepositoryMxmlImportJob", SparkMxmlImportJob.class);
			jobClasses.put("RepositoryMxmlGzImportJob", SparkMxmlGzImportJob.class);
			jobClasses.put("RepositoryMappingJob", SparkIntermediateMappingJob.class);
			jobClasses.put("RepositoryFilteringJob", SparkFilteringJob.class);
			jobClasses.put("RepositoryLogSummary", SparkLogSummaryJob.class);
			
			jobClasses.put("ModelHeuristicJob", SparkHeuristicMinerJob.class);
			jobClasses.put("ModelLogReplayJob", SparkLogReplayJob.class);

			jobClasses.put("AnalysisAssociationRuleJob", SparkFpGrowthArMinerJob.class);
			jobClasses.put("AnalysisKpiJob", SparkKpiAnalysis.class);
			jobClasses.put("AnalysisSocialNetworkJob", SparkSocialNetworkAnalysisJob.class);
			jobClasses.put("AnalysisTimeGapJob", SparkTimeGapAnalysisJob.class);
			jobClasses.put("AnalysisDeltaJob", SparkDeltaAnalysisJob.class);
			jobClasses.put("AnalysisTaskMatrixJob", SparkTaskMatrixAnalysisJob.class);
			jobClasses.put("AnalysisDottedChartJob", SparkDottedChartAnalysisJob.class);

//			jobClasses.put("AnalysisKMeansClusteringJob", KMeansClusteringJob.class);
			jobClasses.put("HNetToBPMNMinerJob", SparkHNetToBPMNMinerJob.class);
			jobClasses.put("ModelHeuristicJob2", SparkHeuristicMinerJob2.class);
			jobClasses.put("ModelIdleTimeNetworkMinerJob", SparkIdleTimeNetworkMiner.class);

			jobClasses.put("HelloWorldJob", WorldJob.class);
			jobClasses.put("YmKPIJob", SparkYmkpiJob.class);


			if (args.length < 5) throw new Exception("Args Invalid");
			driverUri = args[0];
			String hdfs = args[1];
			String spark = args[2];
			String jobClass = args[3];
			String jobPath = args[4];
			String userId = args.length > 5 ? args[5] : "0";
			if (!jobClass.equalsIgnoreCase("BabBatchJob") && !jobClasses.containsKey(jobClass)) throw new Exception("Job Class Invalid");

			SparkExecutor se = new SparkLocalExecutor();
			se.setHdfsURI(hdfs);
			se.startup(spark);
			
//			addJar(hdfs + "/bab/lib/flexjson-2.1.jar");
			//se.getContext().addJar(hdfs + "/bab/lib/bab-asm-ntreef-iq.jar");

			//*/ Test Connection
			
			String requestPayload = se.getFileUtil().loadTextFile(se, se.getHdfsURI(jobPath));
			SparkExecutor.debug("MAIN CONFIGURATION: " + se.getHdfsURI(jobPath));
			SparkExecutor.debug("MAIN CONFIGURATION: " + requestPayload);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JobConfiguration request = mapper.readValue(requestPayload, JobConfiguration.class);
			se.setAppName("BAB-ASM-NTREEF (" + userId + "): " + request.getJobId());
			SparkExecutor.setDebugId(request.getJobId());
			//se.setExecutorCore(1);
//			se.setExecutorMemory(24576);
//			se.setDriverMemory(24576);
			se.setExecutorMemory(2);
			se.setDriverMemory(2);
			//JavaSparkContext jsc = se.getContext();
			if (!jobClass.equalsIgnoreCase("BabBatchJob") && request.getJobs() == null || request.getJobs().size() < 1) {
				request.getJobs().add(new String[] { jobClass, jobPath });
			}
			boolean isError = false;
			StringBuilder errors = new StringBuilder();
			int count = 0;
			for (String[] params : request.getJobs()) {
				try {
					jobClass = params[0];
					jobPath = params[1];
					count++;
					SparkExecutor.debug("LOADING JOB(" + count + "): " + jobClass + " " + jobPath);
					requestPayload = se.getFileUtil().loadTextFile(se, se.getHdfsURI(jobPath));
					request = mapper.readValue(requestPayload, JobConfiguration.class);
					SparkExecutor.debug("STARTING WITH CONFIGURATION: " + requestPayload);
					AbstractJob job = (AbstractJob) jobClasses.get(jobClass).getConstructors()[0].newInstance(new Object[0]);
					IJobResult result = job.run(request.getConfiguration(), new RawResource("resource.raw.Mxml", null, request.getPath()), se);
					if (result == null || result.getResponse() == null) {
						SparkExecutor.debug("RESULT: FAILED");
						throw new Exception("NULL / Empty Result");
					}
					SparkExecutor.debug("RESULT: " + result.getResponse().length() + " Bytes");
				} catch (Exception e) {
					e.printStackTrace();
					errors.append(e.getMessage());
					isError = true;
				}
			}
			if (isError) {
				throw new Exception(errors.toString());
			}
			se.shutdown();
			long shutdown = System.currentTimeMillis();
			SparkExecutor.debug("SHUTDOWN @ " + shutdown);
			SparkExecutor.debug("DURATION @ " + (shutdown - startup) + " ms");
			report(driverUri);
			/*
		} catch (Exception e) {
			debug("ERROR: " + e.getMessage());
			long shutdown = System.currentTimeMillis();
			debug("SHUTDOWN @ " + shutdown);
			debug("DURATION @ " + (shutdown - startup) + " ms");
			report(driverUri + "/FAILED");
			throw e;
		}
		*/		
	}

	/**
	 * Helper method for report progress remotely from spark master
	 * to BAB web service 
	 * 
	 * @param driverUri
	 */
	public static void report(String driverUri) {
		try {
			URL url = new URL(driverUri);
			URLConnection connection = url.openConnection();
			InputStream in = connection.getInputStream();
			in.close();
		} catch (Exception e) {
			System.out.println("ERROR:  " + e.getMessage());
			e.printStackTrace();
		}
	}
	
}
