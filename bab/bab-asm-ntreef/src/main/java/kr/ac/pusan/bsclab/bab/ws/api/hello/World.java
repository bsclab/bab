package kr.ac.pusan.bsclab.bab.ws.api.hello;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

public class World extends Result {

	private static final long serialVersionUID = 1L;

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
