package kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models;

import java.io.Serializable;
import java.util.Map;

/**
 * Example result class from Test Algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class Ymkpi implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  private YmkpiParameter parameter;
  private Map<String, Map<String, Double>> yearly;
  private Map<String, Map<String, Double>> monthly;

  public YmkpiParameter getParameter() {
    return parameter;
  }

  public void setParameter(YmkpiParameter parameter) {
    this.parameter = parameter;
  }

  public Map<String, Map<String, Double>> getYearly() {
    return yearly;
  }

  public void setYearly(Map<String, Map<String, Double>> yearly) {
    this.yearly = yearly;
  }

  public Map<String, Map<String, Double>> getMonthly() {
    return monthly;
  }

  public void setMonthly(Map<String, Map<String, Double>> monthly) {
    this.monthly = monthly;
  }

}
