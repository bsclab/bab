BAB v2.2 Hotfix Roadmap
====================

# WEB SERVICE
- - - 
## Event Repository

1. Import Event Log
2. Mapping Event Log
3. Dashboard

## Process Discovery

1. Inductive Miner
2. Heuristic Miner
3. Fuzzy Miner
4. Timegap Analysis
5. Log Replay
6. Social Network Analysis

## Process Analysis

1. Footprint-based Conformance Checking
2. Association Rule Miner
3. Delta Analysis

## Process and Data Visualization

1. Task Matrix
2. Dotted Chart
3. Performance Chart

# WEB USER INTERFACE
- - -  

## Data Visualization

1. Data Table Visualization
2. 2D General Matrix Visualization

## Graph Visualization

1. Petri-net Visualization
2. Dependency Graph Visualization
3. BPMN Model Visualization
4. Social Network Visualization

## Chart Visualization

1. Heatmap Chart
2. Dotted Chart
3. Line Chart
4. Bar Chart
5. Pie Chart
